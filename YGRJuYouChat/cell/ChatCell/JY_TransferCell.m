//
//  JY_TransferCell.m
//  TFJunYouChat
//
//  Created by 1 on 2019/3/1.
//  Copyright © 2019年 Reese. All rights reserved.
//

#import "JY_TransferCell.h"

@interface JY_TransferCell ()

@property (nonatomic, strong) UIImageView *headImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *title;
@property (nonatomic, strong) UILabel *moneyLabel;
@property (nonatomic, strong) UIView *titleBG;
@end

@implementation JY_TransferCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)creatUI{
    self.bubbleBg.custom_acceptEventInterval = 1.0;
    
    _imageBackground =[[JY_ImageView alloc]initWithFrame:CGRectZero];
    [_imageBackground setBackgroundColor:[UIColor clearColor]];
    _imageBackground.layer.cornerRadius = 6;
    _imageBackground.image = [UIImage imageNamed:@"transfer_back_icon"];
    _imageBackground.layer.masksToBounds = YES;
    _imageBackground.contentMode = UIViewContentModeScaleAspectFill;
    [self.bubbleBg addSubview:_imageBackground];
    
    _headImageView = [[UIImageView alloc]init];
    _headImageView.frame = CGRectMake(20,15, 36, 36);
    _headImageView.image = [UIImage imageNamed:@"icon_zhuanzhangIcon"];
//    [UIImage imageNamed:@"ic_transfer_money"];
    _headImageView.userInteractionEnabled = NO;
    [_imageBackground addSubview:_headImageView];
    
    
    _moneyLabel = [[UILabel alloc]init];
    _moneyLabel.frame = CGRectMake(CGRectGetMaxX(_headImageView.frame) + 15,20, 180, 15);
    _moneyLabel.font = g_factory.font15;
    _moneyLabel.textColor = [UIColor whiteColor];
    _moneyLabel.numberOfLines = 0;
    _moneyLabel.userInteractionEnabled = NO;
    [_imageBackground addSubview:_moneyLabel];

    
    _nameLabel = [[UILabel alloc]init];
    _nameLabel.frame = CGRectMake(CGRectGetMaxX(_headImageView.frame) + 15,CGRectGetMaxY(_moneyLabel.frame)+5, 180, 14);
    _nameLabel.font = g_factory.font14;
    _nameLabel.textColor = [UIColor whiteColor];
    _nameLabel.numberOfLines = 0;
    _nameLabel.userInteractionEnabled = NO;
    [_imageBackground addSubview:_nameLabel];
    
    UIView *titleBG = [[UIView alloc]init];
    titleBG.backgroundColor = [UIColor whiteColor];
    titleBG.frame = CGRectMake(0, _imageBackground.frame.size.height-23, _imageBackground.frame.size.width, 23);
    [_imageBackground addSubview:titleBG];
    _titleBG = titleBG;

    _title = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, 200, 23)];
    _title.text = @"聚友转账";
    //Localized(@"JX_Transfer")
    _title.font = SYSFONT(10.0);
    _title.textColor = HEXCOLOR(0x999999);
//    [UIColor whiteColor];
    [_imageBackground addSubview:_title];
}

-(void)setCellData{
    [super setCellData];
    int n = imageItemHeight;
    
    if(self.msg.isMySend)
    {
        self.bubbleBg.frame=CGRectMake(CGRectGetMinX(self.headImage.frame)- kChatCellMaxWidth+20 - CHAT_WIDTH_ICON, INSETS, kChatCellMaxWidth-20, n+INSETS -4);
        _imageBackground.frame = self.bubbleBg.bounds;
        
    }
    else
    {
        self.bubbleBg.frame=CGRectMake(CGRectGetMaxX(self.headImage.frame) + CHAT_WIDTH_ICON, INSETS2(self.msg.isGroup), kChatCellMaxWidth-20, n+INSETS -4);
        _imageBackground.frame = self.bubbleBg.bounds;
    }
    
    
    CGFloat bg_top = CGRectGetMaxY(_imageBackground.frame)-23;
    _titleBG.frame = CGRectMake(0, bg_top, _imageBackground.frame.size.width, _imageBackground.frame.size.height-bg_top);
    
    
    _title.frame = CGRectMake(15, _imageBackground.frame.size.height - 23, 200, 23);
    
    if (self.msg.isShowTime) {
        CGRect frame = self.bubbleBg.frame;
        frame.origin.y = self.bubbleBg.frame.origin.y + 40;
        self.bubbleBg.frame = frame;
    }
    
    [self setMaskLayer:_imageBackground];
    JY_UserObject *user = [[JY_UserObject alloc] init];
    user = [user getUserById:self.msg.toUserId];
    _nameLabel.text = self.msg.fileName.length > 0 ? self.msg.fileName : self.msg.isMySend ? [NSString stringWithFormat:@"%@%@",Localized(@"JX_TransferTo"),user.remarkName.length > 0 ? user.remarkName : user.userNickname] : Localized(@"JX_TransferToYou");
    _moneyLabel.text = [NSString stringWithFormat:@"¥%@",self.msg.content];

    if ([self.msg.fileSize intValue] == 2) {
        
        _imageBackground.alpha = 0.5;
    }else {
        
        _imageBackground.alpha = 1;
    }
}

-(void)didTouch:(UIButton*)button{
    self.msg.index = self.indexNum;
    [g_notify postNotificationName:kcellTransferDidTouchNotifaction object:self.msg];
}

+ (float)getChatCellHeight:(JY_MessageObject *)msg {
    if ([g_App.isShowRedPacket intValue] == 1){
        if ([msg.chatMsgHeight floatValue] > 1) {
            return [msg.chatMsgHeight floatValue]-10 - 5;
        }
        
        float n = 0;
        if (msg.isGroup && !msg.isMySend) {
            if (msg.isShowTime) {
                n = ManMan_SCREEN_WIDTH/3 + 10 + 40;
            }else {
                n = ManMan_SCREEN_WIDTH/3 + 10;
            }
            n += GROUP_CHAT_INSET;
        }else {
            if (msg.isShowTime) {
                n = ManMan_SCREEN_WIDTH/3 + 40;
            }else {
                n = ManMan_SCREEN_WIDTH/3;
            }
        }
        
        msg.chatMsgHeight = [NSString stringWithFormat:@"%f",n];
        if (!msg.isNotUpdateHeight) {
            [msg updateChatMsgHeight];
        }
        return n;
        
    }else{
        
        msg.chatMsgHeight = [NSString stringWithFormat:@"0"];
        if (!msg.isNotUpdateHeight) {
            [msg updateChatMsgHeight];
        }
        return 0;
    }
}


@end
