//
//  JY_RedPacketCell.m
//  TFJunYouChat
//
//  Created by lifengye on 2020/09/10.
//  Copyright © 2020 zengwOS. All rights reserved.
//

#import "JY_RedPacketCell.h"

@interface JY_RedPacketCell ()

@property (nonatomic, strong) UIImageView *headImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *title;
@property (nonatomic, strong) UIView *titleBG;

@end

@implementation JY_RedPacketCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
+ (CGFloat)getWidthWithTitle:(NSString *)title font:(UIFont *)font {
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 1000, 0)];
    label.text = title;
    label.font = font;
    [label sizeToFit];
    CGFloat width = label.frame.size.width;
    return ceil(width);
}
- (CGSize)autoMakeColorfulLabel:(UIImageView *)label font:(UIFont *)font text:(NSString *)text {
    NSString *name = text;
    if (name == nil) {
        name = @"";
    }
    CGFloat width =  kChatCellMaxWidth;
    CGFloat height =  imageItemHeight+INSETS -4;
    
    UIColor *leftColor = RGB(238, 125, 105); HEXCOLOR(0xFD6E6A);
    UIColor *rightColor = RGB(234, 90, 77);// HEXCOLOR(0xFFC600);
    UIImage *bgImg = [UIImage gradientColorImageFromColors:@[leftColor, rightColor] gradientType:GradientTypeLeftToRight imgSize:CGSizeMake(width+10, height+2)];
    [label setBackgroundColor:[UIColor colorWithPatternImage:bgImg]];
    label.layer.cornerRadius = height/2.0;
    return CGSizeMake(width+10, height+2);
}
-(void)creatUI{
    self.bubbleBg.custom_acceptEventInterval = 1.0;
    /**
     上面红色的是#ffcc0000，下面白色的是40px，边框颜色#cfcfcf，1个px，文字颜色#999999
     */
    _imageBackground =[[JY_ImageView alloc]initWithFrame:CGRectZero];
    [_imageBackground setBackgroundColor:[UIColor clearColor]];
    _imageBackground.layer.cornerRadius = 6;//bg_changty hongbaokuan
    _imageBackground.image = [UIImage imageNamed:@"weilingqu"];
//    _imageBackground.backgroundColor = HEXCOLOR(0xcc0000);
    _imageBackground.contentMode = UIViewContentModeScaleAspectFill;
    _imageBackground.layer.masksToBounds = YES;
    [self.bubbleBg addSubview:_imageBackground];
    
    _headImageView = [[UIImageView alloc]init];
    _headImageView.frame = CGRectMake(20,15, 28, 36);
//    _headImageView.image = [UIImage imageNamed:@"icon_redIcon"];
    _headImageView.image = [UIImage imageNamed:@"hongb"];
    _headImageView.userInteractionEnabled = NO;
    [_imageBackground addSubview:_headImageView];
    
    _nameLabel = [[UILabel alloc]init];
    _nameLabel.frame = CGRectMake(CGRectGetMaxX(_headImageView.frame) + 12,22.5, 180, 20);
    _nameLabel.font = [UIFont systemFontOfSize:15 weight:UIFontWeightMedium];
    _nameLabel.textColor = [UIColor whiteColor];
    _nameLabel.numberOfLines = 0;
    _nameLabel.userInteractionEnabled = NO;
    [_imageBackground addSubview:_nameLabel];
    

    _titleLingQu = [[UILabel alloc] init];
    _titleLingQu.text = Localized(@"JX_yilingquRed");
    _titleLingQu.font = SYSFONT(12.0);
    _titleLingQu.textColor =  [UIColor whiteColor];
    _titleLingQu.textAlignment = NSTextAlignmentLeft;
    _titleLingQu.hidden = YES;
    [_imageBackground addSubview:_titleLingQu];
    _titleLingQu.frame=CGRectMake(CGRectGetMaxY(_headImageView.frame) + 15, CGRectGetMaxY(_nameLabel.frame)+5, 100, 14);
    
    
//    _lineView = [[UIView alloc] initWithFrame:CGRectMake(15, 60, 180, 0.5)];
//    _lineView.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.2];
//    [_imageBackground addSubview:_lineView];
    
    
    
    UIView *titleBG = [[UIView alloc]init];
    titleBG.backgroundColor = [UIColor whiteColor];
    titleBG.frame = CGRectMake(0, _imageBackground.frame.size.height-23, _imageBackground.frame.size.width, 23);
    [_imageBackground addSubview:titleBG];
    _titleBG = titleBG;
    
    _title = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxY(_headImageView.frame) + 15, CGRectGetMaxX(_nameLabel.frame)+5, 100, 14)];
    _title.text = Localized(@"JX_BusinessCard");
    _title.font = SYSFONT(10.0);
    _title.textColor = HEXCOLOR(0x999999);
//    [UIColor whiteColor];
    [_imageBackground addSubview:_title];
    
    //
//    _redPacketGreet = [[JY_Emoji alloc]initWithFrame:CGRectMake(5, 25, 80, 16)];
//    _redPacketGreet.textAlignment = NSTextAlignmentCenter;
//    _redPacketGreet.font = [UIFont systemFontOfSize:12];
//    _redPacketGreet.textColor = [UIColor whiteColor];
//    _redPacketGreet.userInteractionEnabled = NO;
//    [_imageBackground addSubview:_redPacketGreet];
}

-(void)setCellData{
    [super setCellData];
    int n = imageItemHeight;
    
    if(self.msg.isMySend)
    {
        self.bubbleBg.frame=CGRectMake(CGRectGetMinX(self.headImage.frame)- kChatCellMaxWidth+20 - CHAT_WIDTH_ICON, INSETS, kChatCellMaxWidth-20, n+INSETS -4);
        _imageBackground.frame = self.bubbleBg.bounds;
        
    }
    else
    {
        self.bubbleBg.frame=CGRectMake(CGRectGetMaxX(self.headImage.frame) + CHAT_WIDTH_ICON, INSETS2(self.msg.isGroup), kChatCellMaxWidth-20, n+INSETS -4);
        _imageBackground.frame = self.bubbleBg.bounds;
    }
    //CGRectGetMaxX(_headImageView.frame) +
 
    
//    _title.frame = CGRectMake(15, CGRectGetMaxY(_imageBackground.frame)-20, 100, 14);
//    _lineView.frame=CGRectMake(15, CGRectGetMaxY(_imageBackground.frame)-30, 180, 0.5);
    CGFloat bg_top = CGRectGetMaxY(_imageBackground.frame)-23;
    
    _title.frame = CGRectMake(15, _imageBackground.frame.size.height - 23, 200, 23);
    
    _titleBG.frame = CGRectMake(0, bg_top, _imageBackground.frame.size.width, _imageBackground.frame.size.height-bg_top);
    
    if (self.msg.isShowTime) {
        CGRect frame = self.bubbleBg.frame;
        frame.origin.y = self.bubbleBg.frame.origin.y + 40;
        self.bubbleBg.frame = frame;
    }
    [self.bubbleBg setBackgroundImage:nil forState:0];
    [self.bubbleBg setBackgroundImage:nil forState:UIControlStateSelected];
    [self setMaskLayer:_imageBackground];
    
    //服务端返回的数据类型错乱，强行改
    self.msg.fileName = [NSString stringWithFormat:@"%@",self.msg.fileName];
    if ([self.msg.fileName isEqualToString:@"3"]) {
 
        _nameLabel.text = self.msg.content;
        _title.text =[NSString stringWithFormat:@"%@%@",APP_NAME, Localized(@"JX_MesGift")];
 
    
    }else if ([self.msg.fileName isEqualToString:@"4"]) {
//        _nameLabel.text = @"专属红包";
        _nameLabel.text = self.msg.content;
//        _headImageView.hidden = NO;
//        [g_server getHeadImageSmall:self.msg.toId userName:self.msg.toUserName imageView:_headImageView];
        _headImageView.image = [UIImage imageNamed:@"hongb_zs"];
        _title.text = [NSString stringWithFormat:@"%@专属红包",APP_NAME];
    }else if ([self.msg.fileName isEqualToString:@"2"]) {
        
        _nameLabel.text = self.msg.content;
        _title.text = [NSString stringWithFormat:@"%@%@",APP_NAME,Localized(@"JX_LuckGift")];
  
    
    }else if ([self.msg.fileName isEqualToString:@"1"]){
 
        _nameLabel.text = self.msg.content;
        _title.text = [NSString stringWithFormat:@"%@%@",APP_NAME,Localized(@"JX_UsualGift")];
   
    
    }else  {
        
        _nameLabel.text = self.msg.content;
        _title.text = [NSString stringWithFormat:@"%@%@",APP_NAME,Localized(@"JXredPacket")];
    }
    
    if ([self.msg.fileSize intValue] == 2) {
        _nameLabel.frame = CGRectMake(CGRectGetMaxX(_headImageView.frame) + 8,14, 180, 20);
        _titleLingQu.frame = CGRectMake(CGRectGetMaxX(_headImageView.frame) + 8,CGRectGetMaxY(_nameLabel.frame)+5, 100, 20);
        _imageBackground.alpha = 0.5;
        _titleLingQu.hidden = NO;
    }else {
        _nameLabel.frame = CGRectMake(CGRectGetMaxX(_headImageView.frame) + 8,22.5, 180, 20);
        _titleLingQu.frame = CGRectMake(CGRectGetMaxX(_headImageView.frame) + 8,CGRectGetMaxY(_nameLabel.frame)+5, 100, 20);
        _imageBackground.alpha = 1;
        _titleLingQu.hidden = YES;
    }

}

-(void)didTouch:(UIButton*)button{
    if ([self.msg.fileName isEqualToString:@"3"] || [self.msg.fileName isEqualToString:@"4"]) {
//        //如果可以打开
//        if([self.msg.fileSize intValue] != 2){
//            [g_App showAlert:Localized(@"JX_WantOpenGift")];
//            return;
//        }
        
        [g_notify postNotificationName:kcellRedPacketDidTouchNotifaction object:self.msg];
    }
    
    if ([self.msg.fileName isEqualToString:@"1"] || [self.msg.fileName isEqualToString:@"2"]) {
        //如果可以打开
//        if([self.msg.fileSize intValue] != 2){
            [g_notify postNotificationName:kcellRedPacketDidTouchNotifaction object:self.msg];
            return;
//        }
    }
    
//    [g_server getRedPacket:self.msg.objectId toView:self.chatView];
}

+ (float)getChatCellHeight:(JY_MessageObject *)msg {
    if ([g_App.isShowRedPacket intValue] == 1){
        if ([msg.chatMsgHeight floatValue] > 1) {
            return [msg.chatMsgHeight floatValue]-10 -5 ;
        }
        
        float n = 0;
        if (msg.isGroup && !msg.isMySend) {
            if (msg.isShowTime) {
                n = ManMan_SCREEN_WIDTH/3 + 10 + 40;
            }else {
                n = ManMan_SCREEN_WIDTH/3 + 10;
            }
            n += GROUP_CHAT_INSET;
        }else {
            if (msg.isShowTime) {
                n = ManMan_SCREEN_WIDTH/3 + 40;
            }else {
                n = ManMan_SCREEN_WIDTH/3;
            }
        }
        
        msg.chatMsgHeight = [NSString stringWithFormat:@"%f",n];
        if (!msg.isNotUpdateHeight) {
            [msg updateChatMsgHeight];
        }
        return n;
        
    }else{
        
        msg.chatMsgHeight = [NSString stringWithFormat:@"0"];
        if (!msg.isNotUpdateHeight) {
            [msg updateChatMsgHeight];
        }
        return 0;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
