//
//  JY_MessageCell.h
//  TFJunYouChat
//
//  Created by lifengye on 2020/09/10.
//  Copyright © 2020 zengwOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JY_BaseChatCell.h"
//添加Cell被长按的处理
#import "QBPlasticPopupMenu.h"

@interface JY_MessageCell : JY_BaseChatCell{
    
}
@property (nonatomic,strong) JY_Emoji * messageConent;
@property (nonatomic, strong) UILabel *timeIndexLabel;
@property (nonatomic, assign) NSInteger timerIndex;
@property (nonatomic, strong) NSTimer *readDelTimer;

@property (nonatomic, assign) BOOL isDidMsgCell;

@property (nonatomic, assign) BOOL isOnline;//是否是在线客服

- (void)deleteMsg:(JY_MessageObject *)msg;

@end
