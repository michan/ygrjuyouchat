//
//  JY_RedPacketCell.h
//  TFJunYouChat
//
//  Created by lifengye on 2020/09/10.
//  Copyright © 2020 zengwOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JY_BaseChatCell.h"
@class JY_ChatViewController;
@interface JY_RedPacketCell : JY_BaseChatCell

@property (nonatomic, strong) JY_ImageView* imageBackground;
@property (nonatomic,strong) JY_Emoji * redPacketGreet;

@property (nonatomic, strong) UILabel *titleLingQu;

@property (nonatomic,strong)UIView *lineView;
@end

