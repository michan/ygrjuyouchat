//
//  JY_MergeRelayCell.h
//  TFJunYouChat
//
//  Created by p on 2018/7/5.
//  Copyright © 2018年 Reese. All rights reserved.
//

#import "JY_BaseChatCell.h"

@interface JY_MergeRelayCell : JY_BaseChatCell

@property (nonatomic,strong) UIImageView * imageBackground;
@property (nonatomic,strong) UILabel * titleLabel;

@end
