//
//  JY_MsgStrCell.h
//  TFJunYouChat
//
//  Created by os on 2021/1/14.
//  Copyright © 2021 zengwOS. All rights reserved.
//

#import "JY_BaseChatCell.h"
#import "JY_BaseChatCell.h"
//添加Cell被长按的处理
#import "QBPlasticPopupMenu.h"
#import "FMLinkLabel.h"

NS_ASSUME_NONNULL_BEGIN

@interface JY_MsgStrCell : JY_BaseChatCell

//@property (nonatomic,strong) JY_Emoji * messageConent;

@property (nonatomic,strong) FMLinkLabel * messageConentMsg;

@property (nonatomic, strong) NSString *contentAswer;
@property (nonatomic, strong) NSArray *msgAnswerArr;

@property (nonatomic, strong) UILabel *timeIndexLabel;
@property (nonatomic, assign) NSInteger timerIndex;
@property (nonatomic, strong) NSTimer *readDelTimer;

@property (nonatomic, assign) BOOL isDidMsgCell;

- (void)deleteMsg:(JY_MessageObject *)msg;

@property (nonatomic, assign) BOOL isOnline;//是否是在线客服
@property (nonatomic, assign) CGFloat bgWidth;//背景宽度

@end

NS_ASSUME_NONNULL_END
