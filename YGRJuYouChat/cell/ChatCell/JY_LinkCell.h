//
//  JY_LinkCell.h
//  TFJunYouChat
//
//  Created by lifengye on 2020/8/17.
//  Copyright © 2020 zengwOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JY_BaseChatCell.h"

@interface JY_LinkCell : JY_BaseChatCell

@property (nonatomic,strong) UIImageView * imageBackground;
@property (nonatomic,strong) UILabel * nameLabel;
@property (nonatomic,strong) UIImageView *headImageView;

@end
