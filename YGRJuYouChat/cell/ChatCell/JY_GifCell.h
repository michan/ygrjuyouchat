//
//  JY_GifCell.h
//  TFJunYouChat
//
//  Created by lifengye on 2020/09/11.
//  Copyright © 2020 zengwOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JY_BaseChatCell.h"
#import "SCGIFImageView.h"
@interface JY_GifCell : JY_BaseChatCell
@property (nonatomic,strong) SCGIFImageView* gif;
@end
