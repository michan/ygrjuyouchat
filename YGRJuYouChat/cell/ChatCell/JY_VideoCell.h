//
//  JY_VideoCell.h
//  TFJunYouChat
//
//  Created by lifengye on 2020/09/10.
//  Copyright © 2020 zengwOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JY_BaseChatCell.h"
@class JY_VideoPlayer;

@protocol ManMan_VideoCellDelegate <NSObject>

- (void)showVideoPlayerWithTag:(NSInteger)tag;

@end


@interface JY_VideoCell : JY_BaseChatCell{
}
@property (nonatomic,strong) JY_ImageView * chatImage;
@property (nonatomic, strong) UIButton *pauseBtn;
//@property (nonatomic,assign) UIImage * videoImage;
@property (nonatomic,copy)   NSString *oldFileName;
@property (nonatomic, strong) JY_VideoPlayer *player;
@property (nonatomic, assign) NSInteger indexTag;
@property (nonatomic, assign) BOOL isEndVideo;
@property (nonatomic, strong) UILabel *videoProgress;

@property (nonatomic, assign) id<ManMan_VideoCellDelegate>videoDelegate;

- (void)timeGo:(NSString *)fileName;

// 看完视频后调用的方法
- (void)deleteMsg;


@end
