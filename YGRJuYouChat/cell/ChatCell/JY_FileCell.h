//
//  JY_FileCell.h
//  TFJunYouChat
//
//  Created by lifengye on 2020/09/10.
//  Copyright © 2020 zengwOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JY_BaseChatCell.h"
@interface JY_FileCell : JY_BaseChatCell
@property (nonatomic,strong) UIImageView * imageBackground;
@property (nonatomic,strong) UILabel * fileNameLabel;
@end
