//
//  JY_ShakeCell.h
//  TFJunYouChat
//
//  Created by p on 2018/5/30.
//  Copyright © 2018年 Reese. All rights reserved.
//

#import "JY_BaseChatCell.h"

@interface JY_ShakeCell : JY_BaseChatCell

@property (nonatomic, strong) UIImageView *shakeImageView;


@end
