//
//  JY_GroupMemberCell.m
//  TFJunYouChat
//
//  Created by lifengye on 2020/10/11.
//  Copyright © 2019 zengwOS. All rights reserved.
//

#import "JY_GroupMemberCell.h"

@implementation JY_GroupMemberCell
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 10, 50, 60)];
        _imageView.layer.cornerRadius = 5;
        _imageView.layer.masksToBounds = YES;
        [self.contentView addSubview:_imageView];
        
        
        _maskImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 62, 60)];
        [self.contentView insertSubview:_maskImageView aboveSubview:_imageView];
        
        
        _label = [[JY_Label alloc] initWithFrame:CGRectMake(0, 66, 50, 12)];
        _label.font = g_factory.font12;
        _label.textColor = HEXCOLOR(0x303030);
        _label.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_label];
    }
    return self;
}

- (void)buildNewImageview{
    if (_imageView) {
        [_imageView removeFromSuperview];
        _imageView = nil;
        _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 10, 50, 50)];
        _imageView.layer.cornerRadius = 5;
        _imageView.layer.masksToBounds = YES;
        [self.contentView addSubview:_imageView];
    }
    
    
    if (_maskImageView) {
        [_maskImageView removeFromSuperview];
        _maskImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 62, 60)];
        [self.contentView insertSubview:_maskImageView aboveSubview:_imageView];
    }

}

@end
