//
//  JY_AddressBookCell.h
//  TFJunYouChat
//
//  Created by lifengye on 2020/09/30.
//  Copyright © 2018年 Reese. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QCheckBox.h"
#import "JY_AddressBook.h"

@class JY_AddressBookCell;
@protocol JY_AddressBookCellDelegate <NSObject>

- (void)addressBookCell:(JY_AddressBookCell *)abCell checkBoxSelectIndexNum:(NSInteger)indexNum isSelect:(BOOL)isSelect;
- (void)addressBookCell:(JY_AddressBookCell *)abCell addBtnAction:(JY_AddressBook *)addressBook;

@end

@interface JY_AddressBookCell : UITableViewCell <QCheckBoxDelegate>

@property (nonatomic, strong) JY_ImageView *headImage;
@property (nonatomic, strong) UILabel *name;
@property (nonatomic, strong) UILabel *nickName;
@property (nonatomic, strong) QCheckBox *checkBox;
@property (nonatomic, strong) UIButton *addBtn;
@property (nonatomic, assign) NSInteger index;
@property (nonatomic, assign) BOOL isShowSelect;
@property (nonatomic, strong) UIView *lineView;

@property (nonatomic, weak) id<JY_AddressBookCellDelegate>delegate;

@property (nonatomic, strong) JY_AddressBook *addressBook;

@property (nonatomic, assign) BOOL isInvite;

@end
