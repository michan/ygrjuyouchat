//
//  JY_MediaCell.h
//
//  Created by lifengye on 2020/09/03.
//  Copyright (c) 2014年 Reese. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JY_VideoPlayer.h"

@class Jy_MediaObject;

@interface JY_MediaCell : UITableViewCell{
    UIImageView* bageImage;
    UILabel* bageNumber;
    JY_VideoPlayer* _player;
}
@property (nonatomic,strong) UIButton* pauseBtn;
@property (nonatomic,strong) UIImageView* head;
@property (nonatomic,strong) NSString*  bage;
@property (nonatomic,strong) Jy_MediaObject* media;
@property (nonatomic,weak) id delegate;
@end
