//
//  JY_SearchImageLogCell.h
//  TFJunYouChat
//
//  Created by p on 2019/4/9.
//  Copyright © 2019年 Reese. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface JY_SearchImageLogCell : UICollectionViewCell

@property (nonatomic, strong) JY_MessageObject *msg;

@end

NS_ASSUME_NONNULL_END
