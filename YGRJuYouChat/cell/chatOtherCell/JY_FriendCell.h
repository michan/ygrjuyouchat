//
//  JY_FriendCell.h
//
//  Created by lifengye on 2020/09/03.
//  Copyright (c) 2014年 Reese. All rights reserved.
//

#import <UIKit/UIKit.h>
@class JY_FriendObject;
@class JY_FriendCell;

@protocol ManMan_FriendCellDelegate <NSObject>

- (void) friendCell:(JY_FriendCell *)friendCell headImageAction:(NSString *)userId;

@end

@interface JY_FriendCell : UITableViewCell{
    UIImageView* bageImage;
    UILabel* bageNumber;
    UIButton* _btn2;
    UIButton* _btn1;
    UILabel* _lbSubtitle;
    UILabel* _timeLab;
}
@property (nonatomic,strong) NSString*  title;
@property (nonatomic,strong) NSString*  subtitle;
@property (nonatomic,strong) NSString*  rightTitle;
@property (nonatomic,strong) NSString*  bottomTitle;
@property (nonatomic,strong) NSString*  headImage;
@property (nonatomic,strong) NSString*  bage;
@property (nonatomic,strong) JY_FriendObject* user;
@property (nonatomic,strong) id target;
@property (nonatomic, weak) id<ManMan_FriendCellDelegate>delegate;

@property (nonatomic, strong) UIView* lineView;

-(void)update;

@end
