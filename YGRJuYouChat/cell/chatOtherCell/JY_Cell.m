//
//  JY_Cell.m
//
//  Created by lifengye on 2020/09/03.
//  Copyright (c) 2014年 Reese. All rights reserved.
//

#import "JY_Cell.h"
#import "JY_Label.h"
#import "JY_ImageView.h"
#import "AppDelegate.h"

#define HEAD_WH 45

@implementation JY_Cell
@synthesize title,bottomTitle,headImage,bage,userId;
@synthesize index,delegate,didTouch,lbTitle,lbBottomTitle,lbSubTitle;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){

//        self.contentView.layer.cornerRadius = 8;
//        self.contentView.layer.masksToBounds = YES;
        self.selectionStyle  = UITableViewCellSelectionStyleBlue;
        //内容
        UIFont* f0 = [UIFont systemFontOfSize:13];
        //名称
        UIFont * f1 = [UIFont systemFontOfSize:15];
        //时间
        UIFont* timeFont = [UIFont systemFontOfSize:12];
        
        int n = 68;
        UIView* v = [[UIView alloc]initWithFrame:CGRectMake(0,0, ManMan_SCREEN_WIDTH, n)];
        v.backgroundColor = THE_LINE_COLOR;
        self.selectedBackgroundView = v; 
        
        self.lineView = [[UIView alloc]initWithFrame:CGRectMake(SEPSRATOR_WIDTH,n-LINE_WH,ManMan_SCREEN_WIDTH,LINE_WH)];
       // self.lineView.backgroundColor = THE_LINE_COLOR;
        [self.contentView addSubview:self.lineView];
        
        _delBtn = [[UIButton alloc] initWithFrame:CGRectMake(10, 22, 20, 20)];
        [_delBtn setBackgroundImage:[UIImage imageNamed:@"delete"] forState:UIControlStateNormal];
        [_delBtn addTarget:self action:@selector(delBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        _delBtn.hidden = YES;
        [self.contentView addSubview:_delBtn];
        
        
        CGFloat height = self.frame.size.height;
        
        _headImageView = [[JY_ImageView alloc]init];
        _headImageView.userInteractionEnabled = NO;
        _headImageView.tag = index;
        _headImageView.delegate = self;
        _headImageView.didTouch = @selector(headImageDidTouch);
        if (_homeHeight) {
            _headImageView.frame = CGRectMake(15,(_homeHeight-HEAD_WH)/2,HEAD_WH,HEAD_WH);
        }else{
            _headImageView.frame = CGRectMake(15,7.5,HEAD_WH,HEAD_WH);
        }
        _headImageView.layer.cornerRadius = 5;
        _headImageView.layer.masksToBounds = YES;
        _headImageView.layer.borderColor = [UIColor darkGrayColor].CGColor;
        
        [self.contentView addSubview:self.headImageView];
//        [_headImageView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.mas_equalTo(self).offset(15);
//            make.width.height.mas_equalTo(HEAD_WH);
//            make.centerY.mas_equalTo(self.mas_centerY);
//        }];
        
        [g_notify addObserver:self selector:@selector(headImageNotification:) name:kGroupHeadImageModifyNotifaction object:nil];
        
        
        //昵称Label
        JY_Label* lb;
         
        
        _guanFTitle = [[UILabel alloc] init];
        _guanFTitle.textColor = [UIColor whiteColor];
        _guanFTitle.font = KFontMedium(12);
//        SYSFONT(11);
        _guanFTitle.layer.cornerRadius = 8;
        _guanFTitle.layer.masksToBounds = YES;
        _guanFTitle.text = @"官方";
        _guanFTitle.textAlignment = NSTextAlignmentCenter;
        _guanFTitle.tag = self.index;
        [self.contentView addSubview:_guanFTitle];
        CGSize size1 = [self autoMakeColorfulLabel:_guanFTitle font:SYSFONT(10) text: _guanFTitle.text];
        _guanFTitle.frame=CGRectMake(CGRectGetMaxX(_headImageView.frame)+15, 14, size1.width, size1.height);
        
        
        lb = [[JY_Label alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_guanFTitle.frame)+5, 14, ManMan_SCREEN_WIDTH - 115 -CGRectGetMaxX(_headImageView.frame)-14, 18)];
        lb.textColor = HEXCOLOR(0x323232);
        lb.userInteractionEnabled = NO;
        lb.backgroundColor = [UIColor clearColor];
        lb.font = f1;
        lb.tag = self.index;
        [self.contentView addSubview:lb];
        [lb setText:self.title];
        self.lbTitle = lb;
        
        
        
        _positionLabel = [UIFactory createLabelWith:CGRectMake(CGRectGetMaxX(self.lbTitle.frame)+2, CGRectGetMinY(self.lbTitle.frame), 20, 20) text:@"" font:g_factory.font11 textColor:[UIColor whiteColor] backgroundColor:nil];
        _positionLabel.layer.backgroundColor = [UIColor orangeColor].CGColor;
        _positionLabel.layer.cornerRadius = 5;
        _positionLabel.textAlignment = NSTextAlignmentCenter;
        _positionLabel.hidden = YES;
        [self.contentView addSubview:_positionLabel];
        if (self.positionTitle.length > 0){
            self.positionTitle = self.positionTitle;
        }
        
        //聊天消息Label
        lb = [[JY_Label alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_headImageView.frame)+15, 68-15-15, ManMan_SCREEN_WIDTH-86-50, 15)];
        lb.textColor = [UIColor lightGrayColor];
        lb.userInteractionEnabled = NO;
        lb.backgroundColor = [UIColor clearColor];
        lb.font = f0;
        [self.contentView addSubview:lb];
        [lb setText:self.subtitle];
        self.lbSubTitle = lb;
        
        //时间Label
        self.timeLabel = [[JY_Label alloc]initWithFrame:CGRectMake(ManMan_SCREEN_WIDTH - 115-15, 13, 115, 12)];
        self.timeLabel.textColor = [UIColor lightGrayColor];
        self.timeLabel.userInteractionEnabled = NO;
        self.timeLabel.backgroundColor = [UIColor clearColor];
        self.timeLabel.textAlignment = NSTextAlignmentRight;
        self.timeLabel.font = timeFont;
        [self.contentView addSubview:self.timeLabel];
        [self.timeLabel setText:self.bottomTitle];
        self.lbBottomTitle = self.timeLabel;
        
        [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self).offset(-15);
//            make.width.mas_equalTo(115);
//            make.height.mas_equalTo(12);
            make.centerY.mas_equalTo(self.mas_centerY);
        }];
        
        //快捷回复
        self.replayView = [[JY_ImageView alloc] initWithFrame:CGRectMake(ManMan_SCREEN_WIDTH-45, 13, 45, 45)];
        self.replayView.hidden = YES;
        self.replayView.didTouch = @selector(didQuickReply);
        self.replayView.delegate = self;
        [self.contentView addSubview:self.replayView];
        _replayImgV = [[UIImageView alloc] initWithFrame:CGRectMake(15, 25, 15, 15)];
        _replayImgV.image = [UIImage imageNamed:@"msg_replay_icon"];
//        [self.replayView addSubview:_replayImgV];
        
        //免打扰图标
        self.notPushImageView = [[JY_ImageView alloc] initWithFrame:CGRectMake(ManMan_SCREEN_WIDTH-15*3-10, CGRectGetMinY(lbSubTitle.frame), 15, 15)];
        self.notPushImageView.image = [UIImage imageNamed:@"msg_not_push"];
        self.notPushImageView.hidden = YES;
        [self.contentView addSubview:self.notPushImageView];

        _bageNumber  = [[JY_BadgeView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_headImageView.frame) - 9.5, CGRectGetMinY(_headImageView.frame) - 6.5, 19, 19)];
        
        _bageNumber.delegate = delegate;
        _bageNumber.didDragout = self.didDragout;
        _bageNumber.userInteractionEnabled = YES;
        _bageNumber.lb.font = SYSFONT(12);
//        [self.replayView addSubview:_bageNumber];
        [self.contentView addSubview:_bageNumber];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didQuickReply)];
        [_bageNumber addGestureRecognizer:tap];

        self.bage = bage;
        
        
        self.redView = [[UIView alloc]init];
        [self.contentView addSubview:self.redView];
        self.redView.frame = CGRectMake(CGRectGetMaxX(_headImageView.frame) - 9.5, CGRectGetMinY(_headImageView.frame) - 6.5, 10, 10);
        self.redView.hidden = YES;
        self.redView.backgroundColor = [UIColor redColor];
        ViewRadius(self.redView, 5);
        
//        [self saveBadge:bage withTitle:self.title];
    }
    return self;
}

- (void)delBtnAction:(UIButton *)btn {
    if (self.delegate && [self.delegate respondsToSelector:self.didDelMsg]) {
        [self.delegate performSelectorOnMainThread:self.didDelMsg withObject:self waitUntilDone:YES];
    }
}

-(void)dealloc{
//    NSLog(@"JY_Cell.dealloc");
//    [self.bageDict removeAllObjects];
//    self.bageDict = nil;
    
    [g_notify removeObserver:self name:kGroupHeadImageModifyNotifaction object:nil];
    self.title = nil;
    self.subtitle = nil;
    self.bottomTitle = nil;
    self.headImage = nil;
    self.bage = nil;
    self.userId = nil;
    
    self.lbSubTitle = nil;
    self.lbTitle = nil;
    self.lbBottomTitle = nil;
//    self.bageDict = nil;
//    [_headImageView release];
//    [super dealloc];
}


- (void)setIsNotPush:(BOOL)isNotPush {
    _isNotPush = isNotPush;
    self.notPushImageView.hidden = !isNotPush;
}

- (void)setIsMsgVCCome:(BOOL)isMsgVCCome { // 只有ManMan_MsgViewController显示回复按钮
    _isMsgVCCome = isMsgVCCome;
    self.replayView.hidden = !isMsgVCCome;
    // 这里获取需要userid   一定要在cell赋值userid 之后再调用
    //
    _replayImgV.alpha = 1-([self.userId intValue] == [SHIKU_TRANSFER intValue]);
    
}


- (void)didQuickReply {
    if ([self.userId intValue] == [SHIKU_TRANSFER intValue]) {
        return;
    }
    if (self.delegate && [self.delegate respondsToSelector:self.didReplay]) {
        [self.delegate performSelectorOnMainThread:self.didReplay withObject:self waitUntilDone:YES];
    }
}
 

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)headImageDidTouch{
    if (self.delegate && [self.delegate respondsToSelector:didTouch]) {
        [self.delegate performSelectorOnMainThread:didTouch withObject:self.dataObj waitUntilDone:YES];
    }
}
- (void)getHeadImage{
    if(headImage){
        if([headImage rangeOfString:@"http://"].location == NSNotFound)
            self.headImageView.image = [UIImage imageNamed:headImage];
        else
            [g_server getImage:headImage imageView:self.headImageView];
    }
    [g_server getHeadImageSmall:userId userName:self.title imageView:self.headImageView];
}

-(void)setBage:(NSString *)s{
//    bageNumber.hidden = [s intValue]<=0;
    _replayImgV.hidden = [s intValue] > 0;
    _bageNumber.badgeString = s;
    if ([s intValue] >= 10 && [s intValue] <= 99) {
        _bageNumber.lb.font = SYSFONT(12);
    }else if ([s intValue] > 0 && [s intValue] < 10) {
        _bageNumber.lb.font = SYSFONT(13);
    }else if([s intValue] > 99){
        _bageNumber.badgeString = @"99+";
        _bageNumber.lb.font = SYSFONT(9);
    }
    bage = s;
}

-(void)setForTimeLabel:(NSString *)s{
    self.bottomTitle = s;
    
    self.timeLabel.text = s;
}

-(void)setTitle:(NSString *)s{
    
    NSString *userTypeStr = [NSString stringWithFormat:@"%@",_user.userType];
    if ([userTypeStr intValue]==2) {
       _guanFTitle.hidden = NO;
        
         self.lbTitle.textColor = HEXCOLOR(0xFD7B5D);
    }else{
        self.lbTitle.textColor = HEXCOLOR(0x323232);
         
        _guanFTitle.hidden = YES;
    }
    
   // NSLog(@"userId == %@ s = %@",userId,s);
    
    title = s;
    self.lbTitle.text = s;
}
-(void)setPositionTitle:(NSString *)positionTitle{
    _positionTitle = positionTitle;
    if (positionTitle.length > 0) {
        _positionLabel.text = positionTitle;
        _positionLabel.hidden = NO;
        CGSize positionSize =[positionTitle sizeWithAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:11]}];
        if (positionSize.width >150)
            positionSize.width = 150;
        CGSize titleSize = [self.lbTitle.text sizeWithAttributes:@{NSFontAttributeName: self.lbTitle.font}];
        _positionLabel.frame = CGRectMake(self.lbTitle.frame.origin.x + titleSize.width + 2, CGRectGetMidY(self.lbTitle.frame) + 0, positionSize.width+4, positionSize.height);
//        _positionLabel.center = CGPointMake(_positionLabel.center.x, self.lbTitle.center.y);
        if ([_positionTitle isEqualToString:@"禁抢红包"]) {
            _positionLabel.layer.backgroundColor = [UIColor redColor].CGColor;
        }else {
            _positionLabel.layer.backgroundColor = THEMECOLOR.CGColor;
        }
    }
}

- (void)setSuLabel:(NSString *)s{
//    subtitle = [s retain];
    _subtitle = s;
    self.lbSubTitle.attributedText = [self setContentLabelStr:s];
}
-(void)setSubtitle:(NSString *)subtitle{
    _subtitle = subtitle;
    self.lbSubTitle.attributedText = [self setContentLabelStr:subtitle];
}
- (void)getMessageRange:(NSString*)message :(NSMutableArray*)array {

    if ([message isKindOfClass:[NSNull class]]) {
         
        
        [array addObject:@""];
        return;
    }
    NSRange range=[message rangeOfString: @"["];

    NSRange range1=[message rangeOfString: @"]"];


    // 动画过滤
    if ([message isEqualToString:[NSString stringWithFormat:@"[%@]",Localized(@"emojiVC_Emoji")]]) {
        [array addObject:message];
        return;
    }


    //判断当前字符串是否还有表情的标志。

    if (range.length>0 && range1.length>0 && range1.location > range.location) {

        if (range.location > 0) {

            NSString *str = [message substringToIndex:range.location];

            NSString *str1 = [message substringFromIndex:range.location];

            [array addObject:str];

            [self getMessageRange:str1 :array];

        }else {

            NSString *emojiString = [message substringWithRange:NSMakeRange(range.location + 1, range1.location - 1)];
            BOOL isEmoji = NO;
            NSString *str;
            NSString *str1;
            for (NSMutableDictionary *dic in g_constant.emojiArray) {
                NSString *emoji = [dic objectForKey:@"english"];
                if ([emoji isEqualToString:emojiString]) {
                    isEmoji = YES;
                    break;
                }
            }
            if (isEmoji) {
                str = [message substringWithRange:NSMakeRange(range.location, range1.location + 1)];
                str1 = [message substringFromIndex:range1.location + 1];
                [array addObject:str];
            }else{
                NSString *posString = [message substringWithRange:NSMakeRange(range.location + 1, range1.location)];
                NSRange posRange = [posString rangeOfString:@"["];
                if (posRange.location != NSNotFound) {
                    str = [message substringToIndex:posRange.location + 1];
                    str1 = [message substringFromIndex:posRange.location + 1];
                    [array addObject:str];
                }else{
                    str = [message substringToIndex:range1.location + 1];
                    str1 = [message substringFromIndex:range1.location + 1];
                    [array addObject:str];
                }
            }
            [self getMessageRange:str1 :array];
        }

    }else if (range.length>0 && range1.length>0 && range1.location < range.location){
        NSString *str = [message substringToIndex:range1.location + 1];
        NSString *str1 = [message substringFromIndex:range1.location + 1];
        [array addObject:str];
        [self getMessageRange:str1 :array];
    }else if (message != nil) {

        [array addObject:message];

    }

}
- (NSAttributedString *) setContentLabelStr:(NSString *) str {
    NSMutableArray *contentArray = [NSMutableArray array];
    
    [self getMessageRange:str :contentArray];
    
    NSMutableAttributedString *strM = [[NSMutableAttributedString alloc] init];
    
    NSInteger count = contentArray.count;
    if (contentArray.count > 15) {
        count = 15;
    }
    
    for (NSInteger i = 0; i < count; i ++) {
        
        NSString *object = contentArray[i];
        
//        NSLog(@"%@",object);
        BOOL flag = NO;
        if ([object hasSuffix:@"]"]&&[object hasPrefix:@"["]) {
            
            //如果是表情用iOS中附件代替string在label上显示
            
            NSTextAttachment *imageStr = [[NSTextAttachment alloc]init];
            NSString *imageShortName = [object substringWithRange:NSMakeRange(1, object.length - 2)];
            for (NSInteger i = 0; i < g_constant.emojiArray.count; i ++) {
                NSDictionary *dict = g_constant.emojiArray[i];
                NSString *imageName = dict[@"english"];
                if ([imageName isEqualToString:imageShortName]) {
                    imageStr.image = [UIImage imageNamed:dict[@"filename"]];
                    flag = YES;
                    break;                }
            }
            if (!flag) {
                [strM appendAttributedString:[[NSAttributedString alloc] initWithString:object]];
                
                NSRange range = [object rangeOfString:Localized(@"JX_Draft")];
                [strM addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:range];
                
                range = [object rangeOfString:Localized(@"JX_Someone@Me")];
                [strM addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:range];
                continue;
            }
            //            imageStr.image = [UIImage imageNamed:[object substringWithRange:NSMakeRange(1, object.length - 2)]];
            
            //这里对图片的大小进行设置一般来说等于文字的高度
            
            CGFloat height = self.lbSubTitle.font.lineHeight + 1;
            
            imageStr.bounds = CGRectMake(0, -4, height, height);
            
            NSAttributedString *attrString = [NSAttributedString attributedStringWithAttachment:imageStr];
            
            [strM appendAttributedString:attrString];
            
        }else{
            
            //如果不是表情直接进行拼接

            [strM appendAttributedString:[[NSAttributedString alloc] initWithString:object]];
            
            NSRange range = [object rangeOfString:Localized(@"JX_Draft")];
            [strM addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:range];
            
            range = [object rangeOfString:Localized(@"JX_Someone@Me")];
            [strM addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:range];
        }
        
    }
    
    return strM;
}


-(void)headImageNotification:(NSNotification *)notification{
    NSDictionary * groupDict = notification.object;

    NSString * roomJid = groupDict[@"roomJid"];
    if ([roomJid isEqualToString:self.userId]) {
        UIImage * hImage = groupDict[@"groupHeadImage"];
        self.headImageView.image = hImage;
    }
}

-(void)headImageViewImageWithUserId:(NSString *)userId roomId:(NSString *)roomIdStr {

    if (roomIdStr != nil) {
//        if (![g_server getRoomHeadImageSmall:self.userId imageView:self.headImageView]) {
//            NSString *groupImagePath = [NSString stringWithFormat:@"%@%@/%@.%@",NSTemporaryDirectory(),g_myself.userId,roomIdStr,@"jpg"];
//            if (groupImagePath && [[NSFileManager defaultManager] fileExistsAtPath:groupImagePath]) {
//                self.headImageView.image = [UIImage imageWithContentsOfFile:groupImagePath];
//            }else{
//                [roomData roomHeadImageRoomId:roomIdStr toView:self.headImageView];
//            }
        [g_server getRoomHeadImageSmall:userId roomId:roomIdStr imageView:self.headImageView];
//        }
    }else{
        if(headImage){
            if([headImage rangeOfString:@"http://"].location == NSNotFound)
                self.headImageView.image = [UIImage imageNamed:headImage];
            else
                [g_server getImage:headImage imageView:self.headImageView];
        }
        [g_server getHeadImageSmall:self.userId userName:self.title imageView:self.headImageView];
    }
}

- (void)setIsSmall:(BOOL)isSmall {
    _isSmall = isSmall;
    
    CGFloat headX = 14;
    self.delBtn.hidden = YES;
    if (self.isEdit) {
        self.delBtn.hidden = NO;
        headX = CGRectGetMaxX(_delBtn.frame) + 10;
    }
    NSString *userTypeStr = [NSString stringWithFormat:@"%@",_user.userType];
    if (!isSmall) {
        _headImageView.frame = CGRectMake(headX,7.5,HEAD_WH,HEAD_WH);
        
        if ([userTypeStr intValue]==2) {
            
           self.lbTitle.frame = CGRectMake(CGRectGetMaxX(_guanFTitle.frame)+5, 14, ManMan_SCREEN_WIDTH - 115 -CGRectGetMaxX(_guanFTitle.frame)-14, 18);
        }else{
           self.lbTitle.frame = CGRectMake(CGRectGetMaxX(_headImageView.frame)+15, 15, ManMan_SCREEN_WIDTH - 115 -CGRectGetMaxX(_headImageView.frame)-14, 18);
        }
        
        
        self.lbSubTitle.frame = CGRectMake(CGRectGetMaxX(_headImageView.frame)+15, self.lbSubTitle.frame.origin.y, ManMan_SCREEN_WIDTH - 55 -CGRectGetMaxX(_headImageView.frame)-14, self.lbSubTitle.frame.size.height);
        self.lineView.frame = CGRectMake(CGRectGetMaxX(_headImageView.frame)+15,68-LINE_WH,ManMan_SCREEN_WIDTH,LINE_WH);
    }else {
        
        _headImageView.frame = CGRectMake(headX,7.5,HEAD_WH,HEAD_WH);
        _headImageView.layer.cornerRadius = 5;
      
        
        if ([userTypeStr intValue]==2) {
            
           self.lbTitle.frame = CGRectMake(CGRectGetMaxX(_guanFTitle.frame)+5, 14, ManMan_SCREEN_WIDTH - 115 -CGRectGetMaxX(_guanFTitle.frame)-14, 18);
        }else{
            self.lbTitle.frame = CGRectMake(CGRectGetMaxX(_headImageView.frame)+15, 21.5, ManMan_SCREEN_WIDTH - 115 -CGRectGetMaxX(_headImageView.frame)-14, 18);
           
        }
        
        
        self.lbSubTitle.frame = CGRectMake(CGRectGetMaxX(_headImageView.frame)+15, self.lbSubTitle.frame.origin.y, ManMan_SCREEN_WIDTH - 55 -CGRectGetMaxX(_headImageView.frame)-14, self.lbSubTitle.frame.size.height);
        self.lineView.frame = CGRectMake(CGRectGetMaxX(_headImageView.frame)+15,59-LINE_WH,ManMan_SCREEN_WIDTH,LINE_WH);
    }
}

-(void)setFrame:(CGRect)frame{
    
    frame.origin.y += 1;
    frame.size.height-=1;
//    frame.origin.x =15;
//    frame.size.width = SCREEN_WIDTH - 30;
    
    [super setFrame:frame];
    
}

- (void)setHomeHeight:(CGFloat)homeHeight{
    _homeHeight =  homeHeight;
    _headImageView.frame = CGRectMake(15, (homeHeight-HEAD_WH)/2, HEAD_WH, HEAD_WH);
}
- (CGSize)autoMakeColorfulLabel:(UILabel *)label font:(UIFont *)font text:(NSString *)text {
    NSString *name = text;
    if (name == nil) {
        name = @"";
    }
    CGFloat width = [UILabel getWidthWithTitle:name font:font];
    CGFloat height = [UILabel getHeightByWidth:width title:name font:font]+4;
    
    UIColor *leftColor = HEXCOLOR(0x4C7CFF);
    UIColor *rightColor = HEXCOLOR(0x00DDFF);
    UIImage *bgImg = [UIImage gradientColorImageFromColors:@[leftColor, rightColor] gradientType:GradientTypeLeftToRight imgSize:CGSizeMake(width+10, height+2)];
    [label setBackgroundColor:[UIColor colorWithPatternImage:bgImg]];
    label.layer.cornerRadius = height/2.0;
    return CGSizeMake(width+10, height+2);
}
@end
