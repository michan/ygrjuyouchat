//
//  JY_CourseListCell.h
//  TFJunYouChat
//
//  Created by lifengye on 2020/10/20.
//  Copyright © 2020 zengwOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JY_CourseListVC.h"

typedef int(^ManMan_CourseListCellBlock)(int type);

@interface JY_CourseListCell : UITableViewCell

@property (nonatomic, assign) NSInteger index;
@property (nonatomic, weak) JY_CourseListVC *vc;
@property (nonatomic, assign) ManMan_CourseListCellBlock block;
@property (nonatomic, assign) BOOL isMultiselect;
@property (nonatomic, assign) NSInteger indexNum;

@property (nonatomic, strong) UIButton *multiselectBtn;

- (void) setData:(NSDictionary *)dict;

@end
