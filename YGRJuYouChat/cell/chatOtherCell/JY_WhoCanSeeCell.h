//
//  JY_WhoCanSeeCell.h
//  TFJunYouChat
//
//  Created by p on 2018/6/27.
//  Copyright © 2018年 Reese. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JY_WhoCanSeeCell;
@protocol ManMan_WhoCanSeeCellDelegate <NSObject>

- (void)whoCanSeeCell:(JY_WhoCanSeeCell *)whoCanSeeCell selectAction:(NSInteger)index;
- (void)whoCanSeeCell:(JY_WhoCanSeeCell *)whoCanSeeCell editBtnAction:(NSInteger)index;

@end

@interface JY_WhoCanSeeCell : UITableViewCell
@property (nonatomic, strong) UIButton *contentBtn;
@property (nonatomic, strong) UIImageView *selImageView;
@property (nonatomic, strong) JY_Label *title;
@property (nonatomic, strong) JY_Label *userNames;
@property (nonatomic, strong) UIButton *editBtn;

@property (nonatomic, weak) id<ManMan_WhoCanSeeCellDelegate>delegate;
@property (nonatomic, assign) NSInteger index;
@end
