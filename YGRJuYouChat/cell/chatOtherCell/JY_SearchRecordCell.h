//
//  JY_SearchRecordCell.h
//  TFJunYouChat
//
//  Created by lifengye on 2020/9/6.
//  Copyright © 2019 zengwOS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class JY_SearchRecordCell;
@protocol ManMan_SearchRecordCellDelegate <NSObject>

- (void)deleteCell:(JY_SearchRecordCell *)cell;

@end


@interface JY_SearchRecordCell : UITableViewCell
@property (nonatomic,strong)UIButton *deleteBtn;
@property (nonatomic,weak)id<ManMan_SearchRecordCellDelegate> delegate;
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;
@end

NS_ASSUME_NONNULL_END
