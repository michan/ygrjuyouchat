//
//  JY_GroupHelperCell.h
//  TFJunYouChat
//
//  Created by 1 on 2019/5/29.
//  Copyright © 2019年 Reese. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JY_HelperModel.h"

NS_ASSUME_NONNULL_BEGIN


@class JY_GroupHelperCell;

@protocol ManMan_GroupHelperCellDelegate <NSObject>

- (void)groupHelperCell:(JY_GroupHelperCell *)cell clickAddBtnWithIndex:(NSInteger)index;

@end


@interface JY_GroupHelperCell : UITableViewCell

@property (nonatomic, strong) UIImageView *imageV;
@property (nonatomic, strong) UILabel *title;
@property (nonatomic, strong) UILabel *subTitle;
@property (nonatomic, strong) UIButton *addBtn;
@property (nonatomic, strong) NSArray *groupHelperArr;

@property (weak, nonatomic) id <ManMan_GroupHelperCellDelegate>delegate;


- (void)setDataWithModel:(JY_HelperModel *)model;


@end

NS_ASSUME_NONNULL_END
