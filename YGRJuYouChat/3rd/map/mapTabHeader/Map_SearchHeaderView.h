//
//  Map_SearchHeaderView.h
//  TFJunYouChat
//
//  Created by Qian on 2021/11/8.
//  Copyright © 2021 zengwOS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Map_SearchHeaderView : UIView
@property(nonatomic, assign) BOOL hiddenXiaHuaView;

@property (nonatomic, copy) void (^didHiddenTopClickBlock)(void);
@property (nonatomic, copy) void (^didShowTopClickBlock)(void);
@property (nonatomic, copy) void (^searchBlock)(NSString *searchStr);
+(Map_SearchHeaderView *)instanceHeaerView;
@end

NS_ASSUME_NONNULL_END
