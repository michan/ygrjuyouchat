//
//  Map_SearchHeaderView.m
//  TFJunYouChat
//
//  Created by Qian on 2021/11/8.
//  Copyright © 2021 zengwOS. All rights reserved.
//

#import "Map_SearchHeaderView.h"

@interface Map_SearchHeaderView ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UITextField *tf;
@property (weak, nonatomic) IBOutlet UIView *suView;

@end

@implementation Map_SearchHeaderView
-(void)awakeFromNib{
    [super awakeFromNib];
    ViewRadius(self.tf, 5);
    self.tf.backgroundColor = HEXCOLOR(0xF0F0F0);
    ViewRadius(self.suView,10);
    self.tf.delegate = self;
    
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if (self.didShowTopClickBlock) {
        self.didShowTopClickBlock();
    }
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField.text.length>0) {
        if (self.searchBlock) {
            self.searchBlock(textField.text);
        }
    }
    return YES;    //如果NO就不会显示文本内容
}
- (void)setHiddenXiaHuaView:(BOOL)hiddenXiaHuaView{
    _hiddenXiaHuaView = hiddenXiaHuaView;
    _topView.hidden =hiddenXiaHuaView;
    if (hiddenXiaHuaView) {
        [self.tf resignFirstResponder];
    }
   
}
+(Map_SearchHeaderView *)instanceHeaerView
{
    return  [[[NSBundle mainBundle] loadNibNamed:@"Map_SearchHeaderView" owner:nil options:nil] firstObject];
}
- (IBAction)xiahua:(id)sender {
    if (self.didHiddenTopClickBlock) {
        self.didHiddenTopClickBlock();
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
