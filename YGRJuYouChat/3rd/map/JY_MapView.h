




#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@protocol ManMan_MapViewDelegate;
@interface JY_MapView : UIView

@property (nonatomic,strong)MKMapView *mapView;
@property (nonatomic,assign)double span;//default 40000

- (id)initWithDelegate:(id<ManMan_MapViewDelegate>)delegate;
- (void)beginLoad;
@end


@protocol ManMan_MapViewDelegate <NSObject>

- (NSInteger)numbersWithCalloutViewForMapView;
- (CLLocationCoordinate2D)coordinateForMapViewWithIndex:(NSInteger)index;
- (UIView *)mapViewCalloutContentViewWithIndex:(NSInteger)index;
- (UIImage *)baseMKAnnotationViewImageWithIndex:(NSInteger)index;

@optional
- (void)calloutViewDidSelectedWithIndex:(NSInteger)index;

@end
