请先按照实人认证客户端SDK集成文档，[安卓SDK集成](https://help.aliyun.com/document_detail/127598.html)、[iOS SDK集成](https://help.aliyun.com/document_detail/127602.html)，进行集成，再配合该文档进行UI定制操作。
<a name="Mjs12"></a>
### 可定制的UI内容

<br />以下UI定制功能仅适用于实人认证iOS SDK>=4.6.2版本 和 安卓SDK>=4.6.0版本。<br />可定制的内容主要包括：控件颜色、文案颜色、文案字号、图片资源等。具体可以参考UI定制内容详细说明<br />![](https://intranetproxy.alipay.com/skylark/lark/0/2021/png/21189/1617897855773-b3630396-cebd-4238-824e-4db5a01e787e.png#from=paste&height=607&id=ue58b740c&margin=%5Bobject%20Object%5D&originHeight=1214&originWidth=1948&originalType=binary&ratio=1&size=1372906&status=done&style=none&taskId=ufd3f837a-20a7-4cd5-9cea-6a8a529cdbd&width=974)<br />

<a name="YJcAQ"></a>
#### 基础术语
1、控件： 用来渲染视图的最小单位，换肤方案中可更换皮肤的最小元素<br />2、组件：N 个控件组成复杂的组件<br />3、页面：类似命名空间（一般以 viewcontroller 或者 activity 来划分）<br />

<a name="P34hT"></a>
#### 支持的控件
目前实人认证支持的自定义控件如下：

- button：按钮组件，支持按钮背景色、背景图片、文案颜色、文案字号、文本内边距。
```json
"button": {
    "backgroundColor": "#00FFFFFF", //按钮背景色
    "backgroundImage": "face_confirm_button@2x.png", //按钮背景图片，生效优先级高于背景色
    "textColor": "#FFFFFF", //文案颜色
    "fontSize": 16, //文案字号
    "textPadding": { //文本内边距
      "left": 0,  //
      "right": 0,  //
      "top": 0,  //
      "bottom": 0  //
  	}
}
```

- text: 文本组件，支持文案字号、文案颜色配置。
```json
"text": {
    "textColor": "#000000", //文案颜色
    "fontSize": 36  //文案字号
}
```

- alertDialog： 通用对话弹窗，这是一个复合的组件。
```json
"alertDialog": {
    "positiveText": { //确认按钮的文案，包括文案颜色和文案字号
        "textColor": "#60A3FC",
        "fontSize": 36
    },
    "negativeText": { //取消按钮的文案，包括文案颜色和文案字号
        "textColor": "#999999",
        "fontSize": 18
    },
    "titleText": { //对话框中提示文案的主标题，包括文案颜色和文案字号
        "textColor": "#333333",
        "fontSize": 18
    },
    "messageText": { //对话框提示文案，包括文案颜色和文案字号
        "textColor": "#333333",
        "fontSize": 16
    }
}
```

- imageView: 图片组件，支持图片的配置，包括认证结果的icon、活体过程中左上角关闭icon、H5页面上的相关图片icon等。
```json
"imageView": {
    "src": "face_confirm_button@2x.png"
}
```

- detectAnimation： 检测动画组件，支持未检测到人脸时警告呼吸灯色、人脸检测结束后加载动画色值（v4.8.0 有更新）
```json
"detectAnimation": {
  	"warningColor": "#F6493F", // 活体检测未检测到人脸时的呼吸灯效果色值，在sdk版本号>=4.8.0才有
  	"loadingColor": "188FFF" // 活体检测结束后加载动画效果色值，在sdk版本号>=4.8.0才有
}
```

- control: 通用组件，目前仅支持设置色值（4.11.2 新增）
```json
"control": {
    "backgroundColor": "#4e6af0"
}
```

- container：整合所有样式属性容器组件（h5前端专属）
```json
"container": {
    "backgroundColor": "#FFFFFF",
    "backgroundImage": "face_confirm_button@2x.png", // 背景图片
    "textColor": "#FFFFFF",
    "fontSize": 32,
     "textPadding": {
        "left": 0,
        "right": 0,
        "top": 0,
        "bottom": 32
  	},
    "src": "face_confirm_button@2x.png" // 内含图标的源地址
}

// Example: h5前端认证步骤条组件
"stepBarContainer": {
      "todoTextColor": "#ffffff",
      "todoBackgroundColor": "#d1d1d1",
      "currentTextColor": "#ffffff",
      "currentBackgroundColor": "#ff631d",
      "doneTextColor": "#ffffff",
      "doneBackgroundColor": "#fd8127"
}

```


<a name="e1bQB"></a>
### UI自定义规范和协议
<a name="vzgnd"></a>
#### 自定义规范


- 图片命名需要显示声明二倍图或者三倍图，如：xxx@2x.png、xxx@3x.png，对应尺寸会在示例文档详细说明，均按 2 倍图说明。
- 色值：iOS 和 Android 支持 ARGB，即支持透明度，色值命名需要以 “#” 开头，例如 #FFFFFF、#00FFFFFF，H5 只支持 RGB
- 字号、图片尺寸、边距的单位是 px, 



<a name="x5qzh"></a>
#### 自定义协议


> 实人认证SDK 会携带一套完整的 demo 示例，实际参考SDK包中的 demo 示例
> 下面列出来支持的所有可定制的控件，用户根据自己选择可以定制指定控件，不写则使用默认值


<br />**协议分类：**

- global： 全局配置，当前配置为空时则取全局配置，作用域自下而上，先从最里面的配置开始查找。
- native： 原生相关页面的配置，如活体过程中的相关页面。
- web：H5 页面相关配置，如认证引导页、用户隐私授权页、认证结果页、身份证拍照页面等。
```json
{
    "global": { // 全局配置,目的是对于比较通用的组件不需要多次设置，可复用全局配置，如果某个页面需要特殊定制，则在对应页面下面声明对应的属性进行覆盖
        "mainButton": { // 目前仅支持主按钮的配置，如native或H5页面的蓝色主按钮。
            "backgroundColor": "#00FFFFFF", //按钮背景色
            "backgroundImage": "face_confirm_button@2x.png", // 尺寸：686 * 88，按钮背景图片，生效优先级高于背景色 
            "textColor": "#FFFFFF", //文案颜色
            "fontSize": 32, //文案字号
            "textPadding": { //文本内边距
                "left": 0,
                "right": 0,
                "top": 0,
                "bottom": 0
            }
        }
    },
    "native": { // 原生页面相关配置
        "global": { // 原生下面生效的全局配置
            "alertDialog": { // 通用弹窗
                "positiveText": {
                    "textColor": "#FD521F",
                    "fontSize": 32
                },
                "negativeText": {
                    "textColor": "#FE9779",
                    "fontSize": 32
                },
                "titleText": {
                    "textColor": "#333333",
                    "fontSize": 32
                },
                "messageText": {
                    "textColor": "#333333",
                    "fontSize": 28
                }
            },
            "navigator": { // 活体过程页面上方的icon
                "closeImageView": { // 页面左上角关闭图片
                    "src": "face_close_btn@2x.png" // 尺寸： 64 * 64
                }
            }         
        }, 
      	welcomePage": { // 4.11.2 版本新增，native 欢迎页, 等同于 web.guidePage
            "mainButton": {
                "backgroundColor": "#00FFFFFF",
                "backgroundImage": "face_confirm_button@2x.png",
                "textColor": "#FFFFFF",
                "fontSize": 32,
                "textPadding": {
                    "left": 0,
                    "right": 0,
                    "top": 0,
                    "bottom": 0
                }
            },
            "bannerControl": {
                "backgroundColor": "#4E6AF0"
            }
        },
        "privacyPage": { // 4.11.2 版本新增,native 隐私页, 等同于 web.guidePage
            "mainButton": {
                "backgroundColor": "#00FFFFFF",
                "backgroundImage": "face_confirm_button@2x.png",
                "textColor": "#FFFFFF",
                "fontSize": 32,
                "textPadding": {
                    "left": 0,
                    "right": 0,
                    "top": 0,
                    "bottom": 0
                }
            },
            "logoImageView": {
                "src": "face_security_icon@2x.png"
            }
        },
        "detectPage": { // 活体检测页面
            "actionTipText": { // 动作提示文案
                "textColor": "#333333",
                "fontSize": 48
            },
            "detectAnimation": { // 识别动画
  							"warningColor": "#F6493F", // 活体检测未检测到人脸时的呼吸灯效果色值，在sdk版本号>=4.8.0才有
  							"loadingColor": "#188FFF" // 活体检测结束后加载动画效果色值，在sdk版本号>=4.8.0才有
            }
        },
        "resultPage": { // 识别结果页
            "titleText": { // 主标题文案
                "textColor": "#333333",
                "fontSize": 40
            },
            "messageText": { // 副标题文案
                "textColor": "#333333",
                "fontSize": 30
            },          
            "promptFailImageView": { // 失败时的 icon
                "src": "face_result_icon_fail@2x.png" // 尺寸： 173 * 173
            },
            "promptSucceedImageView": { // 成功时的 icon
                "src": "face_result_icon_ok@2x.png" // 尺寸： 173 * 173
            },
            "mainButton": { // 主按钮，可为空，如果为空则取 global 中的配置
                "backgroundColor": "#00FFFFFF",
                "backgroundImage": "face_confirm_button@2x.png", // 尺寸：686 * 88
                "textColor": "#FFFFFF",
                "fontSize": 32,
                "textPadding": {
                    "left": 0,
                    "right": 0,
                    "top": 0,
                    "bottom": 0
                }
            }
        }
    },
    "web": { // H5 相关页面配置，如果您对接的实人认证方案中没有用到 H5页面则可不关心
        "global": { // web 配置下面的全局配置，作用域仅在 web 下生效
            "alertDialog": { // 通用对话弹窗
                "positiveText": {  //确认按钮文案
                    "textColor": "#FD521F",
                    "fontSize": 32
                },
                "negativeText": {  // 取消按钮文案
                    "textColor": "#FE9779",
                    "fontSize": 32
                }
            },
            "stepBarContainer": { // 进度条设置，在RPBasic、RPManual、RPBioID方案中会涉及
                  "todoTextColor": "#ffffff",  // 下一步操作的文案颜色
                  "todoBackgroundColor": "#d1d1d1", // 下一步操作的进度条背景色
                  "currentTextColor": "#ffffff", // 当前操作的进度条文案颜色
                  "currentBackgroundColor": "#ff631d", // 当前操作的进度条背景色
                  "doneTextColor": "#ffffff", // 上一步操作的进度条文案颜色
                  "doneBackgroundColor": "#fd8127" // 上一步操作的进度条背景色
            }
        },
      	"guidePage": { // H5 认证引导页
            "mainButton": { // 主按钮，可为空，如果为空则取 global 中的配置
                "backgroundColor": "#00FFFFFF",
                "backgroundImage": "face_confirm_button@2x.png", // 尺寸：686 * 88
                "textColor": "#FFFFFF",
                "fontSize": 32,
                "textPadding": {
                    "left": 0,
                    "right": 0,
                    "top": 0,
                    "bottom": 0
                }
            },
            "bannerContainer": { // H5 认证引导页上方的banner
                "backgroundColor": "#FD521F" //banner背景颜色
            }
        },
        "privacyPage": { // H5 用户授权声明页面
            "mainButton": { // 主按钮，可为空，如果为空则取 global 中的配置
                "backgroundColor": "#00FFFFFF",
                "backgroundImage": "face_confirm_button@2x.png", // 尺寸：686 * 88
                "textColor": "#FFFFFF",
                "fontSize": 32,
                "textPadding": {
                    "left": 0,
                    "right": 0,
                    "top": 0,
                    "bottom": 0
                }
            },
            "logoImageView": { // 用户隐私授权页面上方的icon图片
                "src": "web_icon-check-shield@2x.png" // 尺寸： 200 * 200
            }
        },
        "identityPage": { // H5 证件照页面，对应于RPManual方案的手输姓名身份证号页面
            "mainButton": { // 主按钮，可为空，如果为空则取 global 中的配置
                "backgroundColor": "#00FFFFFF",
                "backgroundImage": "face_confirm_button@2x.png", // 尺寸：686 * 88
                "textColor": "#FFFFFF",
                "fontSize": 32,
                "textPadding": {
                    "left": 0,
                    "right": 0,
                    "top": 0,
                    "bottom": 0
                }
            }
        },
        "photoPage": { // 拍照页
            "exampleContainer": { // 拍照示例容器
                "backgroundColor": "#FD521F" //拍照示例页面中，“示例”文案的背景颜色
            },
            "mainButton": { // 主按钮，可为空，如果为空则取 global 中的配置
                "backgroundColor": "#00FFFFFF",
                "backgroundImage": "face_confirm_button@2x.png", // 尺寸：686 * 88
                "textColor": "#FFFFFF",
                "fontSize": 32,
                "textPadding": {
                    "left": 0,
                    "right": 0,
                    "top": 0,
                    "bottom": 0
                }
            }
        },
        "resultPage": { // H5 认证结果页
            "titleText": { //主标题文案
                "textColor": "#FFFFFF",
                "fontSize": 32
            },
            "messageText": { //副标题文案
                "textColor": "#FFFFFF",
                "fontSize": 32
            },
            "promptFailImageView": { // 认证不通过时的icon
                "src": "face_result_icon_fail@2x.png" // 尺寸： 173 * 173
            },
            "promptSucceedImageView": { //认证成功时的icon
                "src": "face_result_icon_ok@2x.png" // 尺寸： 173 * 173
            },
            "mainButton": { // 主按钮，可为空，如果为空则取 global 中的配置
                "backgroundColor": "#00FFFFFF",
                "backgroundImage": "face_confirm_button@2x.png", // 尺寸：686 * 88 
                "textColor": "#FFFFFF",
                "fontSize": 32,
                "textPadding": {
                    "left": 0,
                    "right": 0,
                    "top": 0,
                    "bottom": 0
                }
            }
        }
    }
}
```


<a name="HXesW"></a>
### UI定制方法


> 特点： 一套资源双端通用，只需要一套皮肤资源包植入对应 App 中即可生效



<a name="z"></a>
#### 资源目录

<br />上面介绍了皮肤设置的方式，那么这个皮肤配置文件和图片资源放在那里呢？对于实人 SDK 来说需要的就是一个文件路径，文件里面的目录结构如下（可参考 demo 资源包）：<br />

- `RPSkin.json` 是皮肤配置文件
- `Resources` 文件夹是存放 `RPSkin.json` 皮肤文件中指定的图片资源的，命名要一一对应，关于图片尺寸请参考配置文件的注释
- 这两个文件的命名不可更改


<br />所以对于实人认证 SDK 来说，资源包可以是任何形式和位置（xxx.bundle、普通文件夹等），保证最终的目录结构如下即可<br />![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2021/png/283627/1611025549966-35685516-e3b0-4e28-9755-844e8d624f8d.png#height=165&id=jSynb&margin=%5Bobject%20Object%5D&name=image.png&originHeight=165&originWidth=411&originalType=binary&ratio=1&size=45220&status=done&style=none&width=411)<br />

<a name="4FfGo"></a>
#### iOS 接入
```objectivec
// 配置类
RPConfiguration *configuration = [RPConfiguration configuration];
...
// 设置皮肤路径即可，不设置则使用默认皮肤
configuration.customUIPath = @"xxx/xxx/xxx";
[RPSDK startByNativeWithVerifyToken:verifyToken
                     viewController:self
                      configuration:configuration
                           progress:^(RPPhase phase) { }
                         completion:^(RPResult * _Nonnull result) {
    NSLog(@"实人认证结果：%@", result);
}];

```
<a name="vszEi"></a>
#### Android 接入
```java
RPConfig.Builder configBuilder = new RPConfig.Builder()
                .setSkinInAssets(true) // 是否是内置皮肤
                .setSkinPath(xxx/xxx); // 设置皮肤路径
RPConfig config = configBuilder.build();

// 入口 1
RPVerify.startByNative(this, mVerifyToken, config, new RPEventListener() {
            @Override
            public void onFinish(RPResult rpResult, String code, String msg) {
                Log.d(TAG, "RPVerify onFinish rpResult.code=" + rpResult.code + "，rpResult.message=" + rpResult.message + "，code=" + code + "，msg: " + msg);
                Toast.makeText(getActivity(), "认证结果：" + rpResult.message + " 状态码：" + code + " message: " + msg, Toast.LENGTH_SHORT).show();
            }
        });

// 入口 2
RPVerify.start(this, mVerifyToken, config, new RPEventListener() {
            @Override
            public void onFinish(RPResult rpResult, String code, String msg) {
                Log.e(TAG, "RPVerify onFinish rpResult.code=" + rpResult.code + "rpResult.message=" + rpResult.message + " code=" + code);
                Toast.makeText(getActivity(), "认证结果：" + rpResult.message + " 状态码:" + code, Toast.LENGTH_SHORT).show();
            }
        });

```
<a name="VK2OD"></a>
#### UI定制内容详细说明
![](https://intranetproxy.alipay.com/skylark/lark/0/2021/png/21189/1617897865331-a018700b-6604-42b6-9ea3-b07e854a8560.png#from=paste&height=607&id=uc39e6ea0&margin=%5Bobject%20Object%5D&originHeight=1214&originWidth=1948&originalType=binary&ratio=1&size=1372906&status=done&style=none&taskId=u94e77384-ad75-4db7-a76b-9cf7afab1c9&width=974)<br />上图中对应序号的UI定制详情参考下方表格。

| 序号 | 所属协议类别 | 在协议中的配置信息 |
| --- | --- | --- |
| 1、关闭icon可替换 | native | ![](https://intranetproxy.alipay.com/skylark/lark/0/2021/png/21189/1617898040640-50612d8b-7569-487d-8ce0-ae0bfd624337.png#from=paste&height=170&id=ub2e17745&margin=%5Bobject%20Object%5D&originHeight=340&originWidth=960&originalType=binary&ratio=1&size=157734&status=done&style=none&taskId=u3243f87f-4cf5-4fc8-9605-70f44df681c&width=480) |
| 2、动作提示文案颜色、字号可修改 | native | ![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2021/png/21189/1611216686622-bf8c3145-b09b-4bf0-8250-0f2ffe97b5c1.png#height=92&id=hLFm0&margin=%5Bobject%20Object%5D&name=image.png&originHeight=184&originWidth=1050&originalType=binary&ratio=1&size=27814&status=done&style=none&width=525) |
| 3、未检测到人脸时呼吸灯色值可修改 | native | ![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2021/png/21189/1617021973793-27b1a14f-4e55-4617-87b2-cfd8f98c6ad8.png#height=127&id=taVge&margin=%5Bobject%20Object%5D&name=image.png&originHeight=254&originWidth=1464&originalType=binary&ratio=1&size=222925&status=done&style=none&width=732) |
| 4、对话框标题文案颜色和字号可修改 | native | ![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2021/png/21189/1611217952450-b783ff4f-51e1-4807-9406-7514289d7c76.png#height=422&id=Tg4hO&margin=%5Bobject%20Object%5D&name=image.png&originHeight=844&originWidth=1272&originalType=binary&ratio=1&size=98967&status=done&style=none&width=636) |
| 5、对话框按钮文案颜色和字号可修改 |  |  |
| 6、加载中效果色值可修改 | native | ![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2021/png/21189/1617021973793-27b1a14f-4e55-4617-87b2-cfd8f98c6ad8.png#height=127&id=y8dd9&margin=%5Bobject%20Object%5D&name=image.png&originHeight=254&originWidth=1464&originalType=binary&ratio=1&size=222925&status=done&style=none&width=732) |
| 7、验证结果icon可替换 | native | ![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2021/png/21189/1611218053564-06240b70-cda3-43aa-9575-d33488118556.png#height=170&id=iSTCU&margin=%5Bobject%20Object%5D&name=image.png&originHeight=340&originWidth=1076&originalType=binary&ratio=1&size=56526&status=done&style=none&width=538) |
| 8、验证结果标题和副标题文案颜色、字号可修改 | native | ![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2021/png/21189/1611218111210-d0c1266b-4352-438f-b660-2be83c768dad.png#height=193&id=5Z3ux&margin=%5Bobject%20Object%5D&name=image.png&originHeight=386&originWidth=1078&originalType=binary&ratio=1&size=48397&status=done&style=none&width=539) |
| 9、主按钮背景色、背景图片可替换；按钮文案颜色和字号可修改；<br />文本内边距可修改； | global或native | ![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2021/png/21189/1611218210289-b575b93c-e147-4dff-81d7-958c7026ccdc.png#height=151&id=OJS5G&margin=%5Bobject%20Object%5D&name=image.png&originHeight=590&originWidth=1922&originalType=binary&ratio=1&size=131305&status=done&style=none&width=492)<br />或<br />![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2021/png/21189/1611218301784-3a83f8a8-c8f8-40cb-9bc0-b9bb6124449e.png#height=314&id=llL8h&margin=%5Bobject%20Object%5D&name=image.png&originHeight=628&originWidth=1178&originalType=binary&ratio=1&size=80037&status=done&style=none&width=589)<br /> |
| 10、banner背景颜色可修改 | web 且 native | ![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2021/png/21189/1611218401197-4b591aea-697b-4766-a7e2-cdfb60a72dbe.png#height=128&id=MwbNU&margin=%5Bobject%20Object%5D&name=image.png&originHeight=256&originWidth=1080&originalType=binary&ratio=1&size=35255&status=done&style=none&width=540)<br />如果你是大于 4.11.2 版本则需要同时设置如下配置<br />![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2021/png/283627/1629724435385-a0286cbc-52a3-431d-b392-06acb71d380b.png#clientId=u881db808-5519-4&from=paste&height=111&id=u455bf814&margin=%5Bobject%20Object%5D&name=image.png&originHeight=111&originWidth=325&originalType=binary&ratio=1&size=30147&status=done&style=none&taskId=uf2b5804e-7a84-4410-8c27-f4330243bf2&width=325) |
| 11、主按钮背景色、背景图片可替换；按钮文案颜色和字号可修改；<br />文本内边距可修改； | global<br />或web版块的guidePage、privacyPage、identityPage、photoPage、resultPage | ![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2021/png/21189/1611218210289-b575b93c-e147-4dff-81d7-958c7026ccdc.png#height=151&id=EYdZV&margin=%5Bobject%20Object%5D&name=image.png&originHeight=590&originWidth=1922&originalType=binary&ratio=1&size=131305&status=done&style=none&width=492)<br />或<br />![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2021/png/21189/1611218479960-24d59568-978d-487b-b4ec-304fe44bc83e.png#height=268&id=sN96y&margin=%5Bobject%20Object%5D&name=image.png&originHeight=536&originWidth=1180&originalType=binary&ratio=1&size=76696&status=done&style=none&width=590)<br />（下面为 4.11.2 版本新增）<br />![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2021/png/283627/1629724495602-f3ffbda1-ac4f-4889-9c11-2e2064320e34.png#clientId=u881db808-5519-4&from=paste&height=299&id=ue50fcdac&margin=%5Bobject%20Object%5D&name=image.png&originHeight=299&originWidth=586&originalType=binary&ratio=1&size=83372&status=done&style=none&taskId=u5167f51e-b00e-4df0-b9c9-05bbbb8e5bc&width=586)<br />![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2021/png/283627/1629724631885-478deb27-1f21-40db-93d3-47faaecc9a45.png#clientId=u881db808-5519-4&from=paste&height=303&id=u95a601cd&margin=%5Bobject%20Object%5D&name=image.png&originHeight=303&originWidth=539&originalType=binary&ratio=1&size=90504&status=done&style=none&taskId=ubd9645de-1c08-4daa-91e5-083e073b9e3&width=539) |
| 12、隐私授权声明icon可替换 | web 且 native | ![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2021/png/21189/1611218565060-68611f34-cda8-4745-b6ec-0c4523983727.png#height=127&id=EkboA&margin=%5Bobject%20Object%5D&name=image.png&originHeight=254&originWidth=1080&originalType=binary&ratio=1&size=40356&status=done&style=none&width=540)<br />（4.11.2 版本新增）<br />![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2021/png/283627/1629724562898-32e0839c-46ce-465d-8dd6-a1b2fbc4219a.png#clientId=u881db808-5519-4&from=paste&height=118&id=ub88b9749&margin=%5Bobject%20Object%5D&name=image.png&originHeight=118&originWidth=436&originalType=binary&ratio=1&size=31909&status=done&style=none&taskId=ue6c23b56-6e3a-4b8c-ae61-80cc097a262&width=436) |
| 13、进度条当前操作和下一步操作的文案颜色、进度条背景色可修改 | web | ![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2021/png/21189/1611218634352-abb371b3-2223-470a-b97f-9604eeda3424.png#height=253&id=xi40d&margin=%5Bobject%20Object%5D&name=image.png&originHeight=506&originWidth=1290&originalType=binary&ratio=1&size=135393&status=done&style=none&width=645) |
| 14、进度条上一步操作的文案颜色，进度条背景色可修改 |  |  |
| 15、拍照页“示例”文案的背景色可修改 | web | ![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2021/png/21189/1611218715027-159481a6-7cc3-4ed7-9a7c-e032081822a0.png#height=87&id=3Rm55&margin=%5Bobject%20Object%5D&name=image.png&originHeight=174&originWidth=1116&originalType=binary&ratio=1&size=34158&status=done&style=none&width=558) |
| 16、弹框按钮文案颜色和字号可修改 | web | ![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2021/png/21189/1611218848306-35cf53c2-fb37-4abc-9a9d-f72864b44f3f.png#height=249&id=cMzws&margin=%5Bobject%20Object%5D&name=image.png&originHeight=498&originWidth=1128&originalType=binary&ratio=1&size=83708&status=done&style=none&width=564) |
| 17、认证结果icon可替换 | web | ![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2021/png/21189/1611218915193-c8746c3d-6b35-441b-86a5-a994073cda93.png#height=171&id=a7zhE&margin=%5Bobject%20Object%5D&name=image.png&originHeight=342&originWidth=1080&originalType=binary&ratio=1&size=58923&status=done&style=none&width=540) |
| 18、认证结果标题和副标题文案颜色、字号可修改 | web | ![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2021/png/21189/1611218965803-6cae6727-64b4-40db-a1ef-c2a90f00b423.png#height=191&id=VWDry&margin=%5Bobject%20Object%5D&name=image.png&originHeight=382&originWidth=1078&originalType=binary&ratio=1&size=46362&status=done&style=none&width=539) |

