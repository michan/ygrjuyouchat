//
//  acceptCallViewController.h
//  shiku_im
//
//  Created by MacZ on 2017/8/7.
//  Copyright © 2017年 Reese. All rights reserved.
//

#import "JY_admobViewController.h"
@class JY_AudioPlayer;

@interface AskCallViewController : JY_admobViewController{
    BOOL _bAnswer;
    JY_AudioPlayer* _player;
}
@property (nonatomic, copy) NSString * toUserId;
@property (nonatomic, copy) NSString * toUserName;
@property (nonatomic, assign) int type;
@property (nonatomic, strong) NSTimer *timer;

@property (nonatomic, copy) NSString *meetUrl;

@end
