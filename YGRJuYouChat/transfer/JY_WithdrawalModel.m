//
//  JY_WithdrawalModel.m
//  TFJunYouChat
//
//  Created by 月月 on 2021/10/9.
//  Copyright © 2021 zengwOS. All rights reserved.
//

#import "JY_WithdrawalModel.h"

@implementation JY_WithdrawalModel
- (void)getWithdrawaWithDict:(NSDictionary *)dict {
    self.userId = dict[@"userId"];
    self.transferStatus = dict[@"transferStatus"];
    self.changeType = [dict[@"changeType"] intValue];
    self.toUserId = dict[@"toUserId"];
    self.desc  = dict[@"desc"];
    self.money = [dict[@"money"] doubleValue];
    self.type = [dict[@"type"] intValue];
    self.payType = [dict[@"payType"] intValue];
    self.time  = [self getTime:dict[@"time"]];
}
- (NSString *)getTime:(NSString *)time {
    NSTimeInterval interval    = [time doubleValue];
    NSDate *date               = [NSDate dateWithTimeIntervalSince1970:interval];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString*currentDateStr = [formatter stringFromDate: date];
    return currentDateStr;
}
@end
