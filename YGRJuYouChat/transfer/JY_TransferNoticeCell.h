#import <UIKit/UIKit.h>
@class JY_TransferNoticeModel;
@interface JY_TransferNoticeCell : UITableViewCell
- (void)setDataWithMsg:(JY_MessageObject *)msg model:(id)tModel;
+ (float)getChatCellHeight:(JY_MessageObject *)msg;
@end


@interface ManMan_TransferNoticeBackCell : UITableViewCell
- (void)setDataWithMsg:(JY_MessageObject *)msg model:(id)tModel;
+ (float)getChatCellHeight:(JY_MessageObject *)msg;
@end
