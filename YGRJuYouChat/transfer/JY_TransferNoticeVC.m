#import "JY_TransferNoticeVC.h"
#import "JY_TransferNoticeCell.h"
#import "JY_TransferNoticeModel.h"
#import "JY_TransferModel.h"
#import "JY_TransferOpenPayModel.h"
#import "JY_WithdrawalModel.h"
@interface JY_TransferNoticeVC ()
@property (nonatomic, strong) NSArray *array;
@end
@implementation JY_TransferNoticeVC
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = Localized(@"JX_PaymentNo.");
    self.heightHeader = ManMan_SCREEN_TOP;
    self.heightFooter = 0;
    self.isGotoBack = YES;
    [self createHeadAndFoot];
    self.isShowFooterPull = NO;
    self.isShowHeaderPull = NO;
    _table.backgroundColor = HEXCOLOR(0xF2F2F2);
    [self getData];
}
- (void)getData {
    _array = [[JY_MessageObject sharedInstance] fetchAllMessageListWithUser:SHIKU_TRANSFER];
    if (_array.count > 0) {
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:_array.count-1 inSection:0]
                                    animated:NO
                              scrollPosition:UITableViewScrollPositionMiddle];
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _array.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    JY_MessageObject *msg=[_array objectAtIndex:indexPath.row];
    
    if ([msg.type intValue] == kWCMessageTypeTransferBack){
        
        return [ManMan_TransferNoticeBackCell getChatCellHeight:msg];
    }
    return [JY_TransferNoticeCell getChatCellHeight:msg];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    JY_MessageObject *msg = _array[indexPath.row];

    if ([msg.type intValue] == kWCMessageTypeTransferBack)
    {
        static NSString *cellIdentifier = @"ManMan_TransferNoticeBackCell";
        ManMan_TransferNoticeBackCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (!cell) {
            cell = [[ManMan_TransferNoticeBackCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;

        NSDictionary *dict = [self dictionaryWithJsonString:msg.content];
        if ([msg.type intValue] == kWCMessageTypeTransferBack) {
            JY_TransferModel *model = [[JY_TransferModel alloc] init];
            [model getTransferDataWithDict:dict];
            [cell setDataWithMsg:msg model:model];
        }
        else if ([msg.type intValue] == kWCMessageTypeOpenPaySuccess) {
            JY_TransferOpenPayModel *model = [[JY_TransferOpenPayModel alloc] init];
            [model getTransferDataWithDict:dict];
            [cell setDataWithMsg:msg model:model];
        }
        else {
            JY_TransferNoticeModel *model = [[JY_TransferNoticeModel alloc]init];
            [model getTransferNoticeWithDict:dict];
            [cell setDataWithMsg:msg model:model];
        }
        return cell;
        
    }
    static NSString *cellIdentifier = @"JY_TransferNoticeCell";
    JY_TransferNoticeCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[JY_TransferNoticeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    NSDictionary *dict = [self dictionaryWithJsonString:msg.content];
    if ([msg.type intValue] == kWCMessageTypeTransferBack) {
        JY_TransferModel *model = [[JY_TransferModel alloc] init];
        [model getTransferDataWithDict:dict];
        [cell setDataWithMsg:msg model:model];
    }
    else if ([msg.type intValue] == kWCMessageTypeOpenPaySuccess) {
        JY_TransferOpenPayModel *model = [[JY_TransferOpenPayModel alloc] init];
        [model getTransferDataWithDict:dict];
        [cell setDataWithMsg:msg model:model];
    }
    else if ([msg.type intValue] == kWCMessageTypeWithdrawSuccess) {
        JY_WithdrawalModel *model = [[JY_WithdrawalModel alloc] init];
        [model getWithdrawaWithDict:dict];
        [cell setDataWithMsg:msg model:model];
    }
    else {
        JY_TransferNoticeModel *model = [[JY_TransferNoticeModel alloc]init];
        [model getTransferNoticeWithDict:dict];
        [cell setDataWithMsg:msg model:model];
    }
    return cell;
}
- (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString
{
    if (jsonString == nil) {
        return nil;
    }
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                        options:NSJSONReadingMutableContainers
                                                          error:&err];
    if(err)
    {
        NSLog(@"json解析失败：%@",err);
        return nil;
    }
    return dic;
}
@end
