//
//  JY_WithdrawalModel.h
//  TFJunYouChat
//
//  Created by 月月 on 2021/10/9.
//  Copyright © 2021 zengwOS. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface JY_WithdrawalModel : NSObject
@property (nonatomic, strong) NSString *transferStatus;
@property (nonatomic, assign) int changeType;
@property (nonatomic, strong) NSString *toUserId;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, assign) int payType;
@property (nonatomic, strong) NSString *desc;
@property (nonatomic, strong) NSString *tradeNo;
@property (nonatomic, assign) double money;
@property (nonatomic, assign) int type;
@property (nonatomic, strong) NSString *time;
- (void)getWithdrawaWithDict:(NSDictionary *)dict;

@end

NS_ASSUME_NONNULL_END
