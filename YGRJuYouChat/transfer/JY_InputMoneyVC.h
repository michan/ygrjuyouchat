#import "JY_admobViewController.h"
typedef NS_ENUM(NSInteger, ManMan_InputMoneyType) {
    ManMan_InputMoneyTypeSetMoney,        
    ManMan_InputMoneyTypeCollection,      
    ManMan_InputMoneyTypePayment,         
};
@interface JY_InputMoneyVC : JY_admobViewController
@property (nonatomic, assign) ManMan_InputMoneyType type;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *money;
@property (nonatomic, strong) NSString *desStr;
@property (nonatomic, strong) NSString *paymentCode;
@property (weak, nonatomic) id delegate;
@property (nonatomic, assign) SEL onInputMoney;
@end
