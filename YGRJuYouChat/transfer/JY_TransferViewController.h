#import "JY_admobViewController.h"
@protocol transferVCDelegate <NSObject>
-(void)transferToUser:(NSDictionary *)redpacketDict;
@end
@interface JY_TransferViewController : JY_admobViewController
@property (nonatomic, strong) JY_UserObject *user;
@property (weak, nonatomic) id <transferVCDelegate> delegate;
@end
