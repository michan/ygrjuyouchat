#import <UIKit/UIKit.h>
@class JY_RecordModel;
NS_ASSUME_NONNULL_BEGIN
@interface JY_RecordCell : UITableViewCell
@property (nonatomic, strong) UIView *lineView;
- (void)setData:(JY_RecordModel *)model;
@end
NS_ASSUME_NONNULL_END
