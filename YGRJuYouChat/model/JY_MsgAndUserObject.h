//
//  JY_MsgAndUserObject.h
//
//  Created by Reese on 13-8-15.
//  Copyright (c) 2013年 Reese. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JY_MsgAndUserObject : NSObject
@property (nonatomic,strong) JY_MessageObject* message;
@property (nonatomic,strong) JY_UserObject* user;

+(JY_MsgAndUserObject *)unionWithMessage:(JY_MessageObject *)aMessage andUser:(JY_UserObject *)aUser;
@end
