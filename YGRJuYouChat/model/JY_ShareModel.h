//
//  JY_ShareModel.h
//  TFJunYouChat
//
//  Created by MacZ on 16/8/22.
//  Copyright © 2020 zengwOS. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    ManMan_ShareToWechatSesion,
    ManMan_ShareToWechatTimeline,
    ManMan_ShareToSina,
    ManMan_ShareToFaceBook,
    ManMan_ShareToTwitter,
    ManMan_ShareToWhatsapp,
    ManMan_ShareToSMS,
    ManMan_ShareToLine,
} ManMan_ShareTo;

@interface JY_ShareModel : NSObject

@property (nonatomic,assign) ManMan_ShareTo shareTo;
@property (nonatomic,copy) NSString *shareTitle;
@property (nonatomic,copy) NSString *shareContent;
@property (nonatomic,copy) NSString *shareUrl;
@property (nonatomic,strong) UIImage *shareImage;
@property (nonatomic,copy) NSString *shareImageUrl;

@end
