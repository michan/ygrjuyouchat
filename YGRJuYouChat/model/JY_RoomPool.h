//
//  JY_RoomPool.h
//  TFJunYouChat
//
//  Created by lifengye on 2020/09/021.
//  Copyright (c) 2014年 Reese. All rights reserved.
//

#import <Foundation/Foundation.h>

@class JY_RoomObject;
@class XMPPRoomCoreDataStorage;

@interface JY_RoomPool : NSObject{
//    NSMutableDictionary* _pool;
    XMPPRoomCoreDataStorage* _storage;
}
@property (nonatomic,strong) NSMutableDictionary* pool;

-(JY_RoomObject*)createRoom:(NSString*)jid title:(NSString*)title;
-(JY_RoomObject*)joinRoom:(NSString*)jid title:(NSString*)title lastDate:(NSDate *)lastDate isNew:(bool)isNew;

-(void)setRoomPool:(NSString*)jid title:(NSString*)title;
//-(JY_RoomObject*)connectRoom:(NSString*)jid title:(NSString*)title;

-(void)deleteAll;
-(void)createAll;
-(void)reconnectAll;
-(void)delRoom:(NSString*)jid;
-(JY_RoomObject*)getRoom:(NSString*)jid;


-(void)connectRoom;
@end
