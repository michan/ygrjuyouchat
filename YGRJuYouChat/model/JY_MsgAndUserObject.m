//
//  JY_MsgAndUserObject.m
//
//  Created by Reese on 13-8-15.
//  Copyright (c) 2013年 Reese. All rights reserved.
//

#import "JY_MsgAndUserObject.h"

@implementation JY_MsgAndUserObject
@synthesize message,user;


+(JY_MsgAndUserObject *)unionWithMessage:(JY_MessageObject *)aMessage andUser:(JY_UserObject *)aUser
{
    JY_MsgAndUserObject *unionObject=[[JY_MsgAndUserObject alloc]init];
    unionObject.user = aUser;
    unionObject.message = aMessage;
//    NSLog(@"%d,%d",aMessage.retainCount,aUser.retainCount);
    return unionObject;
}

-(void)dealloc{
//    NSLog(@"JY_MsgAndUserObject.dealloc");
    self.user = nil;
    self.message = nil;
//    [super dealloc];
}



@end
