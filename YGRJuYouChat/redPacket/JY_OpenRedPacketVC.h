#import <UIKit/UIKit.h>
#import "JY_PacketObject.h"
#import "JY_GetPacketList.h"
@interface JY_OpenRedPacketVC : JY_admobViewController{
    JY_OpenRedPacketVC *_pSelf;
}
@property (strong, nonatomic) IBOutlet UIImageView *headerImageView;
@property (strong, nonatomic) IBOutlet UILabel *fromUserLabel;
@property (strong, nonatomic) IBOutlet UILabel *greetLabel;
@property (strong, nonatomic) IBOutlet UILabel *moneyLabel;
@property (strong, nonatomic) IBOutlet UIView *centerRedPView;
@property (strong, nonatomic) NSDictionary * dataDict;
@property (strong, nonatomic) JY_PacketObject * packetObj;
@property (strong, nonatomic) NSArray * packetListArray;
@property (strong, nonatomic) IBOutlet UIView *blackBgView;
- (void)doRemove;
@end
