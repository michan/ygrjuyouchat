#import "JY_admobViewController.h"
@protocol SendRedPacketVCDelegate <NSObject>
-(void)sendRedPacketDelegate:(NSDictionary *)redpacketDict;
@end
@interface JY_SendRedPacketViewController : JY_admobViewController
@property (nonatomic, assign) BOOL isRoom;
@property (nonatomic,strong) NSString* roomJid;
@property (nonatomic, strong) NSString *toUserId;
@property (nonatomic, assign) NSInteger size;
@property (nonatomic, weak) id<SendRedPacketVCDelegate> delegate;
@property (nonatomic,strong) roomData* room;
@end
