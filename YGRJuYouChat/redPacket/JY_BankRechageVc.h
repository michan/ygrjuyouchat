//
//  JY_BankRechageVc.h
//  TFJunYouChat
//
//  Created by os on 2020/12/24.
//  Copyright © 2020 zengwOS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface JY_BankRechageVc : JY_admobViewController {
    UITextField* _platformName;
    UITextField* _account;
    UITextField* _amount;
    UITextField* _reason;
    UITextField* _remark;
    UITextField* _verifyCode;
}

@end

NS_ASSUME_NONNULL_END
