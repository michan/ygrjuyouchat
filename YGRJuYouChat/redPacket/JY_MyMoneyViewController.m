#import "JY_MyMoneyViewController.h"
#import "UIImage+Color.h"
#import "JY_CashWithDrawViewController.h"
#import "JY_RechargeViewController.h"
#import "JY_RecordCodeVC.h"
#import "JY_MoneyMenuViewController.h"
#import "JY_RealCerVc.h"
#import "JY_JLApplyForWithdrawalVC.h"
#import "JY_BankRechageVc.h"
#import "JXBankList_VC.h"
#import "JXp1a1y1WithBankMoney_VC.h"
#import "JXWithdrawal_VC.h"
#import "CertifyVC.h"
#import "JXRedPacketDisplayVC.h"
#import "JY_BillingRecordsVC.h"
#import "QLMY_PayViewController.h"
#import "TFJunYou_openFreezeNoVc.h"

#define MY_INSET  0
#define HEIGHT 50

@interface JY_MyMoneyViewController ()<UIScrollViewDelegate>
@property (nonatomic, strong) UIButton * listButton;
@property (nonatomic, strong) UIButton * backButton;
@property (nonatomic, strong) UIImageView * iconView;
@property (nonatomic, strong) UILabel * headTitleLabel;
@property (nonatomic, strong) UILabel * myMoneyLabel;
@property (nonatomic, strong) UILabel * balanceLabel;
@property (nonatomic, strong) UIView * hederBackGroundView;
@property (nonatomic, strong) UIButton * rechargeBtn;
@property (nonatomic, strong) UIButton * withdrawalsBtn;

@property (nonatomic, strong) UIButton * mybankBtn;

@end
@implementation JY_MyMoneyViewController
-(instancetype)init{
    if (self = [super init]) {
        self.heightHeader = 0;
        self.heightFooter = 0;
        self.isGotoBack = YES;
        self.title = @"钱包";
        [g_notify addObserver:self selector:@selector(doRefresh:) name:kUpdateUserNotifaction object:nil];
    }
    return self;
}
-(void)myMoneypakege{
    QLMY_PayViewController * webVC =QLMY_PayViewController.new;
    webVC = [webVC init];
   [g_navigation pushViewController:webVC animated:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self createHeadAndFoot];
//    self.tableBody.alwaysBounceVertical = YES;
    self.tableBody.delegate = self;
    
    [self.view addSubview:self.headTitleLabel];
    [self.view addSubview:self.backButton];
    
    
//    [self.view addSubview:self.listButton];
    [self.tableBody addSubview:self.iconView];
    [self.tableBody addSubview:self.myMoneyLabel];
    [self.tableBody addSubview:self.balanceLabel];
    [self.tableBody addSubview:self.rechargeBtn];
    [self.tableBody addSubview:self.withdrawalsBtn];
    [self.tableBody insertSubview:self.hederBackGroundView atIndex:0];

    
    int h=CGRectGetMaxY(self.hederBackGroundView.frame);
    self.tableBody.backgroundColor = HEXCOLOR(0xf0f0f0);
    int w=ManMan_SCREEN_WIDTH;
    
    
   
    
    
    JY_ImageView* iv;
    /*  */
    CGFloat itemLeft = 15;
    CGFloat itemWidth = (SCREEN_WIDTH-30)/3;
    CGFloat itemHeight = 96;
    
    
    UIView *contentV = [[UIView alloc]initWithFrame:CGRectMake(itemLeft, h, SCREEN_WIDTH-30, 2*itemHeight)];
    [self.tableBody addSubview:contentV];
    contentV.backgroundColor = [UIColor whiteColor];
    ViewRadius(contentV, 5);
    
    
    iv = [self createButton:@"银行卡" drawTop:NO drawBottom:NO icon:THESIMPLESTYLE ? @"icon_yinhangqia" : @"icon_yinhangqia" click:@selector(yBankCardBtnAction:) index:contentV];
    iv.frame = CGRectMake(0, 0, itemWidth, itemHeight);
    
    itemLeft += itemWidth;
    iv = [self createButton:@"账单记录" drawTop:NO drawBottom:NO icon:THESIMPLESTYLE ? @"icon_hongbaojilu-1" : @"icon_hongbaojilu-1" click:@selector(onBill) index:contentV];
    iv.frame = CGRectMake(itemWidth,0, itemWidth, itemHeight);

    itemLeft+=itemWidth;
    iv = [self createButton:@"实名认证" drawTop:NO drawBottom:NO icon:THESIMPLESTYLE ? @"icon_shimingrenzheng-" : @"icon_shimingrenzheng-" click:@selector(cerActivity) index:contentV];
    
    iv.frame = CGRectMake(itemWidth * 2,0, itemWidth, itemHeight);
    
    h+=itemHeight;
    itemLeft+=itemWidth;
    itemLeft = 15;
    iv = [self createButton:@"支付密码" drawTop:NO drawBottom:NO icon:THESIMPLESTYLE ? @"icon_zhifumima" : @"icon_zhifumima" click:@selector(listButtonAction) index:contentV];
    
    iv.frame = CGRectMake(0,itemHeight, itemWidth, itemHeight);

    itemLeft+=itemWidth;
    iv = [self createButton:@"安全锁" drawTop:NO drawBottom:NO icon:@"ic_anquansuo" click:@selector(gotoLockVC) index:contentV];
    
    iv.frame = CGRectMake(itemWidth,itemHeight, itemWidth, itemHeight);

}

-(void)gotoLockVC{
    [_wait show];
    [g_server get_act_ApiFreezeAssetsStatustoView:self];
}

-(JY_ImageView*)createButton:(NSString*)title drawTop:(BOOL)drawTop drawBottom:(BOOL)drawBottom icon:(NSString*)icon click:(SEL)click index:(UIView *)contentV{
    JY_ImageView* btn = [[JY_ImageView alloc] init];
    btn.backgroundColor = [UIColor whiteColor];
    btn.userInteractionEnabled = YES;
    btn.didTouch = click;
    btn.delegate = self;
   
    [contentV addSubview:btn];
     
  
    
    CGFloat itemWidth = (SCREEN_WIDTH-30)/3;
    CGFloat itemHeight = 96;
    
    JY_Label* p = [[JY_Label alloc] initWithFrame:CGRectMake(0, itemHeight - 40, itemWidth, 20)];
    p.text = title;
    p.font = g_factory.font16;
    p.backgroundColor = [UIColor clearColor];
    p.textColor = HEXCOLOR(0x323232);
    p.textAlignment = NSTextAlignmentCenter;
    [btn addSubview:p];
    if(icon){
        UIImageView* iv = [[UIImageView alloc] initWithFrame:CGRectMake(itemWidth/2-12.5, 23, 25, 25)];
        iv.image = [UIImage imageNamed:icon];
        [btn addSubview:iv];
    }
//    if(drawTop){
//        UIView* line = [[UIView alloc] initWithFrame:CGRectMake(53,0,ManMan_SCREEN_WIDTH-53,LINE_WH)];
//        //line.backgroundColor = THE_LINE_COLOR;
//        [btn addSubview:line];
//    }
//    if(drawBottom){
//        UIView* line = [[UIView alloc] initWithFrame:CGRectMake(53,HEIGHT-0.3,ManMan_SCREEN_WIDTH-53,LINE_WH)];
//        line.backgroundColor = THE_LINE_COLOR;
//        line.alpha = 0.5;
//        [btn addSubview:line];
//    }
//    if(click){
//        UIImageView* iv;
//        iv = [[UIImageView alloc] initWithFrame:CGRectMake(ManMan_SCREEN_WIDTH-10-17, (HEIGHT-13)/2, 7, 13)];
//        iv.image = [UIImage imageNamed:@"new_icon_>"];
//        [btn addSubview:iv];
//    }
    return btn;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [g_server getUserMoenyToView:self];
}
-(void)dealloc{
    [g_notify removeObserver:self];
}
-(UIButton *)listButton{
    if(!_listButton){
        _listButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _listButton.frame = CGRectMake(ManMan_SCREEN_WIDTH-24-15, ManMan_SCREEN_TOP - 35, 24, 24);
        [_listButton setImage:THESIMPLESTYLE ? [UIImage imageNamed:@"money_menu"] : [UIImage imageNamed:@"money_menu"] forState:UIControlStateNormal];
        _listButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [_listButton addTarget:self action:@selector(listButtonAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _listButton;
}
-(UIButton *)backButton{
    if(!_backButton){
        _backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _backButton.frame = CGRectMake(10, ManMan_SCREEN_TOP - 35, 24, 24);
        [_backButton setImage:THESIMPLESTYLE ? [UIImage imageNamed:@"photo_title_back_black"] : [UIImage imageNamed:@"photo_title_back_black"] forState:UIControlStateNormal];
        _backButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [_backButton addTarget:self action:@selector(actionQuit) forControlEvents:UIControlEventTouchUpInside];
    }
    return _backButton;
}
-(UILabel *)headTitleLabel{
    if (!_headTitleLabel) {
        _headTitleLabel = [UIFactory createLabelWith:CGRectZero text:@"我的钱包" font:g_factory.font18 textColor:HEXCOLOR(0x000000) backgroundColor:nil];
        _headTitleLabel.textAlignment = NSTextAlignmentCenter;
        _headTitleLabel.frame = CGRectMake(0, ManMan_SCREEN_TOP - 30, ManMan_SCREEN_WIDTH, 20);
    }
    return _headTitleLabel;
}
-(UILabel *)myMoneyLabel{
    if (!_myMoneyLabel) {
        _myMoneyLabel = [UIFactory createLabelWith:CGRectZero text:Localized(@"JXMoney_myPocket") font:g_factory.font16 textColor:[UIColor whiteColor] backgroundColor:nil];
        _myMoneyLabel.textAlignment = NSTextAlignmentCenter;
        _myMoneyLabel.frame = CGRectMake(0, 80, ManMan_SCREEN_WIDTH, 20);
    }
    return _myMoneyLabel;
}
-(UILabel *)balanceLabel{
    if (!_balanceLabel) {
        NSString * moneyStr = [NSString stringWithFormat:@"¥%.2f",g_App.myMoney];
        _balanceLabel = [UIFactory createLabelWith:CGRectZero text:moneyStr font:g_factory.font28 textColor:[UIColor whiteColor] backgroundColor:nil];
        _balanceLabel.textAlignment = NSTextAlignmentCenter;
        _balanceLabel.font = SYSFONT(40);
        _balanceLabel.frame = CGRectMake(0, CGRectGetMaxY(_myMoneyLabel.frame)+23, ManMan_SCREEN_WIDTH, 40);
    }
    return _balanceLabel;
}

-(UIView *)hederBackGroundView{
    if (!_hederBackGroundView) {
        _hederBackGroundView = [UIView new];
        CGFloat height =CGRectGetMaxY(_balanceLabel.frame)+100+(THE_DEVICE_HAVE_HEAD ? 50 : 20);
        _hederBackGroundView.frame = CGRectMake(0, THE_DEVICE_HAVE_HEAD ? -50 : -20, ManMan_SCREEN_WIDTH, height);
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(15, THE_DEVICE_HAVE_HEAD?88:64 + 15, ManMan_SCREEN_WIDTH-30,height-(THE_DEVICE_HAVE_HEAD?88:64) - 30)];
        view.backgroundColor = HEXCOLOR(0x01BF5D);
        [_hederBackGroundView addSubview:view];
        ViewRadius(view, 8);
        
        
        _hederBackGroundView.backgroundColor =HEXCOLOR(0xf0f0f0);
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@""]];
        imageView.frame = CGRectMake(0, _hederBackGroundView.frame.size.height - 30, ManMan_SCREEN_WIDTH, 30);
        
        
        [_hederBackGroundView addSubview:imageView];
    }
    return _hederBackGroundView;
}

-(UIButton *)rechargeBtn{
    if (!_rechargeBtn) {
        _rechargeBtn = [UIFactory createButtonWithRect:CGRectZero title:@"充值" titleFont:g_factory.font16 titleColor:[UIColor whiteColor] normal:nil selected:nil selector:@selector(rechargeBtnAction:) target:self];
        _rechargeBtn.frame = CGRectMake((ManMan_SCREEN_WIDTH - 80)/2 - 70, CGRectGetMaxY(_balanceLabel.frame)+20, 100, 40);
        [_rechargeBtn setBackgroundImage:[UIImage createImageWithColor:[UIColor clearColor]] forState:UIControlStateNormal];
        _rechargeBtn.layer.borderColor = [UIColor whiteColor].CGColor;
        _rechargeBtn.layer.borderWidth = 1;
        _rechargeBtn.layer.cornerRadius = 5;
        _rechargeBtn.clipsToBounds = YES;
    }
    return _rechargeBtn;
}
-(UIButton *)withdrawalsBtn{
    if (!_withdrawalsBtn) {
        _withdrawalsBtn = [UIFactory createButtonWithRect:CGRectZero title:@"提现" titleFont:g_factory.font16  titleColor:[UIColor whiteColor] normal:nil selected:nil selector:@selector(withdrawalsBtnAction:) target:self];
        _withdrawalsBtn.frame = CGRectMake((ManMan_SCREEN_WIDTH - 80)/2 + 70, CGRectGetMaxY(_balanceLabel.frame)+20, 100, 40);
        [_withdrawalsBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_withdrawalsBtn setBackgroundImage:[UIImage createImageWithColor:[UIColor clearColor]] forState:UIControlStateNormal];
        _withdrawalsBtn.layer.borderColor = [UIColor whiteColor].CGColor;
        _withdrawalsBtn.layer.borderWidth = 1;
        _withdrawalsBtn.layer.cornerRadius = 5;
        _withdrawalsBtn.clipsToBounds = YES;

    }
    return _withdrawalsBtn;
}
//我的银行卡

- (UIButton *)mybankBtn{
    if (!_mybankBtn) {
        _mybankBtn = [UIFactory createButtonWithRect:CGRectZero title:Localized(@"JX_MyBankCard") titleFont:g_factory.font16  titleColor:[UIColor whiteColor] normal:nil selected:nil selector:@selector(yBankCardBtnAction:) target:self];
        _mybankBtn.frame = CGRectMake(15, CGRectGetMaxY(_withdrawalsBtn.frame)+30, CGRectGetWidth(_withdrawalsBtn.frame), CGRectGetHeight(_withdrawalsBtn.frame));
        [_mybankBtn setTitleColor:THEMECOLOR forState:UIControlStateNormal];
        [_mybankBtn setBackgroundImage:[UIImage createImageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
        _mybankBtn.layer.cornerRadius = 7;
        _mybankBtn.clipsToBounds = YES;
        _mybankBtn.layer.borderColor = THEMECOLOR.CGColor;
        _mybankBtn.layer.borderWidth = 1.f;
    }
    return _mybankBtn;
}
- (void)yBankCardBtnAction:(UIButton *)sender{
    JXBankList_VC *monVC = [[JXBankList_VC alloc] init];
    [g_navigation pushViewController:monVC animated:YES];
    
}
//实名认证
-(void)cerActivity{
    
    CertifyVC * webVC = [CertifyVC alloc];
    webVC = [webVC init];
   [g_navigation pushViewController:webVC animated:YES];
 
}
// 红包记录
- (void)redEnvelopeToRecord {
    JXRedPacketDisplayVC *vc = [[JXRedPacketDisplayVC alloc] init];
    [g_navigation pushViewController:vc animated:YES];
}
// 账单记录
- (void)onBill {
//    JY_RecordCodeVC * recordVC = [[JY_RecordCodeVC alloc]init];
//    recordVC.type = ManMan_RecordTypePayBill;
    JY_BillingRecordsVC * recordVC = [[JY_BillingRecordsVC alloc] init];
    [g_navigation pushViewController:recordVC animated:YES];
}

-(void)updateBalanceLabel{
    NSString * moneyStr = [NSString stringWithFormat:@"¥%.2f",g_App.myMoney];
    self.balanceLabel.text = moneyStr;
    CGFloat Width = [self.balanceLabel.text sizeWithAttributes:@{NSFontAttributeName:self.balanceLabel.font}].width;
    CGRect frame = self.balanceLabel.frame;
    frame.size.width = Width;
    self.balanceLabel.frame = frame;
    self.balanceLabel.center = CGPointMake(self.iconView.center.x, self.balanceLabel.center.y);
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
#pragma mark Action
-(void)listButtonAction{
    _listButton.enabled = NO;
    [self performSelector:@selector(delayButtonReset) withObject:nil afterDelay:0.5];
    JY_MoneyMenuViewController *monVC = [[JY_MoneyMenuViewController alloc] init];
    [g_navigation pushViewController:monVC animated:YES];
}
//充值
-(void)rechargeBtnAction:(UIButton *)button{
    _rechargeBtn.enabled = NO;
    
    
//    if ([g_App.shortVideo intValue]==0) {
//         
//        JY_RealCerVc *cervc=[[JY_RealCerVc alloc]init];
//        [g_navigation pushViewController:cervc animated:YES];
//        return;
//    }
    
    [self performSelector:@selector(delayButtonReset) withObject:nil afterDelay:0.5];
    
//    JY_BankRechageVc * vc = [[JY_BankRechageVc alloc] init];
//    [g_navigation pushViewController:vc animated:YES];
//
//    return;
    JXp1a1y1WithBankMoney_VC * rechargeVC = [[JXp1a1y1WithBankMoney_VC alloc] init];
    [g_navigation pushViewController:rechargeVC animated:YES];
}
-(void)withdrawalsBtnAction:(UIButton *)button{
    _withdrawalsBtn.enabled = NO;
    [self performSelector:@selector(delayButtonReset) withObject:nil afterDelay:0.5];
    // JY_JLApplyForWithdrawalVC JY_CashWithDrawViewController
    JXWithdrawal_VC * cashWithVC = [[JXWithdrawal_VC alloc] init];
    //JY_CashWithDrawViewController * cashWithVC = [[JY_CashWithDrawViewController alloc] init];
    [g_navigation pushViewController:cashWithVC animated:YES];
}
-(void)problemBtnAction{
    [self performSelector:@selector(delayButtonReset) withObject:nil afterDelay:0.5];
}
-(void)delayButtonReset{
    _rechargeBtn.enabled = YES;
    _withdrawalsBtn.enabled = YES;
    _listButton.enabled = YES;
}
-(void)doRefresh:(NSNotification *)notifacation{
    _balanceLabel.text = [NSString stringWithFormat:@"¥%.2f",g_App.myMoney];
}
-(void) didServerResultSucces:(JY_Connection*)aDownload dict:(NSDictionary*)dict array:(NSArray*)array1{
    [_wait hide];
    if ([aDownload.action isEqualToString:act_getUserMoeny]) {
        g_App.myMoney = [dict[@"balance"] doubleValue];
        NSString * moneyStr = [NSString stringWithFormat:@"¥%.2f",g_App.myMoney];
        _balanceLabel.text = moneyStr;
    }
    if ([aDownload.action isEqualToString:act_ApiFreezeAssetsGetStatus]) {
        BOOL status =   [dict[@"data"] boolValue];//true==已冻结
        TFJunYou_openFreezeNoVc *vc = [[TFJunYou_openFreezeNoVc alloc]init];
        //1是解冻资金 2是冻结资金
        vc.type = status?1:2;
        [g_navigation pushViewController:vc animated:YES];
        
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    scrollView.bounces = NO;
}
@end
