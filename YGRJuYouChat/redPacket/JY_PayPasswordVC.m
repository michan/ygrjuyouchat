#import "JY_PayPasswordVC.h"
#import "UIImage+Color.h"
#import "JY_MoneyMenuViewController.h"
#import "JY_TextField.h"
#import "JY_UserObject.h"
#import "JY_SendRedPacketViewController.h"
#import "JY_CashWithDrawViewController.h"
#import "JY_TransferViewController.h"
#import "JY_InputMoneyVC.h"
#import "webpageVC.h"
#import "JY_PayViewController.h"
#import "JXOpenBank_VC.h"

#define kDotSize CGSizeMake (10, 10) 
#define kDotCount 6  
#define K_Field_Height 40
@interface JY_PayPasswordVC () <UITextFieldDelegate>
@property (nonatomic, strong) JY_TextField *textField;
@property (nonatomic, strong) NSMutableArray *dotArray;
@property (nonatomic, strong) NSMutableArray *dotBGArray;

@property (nonatomic, strong) NSMutableArray *shuArray;
@property (nonatomic, strong) UILabel *titleLab;
@property (nonatomic, strong) UILabel *detailLab;
@property (nonatomic, strong) UIButton *nextBtn;
@end
@implementation JY_PayPasswordVC
- (instancetype)init {
    self = [super init];
    if (self) {
        self.view.backgroundColor = [UIColor whiteColor];
        [self setupViews];
        [self initPwdTextField];
        [self setupTitle];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.textField becomeFirstResponder];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}
- (void)setupViews {
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, ManMan_SCREEN_TOP - 46, 46, 46)];
    [btn setBackgroundImage:[UIImage imageNamed:@"title_back_black_big"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(didDissVC) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn];
    self.titleLab.frame = CGRectMake(0, 160, ManMan_SCREEN_WIDTH, 40);
    self.detailLab.frame = CGRectMake(0, CGRectGetMaxY(self.titleLab.frame)+30, ManMan_SCREEN_WIDTH, 17);
    
    self.textField.frame = CGRectMake((SCREEN_WIDTH -315) / 2, CGRectGetMaxY(self.titleLab.frame)+70,315, K_Field_Height);
    self.nextBtn.frame = CGRectMake(25, CGRectGetMaxY(self.textField.frame)+25, ManMan_SCREEN_WIDTH-50, 44);
    [self.view addSubview:self.textField];
    [self.view addSubview:self.titleLab];
    [self.view addSubview:self.detailLab];
    [self.view addSubview:self.nextBtn];
}
- (void)didDissVC {
    if (self.type == ManMan_PayTypeInputPassword) {
        [self goBackToVC];
    }else {
        if (self.enterType == ManMan_EnterTypeCoinNum){
            
            [g_navigation dismissViewController:self animated:YES];
            
        }else{
           [g_App showAlert:Localized(@"JX_CancelPayPsw") delegate:self];
        }
    }
}
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        [self goBackToVC];
    }
}
- (void)setupTitle {
    if (self.type == ManMan_PayTypeSetupPassword) {  
        [self.nextBtn setHidden:YES];
        self.titleLab.text =@"设置支付密码";// Localized(@"JX_SetPayPsw");
        self.detailLab.text = @"";//Localized(@"JX_SetPayPswNo.1");
    } else if (self.type == ManMan_PayTypeRepeatPassword) { 
        [self.nextBtn setHidden:NO];
        self.titleLab.text =@"设置支付密码";
        self.titleLab.text = Localized(@"JX_SetPayPsw");
        self.detailLab.text =@"";// Localized(@"JX_SetPayPswNo.2");
    } else if (self.type == ManMan_PayTypeInputPassword) { 
        [self.nextBtn setHidden:YES];
        self.titleLab.text = Localized(@"JX_UpdatePassWord");
        self.detailLab.text = Localized(@"JX_EnterToVerify");
    }
}
- (void)didNextButton {
    if ([self.textField.text length] < 6) {
        [g_App showAlert:Localized(@"JX_PswError")];
        [self clearUpPassword];
        return;
    }
    if (![self.textField.text isEqualToString:self.lastPsw]) {
        [g_App showAlert:Localized(@"JX_NotMatch")];
        [self goToSetupTypeVCWithOld:NO];
        return;
    }
    if ([self.textField.text isEqualToString:self.oldPsw]) {
        [g_App showAlert:Localized(@"JX_NewEqualOld")];
        [self goToSetupTypeVCWithOld:NO];
        return;
    }
    if(self.type == ManMan_PayTypeRepeatPassword) {
        JY_UserObject *user = [[JY_UserObject alloc] init];
        user.payPassword = self.textField.text;
        user.oldPayPassword = self.oldPsw;
        [g_server updatePayPasswordWithUser:user toView:self];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (void)initPwdTextField {
    CGFloat width = (ManMan_SCREEN_WIDTH - 30*2) / kDotCount;
//    for (int i = 0; i < kDotCount - 1; i++) {
//        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.textField.frame) + (i + 1) * width, CGRectGetMinY(self.textField.frame), 0.5, K_Field_Height)];
//        lineView.backgroundColor = [UIColor redColor];
//        [self.view addSubview:lineView];
//    }
    self.dotArray = [[NSMutableArray alloc] init];
    self.dotBGArray = [[NSMutableArray alloc] init];
    self.shuArray = [[NSMutableArray alloc] init];
    CGFloat bgWh =40;
    CGFloat marignLeft =(SCREEN_WIDTH -315)/2;
    for (int i = 0; i < kDotCount; i++) {
        UIView *dogBgView = [[UIView alloc] initWithFrame:CGRectMake(marignLeft+i*bgWh + i *15,  CGRectGetMinY(self.textField.frame), 40, 40)];
        dogBgView.backgroundColor = HEXCOLOR(0xf0f0f0);
        dogBgView.layer.cornerRadius = 5;
        [self.view addSubview:dogBgView];
        [self.dotBGArray addObject:dogBgView];
        
        UIView *shu = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 2, 25)];
        shu.center = dogBgView.center;
        shu.backgroundColor = HEXCOLOR(0x05D168);
        [self.view addSubview:shu];
        [self.shuArray addObject:shu];
        shu.hidden = YES;
        
        if (i == 0) {
            dogBgView.backgroundColor = [UIColor whiteColor];
            dogBgView.layer.masksToBounds = NO;
            dogBgView.layer.shadowColor = HEXCOLOR(0xF0F0F0).CGColor;
            dogBgView.layer.shadowOffset = CGSizeMake(0,2.5);
            dogBgView.layer.shadowOpacity = 1;
            dogBgView.layer.shadowRadius = 7;
            shu.hidden = NO;
        }
        UIView *dotView = [[UIView alloc] initWithFrame:CGRectMake(0, 0 + (K_Field_Height - kDotSize.height) / 2, 10, 10)];
        dotView.center = dogBgView.center;
        dotView.backgroundColor = [UIColor blackColor];
        dotView.layer.cornerRadius = kDotSize.width / 2.0f;
        dotView.clipsToBounds = YES;
        dotView.hidden = YES;
        [self.view addSubview:dotView];
        [self.dotArray addObject:dotView];
        
        
        
    }
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if([string isEqualToString:@"\n"]) {
        [textField resignFirstResponder];
        return NO;
    } else if(string.length == 0) {
        return YES;
    }
    else if(textField.text.length >= kDotCount) {
        return NO;
    } else {
        return YES;
    }
}
- (void)clearUpPassword {
    self.textField.text = @"";
    [self textFieldDidChange:self.textField];
}
- (void)textFieldDidChange:(UITextField *)textField {
    for (UIView *dotView in self.dotArray) {
        dotView.hidden = YES;
    }
    for (int i = 0; i < textField.text.length; i++) {
        ((UIView *)[self.dotArray objectAtIndex:i]).hidden = NO;
    }
    for (int i = 0;i<self.dotBGArray.count;i++) {
        UIView *dotBgView = self.dotBGArray[i];
        UIView *shu = self.shuArray[i];
        if (textField.text.length == i) {
            dotBgView.backgroundColor = [UIColor whiteColor];
            dotBgView.layer.masksToBounds = NO;
            dotBgView.layer.shadowColor = HEXCOLOR(0xF0F0F0).CGColor;
            dotBgView.layer.shadowOffset = CGSizeMake(0,2.5);
            dotBgView.layer.shadowOpacity = 1;
            dotBgView.layer.shadowRadius = 7;
            shu.hidden = NO;
        }else{
            dotBgView.backgroundColor = HEXCOLOR(0xf0f0f0);
            dotBgView.layer.masksToBounds = NO;
            dotBgView.layer.shadowColor = [UIColor whiteColor].CGColor;
            dotBgView.layer.shadowOffset = CGSizeMake(0,2.5);
            dotBgView.layer.shadowOpacity = 1;
            dotBgView.layer.shadowRadius = 7;
            shu.hidden = YES;
        }
    }
    if (textField.text.length >= kDotCount) {
        if (self.type == ManMan_PayTypeSetupPassword) {
            JY_PayPasswordVC *payVC = [JY_PayPasswordVC alloc];
            payVC.delegate = self.delegate;
            payVC.type = ManMan_PayTypeRepeatPassword;
            payVC.enterType = self.enterType;
            payVC.lastPsw = self.textField.text;
            payVC.oldPsw = self.oldPsw;
            payVC = [payVC init];
            [g_navigation pushViewController:payVC animated:YES];
        }else if(self.type == ManMan_PayTypeRepeatPassword) {
            [self.nextBtn setUserInteractionEnabled:YES];
            [_nextBtn setBackgroundColor:HEXCOLOR(0x05D168)];
        } else if(self.type == ManMan_PayTypeInputPassword) {
            JY_UserObject *user = [[JY_UserObject alloc] init];
            user.payPassword = self.textField.text;
            [g_server checkPayPasswordWithUser:user toView:self];
        }
    }else {
        [self.nextBtn setUserInteractionEnabled:NO];
        [_nextBtn setBackgroundColor:[HEXCOLOR(0x05D168) colorWithAlphaComponent:0.5]];
    }
}
-(void) didServerResultSucces:(JY_Connection*)aDownload dict:(NSDictionary*)dict array:(NSArray*)array1{
    [_wait stop];
    if([aDownload.action isEqualToString:act_UpdatePayPassword]){
        [self.textField resignFirstResponder];
        if ([self.delegate respondsToSelector:@selector(updatePayPasswordSuccess:)]) {
            [self.delegate updatePayPasswordSuccess:self.textField.text];
        }
        [self clearUpPassword];
        [g_App showAlert:Localized(@"JX_SetUpSuccess")];
        g_server.myself.isPayPassword = [dict objectForKey:@"payPassword"];
        [self goBackToVC];
    }
    if([aDownload.action isEqualToString:act_CheckPayPassword]){
        [self goToSetupTypeVCWithOld:YES];
    }
}
-(int) didServerResultFailed:(JY_Connection*)aDownload dict:(NSDictionary*)dict{
    [_wait stop];
    return show_error;
}
-(int) didServerConnectError:(JY_Connection*)aDownload error:(NSError *)error{
    [_wait stop];
    return show_error;
}
-(void) didServerConnectStart:(JY_Connection*)aDownload{
    [_wait start];
}
- (void)goBackToVC {
    if (self.enterType == ManMan_EnterTypeDefault) {
        [g_navigation popToViewController:[JY_MoneyMenuViewController class] animated:YES];
    }
    else if (self.enterType == ManMan_EnterTypeWithdrawal){
        [g_navigation popToViewController:[JY_CashWithDrawViewController class] animated:YES];
    }
    else if (self.enterType == ManMan_EnterTypeTransfer){
        [g_navigation popToViewController:[JY_TransferViewController class] animated:YES];
    }
    else if (self.enterType == ManMan_EnterTypeQr){
        [g_navigation popToViewController:[JY_InputMoneyVC class] animated:YES];
    }
    else if (self.enterType == ManMan_EnterTypeSkPay){
        [g_navigation popToViewController:[webpageVC class] animated:YES];
    }
    else if (self.enterType == ManMan_EnterTypePayQr){
        [g_navigation popToViewController:[JY_PayViewController class] animated:YES];
    }
    else if (self.enterType == ManMan_EnterTypeBankCard){
        [g_navigation popToViewController:[JXOpenBank_VC class] animated:YES];
    }
    else {
        
        [g_navigation popToViewController:[JY_SendRedPacketViewController class] animated:YES];
    }
}
- (void)goToSetupTypeVCWithOld:(BOOL)isOld {
    JY_PayPasswordVC *payVC = [JY_PayPasswordVC alloc];
    payVC.delegate = self.delegate;
    payVC.type = ManMan_PayTypeSetupPassword;
    payVC.enterType = self.enterType;
    payVC.lastPsw = self.textField.text;
    payVC.oldPsw = isOld ? self.textField.text : self.oldPsw;
    payVC = [payVC init];
    [g_navigation pushViewController:payVC animated:YES];
}
#pragma mark - init
- (UITextField *)textField {
    if (!_textField) {
        _textField = [[JY_TextField alloc] init];
        _textField.backgroundColor = [UIColor whiteColor];
        _textField.textColor = [UIColor whiteColor];
        _textField.tintColor = [UIColor whiteColor];
        _textField.delegate = self;
        _textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        _textField.keyboardType = UIKeyboardTypeNumberPad;
//        _textField.layer.borderColor = [[UIColor blackColor] CGColor];
//        _textField.layer.borderWidth = 0.5;
        [_textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    }
    return _textField;
}
- (UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.textAlignment = NSTextAlignmentCenter;
        _titleLab.font = SYSFONT(20);
        _titleLab.textColor = HEXCOLOR(0x303030);
    }
    return _titleLab;
}
- (UILabel *)detailLab {
    if (!_detailLab) {
        _detailLab = [[UILabel alloc] init];
        _detailLab.textAlignment = NSTextAlignmentCenter;
    }
    return _detailLab;
}
- (UIButton *)nextBtn {
    if (!_nextBtn) {
        _nextBtn = [[UIButton alloc] init];
        [_nextBtn setTitle:@"完成" forState:UIControlStateNormal];
        [_nextBtn setBackgroundColor:[HEXCOLOR(0x05D168) colorWithAlphaComponent:0.6]];
        _nextBtn.userInteractionEnabled = NO;
        _nextBtn.layer.masksToBounds = YES;
        _nextBtn.layer.cornerRadius = 4.f;
        [self.nextBtn setHidden:YES];
        [_nextBtn addTarget:self action:@selector(didNextButton) forControlEvents:UIControlEventTouchUpInside];
    }
    return _nextBtn;
}
@end
