//
//  MYTView.m
//  shiku_im
//
//  Created by 冉彬 on 2020/10/29.
//  Copyright © 2020 Reese. All rights reserved.
//

#import "MYTView.h"
#import "UIButton+ActionBlock.h"

@implementation MYTView



- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
//        [self loadUI];
    }
    return self;
}



-(void)loadUIWithAry:(NSArray *)ary {
    self.vAry = ary;
    for (UIView *v in self.subviews) {
        [v removeFromSuperview];
    }
    UIScrollView *scV = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    scV.backgroundColor = [UIColor whiteColor];
    CGFloat y = 0;
    int index = 0;
    for (NSString *s in ary) {
        UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, y, self.frame.size.width, 40)];
        [btn setTitle:s forState:0];
        [btn setTitleColor:HEXCOLOR(0x333333) forState:0];
        btn.tag = 1000 + index;
        @weakify(self);
        [btn addClickActionBlock:^(UIButton * _Nonnull button) {
            weak_self.blocl(weak_self.vAry[button.tag - 1000]);
            [weak_self hide];
        }];
        y += 40;
        index ++;
        [scV addSubview:btn];
        scV.contentSize = CGSizeMake(self.frame.size.width, y);
    }
    [self addSubview:scV];
    
}



@end
