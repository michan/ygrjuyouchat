#import "JY_RedInputView.h"
#import "JY_RoomMemberListVC.h"
//#define RowHeight 48
//#define RowMaxHeight 60
//#define RowMargin 10
//#define RowMaxMargin 15

#define RowHeight 60
#define RowMaxHeight 60

#define RowMargin 10
#define RowMaxMargin 15
@interface JY_RedInputView () <UITextFieldDelegate> {
    CGFloat _commandY;
    CGFloat _greetY;
    CGFloat _countY;
    CGFloat _moneyY;
    CGFloat _sendY;
}
@end
@implementation JY_RedInputView
-(instancetype)initWithFrame:(CGRect)frame type:(NSUInteger)type isRoom:(BOOL)isRoom delegate:(id)delegate{
    if (self = [super initWithFrame:frame]) {
        self.frame = frame;
        self.type = type;
        self.delegate = delegate;
        self.isRoom = isRoom;
        self.markType = 1;
        [self customSubViews];    }
    return self;
}

-(instancetype)init{
    if (self = [super init]) {
        [self customSubViews];
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self customSubViews];
    }
    return self;
}

-(void)layoutSubviews{
    if (_type == 1 || _type == 3) { // 普通or拼手气。 or 专属
        if (_isRoom) {
            _moneyY = RowMargin + 10;
            _countY = _moneyY + RowHeight + RowMargin;
            _commandY = 10000;
            _greetY = _countY + RowHeight + RowMargin;
            _sendY = RowHeight*1+RowMaxHeight+RowMargin+RowMaxHeight+90+20;
        }else{
            _moneyY = RowMargin + 10;
            _greetY = _moneyY +RowMargin*2 + RowHeight;
            _sendY = RowHeight+RowMaxHeight+RowMargin+RowMaxHeight+60;
            _commandY = 10000;
            if (_type == 1) {
                _greetY = _moneyY + RowHeight + RowMaxMargin;
                _sendY = RowHeight*1+RowMaxHeight+RowMargin+RowMaxHeight+50;
            }
        }
    }else if (_type == 2) { // 口令
        if(_isRoom){
            _moneyY = RowMargin +50;
            _countY = _moneyY + RowHeight;
            _commandY = _countY + RowHeight;
            _greetY = _commandY + RowHeight;
            _sendY = RowHeight*1+RowMaxHeight+RowMargin+RowMaxHeight+90+20;
        }else{
            _moneyY = RowMargin +50;
            _commandY = _moneyY + RowHeight;
            _greetY = _commandY +RowMargin*2 + RowHeight;
            _sendY = RowHeight+RowMaxHeight+RowMargin+RowMaxHeight+60;
            if (_type == 1) {
                _greetY = _moneyY + RowHeight + RowMaxMargin;
                _sendY = RowHeight+RowMaxHeight+RowMargin+RowMaxHeight+50;
            }
        }
    }
    
    
    if(_isRoom){
        _countView.frame = CGRectMake(20, _countY, self.frame.size.width-40, RowHeight);
        _countUnit.frame = CGRectMake(CGRectGetWidth(_countView.frame)-40, 0, 40, RowHeight);
        _countTextField.frame = CGRectMake(CGRectGetMaxX(_countTitle.frame), 0, CGRectGetMinX(_countUnit.frame)-CGRectGetMaxX(_countTitle.frame), RowHeight);
        _selectImageView.frame = CGRectMake(CGRectGetWidth(_countView.frame)-30, RowHeight/2-10, 20, 20);
        _line1.frame = CGRectMake(15, RowHeight-LINE_WH, CGRectGetWidth(_countView.frame)-30, LINE_WH);
    }
    _moneyView.frame = CGRectMake(20, _moneyY, self.frame.size.width-40, RowHeight);
    _line.frame = CGRectMake(15, RowHeight-LINE_WH, CGRectGetWidth(_moneyView.frame)-30, LINE_WH);
    _moneyUnit.frame = CGRectMake(CGRectGetWidth(_moneyView.frame)-40, 0, 40, RowHeight);
    _moneyTextField.frame = CGRectMake(CGRectGetMaxX(_moneyTitle.frame), 0, CGRectGetMinX(_moneyUnit.frame)-CGRectGetMaxX(_moneyTitle.frame), RowHeight);
    _moneyTitleLeftImageView.hidden = NO;

    _commandView.frame = CGRectMake(20, _commandY, self.frame.size.width-40, _type == 1 || (_type == 2 && _isRoom) ? RowMaxHeight : RowHeight);
    _line3.frame = CGRectMake(15, RowHeight-LINE_WH, CGRectGetWidth(_commandView.frame)-30, LINE_WH);
    
    _greetView.frame = CGRectMake(20, _greetY, self.frame.size.width-40, _type == 1 || (_type == 2 && _isRoom) ? RowMaxHeight : RowHeight);
    _line2.frame = CGRectMake(15, RowHeight-LINE_WH, CGRectGetWidth(_greetView.frame)-30, LINE_WH);
    _sendButton.frame = CGRectMake(20, _sendY, self.frame.size.width-(20)*2, RowMaxHeight - 10);
    if (_isRoom) {
        _sendButton.tag = 2; // 拼手气
    }else {
        _sendButton.tag = 1; // 普通
    }
    
    
    _allMoneyLab.frame = CGRectMake(0, CGRectGetMinY(_sendButton.frame)-40 - 26, ManMan_SCREEN_WIDTH, 50);

    _greetTitle.hidden = NO;
    _noticeTitle.frame = CGRectMake(15 + 20, CGRectGetMaxY(_greetView.frame)+12, ManMan_SCREEN_WIDTH-15, 13);
    _greetTextField.frame = CGRectMake(CGRectGetMaxX(_greetTitle.frame), 0, CGRectGetWidth(_greetView.frame)-CGRectGetWidth(_greetTitle.frame)-15-20, _type == 1 || (_type == 2 && _isRoom)  ? RowMaxHeight : RowHeight);
    _commandTextField.frame = CGRectMake(CGRectGetMaxX(_commandTitle.frame), 0, CGRectGetWidth(_commandView.frame)-CGRectGetWidth(_commandTitle.frame)-15-20, _type == 1 || (_type == 2 && _isRoom)  ? RowMaxHeight : RowHeight);
    
    
    
    [self viewLocalized];
}

-(void)viewLocalized{
   
    _moneyTitle.text = Localized(@"JXRed_totalAmount");//@"总金额";//
    _countUnit.text = Localized(@"JXRed_A");//@"个";//
    _moneyUnit.text = Localized(@"JX_ChinaMoney");//@"元";//
    if (_isRoom) {
        [_sendButton setTitle:Localized(@"JXRed_send") forState:UIControlStateNormal];//@"发送群红包"
        [_sendButton setTitle:Localized(@"JXRed_send") forState:UIControlStateHighlighted];
    }else {
        [_sendButton setTitle:Localized(@"JXRed_send") forState:UIControlStateNormal];//@"塞钱进红包"
        [_sendButton setTitle:Localized(@"JXRed_send") forState:UIControlStateHighlighted];
    }
    
    _moneyTextField.placeholder = Localized(@"JXRed_inputAmount");//@"输入金额";//
    _countTextField.placeholder = Localized(@"JXRed_inputNumPackets");//@"请输入红包个数";//
    _commandTextField.placeholder = Localized(@"JXRed_orderPlace"); //@"如“我真帅”";// eg."I'm so handsome";
    switch (_type) {
        case 1:{
            if (_isRoom) {
                _noticeTitle.text = @"当前为普通红包，改为拼手气红包";//@"小伙伴领取的金额相同";//
                _countTitle.text = Localized(@"JXRed_numberPackets");// @"红包个数";//
            }
            _greetTextField.placeholder = Localized(@"JXRed_greetOlace");//@"恭喜发财，万事如意";// Congratulation, everything goes well
            break;
        }
        case 2:{
            _noticeTitle.text = @"当前为普通红包，改为拼手气红包";//@"小伙伴领取的金额随机";//
            _greetTextField.placeholder = Localized(@"JXRed_greetOlace");//@"恭喜发财，万事如意";
            _countTitle.text = Localized(@"JXRed_numberPackets");// @"红包个数";//
            break;
        }
        case 3:{
            _noticeTitle.text = Localized(@"JXRed_NoticeOrder");//@"小伙伴需回复口令抢红包";//
            _greetTextField.placeholder = Localized(@"JXRed_orderPlace");//@"如“我真帅”";// eg."I'm so handsome";
//            _greetTitle.text = Localized(@"JXRed_setOrder");//@"设置口令";//
            _countTitle.text = @"专属";// @"红包个数";//
            _countTextField.text = @"任何人";
            _countTextField.enabled = NO;
            _countUnit.hidden = YES;
            break;
        }
        default:
            break;
    }
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"当前为普通红包，改为拼手气红包" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 12],NSForegroundColorAttributeName: [UIColor colorWithRed:52/255.0 green:52/255.0 blue:52/255.0 alpha:1.0]}];


    [string addAttributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:198/255.0 green:134/255.0 blue:2/255.0 alpha:1.0]} range:NSMakeRange(8, string.length - 8)];


    _noticeTitle.attributedText = string;
    _noticeTitle.hidden = YES;
}

-(void)customSubViews{
    if(_isRoom){
        [self addSubview:self.countView];
        
    }
    [self addSubview:self.moneyView];
    if (_type != 2) {
        [self addSubview:self.greetView];
    }
    [self addSubview:self.commandView];
    
    [self addSubview:self.sendButton];
    if (_isRoom && _type == 1) {
        [self addSubview:self.noticeTitle];
    }
//    [self addSubview:self.noticeTipsTitle];
        [self addSubview:self.allMoneyLab];
//    [self insertSubview:self.bgImageView atIndex:0];
    self.backgroundColor = [UIColor groupTableViewBackgroundColor];
}

- (void)sendRedPacket {
    if ([self.delegate respondsToSelector:@selector(inputView:redPacketType:money:number:bless:command:exclusiveId:user:)]) {
        
        int redPacketType = 0;
        NSString *money = _moneyTextField.text;
        NSString *number = _countTextField.text;
        NSString *bless = _greetTextField.text;
        NSString *command = _commandTextField.text;
        NSString *exclusiveId = nil;
        //1是普通红包，2是手气红包，3是口令红包 ,4是专属红包
        if (_isRoom) {
//            if (_type == 1) {
////                redPacketType = (int)self.markType;
//                // 强制改
//                redPacketType = 2;
//            }else if (_type == 2) {
//                redPacketType = 3;
//            }else if (_type == 3) {
//                redPacketType = 4;
//                if (self.user) {
//                    exclusiveId = [NSString stringWithFormat:@"%ld", self.user.userId];
//                }
//            }
            redPacketType = 2;
        }else {
            if (_type == 1) {
                redPacketType = 1;
            }else if (_type == 2) {
                redPacketType = 3;
            }
        }
        
        [self endEditing:YES];
        [self.delegate inputView:self redPacketType:redPacketType money:money number:number bless:bless command:command exclusiveId:exclusiveId user:self.user];
    }
}

// 专属点击
- (void)theOnlyTap {
    if (_isRoom) {
        if (_isRoom) {
            JY_RoomMemberListVC *vc = [[JY_RoomMemberListVC alloc] init];
            vc.title = @"选择指定领取人";
            vc.room = self.room;
            vc.type = Type_RedPacketAssignUser;
            __weak typeof(self) weakSelf = self;
            vc.callBack = ^(memberData *user) {
                
                if ([user.role isEqual:@10]) {
                    weakSelf.countTextField.text = @"任何人";
                    weakSelf.user = nil;
                }else {
                    memberData *mData = [self.room getMember:g_myself.userId];
                    NSString *displayName = user.userNickName?user.userNickName:user.userName;
                    if (!weakSelf.room.allowSendCard && [mData.role intValue] != 1 && [mData.role intValue] != 2) {
                        NSString *tempDisplayName = [displayName substringToIndex:[displayName length]-1];
                        weakSelf.countTextField.text = [tempDisplayName stringByAppendingString:@"*"];
                    }else {
                        weakSelf.countTextField.text = displayName;
                    }
                    
                    weakSelf.user = user;
                }
            };
            [g_navigation pushViewController:vc animated:YES];
        }
    }
}


- (void)changeRedPacketType {
    if ([_noticeTitle.attributedText.string containsString:@"当前为普通红包"]) {
        NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"当前为拼手气红包，改为普通红包" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 12],NSForegroundColorAttributeName: [UIColor colorWithRed:52/255.0 green:52/255.0 blue:52/255.0 alpha:1.0]}];


        [string addAttributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:198/255.0 green:134/255.0 blue:2/255.0 alpha:1.0]} range:NSMakeRange(9, string.length - 9)];


        _noticeTitle.attributedText = string;
        
        _moneyTitleLeftImageView.hidden = NO;
        _moneyTitle.frame = CGRectMake(40, 0, 80, RowHeight);
        self.markType = 2;
    }else {
        NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"当前为普通红包，改为拼手气红包" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 12],NSForegroundColorAttributeName: [UIColor colorWithRed:52/255.0 green:52/255.0 blue:52/255.0 alpha:1.0]}];


        [string addAttributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:198/255.0 green:134/255.0 blue:2/255.0 alpha:1.0]} range:NSMakeRange(8, string.length - 8)];


        _noticeTitle.attributedText = string;
        
        _moneyTitleLeftImageView.hidden = YES;
        _moneyTitle.frame = CGRectMake(15, 0, 80, RowHeight);
        self.markType = 1;
    }
}


- (void)textFieldDidChange:(UITextField *)textField {
    _allMoneyLab.text = [NSString stringWithFormat:@"¥%.2f",[textField.text doubleValue]];
    _sendButton.enabled =  [_moneyTextField.text doubleValue] > 0;
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (textField == _countTextField) {
        if ([textField.text rangeOfString:@"."].location != NSNotFound) {
            return NO;
        }
    }
    if (textField == _moneyTextField) {
        if ([toBeString doubleValue] > 100000) {
            return NO;
        }
        // 首位不能输入 .
        if (IsStringNull(textField.text) && [string isEqualToString:@"."]) {
            return NO;
        }
        //限制.后面最多有两位，且不能再输入.
        if ([textField.text rangeOfString:@"."].location != NSNotFound) {
            //有.了 且.后面输入了两位  停止输入
            if (toBeString.length > [toBeString rangeOfString:@"."].location+3) {
                return NO;
            }
            //有.了，不允许再输入.
            if ([string isEqualToString:@"."]) {
                return NO;
            }
        }
        //限制首位0，后面只能输入. 和 删除
        if ([textField.text isEqualToString:@"0"]) {
            if (![string isEqualToString:@"."] && ![string isEqualToString:@""]) {
                return NO;
            }
        }
        //限制只能输入：1234567890.
        NSCharacterSet * characterSet = [[NSCharacterSet characterSetWithCharactersInString:@"1234567890"] invertedSet];
        NSString * filtered = [[string componentsSeparatedByCharactersInSet:characterSet] componentsJoinedByString:@""];
        return YES;//[string isEqualToString:filtered];
    }
    
    return YES;
}

- (UIImageView *)bgImageView {
    if (!_bgImageView) {
        if (_isRoom) {
            _bgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 400)];
        }else {
            _bgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 380)];
        }
        _bgImageView.image = [UIImage imageNamed:@"hg_send_bg"];
    }
    return _bgImageView;
}

- (UIImageView *)selectImageView {
    if (!_selectImageView) {
        _selectImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        _selectImageView.image = [UIImage imageNamed:@"set_list_next"];
    }
    return _selectImageView;
}

- (UILabel *)allMoneyLab {
    if (!_allMoneyLab) {
        _allMoneyLab = [[UILabel alloc] init];
        _allMoneyLab.font = [UIFont boldSystemFontOfSize:40];
        _allMoneyLab.text = @"¥0.00";
        _allMoneyLab.textAlignment = NSTextAlignmentCenter;
    }
    return _allMoneyLab;
}


-(UIView *)countView{
    if (!_countView) {
        _countView = [[UIView alloc] init];
            _countView.backgroundColor = [UIColor whiteColor];
        _countView.layer.cornerRadius = 6;
        _countView.layer.masksToBounds = YES;
        [_countView addSubview:self.countTitle];
        [_countView addSubview:self.countTextField];
        [_countView addSubview:self.countUnit];
        if (_type == 3) {
            [_countView addSubview:self.selectImageView];
            _commandTextField.userInteractionEnabled = YES;
            // 专属点击
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(theOnlyTap)];
            [_countView addGestureRecognizer:tap];
        }
//        [_countView addSubview:self.line1];
    }
    return _countView;
}

-(UIView *)moneyView{
    if (!_moneyView) {
        _moneyView = [[UIView alloc] init];
        _moneyView.backgroundColor = [UIColor whiteColor];
        _moneyView.layer.cornerRadius = 6;
        _moneyView.layer.masksToBounds = YES;
        [_moneyView addSubview:self.moneyTitle];
        [_moneyView addSubview:self.moneyTextField];
        [_moneyView addSubview:self.moneyUnit];
        if (_isRoom) {
            [_moneyView addSubview:self.moneyTitleLeftImageView];
        }
//        [_moneyView addSubview:self.line];
    }
    return _moneyView;
}

-(UIView *)greetView{
    if (!_greetView) {
        _greetView = [[UIView alloc] init];
        _greetView.backgroundColor = [UIColor whiteColor];
        _greetView.layer.cornerRadius = 6;
        _greetView.layer.masksToBounds = YES;
        [_greetView addSubview:self.greetTitle];
        [_greetView addSubview:self.greetTextField];
//        [_greetView addSubview:self.line2];
    }
    return _greetView;
}

-(UIView *)commandView{
    if (!_commandView) {
        _commandView = [[UIView alloc] init];
        _commandView.backgroundColor = [UIColor whiteColor];
        [_commandView addSubview:self.commandTitle];
        [_commandView addSubview:self.commandTextField];
        [_commandView addSubview:self.line3];
    }
    return _commandView;
}

-(UIButton *)sendButton{
    if (!_sendButton) {
        _sendButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_sendButton setBackgroundImage:[UIImage createImageWithColor:HEXCOLOR(0xFA585E)] forState:UIControlStateNormal];
        [_sendButton setBackgroundImage:[UIImage createImageWithColor:[HEXCOLOR(0xFA585E) colorWithAlphaComponent:0.5]] forState:UIControlStateDisabled];
        [_sendButton.titleLabel setFont:g_factory.font16];
        _sendButton.enabled = NO;
        _sendButton.layer.masksToBounds = YES;
        _sendButton.layer.cornerRadius = 7.f;
        [_sendButton addTarget:self action:@selector(sendRedPacket) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _sendButton;
}

-(UILabel *)noticeTitle{
    if (!_noticeTitle) {
        _noticeTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 20)];
        _noticeTitle.userInteractionEnabled = YES;
        _noticeTitle.font = g_factory.font13;
        _noticeTitle.textColor = [UIColor lightGrayColor];
        if (_isRoom && _type != 3) {
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeRedPacketType)];
            [_noticeTitle addGestureRecognizer:tap];
        }
    }
    return _noticeTitle;
}

-(UILabel *)noticeTipsTitle{
    if (!_noticeTipsTitle) {
        _noticeTipsTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, self.bounds.size.height - 80, self.bounds.size.width, 40)];
        _noticeTipsTitle.userInteractionEnabled = YES;
        _noticeTipsTitle.font = g_factory.font12;
        _noticeTipsTitle.textColor = [UIColor lightGrayColor];
        _noticeTipsTitle.numberOfLines = 2;
        _noticeTipsTitle.textAlignment = NSTextAlignmentCenter;
        _noticeTipsTitle.text = @"未领取的红包，将在24小时内发起退款";
    }
    return _noticeTipsTitle;
}

-(UILabel *)countTitle{
    if (!_countTitle) {
        _countTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 80, RowHeight)];
        _countTitle.font = g_factory.font15;
        _countTitle.textColor = [UIColor blackColor];
//        _countTitle.text = @"红包个数";
    }
    return _countTitle;
}
-(UILabel *)moneyTitle{
    if (!_moneyTitle) {
        if (_room) {
            
        }
        _moneyTitle = [[UILabel alloc] initWithFrame:CGRectMake(_isRoom ? 40 : 15, 0, 80, RowHeight)];
        _moneyTitle.font = g_factory.font15;
        _moneyTitle.textColor = [UIColor blackColor];
//        _moneyTitle.text = @"总金额";
    }
    return _moneyTitle;
}

-(UIImageView *)moneyTitleLeftImageView{
    if (!_moneyTitleLeftImageView) {
        _moneyTitleLeftImageView = [[UIImageView alloc] initWithFrame: CGRectMake(15, RowHeight/2-10, 20, 20)];
        _moneyTitleLeftImageView.image = [UIImage imageNamed:@"redpacketType_pin"];
    }
    return _moneyTitleLeftImageView;
}


-(UILabel *)greetTitle{
    if (!_greetTitle) {
        _greetTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 80, RowHeight)];
        _greetTitle.font = g_factory.font15;
        _greetTitle.textColor = [UIColor blackColor];
        _greetTitle.text = @"备注:";
    }
    return _greetTitle;
}

-(UILabel *)commandTitle{
    if (!_commandTitle) {
        _commandTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 80, RowHeight)];
        _commandTitle.font = g_factory.font15;
        _commandTitle.textColor = [UIColor blackColor];
        _commandTitle.text = @"口令:";
    }
    return _commandTitle;
}


-(UITextField *)countTextField{
    if (!_countTextField) {
        _countTextField = [UIFactory createTextFieldWith:CGRectZero delegate:_delegate returnKeyType:UIReturnKeyNext secureTextEntry:NO placeholder:nil font:g_factory.font15];
        _countTextField.text = @"";    // 红包默认最少为1个
//        _countTextField.clearButtonMode = UITextFieldViewModeNever;
        _countTextField.textAlignment = NSTextAlignmentRight;
        _countTextField.borderStyle = UITextBorderStyleNone;
        _countTextField.keyboardType = UIKeyboardTypeNumberPad;
        _countTextField.returnKeyType=UIReturnKeyDone;
        
    }
    return _countTextField;
}
-(UITextField *)moneyTextField{
    if (!_moneyTextField) {
        _moneyTextField = [UIFactory createTextFieldWith:CGRectZero delegate:self returnKeyType:UIReturnKeyNext secureTextEntry:NO placeholder:nil font:g_factory.font15];
//        _moneyTextField.clearButtonMode = UITextFieldViewModeNever;
        _moneyTextField.textAlignment = NSTextAlignmentRight;
        _moneyTextField.borderStyle = UITextBorderStyleNone;
        _moneyTextField.keyboardType = UIKeyboardTypeDecimalPad;
        _moneyTextField.returnKeyType=UIReturnKeyDone;
        
        [_moneyTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    }
    return _moneyTextField;
}
-(UITextField *)greetTextField{
    if (!_greetTextField) {
        _greetTextField = [UIFactory createTextFieldWith:CGRectZero delegate:_delegate returnKeyType:UIReturnKeyNext secureTextEntry:NO placeholder:nil font:g_factory.font15];
        _greetTextField.textAlignment = NSTextAlignmentRight;
        _greetTextField.borderStyle = UITextBorderStyleNone;
        _greetTextField.returnKeyType=UIReturnKeyDone;
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
        _greetTextField.leftView = view;
        _greetTextField.leftViewMode = UITextFieldViewModeAlways;
    }
    return _greetTextField;
}

-(UITextField *)commandTextField{
    if (!_commandTextField) {
        _commandTextField = [UIFactory createTextFieldWith:CGRectZero delegate:_delegate returnKeyType:UIReturnKeyNext secureTextEntry:NO placeholder:nil font:g_factory.font15];
        _commandTextField.textAlignment = NSTextAlignmentRight;
        _commandTextField.borderStyle = UITextBorderStyleNone;
        _commandTextField.keyboardType = UIKeyboardTypeDefault;
        _commandTextField.returnKeyType=UIReturnKeyDone;
    
    }
    return _commandTextField;
}


-(UILabel *)countUnit{
    if (!_countUnit) {
        _countUnit = [[UILabel alloc] initWithFrame:CGRectZero];
        _countUnit.font = g_factory.font15;
        _countUnit.textColor = [UIColor blackColor];
        _countUnit.textAlignment = NSTextAlignmentCenter;
//        _countUnit.text = @"个";
    }
    return _countUnit;
}
-(UILabel *)moneyUnit{
    if (!_moneyUnit) {
        _moneyUnit = [[UILabel alloc] initWithFrame:CGRectZero];
        _moneyUnit.font = g_factory.font15;
        _moneyUnit.textColor = [UIColor blackColor];
        _moneyUnit.textAlignment = NSTextAlignmentCenter;
//        _moneyUnit.text = @"元";
    }
    return _moneyUnit;
}

- (UIView *)line {
    if (!_line) {
        _line = [[UIView alloc] init];
        _line.backgroundColor = THE_LINE_COLOR;
    }
    return _line;
}

- (UIView *)line1 {
    if (!_line1) {
        _line1 = [[UIView alloc] init];
        _line1.backgroundColor = THE_LINE_COLOR;
    }
    return _line1;
}
- (UIView *)line2 {
    if (!_line2) {
        _line2 = [[UIView alloc] init];
        _line2.backgroundColor = THE_LINE_COLOR;
    }
    return _line2;
}

- (UIView *)line3 {
    if (!_line3) {
        _line3 = [[UIView alloc] init];
        _line3.backgroundColor = THE_LINE_COLOR;
    }
    return _line3;
}

-(void)stopEdit{
    
    
    [_countTextField resignFirstResponder];
    [_moneyTextField resignFirstResponder];
    [_greetTextField resignFirstResponder];
    [_commandTextField resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [_countTextField resignFirstResponder];
    [_moneyTextField resignFirstResponder];
    [_greetTextField resignFirstResponder];
    [_commandTextField resignFirstResponder];
    
    return YES;
}

@end
