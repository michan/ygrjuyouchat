#import <UIKit/UIKit.h>
#import "JY_PacketObject.h"
#import "JY_GetPacketList.h"
@interface JY_redPacketDetailVC : JY_TableViewController
@property (nonatomic,strong) NSString * redPacketId;
@property (nonatomic,strong) NSDictionary * dataDict;
@property (nonatomic,strong) NSArray * OpenMember;
@property (nonatomic,strong) JY_PacketObject * packetObj;
@property (nonatomic, assign) BOOL isGroup;  
@property (nonatomic, assign) int code; 
@property (strong, nonatomic) UIImageView * headImgV;
@property (strong, nonatomic) UIView *contentView;
@property (strong, nonatomic) UIImageView *headerImageView;
@property (strong, nonatomic) UILabel *totalMoneyLabel;
@property (strong, nonatomic) UILabel *fromUserLabel;
@property (strong, nonatomic) UILabel *greetLabel;
@property (strong, nonatomic) IBOutlet UILabel *showNumLabel;
@property (strong, nonatomic) UILabel * returnMoneyLabel;
@end
