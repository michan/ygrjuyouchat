#import "JY_admobViewController.h"
typedef NS_OPTIONS(NSInteger, ManMan_PayType) {
    ManMan_PayTypeSetupPassword,     
    ManMan_PayTypeRepeatPassword,    
    ManMan_PayTypeInputPassword,     
};
typedef NS_OPTIONS(NSInteger, ManMan_EnterType) {
    ManMan_EnterTypeDefault,            
    ManMan_EnterTypeWithdrawal,         
    ManMan_EnterTypeSendRedPacket,      
    ManMan_EnterTypeTransfer,           
    ManMan_EnterTypeQr,                 
    ManMan_EnterTypeSkPay,              
    ManMan_EnterTypePayQr,
    ManMan_EnterTypeCoinNum,
    ManMan_EnterTypeBankCard,

};
@protocol ManMan_PayPasswordVCDelegate <NSObject>
- (void)updatePayPasswordSuccess:(NSString *)payPassword;
@end
@interface JY_PayPasswordVC : JY_admobViewController
@property (nonatomic, assign) ManMan_PayType type;
@property (nonatomic, assign) ManMan_EnterType enterType;
@property (nonatomic, strong) NSString *lastPsw;
@property (nonatomic, strong) NSString *oldPsw;
@property (nonatomic, weak) id<ManMan_PayPasswordVCDelegate> delegate;
@end
