//
//  CertifyVC.m
//  shiku_im
//
//  Created by 冉彬 on 2020/11/5.
//  Copyright © 2020 Reese. All rights reserved.
//

#import "CertifyVC.h"
#import "View+MASAdditions.h"
#import "UIButton+ActionBlock.h"
#import "CertifyResultVC.h"
#import <RPSDK/RPSDK.h>
#import <AliyunIdentityManager/AliyunIdentityPublicApi.h>

@interface CertifyVC ()
@property (nonatomic, strong) JY_ImageView *head;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *statusLabel;
@property (nonatomic, strong) UILabel *goLabel;
@property (nonatomic, strong) UIImageView *mImV;
@property (nonatomic, strong) NSString *tokenString;
@property (nonatomic, assign) BOOL isCertiy;

@end

@implementation CertifyVC
 
//- (void)viewDidLoad {
//    [super viewDidLoad];
//    self.title = @"实名认证";
//    self.view.backgroundColor = HEXCOLOR(0xF2F2F2);
//    [self loadUI];
//    // Do any additional setup after loading the view.
//}
//
//

- (id)init
{
    self = [super init];
    if (self) {
        _wait = [ATMHud sharedInstance];
        self.isCertiy = NO;
        self.isGotoBack = YES;
        self.title = @"实名认证";
        self.heightHeader = ManMan_SCREEN_TOP;
        self.heightFooter = 0;
        [self createHeadAndFoot];
        self.tableBody.backgroundColor = [UIColor whiteColor];
//        self.tableBody.scrollEnabled = YES;
        [self loadUI];
        [_wait start];
        [g_server checkCerityTokenToView:self];
        [g_server getCerityTokenToView:self];
    }
    return self;
}


-(void)loadUI
{
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 55)];
    view.backgroundColor = HEXCOLOR(0xffffff);
    [self.tableBody addSubview:view];
    self.tableBody.backgroundColor = HEXCOLOR(0xf0f0f0);
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(18, 16, 80, 23)];
    label.text = @"身份认证";
    label.font = [UIFont systemFontOfSize:15];
    label.textColor = HEXCOLOR(0x303030);
    [view addSubview:label];
    
    _head = [[JY_ImageView alloc] initWithFrame:CGRectMake(18, 7.5, 40, 40)];
//    _head.layer.cornerRadius = 3;
//    _head.layer.masksToBounds = YES;
//    _head.didTouch = @selector(onResume);
//    _head.delegate = self;
    
    
    
//    [view addSubview:_head];
    
    CGFloat nx = _head.frame.origin.x + _head.frame.size.width + 10;
    UILabel *nikeNameLab = [UILabel new];
    nikeNameLab.textColor = HEXCOLOR(0x333333);
    nikeNameLab.font = SYSFONT(16);
    nikeNameLab.text = @"";
    nikeNameLab.frame = CGRectMake(nx, _head.frame.origin.y + 3, ManMan_SCREEN_WIDTH - nx - 18, 17);
//    nikeNameLab.center = CGPointMake(nikeNameLab.center.x, _head.center.y);
    _nameLabel = nikeNameLab;
//    [view addSubview:_nameLabel];
    
    UILabel *sLabel = [UILabel new];
    sLabel.textColor = [UIColor redColor];
    sLabel.font = SYSFONT(13);
    sLabel.text = @"审核中";
    sLabel.frame = CGRectMake(nx, _head.frame.origin.y + 3 + 17, ManMan_SCREEN_WIDTH - nx - 18, 17);
    self.statusLabel = sLabel;
//    [view addSubview:self.statusLabel];
    
    
    UILabel *tLabel = [UILabel new];
    tLabel.textColor = [UIColor lightGrayColor];
    tLabel.font = SYSFONT(13);
    tLabel.text = @"立即认证";
    [tLabel sizeToFit];
    tLabel.frame = CGRectMake(ManMan_SCREEN_WIDTH - tLabel.frame.size.width - 18 - 8 - 18, _head.frame.origin.y + 3 + 17, tLabel.frame.size.width, 17);
    tLabel.center = CGPointMake(tLabel.center.x, _head.center.y);
    self.goLabel = tLabel;
    [view addSubview:tLabel];
    
    
    UIImageView *morePic = [UIImageView new];
    morePic.image = [UIImage imageNamed:@"more_flag"];
    morePic.frame = CGRectMake(ManMan_SCREEN_WIDTH - 18 - 12, 0, 8, 12);
    morePic.center = CGPointMake(morePic.center.x, _head.center.y);
    self.mImV = morePic;
    [view addSubview:morePic];
    
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, _head.frame.origin.y, ManMan_SCREEN_WIDTH, 40)];
    @weakify(self)
    [btn addClickActionBlock:^(UIButton * _Nonnull button) {
        [weak_self certifyBtnAction];
    }];
    [self.tableBody addSubview:btn];
    [self refreshUI];
    [self doRefresh];

}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


-(void)refreshUI {
    if (self.isCertiy) {
        self.goLabel.text = @"已认证";
        self.statusLabel.text = @"已认证";
        self.statusLabel.textColor = [UIColor redColor];
        self.goLabel.hidden = NO;
        self.mImV.hidden = YES;
    }
    else
    {
        self.goLabel.text = @"立即认证";
        self.statusLabel.text = @"未认证";
        self.statusLabel.textColor = [UIColor lightGrayColor];
        self.goLabel.hidden = NO;
        self.mImV.hidden = NO;
        
    }
}



-(void)doRefresh{
    _head.image = nil;
    [g_server delHeadImage:g_server.myself.userId];
    [_head sd_setImageWithURL:[NSURL URLWithString:[g_server getHeadImageTUrl:g_server.myself.userId]] placeholderImage:nil];
    _nameLabel.text = g_server.myself.userNickname;
}

-(void)certifyBtnAction
{
    NSLog(@"去认证");
    id dict =  [AliyunIdentityManager getMetaInfo];
    
    if (self.isCertiy) {
        return;
    }
    
//    CertifyResultVC *vc = [[CertifyResultVC alloc] init];
//    [g_navigation pushViewController:vc animated:YES];
    @weakify(self)
    
    NSDictionary *extParams = [[NSMutableDictionary alloc]init];
    [extParams setValue:self forKey:@"currentCtr"];
    [extParams setValue:@"05D168" forKey:ZIM_EXT_PARAMS_KEY_OCR_BOTTOM_BUTTON_COLOR];
    [extParams setValue:@"05D168" forKey:ZIM_EXT_PARAMS_KEY_OCR_BOTTOM_BUTTON_CLICKED_COLOR];
    [extParams setValue:@"05D168" forKey:ZIM_EXT_PARAMS_KEY_OCR_FACE_CIRCLE_COLOR];
//    [extParams setValue:@"true" forKey:ZIM_EXT_PARAMS_KEY_USE_VIDEO_UPLOAD];

    if (!self.tokenString || self.tokenString.length == 0 ) {
        [g_server getCerityTokenToView:self];
        return;
    }
    [[AliyunIdentityManager sharedInstance] verifyWith:self.tokenString extParams:extParams onCompletion:^(ZIMResponse *response) {
         dispatch_async(dispatch_get_main_queue(), ^{
             
             NSString *title = @"刷脸成功";
             switch (response.code) {
                 case 1000:
                 {
                     [self->_wait start];
                     [g_server saveCerityTokenToView:weak_self];
                 }
                     break;
                 case 1003:
                     title = @"用户退出";
                     break;
                 case 2002:
                     title = @"网络错误";
                     break;
                 case 2006:
                     title = @"刷脸失败";
                     break;
                 default:
                     break;
             }
             [JY_MyTools showTipView:title];
         });
    }];
    return;
    [RPSDK startWithVerifyToken:self.tokenString
                 viewController:self
                     completion:^(RPResult * _Nonnull result) {
        // 建议接入方调用实人认证服务端接口DescribeVerifyResult，来获取最终的认证状态，并以此为准进行业务上的判断和处理。
        NSLog(@"实人认证结果：%@", result);
        switch (result.state) {
            case RPStatePass:
            {
                // 认证通过。
//                weak_self
                dispatch_async(dispatch_get_main_queue(), ^{
                    [_wait start];
                    [g_server saveCerityTokenToView:weak_self];
                });
            }
                
                break;
            case RPStateFail:
                // 认证不通过。
                [JY_MyTools showTipView:@"认证不通过"];
                break;
            case RPStateNotVerify:
                [JY_MyTools showTipView:@"认证失败"];
                // 未认证。
                // 通常是用户主动退出或者姓名身份证号实名校验不匹配等原因导致。
                // 具体原因可通过result.errorCode来区分（详见文末错误码说明表格）。
                break;
        }
    }];
}



-(void) didServerResultSucces:(JY_Connection*)aDownload dict:(NSDictionary*)dict array:(NSArray*)array1{
    [_wait stop];
    if ([aDownload.action isEqualToString:act_GetCerityToken]) {
        NSString *tokenStr = [dict objectForKey:@"resultObject"];
        self.tokenString = tokenStr;
    }
    else if ([aDownload.action isEqualToString:act_CheckCerityToken])
    {
        NSDictionary *tempDict = [aDownload.responseData mj_JSONObject];
        BOOL isc = [[tempDict objectForKey:@"data"] boolValue];
        self.isCertiy = isc;
        [self refreshUI];
    }
    else if ([aDownload.action isEqualToString:act_SaveCerityToken])
    {
        self.isCertiy = YES;
        [self refreshUI];
    }
    
    
    
}

-(int) didServerResultFailed:(JY_Connection*)aDownload dict:(NSDictionary*)dict{
    [_wait stop];
    return hide_error;
}

-(int) didServerConnectError:(JY_Connection*)aDownload error:(NSError *)error{//error为空时，代表超时
    [_wait stop];
    return hide_error;
}






@end
