#import "JY_MoneyMenuViewController.h"
#import "JY_RecordCodeVC.h"
#import "JY_PayPasswordVC.h"
#import "JY_forgetPwdVC.h"
#define HEIGHT 56
@interface JY_MoneyMenuViewController ()<forgetPwdVCDelegate>
@end
@implementation JY_MoneyMenuViewController
- (instancetype)init {
    self = [super init];
    if (self) {
        self.title = @"支付密码";
        self.heightHeader = ManMan_SCREEN_TOP;
        self.heightFooter = 0;
        self.isGotoBack = YES;
        [self createHeadAndFoot];
        int h=0;
        int w=ManMan_SCREEN_WIDTH;
        JY_ImageView* iv;
//        iv = [self createButton:Localized(@"JX_Bill") drawTop:NO drawBottom:YES click:@selector(onBill)];
//        iv.frame = CGRectMake(0,h, w, HEIGHT);
//        h+=iv.frame.size.height;
        iv = [self createButton:@"设置支付密码" drawTop:NO drawBottom:YES  click:@selector(onPayThePassword)];
        iv.frame = CGRectMake(0,h, w, HEIGHT);
        h+=iv.frame.size.height;
        iv = [self createButton:@"忘记支付密码" drawTop:NO drawBottom:NO  click:@selector(onForgetPassWord)];
        iv.frame = CGRectMake(0,h, w, HEIGHT);
        h+=iv.frame.size.height;
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}
- (void)onBill {
    JY_RecordCodeVC * recordVC = [[JY_RecordCodeVC alloc]init];
    recordVC.type = ManMan_RecordTypePayBill;
    [g_navigation pushViewController:recordVC animated:YES];
}
- (void)onPayThePassword {
    JY_PayPasswordVC * PayVC = [JY_PayPasswordVC alloc];
    if ([g_server.myself.isPayPassword boolValue]) {
        PayVC.type = ManMan_PayTypeInputPassword;
    }else {
        PayVC.type = ManMan_PayTypeSetupPassword;
    }
    PayVC.enterType = ManMan_EnterTypeDefault;
    PayVC = [PayVC init];
    [g_navigation pushViewController:PayVC animated:YES];
}
- (void)onForgetPassWord {
    JY_forgetPwdVC* vc = [[JY_forgetPwdVC alloc] init];
    vc.delegate = self;
    vc.isPayPWD = YES;
    vc.isModify = NO;
    [g_navigation pushViewController:vc animated:YES];
}
- (void)forgetPwdSuccess {
    g_server.myself.isPayPassword = nil;
    [self onPayThePassword];
}
- (void)viewDidLoad {
    [super viewDidLoad];
}
-(JY_ImageView*)createButton:(NSString*)title drawTop:(BOOL)drawTop drawBottom:(BOOL)drawBottom click:(SEL)click{
    JY_ImageView* btn = [[JY_ImageView alloc] init];
    btn.backgroundColor = [UIColor whiteColor];
    btn.userInteractionEnabled = YES;
    btn.didTouch = click;
    btn.delegate = self;
    [self.tableBody addSubview:btn];
    JY_Label* p = [[JY_Label alloc] initWithFrame:CGRectMake(15, 0, self_width-15, HEIGHT)];
    p.text = title;
    p.font = g_factory.font17;
    p.backgroundColor = [UIColor clearColor];
    p.textColor = HEXCOLOR(0x323232);
    [btn addSubview:p];
    if(drawTop){
        UIView* line = [[UIView alloc] initWithFrame:CGRectMake(15,0,ManMan_SCREEN_WIDTH-20,LINE_WH)];
        line.backgroundColor = THE_LINE_COLOR;
        [btn addSubview:line];
    }
    if(drawBottom){
        UIView* line = [[UIView alloc] initWithFrame:CGRectMake(15,HEIGHT-LINE_WH,ManMan_SCREEN_WIDTH-15,LINE_WH)];
        line.backgroundColor = THE_LINE_COLOR;
        [btn addSubview:line];
    }
    if(click){
        UIImageView* iv;
        iv = [[UIImageView alloc] initWithFrame:CGRectMake(ManMan_SCREEN_WIDTH-15-7, (HEIGHT-13)/2, 7, 13)];
        iv.image = [UIImage imageNamed:@"new_icon_>"];
        [btn addSubview:iv];
    }
    return btn;
}
@end
