#import "JY_RecordCodeVC.h"
#import "JY_RecordTBCell.h"
#import "JY_PayPasswordVC.h"
@interface JY_RecordCodeVC ()
@end
@implementation JY_RecordCodeVC
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.heightHeader = ManMan_SCREEN_TOP;
        self.heightFooter = 0;
        self.isGotoBack = YES;
        _dataArr = [NSMutableArray array];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self customView];
    [self getServerData];
}
-(void)getServerData{
    [_wait start];
    if (self.type == ManMan_RecordTypePayBill) {
        [g_server getConsumeRecord:_page toView:self];
    }else if(self.type == ManMan_RecordTypeRedPacket) {
        [g_server getAutoGetAssignRedReceiveRecord:_page toView:self];
    }else if(self.type == ManMan_RecordTypeRedPacketReceive) {
        long time = self.selectedTime;
        NSDate *date = [NSDate date];
        NSTimeInterval timeSecond = [date timeIntervalSince1970];
        if (time == 0) {
            // 所有
            [g_server redPacketGetRedReceiveListIndex:_page StartTime:0 EndTime:timeSecond toView:self];
        }else {
            NSTimeInterval startTime = timeSecond - time;
            [g_server redPacketGetRedReceiveListIndex:_page StartTime:startTime EndTime:timeSecond toView:self];
        }
    }else if(self.type == ManMan_RecordTypeRedPacketSend) {
        [g_server redPacketGetSendRedPacketListIndex:_page toView:self];
    }
    
}

- (void)onPayThePassword {
    JY_PayPasswordVC * PayVC = [JY_PayPasswordVC alloc];
    if ([g_server.myself.isPayPassword boolValue]) {
        PayVC.type = ManMan_PayTypeInputPassword;
    }else {
        PayVC.type = ManMan_PayTypeSetupPassword;
    }
    PayVC.enterType = ManMan_EnterTypeDefault;
    PayVC = [PayVC init];
    [g_navigation pushViewController:PayVC animated:YES];
}

- (void)customView{
    if (self.type == ManMan_RecordTypePayBill) {
        self.title = Localized(@"JXRecordCodeVC_Title");
    }else if (self.type == ManMan_RecordTypeRedPacket) {
        self.title = @"自动领取红包记录";
    }else if (self.type == ManMan_RecordTypeRedPacketReceive) {
        self.title = @"收到的红包";
    }else if(self.type == ManMan_RecordTypeRedPacketSend) {
        self.title = @"发出的红包";
    }

    [self createHeadAndFoot];
    _table.delegate = self;
    _table.dataSource = self;
    _table.separatorStyle = UITableViewCellSeparatorStyleNone;
    _table.allowsSelection = NO;
    if (@available(iOS 11.0, *)) {
        _table.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
}
-(void)getDataObjFromArr:(NSMutableArray*)arr{
    [_table reloadData];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_dataArr count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    JY_RecordTBCell * cell = [tableView dequeueReusableCellWithIdentifier:@"JY_RecordTBCell"];
    NSDictionary * cellModel = _dataArr[indexPath.row];
    if (cell == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"JY_RecordTBCell" owner:self options:nil][0];
    }
    NSString *descString = [cellModel objectForKey:@"desc"];
    if (descString == nil) {
        descString = [cellModel objectForKey:@"userName"];
    }
    
    cell.titleLabel.text = descString;
    
    NSString *timeString = [cellModel objectForKey:@"time"];
    if (timeString == nil) {
        timeString =  [cellModel objectForKey:@"sendTime"];
    }
    
    NSTimeInterval  creatTime = [timeString  doubleValue];
    NSDate * date = [NSDate dateWithTimeIntervalSince1970:creatTime];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:8*60*60]];
    cell.timeLabel.text = [dateFormatter stringFromDate:date];
    NSString *symbolStr;
    int type = [cellModel[@"type"] intValue];
    if (type == 2 || type == 4 || type == 7 || type == 10 || type == 12 || type == 14|| type == 16) {
        symbolStr = @"-";
        cell.moneyLabel.textColor = [UIColor blackColor];
    }else {
        symbolStr = @"+";
        cell.moneyLabel.textColor = THEMECOLOR;
    }
    cell.moneyLabel.text = [NSString stringWithFormat:@"%@%@ %@",symbolStr,cellModel[@"money"],Localized(@"JX_ChinaMoney")];
    cell.refundLabel.text = @"";
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 68;
}
-(void) didServerResultSucces:(JY_Connection*)aDownload dict:(NSDictionary*)dict array:(NSArray*)array1{
    [_wait stop];
    [self stopLoading];
    if ([aDownload.action isEqualToString:act_consumeRecord]) {
        if (dict == nil) {
            return;
        }
        _footer.hidden = [(NSArray *)dict[@"pageData"] count] < 20;
        if(_page == 0){
            [_dataArr removeAllObjects];
            [_dataArr addObjectsFromArray:dict[@"pageData"]];
        }else{
            if([(NSArray *)dict[@"pageData"] count]>0){
                [_dataArr addObjectsFromArray:dict[@"pageData"]];
            }
        }
        _page ++;
        [self getDataObjFromArr:_dataArr];
    }
    
    // 自动收到的指定红包
    if ([aDownload.action isEqualToString:act_autoGetAssignRedReceiveRecord]) {
        //添加到数据源
               if (array1 == nil) {
                   return;
               }
               if (_page == 0) {
                   _dataArr = [[NSMutableArray alloc]initWithArray:array1];
               }else if(_page > 0){
                   [_dataArr addObjectsFromArray:array1];
               }else{
                   //没有更多数据
               }
               
               [self getDataObjFromArr:_dataArr];
    }
    
    // 收到的红包
    if ([aDownload.action isEqualToString:act_redPacketGetRedReceiveList]) {
        //添加到数据源
               if (dict == nil) {
                   return;
               }
        
        
               if (_page == 0) {
                   _dataArr = [[NSMutableArray alloc]initWithArray:dict[@"list"]];
               }else if(_page > 0){
                   [_dataArr addObjectsFromArray:dict[@"list"]];
               }else{
                   //没有更多数据
               }
               
               [self getDataObjFromArr:_dataArr];
    }
    
    // 收发出的红包
       if ([aDownload.action isEqualToString:act_redPacketGetSendRedPacketList]) {
           //添加到数据源
                  if (dict == nil) {
                      return;
                  }
           
           
                  if (_page == 0) {
                      _dataArr = [[NSMutableArray alloc]initWithArray:dict[@"list"]];
                  }else if(_page > 0){
                      [_dataArr addObjectsFromArray:dict[@"list"]];
                  }else{
                      //没有更多数据
                  }
                  
                  [self getDataObjFromArr:_dataArr];
       }
}
-(int) didServerResultFailed:(JY_Connection*)aDownload dict:(NSDictionary*)dict{
    [_wait stop];
    [self stopLoading];
    return hide_error;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
@end
