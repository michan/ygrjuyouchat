#import "JY_TableViewController.h"

typedef NS_ENUM(NSUInteger, ManMan_RecordType) {
    ManMan_RecordTypePayBill,
    ManMan_RecordTypeRedPacket, // 指定红包
    ManMan_RecordTypeRedPacketReceive, // 收到的红包
    ManMan_RecordTypeRedPacketSend, // 发出的红包
};

@interface JY_RecordCodeVC : JY_TableViewController
@property (strong, nonatomic) NSMutableArray * dataArr;
@property (nonatomic,strong) NSMutableArray * dataObjArray;
@property (nonatomic,assign) ManMan_RecordType type;//消息记录对象数组
@property(nonatomic, assign) long selectedTime;
@end
