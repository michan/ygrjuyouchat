#import "JY_TableViewController.h"
@protocol RechargeDelegate <NSObject>
-(void)rechargeSuccessed;
@end
@interface JY_RechargeViewController : JY_admobViewController
@property (nonatomic, weak) id<RechargeDelegate> rechargeDelegate;
@property (nonatomic,assign) BOOL isQuitAfterSuccess;
@end
