//
//  MYTView.h
//  shiku_im
//
//  Created by 冉彬 on 2020/10/29.
//  Copyright © 2020 Reese. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FWPopupBaseView.h"
NS_ASSUME_NONNULL_BEGIN

typedef void (^MYTViewBlock)(NSString *str);


@interface MYTView : FWPopupBaseView

@property (nonatomic, strong) NSArray *vAry;
@property (nonatomic, copy) MYTViewBlock blocl;

-(void)loadUIWithAry:(NSArray *)ary;

@end

NS_ASSUME_NONNULL_END
