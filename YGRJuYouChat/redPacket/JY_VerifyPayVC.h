#import "JY_admobViewController.h"
typedef NS_OPTIONS(NSInteger, ManMan_VerifyType) {
    ManMan_VerifyTypeWithdrawal,         
    ManMan_VerifyTypeSendReadPacket,     
    ManMan_VerifyTypeTransfer,           
    ManMan_VerifyTypeQr,                 
    ManMan_VerifyTypeSkPay,              
    ManMan_VerifyTypePaymentCode,
    ManMan_VerifyTypeUserCancel,
};
@interface JY_VerifyPayVC : JY_admobViewController
@property (nonatomic, assign) ManMan_VerifyType type;
@property (nonatomic, strong) NSString *RMB;
@property (nonatomic, strong) NSString *titleStr;
@property (nonatomic, assign) SEL didDismissVC;
@property (nonatomic, assign) id delegate;
@property (nonatomic, assign) SEL didVerifyPay;
- (void)clearUpPassword;
- (NSString *)getMD5Password;
- (void)didDismissVerifyPayVC;
@end
