#import "JY_SendRedPacketViewController.h"
#import "JY_TopSiftJobView.h"
#import "JY_RedInputView.h"
#import "JY_RechargeViewController.h"
#import "JY_VerifyPayVC.h"
#import "JY_PayPasswordVC.h"
#import "JY_PayServer.h"
#import "WXApi.h"

#import "JXp1a1y1WithBankMoney_VC.h"

#define TopHeight 40
@interface JY_SendRedPacketViewController ()<UITextFieldDelegate,UIScrollViewDelegate,RechargeDelegate,ManMan_RedInputViewDelegate>
@property (nonatomic, strong) JY_TopSiftJobView * topSiftView;
@property (nonatomic, strong) JY_RedInputView * luckyView;
@property (nonatomic, strong) JY_RedInputView * nomalView;
@property (nonatomic, strong) JY_RedInputView * orderView;
@property (nonatomic, strong) JY_VerifyPayVC * verVC;
@property (nonatomic, strong) memberData * user;
@property (nonatomic, copy) NSString * moneyText;
@property (nonatomic, copy) NSString * countText;
@property (nonatomic, copy) NSString * greetText;
@property (nonatomic, assign) NSInteger indexInt;
@property (nonatomic, strong) UIButton *button;
@end
@implementation JY_SendRedPacketViewController
-(instancetype)init{
    if (self = [super init]) {
        
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent]; 
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.isGotoBack = NO;
    
    self.heightFooter = 0;
    self.heightHeader = 0;
    
    [self createHeadAndFoot];
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(endEdit:)];
    [self.tableBody addGestureRecognizer:tap];

    if (_isRoom) {
        self.tableBody.contentSize = CGSizeMake(ManMan_SCREEN_WIDTH, self.tableBody.frame.size.height/1.2);
    }else{
        self.tableBody.contentSize = CGSizeMake(ManMan_SCREEN_WIDTH, self.tableBody.frame.size.height/1.2);
    }
    
    self.tableBody.delegate = self;
    self.tableBody.pagingEnabled = YES;
    self.tableBody.showsHorizontalScrollIndicator = NO;
    self.tableBody.backgroundColor = [UIColor clearColor];
    
    [self.view addSubview:self.topSiftView];
    [self.tableBody addSubview:self.nomalView];
//    [self.tableBody addSubview:self.orderView];
//    if(_isRoom){
//        [self.tableBody addSubview:self.luckyView];
//        [_luckyView.sendButton addTarget:self action:@selector(sendRedPacket:) forControlEvents:UIControlEventTouchUpInside];
//    }else {
//
//    }
    
    
    [_nomalView.sendButton addTarget:self action:@selector(sendRedPacket:) forControlEvents:UIControlEventTouchUpInside];
//    [_orderView.sendButton addTarget:self action:@selector(sendRedPacket:) forControlEvents:UIControlEventTouchUpInside];
    [self createHeaderView];
}
-(void)createHeaderView{
    int heightHeader = ManMan_SCREEN_TOP;
    
    UIView *  tableHeader = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ManMan_SCREEN_WIDTH, heightHeader)];
    tableHeader.backgroundColor = [UIColor clearColor];
    UIImageView* iv = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, ManMan_SCREEN_WIDTH, heightHeader)];
    iv.image = [UIImage imageNamed:@""];
    iv.userInteractionEnabled = YES;
    [tableHeader addSubview:iv];
    
    
    UIImageView* ivbg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, ManMan_SCREEN_WIDTH, 250)];
    ivbg.image = [UIImage imageNamed:@""];
    ivbg.contentMode = UIViewContentModeScaleToFill;
    ivbg.userInteractionEnabled = NO;
    ivbg.backgroundColor = [UIColor whiteColor];
    [self.view insertSubview:ivbg atIndex:0];
    
    
    
    JY_Label* p = [[JY_Label alloc]initWithFrame:CGRectMake(40, ManMan_SCREEN_TOP -15- 17, ManMan_SCREEN_WIDTH, 20)];
    p.center = CGPointMake(tableHeader.center.x, p.center.y);
    p.backgroundColor = [UIColor clearColor];
    p.textAlignment   = NSTextAlignmentCenter;
    p.textColor       = [UIColor blackColor];
    p.font = [UIFont systemFontOfSize:20.0];
    p.text = Localized(@"JXredPacket");
    p.userInteractionEnabled = YES;
    p.didTouch = @selector(actionTitle:);
    p.delegate = self;
    p.changeAlpha = NO;
    [tableHeader addSubview:p];
        
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, ManMan_SCREEN_TOP - 46, 46, 46)];
    [btn setBackgroundImage:[[UIImage imageNamed:@"title_back_black_big"] imageWithTintColor:[UIColor blackColor]] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(actionQuit) forControlEvents:UIControlEventTouchUpInside];
    [tableHeader addSubview:btn];
    
    [g_notify addObserver:self selector:@selector(changeKeyBoard:) name:UIKeyboardWillShowNotification object:nil];
    
    [self.view addSubview:tableHeader];
}
-(JY_RedInputView *)luckyView{
    if (!_luckyView) {
        _luckyView = [[JY_RedInputView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_orderView.frame), ManMan_SCREEN_TOP+60, ManMan_SCREEN_WIDTH, self.tableBody.contentSize.height-ManMan_SCREEN_TOP-40) type:3 isRoom:_isRoom delegate:self];
        _luckyView.room = self.room;
    }
    return _luckyView;
}
-(JY_RedInputView *)nomalView{
    if (!_nomalView) {
        _nomalView = [[JY_RedInputView alloc] initWithFrame:CGRectMake(0, ManMan_SCREEN_TOP, ManMan_SCREEN_WIDTH, ManMan_SCREEN_HEIGHT) type:1 isRoom:_isRoom delegate:self];
        _nomalView.room = self.room;
    }
    return _nomalView;
}
-(JY_RedInputView *)orderView{
    if (!_orderView) {
        _orderView = [[JY_RedInputView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_nomalView.frame), ManMan_SCREEN_TOP+60, ManMan_SCREEN_WIDTH, self.tableBody.contentSize.height-ManMan_SCREEN_TOP-40) type:2 isRoom:_isRoom delegate:self];
        _orderView.room = self.room;
    }
    return _orderView;
}
-(JY_TopSiftJobView *)topSiftView{
    if (!_topSiftView) {
        _topSiftView = [[JY_TopSiftJobView alloc] initWithFrame:CGRectMake(0, ManMan_SCREEN_TOP, ManMan_SCREEN_WIDTH, 40)];
        _topSiftView.delegate = self;
        _topSiftView.isShowMoreParaBtn = NO;
        _topSiftView.preferred = 0;
        _topSiftView.backgroundColor = [UIColor clearColor];
        _topSiftView.titleNormalColor = [UIColor clearColor];
        _topSiftView.titleSelectedColor = [UIColor clearColor];
        _topSiftView.isShowBottomLine = NO;
        NSArray * itemsArray;
//        if (_isRoom) {
////            itemsArray = [[NSArray alloc] initWithObjects:Localized(@"JX_LuckGift"),Localized(@"JX_UsualGift"),Localized(@"JX_MesGift"), nil];
//            itemsArray = [[NSArray alloc] initWithObjects:@"普通",@"口令",@"专属", nil];
//        }else{
////            itemsArray = [[NSArray alloc] initWithObjects:Localized(@"JX_UsualGift"),Localized(@"JX_MesGift"), nil];
//            itemsArray = [[NSArray alloc] initWithObjects:@"普通",@"口令", nil];
//        }
        itemsArray = [[NSArray alloc] initWithObjects:@"普通", nil];
        _topSiftView.dataArray = itemsArray;
//        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 40 - LINE_WH, ManMan_SCREEN_WIDTH, LINE_WH)];
//        line.backgroundColor = HEXCOLOR(0x149DFB);
//        [_topSiftView addSubview:line];
    }
    return _topSiftView;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (void)topItemBtnClick:(UIButton *)btn{
    [self checkAfterScroll:(btn.tag-100)];
}
- (void)checkAfterScroll:(CGFloat)offsetX{
    [self.tableBody setContentOffset:CGPointMake(offsetX*ManMan_SCREEN_WIDTH, 0) animated:YES];
    [self endEdit:nil];
}

-(void)changeKeyBoard:(NSNotification *)aNotifacation
{
 
    
    self.tableBody.scrollEnabled=NO;
}

-(void)endEdit:(UIGestureRecognizer *)ges{
    [_luckyView stopEdit];
    [_nomalView stopEdit];
    [_orderView stopEdit];
}


- (void)actionQuit{
    [_wait stop];
    [g_server stopConnection:self];
    [g_window endEditing:YES];
    [g_notify removeObserver:self];
    
    [self dismissViewControllerAnimated:self completion:nil];
//    [g_navigation dismissViewController:self animated:YES];
}
#pragma mark -------------ScrollDelegate----------------
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    [self endEdit:nil];
//    int page = (int)(scrollView.contentOffset.x/ManMan_SCREEN_WIDTH);
//    switch (page) {
//        case 0:
//            [_topSiftView resetItemBtnWith:0];
//            [_topSiftView moveBottomSlideLine:0];
//            break;
//        case 1:
//            [_topSiftView resetItemBtnWith:ManMan_SCREEN_WIDTH];
//            [_topSiftView moveBottomSlideLine:ManMan_SCREEN_WIDTH];
//            break;
//        case 2:
//            [_topSiftView resetItemBtnWith:ManMan_SCREEN_WIDTH*2];
//            [_topSiftView moveBottomSlideLine:ManMan_SCREEN_WIDTH*2];
//            break;
//        default:
//            break;
//    }
}
-(void)sendRedPacket:(UIButton *)button{
    _button = button;
    [g_server getUserMoenyToView:self];
//    if (button.tag == 1) {
//        _moneyText = _nomalView.moneyTextField.text;
//        _countText = _nomalView.countTextField.text;
//        _greetText = _nomalView.greetTextField.text;
//    }else if(button.tag == 2){
//        _moneyText = _luckyView.moneyTextField.text;
//        _countText = _luckyView.countTextField.text;
//        _greetText = _luckyView.greetTextField.text;
//    }else if(button.tag == 3){
//        _moneyText = _orderView.moneyTextField.text;
//        _countText = _orderView.countTextField.text;
//        _greetText = _orderView.greetTextField.text;
//    }
//    if (_moneyText == nil || [_moneyText isEqualToString:@""] || [_moneyText floatValue]==0.0) {
//        [g_App showAlert:Localized(@"JX_InputGiftCount")];
//        return;
//    }
////    if ([_moneyText intValue] >200) {
////        [g_App showAlert:@"金额必须介于 0.01元 和 200元 之间"];
////        return;
////    }
//    if (!_isRoom) {
//        _countText = @"1";
//    }
//    if (_isRoom && (_countText == nil|| [_countText isEqualToString:@""] || [_countText intValue] <= 0)) {
//        [g_App showAlert:Localized(@"JXGiftForRoomVC_InputGiftCount")];
//        return;
//    }
//    if (([_moneyText doubleValue]/[_countText intValue]) < 0.01) {
//        [g_App showAlert:Localized(@"JXRedPaket_001")];
//        return;
//    }
//
//     int aaa =  [_countText intValue];
//
//    if (200*aaa >= [_moneyText floatValue]&&[_moneyText floatValue] > 0) {
//        if (button.tag == 3) {
//            if ([_greetText isEqualToString:@""]) {
//                [g_App showAlert:Localized(@"JXGiftForRoomVC_InputGiftWord")];
//                return;
//            }
//            _greetText = [_greetText stringByReplacingOccurrencesOfString:@" " withString:@""];
//            if ([_greetText isEqualToString:@""]) {
//                [JY_MyTools showTipView:@"请输入有效口令"];
//                return;
//            }
//        }
//        if (button.tag == 1) {
//            if ((int)([_moneyText floatValue] * 100) % [_countText integerValue] != 0) {
//                [JY_MyTools showTipView:@"普通红包需要均分金额"];
//                return;
//            }
//        }
//        if ([_greetText isEqualToString:@""]) {
//            _greetText = Localized(@"JX_GiftText");
//        }
//        self.indexInt = button.tag;
//        if ([g_server.myself.isPayPassword boolValue]) {
//            self.verVC = [JY_VerifyPayVC alloc];
//            self.verVC.type = ManMan_VerifyTypeSendReadPacket;
//            self.verVC.RMB = _moneyText;
//            self.verVC.delegate = self;
//            self.verVC.didDismissVC = @selector(dismissVerifyPayVC);
//            self.verVC.didVerifyPay = @selector(didVerifyPay:);
//            self.verVC = [self.verVC init];
//            [self.view addSubview:self.verVC.view];
//        } else {
//            JY_PayPasswordVC *payPswVC = [JY_PayPasswordVC alloc];
//            payPswVC.type = ManMan_PayTypeSetupPassword;
//            payPswVC.enterType = ManMan_EnterTypeSendRedPacket;
//            payPswVC = [payPswVC init];
//            [g_navigation pushViewController:payPswVC animated:YES];
//        }
//    }else{
//
//       // [g_App showAlert:@"红包总金额要小于人数*200/个"];
//       // [g_App showAlert:Localized(@"JX_InputMoneyCount")];
//    }
}
- (void)didVerifyPay:(NSString *)sender {
    NSString *assignUserId = _user != nil ?[NSString stringWithFormat:@"%ld", _user.userId]:@"";
    
    long time = (long)[[NSDate date] timeIntervalSince1970];
    time = (time *1000 + g_server.timeDifference)/1000;
    NSString *secret = [self getSecretWithText:sender time:time];
    NSMutableArray *arr = [NSMutableArray arrayWithObjects:@"type",[NSString stringWithFormat:@"%ld",self.indexInt],@"moneyStr",_moneyText,@"count",_countText,@"greetings",_greetText, nil];
    if (self.roomJid.length > 0) {
        [arr addObject:@"roomJid"];
        [arr addObject:self.roomJid];
    }else {
        [arr addObject:@"toUserId"];
        [arr addObject:self.toUserId];
    }
    
    if (assignUserId.length > 0) {
        [arr addObject:@"assignUserId"];
    }
    [g_payServer payServerWithActionV2:act_sendRedPacketV2 param:arr payPassword:sender assignUserId:assignUserId time:time toView:self];
}

- (void)dismissVerifyPayVC {
    [self.verVC.view removeFromSuperview];
}

-(void)tuningWxWith:(NSDictionary *)dict{
     
    PayReq *req = [[PayReq alloc] init];
    req.partnerId = [dict objectForKey:@"partnerId"];
    req.prepayId = [dict objectForKey:@"prepayId"];
    req.nonceStr = [dict objectForKey:@"nonceStr"];
    req.timeStamp = [[dict objectForKey:@"timeStamp"] intValue];
    req.package = @"Sign=WXPay";//[dict objectForKey:@"package"];
    req.sign = [dict objectForKey:@"sign"];
    //  [WXApi sendReq:req];
   [WXApi sendReq:req completion:^(BOOL success) {  }];
     
}


-(void)receiveWXPayFinishNotification:(NSNotification *)notifi{
  
    PayResp *resp = notifi.object;
    switch (resp.errCode) {
        case WXSuccess:{
            [g_App showAlert:Localized(@"JXMoney_PaySuccess") delegate:self tag:1001 onlyConfirm:YES];
            [self actionQuit];
//            if (_isQuitAfterSuccess) {
//                [self actionQuit];
//            }
            break;
        }
        case WXErrCodeUserCancel:{
            //取消了支付
            break;
        }
        default:{
            //支付错误
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"支付失败！retcode = %d, retstr = %@", resp.errCode,resp.errStr] message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
    } 
}
-(void) didServerResultSucces:(JY_Connection*)aDownload dict:(NSDictionary*)dict array:(NSArray*)array1{
    [_wait stop];
    if ([aDownload.action isEqualToString:act_getUserMoeny]) {
        g_App.myMoney = [dict[@"balance"] doubleValue];
        if (g_App.myMoney <= 0) {
            //。
            [g_App showAlert:Localized(@"JX_NotEnough") delegate:self tag:2000 onlyConfirm:NO];
        }else {
            UIButton *button = _button;
            //1是普通红包，2是手气红包，3是口令红包
            if (button.tag == 1 || button.tag == 2) {
                _moneyText = _nomalView.moneyTextField.text;
                _countText = _nomalView.countTextField.text;
                _greetText = _nomalView.greetTextField.text;
                _user = _nomalView.user;
                
            }
            if (_moneyText == nil || [_moneyText isEqualToString:@""]) {
                [g_App showAlert:Localized(@"JX_InputGiftCount")];
                return;
            }
            
            if (!_isRoom) {
                _countText = @"1";
            }
            
            if (_isRoom && (_countText == nil|| [_countText isEqualToString:@""] || [_countText intValue] <= 0)) {
                [g_App showAlert:Localized(@"JXGiftForRoomVC_InputGiftCount")];
                return;
            }
            
            if (([_moneyText doubleValue]/[_countText intValue]) < 0.01) {
                [g_App showAlert:Localized(@"JXRedPaket_001")];
                return;
            }
            if ([_moneyText doubleValue] > g_App.myMoney) {
                //。
                [g_App showAlert:Localized(@"JX_NotEnough") delegate:self tag:2000 onlyConfirm:NO];
                return;
            }
            
            int aaa =  [_countText intValue];
 
            if (200*aaa >= [_moneyText floatValue]&&[_moneyText floatValue] > 0) {
                
                if (button.tag == 3 && [_greetText isEqualToString:@""]) {
                    [g_App showAlert:Localized(@"JXGiftForRoomVC_InputGiftWord")];
                    return;
                }
                //祝福语
                if ([_greetText isEqualToString:@""]) {
                    _greetText = Localized(@"JX_GiftText");
                }

                self.indexInt = _user != nil ? 4: button.tag;
                if ([g_server.myself.isPayPassword boolValue]) {
                    self.verVC = [JY_VerifyPayVC alloc];
                    self.verVC.type = ManMan_VerifyTypeSendReadPacket;
                    self.verVC.RMB = _moneyText;
                    self.verVC.delegate = self;
                    self.verVC.didDismissVC = @selector(dismissVerifyPayVC);
                    self.verVC.didVerifyPay = @selector(didVerifyPay:);
                    self.verVC = [self.verVC init];
                    
                    [self.view addSubview:self.verVC.view];
                } else {
                    JY_PayPasswordVC *payPswVC = [JY_PayPasswordVC alloc];
                    payPswVC.type = ManMan_PayTypeSetupPassword;
                    payPswVC.enterType = ManMan_EnterTypeSendRedPacket;
                    payPswVC = [payPswVC init];
                    [g_navigation pushViewController:payPswVC animated:YES];
                }
            }else{
                [g_App showAlert:[NSString stringWithFormat:@"请输入0.01~%d元", 200*aaa]];
            }
        }
    }
    if ([aDownload.action isEqualToString:act_sendRedPacket] || [aDownload.action isEqualToString:act_sendRedPacketV2]) {
        NSMutableDictionary * muDict = [NSMutableDictionary dictionaryWithDictionary:dict];
        [muDict setObject:[dict objectForKey:@"greetings"] forKey:@"greet"];
        NSString *type = [muDict objectForKey:@"type"];
        if (type.intValue == 4) {
            NSString *assignUserId = [muDict objectForKey:@"assignUserIds"];
            if (assignUserId!=nil) {
            
                [muDict setObject:assignUserId forKey:@"toId"];
            }

            [muDict setObject:_user.userNickName forKey:@"toUserName"];
        }
        [self dismissVerifyPayVC];  // 销毁支付密码界面
      
        
        [_wait stop];
        [g_server stopConnection:self];
        [g_window endEditing:YES];
        [g_notify removeObserver:self];
        [self dismissViewControllerAnimated:YES completion:^{
            int64_t delayInSeconds = 0.5;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                //成功创建红包，发送一条含红包Id的消息
                if (self.delegate && [self.delegate respondsToSelector:@selector(sendRedPacketDelegate:)]) {
                    
                    [self.delegate performSelector:@selector(sendRedPacketDelegate:) withObject:muDict];
                }
            });
        }];
        
//        [self actionQuit];
    }
}


-(int) didServerResultFailed:(JY_Connection*)aDownload dict:(NSDictionary*)dict{
    [_wait stop];
    if ([aDownload.action isEqualToString:act_sendRedPacket] || [aDownload.action isEqualToString:act_sendRedPacketV2] || [aDownload.action isEqualToString:act_TransactionGetCode]) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.verVC clearUpPassword];
        });
    }
    return show_error;
}
-(int) didServerConnectError:(JY_Connection*)aDownload error:(NSError *)error{
    [_wait stop];
    return show_error;
}
-(void) didServerConnectStart:(JY_Connection*)aDownload{
    [_wait start];
}
#pragma mark - alertViewDelegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 2000){
        if (buttonIndex == 1) {
            [self rechargeButtonAction];
        }
    }
}
-(void)rechargeButtonAction{
    //。
    JXp1a1y1WithBankMoney_VC * rechargeVC = [[JXp1a1y1WithBankMoney_VC alloc]init];
//    rechargeVC.rechargeDelegate = self;
//    rechargeVC.isQuitAfterSuccess = YES;
    [g_navigation pushViewController:rechargeVC animated:YES];
}
#pragma mark - RechargeDelegate
-(void)rechargeSuccessed{
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    JY_RedInputView * inputView = (JY_RedInputView *)textField.superview.superview;
    if (textField.returnKeyType == UIReturnKeyDone) {
        [inputView stopEdit];
    }
    return YES;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([string isEqualToString:@""]) {
        return YES;
    }
    JY_RedInputView * inputView = (JY_RedInputView *)textField.superview.superview;
    if (textField == inputView.countTextField && [textField.text intValue] > 1000) {
        return NO;
    }
    if (textField == inputView.greetTextField && textField.text.length >= 20) {
        return NO;
    }
    if (textField == inputView.moneyTextField) {
        NSRange range = [textField.text rangeOfString:@"."];
        if (range.location != NSNotFound) {
            NSString *firstStr = [textField.text substringFromIndex:range.location + 1];
            if (firstStr.length > 1) {
                return NO;
            }
        }
    }
    if (textField == inputView.countTextField) {
        if ([textField.text rangeOfString:@"."].location != NSNotFound) {
            return NO;
        }
        NSMutableString *str = [NSMutableString string];
        [str appendString:textField.text];
        [str appendString:string];
        if ([str integerValue] > self.size) {
            textField.text = [NSString stringWithFormat:@"%ld", self.size];
            [JY_MyTools showTipView:@"红包个数不能超过群人数"];
            return NO;
        }
    }
    return YES;
}
- (NSString *)getSecretWithText:(NSString *)text time:(long)time {
    NSMutableString *str1 = [NSMutableString string];
    [str1 appendString:APIKEY];
    [str1 appendString:[NSString stringWithFormat:@"%ld",time]];
    [str1 appendString:[NSString stringWithFormat:@"%@",[NSNumber numberWithDouble:[_moneyText doubleValue]]]];
    str1 = [[g_server getMD5String:str1] mutableCopy];
    [str1 appendString:g_myself.userId];
    [str1 appendString:g_server.access_token];
    NSMutableString *str2 = [NSMutableString string];
    str2 = [[g_server getMD5String:text] mutableCopy];
    [str1 appendString:str2];
    str1 = [[g_server getMD5String:str1] mutableCopy];
    return [str1 copy];
}
@end
