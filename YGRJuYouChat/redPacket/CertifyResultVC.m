//
//  CertifyResultVC.m
//  shiku_im
//
//  Created by 冉彬 on 2020/11/6.
//  Copyright © 2020 Reese. All rights reserved.
//

#import "CertifyResultVC.h"

@interface CertifyResultVC ()

@end

@implementation CertifyResultVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (id)init
{
    self = [super init];
    if (self) {
        
        self.isGotoBack = YES;
        self.title = @"实名认证";
        self.heightHeader = ManMan_SCREEN_TOP;
        self.heightFooter = 0;
        [self createHeadAndFoot];
        self.tableBody.backgroundColor = [UIColor whiteColor];
//        self.tableBody.scrollEnabled = YES;
        [self loadUI];
    }
    return self;
}


-(void)loadUI
{
    
}

@end
