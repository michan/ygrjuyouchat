#import <UIKit/UIKit.h>
@class JY_RedInputView;
@protocol ManMan_RedInputViewDelegate <NSObject>
/// 发送红包代理
/// @param inputView view
/// @param type  1是普通红包，2是手气红包，3是口令红包 ,4是专属红包
/// @param money 金额
/// @param number 红包个数
/// @param bless 祝福语
/// @param command 口令
/// @param exclusiveId 专属红包人id
- (void)inputView:(JY_RedInputView *)inputView redPacketType:(int)type money:(NSString *)money number:(NSString *)number bless:(NSString *)bless command:(NSString *)command exclusiveId:(NSString *)exclusiveId user:(memberData *)user;
@end
@interface JY_RedInputView : UIView

@property (nonatomic, assign) NSUInteger type;
@property (nonatomic, assign) BOOL isRoom;
@property (nonatomic, weak) id<ManMan_RedInputViewDelegate> delegate;
@property (nonatomic,strong) roomData * room;
//  指定人领取红包
@property (nonatomic,strong)  memberData *user;
// 普通1,拼手气2
@property (nonatomic, assign) NSUInteger markType;

@property (nonatomic, strong) UIView * countView;
@property (nonatomic, strong) UIView * moneyView;
@property (nonatomic, strong) UIView * greetView;
@property (nonatomic, strong) UIView * commandView;

@property (nonatomic, strong) UIButton * sendButton;
@property (nonatomic, strong) UILabel * noticeTitle;
@property (nonatomic, strong) UILabel * noticeTipsTitle;


@property (nonatomic, strong) UITextField * countTextField;
@property (nonatomic, strong) UITextField * moneyTextField;
@property (nonatomic, strong) UITextField * greetTextField;
@property (nonatomic, strong) UITextField * commandTextField;

@property (nonatomic, strong) UILabel * countTitle;
@property (nonatomic, strong) UILabel * moneyTitle;
@property (nonatomic, strong) UILabel * greetTitle;
@property (nonatomic, strong) UILabel * commandTitle;


@property (nonatomic, strong) UILabel * countUnit;
@property (nonatomic, strong) UILabel * moneyUnit;


@property (nonatomic, strong) UILabel * allMoneyLab;

@property (nonatomic, strong)  UIImageView *bgImageView;
@property (nonatomic, strong)  UIImageView *selectImageView;
@property (nonatomic, strong)  UIImageView *moneyTitleLeftImageView;
@property (nonatomic, strong) UIView * line;
@property (nonatomic, strong) UIView * line1;
@property (nonatomic, strong) UIView * line2;
@property (nonatomic, strong) UIView * line3;

-(instancetype)initWithFrame:(CGRect)frame type:(NSUInteger)type isRoom:(BOOL)isRoom delegate:(id<ManMan_RedInputViewDelegate>)delegate;

-(void)stopEdit;
@end
