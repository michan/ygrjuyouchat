//
//  QLGroupManagerCell.h
//  TFJunYouChat
//
//  Created by Qian on 2022/1/11.
//  Copyright © 2022 zengwOS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QLGroupManagerCell : UICollectionViewCell

@property(nonatomic, strong) memberData *user;
@property(nonatomic, copy)NSString *img;

@end

NS_ASSUME_NONNULL_END
