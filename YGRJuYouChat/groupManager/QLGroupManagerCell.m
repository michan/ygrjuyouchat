//
//  QLGroupManagerCell.m
//  TFJunYouChat
//
//  Created by Qian on 2022/1/11.
//  Copyright © 2022 zengwOS. All rights reserved.
//

#import "QLGroupManagerCell.h"

@interface QLGroupManagerCell ()

@property (weak, nonatomic) IBOutlet UIImageView *imgV;
@end

@implementation QLGroupManagerCell

- (void)awakeFromNib {
    [super awakeFromNib];
    ViewRadius(self, 5);
    // Initialization code
}
- (void)setUser:(memberData *)user{
    _user = user;
    [g_server getHeadImageSmall:[NSString stringWithFormat:@"%ld",user.userId] userName:user.userNickName imageView:self.imgV];
}
- (void)setImg:(NSString *)img{
    _img = img;
    if ([img containsString:@"add"] || [img containsString:@""]) {
        _imgV.image = [UIImage imageNamed:img];
    }else{
        [_imgV sd_setImageWithURL:[NSURL URLWithString:img] placeholderImage:nil];
    }
}

@end
