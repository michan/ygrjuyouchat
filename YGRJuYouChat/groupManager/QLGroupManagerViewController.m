//
//  QLGroupManagerViewController.m
//  TFJunYouChat
//
//  Created by Qian on 2022/1/11.
//  Copyright © 2022 zengwOS. All rights reserved.
//

#import "QLGroupManagerViewController.h"
#import "QLGroupManagerCell.h"
#import "JY_SelFriendVC.h"
#import "JY_QLGroupManagerTypeModel.h"
#import "JY_GroupManagementVC.h"
#import "JY_SelectFriendsVC.h"
@interface QLGroupManagerViewController ()<UICollectionViewDataSource,UICollectionViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *nickName;

@property (weak, nonatomic) IBOutlet UIImageView *headerV;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (copy, nonatomic)  NSMutableArray *dataArray;

@property (strong, nonatomic) memberData *owner;

@property (nonatomic,strong) memberData  * currentMember;

@end

@implementation QLGroupManagerViewController
- (IBAction)backClick:(id)sender {
    [g_navigation popToViewController:[JY_GroupManagementVC class] animated:YES];
}
- (NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc] init];
    }
    return _dataArray;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.itemSize = CGSizeMake(50,50);//每一个cell的大小
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;//滚动方向
    layout.sectionInset = UIEdgeInsetsMake(20, 20, 20, 20);//四周的边距
    self.collectionView.collectionViewLayout = layout;
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    [self.collectionView registerNib:[UINib nibWithNibName:@"QLGroupManagerCell" bundle:nil] forCellWithReuseIdentifier:@"QLGroupManagerCell"];
    self.collectionView.showsHorizontalScrollIndicator = NO;
    self.collectionView.showsVerticalScrollIndicator = NO;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadData:) name:@"CHANGEADMINSUCCESS" object:nil];
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"CHANGEADMINSUCCESS" object:nil];
}
-(void)reloadData:(NSNotification *)nofi{
    NSArray *members   = [nofi.userInfo valueForKey:@"obj"];
    [self setData:members];
}
-(void)setData:(NSArray *)members{
    [self.dataArray removeAllObjects];
    NSInteger count = 0;
    for (memberData *data in members) {
        if ([data.role integerValue] == 2) {
            [self.dataArray addObject:data];
           
        }else if([data.role integerValue] == 1){
            self.owner = data;
        }
    }
    count = self.dataArray.count;
    JY_QLGroupManagerTypeModel *add = JY_QLGroupManagerTypeModel.new;
    add.type = 1;
    add.img = @"add_1";
    [self.dataArray addObject:add];
    if (self.dataArray.count>1) {
        JY_QLGroupManagerTypeModel *reduce = JY_QLGroupManagerTypeModel.new;
        reduce.type = 0;
        reduce.img = @"add_2";
        [self.dataArray addObject:reduce];
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        self.nickName.text = self.owner.userNickName;
        [g_server getHeadImageSmall:[NSString stringWithFormat:@"%ld",self.owner.userId] userName:self.owner.userNickName imageView:self.headerV];
        ViewRadius(self.headerV, 3);
        self.countLabel.text = [NSString stringWithFormat:@"管理员(%ld/%ld)",count,count];
    });
    
    [self.collectionView reloadData];
}
- (void)setRoom:(roomData *)room{
    _room = room;
    [self setData:room.members];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataArray.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    QLGroupManagerCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"QLGroupManagerCell" forIndexPath:indexPath];
    id model = self.dataArray[indexPath.row];
    if ([model isKindOfClass:[JY_QLGroupManagerTypeModel class]]) {
        JY_QLGroupManagerTypeModel *typeModel =model;
        cell.img = typeModel.img;
    }else{
        memberData *user = model;
        cell.user = user;
    }
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    id model = self.dataArray[indexPath.row];
    if ([model isKindOfClass:[JY_QLGroupManagerTypeModel class]]) {
        JY_QLGroupManagerTypeModel *typeModel =model;
        [self addAdmin];
    }
}


-(void)addAdmin{
//        JY_SelectFriendsVC* vc = [JY_SelectFriendsVC alloc];
//        vc.isNewRoom = NO;
//    //    vc.chatRoom = self.chatRoom;
//        vc.room = self.room;
//        vc.delegate = self;
//        vc.didSelect = @selector(onAfterAddMember:);
//        vc.title = @"添加管理员";
//        vc = [vc init];
//        [g_navigation pushViewController:vc animated:YES];
    [self setManagerWithType:ManMan_SelUserTypeSpecifyAdmin];
}
- (void)setManagerWithType:(ManMan_SelUserType)type {
    memberData *data = [self.room getMember:g_myself.userId];
    if ([data.role intValue] != 1) {
        [g_App showAlert:Localized(@"JXRoomMemberVC_NotGroupMarsterCannotDoThis")];
        return;
    }
    JY_SelFriendVC * selVC = [[JY_SelFriendVC alloc] init];
    selVC.title = @"添加管理员";
    selVC.type = type;
    selVC.room = self.room;
    selVC.delegate = self;
    if (type == ManMan_SelUserTypeSpecifyAdmin) {
        selVC.didSelect = @selector(specifyAdministratorDelegate:);
    }
    [g_navigation pushViewController:selVC animated:YES];
}

-(void)specifyAdministratorDelegate:(memberData *)member{
    _currentMember = member;
    int type;
    if ([member.role intValue] == 2) {
        type = 3;
    }else {
        type = 2;
    }
    [g_server setRoomAdmin:member.roomId userId:[NSString stringWithFormat:@"%ld",member.userId] type:type toView:self];
}
-(void) didServerResultSucces:(JY_Connection*)aDownload dict:(NSDictionary*)dict array:(NSArray*)array1{
    if ([aDownload.action isEqualToString:act_roomSetAdmin]) {
        //设置群组管理员
        NSString *str;
        if ([_currentMember.role intValue] == 2) {
            _currentMember.role = [NSNumber numberWithInt:3];
            str = Localized(@"JXRoomMemberVC_CancelAdministratorSuccess");
        }else {
            _currentMember.role = [NSNumber numberWithInt:2];
            str = Localized(@"JXRoomMemberVC_SetAdministratorSuccess");
        }
        [g_server showMsg:str];
    }
}
@end
