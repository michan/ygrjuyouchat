//
//  JY_QLGroupManagerTypeModel.h
//  TFJunYouChat
//
//  Created by Qian on 2022/1/11.
//  Copyright © 2022 zengwOS. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface JY_QLGroupManagerTypeModel : NSObject

@property(nonatomic, assign) NSInteger type;

@property(nonatomic, copy) NSString * img;
@end

NS_ASSUME_NONNULL_END
