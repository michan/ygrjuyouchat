//
//  JY_PublishNoVc.m
//  TFJunYouChat
//
//  Created by os on 2021/1/21.
//  Copyright © 2021 zengwOS. All rights reserved.
//
#define HEIGHT 64
#define MY_INSET  10
#define TOP_ADD_HEIGHT  400

#import "JY_PublishNoVc.h"
#import "JY_ChatViewController.h"
#import "JY_TransferNoticeVC.h"
#import "JY_SearchUserVC.h"
#import "JY_SearchUserVC.h"

@interface JY_PublishNoVc ()

@end

@implementation JY_PublishNoVc

- (id)init{
    if (self = [super init]) {
        
        self.title = @"公众号";
        self.heightFooter = 0;
        self.heightHeader = ManMan_SCREEN_TOP;
        [self createHeadAndFoot];
        self.tableBody.backgroundColor = THEMECOLOR;
        
        int h=-20;
        int w=ManMan_SCREEN_WIDTH;
        JY_ImageView* iv;
        
        iv = [self createButton:@"客服系统" drawTop:NO drawBottom:NO icon:THESIMPLESTYLE ? @"im_10000" : @"im_10000" click:@selector(onCustomer)];
        iv.frame = CGRectMake(MY_INSET,h, w-MY_INSET*2, HEIGHT);
        h+=iv.frame.size.height;
        
        iv = [self createButton:Localized(@"JX_PaymentNo.") drawTop:NO drawBottom:NO icon:THESIMPLESTYLE ? @"icon_dongtai" : @"icon_dongtai" click:@selector(onMyBlog)];
        iv.frame = CGRectMake(MY_INSET,h, w-MY_INSET*2, HEIGHT);
        h+=iv.frame.size.height;
        
        
        iv = [self createButton:@"系统客服" drawTop:NO drawBottom:NO icon:THESIMPLESTYLE ? @"icon_dongtai" : @"icon_dongtai" click:@selector(CustomerSystem)];
        iv.frame = CGRectMake(MY_INSET,h, w-MY_INSET*2, HEIGHT);
        h+=iv.frame.size.height;
        //JX_PublicNumber
        
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
   
    
}
- (void)onCustomer{
    JY_ChatViewController *vc = [[JY_ChatViewController alloc]init];
    
    [g_navigation pushViewController:vc animated:YES];
}

- (void)CustomerSystem{
   // JY_PayViewController *payVC = [[JY_PayViewController alloc] init];
     
    JY_TransferNoticeVC *vc = [[JY_TransferNoticeVC alloc]init];
    
    [g_navigation pushViewController:vc animated:YES];
     
}
- (void)onMyBlog{
    
    JY_SearchUserVC *searchUserVC = [[JY_SearchUserVC alloc]init];
     searchUserVC.type = ManMan_SearchTypePublicNumber;
     searchUserVC = [searchUserVC init];
    [g_navigation pushViewController:searchUserVC animated:YES];
   
    
        
}

-(JY_ImageView*)createButton:(NSString*)title drawTop:(BOOL)drawTop drawBottom:(BOOL)drawBottom icon:(NSString*)icon click:(SEL)click{
    JY_ImageView* btn = [[JY_ImageView alloc] init];
    btn.backgroundColor = [UIColor whiteColor];
    btn.userInteractionEnabled = YES;
    btn.didTouch = click;
    btn.delegate = self;
    btn.layer.cornerRadius =5;
    btn.layer.masksToBounds =YES;
    [self.tableBody addSubview:btn];
     
  
    JY_Label* p = [[JY_Label alloc] initWithFrame:CGRectMake(60, 0, self_width-35-20-35, HEIGHT)];
    p.text = title;
    p.font = g_factory.font16;
    p.backgroundColor = [UIColor clearColor];
    p.textColor = HEXCOLOR(0x323232);
    [btn addSubview:p];
    if(icon){
        UIImageView* iv = [[UIImageView alloc] initWithFrame:CGRectMake(0, 3, 58, 58)];
        iv.image = [UIImage imageNamed:icon];
        [btn addSubview:iv];
    }
    if(drawTop){
        UIView* line = [[UIView alloc] initWithFrame:CGRectMake(53,0,ManMan_SCREEN_WIDTH-53,LINE_WH)];
        //line.backgroundColor = THE_LINE_COLOR;
        [btn addSubview:line];
    }
    if(drawBottom){
        UIView* line = [[UIView alloc] initWithFrame:CGRectMake(53,HEIGHT-0.3,ManMan_SCREEN_WIDTH-53,LINE_WH)];
        //line.backgroundColor = THE_LINE_COLOR;
        [btn addSubview:line];
    }
    if(click){
        UIImageView* iv;
        iv = [[UIImageView alloc] initWithFrame:CGRectMake(ManMan_SCREEN_WIDTH-15-27, (HEIGHT-13)/2, 7, 13)];
        iv.image = [UIImage imageNamed:@"new_icon_>"];
        [btn addSubview:iv];
    }
    return btn;
}
@end
