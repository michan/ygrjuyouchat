//
//  JY_MainViewController.h
//  TFJunYouChat
//
//  Created by lifengye on 2020/9/15.
//  Copyright © 2020 zengwOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JY_PublishVc.h"
@class JY_TabMenuView;
@class JY_MsgViewController;
@class ManMan_UserViewController;
@class JY_FriendViewController;
@class JY_GroupViewController;
@class JY_PSMyViewController;
@class searchUserVC;
@class JY_WeiboVC;
@class OrganizTreeViewController;
@class JY_SquareViewController;
@class PSJobListVC;
@class PSAuditListVC;
@class PSWriteExamListVC;
@class CYWebAddPointVC;
@class CYWebBettingVC;
@class JY_PublishVc;
 
@class JY_AddrBookVc;
@class JY_FriendCirleVc;
@class JY_PSMyViewController;
@class JY_ShopWebVc;

@interface JY_MainViewController : UIViewController<UIAlertViewDelegate>{
    
    UIView* _topView;
    UIViewController* _selectVC;
    JY_GroupViewController* _groupVC;
    NSMutableArray * _friendArray;
}

@property (strong, nonatomic) JY_MsgViewController* msgVc;
    

@property (strong, nonatomic) JY_FriendViewController* friendVC;

@property (strong, nonatomic) JY_FriendCirleVc *cirleFriendVc;
@property (strong, nonatomic) JY_PSMyViewController *psMyviewVC;
@property (strong, nonatomic) JY_ShopWebVc *shopWebVc;

@property (strong, nonatomic) JY_TabMenuView* tb;
@property (nonatomic, strong) UIImageView* bottomView;
@property (strong, nonatomic) UIButton* btn;
@property (strong, nonatomic) UIView* mainView;
@property (assign) BOOL IS_HR_MODE;
@property (nonatomic, assign) BOOL isLoadFriendAndGroup;
@property (nonatomic,assign) int vcnum;
-(void)onAfterLogin;
-(void)doSelected:(int)n;
@end
