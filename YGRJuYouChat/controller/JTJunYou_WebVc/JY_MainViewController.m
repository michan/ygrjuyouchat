//
//  JY_MainViewController.h
//  TFJunYouChat
//
//  Created by lifengye on 2020/9/15.
//  Copyright © 2020 zengwOS. All rights reserved.
//


#import "JY_MainViewController.h"
#import "JY_TabMenuView.h"
//。
#import "JY_MsgViewController.h"
#import "JY_FriendViewController.h"
#import "JY_FriendCirleVc.h"
#import "JY_PSMyViewController.h"

#import "JY_FriendObject.h"
 
#import "JY_WeiboVC.h"
#import "JY_ProgressVC.h"
#import "JY_GroupViewController.h"
#import "JY_LabelObject.h"
#import "JY_BlogRemind.h"
#import "JY_RoomPool.h"
#import "JY_DeviceAuthController.h"
#import "JY_ShopWebVc.h"

#import <RongIMKit/RongIMKit.h>
#import <RongIMLib/RongIMLib.h>
#import <RongCallKit/RCCall.h>


@interface JY_MainViewController()<RCIMUserInfoDataSource,RCCallSessionDelegate>
@property(nonatomic, strong) RCCallSession *currentCallSession;
@property(nonatomic, assign) NSTimeInterval startTime; // 通话开始时间
@property(nonatomic, assign) NSTimeInterval endTime;// 通话结束时间
@end
@implementation JY_MainViewController{
    NSString * _linkName1;
    NSString * _linkName2;
    NSString * _linkURL1;
    NSString * _linkURL2;
    NSString * _imgUrl1;
    NSString * _imgUrl2;
}

@synthesize btn=_btn,mainView=_mainView;
@synthesize IS_HR_MODE;
@synthesize psMyviewVC=_psMyviewVC;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.isLoadFriendAndGroup = [g_server.myself fetchAllFriends].count < 6;
        self.view.backgroundColor = [UIColor clearColor];
        if(AppStore == 1){
        } else {
            self.vcnum = (int)g_App.linkArray.count;
        }
        if (self.vcnum < 0 || self.vcnum >2) {
            self.vcnum = 0;
        }else{
            for (int i = 0; i < self.vcnum; i++) {
                if (i == 0) {
                    _linkName1 = g_App.linkArray[i][@"desc"];
                    _linkURL1 = g_App.linkArray[i][@"link"];
                    _imgUrl1 = g_App.linkArray[i][@"imgUrl"];
                    if (_linkName1.length < 1) {
                        _linkName1 = @"百度";
                    }
                    if (_linkURL1.length < 1) {
                        _linkURL1 = @"http://www.baidu.com";
                    }
                }else if (i == 1){
                    _linkName2 = g_App.linkArray[i][@"desc"];
                    _linkURL2 = g_App.linkArray[i][@"link"];
                    _imgUrl2 = g_App.linkArray[i][@"imgUrl"];
                    if (_linkName2.length < 1) {
                        _linkName2 = @"百度";
                    }
                    if (_linkURL2.length < 1) {
                        _linkURL2 = @"http://www.baidu.com";
                    }
                }
            }
        }
        _topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ManMan_SCREEN_WIDTH, ManMan_SCREEN_TOP)];
        _mainView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ManMan_SCREEN_WIDTH, ManMan_SCREEN_HEIGHT)];
        [self.view addSubview:_mainView];
        _bottomView = [[UIImageView alloc] initWithFrame:CGRectMake(0, ManMan_SCREEN_HEIGHT-ManMan_SCREEN_BOTTOM, ManMan_SCREEN_WIDTH, ManMan_SCREEN_BOTTOM)];
        _bottomView.userInteractionEnabled = YES;
        _bottomView.backgroundColor = HEXCOLOR(0xF1F1F1);
        [self.view addSubview:_bottomView];
        [self buildTop];
 
        if (g_server.isManualLogin && self.isLoadFriendAndGroup) {
            _groupVC = [JY_GroupViewController alloc];
            [_groupVC scrollToPageUp];
        }
        _msgVc = [[JY_MsgViewController alloc] init];
        _friendVC = [[JY_FriendViewController alloc] init];
      
        _cirleFriendVc = [[JY_FriendCirleVc alloc] init];
        
        _psMyviewVC = [[JY_PSMyViewController alloc] init];
        
         
        [self doSelected:0];
        [g_notify addObserver:self selector:@selector(loginSynchronizeFriends:) name:kXmppClickLoginNotifaction object:nil];
        [g_notify addObserver:self selector:@selector(appDidEnterForeground) name:kApplicationWillEnterForeground object:nil];
        [g_notify addObserver:self selector:@selector(getUserInfo:) name:kXMPPMessageUpadteUserInfoNotification object:nil];
        [g_notify addObserver:self selector:@selector(getRoomSet:) name:kXMPPMessageUpadteGroupNotification object:nil];
        [g_notify addObserver:self selector:@selector(onXmppLoginChanged:) name:kXmppLoginNotifaction object:nil];
        [g_notify addObserver:self selector:@selector(hasLoginOther:) name:kXMPPLoginOtherNotification object:nil];
        [g_notify addObserver:self selector:@selector(showDeviceAuth:) name:kDeviceAuthNotification object:nil];
        if (RongIMKit_Version) {
            _startTime = 0;
            _endTime = 0;
            [g_server rongyunGetTokenPortrait:[g_server getHeadImageOUrl: g_myself.userId] toView:self];
            [RCIM sharedRCIM].userInfoDataSource = self;
            [[RCCall sharedRCCall].currentCallSession addDelegate:self];
            // 重点 监听newSession
            [g_notify addObserver:self selector:@selector(newSession:) name:RCCallNewSessionCreationNotification object:nil];
        }
        
    }
    return self;
}

- (void)newSession:(NSNotification *)note {
    NSLog(@"-------------来了----------------");
    _currentCallSession = (RCCallSession *)note.object;
    // 重点 设置代理 监听通话状态
    [_currentCallSession addDelegate:self];
}

/*!
 获取用户信息
 
 @param userId      用户ID
 @param completion  获取用户信息完成之后需要执行的Block [userInfo:该用户ID对应的用户信息]
 
 @discussion SDK通过此方法获取用户信息并显示，请在completion中返回该用户ID对应的用户信息。
 在您设置了用户信息提供者之后，SDK在需要显示用户信息的时候，会调用此方法，向您请求用户信息用于显示。
 */
- (void)getUserInfoWithUserId:(NSString *)userId completion:(void (^)(RCUserInfo *))completion{
    dispatch_async(dispatch_get_main_queue() , ^{
        JY_UserObject *userObj = [g_myself getUserById:userId];
        // simple
        RCUserInfo *user = [[RCUserInfo alloc]init];
        user.userId = userId;// id
        user.name = userObj.remarkName.length > 0  ? userObj.remarkName : userObj.userNickname;
        
        user.portraitUri = [g_server getHeadImageOUrl: userId];// 头像的url
        return completion(user);
    });
}


/*!
 通话已接通

 @discussion
 通话已接通

 @remarks 代理
*/
- (void)callDidConnect {
    NSLog(@"------通话已接通------");
    _startTime = [[NSDate date] timeIntervalSince1970];
}

/*!
 通话已结束
 
 @discussion
 通话已结束
 
 @remarks 代理
*/
- (void)callDidDisconnect {
    NSLog(@"------通话已结束------");
    if (_startTime == 0) {
        
    }
    _endTime = [[NSDate date] timeIntervalSince1970];
    NSTimeInterval callTime = _endTime - _startTime;
    if (_endTime <= _startTime || _startTime == 0) {
        callTime = 0;
    }
    // 写入数据库 发消息给对方
    JY_UserObject *obj = [[JY_UserObject sharedInstance] getUserById: _currentCallSession.targetId];
    int type;

    switch (_currentCallSession.disconnectReason) {
        case RCCallDisconnectReasonCancel: // 己方取消已发出的通话请求
        case RCCallDisconnectReasonReject: //  己方拒绝收到的通话请求
        {
            if (_currentCallSession.mediaType == RCCallMediaAudio) {
                type = kWCMessageTypeAudioChatCancel;
            }else {
                type = kWCMessageTypeVideoChatCancel;
            }
            
            [g_meeting sendCancel:type toUserId:_currentCallSession.targetId toUserName:obj.userNickname];
        }
            break;
        case RCCallDisconnectReasonHangup: //  己方挂断
        {
            
        
            if (callTime > 0) {
                if (_currentCallSession.mediaType == RCCallMediaAudio) {
                    type = kWCMessageTypeAudioChatEnd;
                }else {
                    type = kWCMessageTypeVideoChatEnd;
                }
                
                [g_meeting sendEnd:type toUserId:obj.userId toUserName:obj.userNickname timeLen:(int)callTime];
            } else {
                if (_currentCallSession.mediaType == RCCallMediaAudio) {
                    type = kWCMessageTypeAudioChatCancel;
                }else {
                    type = kWCMessageTypeVideoChatCancel;
                }
                
                [g_meeting sendCancel:type toUserId:_currentCallSession.targetId toUserName:obj.userNickname];
            }
            
            
        }
            break;
        case RCCallDisconnectReasonBusyLine: //  己方忙碌
        case RCCallDisconnectReasonNoResponse: //  己方未接听
        {
            if (_currentCallSession.mediaType == RCCallMediaAudio) {
                type = kWCMessageTypeAudioChatCancel;
            }else {
                type = kWCMessageTypeVideoChatCancel;
            }
            
            [g_meeting sendNoAnswer:type toUserId:_currentCallSession.targetId toUserName:obj.userNickname];
        }
            break;
            
            
//            case RCCallDisconnectReasonRemoteCancel: //  对方取消已发出的通话请求
//            {
//
//            }
//                break;
//            case RCCallDisconnectReasonRemoteReject: //   对方拒绝收到的通话请求
//            {
//
//            }
//                break;
//            case RCCallDisconnectReasonRemoteHangup: //   通话过程对方挂断
//            {
//
//            }
//                break;
//            case RCCallDisconnectReasonRemoteBusyLine: //    对方忙碌
//            {
//
//            }
//                break;
//            case RCCallDisconnectReasonRemoteNoResponse: //    对方未接听
//            {
//            [g_meeting sendNoAnswer:<#(int)#> toUserId:<#(NSString *)#> toUserName:<#(NSString *)#>];
//            }
//                break;
            
        default:
            break;
    }
    
    
    
    
    _startTime = 0;
    _endTime = 0;

    
}



//传入 秒  得到  xx分钟xx秒
-(NSString *)getMMSSFromSS:(NSString *)totalTime{

    NSInteger seconds = [totalTime integerValue];

    //format of hour
    NSString *str_hour = [NSString stringWithFormat:@"%02ld",seconds/3600];
    //format of minute
    NSString *str_minute = [NSString stringWithFormat:@"%02ld",seconds/60];
    //format of second
    NSString *str_second = [NSString stringWithFormat:@"%02ld",seconds%60];
    //format of time
    NSString *format_time = @"";
    if (seconds > 3600) {
        format_time = [NSString stringWithFormat:@"%@:%@:%@",str_hour,str_minute,str_second];
    } else {
        format_time = [NSString stringWithFormat:@"%@:%@",str_minute,str_second];
    }
    NSLog(@"format_time : %@",format_time);
    return format_time;

}


- (void)appEnterForegroundNotif:(NSNotification *)noti {
    [g_server offlineOperation:(g_server.lastOfflineTime *1000 + g_server.timeDifference)/1000 toView:self];
}
- (void)getUserInfo:(NSNotification *)noti {
    JY_MessageObject *msg = noti.object;
    [g_server getUser:msg.toUserId toView:self];
}
- (void)getRoomSet:(NSNotification *)noti {
    JY_MessageObject *msg = noti.object;
    [g_server getRoom:msg.toUserId toView:self];
}
-(void)dealloc{
    [g_notify removeObserver:self name:kXmppLoginNotifaction object:nil];
    [g_notify removeObserver:self name:kSystemLoginNotifaction object:nil];
    [g_notify removeObserver:self name:kXmppClickLoginNotifaction object:nil];
    [g_notify removeObserver:self name:kXMPPLoginOtherNotification object:nil];
    [g_notify removeObserver:self name:kApplicationWillEnterForeground object:nil];
    [g_notify removeObserver:self name:kXMPPMessageUpadteUserInfoNotification object:nil];
    [g_notify removeObserver:self name:kDeviceAuthNotification object:nil];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loginSynchronizeFriends:nil];
    if (g_server.isManualLogin) {
        if (self.isLoadFriendAndGroup) {
            NSArray *array = [[JY_LabelObject sharedInstance] fetchAllLabelsFromLocal];
            if (array.count <= 0) {
                [g_server friendGroupListToView:self];
            }
        }
    }
    [g_server getCurrentTimeToView:self];
    [g_server getUser:MY_USER_ID toView:self];
}
- (void)appDidEnterForeground {
    [g_server getCurrentTimeToView:self];
}
- (void)loginSynchronizeFriends:(NSNotification*)notification{
        if (self.isLoadFriendAndGroup) {
            [g_server listAttention:0 userId:MY_USER_ID toView:self];
        }else{
            [[JY_XMPP sharedInstance] performSelector:@selector(login) withObject:nil afterDelay:2];
        }
        [[JY_XMPP sharedInstance] performSelector:@selector(login) withObject:nil afterDelay:2];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 10002) {
        [g_server performSelector:@selector(showLogin) withObject:nil afterDelay:0.5];
        return;
    }else if (buttonIndex == 1) {
        [g_server listAttention:0 userId:MY_USER_ID toView:self];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
-(void)buildTop{
    _tb = [JY_TabMenuView alloc];
    if (_vcnum == 0) {
            _tb.items = [NSArray arrayWithObjects:@"聚友",@"通讯录",@"友趣",@"我的",nil];
            _tb.imagesNormal = [NSArray arrayWithObjects:@"bar_1_nor",@"bar_2_nor",@"bar_3_nor",@"bar_4_nor",nil];
            _tb.imagesSelect = [NSArray arrayWithObjects:@"bar_1_sed",@"bar_2_sed",@"bar_3_sed",@"bar_4_sed",nil];
        }else if(self.vcnum == 1){
            _tb.items = [NSArray arrayWithObjects:Localized(@"JXMainViewController_Message"),Localized(@"JX_MailList"),_linkName1,Localized(@"JX_FindTab"),Localized(@"JX_My"),nil];
            _tb.imagesNormal = [NSArray arrayWithObjects:@"news_normal",@"group_chat_normal",@"xiangsiTab_hd",@"find_normal",@"me_normal",nil];
            _tb.imagesSelect = [NSArray arrayWithObjects:@"news_press_gray",@"group_chat_press_gray",@"xiangsiTab_hd",@"find_press_gray",@"me_press_gray",nil];
        }else if (self.vcnum == 2){
            _tb.items = [NSArray arrayWithObjects:Localized(@"JXMainViewController_Message"),Localized(@"JX_MailList"),_linkName1,_linkName2,Localized(@"JX_My"),nil];
            _tb.imagesNormal = [NSArray arrayWithObjects:@"news_normal",@"group_chat_normal",@"find_normal",@"find_normal",@"me_normal",nil];
            _tb.imagesSelect = [NSArray arrayWithObjects:@"news_press_gray",@"group_chat_press_gray",@"find_press_gray",@"find_press_gray",@"me_press_gray",nil];
        }
    _tb.delegate  = self;
    _tb.onDragout = @selector(onDragout:);
    [_tb setBackgroundImageName:@"MessageListCellBkg"];
    _tb.onClick  = @selector(actionSegment:);
    _tb = [_tb initWithFrame:CGRectMake(0, 0, ManMan_SCREEN_WIDTH, ManMan_SCREEN_BOTTOM)];
    [_bottomView addSubview:_tb];
    NSMutableArray *remindArray = [[JY_BlogRemind sharedInstance] doFetchUnread];
    [_tb setBadge:2 title:[NSString stringWithFormat:@"%ld",remindArray.count]];
    
}
-(void)onDragout:(UIButton*)sender{
 
}
-(void)actionSegment:(UIButton*)sender{
    [self doSelected:(int)sender.tag];
    
}
-(void)doSelected:(int)n{
    [_selectVC.view removeFromSuperview];
    switch (n){
        case 0:
            #ifdef IS_SHOW_MENU
                        _selectVC = _squareVC;
            #else
                        if (self.vcnum == 0) {
                            _selectVC = _msgVc;
                        }else if(self.vcnum == 1){
                            _selectVC = _msgVc;
                        }else if(self.vcnum == 2){
                            _selectVC = _msgVc;
                        }
            #endif
            break;
        case 1:
            if (self.vcnum == 0) {
                _selectVC = _friendVC;
            }else if(self.vcnum == 1){
                _selectVC = _friendVC;
            }else if(self.vcnum == 2){
                _selectVC = _friendVC;
            }
            break;
        case 2:
             
            _selectVC = _cirleFriendVc;
            
            break;
        case 3:
            if (self.vcnum == 0) {
               _selectVC = _psMyviewVC;
            }else if(self.vcnum == 1){
               _selectVC = _cirleFriendVc;
            }else if(self.vcnum == 2){
                _selectVC = _cirleFriendVc;
           }
            break;
        case 4:
            if (self.vcnum == 0) {
                _selectVC = _psMyviewVC;
             }else if(self.vcnum == 1){
                _selectVC = _psMyviewVC;
             }else if(self.vcnum == 2){
                _selectVC = _psMyviewVC;
            }
    }
    [_tb selectOne:n];
    [_mainView addSubview:_selectVC.view];
}
-(void)onXmppLoginChanged:(NSNumber*)isLogin{
    if([JY_XMPP sharedInstance].isLogined == login_status_yes){
        [g_server offlineOperation:(g_server.lastOfflineTime *1000 + g_server.timeDifference)/1000 toView:self];
        [self onAfterLogin];
    }
    switch (_tb.selected){
        case 0:
            _btn.hidden = [JY_XMPP sharedInstance].isLogined;
            break;
        case 1:
            _btn.hidden = ![JY_XMPP sharedInstance].isLogined;
            break;
        case 2:
            _btn.hidden = NO;
            break;
        case 3:
            _btn.hidden = ![JY_XMPP sharedInstance].isLogined;
            break;
    }
}
-(void)onAfterLogin{
}
-(void)hasLoginOther:(NSNotification *)notifcation{
    [g_App showAlert:Localized(@"JXXMPP_Other") delegate:self tag:10002 onlyConfirm:YES];
}
- (void)showDeviceAuth:(NSNotification *)notification{
    JY_MessageObject *msg = notification.object;
    JY_DeviceAuthController *authCon = [[JY_DeviceAuthController alloc] initWithMsg:msg];
    UIViewController *lastVC = (UIViewController *)g_navigation.subViews.lastObject;
    [lastVC presentViewController:authCon animated:YES completion:nil];
}
-(void) didServerResultSucces:(JY_Connection*)aDownload dict:(NSDictionary*)dict array:(NSArray*)array1{
    
    if ([aDownload.action isEqualToString:act_rongyunGetToken]) {
        NSString *token = [dict objectForKey:@"token"];
        NSString *reqBody = [dict objectForKey:@"reqBody"];
        reqBody = [reqBody stringByRemovingPercentEncoding];
        if (token.length > 0) {
            
            // 连接 IM 服务
            [[RCIMClient sharedRCIMClient] connectWithToken:token dbOpened:^(RCDBErrorCode code) {
                
            } success:^(NSString *userId) {
                NSLog(@"融云IM登录成功 -- userId = %@",userId);
                dispatch_async(dispatch_get_main_queue(), ^{
                    //设置当前用户信息
                    RCUserInfo *currentUserInfo = [[RCUserInfo alloc] initWithUserId:g_myself.userId
                                                                                name:g_myself.userNickname
                                                                            portrait:[g_server getHeadImageOUrl: g_myself.userId]];
                    [RCIM sharedRCIM].currentUserInfo = currentUserInfo;
                    [RCIM sharedRCIM].enablePersistentUserInfoCache = YES;
                    [RCIM sharedRCIM].enableMessageAttachUserInfo = YES;
                });
                
            } error:^(RCConnectErrorCode errorCode) {
//                [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"融云IM登录失败，errorCode= %ld",errorCode]];
            }];
            
            
        }else{
//            [SVProgressHUD showErrorWithStatus:@"未获取到token"];
        }
    }
    
    if ([aDownload.action isEqualToString:act_AttentionList]) {
        JY_ProgressVC * pv = [JY_ProgressVC alloc];
        pv.dbFriends = (long)[_friendArray count];
        pv.dataArray = array1;
        pv = [pv init];
        if (array1.count > 300) {
            [g_navigation pushViewController:pv animated:YES];
        }
    }
    if ([aDownload.action isEqualToString:act_FriendGroupList]) {
        for (NSInteger i = 0; i < array1.count; i ++) {
            NSDictionary *dict = array1[i];
            JY_LabelObject *labelObj = [[JY_LabelObject alloc] init];
            labelObj.groupId = dict[@"groupId"];
            labelObj.groupName = dict[@"groupName"];
            labelObj.userId = dict[@"userId"];
            NSArray *userIdList = dict[@"userIdList"];
            NSString *userIdListStr = [userIdList componentsJoinedByString:@","];
            if (userIdListStr.length > 0) {
                labelObj.userIdList = [NSString stringWithFormat:@"%@", userIdListStr];
            }
            [labelObj insert];
        }
        NSArray *arr = [[JY_LabelObject sharedInstance] fetchAllLabelsFromLocal];
        for (NSInteger i = 0; i < arr.count; i ++) {
            JY_LabelObject *locLabel = arr[i];
            BOOL flag = NO;
            for (NSInteger j = 0; j < array1.count; j ++) {
                NSDictionary * dict = array1[j];
                if ([locLabel.groupId isEqualToString:dict[@"groupId"]]) {
                    flag = YES;
                    break;
                }
            }
            if (!flag) {
                [locLabel delete];
            }
        }
    }
    if ([aDownload.action isEqualToString:act_offlineOperation]) {
        for (NSDictionary *dict in array1) {
            if ([[dict objectForKey:@"tag"] isEqualToString:@"label"]) {
                [g_notify postNotificationName:kOfflineOperationUpdateLabelList object:nil];
            }
            else if ([[dict objectForKey:@"tag"] isEqualToString:@"friend"]) {
                [g_server getUser:[dict objectForKey:@"friendId"] toView:self];
            }
            else if ([[dict objectForKey:@"tag"] isEqualToString:@"room"]) {
                [g_server getRoom:[dict objectForKey:@"friendId"] toView:self];
            }
        }
    }
    if ([aDownload.action isEqualToString:act_UserGet]) {
        JY_UserObject *user = [[JY_UserObject alloc] init];
        [user getDataFromDict:dict];
        if ([user.userId intValue] == [MY_USER_ID intValue]) {
            [g_server doSaveUser:dict];
        }else {
            JY_UserObject *user1 = [[JY_UserObject sharedInstance] getUserById:user.userId];
            user.content = user1.content;
            user.timeSend = user1.timeSend;
            [user update];
        }
        [g_notify postNotificationName:kOfflineOperationUpdateUserSet object:user];
    }
    if ([aDownload.action isEqualToString:act_roomGet]) {
        JY_UserObject *user = [[JY_UserObject alloc] init];
        [user getDataFromDict:dict];
        user.userId = [dict objectForKey:@"jid"];
        user.roomId = [dict objectForKey:@"id"];
        user.userNickname = [dict objectForKey:@"name"];
        if(![g_xmpp.roomPool getRoom:user.userId] || ![user haveTheUser]){
            user.userDescription = [dict objectForKey:@"desc"];
            user.showRead = [dict objectForKey:@"showRead"];
            user.showMember = [dict objectForKey:@"showMember"];
            user.allowSendCard = [dict objectForKey:@"allowSendCard"];
            user.chatRecordTimeOut = [dict objectForKey:@"chatRecordTimeOut"];
            user.talkTime = [dict objectForKey:@"talkTime"];
            user.allowInviteFriend = [dict objectForKey:@"allowInviteFriend"];
            user.allowUploadFile = [dict objectForKey:@"allowUploadFile"];
            user.allowConference = [dict objectForKey:@"allowConference"];
            user.allowSpeakCourse = [dict objectForKey:@"allowSpeakCourse"];
            user.offlineNoPushMsg = [(NSDictionary *)[dict objectForKey:@"member"] objectForKey:@"offlineNoPushMsg"];
            user.isNeedVerify = [dict objectForKey:@"isNeedVerify"];
            user.createUserId = [dict objectForKey:@"userId"];
            user.content = @" ";
            user.topTime = nil;
            [user insertRoom];
            [g_xmpp.roomPool joinRoom:user.userId title:user.userNickname lastDate:nil isNew:NO];
        }else {
            NSDictionary * groupDict = [user toDictionary];
            roomData * roomdata = [[roomData alloc] init];
            [roomdata getDataFromDict:groupDict];
            [roomdata getDataFromDict:dict];
            JY_UserObject *user1 = [[JY_UserObject sharedInstance] getUserById:roomdata.roomJid];
            user.content = user1.content;
            user.status = user1.status;
            user.offlineNoPushMsg = [NSNumber numberWithBool:roomdata.offlineNoPushMsg];
            user.msgsNew = user1.msgsNew;
            [user update];
        }
        [g_notify postNotificationName:kOfflineOperationUpdateUserSet object:user];
    }
}
-(int) didServerResultFailed:(JY_Connection*)aDownload dict:(NSDictionary*)dict{
    if ([aDownload.action isEqualToString:act_rongyunGetToken]) {
        [g_server rongyunGetTokenPortrait:[g_server getHeadImageOUrl: g_myself.userId] toView:self];
    }
    return hide_error;
}
-(int) didServerConnectError:(JY_Connection*)aDownload error:(NSError *)error{
    return hide_error;
}



@end
