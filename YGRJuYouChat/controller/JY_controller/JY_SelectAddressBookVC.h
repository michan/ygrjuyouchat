//
//  JY_SelectAddressBookVC.h
//  TFJunYouChat
//
//  Created by p on 2019/4/3.
//  Copyright © 2019年 Reese. All rights reserved.
//

#import "JY_TableViewController.h"

NS_ASSUME_NONNULL_BEGIN

@class JY_SelectAddressBookVC;

@protocol ManMan_SelectAddressBookVCDelegate <NSObject>

- (void)selectAddressBookVC:(JY_SelectAddressBookVC *)selectVC doneAction:(NSArray *)array;

@end

@interface JY_SelectAddressBookVC : JY_TableViewController

@property (nonatomic, weak) id<ManMan_SelectAddressBookVCDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
