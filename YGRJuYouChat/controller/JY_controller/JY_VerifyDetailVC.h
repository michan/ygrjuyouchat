//
//  JY_VerifyDetailVC.h
//  TFJunYouChat
//
//  Created by p on 2018/5/29.
//  Copyright © 2018年 Reese. All rights reserved.
//

#import "JY_admobViewController.h"
#import "JY_ChatViewController.h"


@interface JY_VerifyDetailVC : JY_admobViewController
@property (nonatomic, strong) JY_MessageObject *msg;
@property (nonatomic,strong) roomData * room;
@property (nonatomic, weak) JY_ChatViewController *chatVC;

@end
