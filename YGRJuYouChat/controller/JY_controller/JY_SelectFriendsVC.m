//
//  JY_SelectFriendsVC.m
//  TFJunYouChat
//
//  Created by p on 2018/7/2.
//  Copyright © 2018年 Reese. All rights reserved.
//

#import "JY_SelectFriendsVC.h"
#import "JY_ChatViewController.h"
#import "AppDelegate.h"
#import "JY_Label.h"
#import "JY_ImageView.h"
#import "JY_Cell.h"
#import "JY_RoomPool.h"
#import "JY_TableView.h"
#import "JY_NewFriendViewController.h"
#import "menuImageView.h"
#import "QCheckBox.h"
#import "JY_RoomObject.h"
#import "NSString+ContainStr.h"
#import "JY_MessageObject.h"
#import "BMChineseSort.h"
#import "QLSureMingPianView.h"

@interface JY_SelectFriendsVC ()<UITextFieldDelegate, UIAlertViewDelegate,ManMan_RoomObjectDelegate>
@property (nonatomic, strong) UIButton* finishBtn;

@property (nonatomic, strong) NSMutableArray *checkBoxArr;

@property (nonatomic, strong) UIView *noSearchDataView;

@property (nonatomic, strong) UILabel *noSearchDataLabel;

@end

@implementation JY_SelectFriendsVC
@synthesize chatRoom,room,isNewRoom,set,array=_array;

- (UILabel *)noSearchDataLabel{
    if (!_noSearchDataLabel) {
        _noSearchDataLabel = UILabel.new;
        _noSearchDataLabel.frame = CGRectMake(0, 30, SCREEN_WIDTH, 30);
        _noSearchDataLabel.font = [UIFont systemFontOfSize:15];
        _noSearchDataLabel.textColor = HEXCOLOR(0x05D168);
        _noSearchDataLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _noSearchDataLabel;
}
- (UIView *)noSearchDataView{
    if (!_noSearchDataView) {
        _noSearchDataView = [[UIView alloc] initWithFrame:CGRectMake(0, ManMan_SCREEN_TOP + 55, SCREEN_WIDTH, SCREEN_HEIGHT)];
        _noSearchDataView.backgroundColor =HEXCOLOR(0xf0f0f0);// [UIColor whiteColor];
    }
    return _noSearchDataView;
}
- (id)init
{
    self = [super init];
    if (self) {
        
        
        self.heightHeader = ManMan_SCREEN_TOP;
        self.heightFooter = 0;
        self.isGotoBack   = YES;
        //self.view.frame = g_window.bounds;
        self.isShowFooterPull = NO;
        _searchArray = [NSMutableArray array];
        _userIds = [NSMutableArray array];
        _userNames = [NSMutableArray array];
        set   = [[NSMutableSet alloc] init];
        _indexArray = [NSMutableArray array];
        _letterResultArr = [NSMutableArray array];
        _checkBoxArr = [NSMutableArray array];
        _selMenu = 0;
        
        
        [g_notify addObserver:self selector:@selector(newReceipt:) name:kXMPPReceiptNotifaction object:nil];
        [g_notify addObserver:self selector:@selector(onSendTimeout:) name:kXMPPSendTimeOutNotifaction object:nil];
        
        [g_notify addObserver:self selector:@selector(refreshNotif:) name:kLabelVCRefreshNotif object:nil];
    }
    return self;
}

- (void)refreshNotif:(NSNotification *)notif {
    [self actionQuit];
}

-(void)dealloc{
    //移除监听
    [g_notify removeObserver:self];
    [set removeAllObjects];
    [_array removeAllObjects];
    
}
-(void)keyboardChangeFrame:(NSNotification*)noti;
{
    
    CGRect rect= [noti.userInfo[UIKeyboardFrameEndUserInfoKey]CGRectValue];
    _finishBtn.frame = CGRectMake(ManMan_SCREEN_WIDTH - 87 - 15, ManMan_SCREEN_HEIGHT - 13 - 35  - rect.size.height, 87, 35);
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [_seekTextField resignFirstResponder];
}

-(void)keyboardHide:(NSNotification *)noti
{
    _finishBtn.frame = CGRectMake(ManMan_SCREEN_WIDTH - 87 - 15, ManMan_SCREEN_HEIGHT - 13 - 35, 87, 35);
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardChangeFrame:) name:UIKeyboardWillChangeFrameNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardHide:) name:UIKeyboardWillHideNotification object:nil];

    
    [self createHeadAndFoot];
    if (_type == ManMan_SelectFriendTypeGroupAT ||_type == ManMan_SelectFriendTypeSpecifyAdmin) {
        
    }else{

        _finishBtn = [UIButton buttonWithType:UIButtonTypeSystem];
        [_finishBtn setTitle:Localized(@"JX_Confirm") forState:UIControlStateNormal];
        [_finishBtn setTitle:Localized(@"JX_Confirm") forState:UIControlStateHighlighted];
        [_finishBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _finishBtn.layer.masksToBounds = YES;
        _finishBtn.layer.cornerRadius = 3.f;
//        [_finishBtn setBackgroundColor:THEMECOLOR];
        [_finishBtn.titleLabel setFont:SYSFONT(15)];
        
        [_finishBtn setTitleColor:HEXCOLOR(0x909090) forState:UIControlStateNormal];
        _finishBtn.backgroundColor = HEXCOLOR(0xD8D8D8);
//        _finishBtn.frame = CGRectMake(ManMan_SCREEN_WIDTH - 51 - 15, ManMan_SCREEN_TOP - 8 - 29, 51, 29);
        
        _finishBtn.frame = CGRectMake(ManMan_SCREEN_WIDTH - 87 - 15, ManMan_SCREEN_HEIGHT - 13 - 35, 87, 35);
        [_finishBtn addTarget:self action:@selector(onAdd:) forControlEvents:UIControlEventTouchUpInside];
//        [self.tableHeader addSubview:_finishBtn];
//        [self.tableHeader addSubview:_finishBtn];
        if (self.isSendCard) {
        }else{
            [self.view addSubview:_finishBtn];
            
        }
    }
    [self customSearchTextField];
    
    [self getDataArrayByType];
    
    [self.noSearchDataView addSubview:self.noSearchDataLabel];
    [self.view addSubview:self.noSearchDataView];
    self.noSearchDataView.hidden = YES;
}

-(void)getDataArrayByType{
    self.isShowFooterPull = NO;
    self.isShowHeaderPull = NO;
   
    
    if (_type == ManMan_SelectFriendTypeGroupAT || _type == ManMan_SelectFriendTypeSelMembers) {
        if(_type == ManMan_SelectFriendTypeSelMembers){
            self.title =@"邀请您";//Localized(@"JXSip_invite");
            [self getSelUserTypeSelMembersArray];
        }else{
            self.title = @"@群成员";
            [self getGroupATRoomMembersArray];
        }
        [_table reloadData];
    }else if(_type == ManMan_SelectFriendTypeSpecifyAdmin){
        self.title = @"管理员";
        [self getRoomMembersArray];
        [_table reloadData];
    }else if (_type == ManMan_SelectFriendTypeCustomArray) {
        //        self.title

        //选择拼音 转换的 方法
        BMChineseSortSetting.share.sortMode = 2; // 1或2
        //排序 Person对象
        [BMChineseSort sortAndGroup:_array key:@"userNickname" finish:^(bool isSuccess, NSMutableArray *unGroupArr, NSMutableArray *sectionTitleArr, NSMutableArray<NSMutableArray *> *sortedObjArr) {
            if (isSuccess) {
                self.indexArray = sectionTitleArr;
                self.letterResultArr = sortedObjArr;
                [_table reloadData];
            }
        }];

    }
    else{
        if (!self.title) {
            self.title =@"请选择联系人"; // Localized(@"JXSelFriendVC_SelFriend");
        }
        if (self.isSendCard) {
            self.title = @"选择朋友";
        }
        _array=[[NSMutableArray alloc] init];
        [self refresh];
    }
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}


- (void)customSearchTextField{
    
    //搜索输入框
    UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(0, ManMan_SCREEN_TOP, ManMan_SCREEN_WIDTH, 55)];
    backView.backgroundColor =  HEXCOLOR(0xffffff);
    [self.view addSubview:backView];
    
//    UIButton *cancelBtn = [[UIButton alloc] initWithFrame:CGRectMake(backView.frame.size.width-5-45, 5, 45, 30)];
//    [cancelBtn setTitle:Localized(@"JX_Cencal") forState:UIControlStateNormal];
//    [cancelBtn setTitleColor:THEMECOLOR forState:UIControlStateNormal];
//    [cancelBtn addTarget:self action:@selector(cancelBtnAction) forControlEvents:UIControlEventTouchUpInside];
//    cancelBtn.titleLabel.font = SYSFONT(14);
//    [backView addSubview:cancelBtn];
    
    
    _seekTextField = [[UITextField alloc] initWithFrame:CGRectMake(15, 10, backView.frame.size.width - 30, 35)];
    _seekTextField.placeholder =@"搜索";// Localized(@"JX_EnterKeyword");
    _seekTextField.textColor = [UIColor blackColor];
    [_seekTextField setFont:SYSFONT(14)];
    _seekTextField.backgroundColor = HEXCOLOR(0xffffff);
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"card_search"]];
    UIView *leftView = [[UIView alloc ]initWithFrame:CGRectMake(0, 0, 30, 30)];
    //    imageView.center = CGPointMake(leftView.frame.size.width/2, leftView.frame.size.height/2);
    imageView.center = leftView.center;
    [leftView addSubview:imageView];
    _seekTextField.leftView = leftView;
    _seekTextField.leftViewMode = UITextFieldViewModeAlways;
    _seekTextField.borderStyle = UITextBorderStyleNone;
    _seekTextField.layer.masksToBounds = YES;
    _seekTextField.layer.cornerRadius = 3.f;
    _seekTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    _seekTextField.delegate = self;
    _seekTextField.returnKeyType = UIReturnKeyGoogle;
    [backView addSubview:_seekTextField];
    [_seekTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    
    self.tableView.tableHeaderView = backView;
    
}

- (void) textFieldDidChange:(UITextField *)textField {
    
    if (textField.text.length <= 0) {
        [self getDataArrayByType];
//        self.tableView.backgroundColor = HEXCOLOR(0xF0F0F0);
        self.noSearchDataView.hidden = YES;
        return;
    }
    
    
    [_searchArray removeAllObjects];
    //    NSMutableArray *arr = [_array mutableCopy];
    //    for (NSInteger i = 0; i < arr.count; i ++) {
    //
    //        NSString * nameStr = nil;
    //        NSString * cardNameStr = nil;
    //        NSString * nickNameStr = nil;
    //        if ([arr[i] isMemberOfClass:[memberData class]]) {
    //            memberData *obj = arr[i];
    //            nameStr = obj.userName;
    //            cardNameStr = obj.cardName;
    //            nickNameStr = obj.userNickName;
    //        }else if ([arr[i] isMemberOfClass:[JY_UserObject class]]) {
    //            JY_UserObject * obj = arr[i];
    //            nameStr = obj.userNickname;
    //        }
    //        nameStr = !nameStr ? @"" : nameStr;
    //        cardNameStr = !cardNameStr ? @"" : cardNameStr;
    //        nickNameStr = !nickNameStr ? @"" : nickNameStr;
    //        NSString * allStr = [NSString stringWithFormat:@"%@%@%@",nameStr,cardNameStr,nickNameStr];
    //        if ([[allStr lowercaseString] containsMyString:[textField.text lowercaseString]]) {
    //            [_searchArray addObject:arr[i]];
    //        }
    //
    //    }
    
    if (_type == ManMan_SelectFriendTypeGroupAT || _type == ManMan_SelectFriendTypeSelMembers || _type == ManMan_SelectFriendTypeSpecifyAdmin) {
        
//        _searchArray = [memberData searchMemberByFilter:textField.text room:room.roomId];
        for (NSInteger i = 0; i < _array.count; i ++) {
            memberData *data = _array[i];
            memberData *data1 = [self.room getMember:g_myself.userId];
            JY_UserObject *allUser = [[JY_UserObject alloc] init];
            allUser = [allUser getUserById:[NSString stringWithFormat:@"%ld",data.userId]];
            NSString *name = [NSString string];
            if ([data1.role intValue] == 1) {
                name = data.lordRemarkName ? data.lordRemarkName : allUser.remarkName.length > 0  ? allUser.remarkName : data.userNickName;
            }else {
                name = allUser.remarkName.length > 0  ? allUser.remarkName : data.userNickName;
            }
            
            NSString *userStr = [name lowercaseString];
            NSString *textStr = [textField.text lowercaseString];
            if ([userStr rangeOfString:textStr].location != NSNotFound) {
                [_searchArray addObject:data];
            }
        }

    }else{
        for (NSInteger i = 0; i < _array.count; i ++) {
            JY_UserObject * user = _array[i];
            NSString *userStr = [user.userNickname lowercaseString];
            NSString *textStr = [textField.text lowercaseString];
            if ([userStr rangeOfString:textStr].location != NSNotFound) {
                [_searchArray addObject:user];
            }
        }
    }
    if (_searchArray.count == 0) {
        self.noSearchDataView.hidden = NO;
        
        NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:@"没有找到 \"" attributes:@{NSForegroundColorAttributeName:HEXCOLOR(0x303030)}];
        NSAttributedString *a1 = [[NSAttributedString alloc] initWithString:textField.text attributes:@{NSForegroundColorAttributeName:HEXCOLOR(0x05D168)}];
        NSAttributedString *a2 = [[NSAttributedString alloc] initWithString:@"\" 相关结果" attributes:@{NSForegroundColorAttributeName:HEXCOLOR(0x303030)}];
        [attr appendAttributedString:a1];
        [attr appendAttributedString:a2];
        self.noSearchDataLabel.attributedText = attr;
        self.noSearchDataView.backgroundColor =HEXCOLOR(0xffffff);
    }else{
        self.noSearchDataView.hidden = YES;
        self.noSearchDataView.backgroundColor =HEXCOLOR(0xf0f0f0);
    }
    [self.tableView reloadData];
}


- (void) cancelBtnAction {
    _seekTextField.text = nil;
    [_seekTextField resignFirstResponder];
    [self getDataArrayByType];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark   ---------tableView协议----------------
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (_seekTextField.text.length > 0) {
        return 1;
    }
    return [self.indexArray count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (_seekTextField.text.length > 0) {
        return Localized(@"JXFriend_searchTitle");
    }
    return [self.indexArray objectAtIndex:section];
}

-(void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section{
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    header.tintColor = HEXCOLOR(0xF2F2F2);
    [header.textLabel setTextColor:HEXCOLOR(0x999999)];
    [header.textLabel setFont:SYSFONT(15)];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_seekTextField.text.length > 0) {
        return _searchArray.count;
    }
    return [(NSArray *)[self.letterResultArr objectAtIndex:section] count];
}
-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView{
    if (_seekTextField.text.length > 0) {
        return nil;
    }
    return self.indexArray;
}
- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index{
    return index;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    JY_Cell *cell=nil;
    NSString* cellName = [NSString stringWithFormat:@"selVC_%ld_%ld",indexPath.section,indexPath.row];
    cell = [tableView dequeueReusableCellWithIdentifier:cellName];
    
    //    if(cell==nil){
    memberData *data = [self.room getMember:g_myself.userId];
    
    if (_type == ManMan_SelectFriendTypeGroupAT || _type == ManMan_SelectFriendTypeSpecifyAdmin ||  _type == ManMan_SelectFriendTypeSelMembers) {
        
        if (!cell) {
            cell = [[JY_Cell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellName];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        memberData * member;
        if (_seekTextField.text.length > 0) {
            member = _searchArray[indexPath.row];
        }else{
            member = [[self.letterResultArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        }
        //            cell = [[JY_Cell alloc] init];
        [_table addToPool:cell];
        NSString *name = [NSString string];
        JY_UserObject *allUser = [[JY_UserObject alloc] init];
        allUser = [allUser getUserById:[NSString stringWithFormat:@"%ld",member.userId]];
        if (_type == ManMan_SelectFriendTypeSelMembers) {
            if ([data.role intValue] == 1) {
                name = member.lordRemarkName ? member.lordRemarkName : allUser.remarkName.length > 0  ? allUser.remarkName : member.userNickName;
            }else {
                name = allUser.remarkName.length > 0  ? allUser.remarkName : member.userNickName;
            }
        }else {
            name = member.userNickName;
        }
        if (!self.room.showMember && [data.role intValue] != 1 && [data.role intValue] != 2 && member.userId > 0) {
            name = [name substringToIndex:[name length]-1];
            name = [name stringByAppendingString:@"*"];
        }

        cell.title = name;
        cell.positionTitle = [self positionStrRole:[member.role integerValue]];
        if(!member.idStr){//所有人不显示
            //                cell.subtitle = [NSString stringWithFormat:@"%ld",member.userId];
        }else{
            cell.headImage = @"groupImage";
        }
        //            cell.bottomTitle = [TimeUtil formatDate:user.timeCreate format:@"MM-dd HH:mm"];
        cell.userId = [NSString stringWithFormat:@"%ld",member.userId];
        //            [cell initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellName];
        cell.isSmall = YES;
        [cell headImageViewImageWithUserId:nil roomId:nil];
        
        if (_type == ManMan_SelectFriendTypeGroupAT) {
            if(member.idStr){
                if (room.roomId != nil) {
//                    NSString *groupImagePath = [NSString stringWithFormat:@"%@%@/%@.%@",NSTemporaryDirectory(),g_myself.userId,room.roomId,@"jpg"];
//                    if (groupImagePath && [[NSFileManager defaultManager] fileExistsAtPath:groupImagePath]) {
//                        cell.headImageView.image = [UIImage imageWithContentsOfFile:groupImagePath];
//                    }else{
//                        [roomData roomHeadImageRoomId:room.roomId toView:cell.headImageView];
//                    }
                    [g_server getRoomHeadImageSmall:room.roomJid roomId:room.roomId imageView:cell.headImageView];
                }
            }
        }
        
        if(_type == ManMan_SelectFriendTypeSelMembers){
            QCheckBox* btn = [[QCheckBox alloc] initWithDelegate:self];
            btn.frame = CGRectMake(13, 18.5, 22, 22);
            btn.tag = (indexPath.section+1) * 100000 + (indexPath.row+1);
            BOOL b = NO;
            NSString* s = [NSString stringWithFormat:@"%ld",member.userId];
            b = [_existSet containsObject:s];
            btn.selected = b;
            btn.userInteractionEnabled = !b;
            [cell addSubview:btn];
            
            [_checkBoxArr addObject:btn];
        }
        
    }else if (_type == ManMan_SelectFriendTypeCustomArray || _type == ManMan_SelectFriendTypeDisAble) {
        
        if (!cell) {
            cell = [[JY_Cell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellName];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        JY_UserObject *user;
        if (_seekTextField.text.length > 0) {
            user = _searchArray[indexPath.row];
        }else{
            user = [[self.letterResultArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        }
        [_table addToPool:cell];
        cell.title = user.remarkName.length > 0 ? user.remarkName : user.userNickname;
        //            cell.subtitle = user.userId;
        //            cell.bottomTitle = [TimeUtil formatDate:user.timeCreate format:@"MM-dd HH:mm"];
        cell.userId = user.userId;
        cell.isSmall = YES;
        [cell headImageViewImageWithUserId:nil roomId:nil];
        //        cell.headImage   = user.userHead;
        //            [cell initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellName];
        
        QCheckBox* btn = [[QCheckBox alloc] initWithDelegate:self];

        btn.frame = CGRectMake(13, 18.5, 22, 22);
        btn.tag = (indexPath.section+1) * 100000 + (indexPath.row+1);
        
        if (self.disableSet) {
            btn.enabled = ![_disableSet containsObject:user.userId];
        }else{
            btn.enabled = YES;
        }
        
        [_checkBoxArr addObject:btn];
        
        [cell addSubview:btn];
    }else{
        JY_UserObject *user;
        if (_seekTextField.text.length > 0) {
            user = _searchArray[indexPath.row];
        }else{
            user = [[self.letterResultArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        }
        cell = [[JY_Cell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellName];
        if (!self.isSendCard) {
            QCheckBox* btn = [[QCheckBox alloc] initWithDelegate:self];
            btn.frame = CGRectMake(13, 18.5, 22, 22);
            btn.tag = (indexPath.section+1) * 100000 + (indexPath.row+1);
            BOOL b = NO;
            if (self.addressBookArr.count > 0) {
                if ([_existSet containsObject:user.userId]) {
                    btn.selected = [_existSet containsObject:user.userId];
                    [self didSelectedCheckBox:btn checked:btn.selected];
                }
            }else {
                if (room){
                    b = [room isMember:user.userId];
                    BOOL flag = NO;
                    for (NSInteger i = 0; i < _userIds.count; i ++) {
                        NSString *selUserId = _userIds [i];
                        if ([user.userId isEqualToString:selUserId]) {
                            flag = YES;
                            break;
                        }
                    }
                    btn.selected = flag;
                    btn.userInteractionEnabled = !flag;
                }else{
                    b = [_existSet containsObject:user.userId];
                    BOOL flag = NO;
                    for (NSInteger i = 0; i < _userIds.count; i ++) {
                        NSString *selUserId = _userIds [i];
                        if ([user.userId isEqualToString:selUserId]) {
                            flag = YES;
                            break;
                        }
                    }

                    if (b) {
                        if (_type == ManMan_SelectFriendTypeSelFriends) {
                            btn.selected = b;
                        }else {
                            btn.enabled = !b;
                        }
                    }else {
                        if (_type == ManMan_SelectFriendTypeSelFriends) {
                            btn.selected = flag;
                        }else {
                            btn.enabled = !b;
                            btn.selected = flag;
                        }
                    }
                    [self didSelectedCheckBox:btn checked:btn.selected];
                }
            }
            [_checkBoxArr addObject:btn];
            [cell addSubview:btn];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [_table addToPool:cell];
        
        cell.title = user.remarkName.length > 0 ? user.remarkName : user.userNickname;

        //            cell.subtitle = user.userId;
        cell.bottomTitle = [TimeUtil formatDate:user.timeCreate format:@"MM-dd HH:mm"];
        cell.userId = user.userId;
        cell.isSmall = YES;

        
        [cell headImageViewImageWithUserId:nil roomId:nil];

       
        
    }
    
    
    CGFloat headX =  13*2+22;
    if (self.isSendCard) {
        headX = 15;
    }
    cell.headImageView.frame = CGRectMake(headX,9.5,40,40);
    cell.headImageView.layer.cornerRadius = 5;
    cell.lbTitle.frame = CGRectMake(CGRectGetMaxX(cell.headImageView.frame)+15, 21.5, ManMan_SCREEN_WIDTH - 115 -CGRectGetMaxX(cell.headImageView.frame)-14, 18);
    cell.lbSubTitle.frame = CGRectMake(CGRectGetMaxX(cell.headImageView.frame)+15, cell.lbSubTitle.frame.origin.y, ManMan_SCREEN_WIDTH - 55 -CGRectGetMaxX(cell.headImageView.frame)-14, cell.lbSubTitle.frame.size.height);
    cell.lineView.frame = CGRectMake(CGRectGetMaxX(cell.headImageView.frame)+15,59-LINE_WH,ManMan_SCREEN_WIDTH,LINE_WH);

    if (indexPath.row == [(NSArray *)[self.letterResultArr objectAtIndex:indexPath.section] count]-1) {
        cell.lineView.frame = CGRectMake(cell.lineView.frame.origin.x, cell.lineView.frame.origin.y, cell.lineView.frame.size.width,0);
    }else {
        cell.lineView.frame = CGRectMake(cell.lineView.frame.origin.x, cell.lineView.frame.origin.y, cell.lineView.frame.size.width,LINE_WH);
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (self.isSendCard) {

        JY_UserObject *user;
        if (_seekTextField.text.length > 0) {
            user = _searchArray[indexPath.row];
        }else{
            user = [[self.letterResultArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        }
        QLSureMingPianView *mingpian = [QLSureMingPianView initSureMintPianView];
        mingpian.frame = g_window.frame;
        mingpian.nickNameStr = user.userNickname;
        [g_server getHeadImageSmall:@"" userName:user.userNickname imageView:mingpian.head];
        mingpian.clickBlock = ^(NSString * _Nonnull message) {
            self.LeaveMessage = message;
            self.cardUser = user;
            if(self.delegate != nil && [self.delegate respondsToSelector:self.didSelect])
                [self.delegate performSelectorOnMainThread:self.didSelect withObject:self waitUntilDone:YES];
            [self actionQuit];
        };
        [g_window addSubview:mingpian];
        return;
    }
    
    if (_type == ManMan_SelectFriendTypeGroupAT) {
        memberData * member;
        if (_seekTextField.text.length > 0) {
            member = _searchArray[indexPath.row];
        }else{
            member = [[self.letterResultArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        }
        if(self.delegate != nil && [self.delegate respondsToSelector:self.didSelect])
            [self.delegate performSelectorOnMainThread:self.didSelect withObject:member waitUntilDone:YES];
        
        [self actionQuit];
        //        _pSelf = nil;
    }else if (_type == ManMan_SelectFriendTypeSpecifyAdmin) {
        
        memberData * member;
        if (_seekTextField.text.length > 0) {
            member = _searchArray[indexPath.row];
        }else{
            member = [[self.letterResultArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        }
        if ([member.role intValue] == 1) {
            [g_App showAlert:Localized(@"JXGroup_CantSetSelf")];
            return;
        }
        if(self.delegate != nil && [self.delegate respondsToSelector:self.didSelect])
            [self.delegate performSelectorOnMainThread:self.didSelect withObject:member waitUntilDone:YES];
        
        [self actionQuit];
        //        _pSelf = nil;
    }else {
        if (_type == ManMan_SelectFriendTypeSelMembers) {
            memberData *user;
            if (_seekTextField.text.length > 0) {
                user = _searchArray[indexPath.row];
            }else{
                user = [[self.letterResultArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
            }
            if (![_existSet containsObject:[NSString stringWithFormat:@"%ld",user.userId]]) {
                QCheckBox *checkBox = nil;
//                for (NSInteger i = 0; i < _checkBoxArr.count; i ++) {
//                    QCheckBox *btn = _checkBoxArr[i];
//                    if (btn.tag / 1000 == indexPath.section && btn.tag % 1000 == indexPath.row) {
//                        checkBox = btn;
//                        break;
//                    }
//                }
                JY_Cell *cell = [tableView cellForRowAtIndexPath:indexPath];
                checkBox = [cell viewWithTag:(indexPath.section+1) * 100000 + (indexPath.row+1)];
                
                checkBox.selected = !checkBox.selected;
                [self didSelectedCheckBox:checkBox checked:checkBox.selected];
            }
        }else {
            JY_UserObject *user;
            if (_seekTextField.text.length > 0) {
                user = _searchArray[indexPath.row];
            }else{
                user = [[self.letterResultArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
            }
            QCheckBox *checkBox = nil;
            JY_Cell *cell = [tableView cellForRowAtIndexPath:indexPath];
            checkBox = [cell viewWithTag:(indexPath.section+1) * 100000 + (indexPath.row+1)];
            
            if (!checkBox.enabled) {
                return;
            }
            if (!checkBox.enabled) {
                return;
            }
            checkBox.selected = !checkBox.selected;
            [self didSelectedCheckBox:checkBox checked:checkBox.selected];
        }
        
    }
}

-(void)getGroupATRoomMembersArray{
    _array = (NSMutableArray *)[memberData fetchAllMembers:room.roomId sortByName:YES];
    
    memberData * mem = [[memberData alloc] init];
    mem.userId = 0;
    mem.idStr = room.roomJid;
    mem.roomId = room.roomId;
    mem.userNickName = Localized(@"JX_AtALL");
    mem.cardName = Localized(@"JX_AtALL");
    mem.role = [NSNumber numberWithInt:10];
    //    mem.createTime = [[rs objectForColumnName:@"createTime"] longLongValue];
    
    [_array insertObject:mem atIndex:0];
    [self reomveExistsSet];
    
    //选择拼音 转换的 方法
    BMChineseSortSetting.share.sortMode = 2; // 1或2
    //排序 Person对象
    [BMChineseSort sortAndGroup:_array key:@"userNickName" finish:^(bool isSuccess, NSMutableArray *unGroupArr, NSMutableArray *sectionTitleArr, NSMutableArray<NSMutableArray *> *sortedObjArr) {
        if (isSuccess) {
            self.indexArray = sectionTitleArr;
            self.letterResultArr = sortedObjArr;
            [_table reloadData];
        }
    }];
//    //根据Person对象的 name 属性 按中文 对 Person数组 排序
//    self.indexArray = [BMChineseSort IndexWithArray:_array Key:@"userNickName"];
//    self.letterResultArr = [BMChineseSort sortObjectArray:_array Key:@"userNickName"];
}

-(void)getSelUserTypeSelMembersArray{
    _array = (NSMutableArray *)[memberData fetchAllMembers:room.roomId sortByName:YES];
    [self reomveExistsSet];
    //选择拼音 转换的 方法
    BMChineseSortSetting.share.sortMode = 2; // 1或2
    //排序 Person对象
    [BMChineseSort sortAndGroup:_array key:@"userNickName" finish:^(bool isSuccess, NSMutableArray *unGroupArr, NSMutableArray *sectionTitleArr, NSMutableArray<NSMutableArray *> *sortedObjArr) {
        if (isSuccess) {
            self.indexArray = sectionTitleArr;
            self.letterResultArr = sortedObjArr;
            [_table reloadData];
        }
    }];

//    //根据Person对象的 name 属性 按中文 对 Person数组 排序
//    self.indexArray = [BMChineseSort IndexWithArray:_array Key:@"userNickName"];
//    self.letterResultArr = [BMChineseSort sortObjectArray:_array Key:@"userNickName"];
}

-(void)getRoomMembersArray{
    _array = (NSMutableArray *)[memberData fetchAllMembers:room.roomId sortByName:NO];
    [self reomveExistsSet];
    //选择拼音 转换的 方法
    BMChineseSortSetting.share.sortMode = 2; // 1或2
    //排序 Person对象
    [BMChineseSort sortAndGroup:_array key:@"userNickName" finish:^(bool isSuccess, NSMutableArray *unGroupArr, NSMutableArray *sectionTitleArr, NSMutableArray<NSMutableArray *> *sortedObjArr) {
        if (isSuccess) {
            self.indexArray = sectionTitleArr;
            self.letterResultArr = sortedObjArr;
            [_table reloadData];
        }
    }];
//    //根据Person对象的 name 属性 按中文 对 Person数组 排序
//    self.indexArray = [BMChineseSort IndexWithArray:_array Key:@"userNickName"];
//    self.letterResultArr = [BMChineseSort sortObjectArray:_array Key:@"userNickName"];
}

-(void)reomveExistsSet{
    for(NSInteger i=[_array count]-1;i>=0;i--){
        memberData* p = [_array objectAtIndex:i];
        if([self.existSet containsObject:[NSString stringWithFormat:@"%ld",p.userId]]>0)
            [_array removeObjectAtIndex:i];
    }
}

-(void)getArrayData{
    if (self.addressBookArr.count > 0) {
        [_userIds removeAllObjects];
        _array = [NSMutableArray arrayWithArray:self.addressBookArr];
    }else {
        _array=[[JY_UserObject sharedInstance] fetchAllFriendsFromLocal];
    }
    if (self.isShowMySelf) {
        JY_UserObject *mySelf = [[JY_UserObject alloc] init];
        mySelf.userId = g_myself.userId;
        mySelf.userNickname = g_myself.userNickname;
        [_array insertObject:mySelf atIndex:0];
    }
    
    for(NSInteger i=[_array count]-1;i>=0;i--){
        JY_UserObject* u = [_array objectAtIndex:i];
        //。
        if (u.userId.intValue ==10000||u.userId.intValue ==1000050) {
            [_array removeObjectAtIndex:i];
        }
        for (int j=0; j<[room.members count]; j++) {
            memberData* p = [room.members objectAtIndex:j];
            if(p.userId == [u.userId intValue]){
                [_array removeObjectAtIndex:i];
                break;
            }
        }
        
        if (self.isForRoom) {
            if([self.forRoomUser.userId isEqualToString:u.userId]){
                [_array removeObjectAtIndex:i];
            }
        }
        
        if (self.addressBookArr.count > 0) {
            if (![_userIds containsObject:u.userId]) {
                [_userIds addObject:u.userId];
                [_userNames addObject:u.userNickname];
            }
        }
    }
    //选择拼音 转换的 方法
    BMChineseSortSetting.share.sortMode = 2; // 1或2
    //排序 Person对象
    [BMChineseSort sortAndGroup:_array key:@"userNickname" finish:^(bool isSuccess, NSMutableArray *unGroupArr, NSMutableArray *sectionTitleArr, NSMutableArray<NSMutableArray *> *sortedObjArr) {
        if (isSuccess) {
            self.indexArray = sectionTitleArr;
            self.letterResultArr = sortedObjArr;
            [_table reloadData];
        }
    }];

//    NSLog(@"------- indexArray start");
//    //根据Person对象的 name 属性 按中文 对 Person数组 排序
//    self.indexArray = [BMChineseSort IndexWithArray:_array Key:@"userNickname"];
//    NSLog(@"------- letterResultArr start");
//
//    self.letterResultArr = [BMChineseSort sortObjectArray:_array Key:@"userNickname"];
//    NSLog(@"------- letterResultArr stop");
    if(isNewRoom && [_array count]<=0)//没有好友时
        [self performSelector:@selector(onAdd:) withObject:nil afterDelay:0.1];
}

-(void)refresh{
    [self stopLoading];
    _refreshCount++;
    [_array removeAllObjects];
    
    [self getArrayData];
    for (NSString *userId in _existSet) {
        
        NSString *userName = [JY_UserObject getUserNameWithUserId:userId];
        
        if (!userName) {
            userName = @"";
        }
        if (![_userIds containsObject:userId]) {
            [_userIds addObject:userId];
            [_userNames addObject:userName];
        }
    }
    [_table reloadData];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [_seekTextField resignFirstResponder];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 59;
}

-(void)scrollToPageUp{
    [self refresh];
}

- (void)didSelectedCheckBox:(QCheckBox *)checkbox checked:(BOOL)checked{
    if (!checkbox.enabled) {
        return;
    }

    if (self.type == 0 && checked && self.maxSize > 0 &&  _userIds.count  == self.maxSize) {
        checkbox.selected = NO;
        [JY_MyTools showTipView:@"群成员已达上限"];
        return;
    }
        id user;
        if (_seekTextField.text.length > 0) {
            user = _searchArray[checkbox.tag % 100000-1];
        }else{
            user = [[self.letterResultArr objectAtIndex:checkbox.tag / 100000-1] objectAtIndex:checkbox.tag % 100000-1];
        }
    
        NSString *userId;
        NSString *userNickname;
        if ([user isKindOfClass:[JY_UserObject class]]) {
            JY_UserObject *userO = (JY_UserObject *)user;
            if (self.room.isSecretGroup) {
                if (!userO.publicKeyRSARoom || userO.publicKeyRSARoom.length <= 0) {
                    checkbox.selected = NO;
                    [JY_MyTools showTipView:@"此用户不能进入私密群组"];
                    return;
                }
            }
            
            userId = [NSString stringWithFormat:@"%@",userO.userId];
            userNickname = [NSString stringWithFormat:@"%@",userO.userNickname];
        }else if ([user isKindOfClass:[memberData class]]) {
            memberData *member = (memberData *)user;
            userId = [NSString stringWithFormat:@"%ld",member.userId];
            userNickname = [NSString stringWithFormat:@"%@",member.userNickName];
        }
        if(checked){
            if (![_userIds containsObject:userId]) {
                [_userIds addObject:userId];
                [_userNames addObject:userNickname];
            }
            if (![set containsObject:[NSNumber numberWithInteger:checkbox.tag]]) {
                [set addObject:[NSNumber numberWithInteger:checkbox.tag]];
            }

        }
        else{
            if ([_userIds containsObject:userId]) {
                NSInteger index = [_userIds indexOfObject:userId];
                [_userIds removeObject:userId];
                [_userNames removeObjectAtIndex:index];
                [set removeObject:[NSNumber numberWithInteger:checkbox.tag]];
            }
        }
    
    if (_userIds.count == 0) {
        [_finishBtn setTitle:@"完成" forState:UIControlStateNormal];
        [_finishBtn setTitleColor:HEXCOLOR(0x909090) forState:UIControlStateNormal];
        _finishBtn.backgroundColor = HEXCOLOR(0xD8D8D8);
        
    }else{
        [_finishBtn setTitle:[NSString stringWithFormat:@"完成(%ld)",_userIds.count] forState:UIControlStateNormal];
        [_finishBtn setTitleColor:HEXCOLOR(0xffffff) forState:UIControlStateNormal];
        _finishBtn.backgroundColor = HEXCOLOR(0x05D168);
    }
    if (self.isSendCard) {
        _finishBtn.hidden = YES;
    }
}

-(void)onAdd:(UIButton *)btn{
    btn.enabled = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        btn.enabled = YES;
    });
    
    if (self.isSendCard) {
        QLSureMingPianView *mingpian = [QLSureMingPianView initSureMintPianView];
        mingpian.frame = g_window.frame;
        for(NSNumber* n in self.set){
            JY_UserObject *user;
            if (self.seekTextField.text.length > 0) {
                user = self.searchArray[[n intValue] % 100000-1];
            }else{
                user = [[self.letterResultArr objectAtIndex:[n intValue] / 100000-1] objectAtIndex:[n intValue] % 100000-1];
            }
            mingpian.nickNameStr = user.userNickname;
            [g_server getHeadImageSmall:@"" userName:user.userNickname imageView:mingpian.head];
        }
        mingpian.clickBlock = ^(NSString * _Nonnull message) {
            self.LeaveMessage = message;
            if(self.delegate != nil && [self.delegate respondsToSelector:self.didSelect])
                [self.delegate performSelectorOnMainThread:self.didSelect withObject:self waitUntilDone:YES];
            [self actionQuit];
        };
        [g_window addSubview:mingpian];
        return;
    }
    
    if(_type == ManMan_SelectFriendTypeSelFriends || chatRoom || self.isForRoom){
        
        if (!self.addressBookArr || self.addressBookArr.count <= 0) {
            if (self.isForRoom) {
                if (![_userIds containsObject:self.forRoomUser.userId]) {
                    [_userIds addObject:self.forRoomUser.userId];
                    [_userNames addObject:self.forRoomUser.userNickname];
                }
            }
        }
        
        if(self.isForRoom){
            
            NSString* s = [NSUUID UUID].UUIDString;
            s = [[s stringByReplacingOccurrencesOfString:@"-" withString:@""] lowercaseString];
            
            NSString *roomName = [NSString stringWithFormat:@"%@、%@",MY_USER_NAME,[_userNames componentsJoinedByString:@"、"]];
            
            room.roomJid= s;
            room.name   = roomName;
            room.desc   = nil;
            room.userId = [g_myself.userId longLongValue];
            room.userNickName = MY_USER_NAME;
            room.showRead = NO;
            room.showMember = YES;
            room.allowSendCard = YES;
            room.isLook = YES;
            room.isNeedVerify = NO;
            room.allowInviteFriend = YES;
            room.allowUploadFile = YES;
            room.allowConference = YES;
            room.allowSpeakCourse = YES;
            
            [g_server addRoom:room isPublic:YES isNeedVerify:NO category:0 toView:self];
//            chatRoom = [[JY_XMPP sharedInstance].roomPool createRoom:s title:roomName];
//            chatRoom.delegate = self;
            
            [_wait start:Localized(@"JXAlert_CreatRoomIng") delay:30];
            return;
        }
        if ((self.room.isNeedVerify && self.room.userId != [g_myself.userId longLongValue]) || _type == ManMan_SelectFriendTypeSelFriends) {
            
            if (self.isShowAlert) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:Localized(@"JX_SaveLabelNextTime") message:nil delegate:self cancelButtonTitle:Localized(@"JX_DepositAsLabel") otherButtonTitles:Localized(@"JX_Ignore"), nil];
                alert.tag = 2457;
                [alert show];
                return;
            }
            
            
            if(self.delegate != nil && [self.delegate respondsToSelector:self.didSelect])
                [self.delegate performSelectorOnMainThread:self.didSelect withObject:self waitUntilDone:YES];
            [self actionQuit];
        }else {
            [g_server addRoomMember:room.roomId userArray:_userIds toView:self];//用接口即可
        }
        if(isNewRoom){
            [self onNewRoom];
            [self actionQuit];
        }
        return;
    }
    if (_type == ManMan_SelectFriendTypeGroupAT)
        return;
    if (_type == ManMan_SelectFriendTypeSpecifyAdmin)
        return;
    if (_type == ManMan_SelectFriendTypeSelMembers){
    }
    if(self.delegate != nil && [self.delegate respondsToSelector:self.didSelect])
        [self.delegate performSelectorOnMainThread:self.didSelect withObject:self waitUntilDone:YES];
    
    [self actionQuit];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 2457) {
        if (buttonIndex == 0) {
            if ([self.delegate respondsToSelector:self.alertAction]) {
                [self.delegate performSelectorOnMainThread:self.alertAction withObject:self waitUntilDone:YES];
            }
        }else {
            if(self.delegate != nil && [self.delegate respondsToSelector:self.didSelect])
                [self.delegate performSelectorOnMainThread:self.didSelect withObject:self waitUntilDone:YES];
            [self actionQuit];
        }
    }
    
}

-(void)xmppRoomDidCreate{
    [g_server addRoom:room isPublic:YES isNeedVerify:NO category:0 toView:self];
    chatRoom.delegate = nil;
}

-(void)onNewRoom{
    JY_ChatViewController *sendView=[JY_ChatViewController alloc];
    sendView.title = chatRoom.roomTitle;
    sendView.roomJid = chatRoom.roomJid;
    sendView.roomId = room.roomId;
    sendView.chatRoom = chatRoom;
    sendView.room = self.room;
    
    JY_UserObject * user = [[JY_UserObject alloc]init];
    user = [user getUserById:chatRoom.roomJid];
    sendView.chatPerson = user;
    
    sendView = [sendView init];
    //    [g_App.window addSubview:sendView.view];
    [g_navigation pushViewController:sendView animated:YES];
}

-(NSString *)positionStrRole:(NSInteger)role{
    if (_type == ManMan_SelectFriendTypeSpecifyAdmin) {
        NSString * roleStr = nil;
        switch (role) {
            case 1://创建者
                roleStr =@"群主";// Localized(@"JXGroup_Owner");
                break;
            case 2://管理员
                roleStr =@"管理员";// Localized(@"JXGroup_Admin");
                break;
            case 3://普通成员
                roleStr =@"普通成员";// Localized(@"JXGroup_RoleNormal");
                break;
            default:
                roleStr =@"普通成员";// Localized(@"JXGroup_RoleNormal");
                break;
        }
        return roleStr;
    }
    return nil;
}

-(void)onSendTimeout:(NSNotification *)notifacation//超时未收到回执
{
    //    [_wait stop];
    //    JY_MessageObject *msg     = (JY_MessageObject *)notifacation.object;
    //    if([msg.type intValue] == kWCMessageTypeInvite)
    //        [g_App showAlert:[NSString stringWithFormat:@"邀请：%@失败，请重新邀请",msg.toUserName]];
}

-(void)newReceipt:(NSNotification *)notifacation{//新回执
    //    [_wait stop];
    //    JY_MessageObject *msg     = (JY_MessageObject *)notifacation.object;
    //    if([msg.type intValue] == kWCMessageTypeInvite)
    //        [g_server addRoomMember:room.roomId userId:msg.toUserId nickName:msg.toUserName toView:self];
}

-(void) didServerResultSucces:(JY_Connection*)aDownload dict:(NSDictionary*)dict array:(NSArray*)array1{
    [_wait stop];
    if( [aDownload.action isEqualToString:act_roomMemberSet] ){
        for (int i=0;i<[_userIds count];i++) {
            NSString *userId=[_userIds objectAtIndex:i];
            
            memberData* p = [[memberData alloc] init];
            p.userId = [userId intValue];
            p.userNickName = [_userNames objectAtIndex:i];
            p.role = [NSNumber numberWithInt:3];
            [room.members addObject:p];
        }
        if(self.delegate != nil && [self.delegate respondsToSelector:self.didSelect])
            [self.delegate performSelectorOnMainThread:self.didSelect withObject:self waitUntilDone:YES];
        
        [_userIds removeAllObjects];
        [_userNames removeAllObjects];
        [self actionQuit];
        //        _pSelf = nil;
    }
    
    if( [aDownload.action isEqualToString:act_roomAdd] ){
        room.roomId = [dict objectForKey:@"id"];
        //        _room.call = [NSString stringWithFormat:@"%@",[dict objectForKey:@"call"]];
        chatRoom = [[JY_XMPP sharedInstance].roomPool createRoom:room.roomJid title:room.name];
        chatRoom.delegate = self;
        [self insertRoom];
        [g_notify postNotificationName:kUpdateUserNotifaction object:nil];
        [g_notify postNotificationName:kActionRelayQuitVC object:nil];
//        [g_server addRoomMember:room.roomId userArray:_userIds toView:self];//用接口即可
        if(isNewRoom){
            [self onNewRoom];
        }

        int maxUser = [[dict objectForKey:@"maxUserSize"] intValue];
        if (_userIds.count > maxUser) {
            [JY_MyTools showTipView:@"群成员超过上限"];
            [_userIds removeAllObjects];
            [_userNames removeAllObjects];
            [self actionQuit];
        }else {
            
            [g_server addRoomMember:room.roomId userArray:_userIds toView:self];//用接口即可
        }

    }
}
-(void)insertRoom{
    JY_UserObject* user = [[JY_UserObject alloc]init];
    user.userNickname = room.name;
    user.userId = room.roomJid;
    user.userDescription = room.desc;
    user.roomId = room.roomId;
    user.content = Localized(@"JX_WelcomeGroupChat");
    user.showRead =  [NSNumber numberWithBool:room.showRead];
    user.showMember = [NSNumber numberWithBool:room.showMember];
    user.allowSendCard = [NSNumber numberWithBool:room.allowSendCard];
    user.chatRecordTimeOut = room.chatRecordTimeOut;
    user.talkTime = [NSNumber numberWithLong:room.talkTime];
    user.allowInviteFriend = [NSNumber numberWithBool:room.allowInviteFriend];
    user.allowUploadFile = [NSNumber numberWithBool:room.allowUploadFile];
    user.allowConference = [NSNumber numberWithBool:room.allowConference];
    user.allowSpeakCourse = [NSNumber numberWithBool:room.allowSpeakCourse];
    user.isNeedVerify = [NSNumber numberWithBool:room.isNeedVerify];
    [user insertRoom];
}

-(int) didServerResultFailed:(JY_Connection*)aDownload dict:(NSDictionary*)dict{
    [_wait stop];
    return show_error;
}

-(int) didServerConnectError:(JY_Connection*)aDownload error:(NSError *)error{//error为空时，代表超时
    [_wait stop];
    return show_error;
}

-(void) didServerConnectStart:(JY_Connection*)aDownload{
    [_wait start];
}

- (void)actionQuit {
    if (self.isAddWindow) {
        [self.view removeFromSuperview];
        self.view = nil;
    }else{
        [super actionQuit];
    }
}

@end
