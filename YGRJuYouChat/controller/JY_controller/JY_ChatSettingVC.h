//
//  JY_ChatSettingVC.h
//  TFJunYouChat
//
//  Created by p on 2018/5/19.
//  Copyright © 2018年 Reese. All rights reserved.
//

#import "JY_admobViewController.h"
#import "JY_RoomObject.h"

@interface JY_ChatSettingVC : JY_admobViewController

@property (nonatomic,strong) JY_UserObject *user;
@property (nonatomic,strong) JY_RoomObject* chatRoom;
@property (nonatomic,strong) roomData * room;

@end
