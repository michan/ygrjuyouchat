//
//  JY_myMediaVC.h
//
//  Created by lifengye on 2020/09/03.
//  Copyright (c) 2014年 Reese. All rights reserved.
//

#import "JY_TableViewController.h"
#import <UIKit/UIKit.h>
@class menuImageView;
@class JY_MediaCell;

@interface JY_myMediaVC: JY_TableViewController{
    NSMutableArray* _array;
    int _refreshCount;
    JY_MediaCell* _cell;
}
@property(nonatomic,weak) id delegate;
@property(assign) SEL didSelect;
- (void) onAddVideo;

@end
