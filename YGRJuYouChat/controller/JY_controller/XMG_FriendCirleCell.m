//
//  XMG_FriendCirleCell.m
//  JTManyChildrenSongs
//
//  Created by os on 2020/12/3.
//  Copyright © 2020 Reese. All rights reserved.
//

#import "XMG_FriendCirleCell.h"
   
@interface XMG_FriendCirleCell()

@property (nonatomic,weak)UIImageView *titleImg;
@property (nonatomic,weak)UILabel *subTitle;

@end
@implementation XMG_FriendCirleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
          
        
        UIImageView *titleImg=[[UIImageView alloc]init];
        titleImg.image=[UIImage imageNamed:@"homeicon5"];
        [self.contentView addSubview:titleImg];
        _titleImg=titleImg;
        [titleImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.centerY.mas_equalTo(self.contentView.mas_centerY);
        }];
        
        UILabel *subTitle=[[UILabel alloc]init];
        subTitle.font = SYSFONT(15);
        subTitle.text=Localized(@"JXNewFriendVC_NewFirend");
        [self.contentView addSubview:subTitle];
        _subTitle=subTitle;
        [subTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(titleImg.mas_right).mas_offset(10);
            make.centerY.mas_equalTo(titleImg.mas_centerY);
        }];
        
        UIImageView *rightImg=[[UIImageView alloc]init];
        rightImg.image=[UIImage imageNamed:@"new_icon_>"];
        [self.contentView addSubview:rightImg];
        [rightImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.contentView.mas_right).offset(-10);
            make.centerY.mas_equalTo(self.contentView.mas_centerY);
            make.height.equalTo(@13);
            make.width.equalTo(@7);
        }];
         
        
    }
    
    
    return self;
}

- (void)setUserBaseModel:(JY_UserBaseObj *)userBaseModel {
    
    _userBaseModel=userBaseModel;
    _titleImg.image=[UIImage imageNamed:userBaseModel.userId];
    self.subTitle.text=userBaseModel.userNickname;
    
}

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"XMG_FriendCirleCell";
    XMG_FriendCirleCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[XMG_FriendCirleCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:ID];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    
    return cell;
}

-(void)setFrame:(CGRect)frame{
    
    frame.origin.y += 1;
    frame.size.height-=1;
//    frame.origin.x =15;
//    frame.size.width = SCREEN_WIDTH - 30;
    
    [super setFrame:frame];
}
@end

