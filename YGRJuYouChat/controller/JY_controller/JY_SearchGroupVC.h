//
//  JY_SearchGroupVC.h
//  TFJunYouChat
//
//  Created by p on 2019/4/1.
//  Copyright © 2019年 Reese. All rights reserved.
//

#import "JY_TableViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface JY_SearchGroupVC : JY_TableViewController

@property (nonatomic,copy) NSString *searchName;

@end

NS_ASSUME_NONNULL_END
