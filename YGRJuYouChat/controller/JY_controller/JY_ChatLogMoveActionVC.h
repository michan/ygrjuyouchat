//
//  JY_ChatLogMoveActionVC.h
//  TFJunYouChat
//
//  Created by p on 2019/6/11.
//  Copyright © 2019年 Reese. All rights reserved.
//

#import "JY_admobViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface JY_ChatLogMoveActionVC : JY_admobViewController

- (void)moveActionFinish;

@end

NS_ASSUME_NONNULL_END
