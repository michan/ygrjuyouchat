//
//  JY_FriendViewController.h.m
//
//  Created by lifengye on 2020/09/03.
//  Copyright (c) 2014年 Reese. All rights reserved.
//

#import "JY_FriendViewController.h"
#import "JY_ChatViewController.h"
#import "AppDelegate.h"
#import "JY_Label.h"
#import "JY_ImageView.h"
#import "JY_Cell.h"
#import "JY_RoomPool.h"
#import "JY_TableView.h"
#import "JY_NewFriendViewController.h"
#import "menuImageView.h"
#import "FMDatabase.h"
#import "JY_ProgressVC.h"
#import "JY_TopSiftJobView.h"
#import "JY_UserInfoVC.h"
#import "BMChineseSort.h"
#import "JY_GroupViewController.h"
#import "OrganizTreeViewController.h"
#import "JY_TabMenuView.h"
#import "JY_PublicNumberVC.h"
#import "JY_BlackFriendVC.h"
#import "JY__DownListView.h"
#import "JY_NewRoomVC.h"
#import "JY_NearVC.h"
#import "JY_SearchUserVC.h"
#import "JY_ScanQRViewController.h"
#import "JY_LabelVC.h"
#import "JY_AddressBookVC.h"
#import "JY_SearchVC.h"
#import "JY_LabelObject.h"
#import "CYWebCustomerServiceVC.h"
#import "JXMsgCellView.h"
#import "UIView+LK.h"
#define HEIGHT 60
#define IMAGE_HEIGHT  45  // 图片宽高
#define INSET_HEIGHT  10  // 图片文字间距

#define MY_INSET  0

@interface JY_FriendViewController ()<UITextFieldDelegate,ManMan_SelectMenuViewDelegate>
@property (nonatomic, strong) JY_UserObject * currentUser;
//排序后的出现过的拼音首字母数组
@property(nonatomic,strong)NSMutableArray *indexArray;
//排序好的结果数组
@property(nonatomic,strong)NSMutableArray *letterResultArr;


@property (nonatomic, strong) UITextField *seekTextField;
@property (nonatomic, strong) NSMutableArray *searchArray;

@property (nonatomic, strong) UILabel *friendNewMsgNum;
@property (nonatomic, strong) UILabel *abNewMsgNum;
@property (nonatomic, strong) UIButton *moreBtn;
@property (nonatomic, strong) UIView *menuView;

@property (nonatomic, strong) UILabel *myLabel;

@property (nonatomic, assign) CGFloat btnHeight;  // 按钮的真实高度

@property (nonatomic,copy)NSArray *customerArr; //客服信息
@property (nonatomic,weak)JXMsgCellView *cellView;

@property (nonatomic, strong)UIImageView *head;
@end

@implementation JY_FriendViewController

- (id)init
{
    self = [super init];
    if (self) {
        self.isOneInit = YES;
        self.heightHeader = ManMan_SCREEN_TOP;
        self.heightFooter = ManMan_SCREEN_BOTTOM;
        if (_isMyGoIn) {
            self.isGotoBack   = YES;
            self.heightFooter = 0;
        }
//        self.view.frame = g_window.bounds;
        [self createHeadAndFoot];
        [self buildTop];
//        CGRect frame = self.tableView.frame;
//        frame.origin.y += 40;
//        frame.size.height -= 40;
//        self.tableView.frame = frame;
        self.tableView.backgroundColor=  APP_COLOR249;
         [self customView];

        _selMenu = 0;
        
        
//        self.title = Localized(@"JXInputVC_Friend");
        self.title = Localized(@"JX_MailList");
        [g_notify  addObserver:self selector:@selector(newFriend:) name:kXMPPNewFriendNotifaction object:nil];
        
        [g_notify addObserver:self selector:@selector(newRequest:) name:kXMPPNewRequestNotifaction object:nil];
        
        [g_notify addObserver:self selector:@selector(newRequest:) name:kFriendChangeNameNotif object:nil];//好友更改昵称
        
        [g_notify addObserver:self selector:@selector(newReceipt:) name:kXMPPReceiptNotifaction object:nil];
        [g_notify addObserver:self selector:@selector(onSendTimeout:) name:kXMPPSendTimeOutNotifaction object:nil];
        [g_notify addObserver:self selector:@selector(friendRemarkNotif:) name:kFriendRemark object:nil];
        
        [g_notify  addObserver:self selector:@selector(newMsgCome:) name:kXMPPNewMsgNotifaction object:nil];
        [g_notify addObserver:self selector:@selector(friendListRefresh:) name:kFriendListRefresh object:nil];
        [g_notify addObserver:self selector:@selector(refreshABNewMsgCount:) name:kRefreshAddressBookNotif object:nil];
        [g_notify addObserver:self selector:@selector(contactRegisterNotif:) name:kMsgComeContactRegister object:nil];
        [g_notify addObserver:self selector:@selector(newRequest:) name:kFriendPassNotif object:nil];
        [g_notify addObserver:self selector:@selector(refresh) name:kOfflineOperationUpdateUserSet object:nil];
        [g_notify addObserver:self selector:@selector(updateLabels:) name:kXMPPMessageUpadtePasswordNotification object:nil];
        [g_server customerLinkList:self];
        [self setupCustomerLinkList];
    }
    return self;
}

- (void)updateLabels:(NSNotification *)noti {
    JY_MessageObject *msg = noti.object;
    if ([msg.objectId isEqualToString:SYNC_LABEL]) {
        // 同步标签
        [g_server friendGroupListToView:self];
    }
}

- (void)friendListRefresh:(NSNotification *)notif {
    
    [self refresh];
}

- (void)contactRegisterNotif:(NSNotification *)notif {
    JY_MessageObject *msg = notif.object;
    
    NSDictionary *dict = (NSDictionary *)msg.content;
    if ([msg.content isKindOfClass:[NSString class]]) {
        SBJsonParser * resultParser = [[SBJsonParser alloc] init] ;
        dict = [resultParser objectWithString:msg.content];
    }
    JY_AddressBook *addressBook = [[JY_AddressBook alloc] init];
    addressBook.toUserId = [NSString stringWithFormat:@"%@",dict[@"toUserId"]];
    addressBook.toUserName = dict[@"toUserName"];
    addressBook.toTelephone = dict[@"toTelephone"];
    addressBook.telephone = dict[@"telephone"];
    addressBook.registerEd = dict[@"registerEd"];
    addressBook.registerTime = [NSDate dateWithTimeIntervalSince1970:[dict[@"registerTime"] longLongValue]];
    addressBook.isRead = [NSNumber numberWithBool:0];
    [addressBook insert];
    
    [self refreshABNewMsgCount:nil];
}

- (void)refreshABNewMsgCount:(NSNotification *)notif {
    [self refresh];
    JY_MsgAndUserObject* newobj = [[JY_MsgAndUserObject alloc]init];
    newobj.user = [[JY_UserObject sharedInstance] getUserById:FRIEND_CENTER_USERID];
    [self showNewMsgCount:[newobj.user.msgsNew integerValue]];
}

#pragma mark  接受新消息广播
-(void)newMsgCome:(NSNotification *)notifacation
{
    JY_MessageObject *msg = notifacation.object;
    if (![msg isAddFriendMsg]) {
        return;
    }
    
    NSString* s;
    s = [msg getTableName];
    JY_MsgAndUserObject* newobj = [[JY_MsgAndUserObject alloc]init];
    newobj.user = [[JY_UserObject sharedInstance] getUserById:s];
    [self showNewMsgCount:[newobj.user.msgsNew integerValue]];
    
}

- (void) showNewMsgCount:(NSInteger)friendNewMsgNum {
//    friendNewMsgNum = 100;
    if (friendNewMsgNum >= 10 && friendNewMsgNum <= 99) {
        self.friendNewMsgNum.font = SYSFONT(12);
    }else if (friendNewMsgNum > 0 && friendNewMsgNum < 10) {
        self.friendNewMsgNum.font = SYSFONT(13);
    }else if(friendNewMsgNum > 99){
        self.friendNewMsgNum.font = SYSFONT(9);
    }

    self.friendNewMsgNum.text = [NSString stringWithFormat:@"%ld",friendNewMsgNum];
    
    if (friendNewMsgNum <= 0) {
        self.friendNewMsgNum.hidden = YES;
    }else{
        self.friendNewMsgNum.hidden = NO;
    }
    
    NSMutableArray *abUread = [[JY_AddressBook sharedInstance] doFetchUnread];
    if (abUread.count >= 10 && abUread.count <= 99) {
        self.friendNewMsgNum.font = SYSFONT(12);
    }else if (abUread.count > 0 && abUread.count < 10) {
        self.friendNewMsgNum.font = SYSFONT(13);
    }else if(abUread.count > 99){
        self.friendNewMsgNum.font = SYSFONT(9);
    }

    self.abNewMsgNum.text = [NSString stringWithFormat:@"%ld",abUread.count];
    if (abUread.count <= 0) {
        self.abNewMsgNum.hidden = YES;
    }else {
        self.abNewMsgNum.hidden = NO;
    }
    
    NSInteger num = friendNewMsgNum + abUread.count;
//    if (num <= 0) {
//        [g_mainVC.tb setBadge:1 title:@"0"];
//    }else {
//        [g_mainVC.tb setBadge:1 title:[NSString stringWithFormat:@"%ld", num]];
//    }
    if (num <= 0) {
           int number = 0;
           if (g_App.linkArray.count == 0) {
               number = 1;
            }else if(g_App.linkArray.count == 1){
              number = 1;
            }else if(g_App.linkArray.count == 2){
              number = 1;
           }

        _cellView.bageNumber=[NSString stringWithFormat:@"%ld",num];
           [g_mainVC.tb setBadge:number title:@"0"];
       }else {
           int number = 0;
           if (g_App.linkArray.count == 0) {
               number = 1;
            }else if(g_App.linkArray.count == 1){
              number = 1;
            }else if(g_App.linkArray.count == 2){
              number = 1;
           }
           NSLog(@"%@",g_mainVC.tb);
           
           
           _cellView.bageNumber=[NSString stringWithFormat:@"%ld",num];
           [g_mainVC.tb setBadge:number title:[NSString stringWithFormat:@"%ld", num]];
       }

}

-(void)newRequest:(NSNotification *)notifacation
{
    [self getFriend];
}

- (void)scrollToPageUp {
   [self getFriend];
}

-(void)buildTop{
    //刷新好友列表
//    UIButton * getFriendBtn = [[UIButton alloc]initWithFrame:CGRectMake(ManMan_SCREEN_WIDTH-35, ManMan_SCREEN_TOP - 34, 30, 30)];
//    getFriendBtn.custom_acceptEventInterval = .25f;
//    [getFriendBtn addTarget:self action:@selector(getFriend) forControlEvents:UIControlEventTouchUpInside];
////    [getFriendBtn setImage:[UIImage imageNamed:@"synchro_friends"] forState:UIControlStateNormal];
//    [getFriendBtn setBackgroundImage:[UIImage imageNamed:@"synchro_friends"] forState:UIControlStateNormal];
//    [self.tableHeader addSubview:getFriendBtn];
//    message_add_friend_black
    self.moreBtn = [UIFactory createButtonWithImage:@"add_friends_new"
                                          highlight:nil
                                             target:self
                                           selector:@selector(onMore:)];
    self.moreBtn.custom_acceptEventInterval = 1.0f;
    self.moreBtn.frame = CGRectMake(ManMan_SCREEN_WIDTH - 18-BTN_RANG_UP*2, ManMan_SCREEN_TOP - 18-BTN_RANG_UP*2, 18+BTN_RANG_UP*2, 18+BTN_RANG_UP*2);
    [self.tableHeader addSubview:self.moreBtn];
}

- (void) customView {
    //搜索输入框
    
    backView = [[UIView alloc] initWithFrame:CGRectMake(0, ManMan_SCREEN_TOP, ManMan_SCREEN_WIDTH, 235 + HEIGHT)];
    backView.backgroundColor =HEXCOLOR(0xf0f0f0);
    [self.view addSubview:backView];
    self.tableView.tableHeaderView = backView;
    
    _seekTextField = [[UITextField alloc] initWithFrame:CGRectMake(15, 10, backView.frame.size.width - 30, 30)];
    _seekTextField.placeholder = Localized(@"JX_Seach");
    _seekTextField.textColor = [UIColor blackColor];
    [_seekTextField setFont:SYSFONT(14)];
    _seekTextField.layer.cornerRadius =5;
    _seekTextField.layer.masksToBounds = YES;
    _seekTextField.backgroundColor = [UIColor whiteColor]; HEXCOLOR(0xf0f0f0);
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"card_search"]];
    UIView *leftView = [[UIView alloc ]initWithFrame:CGRectMake(0, 0, 40, 40)];
    imageView.center = leftView.center;
//    imageView.xmg_x = (kScreenW-40)/2-24;
    [leftView addSubview:imageView];
    _seekTextField.leftView = leftView;
    _seekTextField.leftViewMode = UITextFieldViewModeAlways;
    _seekTextField.borderStyle = UITextBorderStyleNone;
    _seekTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    _seekTextField.delegate = self;
    _seekTextField.returnKeyType = UIReturnKeyGoogle;
    [backView addSubview:_seekTextField];
    [_seekTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];

    
//    JXMsgCellView *cellView= [JXMsgCellView XIBMsgCellView];
//    cellView.frame=CGRectMake(0, CGRectGetMaxY(_seekTextField.frame)+10, ManMan_SCREEN_WIDTH, 145);
////    cellView.layer.cornerRadius=8;
////    cellView.layer.masksToBounds=YES;
//    [backView addSubview:cellView];
//    _cellView=cellView;
//
//
//    [cellView.friendViewTap addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(friendViewTap)]];
//
//    [cellView.groundChatView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(groundChatViewTap)]];
//
//    [cellView.addresView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(addresViewTap)]];
//    [cellView.jixinView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(jixinViewTap)]];
    
    int h=CGRectGetMaxY(_seekTextField.frame)+10;
    int w=ManMan_SCREEN_WIDTH;
    JY_ImageView* iv;
    
//    NSString *name =
    iv = [self createButton:@"" drawTop:NO drawBottom:YES icon:@"icon_ziji" click:nil];
    self.friendNewMsgNum = [[UILabel alloc] init];
    [iv addSubview:self.friendNewMsgNum];
    iv.frame = CGRectMake(MY_INSET,h, w-MY_INSET*2, HEIGHT);
    h+=iv.frame.size.height;
    
  
    
    _myLabel = [[UILabel alloc] initWithFrame:CGRectMake(75, 0, self_width-35-20-35, HEIGHT)];
    _myLabel.text = @"自己";
    _myLabel.font = g_factory.font16;
    _myLabel.backgroundColor = [UIColor clearColor];
    _myLabel.textColor = HEXCOLOR(0x323232);
    [iv addSubview:_myLabel];
    _myLabel.text = g_server.myself.userNickname;
    
    /*  */
    iv = [self createButton:@"新朋友" drawTop:NO drawBottom:YES icon:THESIMPLESTYLE ? @"icon_xinpy" : @"icon_xinpy" click:@selector(friendViewTap)];
    self.friendNewMsgNum = [[UILabel alloc] init];
    [iv addSubview:self.friendNewMsgNum];
    iv.frame = CGRectMake(MY_INSET,h, w-MY_INSET*2, HEIGHT);
    h+=iv.frame.size.height;
    
    self.friendNewMsgNum = [[UILabel alloc] initWithFrame:CGRectMake(50, 5, 20, 20)];
    self.friendNewMsgNum.backgroundColor = HEXCOLOR(0xEF2D37);
    self.friendNewMsgNum.font = SYSFONT(12);
    self.friendNewMsgNum.textAlignment = NSTextAlignmentCenter;
    self.friendNewMsgNum.layer.cornerRadius = self.friendNewMsgNum.frame.size.width / 2;
    self.friendNewMsgNum.layer.masksToBounds = YES;
    self.friendNewMsgNum.textColor = [UIColor whiteColor];
    self.friendNewMsgNum.hidden = YES;
    self.friendNewMsgNum.text = @"99";
    
    
    
    [iv addSubview:self.friendNewMsgNum];
    
    /*  */
    
    iv = [self createButton:@"我的标签" drawTop:NO drawBottom:NO icon:THESIMPLESTYLE ? @"icon_biaoqian" : @"icon_biaoqian" click:@selector(jixinViewTap)];
    iv.frame = CGRectMake(MY_INSET,h, w-MY_INSET*2, HEIGHT);
    
    h+=iv.frame.size.height;
    /*  */
    iv = [self createButton:@"我的群聊" drawTop:NO drawBottom:YES icon:THESIMPLESTYLE ? @"icon_qunzu" : @"icon_qunzu" click:@selector(groundChatViewTap)];
    iv.frame = CGRectMake(MY_INSET,h, w-MY_INSET*2, HEIGHT);
    h+=iv.frame.size.height;
    
}

-(JY_ImageView*)createButton:(NSString*)title drawTop:(BOOL)drawTop drawBottom:(BOOL)drawBottom icon:(NSString*)icon click:(SEL)click{
    JY_ImageView* btn = [[JY_ImageView alloc] init];
    btn.backgroundColor = [UIColor whiteColor];
    btn.userInteractionEnabled = YES;
    btn.didTouch = click;
    btn.delegate = self;
//    btn.layer.cornerRadius =5;
//    btn.layer.masksToBounds =YES;
    [backView addSubview:btn];
   
     
  
    JY_Label* p = [[JY_Label alloc] initWithFrame:CGRectMake(75, 0, self_width-35-20-35, HEIGHT)];
    p.text = title;
    p.font = g_factory.font16;
    p.backgroundColor = [UIColor clearColor];
    p.textColor = HEXCOLOR(0x323232);
    [btn addSubview:p];
    if(icon){
        UIImageView* iv = [[UIImageView alloc] initWithFrame:CGRectMake(15, 7.5, 45, 45)];
        iv.image = [UIImage imageNamed:icon];
        iv.contentMode = UIViewContentModeScaleAspectFill;
        [btn addSubview:iv];
        if ([icon isEqualToString:@"icon_ziji"]) {
            iv.layer.cornerRadius = 10;
            iv.layer.masksToBounds = YES;
            iv.layer.borderWidth = 3.f;
            iv.layer.borderColor = [UIColor whiteColor].CGColor;
            iv.frame = CGRectMake(12.5, 5, 50, 50);
            _head = iv;
            [g_server getHeadImageSmall:g_server.myself.userId userName:g_server.myself.userNickname imageView:_head];
        }
    }
    if(drawTop){
        UIView* line = [[UIView alloc] initWithFrame:CGRectMake(53,0,ManMan_SCREEN_WIDTH-53,LINE_WH)];
        //line.backgroundColor = THE_LINE_COLOR;
        [btn addSubview:line];
    }
    if(drawBottom){
        UIView* line = [[UIView alloc] initWithFrame:CGRectMake(53,HEIGHT-0.3,ManMan_SCREEN_WIDTH-53,LINE_WH)];
        line.backgroundColor = THE_LINE_COLOR;
        line.alpha = 0.5;
        [btn addSubview:line];
    }
    if(click){
        UIImageView* iv;
        iv = [[UIImageView alloc] initWithFrame:CGRectMake(ManMan_SCREEN_WIDTH-27, (HEIGHT-13)/2, 7, 13)];
        iv.image = [UIImage imageNamed:@"new_icon_>"];
        [btn addSubview:iv];
    }
    return btn;
}

- (void)friendViewTap{
    
     
    JY_MsgAndUserObject* newobj = [[JY_MsgAndUserObject alloc]init];
    newobj.user = [[JY_UserObject sharedInstance] getUserById:FRIEND_CENTER_USERID];
    newobj.message = [[JY_MessageObject alloc] init];
    newobj.message.toUserId = FRIEND_CENTER_USERID;
    newobj.user.msgsNew = [NSNumber numberWithInt:0];
    [newobj.message updateNewMsgsTo0];
    
    NSArray *friends = [[JY_FriendObject sharedInstance] fetchAllFriendsFromLocal];
    for (NSInteger i = 0; i < friends.count; i ++) {
        JY_FriendObject *friend = friends[i];
        if ([friend.msgsNew integerValue] > 0) {
            [friend updateNewMsgUserId:friend.userId num:0];
            [friend updateNewFriendLastContent];
        }
    }
    
    [self showNewMsgCount:0];

    JY_NewFriendViewController* vc = [[JY_NewFriendViewController alloc]init];
    [g_navigation pushViewController:vc animated:YES];
}
- (void)groundChatViewTap{ 
    
    JY_GroupViewController *vc = [[JY_GroupViewController alloc] init];
    [g_navigation pushViewController:vc animated:YES];
}
// 我的设备
- (void)addresViewTap{
    
    JY_BlackFriendVC *vc = [[JY_BlackFriendVC alloc] init];
    vc.isDevice = YES;
    vc.title = Localized(@"JX_MyDevices");
    [g_navigation pushViewController:vc animated:YES];
}

- (void)jixinViewTap{
    JY_LabelVC *vc = [[JY_LabelVC alloc] init];
    [g_navigation pushViewController:vc animated:YES];
}

  
- (void) setFooterView {
    
    if (_array.count > 0) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, ManMan_SCREEN_WIDTH, 44)];
        label.textColor = HEXCOLOR(0x999999);
        label.textAlignment = NSTextAlignmentCenter;
        label.font = SYSFONT(16);
        label.text = [NSString stringWithFormat:@"%ld%@",_array.count,Localized(@"JX_ContactsNumber")];
        
        self.tableView.tableFooterView = label;
    }else {
        self.tableView.tableFooterView = nil;
    }
}

- (void)showMenuView { // 显示菜单栏
    _menuView.hidden = NO;
    CGRect backFrame = backView.frame;
    backFrame.size.height = _btnHeight*2 + 20 + 55;
    backView.frame = backFrame;
    
    CGRect menuFrame = _menuView.frame;
    menuFrame.size.height = _btnHeight*2 + 20;
    _menuView.frame = menuFrame;
    self.tableView.tableHeaderView = backView;
}

- (void)hideMenuView { // 隐藏菜单栏
    _menuView.hidden = YES;
    CGRect backFrame = backView.frame;
    backFrame.size.height = 55;
    backView.frame = backFrame;
    self.tableView.tableHeaderView = backView;
}


#pragma mark 右上角更多
-(void)onMore:(UIButton *)sender{
    NSArray *role = MY_USER_ROLE;
    if ([g_App.config.hideSearchByFriends intValue] == 1 && ([g_App.config.isCommonFindFriends intValue] == 0 || role.count > 0)) {
        [self onSearch];
    }else {
        NSMutableArray *titles = [NSMutableArray arrayWithArray:@[Localized(@"JX_LaunchGroupChat"), Localized(@"JX_Scan"), Localized(@"JXNearVC_NearPer"),Localized(@"JX_SearchPublicNumber")]];
        NSMutableArray *images = [NSMutableArray arrayWithArray:@[@"message_creat_group_black", @"messaeg_scnning_black", @"message_near_person_black",@"message_search_publicNumber"]];
        NSMutableArray *sels = [NSMutableArray arrayWithArray:@[@"onNewRoom", @"showScanViewController", @"onNear",@"searchPublicNumber"]];
        if ([g_App.config.isCommonCreateGroup intValue] == 1 && role.count <= 0) {
            [titles removeObject:Localized(@"JX_LaunchGroupChat")];
            [images removeObject:@"message_creat_group_black"];
            [sels removeObject:@"onNewRoom"];
        }
        if ([g_App.config.isOpenPositionService intValue] == 1) {
            [titles removeObject:Localized(@"JXNearVC_NearPer")];
            [images removeObject:@"message_near_person_black"];
            [sels removeObject:@"onNear"];
        }
        if (![g_App.config.enableMpModule boolValue]) {
            [titles removeObject:Localized(@"JX_SearchPublicNumber")];
            [images removeObject:@"message_search_publicNumber"];
            [sels removeObject:@"searchPublicNumber"];
        }
        
        JY_SelectMenuView *menuView = [[JY_SelectMenuView alloc] initWithTitle:titles image:images cellHeight:44];
        menuView.sels = sels;
        menuView.delegate = self;
        [g_App.window addSubview:menuView];
    }

}

- (void)didMenuView:(JY_SelectMenuView *)MenuView WithIndex:(NSInteger)index {
    
    
    NSString *method = MenuView.sels[index];
    SEL _selector = NSSelectorFromString(method);
    [self performSelectorOnMainThread:_selector withObject:nil waitUntilDone:YES];
    
}

// 搜索公众号
- (void)searchPublicNumber {
    JY_SearchUserVC *searchUserVC = [JY_SearchUserVC alloc];
    searchUserVC.type = ManMan_SearchTypePublicNumber;
    searchUserVC = [searchUserVC init];
    [g_navigation pushViewController:searchUserVC animated:YES];
}


- (void) moreListActionWithIndex:(NSInteger)index {
    
}


//搜索好友
-(void)onSearch{
    JY_SearchUserVC* vc = [JY_SearchUserVC alloc];
    vc.delegate  = self;
    vc.didSelect = @selector(doSearch:);
    vc.type = ManMan_SearchTypeUser;
    vc = [vc init];
    [g_navigation pushViewController:vc animated:YES];
    
    [self cancelBtnAction];
}
-(void)doSearch:(searchData*)p{
    
    JY_NearVC *nearVC = [[JY_NearVC alloc]init];
    nearVC.isSearch = YES;
    [g_navigation pushViewController:nearVC animated:YES];
    [nearVC doSearch:p];
}
// 扫一扫
-(void)showScanViewController{
//    button.enabled = NO;
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        button.enabled = YES;
//    });
    
    AVAuthorizationStatus authStatus =  [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if (authStatus == AVAuthorizationStatusRestricted || authStatus ==AVAuthorizationStatusDenied)
    {
        [g_server showMsg:Localized(@"JX_CanNotopenCenmar")];
        return;
    }
    
    JY_ScanQRViewController * scanVC = [[JY_ScanQRViewController alloc] init];
    
    //    [g_window addSubview:scanVC.view];
    [g_navigation pushViewController:scanVC animated:YES];
}


// 新朋友
- (void)newFriendAction:(JY_ImageView *)imageView {
    // 清空角标
    JY_MsgAndUserObject* newobj = [[JY_MsgAndUserObject alloc]init];
    newobj.user = [[JY_UserObject sharedInstance] getUserById:FRIEND_CENTER_USERID];
    newobj.message = [[JY_MessageObject alloc] init];
    newobj.message.toUserId = FRIEND_CENTER_USERID;
    newobj.user.msgsNew = [NSNumber numberWithInt:0];
    [newobj.message updateNewMsgsTo0];
    
    NSArray *friends = [[JY_FriendObject sharedInstance] fetchAllFriendsFromLocal];
    for (NSInteger i = 0; i < friends.count; i ++) {
        JY_FriendObject *friend = friends[i];
        if ([friend.msgsNew integerValue] > 0) {
            [friend updateNewMsgUserId:friend.userId num:0];
            [friend updateNewFriendLastContent];
        }
    }
    
    [self showNewMsgCount:0];

    JY_NewFriendViewController* vc = [[JY_NewFriendViewController alloc]init];
    [g_navigation pushViewController:vc animated:YES];
    
}

// 群组
- (void)myGroupAction:(JY_ImageView *)imageView {
    JY_GroupViewController *vc = [[JY_GroupViewController alloc] init];
    [g_navigation pushViewController:vc animated:YES];
}


// 标签
- (void)labelAction:(JY_ImageView *)imageView {
    
    JY_LabelVC *vc = [[JY_LabelVC alloc] init];
    [g_navigation pushViewController:vc animated:YES];
}

// 手机通讯录
- (void)addressBookAction:(JY_ImageView *)imageView {
    
    JY_AddressBookVC *vc = [[JY_AddressBookVC alloc] init];
    NSMutableArray *arr = [[JY_AddressBook sharedInstance] doFetchUnread];
    vc.abUreadArr = arr;
    [g_navigation pushViewController:vc animated:YES];
    [[JY_AddressBook sharedInstance] updateUnread];
    
    JY_MsgAndUserObject* newobj = [[JY_MsgAndUserObject alloc]init];
    newobj.user = [[JY_UserObject sharedInstance] getUserById:FRIEND_CENTER_USERID];
    [self showNewMsgCount:[newobj.user.msgsNew integerValue]];
}

-(JY_ImageView*)createButton:(NSString*)title drawTop:(BOOL)drawTop drawBottom:(BOOL)drawBottom icon:(NSString*)icon click:(SEL)click superView:(UIView *)superView{
    JY_ImageView* btn = [[JY_ImageView alloc] init];
    btn.backgroundColor = [UIColor whiteColor];
    btn.userInteractionEnabled = YES;
    btn.didTouch = click;
    btn.delegate = self;
    [superView addSubview:btn];
    
    JY_Label* p = [[JY_Label alloc] initWithFrame:CGRectMake(42 + 14 + 14, 0, 200, HEIGHT)];
    p.text = title;
    p.font = g_factory.font16;
    p.backgroundColor = [UIColor clearColor];
    p.textColor = HEXCOLOR(0x323232);
    //    p.delegate = self;
    //    p.didTouch = click;
    [btn addSubview:p];
    
    if(icon){
        UIImageView* iv = [[UIImageView alloc] initWithFrame:CGRectMake(14, (HEIGHT-42)/2, 42, 42)];
        iv.image = [UIImage imageNamed:icon];
        iv.layer.cornerRadius = iv.frame.size.width / 2;
        iv.layer.masksToBounds = YES;
        [btn addSubview:iv];
    }
    
    if(drawTop){
        UIView* line = [[UIView alloc] initWithFrame:CGRectMake(0,0,ManMan_SCREEN_WIDTH,LINE_WH)];
        line.backgroundColor = THE_LINE_COLOR;
        [btn addSubview:line];
    }
    
    if(drawBottom){
        UIView* line = [[UIView alloc] initWithFrame:CGRectMake(0,HEIGHT-LINE_WH,ManMan_SCREEN_WIDTH,LINE_WH)];
        line.backgroundColor = THE_LINE_COLOR;
        [btn addSubview:line];
    }
    
//    if(click){
//        UIImageView* iv;
//        iv = [[UIImageView alloc] initWithFrame:CGRectMake(ManMan_SCREEN_WIDTH-INSETS-20-3, 16, 20, 20)];
//        iv.image = [UIImage imageNamed:@"set_list_next"];
//        [btn addSubview:iv];
//
//    }
    return btn;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [textField resignFirstResponder];
    JY_SearchVC *vc = [[JY_SearchVC alloc] initWithTable:@"friendsearchrecord"];
    [g_navigation pushViewController:vc animated:YES];
}

- (void) textFieldDidChange:(UITextField *)textField {
    
    if (textField.text.length <= 0) {
        if (!self.isMyGoIn) {
            [self showMenuView];
        }
        [self getArrayData];
        [self.tableView reloadData];
        return;
    }else {
        [self hideMenuView];
    }
    
    [_searchArray removeAllObjects];
    if (_selMenu == 0) {
        _searchArray = [[JY_UserObject sharedInstance] fetchFriendsFromLocalWhereLike:textField.text];
    }else if (_selMenu == 1){
        _searchArray = [[JY_UserObject sharedInstance] fetchBlackFromLocalWhereLike:textField.text];
    }
    
    [self.tableView reloadData];
}

- (void) cancelBtnAction {
    if (_seekTextField.text.length > 0) {
        _seekTextField.text = nil;
        [self getArrayData];
    }
    [_seekTextField resignFirstResponder];
    [self.tableView reloadData];
}

-(void)onClick:(UIButton*)sender{
}

//筛选点击
- (void)topItemBtnClick:(UIButton *)btn{
    [self checkAfterScroll:(btn.tag-100)];
    [_topSiftView resetAllParaBtn];
}

- (void)checkAfterScroll:(CGFloat)offsetX{
    if (offsetX == 0) {
        _selMenu = 0;
    }else {
        _selMenu = 1;
    }
    [self scrollToPageUp];
}

- (void)getFriend{
   [g_server listAttention:0 userId:MY_USER_ID toView:self];
}

//-(void)actionSegment:(UISegmentedControl*)sender{
//    _selMenu = (int)sender.selectedSegmentIndex;
//    [self refresh];
//}

- (void)viewDidLoad{
    [super viewDidLoad];
    _array=[[NSMutableArray alloc] init];
    [self refresh];
    
    [g_notify addObserver:self selector:@selector(doRefresh:) name:kUpdateUserNotifaction object:nil];
    
    [self showNewMsgCount:1];
}
-(void)doRefresh:(NSNotification *)notifacation{
    [g_server getHeadImageSmall:g_server.myself.userId userName:g_server.myself.userNickname imageView:_head];
    _myLabel.text = g_server.myself.userNickname;
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    // 离开时重置_isMyGoIn
    if (_isMyGoIn) {
        _isMyGoIn = NO;
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark   ---------tableView协议----------------
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (_seekTextField.text.length > 0) {
        return 1;
    }
    return [self.indexArray count];
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (_seekTextField.text.length > 0) {
        return Localized(@"JXFriend_searchTitle");
    }
    return [self.indexArray objectAtIndex:section];
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section{
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    header.tintColor = HEXCOLOR(0xF2F2F2);
    [header.textLabel setTextColor:HEXCOLOR(0x999999)];
    [header.textLabel setFont:SYSFONT(15)];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (_seekTextField.text.length > 0) {
        return _searchArray.count;
    }
    return [(NSArray *)[self.letterResultArr objectAtIndex:section] count];
}
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView{
    if (_seekTextField.text.length > 0) {
        return nil;
    }
    return _indexArray;
}
- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index{
    return index;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    JY_UserObject *user;
    if (_seekTextField.text.length > 0) {
        user = _searchArray[indexPath.row];
    }else{
        user = [[_letterResultArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    }
    
    JY_Cell *cell=nil;
    NSString* cellName = [NSString stringWithFormat:@"msg_%d_%ld",_refreshCount,indexPath.row];
    cell = [tableView dequeueReusableCellWithIdentifier:cellName];
    
    if(cell==nil){
        
        cell = [[JY_Cell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellName];
        [_table addToPool:cell];
//        cell.layer.cornerRadius = 6;
//        cell.layer.masksToBounds = YES;
        
        
    }
    cell.user = user;
    cell.userId = user.userId;
    cell.title = user.userNickname;
    cell.title = [self multipleLoginIsOnlineTitle:user];
    //    cell.subtitle = user.userId;
    cell.index = (int)indexPath.row;
    cell.delegate = self;
    cell.didTouch = @selector(onHeadImage:);
    //    cell.bottomTitle = [TimeUtil formatDate:user.timeCreate format:@"MM-dd HH:mm"];
    //    [cell setForTimeLabel:[TimeUtil formatDate:user.timeCreate format:@"MM-dd HH:mm"]];
    cell.timeLabel.frame = CGRectMake(ManMan_SCREEN_WIDTH - 120-20, 9, 115, 20);
    
    [cell.lbTitle setText:cell.title];
    
    cell.dataObj = user;
    //    cell.headImageView.tag = (int)indexPath.row;
    //    cell.headImageView.delegate = cell.delegate;
    //    cell.headImageView.didTouch = cell.didTouch;
    
    cell.isSmall = YES;
    [cell headImageViewImageWithUserId:user.userId roomId:nil];
    
    if (indexPath.section == self.letterResultArr.count-1 && indexPath.row == [(NSArray *)[self.letterResultArr objectAtIndex:indexPath.section] count]-1) {
        cell.lineView.frame = CGRectMake(0, cell.lineView.frame.origin.y, ManMan_SCREEN_WIDTH, cell.lineView.frame.size.height);
    }else if (indexPath.row == [(NSArray *)[self.letterResultArr objectAtIndex:indexPath.section] count]-1) {
        cell.lineView.frame = CGRectMake(cell.lineView.frame.origin.x, cell.lineView.frame.origin.y, cell.lineView.frame.size.width,0);
    }else {
        cell.lineView.frame = CGRectMake(cell.lineView.frame.origin.x, cell.lineView.frame.origin.y, cell.lineView.frame.size.width,LINE_WH);
    }
    
    return cell;
}
 
//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
//    if (_seekTextField.text.length > 0 || section != self.letterResultArr.count-1) {
//        return 0;
//    }
//    return 0;
//}
//
//- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
//    if (_seekTextField.text.length > 0 || section != self.letterResultArr.count-1) {
//        return nil;
//    }
//    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, ManMan_SCREEN_WIDTH, 44)];
//    label.textColor = HEXCOLOR(0x999999);
//    label.textAlignment = NSTextAlignmentCenter;
//    label.font = SYSFONT(16);
//    label.text = [NSString stringWithFormat:@"%ld位联系人",_array.count];
//
//    return label;
//}

- (NSString *)multipleLoginIsOnlineTitle:(JY_UserObject *)user {
    NSString *isOnline;
    
    NSString *isOnLine = [NSString stringWithFormat:@"%@",user.isOnLine];
    
    if ([isOnLine intValue] == 1) {
        isOnline = [NSString stringWithFormat:@"(%@)", Localized(@"JX_OnLine")];
    }else {
        isOnline = [NSString stringWithFormat:@"(%@)", Localized(@"JX_OffLine")];
    }
    NSString *title = user.remarkName.length > 0 ? user.remarkName : user.userNickname;
    if ([user.userId isEqualToString:ANDROID_USERID] || [user.userId isEqualToString:PC_USERID] || [user.userId isEqualToString:MAC_USERID]) {
        title = [title stringByAppendingString:isOnline];
    }
    return title;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{    [super tableView:tableView didSelectRowAtIndexPath:indexPath];
    
    JY_Cell * cell = [_table cellForRowAtIndexPath:indexPath];
    cell.selected = NO;
    
    // 黑名单列表不能点击
    if (_selMenu == 1) {
        return;
    }
    
    JY_UserObject *user;
    if (_seekTextField.text.length > 0) {
        user = _searchArray[indexPath.row];
    }else{
        user = [[self.letterResultArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    }
    
    
    if([user.userId isEqualToString:FRIEND_CENTER_USERID]){
        JY_NewFriendViewController* vc = [[JY_NewFriendViewController alloc]init];
//        [g_App.window addSubview:vc.view];
        [g_navigation pushViewController:vc animated:YES];
//        [vc release];
        return;
    }
    
    JY_MessageObject *msg = [[JY_MessageObject alloc] init];
    JY_ChatViewController *sendView=[JY_ChatViewController alloc];
    if([user.roomFlag intValue] > 0  || user.roomId.length > 0){
        sendView.roomJid = user.userId;
        sendView.roomId = user.roomId;
        [[JY_XMPP sharedInstance].roomPool joinRoom:user.userId title:user.userNickname lastDate:nil isNew:NO];
    }
    sendView.title = user.remarkName.length > 0  ? user.remarkName : user.userNickname;
    sendView.chatPerson = user;
    sendView.lastMsg = msg;
   sendView = [sendView init];
//    [g_App.window addSubview:sendView.view];
    [g_navigation pushViewController:sendView animated:YES];
    
    msg.toUserId = user.userId;
    [msg updateNewMsgsTo0];
    [g_notify postNotificationName:kChatViewDisappear object:nil];
    
//    [sendView release];
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (_seekTextField.text.length <= 0){
        if (_selMenu == 0) {
            JY_UserObject *user = [[self.letterResultArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
            if (user.userId.length <= 5) {
                return NO;
            }else{
                return YES;
            }
        }else{
            return YES;
        }
    }else{
        return NO;
    }
}
 

- (NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (_selMenu == 0) {
        UITableViewRowAction *deleteBtn = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:Localized(@"JX_Delete") handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
            JY_UserObject *user = [[self.letterResultArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
            _currentUser = user;
            [g_server delFriend:user.userId toView:self];
        }];
        
        return @[deleteBtn];
    }
    else {
        UITableViewRowAction *cancelBlackBtn = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:Localized(@"REMOVE") handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
            JY_UserObject *user = [[self.letterResultArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
            _currentUser = user;
            [g_server delBlacklist:user.userId toView:self];
        }];
        
        return @[cancelBlackBtn];
    }
   
}

//-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
//    if (_selMenu == 0 && editingStyle == UITableViewCellEditingStyleDelete) {
//        JY_UserObject *user=_array[indexPath.row];
//        _currentUser = user;
//        [g_server delFriend:user.userId toView:self];
//    }
//}

- (void)dealloc {
    [g_notify removeObserver:self name:kUpdateUserNotifaction object:nil];
    [g_notify removeObserver:self];
//    [_table release];
    [_array removeAllObjects];
//    [_array release];
//    [super dealloc];
}

-(void)getArrayData{
    switch (_selMenu) {
        case 0:{
            //获取好友列表
//            if (self.isOneInit) {//是否新建
//                [g_server listAttention:0 userId:MY_USER_ID toView:self];
//                self.isOneInit = NO;
//            }
            
            //从数据库获取好友staus为2且不是room的
            _array=[[JY_UserObject sharedInstance] fetchAllFriendsFromLocal];
            //选择拼音 转换的 方法
            BMChineseSortSetting.share.sortMode = 2; // 1或2
            //排序 Person对象
            [BMChineseSort sortAndGroup:_array key:@"userNickname" finish:^(bool isSuccess, NSMutableArray *unGroupArr, NSMutableArray *sectionTitleArr, NSMutableArray<NSMutableArray *> *sortedObjArr) {
                if (isSuccess) {
                    self.indexArray = sectionTitleArr;
                    self.letterResultArr = sortedObjArr;
                    [self->_table reloadData];
                }
            }];

//            //根据Person对象的 name 属性 按中文 对 Person数组 排序
//            self.indexArray = [BMChineseSort IndexWithArray:_array Key:@"userNickname"];
//            self.letterResultArr = [BMChineseSort sortObjectArray:_array Key:@"userNickname"];
//
            self.isShowFooterPull = NO;
        }
            break;
        case 1:{
            //获取黑名單列表
            
            //从数据库获取好友staus为-1的
            _array=[[JY_UserObject sharedInstance] fetchAllBlackFromLocal];
            //选择拼音 转换的 方法
            BMChineseSortSetting.share.sortMode = 2; // 1或2
          //排序 Person对象
            [BMChineseSort sortAndGroup:_array key:@"userNickname" finish:^(bool isSuccess, NSMutableArray *unGroupArr, NSMutableArray *sectionTitleArr, NSMutableArray<NSMutableArray *> *sortedObjArr) {
                if (isSuccess) {
                    self.indexArray = sectionTitleArr;
                    self.letterResultArr = sortedObjArr;
                    [_table reloadData];
                }
            }];
            //根据Person对象的 name 属性 按中文 对 Person数组 排序
//            self.indexArray = [BMChineseSort IndexWithArray:_array Key:@"userNickname"];
//            self.letterResultArr = [BMChineseSort sortObjectArray:_array Key:@"userNickname"];
        }
            break;
        case 2:{
            _array=[[JY_UserObject sharedInstance] fetchAllRoomsFromLocal];
            //选择拼音 转换的 方法
            BMChineseSortSetting.share.sortMode = 2; // 1或2
            //排序 Person对象
            [BMChineseSort sortAndGroup:_array key:@"userNickname" finish:^(bool isSuccess, NSMutableArray *unGroupArr, NSMutableArray *sectionTitleArr, NSMutableArray<NSMutableArray *> *sortedObjArr) {
             if (isSuccess) {
                    self.indexArray = sectionTitleArr;
                    self.letterResultArr = sortedObjArr;
                    [self->_table reloadData];
                }
            }];
        }
//            //根据Person对象的 name 属性 按中文 对 Person数组 排序
//            self.indexArray = [BMChineseSort IndexWithArray:_array Key:@"userNickname"];
//            self.letterResultArr = [BMChineseSort sortObjectArray:_array Key:@"userNickname"];
            break;
    }
    
    [self setFooterView];
}
//服务器返回数据
-(void) didServerResultSucces:(JY_Connection*)aDownload dict:(NSDictionary*)dict array:(NSArray*)array1{
    
    //更新本地好友
    if ([aDownload.action isEqualToString:act_AttentionList]) {
        [_wait stop];
        [self stopLoading];
        JY_ProgressVC * pv = [JY_ProgressVC alloc];
        // 服务端不会返回新朋友 ， 减去新朋友
        pv.dbFriends = (long)[_array count] - 1;
        pv.dataArray = array1;
        pv = [pv init];
 
    }
    
    if ([aDownload.action isEqualToString:act_FriendDel]) {
        [_currentUser doSendMsg:XMPP_TYPE_DELALL content:nil];
    }
    
    if([aDownload.action isEqualToString:act_BlacklistDel]){
        [_currentUser doSendMsg:XMPP_TYPE_NOBLACK content:nil];
    }
    
    if( [aDownload.action isEqualToString:act_UserGet] ){
        [_wait stop];
        
        JY_UserObject* user = [[JY_UserObject alloc]init];
        [user getDataFromDict:dict];
        
        JY_UserInfoVC* vc = [JY_UserInfoVC alloc];
        vc.user       = user;
        vc.fromAddType = 6;
        vc = [vc init];
//        [g_window addSubview:vc.view];
        [g_navigation pushViewController:vc animated:YES];
    }
    // 同步标签
    if ([aDownload.action isEqualToString:act_FriendGroupList]) {
        
        for (NSInteger i = 0; i < array1.count; i ++) {
            NSDictionary *dict = array1[i];
            JY_LabelObject *labelObj = [[JY_LabelObject alloc] init];
            labelObj.groupId = dict[@"groupId"];
            labelObj.groupName = dict[@"groupName"];
            labelObj.userId = dict[@"userId"];
            
            NSArray *userIdList = dict[@"userIdList"];
            NSString *userIdListStr = [userIdList componentsJoinedByString:@","];
            if (userIdListStr.length > 0) {
                labelObj.userIdList = [NSString stringWithFormat:@"%@", userIdListStr];
            }
            [labelObj insert];
        }
        
        // 删除服务器上已经删除的
        NSArray *arr = [[JY_LabelObject sharedInstance] fetchAllLabelsFromLocal];
        for (NSInteger i = 0; i < arr.count; i ++) {
            JY_LabelObject *locLabel = arr[i];
            BOOL flag = NO;
            for (NSInteger j = 0; j < array1.count; j ++) {
                NSDictionary * dict = array1[j];
                
                if ([locLabel.groupId isEqualToString:dict[@"groupId"]]) {
                    flag = YES;
                    break;
                }
            }
            
            if (!flag) {
                [locLabel delete];
            }
        }
        
        
        _array = [[JY_LabelObject sharedInstance] fetchAllLabelsFromLocal];
        for (JY_LabelObject *labelObj in _array) {
            NSString *userIdStr = labelObj.userIdList;
            NSArray *userIds = [userIdStr componentsSeparatedByString:@","];
            if (userIdStr.length <= 0) {
                userIds = nil;
            }
            
            NSMutableArray *newUserIds = [userIds mutableCopy];
            for (NSInteger i = 0; i < userIds.count; i ++) {
                NSString *userId = userIds[i];
                NSString *userName = [JY_UserObject getUserNameWithUserId:userId];
                
                if (!userName || userName.length <= 0) {
                    [newUserIds removeObject:userId];
                }
                
            }
            
            NSString *string = [newUserIds componentsJoinedByString:@","];
            
            labelObj.userIdList = string;
            
            [labelObj update];
        }
    }
    
    if ([aDownload.action isEqualToString:act_customerLinkList]){
        self.customerArr = array1;
        CGRect backFrame = backView.frame;
        backFrame.size.height = backFrame.size.height + 50* self.customerArr.count;
        backView.frame = backFrame ;
        if (self.customerArr.count == 0) {
            return;
        }
        
        int cY = _menuView.frame.origin.y + _menuView.frame.size.height;
        for (int i = 0; i < self.customerArr.count; i++) {
            UIView *customerView = [[UIView alloc] initWithFrame:CGRectMake(0, cY + 50 * i, self_width, 50)];
            //            customerView.backgroundColor = [UIColor redColor];
            
            UIView *lineView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ManMan_SCREEN_WIDTH, .5)];
            lineView2.backgroundColor = HEXCOLOR(0xdcdcdc);
            [customerView addSubview:lineView2];
            
            UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(15, lineView2.frame.origin.y+5, 40, 40)];
            NSString *fileURL = self.customerArr[i][@"path"];
            NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:fileURL]];
            UIImage *image = [UIImage imageWithData:data];
            if (image) {
                imageView2.image = image;
            }else {
                imageView2.image = [UIImage imageNamed:@"im_10000"];
            }
            
            [customerView addSubview:imageView2];
            UIButton *customerBtn = [[UIButton alloc] initWithFrame:CGRectMake(imageView2.frame.origin.x+imageView2.frame.size.width + 20, imageView2.frame.origin.y, self_width-imageView2.frame.size.width-15+20, 40)];
            //            [customerBtn setTitle:@"威尼斯客服" forState:UIControlStateNormal];
            if (self.customerArr[i][@"name"]) {
                [customerBtn setTitle:self.customerArr[i][@"name"] forState:UIControlStateNormal];
            }else {
                //                imageView2.image = [UIImage imageNamed:@"im_10000"];
                [customerBtn setTitle:@"--" forState:UIControlStateNormal];
            }
            //    [customerBtn ]
            customerBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            //    customerBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
            //    customerBtn.backgroundColor = UIColor.redColor;
            [customerBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [customerBtn addTarget:self action:@selector(customerClick:) forControlEvents:UIControlEventTouchUpInside];
            customerBtn.tag = i+1;
            [customerView addSubview:customerBtn];
            
            UILabel *hintLabel = [[UILabel alloc] initWithFrame:CGRectMake(self_width - 40, customerBtn.frame.origin.y + customerBtn.frame.size.height - 15, 40, 15)];
            if (self.customerArr[i][@"type"]) {
                hintLabel.text = self.customerArr[i][@"type"];
            }else{
                hintLabel.text = @"--";
            }
            hintLabel.font = [UIFont systemFontOfSize:13];
            hintLabel.backgroundColor = UIColor.clearColor;
            hintLabel.textColor = UIColor.blackColor;
            hintLabel.textAlignment = NSTextAlignmentCenter;
            [customerView addSubview:hintLabel];
            
            
            [backView addSubview:customerView];
            
        }
    }

}

- (void)setupCustomerLinkList {
        self.customerArr = g_App.customerLinkListArray;
        CGRect backFrame = backView.frame;
        backFrame.size.height = backFrame.size.height + 50* self.customerArr.count;
        backView.frame = backFrame ;
        if (self.customerArr.count == 0) {
            return;
        }
        
        int cY = _menuView.frame.origin.y + _menuView.frame.size.height;
        for (int i = 0; i < self.customerArr.count; i++) {
            UIView *customerView = [[UIView alloc] initWithFrame:CGRectMake(0, cY + 50 * i, self_width, 50)];
            //   customerView.backgroundColor = [UIColor redColor];
            
            UIView *lineView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ManMan_SCREEN_WIDTH, .5)];
            lineView2.backgroundColor = HEXCOLOR(0xdcdcdc);
            [customerView addSubview:lineView2];
            
            UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(15, lineView2.frame.origin.y+5, 40, 40)];
            NSString *fileURL = self.customerArr[i][@"path"];
            NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:fileURL]];
            UIImage *image = [UIImage imageWithData:data];
            if (image) {
                imageView2.image = image;
            }else {
                imageView2.image = [UIImage imageNamed:@"im_10000"];
            }
            
            [customerView addSubview:imageView2];
            UIButton *customerBtn = [[UIButton alloc] initWithFrame:CGRectMake(imageView2.frame.origin.x+imageView2.frame.size.width + 20, imageView2.frame.origin.y, self_width-imageView2.frame.size.width-15+20, 40)];
            //            [customerBtn setTitle:@"威尼斯客服" forState:UIControlStateNormal];
            if (self.customerArr[i][@"name"]) {
                [customerBtn setTitle:self.customerArr[i][@"name"] forState:UIControlStateNormal];
            }else {
                //                imageView2.image = [UIImage imageNamed:@"im_10000"];
                [customerBtn setTitle:@"--" forState:UIControlStateNormal];
            }
            //    [customerBtn ]
            customerBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            //    customerBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
            //    customerBtn.backgroundColor = UIColor.redColor;
            [customerBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [customerBtn addTarget:self action:@selector(customerClick:) forControlEvents:UIControlEventTouchUpInside];
            customerBtn.tag = i+1;
            [customerView addSubview:customerBtn];
            
            UILabel *hintLabel = [[UILabel alloc] initWithFrame:CGRectMake(self_width - 40, customerBtn.frame.origin.y + customerBtn.frame.size.height - 15, 40, 15)];
            if (self.customerArr[i][@"type"]) {
                hintLabel.text = self.customerArr[i][@"type"];
            }else{
                hintLabel.text = @"--";
            }
            hintLabel.font = [UIFont systemFontOfSize:13];
            hintLabel.backgroundColor = UIColor.clearColor;
            hintLabel.textColor = UIColor.blackColor;
            hintLabel.textAlignment = NSTextAlignmentCenter;
            [customerView addSubview:hintLabel];
            
            
            [backView addSubview:customerView];
            
        }
}



-(int) didServerResultFailed:(JY_Connection*)aDownload dict:(NSDictionary*)dict{
    [_wait hide];
    
    [self stopLoading];
    return hide_error;
}

-(int) didServerConnectError:(JY_Connection*)aDownload error:(NSError *)error{//error为空时，代表超时
    [_wait hide];
    [self stopLoading];
    return hide_error;
}

-(void) didServerConnectStart:(JY_Connection*)aDownload{
//    [_wait start];
}

-(void)refresh{
    [self stopLoading];
    _refreshCount++;
    [_array removeAllObjects];
//    [_array release];
    [self getArrayData];
    _friendArray = [g_server.myself fetchAllFriendsOrNotFromLocal];
    [_table reloadData];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}

//-(void)scrollToPageUp{
//    [self refresh];
//}

-(void)newFriend:(NSObject*)sender{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self refresh];
    });
}

-(void)onHeadImage:(id)dataObj{

    JY_UserObject *p = (JY_UserObject *)dataObj;
    if([p.userId isEqualToString:FRIEND_CENTER_USERID] || [p.userId isEqualToString:CALL_CENTER_USERID])
        return;
    
    _currentUser = p;
//    [g_server getUser:p.userId toView:self];
    
    JY_UserInfoVC* vc = [JY_UserInfoVC alloc];
    vc.userId       = p.userId;
    vc.user = p;
    vc.fromAddType = 6;
    vc = [vc init];
    [g_navigation pushViewController:vc animated:YES];

    p = nil;
}

-(void)onSendTimeout:(NSNotification *)notifacation//超时未收到回执
{
    //    NSLog(@"onSendTimeout");
    [_wait stop];
//    [g_App showAlert:Localized(@"JXAlert_SendFilad")];
//    [JY_MyTools showTipView:Localized(@"JXAlert_SendFilad")];
}

-(void)newReceipt:(NSNotification *)notifacation{//新回执
    //    NSLog(@"newReceipt");
    JY_MessageObject *msg     = (JY_MessageObject *)notifacation.object;
    if(msg == nil)
        return;
    if(![msg isAddFriendMsg])
        return;
    [_wait stop];
    if([msg.type intValue] == XMPP_TYPE_DELALL){
        if([msg.toUserId isEqualToString:_currentUser.userId] || [msg.fromUserId isEqualToString:_currentUser.userId]){
            [_array removeObject:_currentUser];
            _currentUser = nil;
            [self getArrayData];
            [_table reloadData];
//            [g_App showAlert:Localized(@"JXAlert_DeleteFirend")];
        }
    }
    
    if([msg.type intValue] == XMPP_TYPE_BLACK){//拉黑
        for (JY_UserObject *obj in _array) {
            if ([obj.userId isEqualToString:_currentUser.userId]) {
                [_array removeObject:obj];
                break;
            }
        }
        
        [self getArrayData];
        [self.tableView reloadData];
    }
    
    if([msg.type intValue] == XMPP_TYPE_NOBLACK){
//        _currentUser.status = [NSNumber numberWithInt:friend_status_friend];
//        int status = [_currentUser.status intValue];
//        [_currentUser update];
        
        if (!_currentUser) {
            return;
        }
        [[JY_XMPP sharedInstance].blackList removeObject:_currentUser.userId];
        [JY_MessageObject msgWithFriendStatus:_currentUser.userId status:friend_status_friend];
        for (JY_UserObject *obj in _array) {
            if ([obj.userId isEqualToString:_currentUser.userId]) {
                [_array removeObject:obj];
                break;
            }
        }
    
        [self getArrayData];
        [self.tableView reloadData];
//        [g_App showAlert:Localized(@"JXAlert_MoveBlackList")];
    }
    
    if([msg.type intValue] == XMPP_TYPE_PASS){//通过
        [self getFriend];
    }
}

- (void)friendRemarkNotif:(NSNotification *)notif {
    
    JY_UserObject *user = notif.object;
    
    NSString *remarkName = user.remarkName;
    NSString *nickname = user.userNickname;
    NSString *userId = user.userId;
    
    
    
    
    for (int i = 0; i < _friendArray.count; i ++) {
        JY_UserObject *user1 = _friendArray[i];
        NSLog(@"userId ==== %@",user1.userId);
        if ([user.userId isEqualToString:user1.userId]) {
            
            user1.userNickname = user.userNickname;
            user1.remarkName = user.remarkName ;

            [_table reloadData];
            break;
        }
    }
    
    for (int i = 0; i < _array.count; i ++) {
        JY_UserObject *user1 = _array[i];
        NSLog(@"userId ==== %@",user1.userId);
        if ([user.userId isEqualToString:user1.userId]) {
            
            user1.userNickname = user.userNickname;
            user1.remarkName = user.remarkName ;

            [_table reloadData];
            break;
        }
    }
    
 
    
    
   
}

- (UIButton *)createButtonWithFrame:(CGRect)frame title:(NSString *)title icon:(NSString *)iconName  action:(SEL)action {
    UIButton *button = [[UIButton alloc] init];
    button.frame = frame;
    [button addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *imgV = [[UIImageView alloc] init];
    imgV.frame = CGRectMake((button.frame.size.width-IMAGE_HEIGHT)/2, 20, IMAGE_HEIGHT, IMAGE_HEIGHT);
    imgV.image = [UIImage imageNamed:iconName];
    [button addSubview:imgV];
    
    CGSize size = [title boundingRectWithSize:CGSizeMake(MAXFLOAT, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:SYSFONT(14)} context:nil].size;
    UILabel *lab = [[UILabel alloc] init];
    lab.text = title;
    lab.font = SYSFONT(14);
    lab.textAlignment = NSTextAlignmentCenter;
    lab.textColor = HEXCOLOR(0x323232);
    if (size.width >= button.frame.size.width) {
        size.width = button.frame.size.width-20;
    }
    lab.frame = CGRectMake(0, CGRectGetMaxY(imgV.frame)+INSET_HEIGHT, size.width, size.height);
    CGPoint center = lab.center;
    center.x = imgV.center.x;
    lab.center = center;
    
    CGRect btnFrame = button.frame;
    btnFrame.size.height = CGRectGetMaxY(imgV.frame)+INSET_HEIGHT+size.height;
    button.frame = btnFrame;
    
    [button addSubview:lab];
    
    return button;
}

-(void)customerClick:(UIButton *)sender{
    //    UIButton *btn = (UIButton *)[self.view viewWithTag:sender.tag];
    
    if (self.customerArr[sender.tag - 1][@"link"]) {
        CYWebCustomerServiceVC *vc = [[CYWebCustomerServiceVC alloc] init];
        vc.titleName = self.customerArr[sender.tag - 1][@"name"];
        vc.link = self.customerArr[sender.tag - 1][@"link"];
        [self presentViewController:vc animated:YES completion:^{
            
        }];
    }else {
        NSLog(@"链接不存在！！！！！！！！");
    }
    
}

@end
