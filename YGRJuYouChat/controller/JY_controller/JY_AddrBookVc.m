//
//  JY_AddrBookVc.h.m
//
//  Created by flyeagleTang on 14-4-3.
//  Copyright (c) 2014年 Reese. All rights reserved.
//  [g_mainVC.friendVC showNewMsgCount:0];

#import "JY_AddrBookVc.h"
#import "XMG_AddrBookCell.h"
#import "JY_NewFriendViewController.h"
#import "JY_GroupViewController.h"
#import "JY_BlackFriendVC.h"


@interface JY_AddrBookVc ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,weak) UITableView *tableView;
@property (nonatomic,strong) NSMutableArray *dataArr;
@end

@implementation JY_AddrBookVc

 
- (void)viewDidLoad
{
    [super viewDidLoad]; 
    
    self.heightHeader = ManMan_SCREEN_TOP;
    self.heightFooter = 0;
    self.isGotoBack=NO;
    [self createHeadAndFoot];
    self.title = Localized(@"JX_MailList");
    self.tableBody.backgroundColor=HEXCOLOR(0xF2F2F2);
    
        _dataArr = [NSMutableArray array];
    NSArray *imagesArr = @[@{@"userId":@"icon_xindpy",@"userNickname":Localized(@"JXNewFriendVC_NewFirend")},
                           @{@"userId":@"icon_qunz",@"userNickname":Localized(@"JX_ManyPerChat")},
                           @{@"userId":@"icon_xindpy",@"userNickname":Localized(@"JX_BlackList")}];
    _dataArr = [JY_UserBaseObj mj_objectArrayWithKeyValuesArray:imagesArr];
    
    
    UITableView *tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, ManMan_SCREEN_TOP+0.5, ManMan_SCREEN_WIDTH, ManMan_SCREEN_HEIGHT-ManMan_SCREEN_TOP-ManMan_SCREEN_BOTTOM) style:UITableViewStylePlain];
    tableView.delegate=self;
    tableView.dataSource=self; 
    tableView.backgroundColor = HEXCOLOR(0xF2F2F2);
    tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    tableView.rowHeight=UITableViewAutomaticDimension;
    tableView.estimatedRowHeight=100;
    [self.view addSubview:tableView];
    _tableView=tableView;
}
 
#pragma mark   ---------tableView协议----------------
 
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _dataArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    XMG_AddrBookCell  *cell=[XMG_AddrBookCell cellWithTableView:tableView];
   
    cell.userBaseModel = _dataArr[indexPath.row];

    return cell;
}
 

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row==0) {
         
        
        JY_MsgAndUserObject* newobj = [[JY_MsgAndUserObject alloc]init];
        newobj.user = [[JY_UserObject sharedInstance] getUserById:FRIEND_CENTER_USERID];
        newobj.message = [[JY_MessageObject alloc] init];
        newobj.message.toUserId = FRIEND_CENTER_USERID;
        newobj.user.msgsNew = [NSNumber numberWithInt:0];
        [newobj.message updateNewMsgsTo0];
        
        NSArray *friends = [[JY_FriendObject sharedInstance] fetchAllFriendsFromLocal];
        for (NSInteger i = 0; i < friends.count; i ++) {
            JY_FriendObject *friend = friends[i];
            if ([friend.msgsNew integerValue] > 0) {
                [friend updateNewMsgUserId:friend.userId num:0];
                [friend updateNewFriendLastContent];
            }
        }
        
        [self showNewMsgCount:0];

        JY_NewFriendViewController* vc = [[JY_NewFriendViewController alloc]init];
        [g_navigation pushViewController:vc animated:YES];
        
    }else if (indexPath.row==1){
        
        
        JY_GroupViewController *vc = [[JY_GroupViewController alloc] init];
        [g_navigation pushViewController:vc animated:YES];
    }else if (indexPath.row==3){
        
        JY_BlackFriendVC *vc = [[JY_BlackFriendVC alloc] init];
        vc.isDevice = YES;
        vc.title = Localized(@"JX_MyDevices");
        [g_navigation pushViewController:vc animated:YES];
    }else if (indexPath.row==2){
        
        
        JY_BlackFriendVC *vc = [[JY_BlackFriendVC alloc] init];
        vc.title = Localized(@"JX_BlackList");
        [g_navigation pushViewController:vc animated:YES];
        
    }
    
}
 
- (void) showNewMsgCount:(NSInteger)friendNewMsgNum{
    
    
}
@end
