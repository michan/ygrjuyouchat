//
//  JY_FriendCirleVc.m
//  JTManyChildrenSongs
//
//  Created by os on 2020/12/3.
//  Copyright © 2020 Reese. All rights reserved.
//

#import "JY_FriendCirleVc.h"
#import "XMG_FriendCirleCell.h"
#import "JY_WeiboVC.h"
#import "JY_ScanQRViewController.h"
#import "JY_LabelVC.h"
#import "JY_SearchUserVC.h"
#import "JY_PayViewController.h"

#import "JY_PartViewController.h"
#import "JY_PublicNumberVC.h"
#import "JY_NearVC.h"
#import "JiaTui_WeiboVController.h"
#import "JY_CameraVC.h"
#import "JY_UserInfoVC.h"
#import "ImageResize.h"

#define TopImageInset ManMan_SCREEN_WIDTH/5*1.8

@interface JY_FriendCirleVc ()<UITableViewDelegate,UITableViewDataSource,ManMan_ActionSheetVCDelegate,UIImagePickerControllerDelegate,ManMan_CameraVCDelegate,UINavigationControllerDelegate>
@property (nonatomic,strong) NSMutableArray *dataArr;
@property (nonatomic,strong) UIImageView *topBackImageView;
@property (nonatomic, assign) BOOL isFirstGoin;
@property (nonatomic, strong) NSString *topImageUrl;
@end

@implementation JY_FriendCirleVc
 

- (void)viewDidLoad {
    [super viewDidLoad];
    
   self.heightHeader = 0;
   self.heightFooter = 0;
   self.isGotoBack=NO;
   [self createHeadAndFoot];
   self.title =@"友趣";// Localized(@"JX_MailList");
//    [self topView];
    _table.backgroundColor=HEXCOLOR(0xF2F2F2);
    _dataArr = [NSMutableArray array];
    
    NSArray *imagesArr = @[
        @{@"userId":@"icon_pengyq",@"userNickname":@"友趣"},
        @{@"userId":@"icon_saoys",@"userNickname":@"扫一扫"}
    ];
    
       _dataArr = [JY_UserBaseObj mj_objectArrayWithKeyValuesArray:imagesArr];
    
    
    
    self.tableHeader = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ManMan_SCREEN_WIDTH, ManMan_SCREEN_TOP)];
    self.tableHeader.backgroundColor = HEXCOLOR(0xF2F2F2);
    _table.frame =CGRectMake(0,ManMan_SCREEN_TOP,self_width,ManMan_SCREEN_HEIGHT-self.heightHeader-self.heightFooter);
    [self.view addSubview:self.tableHeader];
    JY_Label* p = [[JY_Label alloc]initWithFrame:CGRectMake(40, ManMan_SCREEN_TOP - 32, self_width-40*2, 20)];
    p.backgroundColor = [UIColor clearColor];
    p.textAlignment   = NSTextAlignmentCenter;
    p.textColor       = HEXCOLOR(0x303030);
    p.text = self.title;
    p.font = [UIFont systemFontOfSize:18.0];
    p.userInteractionEnabled = YES;
    [self.tableHeader addSubview:p];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [UIApplication sharedApplication].statusBarHidden = NO;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

- (void)topView {
    UIView* head = [[UIView alloc]initWithFrame:CGRectMake(0,-20, ManMan_SCREEN_WIDTH,190 + (THE_DEVICE_HAVE_HEAD ? 44 : 20))];
    head.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    JY_ImageView* iv = [[JY_ImageView alloc]initWithFrame:CGRectMake(0,0, ManMan_SCREEN_WIDTH,head.frame.size.height)];
    iv.delegate = self;
//    iv.didTouch = @selector(actionPhotos);
    iv.changeAlpha = NO;
    iv.backgroundColor = [UIColor lightGrayColor];
    iv.clipsToBounds = YES;
    iv.contentMode = UIViewContentModeScaleAspectFill;
    _topBackImageView = iv;
//    [self showTopImage];
    iv.image = [UIImage imageNamed:@"发现背景图"];
    [head addSubview:iv];

    _table.tableHeaderView = head;
//    [self getWeiboBackImage];
}
- (void)showTopImage {
    if (IsStringNull(_topImageUrl)) {
        [g_server getHeadImageLarge:g_myself.userId userName:g_myself.userNickname imageView:_topBackImageView];
    }else {
        [g_server getImage:_topImageUrl imageView:_topBackImageView];
    }
}

- (void)getWeiboBackImage {
    [g_server getUser:g_myself.userId toView:self];
}

-(void)actionPhotos{
    JY_ActionSheetVC *actionVC = [[JY_ActionSheetVC alloc] initWithImages:@[] names:@[Localized(@"JX_ChoosePhoto"),Localized(@"JX_TakePhoto")]];
    actionVC.delegate = self;
    actionVC.tag = 111;
    [self presentViewController:actionVC animated:NO completion:nil];
}


- (void)actionSheet:(JY_ActionSheetVC *)actionSheet didButtonWithIndex:(NSInteger)index {
    if (actionSheet.tag == 111){
        if (index == 0) {
            UIImagePickerController *ipc = [[UIImagePickerController alloc] init];
            ipc.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
            ipc.delegate = self;
            ipc.allowsEditing = YES;
            ipc.modalPresentationStyle = UIModalPresentationCurrentContext;
            if (IS_PAD) {
                UIPopoverController *pop =  [[UIPopoverController alloc] initWithContentViewController:ipc];
                [pop presentPopoverFromRect:CGRectMake((self.view.frame.size.width - 320) / 2, 0, 300, 300) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
            }else {
                [self presentViewController:ipc animated:YES completion:nil];
            }
        }else {
            JY_CameraVC *vc = [JY_CameraVC alloc];
            vc.cameraDelegate = self;
            vc.isPhoto = YES;
            vc = [vc init];
            [self presentViewController:vc animated:YES completion:nil];
        }
    }
}


- (void)cameraVC:(JY_CameraVC *)vc didFinishWithImage:(UIImage *)image {
    UIImage *camImage = [ImageResize image:image fillSize:CGSizeMake(640, 640)];
    NSString* filePath = [JY_FileInfo getUUIDFileName:@"jpg"];
    [g_server saveImageToFile:camImage file:filePath isOriginal:NO];
    [g_server uploadFile:filePath validTime:@"-1" messageId:nil toView:self];
    _topBackImageView.image = camImage;
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [ImageResize image:[info objectForKey:@"UIImagePickerControllerEditedImage"] fillSize:CGSizeMake(640, 640)];
    NSString* filePath = [JY_FileInfo getUUIDFileName:@"jpg"];
    [g_server saveImageToFile:image file:filePath isOriginal:NO];
    [g_server uploadFile:filePath validTime:@"-1" messageId:nil toView:self];
    _topBackImageView.image = image;
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(void)actionUser{
    JY_UserInfoVC* vc = [JY_UserInfoVC alloc];
    vc.userId       = g_myself.userId;
    vc.fromAddType = 6;
    vc = [vc init];
    [g_navigation pushViewController:vc animated:YES];
}

#pragma mark   ---------tableView协议----------------
 

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 56;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
     
    return _dataArr.count;
}
  
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    XMG_FriendCirleCell *cell = [XMG_FriendCirleCell cellWithTableView:tableView];

    cell.userBaseModel = _dataArr[indexPath.row];
     
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row==0) {
        JiaTui_WeiboVController *weiboVC = [JiaTui_WeiboVController alloc];
        weiboVC.user = g_server.myself;
         weiboVC = [weiboVC init];
        [g_navigation pushViewController:weiboVC animated:YES];
        
    }else if (indexPath.row==1){
        
        
        AVAuthorizationStatus authStatus =  [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
        if (authStatus == AVAuthorizationStatusRestricted || authStatus ==AVAuthorizationStatusDenied)
        {
            [g_server showMsg:Localized(@"JX_CanNotopenCenmar")];
            return;
        }
        
        JY_ScanQRViewController * scanVC = [[JY_ScanQRViewController alloc] init];
        [g_navigation pushViewController:scanVC animated:YES];
    }
}

- (void)didServerResultSucces:(JY_Connection*)aDownload dict:(NSDictionary*)dict array:(NSArray*)array1 {
    [_wait hide];
    if([aDownload.action isEqualToString:act_UploadFile]){
        JY_UserObject *user = [[JY_UserObject alloc] init];
        user.msgBackGroundUrl = [(NSDictionary *)[dict[@"images"] firstObject] objectForKey:@"oUrl"];
        [g_server updateUser:user toView:self];
    }
    if ([aDownload.action isEqualToString:act_UserUpdate]) {
        NSString *urlStr = [NSString stringWithFormat:@"%@",dict[@"msgBackGroundUrl"]];
        if (IsStringNull(urlStr)) {
            [g_server getHeadImageLarge:g_myself.userId userName:g_myself.userNickname imageView:_topBackImageView];
        }else {
            [g_server getImage:[urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] imageView:_topBackImageView];
        }
    }
    if( [aDownload.action isEqualToString:act_UserGet] ){
        JY_UserObject* p = [[JY_UserObject alloc]init];
        [p getDataFromDict:dict];
        if (!self.isFirstGoin) {
            self.isFirstGoin = YES;
            _topImageUrl = p.msgBackGroundUrl;
//            [self showTopImage];
            return;
        }
        JY_UserInfoVC* vc = [JY_UserInfoVC alloc];
        vc.user       = p;
        vc.fromAddType = 6;
        vc = [vc init];
        [g_navigation pushViewController:vc animated:YES];
    }
}

- (int)didServerResultFailed:(JY_Connection*)aDownload dict:(NSDictionary*)dict {
    [_wait hide];
    return hide_error;
}

- (int)didServerConnectError:(JY_Connection*)aDownload error:(NSError *)error {//error为空时，代表超时
    [_wait hide];
    return hide_error;
}

- (void)didServerConnectStart:(JY_Connection*)aDownload {
    [_wait start];
}
 
@end
