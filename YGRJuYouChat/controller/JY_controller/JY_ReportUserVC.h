//
//  JY_ReportUserVC.h
//  TFJunYouChat
//
//  Created by 1 on 17/6/26.
//  Copyright © 2020 zengwOS. All rights reserved.
//

#import "JY_TableViewController.h"

@protocol ManMan_ReportUserDelegate <NSObject>

-(void)report:(JY_UserObject *)reportUser reasonId:(NSNumber *)reasonId;

@end

@interface JY_ReportUserVC : JY_TableViewController

@property (nonatomic, strong) JY_UserObject * user;

@property (nonatomic, weak) id delegate;

@property (nonatomic, assign) BOOL isUrl;


@end
