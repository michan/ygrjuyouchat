//
//  ManMan_ShareSelectView.h
//  TFJunYouChat
//
//  Created by MacZ on 15/8/26.
//  Copyright (c) 2015年 Reese. All rights reserved.
//

#import "JY_admobViewController.h"
#import <UIKit/UIKit.h>

#import "JY_ShareModel.h"

@protocol ShareListDelegate <NSObject>

- (void)didShareBtnClick:(UIButton *)shareBtn;

@end

@interface JY_ShareListVC : UIViewController{
    UIView *_listView;
    
    JY_ShareListVC *_pSelf;
}

@property (nonatomic,weak) id<ShareListDelegate> shareListDelegate;

- (void)showShareView;

@end
