//
//  JY_CourseListVC.h
//  TFJunYouChat
//
//  Created by lifengye on 2020/10/20.
//  Copyright © 2020 zengwOS. All rights reserved.
//

#import "JY_TableViewController.h"

@interface JY_CourseListVC : JY_TableViewController

@property (nonatomic, assign) int selNum;

- (NSInteger)getSelNum:(NSInteger)num indexNum:(NSInteger)indexNum;

@end
