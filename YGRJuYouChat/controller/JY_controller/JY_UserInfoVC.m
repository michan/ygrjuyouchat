//
//  JY_UserInfoVC.m
//  TFJunYouChat
//
//  Created by flyeagleTang on 14-6-10.
//  Copyright (c) 2014年 Reese. All rights reserved.
//

#import "JY_UserInfoVC.h"
//#import "selectTreeVC.h"
#import "selectValueVC.h"
#import "selectProvinceVC.h"
#import "ImageResize.h"
#import "JY_ChatViewController.h"
#import "JY_LocationVC.h"
#import "JY_MapData.h"
#import "JY_InputValueVC.h"
#import "FMDatabase.h"
#import "JY_userWeiboVC.h"
#import "JY_ReportUserVC.h"
#import "JY_QRCodeViewController.h"
#import "JY_ImageScrollVC.h"
#import "DMScaleTransition.h"
#import "JY_SetLabelVC.h"
#import "JY_LabelObject.h"
#import "JY_SetNoteAndLabelVC.h"
#import "JY_CirclePermissionsVC.h"

#import "JiaTui_WeiboVController.h"
#import "JYFeedBackVC.h"
#import "QL_InfoSettingVC.h"
#define HEIGHT 56
#define LINE_INSET 8

//#define IMGSIZE 150

#define TopHeight 7
#define CellHeight 45

@interface JY_UserInfoVC ()<ManMan_ReportUserDelegate,UITextFieldDelegate,ManMan_SelectMenuViewDelegate,ManMan_ActionSheetVCDelegate>

@property (nonatomic, assign) CGFloat topHeight; // 记录一个生活圈的开始高度
@property (nonatomic, strong) UILabel *tintLab;
@property (nonatomic, strong) UIView *baseView;
@property (nonatomic, strong) UILabel *serverName;

@property (nonatomic, strong) UIButton *notFocusBtn;

@property (nonatomic ,strong) UIButton *moreBtn;

@end

@implementation JY_UserInfoVC
@synthesize user;

- (UIButton *)moreBtn{
    if (!_moreBtn) {
        
        _moreBtn = [[UIButton alloc] initWithFrame:CGRectMake(ManMan_SCREEN_WIDTH - 60, ManMan_SCREEN_TOP - 34, 60, 24)];
        [_moreBtn setImage:[UIImage imageNamed:@"chat_more_black"] forState:UIControlStateNormal];
        _moreBtn.titleLabel.font = [UIFont systemFontOfSize:16.0];
        [_moreBtn addTarget:self action:@selector(gotoMoreVC) forControlEvents:UIControlEventTouchUpInside];

    }
    return  _moreBtn;
}
- (id)init
{
    self = [super init];
    if (self) {
        _titleArr = [[NSMutableArray alloc]init];
        _friendStatus = [user.status intValue];
        _latitude  = [user.latitude doubleValue];
        _longitude = [user.longitude doubleValue];
        
        self.isGotoBack   = YES;
        self.title =@"";//基本信息 Localized(@"JX_BaseInfo");
        self.heightFooter = 0;
        self.heightHeader = ManMan_SCREEN_TOP;
        //self.view.frame = CGRectMake(0, 0, ManMan_SCREEN_WIDTH, ManMan_SCREEN_HEIGHT);
        [self createHeadAndFoot];
        self.tableBody.backgroundColor = HEXCOLOR(0xF2F2F2);
        self.tableBody.scrollEnabled = YES;
        
        if([self.userId isKindOfClass:[NSNumber class]])
            self.userId = [(NSNumber*)self.userId stringValue];
        
        [g_server getUser:self.userId toView:self];
        
        [g_notify addObserver:self selector:@selector(newReceipt:) name:kXMPPReceiptNotifaction object:nil];
        [g_notify addObserver:self selector:@selector(onSendTimeout:) name:kXMPPSendTimeOutNotifaction object:nil];
        [g_notify addObserver:self selector:@selector(friendPassNotif:) name:kFriendPassNotif object:nil];
        [g_notify addObserver:self selector:@selector(newRequest:) name:kXMPPNewRequestNotifaction object:nil];
    }
    return self;
}

- (void)setupServers {
    
    //如果是自己，则不现实按钮
    // 自己/公众号/厂家不删除
    //    if (![self.userId isEqualToString:MY_USER_ID] && ![self.userId isEqualToString:@"18938880001"]) {
    //        UIButton *btn = [UIFactory createButtonWithImage:THESIMPLESTYLE ? @"title_more_black" : @"title_more" highlight:nil target:self selector:@selector(onMore)];
    //        btn.frame = CGRectMake(ManMan_SCREEN_WIDTH-24-8, ManMan_SCREEN_TOP - 34, 24, 24);
    //        [self.tableHeader addSubview:btn];
    //    }
    
    
   
    _baseView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ManMan_SCREEN_WIDTH, 0)];
    _baseView.backgroundColor = [UIColor whiteColor];
    [self.tableBody addSubview:_baseView];
    
    // 更新头像缓存
    [g_server delHeadImage:self.userId];
    
    _head = [[JY_ImageView alloc]initWithFrame:CGRectMake(ManMan_SCREEN_WIDTH/2-70/2, 40, 70, 70)];
    _head.layer.cornerRadius = 10;
    _head.layer.masksToBounds = YES;
    _head.didTouch = @selector(onHeadImage);
    _head.delegate = self;
    _head.image = [UIImage imageNamed:@"avatar_normal"];
    [_baseView addSubview:_head];
    [g_server getHeadImageLarge:self.userId userName:self.user.userNickname imageView:_head];
    
    _serverName = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_head.frame)+15, ManMan_SCREEN_WIDTH, 16)];
    _serverName.text = self.user.userNickname;
    _serverName.font = SYSFONT(16);
    _serverName.textAlignment = NSTextAlignmentCenter;
    [_baseView addSubview:_serverName];
    
    _tintLab = [[UILabel alloc] initWithFrame:CGRectMake(30, CGRectGetMaxY(_serverName.frame)+20, ManMan_SCREEN_WIDTH-60, 40)];
    _tintLab.text = @"说点什么~~";
    _tintLab.font = SYSFONT(15);
    _tintLab.textColor = HEXCOLOR(0x999999);
    _tintLab.numberOfLines = 0;
    _tintLab.textAlignment = NSTextAlignmentCenter;
    [_baseView addSubview:_tintLab];
    
    
    _btn = [[UIButton alloc] initWithFrame:CGRectMake(ManMan_SCREEN_WIDTH/2-50, CGRectGetMaxY(_tintLab.frame)+40, 100, 16)];
    [_btn.titleLabel setFont:SYSFONT(16)];
    [_btn setTitleColor:THEMECOLOR forState:UIControlStateNormal];
    [_btn addTarget:self action:@selector(actionAddFriend:) forControlEvents:UIControlEventTouchUpInside];
    [_baseView addSubview:_btn];
    
    
    // 不再关注
    _notFocusBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, CGRectGetMinY(_btn.frame), 100, 16)];
    [_notFocusBtn.titleLabel setFont:SYSFONT(16)];
    [_notFocusBtn setTitleColor:THEMECOLOR forState:UIControlStateNormal];
    [_notFocusBtn addTarget:self action:@selector(onDeleteFriend) forControlEvents:UIControlEventTouchUpInside];
    _notFocusBtn.hidden = YES;
    [_baseView addSubview:_notFocusBtn];
    
    
    _baseView.frame = CGRectMake(_baseView.frame.origin.x, _baseView.frame.origin.y, _baseView.frame.size.width, CGRectGetMaxY(_btn.frame)+40);
    
}


- (void)setServerDetail {
    _serverName.text = self.user.userNickname;
    if (self.user.userDescription.length > 0) {
        _tintLab.text = self.user.userDescription;
    }
    
    CGSize size = [_tintLab.text boundingRectWithSize:CGSizeMake(ManMan_SCREEN_WIDTH-60, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:SYSFONT(15)} context:nil].size;
    
    _tintLab.frame = CGRectMake(_tintLab.frame.origin.x, _tintLab.frame.origin.y, _tintLab.frame.size.width, size.height);
    
    _btn.frame = CGRectMake(_btn.frame.origin.x, CGRectGetMaxY(_tintLab.frame)+40, _btn.frame.size.width, _btn.frame.size.height);
    
    _notFocusBtn.frame = CGRectMake(_notFocusBtn.frame.origin.x, CGRectGetMaxY(_tintLab.frame)+40, _notFocusBtn.frame.size.width, _notFocusBtn.frame.size.height);
    
    
    _baseView.frame = CGRectMake(_baseView.frame.origin.x, _baseView.frame.origin.y, _baseView.frame.size.width, CGRectGetMaxY(_btn.frame)+40);
    
    [self showAddFriend];
}


- (void)createViews {
    
    [self.tableBody.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    int h = 0;
    
    

    JY_ImageView* iv;
    
    
    
    // 更新头像缓存
    [g_server delHeadImage:self.userId];
    
    self.tableHeader.backgroundColor =[UIColor whiteColor];
    self.tableBody.backgroundColor = HEXCOLOR(0xF0F0F0);
    int Head_height = 132;
    UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ManMan_SCREEN_WIDTH, Head_height)];
    headView.backgroundColor = [UIColor whiteColor];

    [self.tableBody addSubview:headView];
    
    _head = [[JY_ImageView alloc]initWithFrame:CGRectMake(15, (Head_height-83)/2, 83, 83)];
    _head.layer.cornerRadius = 10;
    _head.layer.masksToBounds = YES;
    _head.didTouch = @selector(onHeadImage);
    _head.delegate = self;
    _head.image = [UIImage imageNamed:@"avatar_normal"];
    [headView addSubview:_head];
    [g_server getHeadImageLarge:self.userId userName:self.user.userNickname imageView:_head];
    
    // 名字
    _remarkName = [[UILabel alloc] init];
    _remarkName.font = [UIFont boldSystemFontOfSize:17];
    _remarkName.textColor = [UIColor blackColor];
    _remarkName.frame = CGRectMake(CGRectGetMaxX(_head.frame)+15, CGRectGetMinY(_head.frame), 120, 19);
    _remarkName.text = @"网络有问题";
    [headView addSubview:_remarkName];
    
    _sex = [[UIImageView alloc] init];
    _sex.frame = CGRectMake(CGRectGetMaxX(_remarkName.frame)+5, CGRectGetMinY(_remarkName.frame)+1.5, 14, 14);
    _sex.image = [UIImage imageNamed:@"basic_famale"];
    [headView addSubview:_sex];
    
    // 昵称
    _name = [[UILabel alloc] init];
    _name.font = SYSFONT(15);
    _name.textColor = [UIColor grayColor];
    _name.text = [NSString stringWithFormat:@"%@ : %@",Localized(@"JX_NickName"),@"--"];
    [headView addSubview:_name];
    
    //通讯号
    _account = [[UILabel alloc] init];
    _account.font = SYSFONT(15);
    _account.textColor = [UIColor grayColor];
    _account.text = [NSString stringWithFormat:@"%@ : %@",@"聚友号",@"--"];
    [headView addSubview:_account];
    
    // 地区
    //    _city = [[UILabel alloc] init];
    //    _city.font = SYSFONT(15);
    //    _city.textColor = [UIColor grayColor];
    //    _city.text = [NSString stringWithFormat:@"%@ : %@",@"地区",@"北京"];
    //    [headView addSubview:_city];
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, Head_height-LINE_WH, ManMan_SCREEN_WIDTH, LINE_WH)];
    line.backgroundColor = THE_LINE_COLOR;
    [headView addSubview:line];
    
    
    h = Head_height;
    if ([self.userId intValue] != [MY_USER_ID intValue]) {
        //标签
        iv = [self createButton:@"备注和标签" drawTop:NO drawBottom:NO must:NO click:@selector(onRemark) superView:self.tableBody];
        iv.frame = CGRectMake(0, h, ManMan_SCREEN_WIDTH, HEIGHT);
        _label = [self createLabel:iv default:user.userNickname];
        _label.frame = CGRectMake(_label.frame.origin.x, _label.frame.origin.y, _label.frame.size.width-7-9, _label.frame.size.height);
        
        h+=iv.frame.size.height;
        
        // 描述
        iv = [self createButton:Localized(@"JX_UserInfoDescribe") drawTop:YES drawBottom:NO must:NO click:nil superView:self.tableBody];
        iv.frame = CGRectMake(0, h, ManMan_SCREEN_WIDTH, HEIGHT);
        //如果是朋友且设置备注，这修改为备注，暂时不知道有没有此接口
        _describe = [self createLabel:iv default:user.describe];
        h+=iv.frame.size.height;
        _describeImgV = iv;
        
        
        // 设置生活圈权限
//        iv = [self createButton:@"设置社交圈权限" drawTop:YES drawBottom:NO must:NO click:@selector(onCirclePermissions) superView:self.tableBody];
//        iv.frame = CGRectMake(0, h, ManMan_SCREEN_WIDTH, HEIGHT);
//        //如果是朋友且设置备注，这修改为备注，暂时不知道有没有此接口
//        h+=iv.frame.size.height;
//        _circleImgV = iv;
//
//        h+=LINE_INSET;
    }
    
    
    //    if ([self.userId intValue]>10100 || [self.userId intValue]<10000) {
    //        iv = [self createButton:Localized(@"JX_MemoName") drawTop:NO drawBottom:YES must:NO click:nil superView:self.tableBody];
    //        iv.frame = CGRectMake(0, h, ManMan_SCREEN_WIDTH, HEIGHT);
    //        //如果是朋友且设置备注，这修改为备注，暂时不知道有没有此接口
    //        _remarkName = [self createLabel:iv default:user.remarkName];
    //        h+=iv.frame.size.height;
    //
    //        iv = [self createButton:Localized(@"JX_UserInfoDescribe") drawTop:NO drawBottom:YES must:NO click:nil superView:self.tableBody];
    //        iv.frame = CGRectMake(0, h, ManMan_SCREEN_WIDTH, HEIGHT);
    //        //如果是朋友且设置备注，这修改为备注，暂时不知道有没有此接口
    //        _describe = [self createLabel:iv default:user.describe];
    //        h+=iv.frame.size.height;
    //
    //    }
    
    
    self.topHeight = h;
    
    JY_UserObject *friend = [[JY_UserObject sharedInstance] getUserById:self.userId];
    NSString *status = [NSString stringWithFormat:@"%@",friend.status];
    if ((friend && [status intValue] != friend_status_none)  || [self.userId isEqualToString:g_myself.userId]) {
        
        // 生活圈
        iv = [self createButton:@"发圈" drawTop:NO drawBottom:NO must:NO click:@selector(onMyBlog) superView:self.tableBody];
        iv.frame = CGRectMake(0, h, ManMan_SCREEN_WIDTH, HEIGHT);
        h+=iv.frame.size.height;
        _lifeImgV = iv;
    }
    
 
    
    
    //    iv = [self createButton:Localized(@"JX_Sex") drawTop:NO drawBottom:YES must:NO click:nil superView:self.tableBody];
    //    iv.frame = CGRectMake(0, h, ManMan_SCREEN_WIDTH, HEIGHT);
    //    NSArray* a = [NSArray arrayWithObjects:Localized(@"JX_Wuman"),Localized(@"JX_Man"),nil];
    
    //    NSString * sexStr;
    //    if ([user.sex intValue] == 0 || [user.sex intValue] == 1) {
    //        sexStr = [a objectAtIndex:[user.sex intValue]];
    //    }else if ([user.sex intValue] >= 10000) {
    //        sexStr = @"--";
    //    }else {
    //        sexStr = @"";
    //    }
    //    _sex = [self createLabel:iv default:sexStr];
    //
    //    a = nil;
    //    h+=iv.frame.size.height;
    // 生日
    //    iv = [self createButton:Localized(@"JX_BirthDay") drawTop:NO drawBottom:NO must:NO click:nil superView:self.tableBody];
    //    iv.frame = CGRectMake(0, h, ManMan_SCREEN_WIDTH, HEIGHT);
    //    _date = [self createLabel:iv default:[TimeUtil formatDate:user.birthday format:@"yyyy-MM-dd"]];
    //    h+=iv.frame.size.height;
    //    _birthdayImgV = iv;
    
    //    if ([g_config.isOpenPositionService intValue] == 0) {
    //        iv = [self createButton:Localized(@"JX_Address") drawTop:NO drawBottom:YES must:NO click:nil superView:self.tableBody];
    //        iv.frame = CGRectMake(0, h, ManMan_SCREEN_WIDTH, HEIGHT);
    //        _city = [self createLabel:iv default:city];
    //        h+=iv.frame.size.height;
    //    }
    
    // 在线时间
    //    iv = [self createButton:Localized(@"JX_LastOnlineTime") drawTop:YES drawBottom:NO must:NO click:nil superView:self.tableBody];
    //    iv.frame = CGRectMake(0, h, ManMan_SCREEN_WIDTH, HEIGHT);
    //    _lastTImgV = iv;
    //    _lastTime = [self createLabel:iv default:[self dateTimeDifferenceWithStartTime:self.user.showLastLoginTime]];
    //    h+=iv.frame.size.height;
    
    //    // 显示手机号
    //    iv = [self createButton:Localized(@"JX_MobilePhoneNo.") drawTop:YES drawBottom:NO must:NO click:nil superView:self.tableBody];
    //    iv.frame = CGRectMake(0, h, ManMan_SCREEN_WIDTH, HEIGHT);
    //    _showNImgV = iv;
    //    _showNum = [self createLabel:iv default:self.user.telephone];
    //    h+=iv.frame.size.height;
    
    // 个性签名
    //    iv = [self createButton:Localized(@"JX_Personal_signature") drawTop:YES drawBottom:NO must:NO click:nil superView:self.tableBody];
    //    iv.frame = CGRectMake(0, h, ManMan_SCREEN_WIDTH, HEIGHT);
    //    _desImgV = iv;
    //    _desLab = [self createLabel:iv default:self.user.userDescription];
    //    _desLab.numberOfLines = 0;
    //    h+=iv.frame.size.height;
    
    h+=LINE_INSET;
    
    _baseView = [[UIView alloc] initWithFrame:CGRectMake(0, h, ManMan_SCREEN_WIDTH, 0)];
    [self.tableBody addSubview:_baseView];
    h = 0;
    
    //    if ([g_config.isOpenPositionService intValue] == 0) {
    //        if (!self.isJustShow) {
    //            iv = [self createButton:Localized(@"JXUserInfoVC_Loation") drawTop:NO drawBottom:YES must:NO click:@selector(actionMap) superView:_baseView];
    //            iv.frame = CGRectMake(0, h, ManMan_SCREEN_WIDTH, HEIGHT);
    //            h+=iv.frame.size.height;
    //        }
    //    }
    
    //    iv = [self createButton:Localized(@"JXQR_QRImage") drawTop:NO drawBottom:NO must:NO click:@selector(showUserQRCode) superView:_baseView];
    //    iv.frame = CGRectMake(0, h, ManMan_SCREEN_WIDTH, HEIGHT);
    //    UIImageView * qrView = [[UIImageView alloc] init];
    //    qrView.frame = CGRectMake(ManMan_SCREEN_WIDTH-15-7-9-16, (HEIGHT-16)/2, 16, 16);
    //    qrView.image = [UIImage imageNamed:@"qrcodeImage"];
    //    [iv addSubview:qrView];
    //    h+=iv.frame.size.height;
    
    
    h+=1;
    
    
    NSString *inviterUserName = @"自己入群";
    for (NSDictionary *dict in _dictData[@"members"]) {
        
        NSString *userId = [NSString stringWithFormat:@"%@",dict[@"userId"]];
        
        if ([userId isEqualToString:_userId]) {
            
            id obj = [dict objectForKey:@"inviterUserId"];
            if (obj == nil) {
                inviterUserName = @"自己入群";
            } else {
                if ([[NSString stringWithFormat:@"%@", obj] isEqualToString:_userId]) {
                    inviterUserName = @"自己入群";
                }else {
                    inviterUserName = [NSString stringWithFormat:@"%@ %@",dict[@"inviterUserName"],@"邀请入群"];
                }
            }
            
            
        }
    }
    
    //。
    if (_dictData!=nil &&_isAdmin) {
        iv = [self createButton:Localized(@"JX_comeGround") drawTop:NO drawBottom:NO must:NO click:nil superView:_baseView];
        iv.frame = CGRectMake(0, h, ManMan_SCREEN_WIDTH, HEIGHT);
        _Fangshi = [self createLabel:iv default:[NSString stringWithFormat:@"%@",inviterUserName]];
        h+=iv.frame.size.height;
        h+=10;
    }
    
    int n = _friendStatus;
    
    if (![self.userId isEqualToString:g_myself.userId]) {
        
        //标题数组
//        iv = [self createButton:@"投诉" drawTop:NO drawBottom:NO must:NO click:@selector(reportUserView) superView:_baseView];
//        iv.frame = CGRectMake(0, h-10, ManMan_SCREEN_WIDTH, HEIGHT);
//        h+=iv.frame.size.height;
        
//        iv = [self createButton:@"更多信息" drawTop:NO drawBottom:NO must:NO click:@selector(gotoMoreVC) superView:_baseView];
//        iv.frame = CGRectMake(0, h-10, ManMan_SCREEN_WIDTH, HEIGHT);
//        h+=iv.frame.size.height;
    }
    [self.tableHeader addSubview:self.moreBtn];
    
    
    
    //case 2:
    //    if(n == friend_status_black){
    //        [self onDelBlack];
    //        [self viewDisMissAction];
    //    }else{
    //        [self onAddBlack];
    //        [self viewDisMissAction];
    //    }
    //    break;
    if ([user.userType intValue] != 2) {
        
        if(n == friend_status_friend){
//            if(n == friend_status_black){
//                //标题数组
//                iv = [self createButton:@"取消黑名单" drawTop:YES drawBottom:NO must:NO click:@selector(onDelBlack) superView:_baseView];
//                iv.frame = CGRectMake(0, h, ManMan_SCREEN_WIDTH, HEIGHT);
//                h+=iv.frame.size.height;
//
//            }else if(![user.isBeenBlack boolValue]) {
//                //标题数组
//                iv = [self createButton:@"加入黑名单" drawTop:YES drawBottom:NO must:NO click:@selector(onAddBlack) superView:_baseView];
//                iv.frame = CGRectMake(0, h, ManMan_SCREEN_WIDTH, HEIGHT);
//                h+=iv.frame.size.height;
//            }
//            if(![user.isBeenBlack boolValue]){
//                if(n == friend_status_friend){
//                    //标题数组
//                    iv = [self createButton:@"删除好友" drawTop:YES drawBottom:NO must:NO click:@selector(onDeleteFriend) superView:_baseView];
//                    iv.frame = CGRectMake(0, h, ManMan_SCREEN_WIDTH, HEIGHT);
//                    h+=iv.frame.size.height;
//                }else{
//                }
//            }
        }else{
            
            //             [_titleArr addObject:Localized(@"JXUserInfoVC_AddBlackList")];
            //             [_titleArr addObject:Localized(@"JXUserInfoVC_DeleteFirend")];
        }
        
    }
    
    
    
    
#pragma mark 消息免打扰
    //        if (_friendStatus == friend_status_friend && ![user.isBeenBlack boolValue]) {
    
    //            iv = [self createButton:Localized(@"JX_MessageFree") drawTop:NO drawBottom:YES must:NO click:@selector(switchAction:)];
    //            iv.frame = CGRectMake(0, h, ManMan_SCREEN_WIDTH, HEIGHT);
    //            h+=iv.frame.size.height;
    //        }
    //@"18938880001"
    /* if ([g_myself.telephone rangeOfString:@"18938880001"].location != NSNotFound) {
     
     iv = [self createButton:Localized(@"JX_MobilePhoneNo.") drawTop:YES drawBottom:NO must:NO click:@selector(callNumber) superView:_baseView];
     iv.frame = CGRectMake(0, h, ManMan_SCREEN_WIDTH, HEIGHT);
     
     UILongPressGestureRecognizer *longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longTapAction:)];
     [iv addGestureRecognizer:longTap];
     
     _tel = [[UILabel alloc] initWithFrame:CGRectMake(0,INSETS,ManMan_SCREEN_WIDTH - INSETS - 20 - 5,HEIGHT-INSETS*2)];
     _tel.userInteractionEnabled = NO;
     _tel.text = s;
     _tel.font = g_factory.font16;
     _tel.textAlignment = NSTextAlignmentRight;
     _tel.textColor = HEXCOLOR(0x999999);
     //        [iv addSubview:_tel];
     
     NSString *subString = [user.telephone substringToIndex:2];
     if ([subString isEqualToString:@"86"]) {
     NSDate *date = [g_myself.phoneDic objectForKey:[user.telephone substringFromIndex:2]];
     if (date) {
     long long n = (long long)[date timeIntervalSince1970];
     NSString *time = [TimeUtil getTimeStrStyle1:n];
     NSString *str = [NSString stringWithFormat:@"%@,%@:%@",[user.telephone substringFromIndex:2],Localized(@"JX_HaveToDial"),time];
     _tel.text = str;
     }else {
     _tel.text = [user.telephone substringFromIndex:2];
     }
     
     }else {
     _tel.text = user.telephone;
     }
     h+=iv.frame.size.height;
     } */
    
    h+=30;
    
    if (!self.isJustShow) {
        
        if([self.userId intValue] != [MY_USER_ID intValue]){
//            _btn = [UIFactory createCommonButton:@"加为好友" target:self action:@selector(actionAddFriend:)];
            _btn = [[UIButton alloc] init];
            [_btn setTitle:@"加为好友" forState:UIControlStateNormal];
            _btn.backgroundColor = HEXCOLOR(0x05D168);
            [_btn.titleLabel setFont:SYSFONT(16)];
            [_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [_btn addTarget:self action:@selector(actionAddFriend:) forControlEvents:UIControlEventTouchUpInside];
            _btn.frame = CGRectMake(15, h, ManMan_SCREEN_WIDTH-30, 40);
            _btn.layer.masksToBounds = YES;
            _btn.layer.cornerRadius = 7.f;
            _btn.custom_acceptEventInterval = 1.0;
            [_baseView addSubview:_btn];
            [self showAddFriend];
            h+=_btn.frame.size.height;
            h+=INSETS;
        }
        
        //如果是自己，则不现实按钮
        // 自己/公众号/厂家不删除
        //        if (![self.userId isEqualToString:MY_USER_ID] && ![self.userId isEqualToString:CALL_CENTER_USERID] && ![self.userId isEqualToString:@"18938880001"]) {
        //            UIButton *btn = [UIFactory createButtonWithImage:THESIMPLESTYLE ? @"title_more_black" : @"title_more" highlight:nil target:self selector:@selector(onMore)];
        //            btn.frame = CGRectMake(ManMan_SCREEN_WIDTH-24-8, ManMan_SCREEN_TOP - 34, 24, 24);
        //            [self.tableHeader addSubview:btn];
        //        }
        
    }
    
    
    
    _currentData = [self.room getMember:self.userId];
    //禁言
    if (self.fromAddType == 3 && self.isAdmin) {
        _noTalkBtn = [[UIButton alloc]init];
        [_noTalkBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_noTalkBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_noTalkBtn.titleLabel setFont:g_factory.font15];
        _noTalkBtn.backgroundColor = [UIColor redColor];
        _noTalkBtn.frame = CGRectMake(15, h, ManMan_SCREEN_WIDTH-30, 40);
        _noTalkBtn.layer.masksToBounds = YES;
        _noTalkBtn.layer.cornerRadius = 7.f;
        [_noTalkBtn addTarget:self action:@selector(noTalkBtnClickEvent:) forControlEvents:UIControlEventTouchUpInside];
    
        _noTalkBtn.custom_acceptEventInterval = 1.0;
        [_noTalkBtn setTitle:@"禁言" forState:UIControlStateNormal];
        [_baseView addSubview:_noTalkBtn];
        _noTalkBtn.selected = _currentData.role;
        h+=_noTalkBtn.frame.size.height;
        h+=INSETS;
    }
    
    
    
    CGRect frame = _baseView.frame;
    frame.size.height = h;
    _baseView.frame = frame;
    
    if (self.tableBody.frame.size.height < CGRectGetMaxY(_baseView.frame)+30) {
        self.tableBody.contentSize = CGSizeMake(ManMan_SCREEN_WIDTH, CGRectGetMaxY(_baseView.frame)+30);
    }
    
    //    JY_UserObject *user = [[JY_UserObject sharedInstance] getUserById:_userId];
    //    [self setUserInfo:user];
}

- (void)onCirclePermissions {
    JY_CirclePermissionsVC *vc = [[JY_CirclePermissionsVC alloc] init];
    vc.user = self.user;
    [g_navigation pushViewController:vc animated:YES];
}

- (void)newRequest:(NSNotification *)notif {
    [g_server getUser:self.userId toView:self];
}

- (void) setUserInfo:(JY_UserObject *)user {
    if (self.user.content) {
        user.content = self.user.content;
    }
    self.user = user;
    
    // 更新用户信息
    [user updateUserNickname];
    
    _friendStatus = [user.status intValue];
    _latitude  = [user.latitude doubleValue];
    _longitude = [user.longitude doubleValue];
    
    // 设置用户名字、备注、通讯号、地区等...
    [self setLabelAndDescribe];
    
    if ([user.showLastLoginTime intValue] > 0 && [user.userType intValue] != 2) {
        _lastTime.text = [self dateTimeDifferenceWithStartTime:user.showLastLoginTime];
        _lastTImgV.hidden = NO;
    }else {
        _lastTImgV.hidden = YES;
        
    }
    if (user.telephone.length > 0 && [user.userType intValue] != 2) {
        _showNum.text = user.phone;
        _showNImgV.hidden = NO;
    }else {
        _showNImgV.hidden = YES;
    }
    
    if (user.userDescription.length > 0) {
        _desLab.text = user.userDescription;
        _desImgV.hidden = NO;
    }else {
        _desImgV.hidden = YES;
    }
    
    
    if (self.tableBody.frame.size.height < CGRectGetMaxY(_baseView.frame)+30) {
        self.tableBody.contentSize = CGSizeMake(ManMan_SCREEN_WIDTH, CGRectGetMaxY(_baseView.frame)+30);
    }
    
    _date.text = [TimeUtil formatDate:user.birthday format:@"yyyy-MM-dd"];
    
    
    if ([user.offlineNoPushMsg intValue] == 1) {
        [_messageFreeSwitch setOn:YES];
    }else {
        [_messageFreeSwitch setOn:NO];
    }
    
    if (_tel) {
        NSString *subString = [user.telephone substringToIndex:2];
        if ([subString isEqualToString:@"86"]) {
            NSDate *date = [g_myself.phoneDic objectForKey:[user.telephone substringFromIndex:2]];
            if (date) {
                long long n = (long long)[date timeIntervalSince1970];
                NSString *time = [TimeUtil getTimeStrStyle1:n];
                NSString *str = [NSString stringWithFormat:@"%@,%@:%@",[user.telephone substringFromIndex:2],Localized(@"JX_HaveToDial"),time];
                _tel.text = str;
            }else {
                _tel.text = [user.telephone substringFromIndex:2];
            }
            
        }else {
            _tel.text = user.telephone;
        }
    }
    
    [self showAddFriend];
}

- (void)setLabelAndDescribe {
    NSString* city = [g_constant getAddressForNumber:user.provinceId cityId:user.cityId areaId:user.areaId];
    
    _remarkName.text = user.remarkName.length > 0 ? user.remarkName : user.userNickname;
    CGSize sizeN = [_remarkName.text sizeWithAttributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:17]}];
    _remarkName.frame = CGRectMake(_remarkName.frame.origin.x, _remarkName.frame.origin.y, sizeN.width, _remarkName.frame.size.height);
    
    _sex.frame = CGRectMake(CGRectGetMaxX(_remarkName.frame)+5, _sex.frame.origin.y, 14, 14);
    if ([user.sex intValue] == 0) {// 女
        _sex.image = [UIImage imageNamed:@"basic_famale"];
    }else {// 男
        _sex.image = [UIImage imageNamed:@"basic_male"];
    }
    
    if (user.remarkName.length > 0) {
        _name.hidden = NO;
        _name.frame = CGRectMake(CGRectGetMinX(_remarkName.frame), CGRectGetMaxY(_remarkName.frame)+10, 200, 15);
        _account.frame = CGRectMake(CGRectGetMinX(_remarkName.frame), CGRectGetMaxY(_name.frame)+10, 200, 15);
        
        _name.text = [NSString stringWithFormat:@"%@ : %@",Localized(@"JX_NickName"),user.userNickname];
    }else {
        _name.hidden = YES;
        _account.frame = CGRectMake(CGRectGetMinX(_remarkName.frame), CGRectGetMaxY(_remarkName.frame)+10, 200, 15);
    }
    if (user.account.length > 0) {
        _account.hidden = NO;
        _city.frame = CGRectMake(CGRectGetMinX(_remarkName.frame), CGRectGetMaxY(_account.frame)+10, 0, 15);
        _account.text = [NSString stringWithFormat:@"%@ : %@",@"聚友号",user.account.length > 0 ? user.account : @"--"];
    }else {
        _account.hidden = YES;
        _city.frame = CGRectMake(CGRectGetMinX(_remarkName.frame), user.remarkName.length > 0 ? CGRectGetMaxY(_name.frame)+3 :CGRectGetMaxY(_remarkName.frame)+10, 200, 15);
    }
    
    _city.text = [NSString stringWithFormat:@"%@ : %@",@"地区",city.length > 0 ? @"北京" : @"--"];
    
    
    _describe.text = self.user.describe;
    
    // 标签
    NSMutableArray *array = [[JY_LabelObject sharedInstance] fetchLabelsWithUserId:self.user.userId];
    NSMutableString *labelsName = [NSMutableString string];
    for (NSInteger i = 0; i < array.count; i ++) {
        JY_LabelObject *labelObj = array[i];
        if (i == 0) {
            [labelsName appendString:labelObj.groupName];
        }else {
            [labelsName appendFormat:@",%@",labelObj.groupName];
        }
    }
    if (labelsName.length > 0 && self.user.describe.length <= 0) {
        _labelLab.text =@"备注和标签";// Localized(@"JX_Label");
        _label.text = labelsName;
        [self updateSubviewFrameIsHide:YES];
        _describeImgV.hidden = YES;
    }
    else if (labelsName.length > 0 && self.user.describe.length > 0) {
        _labelLab.text =@"备注和标签";// Localized(@"JX_Label");
        _label.text = labelsName;
        [self updateSubviewFrameIsHide:NO];
        _describeImgV.hidden = NO;
    }
    else if (self.user.describe.length > 0 && labelsName.length <= 0) {
        _labelLab.text = Localized(@"JX_UserInfoDescribe");
        _label.text = self.user.describe;
        [self updateSubviewFrameIsHide:YES];
        _describeImgV.hidden = YES;
    }
    else {
        _labelLab.text = Localized(@"JX_SetNotesAndLabels");
        _label.text = @"";
        [self updateSubviewFrameIsHide:YES];
        _describeImgV.hidden = YES;
    }
    
}


- (void)updateSubviewFrameIsHide:(BOOL)isHide {
    int y = 0;
    if ([self.userId intValue] == [MY_USER_ID intValue]) {
        y = self.topHeight;
    }else {
        if(isHide) {
            y = self.topHeight-HEIGHT;
        }else {
            y = self.topHeight;
        }
        
    }
    
    _circleImgV.frame = CGRectMake(0, y-HEIGHT-8, ManMan_SCREEN_WIDTH, HEIGHT);
    
    //    _lifeImgV.frame = CGRectMake(0, y, ManMan_SCREEN_WIDTH, HEIGHT);
    
    if ((_lifeImgV && [self.user.status intValue] != friend_status_none) || [self.userId isEqualToString:g_myself.userId]) {
        
        _lifeImgV.hidden = NO;
        _lifeImgV.frame = CGRectMake(0, y, ManMan_SCREEN_WIDTH, HEIGHT);
        y += HEIGHT;
        
    }else {
        _lifeImgV.hidden = YES;
    }
    
    _birthdayImgV.frame = CGRectMake(0, y, ManMan_SCREEN_WIDTH, HEIGHT);
    if ([user.showLastLoginTime intValue] > 0 && [user.userType intValue] != 2){
        y += HEIGHT;
        _lastTImgV.frame = CGRectMake(0, y, ManMan_SCREEN_WIDTH, HEIGHT);
    }
    if (user.telephone.length > 0 && [user.userType intValue] != 2 && [g_config.regeditPhoneOrName intValue] == 0) {
        y += HEIGHT;
        _showNImgV.frame = CGRectMake(0, y, ManMan_SCREEN_WIDTH, HEIGHT);
    }
    if (user.userDescription.length > 0) {
        y += HEIGHT;
        CGSize size = [user.userDescription boundingRectWithSize:CGSizeMake(ManMan_SCREEN_WIDTH/2-15, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:SYSFONT(15)} context:nil].size;
        int h = HEIGHT;
        if (size.height > 20) {
            // 大于一行，就改变高度
            h = (HEIGHT-15) + size.height;
            _desLab.frame = CGRectMake(_desLab.frame.origin.x, _desLab.frame.origin.y, _desLab.frame.size.width, size.height);
            UILabel *label = [_desImgV viewWithTag:100];
            label.frame = CGRectMake(label.frame.origin.x, 0, label.frame.size.width, h);
        }
        _desImgV.frame = CGRectMake(0, y, ManMan_SCREEN_WIDTH, h);
        y += h;
    }else {
        y += 0;
    }
    
    _baseView.frame = CGRectMake(0, y+INSETS, ManMan_SCREEN_WIDTH, _baseView.frame.size.height);
    
}


- (void)friendPassNotif:(NSNotification *)notif {
    JY_FriendObject *user = notif.object;
    if ([user.userId isEqualToString:self.userId]) {
        _friendStatus = friend_status_friend;
        [self showAddFriend];
    }
}

- (void)callNumber {
    NSMutableString* str;
    NSString *subString = [user.telephone substringToIndex:2];
    if ([subString isEqualToString:@"86"]) {
        str = [[NSMutableString alloc]initWithFormat:@"telprompt://%@",[user.telephone substringFromIndex:2]];
        [g_myself insertPhone:[user.telephone substringFromIndex:2] time:[NSDate date]];
        [g_myself.phoneDic setObject:[NSDate date] forKey:[user.telephone substringFromIndex:2]];
        
        NSDate *date = [g_myself.phoneDic objectForKey:[user.telephone substringFromIndex:2]];
        if (date) {
            long long n = (long long)[date timeIntervalSince1970];
            NSString *time = [TimeUtil getTimeStrStyle1:n];
            NSString *str = [NSString stringWithFormat:@"%@,%@:%@",[user.telephone substringFromIndex:2],Localized(@"JX_HaveToDial"),time];
            _tel.text = str;
        }else {
            _tel.text = [user.telephone substringFromIndex:2];
        }
        
    }else {
        str = [[NSMutableString alloc]initWithFormat:@"telprompt://%@",user.telephone];
        [g_myself insertPhone:user.telephone time:[NSDate date]];
        [g_myself.phoneDic setObject:[NSDate date] forKey:user.telephone];
        
        _tel.text = user.telephone;
    }
    
    [g_notify postNotificationName:kNearRefreshCallPhone object:nil];
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:str]];
}

- (void) longTapAction:(UILongPressGestureRecognizer *)longTap {
    if(longTap.state == UIGestureRecognizerStateBegan)
    {
        NSString *subString = [user.telephone substringToIndex:2];
        if ([subString isEqualToString:@"86"]) {
            [g_myself deletePhone:[user.telephone substringFromIndex:2]];
            [g_myself.phoneDic removeObjectForKey:[user.telephone substringFromIndex:2]];
            
            _tel.text = [user.telephone substringFromIndex:2];
            
        }else {
            [g_myself deletePhone:user.telephone];
            [g_myself.phoneDic removeObjectForKey:user.telephone];
            
            _tel.text = user.telephone;
        }
        
        [g_notify postNotificationName:kNearRefreshCallPhone object:nil];
    }
}

-(void)switchAction:(UISwitch *) sender{
    
    if (_friendStatus == friend_status_friend && ![user.isBeenBlack boolValue]) {
        
        int offlineNoPushMsg = sender.isOn;
        [g_server friendsUpdateOfflineNoPushMsgUserId:g_myself.userId toUserId:user.userId offlineNoPushMsg:offlineNoPushMsg type:0 toView:self];
    }else {
        [sender setOn:!sender.isOn];
        [g_server showMsg:Localized(@"JX_PleaseAddAsFriendFirst")];
    }
    
}

-(void)onHeadImage{
    [g_server delHeadImage:self.user.userId];
    
    JY_ImageScrollVC * imageVC = [[JY_ImageScrollVC alloc]init];
    
    imageVC.imageSize = CGSizeMake(ManMan_SCREEN_WIDTH, ManMan_SCREEN_WIDTH);
    
    imageVC.iv = [[JY_ImageView alloc] initWithFrame:CGRectMake(0, 0, ManMan_SCREEN_WIDTH, ManMan_SCREEN_WIDTH)];
    
    imageVC.iv.center = imageVC.view.center;
    
    [g_server getHeadImageLarge:self.user.userId userName:self.user.userNickname imageView:imageVC.iv];
    
    [self addTransition:imageVC];
    
    [self presentViewController:imageVC animated:YES completion:^{
        
    }];
    
}

//添加VC转场动画
- (void) addTransition:(JY_ImageScrollVC *) siv
{
    _scaleTransition = [[DMScaleTransition alloc]init];
    [siv setTransitioningDelegate:_scaleTransition];
    
}

-(void)dealloc{
    NSLog(@"JY_UserInfoVC.dealloc");
    [g_notify  removeObserver:self name:kXMPPSendTimeOutNotifaction object:nil];
    [g_notify  removeObserver:self name:kXMPPReceiptNotifaction object:nil];
    [g_notify removeObserver:self];
    self.user = nil;
    //    [super dealloc];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
   
}
-(void)doneBtnAction:(UIButton *)sender{
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) didServerResultSucces:(JY_Connection*)aDownload dict:(NSDictionary*)dict array:(NSArray*)array1{
    if([aDownload.action isEqualToString:act_AttentionAdd]){//加好友
        int n = [[dict objectForKey:@"type"] intValue];
        if( n==2 || n==4)
            _friendStatus = friend_status_friend;//成为好友，一般是无需验证
        //        else
        //            _friendStatus = friend_status_see;//单向关注
        
        if ([self.user.userType intValue] == 2) {
            user.status = [NSNumber numberWithInt:2];
            if([user haveTheUser])
                [user insert];
            else
                [user update];
            
            [self actionQuit];
            [g_notify postNotificationName:kActionRelayQuitVC object:nil];
            
            [self.user doSendMsg:XMPP_TYPE_FRIEND content:nil];
            [self showAddFriend];
            
            JY_ChatViewController *chatVC=[JY_ChatViewController alloc];
            chatVC.title = user.userNickname;
            chatVC.chatPerson = self.user;
            chatVC = [chatVC init];
            
            [g_navigation pushViewController:chatVC animated:YES];
            
        }else {
            if(_friendStatus == friend_status_friend){
                [_wait stop];
                [self doMakeFriend];
            }
            else
                [self doSayHello];
        }
    }
    if ([aDownload.action isEqualToString:act_FriendDel]) {//删除好友
        [self.user doSendMsg:XMPP_TYPE_DELALL content:nil];
    }
    if([aDownload.action isEqualToString:act_BlacklistAdd]){//拉黑
        [self.user doSendMsg:XMPP_TYPE_BLACK content:nil];
    }
    
    if([aDownload.action isEqualToString:act_FriendRemark]){
        ////备注好友名
        [_wait stop];
        JY_UserObject* user1 = [[JY_UserObject sharedInstance] getUserById:user.userId];
        user1.userNickname = user.userNickname;
        user1.remarkName = user.remarkName;
        user1.describe = user.describe;
        // 修改备注后实时刷新
        [user1 update];
        [g_notify postNotificationName:kFriendRemark object:user1];
       
        [g_server showMsg:Localized(@"JXAlert_SetOK")];
    }
    
    if([aDownload.action isEqualToString:act_BlacklistDel]){
        [self.user doSendMsg:XMPP_TYPE_NOBLACK content:nil];
    }
    
    if([aDownload.action isEqualToString:act_AttentionDel]){
        [user doSendMsg:XMPP_TYPE_DELSEE content:nil];
    }
    
    if([aDownload.action isEqualToString:act_Report]){
        [_wait stop];
        [g_server showMsg:Localized(@"JXUserInfoVC_ReportSuccess")];
    }
    
    if([aDownload.action isEqualToString:act_friendsUpdateOfflineNoPushMsg]){
        [_wait stop];
        [g_server showMsg:Localized(@"JXAlert_SetOK")];
    }
    if([aDownload.action isEqualToString:act_filterUserCircle]){
        [_wait stop];
    }
    if( [aDownload.action isEqualToString:act_UserGet] ){
        JY_UserObject* user = [[JY_UserObject alloc]init];
        [user getDataFromDict:dict];
        [user insertFriend];
        if ([user.userType intValue] != 2) {
            [self createViews];
        }
        
        if (self.user && ![self.user.userNickname isEqualToString:user.userNickname]) {
            [g_notify postNotificationName:kFriendListRefresh object:nil];
        }
        
        [self setUserInfo:user];
        
        _dictData = dict;
        
        if ([user.userType intValue] == 2) {
            [self setupServers];
            [self setServerDetail];
        }
        
        if (self.chatVC) {
            [self.chatVC.tableView reloadData];
        }
        
        
    }
    
    if( [aDownload.action isEqualToString:act_roomMemberSet] ){
        [_wait stop];
        [g_App showAlert:Localized(@"JXAlert_SetOK")];
    }
    
}

-(int) didServerResultFailed:(JY_Connection*)aDownload dict:(NSDictionary*)dict{
    [_wait stop];
    return show_error;
}

-(int) didServerConnectError:(JY_Connection*)aDownload error:(NSError *)error{//error为空时，代表超时
    [_wait stop];
    return show_error;
}

-(void) didServerConnectStart:(JY_Connection*)aDownload{
    if( [aDownload.action isEqualToString:act_UserGet] ){
        [_wait stop];
        return;
    }
    [_wait start];
}

-(JY_ImageView*)createButton:(NSString*)title drawTop:(BOOL)drawTop drawBottom:(BOOL)drawBottom must:(BOOL)must click:(SEL)click superView:(UIView *)superView{
    JY_ImageView* btn = [[JY_ImageView alloc] init];
    btn.backgroundColor = [UIColor whiteColor];
    btn.userInteractionEnabled = YES;
    btn.didTouch = click;
    btn.delegate = self;
    [superView addSubview:btn];
    
    if(must){
        UILabel* p = [[UILabel alloc] initWithFrame:CGRectMake(INSETS, 5, 20, HEIGHT-5)];
        p.text = @"*";
        p.font = g_factory.font18;
        p.backgroundColor = [UIColor clearColor];
        p.textColor = [UIColor redColor];
        p.textAlignment = NSTextAlignmentCenter;
        [btn addSubview:p];
    }
    
    JY_Label* p = [[JY_Label alloc] initWithFrame:CGRectMake(15, 0, 200, HEIGHT)];
    p.text = title;
    p.font = g_factory.font16;
    p.backgroundColor = [UIColor clearColor];
    p.tag = 100;
    p.textColor = [UIColor blackColor];
    [btn addSubview:p];
    if (@selector(onRemark) == click) {
        _labelLab = p;
    }
    if(drawTop){
        UIView* line = [[UIView alloc] initWithFrame:CGRectMake(15,0,ManMan_SCREEN_WIDTH-15,LINE_WH)];
        line.backgroundColor = THE_LINE_COLOR;
        [btn addSubview:line];
    }
    
    if(drawBottom){
        UIView* line = [[UIView alloc]initWithFrame:CGRectMake(15,HEIGHT-LINE_WH,ManMan_SCREEN_WIDTH-15,LINE_WH)];
        line.backgroundColor = THE_LINE_COLOR;
        [btn addSubview:line];
    }
    
    //这个选择器仅用于判断，之后会修改为不可点击
    SEL check = @selector(switchAction:);
    //创建switch
    if(click == check){
        UISwitch * switchView = [[UISwitch alloc]initWithFrame:CGRectMake(ManMan_SCREEN_WIDTH-INSETS-51, 6, 20, 20)];
        if ([title isEqualToString:Localized(@"JX_MessageFree")]) {
            _messageFreeSwitch = switchView;
            if ([user.offlineNoPushMsg intValue] == 1) {
                [_messageFreeSwitch setOn:YES];
            }else {
                [_messageFreeSwitch setOn:NO];
            }
        }
        
        switchView.onTintColor = THEMECOLOR;
        
        [switchView addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventTouchUpInside];
        
        [btn addSubview:switchView];
        //取消调用switchAction
        btn.didTouch = @selector(hideKeyboard);
        
    }else if(click){
        btn.frame = CGRectMake(btn.frame.origin.x -20, btn.frame.origin.y, btn.frame.size.width, btn.frame.size.height);
        
        UIImageView* iv;
        iv = [[UIImageView alloc] initWithFrame:CGRectMake(ManMan_SCREEN_WIDTH-15-7, (HEIGHT-13)/2, 7, 13)];
        iv.image = [UIImage imageNamed:@"new_icon_>"];
        [btn addSubview:iv];
    }
    return btn;
}

-(UITextField*)createTextField:(UIView*)parent default:(NSString*)s hint:(NSString*)hint{
    UITextField* p = [[UITextField alloc] initWithFrame:CGRectMake(ManMan_SCREEN_WIDTH/2,INSETS,ManMan_SCREEN_WIDTH/2,HEIGHT-INSETS*2)];
    p.delegate = self;
    p.autocorrectionType = UITextAutocorrectionTypeNo;
    p.autocapitalizationType = UITextAutocapitalizationTypeNone;
    p.enablesReturnKeyAutomatically = YES;
    p.borderStyle = UITextBorderStyleNone;
    p.returnKeyType = UIReturnKeyDone;
    p.clearButtonMode = UITextFieldViewModeAlways;
    p.textAlignment = NSTextAlignmentRight;
    p.userInteractionEnabled = YES;
    p.text = s;
    p.placeholder = hint;
    p.font = g_factory.font14;
    [parent addSubview:p];
    //    [p release];
    return p;
}

-(UILabel*)createLabel:(UIView*)parent default:(NSString*)s{
    UILabel* p = [[UILabel alloc] initWithFrame:CGRectMake(ManMan_SCREEN_WIDTH/2,(HEIGHT-15)/2,ManMan_SCREEN_WIDTH/2 - 15,15)];
    p.userInteractionEnabled = NO;
    p.text = s;
    p.font = g_factory.font15;
    p.textAlignment = NSTextAlignmentRight;
    p.textColor = HEXCOLOR(0x999999);
    [parent addSubview:p];
    
    return p;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}

-(void)actionAddFriend:(UIView*)sender{
    
    // 验证XMPP是否在线
    if(g_xmpp.isLogined != 1){
        [g_xmpp showXmppOfflineAlert];
        return;
    }
    
    if([user.isBeenBlack boolValue]){
        [g_server showMsg:Localized(@"TO_BLACKLIST")];
        return;
    }
    switch (_friendStatus) {
        case friend_status_black:{
            //            [g_server addAttention:user.userId  toView:self];
            [self onDelBlack];
            //            [self viewDisMissAction];
            
        }
            break;
        case friend_status_none:
        case friend_status_see:
            [g_server addAttention:user.userId fromAddType:self.fromAddType toView:self];
            break;
            //        case friend_status_see:
            //            [self doSayHello];
            //            break;
        case friend_status_friend:{//发消息
            if([user haveTheUser])
                [user insert];
            else
                [user update];
            
            [self actionQuit];
            [g_notify postNotificationName:kActionRelayQuitVC object:nil];
            
            JY_ChatViewController *chatVC=[JY_ChatViewController alloc];
            chatVC.title = user.userNickname;
            chatVC.chatPerson = self.user;
            chatVC = [chatVC init];
            [g_navigation pushViewController:chatVC animated:YES];
        }
            break;
    }
}
-(void)sendYuYing{
    if([user haveTheUser])
        [user insert];
    else
        [user update];
    
    [self actionQuit];
    [g_notify postNotificationName:kActionRelayQuitVC object:nil];
    
    JY_ChatViewController *chatVC=[JY_ChatViewController alloc];
    chatVC.title = user.userNickname;
    chatVC.isAudioOrVideo = YES;
    chatVC.chatPerson = self.user;
    chatVC = [chatVC init];
    [g_navigation pushViewController:chatVC animated:YES];
}

//禁言
- (void)noTalkBtnClickEvent:(UIButton *)sender{
    
    JY_ActionSheetVC *actionVC = [[JY_ActionSheetVC alloc] initWithImages:@[] names:@[Localized(@"JXAlert_NotGag"),Localized(@"JXAlert_GagTenMinute"),Localized(@"JXAlert_GagOneHour"),Localized(@"JXAlert_GagOne"),Localized(@"JXAlert_GagThere"),Localized(@"JXAlert_GagOneWeek"),Localized(@"JXAlert_GagFifteen")]];
    actionVC.delegate = self;
    [self presentViewController:actionVC animated:NO completion:nil];
    
}


- (void)actionSheet:(JY_ActionSheetVC *)actionSheet didButtonWithIndex:(NSInteger)index {
    NSTimeInterval n = [[NSDate date] timeIntervalSince1970];
    memberData* member = [self.room getMember:self.userId];
    switch (index) {
        case 0:
            member.talkTime = 0;
            break;
        case 1:
            member.talkTime = 10*60+n;
            break;
        case 2:
            member.talkTime = 1*3600+n;
            break;
        case 3:
            member.talkTime = 24*3600+n;
            break;
        case 4:
            member.talkTime = 3*24*3600+n;
            break;
        case 5:
            member.talkTime = 7*24*3600+n;
            break;
        case 6:
            member.talkTime = 15*24*3600+n;
            break;
        default:
            break;
    }
    
    [g_server setDisableSay:self.room.roomId member:member toView:self];
    member = nil;
}

-(void)doSayHello{//打招呼
    _xmppMsgId = [self.user doSendMsg:XMPP_TYPE_SAYHELLO content:Localized(@"JXUserInfoVC_Hello")];
}

-(void)onSendTimeout:(NSNotification *)notifacation//超时未收到回执
{
    //    NSLog(@"onSendTimeout");
    [_wait stop];
    //    [g_App showAlert:Localized(@"JXAlert_SendFilad")];
    [JY_MyTools showTipView:Localized(@"JXAlert_SendFilad")];
}

-(void)newReceipt:(NSNotification *)notifacation{//新回执
    //    NSLog(@"newReceipt");
    JY_MessageObject *msg     = (JY_MessageObject *)notifacation.object;
    if(msg == nil)
        return;
    if(![msg isAddFriendMsg])
        return;
    if(![msg.toUserId isEqualToString:self.user.userId])
        return;
    [_wait stop];
    if([msg.type intValue] == XMPP_TYPE_SAYHELLO){//打招呼
        [g_server showMsg:Localized(@"JXAlert_SayHiOK")];
    }
    
    if([msg.type intValue] == XMPP_TYPE_BLACK){//拉黑
        user.status = [NSNumber numberWithInt:friend_status_black];
        _friendStatus = [user.status intValue];
        [[JY_XMPP sharedInstance].blackList addObject:user.userId];
        [user update];
        [self showAddFriend];
        [g_server showMsg:Localized(@"JXAlert_AddBlackList")];
        
        [g_notify postNotificationName:kXMPPNewFriendNotifaction object:nil];
        //        [JY_MessageObject msgWithFriendStatus:user.userId status:_friendStatus];
        //        [user notifyDelFriend];
    }
    
    if([msg.type intValue] == XMPP_TYPE_DELSEE){//删除关注，弃用
        _friendStatus = friend_status_none;
        [self showAddFriend];
        [JY_MessageObject msgWithFriendStatus:user.userId status:_friendStatus];
        [g_server showMsg:Localized(@"JXAlert_CencalFollow")];
    }
    
    if([msg.type intValue] == XMPP_TYPE_DELALL){//删除好友
        _friendStatus = friend_status_none;
        [self showAddFriend];
        
        [g_notify postNotificationName:kXMPPNewFriendNotifaction object:nil];
        if ([self.user.userType intValue] == 2) {
            [g_server showMsg:@"已取消关注"];
            user.status = [NSNumber numberWithInt:1];
            [user update];
            
        }else {
            [g_server showMsg:Localized(@"JXAlert_DeleteFirend")];
        }
    }
    
    if([msg.type intValue] == XMPP_TYPE_NOBLACK){//取消拉黑
        user.status = [NSNumber numberWithInt:friend_status_friend];
        [user updateStatus];
        
        _friendStatus = friend_status_friend;
        [self showAddFriend];
        if ([[JY_XMPP sharedInstance].blackList containsObject:user.userId]) {
            [[JY_XMPP sharedInstance].blackList removeObject:user.userId];
            [JY_MessageObject msgWithFriendStatus:user.userId status:friend_status_friend];
        }
        [g_server showMsg:Localized(@"JXAlert_MoveBlackList")];
        
        [g_notify postNotificationName:kXMPPNewFriendNotifaction object:nil];
    }
    if([msg.type intValue] == XMPP_TYPE_FRIEND){//无验证加好友
        if ([g_myself.telephone rangeOfString:@"18938880001"].location == NSNotFound) {
            [g_server showMsg:Localized(@"JX_AddSuccess")];
        }
        user.status = [NSNumber numberWithInt:2];
        [g_notify postNotificationName:kXMPPNewFriendNotifaction object:nil];
    }
}

-(void)showAddFriend{
    CGFloat inset = (ManMan_SCREEN_WIDTH-200)/3;
    _notFocusBtn.hidden = YES;
    switch (_friendStatus) {
        case friend_status_hisBlack:
            break;
        case friend_status_black://黑名单则不显示
            [_btn setTitle:@"取消黑名单" forState:UIControlStateNormal];
            break;
        case friend_status_none:
        case friend_status_see:
            if([user.isBeenBlack boolValue])
                [_btn setTitle:Localized(@"TO_BLACKLIST") forState:UIControlStateNormal];
            else {
                if ([self.user.userType intValue] == 2) {
                    _btn.hidden = NO;
                    [_btn setTitle:@"关注公众号" forState:UIControlStateNormal];
                    _btn.frame = CGRectMake(ManMan_SCREEN_WIDTH/2-50, _btn.frame.origin.y, _btn.frame.size.width, _btn.frame.size.height);
                }else {
                    [_btn setTitle:@"加为好友" forState:UIControlStateNormal];
                }
            }
            break;
        case friend_status_friend:
            if([user.isBeenBlack boolValue])
                [_btn setTitle:Localized(@"TO_BLACKLIST") forState:UIControlStateNormal];
            else {
                if ([self.user.userType intValue] == 2 ) {
                    
                    _btn.hidden = !self.isShowGoinBtn;
                    [_btn setTitle:@"进入公众号" forState:UIControlStateNormal];
                    _btn.frame = CGRectMake(inset, _btn.frame.origin.y, _btn.frame.size.width, _btn.frame.size.height);
                    if (_btn.hidden) {
                        _notFocusBtn.frame = CGRectMake(ManMan_SCREEN_WIDTH/2-_notFocusBtn.frame.size.width/2, _notFocusBtn.frame.origin.y, _notFocusBtn.frame.size.width, _notFocusBtn.frame.size.height);
                    }else {
                        _notFocusBtn.frame = CGRectMake(ManMan_SCREEN_WIDTH-inset-100, _notFocusBtn.frame.origin.y, _notFocusBtn.frame.size.width, _notFocusBtn.frame.size.height);
                    }
                    [_notFocusBtn setTitle:@"不再关注" forState:UIControlStateNormal];
                    _notFocusBtn.hidden = NO;
                }else {
                    [_btn setTitle:@"发消息" forState:UIControlStateNormal];
                    
                    _btn.hidden = YES;
                    UIButton *sendMsgBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, CGRectGetMinY(_btn.frame)-30, SCREEN_WIDTH, 50)];
                    [sendMsgBtn setTitle:@"发消息" forState:UIControlStateNormal];
                    sendMsgBtn.backgroundColor = [UIColor whiteColor];
                    sendMsgBtn.titleLabel.font = [UIFont systemFontOfSize:15];
                    [sendMsgBtn setTitleColor:HEXCOLOR(0x426085) forState:UIControlStateNormal];
                    [_btn.superview addSubview:sendMsgBtn];
                    [sendMsgBtn addTarget:self action:@selector(actionAddFriend:) forControlEvents:UIControlEventTouchUpInside];
                    [sendMsgBtn setImage:[UIImage imageNamed:@"L_icon_xiaoxi"] forState:UIControlStateNormal];
                    
                    UIButton *sendYUy = [[UIButton alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(sendMsgBtn.frame) +1, SCREEN_WIDTH, 50)];
                    [sendYUy setTitle:@"语音视频通话" forState:UIControlStateNormal];
                    sendYUy.backgroundColor = [UIColor whiteColor];
                    sendYUy.titleLabel.font = [UIFont systemFontOfSize:15];
                    [sendYUy setTitleColor:HEXCOLOR(0x426085) forState:UIControlStateNormal];
                    [_btn.superview addSubview:sendYUy];
                    [sendYUy addTarget:self action:@selector(sendYuYing) forControlEvents:UIControlEventTouchUpInside];
                    [sendYUy setImage:[UIImage imageNamed:@"L_icon_shipin"] forState:UIControlStateNormal];

                    _baseView.frame = CGRectMake(_baseView.frame.origin.x, _baseView.frame.origin.y, _baseView.frame.size.width, CGRectGetMaxY(sendYUy.frame)+40);
                }
            }
            break;
    }
}

-(void)onMyBlog{
    
    JiaTui_WeiboVController* vc = [JiaTui_WeiboVController alloc];
    
    JY_UserObject *userObj = [[JY_UserObject sharedInstance] getUserById:self.userId];
    vc.user = userObj;
    vc.isGotoBack = YES;
    vc = [vc init];
    //    [g_window addSubview:vc.view];
    [g_navigation pushViewController:vc animated:YES];
    
}

-(void)actionMap{
    
    
    if (_longitude <=0 && _latitude <= 0) {
        [g_server showMsg:Localized(@"JX_NotShareLocation")];
        return;
    }
    
    JY_MapData * mapData = [[JY_MapData alloc] init];
    mapData.latitude = [NSString stringWithFormat:@"%f",_latitude];
    mapData.longitude = [NSString stringWithFormat:@"%f",_longitude];
    NSArray * locations = @[mapData];
    if (g_config.isChina) {
        JY_LocationVC * vc = [JY_LocationVC alloc];
        vc.locations = [NSMutableArray arrayWithArray:locations];
        vc.locationType = ManMan_LocationTypeShowStaticLocation;
        vc = [vc init];
        [g_navigation pushViewController:vc animated:YES];
    }else {
        _gooMap = [JY_GoogleMapVC alloc];
        _gooMap.locations = [NSMutableArray arrayWithArray:locations];
        _gooMap.locationType = ManMan_GooLocationTypeShowStaticLocation;
        _gooMap = [_gooMap init];
        [g_navigation pushViewController:_gooMap animated:YES];
    }
    
    //    JY_LocationVC* vc = [JY_LocationVC alloc];
    //    vc.isSend = NO;
    //    vc.locationType = ManMan_LocationTypeShowStaticLocation;
    //    NSMutableArray * locationsArray = [[NSMutableArray alloc]init];
    //
    //    JY_MapData* p = [[JY_MapData alloc]init];
    //    p.latitude = [NSString stringWithFormat:@"%f",_latitude];
    //    p.longitude = [NSString stringWithFormat:@"%f",_longitude];
    //    p.title = _name.text;
    //    p.subtitle = _city.text;
    //    [locationsArray addObject:p];
    //    vc.locations = locationsArray;
    //
    //    vc = [vc init];
    ////    [g_window addSubview:vc.view];
    //    [g_navigation pushViewController:vc animated:YES];
    
}

-(void)gotoMoreVC{
    QL_InfoSettingVC *vc = QL_InfoSettingVC.new;
//    vc.user = self.user;
//    vc.userId = self.userId;
    vc.supVC = self;
    vc.friendStatus = _friendStatus;
    vc.notSeeHim = user.notSeeHim;
    vc.notLetSeeHim = user.notLetSeeHim;
    vc.isBeenBlack = user.isBeenBlack;
    [g_navigation pushViewController:vc animated:YES];
}
-(void)reportUserView{
    
    
    
    //投诉
    JYFeedBackVC *vc  = [JYFeedBackVC new];
    vc.userId = user.userId;
    vc.type = FeedBackTypeComplaints;
    [g_navigation pushViewController:vc animated:YES];
    return;
    
    JY_ReportUserVC * reportVC = [[JY_ReportUserVC alloc] init];
    reportVC.user = user;
    reportVC.delegate = self;
    //    [g_window addSubview:reportVC.view];
    [g_navigation pushViewController:reportVC animated:YES];
    
}

- (void)report:(JY_UserObject *)reportUser reasonId:(NSNumber *)reasonId {
    [g_server reportUser:reportUser.userId roomId:nil webUrl:nil reasonId:reasonId toView:self];
}

- (void)onMore{
    int n = _friendStatus;
    //标题数组
    [_titleArr removeAllObjects];
    [_titleArr addObject:Localized(@"JXUserInfoVC_Report")];
    
    if ([user.userType intValue] != 2) {
        [_titleArr addObject:Localized(@"JX_SetNotesAndDescriptions")];
        
        if(n == friend_status_friend){
            if(n == friend_status_black)
                [_titleArr addObject:@"取消黑名单"];
            else if(![user.isBeenBlack boolValue]) {
                [_titleArr addObject:Localized(@"JXUserInfoVC_AddBlackList")];
            }
            if(![user.isBeenBlack boolValue]){
                if(n == friend_status_friend)
                    [_titleArr addObject:Localized(@"JXUserInfoVC_DeleteFirend")];
                else
                    [_titleArr addObject:Localized(@"JX_AddFriend")];
            }
        }else{
            
            [_titleArr addObject:Localized(@"JXUserInfoVC_AddBlackList")];
            [_titleArr addObject:Localized(@"JXUserInfoVC_DeleteFirend")];
            
        }
        
    }
    
    
    //    //模糊背景
    //    _bgBlackAlpha = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ManMan_SCREEN_WIDTH, ManMan_SCREEN_HEIGHT)];
    //    _bgBlackAlpha.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.1];
    //    [self.view addSubview:_bgBlackAlpha];
    ////    [_bgBlackAlpha release];
    
    //自定义View
    _selectView = [[JY_SelectMenuView alloc] initWithTitle:_titleArr image:@[] cellHeight:CellHeight];
    _selectView.alpha = 0.0;
    _selectView.delegate = self;
    [self.view addSubview:_selectView];
    //    [_selectView release];
    //动画
    [UIView animateWithDuration:0.3 animations:^{
        _selectView.alpha = 1;
    }];
}

- (void)didMenuView:(JY_SelectMenuView *)MenuView WithIndex:(NSInteger)index {
    long n = _friendStatus;//好友状态
    //    if(n>[_titleArr count]-1)
    //        return;
    switch (index) {
        case 0:
            [self reportUserView];
            [self viewDisMissAction];
            break;
        case 1:
            [self onRemark];
            [self viewDisMissAction];
            break;
        case 2:
            if(n == friend_status_black){
                [self onDelBlack];
                [self viewDisMissAction];
            }else{
                [self onAddBlack];
                [self viewDisMissAction];
            }
            break;
        case 3:
            if(n == friend_status_see || n == friend_status_friend){
                //                [self onCancelSee];
                //                [self viewDisMissAction];
                [self onDeleteFriend];
                [self viewDisMissAction];
            }else{
                [self actionAddFriend:nil];
                [self viewDisMissAction];
            }
            
            break;
            
        default:
            [self viewDisMissAction];
            break;
    }
    
}

//- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
//    UITouch * touch = touches.anyObject;
//    if (_selectView == nil) {
//        return;
//    }
//    CGPoint location = [touch locationInView:_selectView];
//    //不在选择范围内
//    if (location.x < 0 || location.x > _selectView.frame.size.width || location.y < 0 || location.y > _selectView.frame.size.height) {
//        [self viewDisMissAction];
//        return;
//    }
//    int num = (location.y - TopHeight)/CellHeight;
//    long n = _friendStatus;//好友状态
//    if(n>[_titleArr count]-1)
//        return;
//    switch (num) {
//        case 0:
//            [self reportUserView];
//            [self viewDisMissAction];
//            break;
//        case 1:
//            [self onRemark];
//            [self viewDisMissAction];
//            break;
//        case 2:
//            if(n == friend_status_black){
//                [self onDelBlack];
//                [self viewDisMissAction];
//            }else{
//                [self onAddBlack];
//                [self viewDisMissAction];
//            }
//            break;
//        case 3:
//            if(n == friend_status_see || n == friend_status_friend){
////                [self onCancelSee];
////                [self viewDisMissAction];
//                [self onDeleteFriend];
//                [self viewDisMissAction];
//            }else{
//                [self actionAddFriend:nil];
//                [self viewDisMissAction];
//            }
//
//            break;
//            
//        default:
//            [self viewDisMissAction];
//            break;
//    }
//}

- (void)viewDisMissAction{
    [UIView animateWithDuration:0.4 animations:^{
        _bgBlackAlpha.alpha = 0.0;
    } completion:^(BOOL finished) {
        [_selectView removeFromSuperview];
        _selectView = nil;
        [_bgBlackAlpha removeFromSuperview];
    }];
}

#pragma mark ---------------创建设置好友备注页面----------------
-(void)onRemark{
    //    JY_InputValueVC* vc = [JY_InputValueVC alloc];
    //    vc.value = user.remarkName;
    //    vc.title = Localized(@"JXUserInfoVC_SetName");
    //    vc.delegate  = self;
    //    vc.isLimit = YES;
    //    vc.didSelect = @selector(onSaveNickName:);
    //    vc = [vc init];
    ////    [g_window addSubview:vc.view];
    //    [g_navigation pushViewController:vc animated:YES];
    
    JY_SetNoteAndLabelVC *vc = [[JY_SetNoteAndLabelVC alloc] init];
    vc.title = Localized(@"JX_SetNotesAndLabels");
    vc.delegate = self;
    vc.didSelect = @selector(refreshLabel:);
    vc.user = self.user;
    [g_navigation pushViewController:vc animated:YES];
}


- (void)refreshLabel:(JY_UserObject *)user {
    
    self.user.remarkName = user.remarkName;
    self.user.describe = user.describe;
    
    //    CGSize sizeN = [user.remarkName sizeWithAttributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:16]}];
    //    _remarkName.frame = CGRectMake(_remarkName.frame.origin.x, _remarkName.frame.origin.y, sizeN.width, _remarkName.frame.size.height);
    //    _sex.frame = CGRectMake(CGRectGetMaxX(_remarkName.frame)+3, _sex.frame.origin.y, _sex.frame.size.width, _sex.frame.size.height);
    //
    //    _remarkName.text = user.remarkName.length > 0 ? user.remarkName : user.userNickname;
    
    [self setLabelAndDescribe];
    
    [g_server setFriendName:self.user.userId noteName:user.remarkName describe:user.describe toView:self];
}


-(void)onSaveNickName:(JY_InputValueVC*)vc{
    _remarkName.text = vc.value;
    user.remarkName = vc.value;
    [g_server setFriendName:user.userId noteName:vc.value describe:nil toView:self];
}

-(void)onAddBlack{
    [g_server addBlacklist:user.userId toView:self];
}

-(void)onDelBlack{
    [g_server delBlacklist:user.userId toView:self];
}

-(void)onCancelSee{
    [g_server delAttention:user.userId toView:self];
}

-(void)onDeleteFriend{
    //    [g_server delAttention:user.userId toView:self];
    [g_server delFriend:user.userId toView:self];
}

-(void)doMakeFriend{
    _friendStatus = friend_status_friend;
    //    user.status = [NSNumber numberWithInt:_friendStatus];
    //    if([user haveTheUser])
    //        [user update];
    //    else
    //        [user insert];
    [self.user doSendMsg:XMPP_TYPE_FRIEND content:nil];
    [JY_MessageObject msgWithFriendStatus:user.userId status:_friendStatus];
    [self showAddFriend];
}

-(void)showUserQRCode{
    JY_QRCodeViewController * qrVC = [[JY_QRCodeViewController alloc] init];
    qrVC.type = QRUserType;
    qrVC.userId = user.userId;
    qrVC.account = user.account;
    qrVC.nickName = user.userNickname;
    qrVC.sex = user.sex;
    //    [g_window addSubview:qrVC.view];
    [g_navigation pushViewController:qrVC animated:YES];
}

- (NSString *)dateTimeDifferenceWithStartTime:(NSNumber *)compareDate {
    NSInteger timeInterval = [[NSDate date] timeIntervalSince1970] - [compareDate integerValue];
    long temp = 0;
    NSString *result;
    if (timeInterval < 60) {
        result = [NSString stringWithFormat:@"%d%@",(int)timeInterval,Localized(@"SECONDS_AGO")];
    }
    else if((temp = timeInterval/60) <60){
        result = [NSString stringWithFormat:@"%ld%@",temp,Localized(@"MINUTES_AGO")];
    }
    
    else if((temp = temp/60) <24){
        result = [NSString stringWithFormat:@"%ld%@",temp,Localized(@"JX_HoursAgo")];
    }
    
    else if((temp = temp/24) <30){
        result = [NSString stringWithFormat:@"%ld%@",temp,Localized(@"JX_DaysAgo")];
    }
    
    else if((temp = temp/30) <12){
        result = [NSString stringWithFormat:@"%ld%@",temp,Localized(@"JX_MonthAgo")];
    }
    else{
        temp = temp/12;
        result = [NSString stringWithFormat:@"%ld%@",temp,Localized(@"JX_YearsAgo")];
    }
    
    return  result;
}


-(void)hideKeyboard{
    [self.view endEditing:YES];
}

@end
