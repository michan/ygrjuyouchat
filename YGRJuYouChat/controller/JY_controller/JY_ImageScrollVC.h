//
//  JY_ImageScrollVC.h
//  TFJunYouChat
//
//  Created by lifengye on 2020/3/14.
//  Copyright © 2020 zengwOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JY_ImageScrollVC : UIViewController

@property (nonatomic,strong)JY_ImageView * iv;
@property (nonatomic,strong)UIScrollView * scrollView;
@property (nonatomic) CGSize imageSize;



@end
