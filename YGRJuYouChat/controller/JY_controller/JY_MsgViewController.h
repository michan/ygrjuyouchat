//
//  JY_MsgViewController.h
//
//  Created by lifengye on 2020/09/03.
//  Copyright (c) 2014年 Reese. All rights reserved.
//

#import "JY_TableViewController.h"
#import "JY_AudioPlayer.h"
#import <UIKit/UIKit.h>

@interface JY_MsgViewController : JY_TableViewController <UIScrollViewDelegate>{
//    NSMutableArray *_array;
    int _refreshCount;
    int _recordCount;
    float lastContentOffset;
    int upOrDown;
    JY_AudioPlayer* _audioPlayer;
    roomData* _room;
    JY_RoomObject *_chatRoom;
}
@property(nonatomic,assign) int msgTotal;
@property (nonatomic, strong) NSMutableArray *array;

@property (nonatomic, strong) UIView *backView;
@property (nonatomic,copy)NSArray *customerArr; //客服信息

- (void)cancelBtnAction;
- (void)getTotalNewMsgCount;

@end
