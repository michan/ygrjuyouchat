//
//  JY_NewFriendViewController.h
//
//  Created by lifengye on 2020/09/03.
//  Copyright (c) 2014年 Reese. All rights reserved.
//

#import "JY_TableViewController.h"
#import <UIKit/UIKit.h>

@class JY_FriendObject;
@class JY_FriendCell;

@interface JY_NewFriendViewController: JY_TableViewController<UITextFieldDelegate>{
    NSMutableArray* _array;
    int _refreshCount;
    JY_FriendObject *_user;
    NSMutableDictionary* poolCell;
    int _friendStatus;
    JY_FriendCell* _cell;
}

@end
