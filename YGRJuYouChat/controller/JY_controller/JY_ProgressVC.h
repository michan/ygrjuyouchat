//
//  JY_ProgressVC.h
//  TFJunYouChat
//
//  Created by lifengye on 2020/09/10.
//  Copyright © 2020 zengwOS. All rights reserved.
//

#import "JY_admobViewController.h"

@interface JY_ProgressVC : JY_admobViewController
@property (nonatomic,strong) UIProgressView * progressView;//进度条
@property (nonatomic,strong) UILabel * progressLabel;//进度
@property (nonatomic,strong) NSArray * dataArray;//数据
@property (nonatomic,assign) long dbFriends;
@property (nonatomic,strong) UILabel * dbCountLabel;
@property (nonatomic,strong) UILabel * sysCountLabel;
@property (nonatomic,strong) UIButton * comBtn;
@end
