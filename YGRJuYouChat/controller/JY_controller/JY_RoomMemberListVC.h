//
//  JY_RoomMemberListVC.h
//  TFJunYouChat
//
//  Created by p on 2018/7/3.
//  Copyright © 2018年 Reese. All rights reserved.
//

#import "JY_TableViewController.h"
#import "JY_RoomObject.h"

typedef enum : NSUInteger {
    Type_Default = 1,
    Type_NotTalk,
    Type_DelMember,
    Type_AddNotes,
    Type_RedPacketAssignUser,
} RoomMemberListType;

// 指定人红包回调
typedef void(^CallBack)(memberData *);

@class JY_InputValueVC;
@class JY_RoomMemberListVC;

@protocol ManMan_RoomMemberListVCDelegate <NSObject>

- (void) roomMemberList:(JY_RoomMemberListVC *)vc delMember:(memberData *)member;

- (void)roomMemberList:(JY_RoomMemberListVC *)selfVC addNotesVC:(JY_InputValueVC *)vc;

@end

@interface JY_RoomMemberListVC : JY_TableViewController


@property (nonatomic,strong) roomData* room;

@property (nonatomic, assign) RoomMemberListType type;
@property (nonatomic,strong) JY_RoomObject* chatRoom;
@property (nonatomic, copy) CallBack callBack;

@property (nonatomic, weak) id<ManMan_RoomMemberListVCDelegate>delegate;

@end
