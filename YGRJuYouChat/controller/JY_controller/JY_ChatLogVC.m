//
//  JY_ChatLogVC.m
//  TFJunYouChat
//
//  Created by p on 2018/7/5.
//  Copyright © 2018年 Reese. All rights reserved.
//

#import "JY_ChatLogVC.h"
#import "JY_BaseChatCell.h"
#import "JY_MessageCell.h"
#import "JY_ImageCell.h"
#import "JY_LocationCell.h"
#import "JY_GifCell.h"
#import "JY_VideoCell.h"
#import "JY_EmojiCell.h"
#import "JY_FaceCustomCell.h"

@interface JY_ChatLogVC ()

@end

@implementation JY_ChatLogVC


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = HEXCOLOR(0xF2F2F2);
    self.tableView.backgroundColor = HEXCOLOR(0xF2F2F2);
    self.isShowFooterPull = NO;
    self.isShowHeaderPull = NO;
    self.isGotoBack = YES;

    self.heightHeader = ManMan_SCREEN_TOP;
    self.heightFooter = 0;
    //self.view.frame = CGRectMake(0, 0, ManMan_SCREEN_WIDTH, ManMan_SCREEN_HEIGHT);
    [self createHeadAndFoot];
    
    
    [self.tableView reloadData];
    
}


- (void)actionQuit {
    [super actionQuit];
}

#pragma mark   ---------tableView协议----------------
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _array.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    JY_MessageObject *msg=[_array objectAtIndex:indexPath.row];
    
    NSLog(@"indexPath.row:%ld,%ld",indexPath.section,indexPath.row);
    
    //返回对应的Cell
    JY_BaseChatCell * cell = [self getCell:msg indexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    msg.changeMySend = 1;
    cell.msg = msg;
    cell.indexNum = (int)indexPath.row;
    cell.delegate = self;
    //    cell.chatCellDelegate = self;
    //    cell.readDele = @selector(readDeleWithUser:);
    cell.isShowHead = YES;
    [cell setCellData];
    [cell setHeaderImage];
    [cell setBackgroundImage];
    [cell isShowSendTime];
    //转圈等待
    if ([msg.isSend intValue] == transfer_status_ing) {
        [cell drawIsSend];
    }
    msg = nil;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    JY_MessageObject *msg=[_array objectAtIndex:indexPath.row];
    
    switch ([msg.type intValue]) {
        case kWCMessageTypeText:
            return [JY_MessageCell getChatCellHeight:msg];
            break;
        case kWCMessageTypeImage:
            return [JY_ImageCell getChatCellHeight:msg];
            break;
        case kWCMessageTypeCustomFace:
            return [JY_FaceCustomCell getChatCellHeight:msg];
            break;
        case kWCMessageTypeEmoji:
            return [JY_EmojiCell getChatCellHeight:msg];
            break;
        case kWCMessageTypeLocation:
            return [JY_LocationCell getChatCellHeight:msg];
            break;
        case kWCMessageTypeGif:
            return [JY_GifCell getChatCellHeight:msg];
            break;
        case kWCMessageTypeVideo:
            return [JY_VideoCell getChatCellHeight:msg];
            break;
        default:
            return [JY_BaseChatCell getChatCellHeight:msg];
            break;
    }
}


#pragma mark -----------------获取对应的Cell-----------------
- (JY_BaseChatCell *)getCell:(JY_MessageObject *)msg indexPath:(NSIndexPath *)indexPath{
    JY_BaseChatCell * cell = nil;
    switch ([msg.type intValue]) {
        case kWCMessageTypeText:
            cell = [self creatMessageCell:msg indexPath:indexPath];
            break;
            
        case kWCMessageTypeImage:
            cell = [self creatImageCell:msg indexPath:indexPath];
            break;
        case kWCMessageTypeCustomFace:
            cell = [self creatFaceCustomCell:msg indexPath:indexPath];
            break;
        case kWCMessageTypeEmoji:
            cell = [self creatEmojiCell:msg indexPath:indexPath];
            break;
        case kWCMessageTypeLocation:
            cell = [self creatLocationCell:msg indexPath:indexPath];
            break;
            
        case kWCMessageTypeGif:
            cell = [self creatGifCell:msg indexPath:indexPath];
            break;
            
        case kWCMessageTypeVideo:
            cell = [self creatVideoCell:msg indexPath:indexPath];
            break;
        default:
            cell = [[JY_BaseChatCell alloc] init];
            break;
    }
    return cell;
}
#pragma  mark -----------------------创建对应的Cell---------------------
//文本
- (JY_BaseChatCell *)creatMessageCell:(JY_MessageObject *)msg indexPath:(NSIndexPath *)indexPath{
    NSString * identifier = @"JY_MessageCell";
    JY_MessageCell *cell=[_table dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[JY_MessageCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
    }
    return cell;
}
//图片
- (JY_BaseChatCell *)creatImageCell:(JY_MessageObject *)msg indexPath:(NSIndexPath *)indexPath{
    NSString * identifier = @"JY_ImageCell";
    JY_ImageCell *cell=[_table dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[JY_ImageCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        //        cell.chatImage.delegate = self;
        //        cell.chatImage.didTouch = @selector(onCellImage:);
    }
    return cell;
}
// 自定义表情
- (JY_BaseChatCell *)creatFaceCustomCell:(JY_MessageObject *)msg indexPath:(NSIndexPath *)indexPath{
    NSString * identifier = @"JY_FaceCustomCell";
    JY_FaceCustomCell *cell=[_table dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[JY_FaceCustomCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        //        cell.chatImage.delegate = self;
        //        cell.chatImage.didTouch = @selector(onCellImage:);
    }
    return cell;
}

// 表情包
- (JY_BaseChatCell *)creatEmojiCell:(JY_MessageObject *)msg indexPath:(NSIndexPath *)indexPath{
    NSString * identifier = @"JY_EmojiCell";
    JY_EmojiCell *cell=[_table dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[JY_EmojiCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        //        cell.chatImage.delegate = self;
        //        cell.chatImage.didTouch = @selector(onCellImage:);
    }
    return cell;
}
//视频
- (JY_BaseChatCell *)creatVideoCell:(JY_MessageObject *)msg indexPath:(NSIndexPath *)indexPath{
    NSString * identifier = @"JY_VideoCell";
    JY_VideoCell *cell=[_table dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[JY_VideoCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    return cell;
}
//位置
- (JY_BaseChatCell *)creatLocationCell:(JY_MessageObject *)msg indexPath:(NSIndexPath *)indexPath{
    NSString * identifier = @"JY_LocationCell";
    JY_LocationCell *cell=[_table dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[JY_LocationCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    return cell;
}
//动画
- (JY_BaseChatCell *)creatGifCell:(JY_MessageObject *)msg indexPath:(NSIndexPath *)indexPath{
    NSString * identifier = @"JY_GifCell";
    JY_GifCell *cell=[_table dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[JY_GifCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
