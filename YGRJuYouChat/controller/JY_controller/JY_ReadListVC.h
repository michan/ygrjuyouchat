//
//  JY_ReadListVC.h
//  TFJunYouChat
//
//  Created by lifengye on 2020/9/2.
//  Copyright © 2020 zengwOS. All rights reserved.
//

#import "JY_TableViewController.h"

@class roomData;

@interface JY_ReadListVC : JY_TableViewController

@property (nonatomic, strong) JY_MessageObject *msg;
@property (nonatomic, strong) roomData *room;

@end
