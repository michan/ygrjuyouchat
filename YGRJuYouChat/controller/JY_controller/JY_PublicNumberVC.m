//
//  JY_PublicNumberVC.m
//  TFJunYouChat
//
//  Created by p on 2018/6/4.
//  Copyright © 2018年 Reese. All rights reserved.
//

#import "JY_PublicNumberVC.h"
#import "JY_Cell.h"
#import "JY_ChatViewController.h"
#import "JY_SearchUserVC.h"
#import "JY_TransferNoticeVC.h"
#import "JY_UserInfoVC.h"

@interface JY_PublicNumberVC ()
@property (nonatomic, strong) NSMutableArray *array;
@property (nonatomic, assign) NSInteger currentIndex;
@end

@implementation JY_PublicNumberVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.heightHeader = ManMan_SCREEN_TOP;
    self.heightFooter = 0;
    self.isGotoBack   = YES;
    self.isShowHeaderPull = NO;
    self.isShowFooterPull = NO;
    _array = [NSMutableArray array];
    self.title = Localized(@"JX_PublicNumber");
    [self createHeadAndFoot];
    [self setupSearchPublicNumber];
    self.view.backgroundColor = THE_LINE_COLOR;
    _table.backgroundColor = THE_LINE_COLOR;
    [self getServerData];
    
    [g_notify addObserver:self selector:@selector(newReceipt:) name:kXMPPReceiptNotifaction object:nil];
}

-(void)newReceipt:(NSNotification *)notifacation{//新回执
    //    NSLog(@"newReceipt");
    JY_MessageObject *msg     = (JY_MessageObject *)notifacation.object;
    if(msg == nil)
        return;
    if(![msg isAddFriendMsg])
        return;
    [_wait stop];
    
    if([msg.type intValue] == XMPP_TYPE_DELALL){//删除好友
        [self getServerData];
        [g_notify postNotificationName:kXMPPNewFriendNotifaction object:nil];

    }
}

- (void)setupSearchPublicNumber {
    if ([g_config.enableMpModule boolValue]) {
        UIButton *moreBtn = [UIFactory createButtonWithImage:@"search_publicNumber_black"
                                                   highlight:nil
                                                      target:self
                                                    selector:@selector(searchPublicNumber)];
        moreBtn.custom_acceptEventInterval = 1.0f;
        moreBtn.frame = CGRectMake(ManMan_SCREEN_WIDTH - 18-15, ManMan_SCREEN_TOP - 18-15, 18, 18);
        [self.tableHeader addSubview:moreBtn];
    }
}


- (void)searchPublicNumber {
    JY_SearchUserVC *searchUserVC = [JY_SearchUserVC alloc];
    searchUserVC.type = ManMan_SearchTypePublicNumber;
    searchUserVC = [searchUserVC init];
    [g_navigation pushViewController:searchUserVC animated:YES];
}

- (void)getServerData {
    
    self.array = [[JY_UserObject sharedInstance] fetchSystemUser];

    [_table reloadData];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _array.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 88;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    JY_UserObject *user = _array[indexPath.row];
     
    JY_Cell *cell=nil;
    NSString* cellName = @"JY_Cell";
    cell = [tableView dequeueReusableCellWithIdentifier:cellName];
    
    if(cell==nil){
        
        cell = [[JY_Cell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellName];
        [_table addToPool:cell];
        cell.selectionStyle = UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.layer.cornerRadius = 8;
        cell.layer.masksToBounds = YES;
    }
    
    
    cell.title = user.userNickname;
    
    cell.index = (int)indexPath.row;
    cell.delegate = self;
//    cell.didTouch = @selector(onHeadImage:);
    [cell setForTimeLabel:[TimeUtil formatDate:user.timeCreate format:@"MM-dd HH:mm"]];
    cell.timeLabel.frame = CGRectMake(ManMan_SCREEN_WIDTH - 115-15, 59/2-10, 0, 0);
    cell.userId = user.userId;
    [cell.lbTitle setText:cell.title];
    
    cell.dataObj = user;
    //    cell.headImageView.tag = (int)indexPath.row;
    //    cell.headImageView.delegate = cell.delegate;
    //    cell.headImageView.didTouch = cell.didTouch;
    
    cell.isSmall = YES;
    [cell headImageViewImageWithUserId:nil roomId:nil];
    
    float height=[self tableView:tableView heightForRowAtIndexPath:indexPath];
    UIView * view=[cell.contentView viewWithTag:1200];
    if(view==nil){
        UIView* line = [[UIView alloc]init];
        line.backgroundColor = THE_LINE_COLOR;
        line.frame=CGRectMake(0, height-LINE_WH, ManMan_SCREEN_WIDTH, LINE_WH);
        [cell.contentView addSubview:line];
        line.tag=1200;
    }else{
        view.frame=CGRectMake(0, height-LINE_WH, ManMan_SCREEN_WIDTH, LINE_WH);
    }
  
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    JY_Cell* cell = (JY_Cell*)[tableView cellForRowAtIndexPath:indexPath];
    
    cell.selected = NO;
    JY_UserObject *user = _array[indexPath.row];
    
    if ([user.userId intValue] == [SHIKU_TRANSFER intValue]) {
        JY_TransferNoticeVC *noticeVC = [[JY_TransferNoticeVC alloc] init];
        [g_navigation pushViewController:noticeVC animated:YES];
        return;
    }
    
    if([user.userType intValue] == 2 && [user.status intValue] != 2){
        JY_UserInfoVC* userVC = [JY_UserInfoVC alloc];
        userVC.userId = user.userId;
        userVC.user = user;
        userVC.fromAddType = 6;
        userVC = [userVC init];
        
        [g_navigation pushViewController:userVC animated:YES];
        return;
    }
    
    JY_ChatViewController *sendView=[JY_ChatViewController alloc];
    
    sendView.scrollLine = 0;
    sendView.title = user.userNickname;
    sendView.chatPerson = user;
    sendView = [sendView init];
    [g_navigation pushViewController:sendView animated:YES];
    sendView.view.hidden = NO;
}



// 进入编辑模式，按下出现的编辑按钮后,进行删除操作
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        JY_UserObject *user = _array[indexPath.row];
         _currentIndex = indexPath.row;
        [g_server delAttention:user.userId toView:self];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    JY_UserObject *user = _array[indexPath.row];

    if ([user.userId intValue] == 10000) {
        return NO;
    }
    return YES;
}

// 定义编辑样式
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}

// 修改编辑按钮文字
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return Localized(@"JX_Delete");
}

//服务器返回数据
-(void) didServerResultSucces:(JY_Connection*)aDownload dict:(NSDictionary*)dict array:(NSArray*)array1{
    
    if ([aDownload.action isEqualToString:act_AttentionDel]) {
        [_wait stop];
        
        JY_UserObject *user = _array[_currentIndex];
        [_array removeObject:user];
        [_table deleteRow:(int)_currentIndex section:0];
        
        [user doSendMsg:XMPP_TYPE_DELALL content:nil];

    }
}



-(int) didServerResultFailed:(JY_Connection*)aDownload dict:(NSDictionary*)dict{
    [_wait hide];
    return show_error;
}

-(int) didServerConnectError:(JY_Connection*)aDownload error:(NSError *)error{//error为空时，代表超时
    [_wait hide];
    return show_error;
}

-(void) didServerConnectStart:(JY_Connection*)aDownload{
    [_wait start];
}


@end
