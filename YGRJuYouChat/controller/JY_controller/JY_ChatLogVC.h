//
//  JY_ChatLogVC.h
//  TFJunYouChat
//
//  Created by p on 2018/7/5.
//  Copyright © 2018年 Reese. All rights reserved.
//

#import "JY_TableViewController.h"

@interface JY_ChatLogVC : JY_TableViewController

@property (nonatomic, strong) NSMutableArray *array;

@end
