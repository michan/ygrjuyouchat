//
//  JY_SearchUserVC.h
//  TFJunYouChat
//
//  Created by flyeagleTang on 14-6-10.
//  Copyright (c) 2014年 Reese. All rights reserved.
//

#import "JY_admobViewController.h"
@class searchData;


typedef NS_ENUM(NSInteger, ManMan_SearchType) {
    ManMan_SearchTypeUser,           // 好友
    ManMan_SearchTypePublicNumber,   // 公众号
};


@interface JY_SearchUserVC : JY_admobViewController{
    UITextField* _name;
    UITextField* _minAge;
    UITextField* _maxAge;
    UILabel* _date;
    UILabel* _sex;
    UILabel* _industry;
    UILabel* _function;
    
    UIImage* _image;
    JY_ImageView* _head;
    
    NSMutableArray* _values;
    NSMutableArray* _numbers;
}

@property (nonatomic, assign) ManMan_SearchType type;
@property (nonatomic,strong) searchData* job;
@property(nonatomic,weak) id delegate;
@property(assign) SEL didSelect;


@end
