//
//  JY_FileDetailViewController.h
//  TFJunYouChat
//
//  Created by 1 on 17/7/7.
//  Copyright © 2020 zengwOS. All rights reserved.
//

#import "JY_admobViewController.h"
@class JY_ShareFileObject;

@interface JY_FileDetailViewController : JY_admobViewController

@property (nonatomic,strong) JY_ShareFileObject * shareFile;

@end
