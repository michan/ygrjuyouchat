//
//  JY_MenuView.h
//  TFJunYouChat
//
//  Created by 1 on 2018/9/6.
//  Copyright © 2018年 Reese. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JY_MenuView;

@protocol ManMan_MenuViewDelegate <NSObject>

- (void)didMenuView:(JY_MenuView *)menuView WithButtonIndex:(NSInteger)index;

@end

@interface JY_MenuView : UIView

@property (nonatomic, weak) id<ManMan_MenuViewDelegate>delegate;

@property (nonatomic, strong) NSArray *titles;


/**
 
暂为weiboVC 专用控件
Point (.x 暂时无效)

 */

//  创建
- (instancetype)initWithPoint:(CGPoint)point Title:(NSArray *)titles Images:(NSArray *)images;

//  隐藏
- (void)dismissBaseView;


@end
