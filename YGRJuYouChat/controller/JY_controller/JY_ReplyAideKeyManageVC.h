//
//  JY_ReplyAideKeyManageVC.h
//  TFJunYouChat
//
//  Created by p on 2019/5/15.
//  Copyright © 2019年 Reese. All rights reserved.
//

#import "JY_admobViewController.h"

NS_ASSUME_NONNULL_BEGIN
@class JY_HelperModel;

@interface JY_ReplyAideKeyManageVC : JY_admobViewController

@property (nonatomic, strong) NSMutableArray *keys;

@property (nonatomic, strong) NSString *roomId;
@property (nonatomic, strong) NSString *helperId;
@property (nonatomic, strong) JY_HelperModel *model;

@end

NS_ASSUME_NONNULL_END
