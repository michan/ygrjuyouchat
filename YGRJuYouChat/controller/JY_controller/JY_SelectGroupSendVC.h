//
//  JY_SelectGroupSendVC.h
//  TFJunYouChat
//
//  Created by lifengye on 2020/8/14.
//  Copyright © 2019 zengwOS. All rights reserved.
//

#import "JY_TableViewController.h"

#define SELECTGroup Localized(@"JX_SelectGroup")
#define SELECTColleague Localized(@"JX_ChooseColleagues")
#define SELECTMaillist Localized(@"JX_SelectPhoneContact")
NS_ASSUME_NONNULL_BEGIN
@class JY_SelectGroupSendVC;
@protocol ManMan_SelectGroupSendVCDelegate <NSObject>

- (void)selectVC:(JY_SelectGroupSendVC *)selectLabelsVC selectArray:(NSMutableArray *)array;

@end

@interface JY_SelectGroupSendVC : JY_TableViewController
@property (nonatomic,strong)NSString *titleString;
@property (nonatomic,weak) id<ManMan_SelectGroupSendVCDelegate> delegate;
@property (nonatomic,strong) NSMutableArray *seletedArray;
- (instancetype)initWithTitle:(NSString *)title;

@end

NS_ASSUME_NONNULL_END
