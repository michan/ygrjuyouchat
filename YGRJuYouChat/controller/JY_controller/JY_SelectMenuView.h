//
//  JY_SelectMenuView.h
//  TFJunYouChat
//
//  Created by lifengye on 2020/9/12.
//  Copyright © 2020 zengwOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JY_SelectMenuView;

@protocol ManMan_SelectMenuViewDelegate <NSObject>

- (void)didMenuView:(JY_SelectMenuView *)MenuView WithIndex:(NSInteger)index;

@end

@interface JY_SelectMenuView : UIView

@property (nonatomic, weak) id<ManMan_SelectMenuViewDelegate>delegate;
@property (nonatomic, strong) NSMutableArray *sels;

- (instancetype)initWithTitle:(NSArray *)titleArr image:(NSArray *)images cellHeight:(int)height;


// 隐藏
- (void)hide;

@end
