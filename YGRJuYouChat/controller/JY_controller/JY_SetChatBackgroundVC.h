//
//  JY_SetChatBackgroundVC.h
//  TFJunYouChat
//
//  Created by lifengye on 2020/12/8.
//  Copyright © 2020 zengwOS. All rights reserved.
//

#import "JY_admobViewController.h"

@class JY_SetChatBackgroundVC;
@protocol ManMan_SetChatBackgroundVCDelegate <NSObject>

- (void)setChatBackgroundVC:(JY_SetChatBackgroundVC *)setChatBgVC image:(UIImage *)image;

@end

@interface JY_SetChatBackgroundVC : JY_admobViewController

@property (nonatomic, weak) id<ManMan_SetChatBackgroundVCDelegate>delegate;
@property (nonatomic, copy) NSString *userId;

@end
