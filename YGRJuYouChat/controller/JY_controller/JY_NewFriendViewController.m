//
//  JY_NewFriendViewController.h.m
//
//  Created by lifengye on 2020/09/03.
//  Copyright (c) 2014年 Reese. All rights reserved.
//

#import "JY_NewFriendViewController.h"
#import "JY_ChatViewController.h"
#import "AppDelegate.h"
#import "JY_Label.h"
#import "JY_ImageView.h"
#import "JY_FriendCell.h"
#import "JY_RoomPool.h"
#import "JY_FriendObject.h"
#import "UIFactory.h"
#import "JY_InputVC.h"
#import "JY_UserInfoVC.h"

@interface JY_NewFriendViewController ()<ManMan_FriendCellDelegate>

@property(nonatomic, strong)UITextField *searchTf;

@end

@implementation JY_NewFriendViewController

- (UITextField *)searchTf{
    if (!_searchTf) {
        _searchTf = [[UITextField alloc] init];
        _searchTf.backgroundColor = [UIColor whiteColor];
        ViewRadius(_searchTf, 5);
        _searchTf.placeholder = @"🔍 搜索";
        _searchTf.textAlignment = NSTextAlignmentCenter;
    }
    return _searchTf;
}
- (id)init
{
    self = [super init];
    if (self) {
        self.heightHeader = ManMan_SCREEN_TOP;
        self.heightFooter = 0;
        self.isGotoBack   = YES;
        self.title =@"新朋友";// Localized(@"JXNewFriendVC_NewFirend");
        
        //self.view.frame = CGRectMake(0, 0, ManMan_SCREEN_WIDTH, ManMan_SCREEN_HEIGHT);
        [self createHeadAndFoot];
        [self addRightBarButton];
        self.isShowFooterPull = NO;
        
        self.tableView.backgroundColor = HEXCOLOR(0xF2F2F2);

        [g_notify addObserver:self selector:@selector(newRequest:) name:kXMPPNewRequestNotifaction object:nil];
        [g_notify addObserver:self selector:@selector(newReceipt:) name:kXMPPReceiptNotifaction object:nil];
        [g_notify addObserver:self selector:@selector(onSendTimeout:) name:kXMPPSendTimeOutNotifaction object:nil];

        poolCell = [[NSMutableDictionary alloc]init];
        current_chat_userId = FRIEND_CENTER_USERID;
        _table.delegate = self;
        self.view.backgroundColor = self.tableView.backgroundColor;
        [self.view addSubview:self.searchTf];
        self.searchTf.frame = CGRectMake(15, ManMan_SCREEN_TOP, SCREEN_WIDTH-30, 40);
        self.tableView.frame = CGRectMake(0, ManMan_SCREEN_TOP + 50, SCREEN_WIDTH, SCREEN_HEIGHT - CGRectGetMaxY(self.searchTf.frame)  - 10);
    }
    return self;
}

- (void)dealloc {
//    NSLog(@"JY_NewFriendViewController.dealloc");
//    [super dealloc];
}

-(void)free{
    current_chat_userId = nil;
    [_array removeAllObjects];
//    [_array release];
    [poolCell removeAllObjects];
//    [poolCell release];
    
    [g_notify  removeObserver:self name:kXMPPNewRequestNotifaction object:nil];
    [g_notify  removeObserver:self name:kXMPPSendTimeOutNotifaction object:nil];
    [g_notify  removeObserver:self name:kXMPPReceiptNotifaction object:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _array   = [[NSMutableArray alloc]init];
    [self refresh];
}
/// 添加清空按钮
-(void)addRightBarButton{
    
    UIButton *clearButton = [UIFactory createButtonWithImage:@"nav_delete_btn"
                                          highlight:nil
                                             target:self
                                           selector:@selector(clearButtonClick:)];
    clearButton.custom_acceptEventInterval = 1.0f;
    clearButton.frame = CGRectMake(ManMan_SCREEN_WIDTH - 18-BTN_RANG_UP*2, ManMan_SCREEN_TOP - 18-BTN_RANG_UP*2, 18+BTN_RANG_UP*2, 18+BTN_RANG_UP*2);
    [self.tableHeader addSubview:clearButton];
}
#pragma mark 清空列表
-(void)clearButtonClick:(UIButton *)sender{
    [self removeAllFriendsFromLocal];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark   ---------tableView协议----------------
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _array.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	JY_FriendCell *cell=nil;
    NSString* cellName = [NSString stringWithFormat:@"msg_%d_%ld",_refreshCount,indexPath.row];
    cell = [tableView dequeueReusableCellWithIdentifier:cellName];
    if(cell==nil){
        JY_FriendObject *user=_array[indexPath.row];
        cell = [JY_FriendCell alloc];
        [_table addToPool:cell];
        cell.tag   = indexPath.row;
        cell.delegate = self;
        cell.title = user.remarkName.length > 0 ? user.remarkName : user.userNickname;
        cell.subtitle = user.userId;
        cell.bottomTitle = [TimeUtil formatDate:user.timeCreate format:@"MM-dd HH:mm"];
        cell.user        = user;
        cell.target      = self;
        cell = [cell initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellName];
        user = nil;
    }
    
    if (indexPath.row == _array.count - 1) {
        cell.lineView.frame = CGRectMake(cell.lineView.frame.origin.x, cell.lineView.frame.origin.y, cell.lineView.frame.size.width, 0);
    }else {
        cell.lineView.frame = CGRectMake(cell.lineView.frame.origin.x, cell.lineView.frame.origin.y, cell.lineView.frame.size.width, LINE_WH);
    }

    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    JY_FriendObject *friend=_array[indexPath.row];
    JY_UserObject *user = [[JY_UserObject alloc] init];
    user.userNickname = friend.userNickname;
    user.userId = friend.userId;
    
    
    
    
    JY_UserInfoVC* vc = [JY_UserInfoVC alloc];
    vc.userId       = user.userId;
    vc.fromAddType = 6;
    vc = [vc init];
    [g_navigation pushViewController:vc animated:YES];
    
    
//    JY_ChatViewController *sendView=[JY_ChatViewController alloc];
//    sendView.isHiddenFooter = YES;
//    sendView.title = friend.userNickname;
//    sendView.chatPerson = user;
//    sendView = [sendView init];
//    [g_navigation pushViewController:sendView animated:YES];
}

- (void)friendCell:(JY_FriendCell *)friendCell headImageAction:(NSString *)userId {
    
//    [g_server getUser:userId toView:self];
    
    JY_UserInfoVC* vc = [JY_UserInfoVC alloc];
    vc.userId       = userId;
    vc.fromAddType = 6;
    vc = [vc init];
    [g_navigation pushViewController:vc animated:YES];
}

-(void)refresh{
    [self stopLoading];
    _refreshCount++;
//    [_array release];
    _array=[[JY_FriendObject sharedInstance] fetchAllFriendsFromLocal];
    [_table reloadData];
}
-(void)removeAllFriendsFromLocal{
    [[JY_FriendObject sharedInstance] deleteAllFriendsFromLocal];
    [self refresh];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 84;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"%ld",indexPath.row);
    JY_FriendObject *user = _array[indexPath.row];
    [_array removeObjectAtIndex:indexPath.row];
    [user delete];
    [_table reloadData];
}

-(void)scrollToPageUp{
    [self refresh];
}

-(void)onSendTimeout:(NSNotification *)notifacation//超时未收到回执
{
    [_wait stop];
    JY_MessageObject *msg     = (JY_MessageObject *)notifacation.object;
    if(msg==nil)
        return;
    JY_FriendCell* cell = [poolCell objectForKey:msg.messageId];
    if(cell){
//        [g_App showAlert:Localized(@"JXAlert_SendFilad")];
        [JY_MyTools showTipView:Localized(@"JXAlert_SendFilad")];
        [poolCell removeObjectForKey:msg.messageId];
    }
}

-(void)newRequest:(NSNotification *)notifacation{//新推送
//    NSLog(@"newRequest");
    JY_FriendObject *user     = (JY_FriendObject *)notifacation.object;
    if(user == nil)
        return;
//    if(_wait.isShowing)//正在等待，就不刷新
//        return;
    for(int i=0;i<[_array count];i++){
        JY_FriendObject* friend = [_array objectAtIndex:i];
        if([friend.userId isEqualToString:user.userId]){
            [friend loadFromObject:user];
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
            JY_FriendCell* cell = (JY_FriendCell*)[_table cellForRowAtIndexPath:indexPath];
            [cell update];
            cell = nil;
            return;
        }
        friend = nil;
    }
    [self refresh];
}

-(void)onSayHello:(UIButton*)sender{//打招呼
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    _cell = (JY_FriendCell*)[_table cellForRowAtIndexPath:indexPath];

    JY_InputVC* vc = [JY_InputVC alloc];
    vc.delegate = self;
    vc.didTouch = @selector(onInputHello:);
    vc.inputText = Localized(@"JXNewFriendVC_Iam");
    vc = [vc init];
    [g_window addSubview:vc.view];
}

-(void)onInputHello:(JY_InputVC*)sender{
    NSString* messageId = [_cell.user doSendMsg:XMPP_TYPE_SAYHELLO content:sender.inputText];
    [poolCell setObject:_cell forKey:messageId];
    [_wait start:nil];
}


-(void)onFeedback:(UIButton*)sender{//回话
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    _cell = (JY_FriendCell*)[_table cellForRowAtIndexPath:indexPath];
    
    JY_InputVC* vc = [JY_InputVC alloc];
    vc.delegate = self;
    vc.didTouch = @selector(onInputReply:);
    vc.inputText = Localized(@"JXNewFriendVC_Who");
    vc = [vc init];
    [g_window addSubview:vc.view];
}

-(void)onInputReply:(JY_InputVC*)sender{
    NSString* messageId = [_cell.user doSendMsg:XMPP_TYPE_FEEDBACK content:sender.inputText];
    [poolCell setObject:_cell forKey:messageId];
    [_wait start:nil];
}

-(void)onSeeHim:(UIButton*)sender{//关注他
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    JY_FriendCell* cell = (JY_FriendCell*)[_table cellForRowAtIndexPath:indexPath];
    NSString* messageId = [cell.user doSendMsg:XMPP_TYPE_NEWSEE content:nil];
    [poolCell setObject:cell forKey:messageId];
    [_wait start:nil];
}

-(void)onAddFriend:(UIButton*)sender{//加好友
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    _cell = (JY_FriendCell*)[_table cellForRowAtIndexPath:indexPath];
    _user = _cell.user;
    [g_server addAttention:_user.userId fromAddType:0 toView:self];
}

-(void)actionQuit{
    [self free];
    [super actionQuit];
}

-(void) didServerResultSucces:(JY_Connection*)aDownload dict:(NSDictionary*)dict array:(NSArray*)array1{
    [_wait stop];
    if([aDownload.action isEqualToString:act_AttentionAdd]){
        int n = [[dict objectForKey:@"type"] intValue];
        if( n==2 || n==4)
            _friendStatus = 2;
//        else
//            _friendStatus = 1;

        if(_friendStatus == 2){
            NSString* messageId = [_user doSendMsg:XMPP_TYPE_PASS content:nil];
            [poolCell setObject:_cell forKey:messageId];
            [_wait start:nil];
            messageId = nil;
        }
    }
    
    //点击好友头像响应
    if( [aDownload.action isEqualToString:act_UserGet] ){
        JY_UserObject* user = [[JY_UserObject alloc]init];
        [user getDataFromDict:dict];
        
        JY_UserInfoVC* vc = [JY_UserInfoVC alloc];
        vc.user       = user;
        vc.fromAddType = 6;
//        vc.isJustShow = YES;
        vc = [vc init];
//        [g_window addSubview:vc.view];
        [g_navigation pushViewController:vc animated:YES];
//        [self cancelBtnAction];
    }
}

-(int) didServerResultFailed:(JY_Connection*)aDownload dict:(NSDictionary*)dict{
    [_wait stop];
    return show_error;
}

-(int) didServerConnectError:(JY_Connection*)aDownload error:(NSError *)error{//error为空时，代表超时
    [_wait stop];
    return show_error;
}

-(void) didServerConnectStart:(JY_Connection*)aDownload{
    [_wait start];
}

-(void)newReceipt:(NSNotification *)notifacation{//新回执
    //    NSLog(@"newReceipt");
    [_wait stop];
    JY_MessageObject *msg     = (JY_MessageObject *)notifacation.object;
    if(msg == nil)
        return;
    if(![msg isAddFriendMsg])
        return;
    if(![msg.toUserId isEqualToString:_cell.user.userId])
        return;
    if([msg.type intValue] == XMPP_TYPE_PASS){//通过
        _friendStatus = friend_status_friend;
        _user.status = [NSNumber numberWithInt:_friendStatus];
        [_user update];

        JY_MessageObject *msg=[[JY_MessageObject alloc] init];
        msg.type = [NSNumber numberWithInt:kWCMessageTypeText];
        msg.toUserId = _user.userId;
        msg.fromUserId = MY_USER_ID;
        msg.fromUserName = g_server.myself.userNickname;
        msg.content = Localized(@"JXFriendObject_StartChat");
        msg.timeSend = [NSDate date];
        [msg insert:nil];
        [msg updateLastSend:UpdateLastSendType_None];
        [msg notifyNewMsg];
    }
    
    JY_FriendCell* cell = [poolCell objectForKey:msg.messageId];
    if(cell){
        [cell.user loadFromMessageObj:msg];
        [cell update];
        [g_App showAlert:Localized(@"JXAlert_SendOK")];
        [poolCell removeObjectForKey:msg.messageId];
    }
}

@end
