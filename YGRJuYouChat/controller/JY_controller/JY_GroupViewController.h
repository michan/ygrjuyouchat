//
//  JY_GroupViewController
//  BaseProject
//
//  Created by Huan Cho on 13-8-3.
//  Copyright (c) 2013年 ch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JY_TableViewController.h"
#import "JY_TopSiftJobView.h"

@protocol XMPPRoomDelegate;
@class JY_RoomObject;
@class menuImageView;

@interface JY_GroupViewController : JY_TableViewController{
    
    int _refreshCount;
    int _recordCount;

    NSString* _roomJid;
    JY_RoomObject *_chatRoom;
    UITextField* _inputText;

    menuImageView* _tb;
    UIScrollView * _scrollView;
    int _selMenu;
    JY_TopSiftJobView *_topSiftView; //表头筛选控件
//    UIButton * _myRoomBtn;
//    UIButton * _allRoomBtn;
//    UIView *_topScrollLine;
//    int _sel;
}
@property (nonatomic,strong) NSMutableArray * array;
@property (assign,nonatomic) int sel;

//- (void)actionNewRoom;
//- (void)reconnectToRoom;
@end
