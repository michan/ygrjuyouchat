//
//  JY_RelayVC.h
//  TFJunYouChat
//
//  Created by lifengye on 2020/6/27.
//  Copyright © 2020 zengwOS. All rights reserved.
//

#import "JY_TableViewController.h"
#import "JY_ChatViewController.h"

@class JY_RelayVC;
@protocol ManMan_RelayVCDelegate <NSObject>

@optional

- (void)relay:(JY_RelayVC *)relayVC MsgAndUserObject:(JY_MsgAndUserObject *)obj;

- (void)shareSuccess;

@end

@interface JY_RelayVC : JY_TableViewController

//@property (nonatomic, strong) JY_MessageObject *msg;
@property (nonatomic, strong) NSMutableArray *relayMsgArray;

@property (nonatomic, assign) BOOL isCourse;
@property (nonatomic, weak) id<ManMan_RelayVCDelegate> relayDelegate;

@property (nonatomic, strong) JY_UserObject *chatPerson;
@property (nonatomic,copy) NSString *roomJid;

@property (nonatomic, assign) BOOL isShare;
@property (nonatomic, copy) NSString *shareSchemes;
@property (nonatomic, assign) BOOL isUrl;
@property (nonatomic, assign) BOOL isMoreSel;
@property (nonatomic, weak) JY_ChatViewController *chatVC; ;


@end
