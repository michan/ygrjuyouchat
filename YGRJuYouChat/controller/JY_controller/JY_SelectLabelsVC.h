//
//  JY_SelectLabelsVC.h
//  TFJunYouChat
//
//  Created by p on 2018/7/19.
//  Copyright © 2018年 Reese. All rights reserved.
//

#import "JY_TableViewController.h"

@class JY_SelectLabelsVC;
@protocol ManMan_SelectLabelsVCDelegate <NSObject>

- (void)selectLabelsVC:(JY_SelectLabelsVC *)selectLabelsVC selectLabelsArray:(NSMutableArray *)array;

@end

@interface JY_SelectLabelsVC : JY_TableViewController

@property (nonatomic, strong) NSMutableArray *selLabels;

@property (nonatomic, weak) id<ManMan_SelectLabelsVCDelegate>delegate;

@end
