//
//  JY_NewLabelVC.h
//  TFJunYouChat
//
//  Created by p on 2018/6/21.
//  Copyright © 2018年 Reese. All rights reserved.
//

#import "JY_TableViewController.h"
#import "JY_LabelObject.h"

@interface JY_NewLabelVC : JY_TableViewController

@property (nonatomic, strong) JY_LabelObject *labelObj;

@end
