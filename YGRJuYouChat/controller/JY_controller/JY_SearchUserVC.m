//
//  JY_SearchUserVC.m
//  TFJunYouChat
//
//  Created by flyeagleTang on 14-6-10.
//  Copyright (c) 2014年 Reese. All rights reserved.
//

#import "JY_SearchUserVC.h"
//#import "selectTreeVC.h"
#import "selectProvinceVC.h"
#import "selectValueVC.h"
#import "ImageResize.h"
#import "searchData.h"
#import "JY_SearchUserListVC.h"
#import "JY_SearchUserVC.h"
#import "JY_ScanQRViewController.h"
#import "QLAddFirendCell.h"
#import "JY_QRCodeViewController.h"


#define HEIGHT 44
#define STARTTIME_TAG 1
#define IMGSIZE 100

@interface JY_SearchUserVC ()<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>
@property(nonatomic, strong)UITextField *nSearchTF;
@property(nonatomic, strong)UIImageView *searchImageView;
@property(nonatomic, strong)UILabel *myAccessLabel;
@property(nonatomic, strong)UIButton *accessImg;
@property(nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *datas;
@property (nonatomic,strong) JY_UserObject* user;
@end

@implementation JY_SearchUserVC
@synthesize job,delegate,didSelect,user;
//@synthesize user;
- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, ManMan_SCREEN_TOP, SCREEN_WIDTH, SCREEN_HEIGHT - ManMan_SCREEN_TOP) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableHeaderView = [self searchView];
        _tableView.backgroundColor = HEXCOLOR(0xF0F0F0);
        _tableView.tableFooterView = [UIView new];
        [_tableView registerNib:[UINib nibWithNibName:@"QLAddFirendCell" bundle:nil] forCellReuseIdentifier:@"QLAddFirendCell"];
    }
    return _tableView;
}
- (NSArray *)datas{
    if (!_datas) {
        _datas = @[@{@"image":@"icon_ql_saoyisao",@"title":@"扫一扫二维码",@"subTitle":@"扫一扫二维码，轻松添加好友"}];
//        @{@"image":@"icon_ql_shouji",@"title":@"添加手机联系人",@"subTitle":@"将手机通讯录中的朋友加为好友"}
    }
    return _datas;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.datas.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    QLAddFirendCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QLAddFirendCell" forIndexPath:indexPath];
    cell.selectionStyle = 0;
    cell.data = self.datas[indexPath.row];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        [self showScanViewController];
    }
}
-(CGFloat )tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 67;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.00;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.00;
}
- (id)init
{
    self = [super init];
    if (self) {
        [g_server getUser:g_myself.userId toView:self];
        job = [[searchData alloc] init];
        self.isGotoBack   = YES;
        if (self.type == ManMan_SearchTypeUser) {
            self.title =@"加好友";// Localized(@"JXNearVC_AddFriends");
        }else {
            self.title = Localized(@"JX_SearchPublicNumber");
        }
        self.heightFooter = 0;
        self.heightHeader = ManMan_SCREEN_TOP;
        //self.view.frame = CGRectMake(0, 0, ManMan_SCREEN_WIDTH, ManMan_SCREEN_HEIGHT);
        [self createHeadAndFoot];
        
        int h = 10;
        _values = [[NSMutableArray alloc]initWithObjects:Localized(@"JXSearchUserVC_AllDate"),Localized(@"JXSearchUserVC_OneDay"),Localized(@"JXSearchUserVC_TwoDay"),Localized(@"JXSearchUserVC_ThereDay"),Localized(@"JXSearchUserVC_OneWeek"),Localized(@"JXSearchUserVC_TwoWeek"),Localized(@"JXSearchUserVC_OneMonth"),Localized(@"JXSearchUserVC_SixWeek"),Localized(@"JXSearchUserVC_TwoMonth"),nil];
        _numbers = [[NSMutableArray alloc]initWithObjects:@"0",@"1",@"2",@"3",@"7",@"14",@"30",@"42",@"60",nil];
        
//        NSString* city = [g_constant getAddressForNumber:g_myself.provinceId cityId:g_myself.cityId areaId:g_myself.areaId];
        job.sex    = -1;
        
        JY_ImageView* iv;
        
        NSString *name;
        NSString *phoneN;
        NSString *input;
        if (self.type == ManMan_SearchTypeUser) {
            if ([g_config.nicknameSearchUser intValue] != 0 && [g_config.regeditPhoneOrName intValue] == 0) {
                name = Localized(@"JX_NickName");
                phoneN = Localized(@"JX_OrPhoneNumber");
                input = Localized(@"JX_InputNickName");
            }else if([g_config.nicknameSearchUser intValue] == 0 && [g_config.regeditPhoneOrName intValue] == 0) {
                name = Localized(@"JX_SearchPhoneNumber");
                phoneN = @"";
                input = Localized(@"JX_InputPhone");
            }else if ([g_config.nicknameSearchUser intValue] == 0 && [g_config.regeditPhoneOrName intValue] == 1) {
                name = Localized(@"JX_UserName");
                phoneN = @"";
                input = Localized(@"JX_InputUserAccount");
            }else {
                name = Localized(@"JX_NickName");
                phoneN = Localized(@"JX_SearchOrUserName");
                input = Localized(@"JX_InputNickName");
            }
        }else {
            name = @"";
            phoneN = Localized(@"JX_PublicNumber");
            input = Localized(@"JX_PleaseEnterThe");
        }
        
        iv = [self createButton:[NSString stringWithFormat:@"%@%@",name,phoneN] drawTop:NO drawBottom:NO must:NO click:nil];
        iv.frame = CGRectMake(0, h, ManMan_SCREEN_WIDTH, HEIGHT);
        _name = [self createTextField:iv default:job.name hint:[NSString stringWithFormat:@"%@%@",input,phoneN]];
//        [_name becomeFirstResponder];
        h+=iv.frame.size.height;
        
        /*
        iv = [self createButton:Localized(@"JX_Sex") drawTop:NO drawBottom:YES must:NO click:@selector(onSex)];
        iv.frame = CGRectMake(0, h, ManMan_SCREEN_WIDTH, HEIGHT);
        _sex = [self createLabel:iv default:Localized(@"JXSearchUserVC_All")];
        h+=iv.frame.size.height;
        
        iv = [self createButton:Localized(@"JXSearchUserVC_MinAge") drawTop:NO drawBottom:YES must:NO click:nil];
        iv.frame = CGRectMake(0, h, ManMan_SCREEN_WIDTH, HEIGHT);
        _minAge = [self createTextField:iv default:@"0" hint:Localized(@"JXSearchUserVC_MinAge")];
        h+=iv.frame.size.height;
        
        iv = [self createButton:Localized(@"JXSearchUserVC_MaxAge") drawTop:NO drawBottom:YES must:NO click:nil];
        iv.frame = CGRectMake(0, h, ManMan_SCREEN_WIDTH, HEIGHT);
        _maxAge = [self createTextField:iv default:@"200" hint:Localized(@"JXSearchUserVC_MaxAge")];
        h+=iv.frame.size.height;
        
        iv = [self createButton:Localized(@"JXSearchUserVC_AppearTime") drawTop:NO drawBottom:YES must:NO click:@selector(onDate)];
        iv.frame = CGRectMake(0, h, ManMan_SCREEN_WIDTH, HEIGHT);
        _date = [self createLabel:iv default:[_values objectAtIndex:0]];
        h+=iv.frame.size.height;
        */
        h+=30;
        UIButton* _btn;
        _btn = [UIFactory createCommonButton:Localized(@"JX_Seach") target:self action:@selector(onSearch)];
        _btn.custom_acceptEventInterval = .25f;
        _btn.frame = CGRectMake(15, h, ManMan_SCREEN_WIDTH-30, 40);
        _btn.titleLabel.font = SYSFONT(16);
        _btn.layer.masksToBounds = YES;
        _btn.layer.cornerRadius = 7.f;
        [self.tableBody addSubview:_btn];
        
        
        h+=_btn.frame.size.height + 50;
        iv = [self createButton:Localized(@"JX_Scan") drawTop:NO drawBottom:NO icon:THESIMPLESTYLE ? @"messaeg_scnning_black" : @"messaeg_scnning_black" click:@selector(showScanViewController)];
        iv.frame = CGRectMake(0,h, ManMan_SCREEN_WIDTH, 50);
        h+=iv.frame.size.height;
        self.view.backgroundColor = HEXCOLOR(0xF0F0F0);//[UIColor groupTableViewBackgroundColor];
        self.tableHeader.backgroundColor = HEXCOLOR(0xF0F0F0);
//        self.tableBody.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [self.view addSubview:self.tableView];
        self.tableBody.hidden = YES;
    }
    return self;
}
-(UIView *)searchView{
    UIView * backView = [[UIView alloc] initWithFrame:CGRectMake(0, ManMan_SCREEN_TOP, ManMan_SCREEN_WIDTH, 100)];
    [self.view addSubview:backView];

 
    
    CGSize size =[@"聚友号/手机号" boundingRectWithSize:CGSizeMake(MAXFLOAT, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:g_factory.font14} context:nil].size;
    
    _nSearchTF = [[UITextField alloc] initWithFrame:CGRectMake(15, 5, backView.frame.size.width - 30, 35)];
    [_nSearchTF becomeFirstResponder];
    _nSearchTF.placeholder =@"聚友号/手机号";// [NSString stringWithFormat:@"%@", Localized(@"JX_SearchChatLog")];
    _nSearchTF.backgroundColor = [UIColor whiteColor]; HEXCOLOR(0xf0f0f0);
    _nSearchTF.textColor = [UIColor blackColor];
    [_nSearchTF setFont:SYSFONT(14)];
//    _seekTextField.backgroundColor = [UIColor whiteColor];
    _searchImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"card_search"]];
    UIView *leftView = [[UIView alloc ]initWithFrame:CGRectMake(ManMan_SCREEN_WIDTH/2-size.width, 0, 30, 30)];
//    imageView.center = CGPointMake(leftView.frame.size.width/2, leftView.frame.size.height/2);
    _searchImageView.center = leftView.center;
    [leftView addSubview:_searchImageView];
    _nSearchTF.leftView = leftView;
    _nSearchTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    _nSearchTF.layer.cornerRadius = 5;
    _nSearchTF.layer.masksToBounds = YES;
    _nSearchTF.leftViewMode = UITextFieldViewModeAlways;
    _nSearchTF.textAlignment = NSTextAlignmentCenter;
    _nSearchTF.borderStyle = UITextBorderStyleNone;
    _nSearchTF.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    _nSearchTF.delegate = self;
    _nSearchTF.returnKeyType =UIReturnKeySearch;// UIReturnKeyGoogle;
    [backView addSubview:_nSearchTF];
    
//    [_newSearchTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    CGSize nameSize =[@"我的聚友号：" boundingRectWithSize:CGSizeMake(MAXFLOAT, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:g_factory.font14} context:nil].size;
    _myAccessLabel = [[UILabel alloc] initWithFrame:CGRectMake((SCREEN_WIDTH -nameSize.width )/2 -20, 62, nameSize.width, 15)];
    _myAccessLabel.font = [UIFont systemFontOfSize:14];
    _myAccessLabel.textColor = HEXCOLOR(0x303030);
    _myAccessLabel.text = @"我的聚友号：";
    [backView addSubview:_myAccessLabel];
    _myAccessLabel.textAlignment = NSTextAlignmentCenter;
    
    _accessImg = [[UIButton alloc] initWithFrame:CGRectMake((SCREEN_WIDTH -nameSize.width )/2 + nameSize.width - 10, 61, 16, 16)];
    [backView addSubview:_accessImg];
    [_accessImg setImage:[UIImage imageNamed:@"icon_lv_erweima"] forState:UIControlStateNormal];
    [_accessImg addTarget:self action:@selector(showUserQRCode) forControlEvents:UIControlEventTouchUpInside];
    return backView;
}
-(void) didServerResultSucces:(JY_Connection*)aDownload dict:(NSDictionary*)dict array:(NSArray*)array1{
    
    [_wait stop];
    if( [aDownload.action isEqualToString:act_UserGet] ){
        [_wait stop];
        
        JY_UserObject* user = [[JY_UserObject alloc]init];
        [user getDataFromDict:dict];
        self.user = user;
    }
}
-(void)setUser:(JY_UserObject *)user{
    NSString *accessStr = [NSString stringWithFormat:@"我的聚友号:%@",user.account];
    CGSize nameSize =[accessStr boundingRectWithSize:CGSizeMake(MAXFLOAT, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:g_factory.font14} context:nil].size;
    _myAccessLabel.text = accessStr;
    _myAccessLabel.frame =CGRectMake((SCREEN_WIDTH -nameSize.width )/2 -20, 62, nameSize.width, 15);
    _accessImg.frame = CGRectMake((SCREEN_WIDTH -nameSize.width )/2 + nameSize.width - 10, 61, 16, 16);
}
-(void)showUserQRCode{
    [g_server getUser:nil toView:self];
    JY_QRCodeViewController * qrVC = [[JY_QRCodeViewController alloc] init];
    qrVC.type = QRUserType;
    qrVC.userId = user.userId;
    qrVC.account = user.account;
    qrVC.nickName = user.userNickname;
    qrVC.sex = user.sex;
    [g_navigation pushViewController:qrVC animated:YES];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.view endEditing:YES];
    if (_nSearchTF == textField) {
        if (_nSearchTF.text.length > 0) {
            [self onSearch];
            return YES;
        }
        return NO;
    }
    return NO;
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField.text.length == 0) {
        self.searchImageView.hidden = NO;
    }else{
        self.searchImageView.hidden = YES;
    }
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    self.searchImageView.hidden = YES;
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [_nSearchTF resignFirstResponder];
}
-(void)showScanViewController{
//    button.enabled = NO;
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        button.enabled = YES;
//    });
    
    AVAuthorizationStatus authStatus =  [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if (authStatus == AVAuthorizationStatusRestricted || authStatus ==AVAuthorizationStatusDenied)
    {
        [g_server showMsg:Localized(@"JX_CanNotopenCenmar")];
        return;
    }
    
    JY_ScanQRViewController * scanVC = [[JY_ScanQRViewController alloc] init];
    
//    [g_window addSubview:scanVC.view];
    [g_navigation pushViewController:scanVC animated:YES];
}

-(void)dealloc{
//    NSLog(@"JY_SearchUserVC.dealloc");
    self.job = nil;
//    [super dealloc];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}

-(JY_ImageView*)createButton:(NSString*)title drawTop:(BOOL)drawTop drawBottom:(BOOL)drawBottom icon:(NSString*)icon click:(SEL)click{
    JY_ImageView* btn = [[JY_ImageView alloc] init];
    btn.backgroundColor = [UIColor whiteColor];
    btn.userInteractionEnabled = YES;
    btn.didTouch = click;
    btn.delegate = self;
//    btn.layer.cornerRadius =5;
//    btn.layer.masksToBounds =YES;
    [self.tableBody addSubview:btn];
     
  
    JY_Label* p = [[JY_Label alloc] initWithFrame:CGRectMake(60, 0, self_width-35-20-35, 30)];
    p.text = title;
    p.font = g_factory.font16;
    p.backgroundColor = [UIColor clearColor];
    p.textColor = HEXCOLOR(0x323232);
    [btn addSubview:p];
    
    
    JY_Label* p1 = [[JY_Label alloc] initWithFrame:CGRectMake(60, CGRectGetMaxY(p.frame), self_width-35-20-35, 20)];
    p1.text = @"扫描二维码名片";
    p1.font = g_factory.font12;
    p1.backgroundColor = [UIColor clearColor];
    p1.textColor = [UIColor lightGrayColor];
    [btn addSubview:p1];
    
    
    if(icon){
        UIImageView* iv = [[UIImageView alloc] initWithFrame:CGRectMake(15, 10, 30, 30)];
        iv.image = [UIImage imageNamed:icon];
        [btn addSubview:iv];
    }
    if(drawTop){
        UIView* line = [[UIView alloc] initWithFrame:CGRectMake(53,0,ManMan_SCREEN_WIDTH-53,LINE_WH)];
        //line.backgroundColor = THE_LINE_COLOR;
        [btn addSubview:line];
    }
    if(drawBottom){
        UIView* line = [[UIView alloc] initWithFrame:CGRectMake(53,HEIGHT-0.3,ManMan_SCREEN_WIDTH-53,LINE_WH)];
        line.backgroundColor = THE_LINE_COLOR;
        line.alpha = 0.5;
        [btn addSubview:line];
    }
    if(click){
        UIImageView* iv;
        iv = [[UIImageView alloc] initWithFrame:CGRectMake(ManMan_SCREEN_WIDTH-10-27, (50-13)/2, 7, 13)];
        iv.image = [UIImage imageNamed:@"new_icon_>"];
        [btn addSubview:iv];
    }
    return btn;
}

-(JY_ImageView*)createButton:(NSString*)title drawTop:(BOOL)drawTop drawBottom:(BOOL)drawBottom must:(BOOL)must click:(SEL)click{
    JY_ImageView* btn = [[JY_ImageView alloc] init];
    btn.backgroundColor = [UIColor whiteColor];
    btn.userInteractionEnabled = YES;
    btn.delegate = self;
    if(click)
        btn.didTouch = click;
    else
        btn.didTouch = @selector(hideKeyboard);
    [self.tableBody addSubview:btn];
//    [btn release];
    
    if(must){
        UILabel* p = [[UILabel alloc] initWithFrame:CGRectMake(INSETS, 5, 20, HEIGHT-5)];
        p.text = @"*";
        p.font = g_factory.font18;
        p.backgroundColor = [UIColor clearColor];
        p.textColor = [UIColor redColor];
        p.textAlignment = NSTextAlignmentCenter;
        [btn addSubview:p];
//        [p release];
    }
    
    JY_Label* p = [[JY_Label alloc] initWithFrame:CGRectMake(20, 0, ManMan_SCREEN_WIDTH/2-40, HEIGHT)];
    p.text = title;
    p.font = g_factory.font16;
    p.backgroundColor = [UIColor clearColor];
    p.textColor = [UIColor blackColor];
    [btn addSubview:p];
//    [p release];
    
    if(drawTop){
        UIView* line = [[UIView alloc] initWithFrame:CGRectMake(0,0,ManMan_SCREEN_WIDTH,LINE_WH)];
        line.backgroundColor = THE_LINE_COLOR;
        [btn addSubview:line];
//        [line release];
    }
    
    if(drawBottom){
        UIView* line = [[UIView alloc]initWithFrame:CGRectMake(0,HEIGHT-LINE_WH,ManMan_SCREEN_WIDTH,LINE_WH)];
        line.backgroundColor = THE_LINE_COLOR;
        [btn addSubview:line];
//        [line release];
    }
    
    if(click){
        UIImageView* iv;
        iv = [[UIImageView alloc] initWithFrame:CGRectMake(ManMan_SCREEN_WIDTH-15-7, (HEIGHT-13)/2, 7, 13)];
        iv.image = [UIImage imageNamed:@"new_icon_>"];
        [btn addSubview:iv];
//        [iv release];
    }
    return btn;
}

-(UITextField*)createTextField:(UIView*)parent default:(NSString*)s hint:(NSString*)hint{
    UITextField* p = [[UITextField alloc] initWithFrame:CGRectMake(ManMan_SCREEN_WIDTH/2,INSETS,ManMan_SCREEN_WIDTH/2-15,HEIGHT-INSETS*2)];
    p.delegate = self;
    p.autocorrectionType = UITextAutocorrectionTypeNo;
    p.autocapitalizationType = UITextAutocapitalizationTypeNone;
    p.enablesReturnKeyAutomatically = YES;
    p.borderStyle = UITextBorderStyleNone;
    p.returnKeyType = UIReturnKeyDone;
    p.clearButtonMode = UITextFieldViewModeWhileEditing;
    p.textAlignment = NSTextAlignmentRight;
    p.userInteractionEnabled = YES;
    p.text = s;
    p.placeholder = hint;
    p.font = g_factory.font16;
    [parent addSubview:p];
//    [p release];
    return p;
}

-(UILabel*)createLabel:(UIView*)parent default:(NSString*)s{
    UILabel* p = [[UILabel alloc] initWithFrame:CGRectMake(ManMan_SCREEN_WIDTH/2,INSETS,ManMan_SCREEN_WIDTH/2 - 30,HEIGHT-INSETS*2)];
    p.userInteractionEnabled = NO;
    p.text = s;
    p.font = g_factory.font14;
    p.textAlignment = NSTextAlignmentRight;
    [parent addSubview:p];
//    [p release];
    return p;
}

-(void)onSex{
    if([self hideKeyboard])
        return;
    
    selectValueVC* vc = [selectValueVC alloc];
    vc.values = [NSMutableArray arrayWithObjects:Localized(@"JXSearchUserVC_All"),Localized(@"JX_Man"),Localized(@"JX_Wuman"),nil];
    vc.selNumber = 0;
    vc.numbers   = [NSMutableArray arrayWithObjects:@"-1",@"1",@"0",nil];
    vc.delegate  = self;
    vc.didSelect = @selector(onSelSex:);
    vc.quickSelect = YES;
    vc = [vc init];
//    [g_window addSubview:vc.view];
    [g_navigation pushViewController:vc animated:YES];
}

-(void)onSelSex:(selectValueVC*)sender{
    if([self hideKeyboard])
        return;
    
    _sex.text  = sender.selValue;
    job.sex    = sender.selNumber;
}

-(void)onDate{
    if([self hideKeyboard])
        return;
    
    selectValueVC* vc = [selectValueVC alloc];
    vc.values = _values;
    vc.selNumber = 0;
    vc.numbers   = _numbers;
    vc.delegate  = self;
    vc.didSelect = @selector(onSelDate:);
    vc.quickSelect = YES;
    vc = [vc init];
//    [g_window addSubview:vc.view];
    [g_navigation pushViewController:vc animated:YES];
}

-(void)onSelDate:(selectValueVC*)sender{
    job.showTime = sender.selNumber;
    _date.text = sender.selValue;
}

-(void)onSearch{
    NSString *searchText =_nSearchTF.text;
    if ([searchText isEqualToString:@""]) {//[_name.text isEqualToString:@""]
        if (self.type == ManMan_SearchTypeUser) {
            if ([g_config.nicknameSearchUser intValue] == 0 && [g_config.regeditPhoneOrName intValue] == 0) {
                [g_App showAlert:Localized(@"JX_InputPhone")];
            }else if ([g_config.nicknameSearchUser intValue] == 0 && [g_config.regeditPhoneOrName intValue] == 1){
                [g_App showAlert:Localized(@"JX_InputUserAccount")];
            }else {
                [g_App showAlert:Localized(@"JX_InputNickName")];
            }
        }else {
            [g_App showAlert:Localized(@"JX_PleaseEnterTheServerNo.")];
        }
    }else{
        job.name = searchText;
        job.minAge = [_minAge.text intValue];
        job.maxAge = [_maxAge.text intValue];
        [self actionQuit];
        JY_SearchUserListVC *vc = [[JY_SearchUserListVC alloc] init];
        if (self.type == ManMan_SearchTypeUser) {
            vc.isUserSearch = YES;
        }else {
            vc.isUserSearch = NO;
        }
        vc.keyWorld = _nSearchTF.text;
        vc.search = job;
        vc.title = @"搜索结果";
        [g_navigation pushViewController:vc animated:YES];
//        if (delegate && [delegate respondsToSelector:didSelect])
////            [delegate performSelector:didSelect withObject:job];
//            [delegate performSelectorOnMainThread:didSelect withObject:job waitUntilDone:NO];
    }
    
}

-(BOOL)hideKeyboard{
    BOOL b = _name.editing;
    [self.view endEditing:YES];
    return b;
}


//- (BOOL)textFieldShouldReturn:(UITextField *)textField
//{
//    [self.view endEditing:YES];
//    return YES;
//}

@end
