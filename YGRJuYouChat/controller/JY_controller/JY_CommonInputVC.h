//
//  JY_CommonInputVC.h
//  TFJunYouChat
//
//  Created by p on 2019/4/1.
//  Copyright © 2019年 Reese. All rights reserved.
//

#import "JY_admobViewController.h"

NS_ASSUME_NONNULL_BEGIN

@class JY_CommonInputVC;

@protocol ManMan_CommonInputVCDelegate <NSObject>

- (void)commonInputVCBtnActionWithVC:(JY_CommonInputVC *)commonInputVC;

@end

@interface JY_CommonInputVC : JY_admobViewController

@property (nonatomic, copy) NSString *titleStr;
@property (nonatomic,copy) NSString *subTitle;
@property (nonatomic,copy) NSString *tip;
@property (nonatomic,copy) NSString *btnTitle;
@property (nonatomic, strong) UITextField *name;
@property (nonatomic, weak) id<ManMan_CommonInputVCDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
