//
//  QLAlertView.m
//  TFJunYouChat
//
//  Created by Qian on 2021/10/31.
//  Copyright © 2021 zengwOS. All rights reserved.
//

#import "QLAlertView.h"



@implementation QLAlertView
- (IBAction)dismissClick:(id)sender {
    [self removeFromSuperview];
}

- (IBAction)shareClick:(id)sender {
    if (self.sendValueBlock) {
        [self removeFromSuperview];
        self.sendValueBlock(1);
    }
}
- (IBAction)saveClick:(id)sender {
    if (self.sendValueBlock) {
        [self removeFromSuperview];
        self.sendValueBlock(2);
    }
}
- (IBAction)cancerClick:(id)sender {
    if (self.sendValueBlock) {
        [self removeFromSuperview];
        self.sendValueBlock(0);
    }
    
}

@end
