//
//  QLAlertView.h
//  TFJunYouChat
//
//  Created by Qian on 2021/10/31.
//  Copyright © 2021 zengwOS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef  void(^sendValue)(NSInteger  value);
@interface QLAlertView : UIView
@property (nonatomic, copy)  sendValue  sendValueBlock;

@end

NS_ASSUME_NONNULL_END
