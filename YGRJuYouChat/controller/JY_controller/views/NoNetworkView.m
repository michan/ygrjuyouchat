//
//  NoNetworkView.m
//  TFJunYouChat
//
//  Created by Qian on 2021/10/31.
//  Copyright © 2021 zengwOS. All rights reserved.
//

#import "NoNetworkView.h"

@implementation NoNetworkView
+(NoNetworkView *)proNoNetworkView{
    return [[NSBundle mainBundle] loadNibNamed:@"NoNetworkView" owner:nil options:nil].firstObject;
}
- (IBAction)click:(id)sender {
    if (self.viewBlockClick) {
        self.viewBlockClick();
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
