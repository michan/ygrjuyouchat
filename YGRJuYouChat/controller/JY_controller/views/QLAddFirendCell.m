//
//  QLAddFirendCell.m
//  TFJunYouChat
//
//  Created by Qian on 2021/10/31.
//  Copyright © 2021 zengwOS. All rights reserved.
//

#import "QLAddFirendCell.h"
@interface QLAddFirendCell ()
@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *sub;

@end
@implementation QLAddFirendCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setData:(NSDictionary *)data{
    _data = data;
//    @"image":@"icon_ql_saoyisao",@"title":@"扫一扫二维码",@"subTitle":@"扫一扫二维码，轻松添加好友"
    self.img.image = [UIImage imageNamed:data[@"image"]];
    self.title.text = data[@"title"];
    self.sub.text = data[@"subTitle"];
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
