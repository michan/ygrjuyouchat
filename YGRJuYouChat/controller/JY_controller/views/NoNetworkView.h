//
//  NoNetworkView.h
//  TFJunYouChat
//
//  Created by Qian on 2021/10/31.
//  Copyright © 2021 zengwOS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef  void(^clickBlock)(void);
@interface NoNetworkView : UIView

@property (nonatomic, copy) clickBlock viewBlockClick;
+(NoNetworkView *)proNoNetworkView;
@end

NS_ASSUME_NONNULL_END
