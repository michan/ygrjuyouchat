//
//  QLAddFirendCell.h
//  TFJunYouChat
//
//  Created by Qian on 2021/10/31.
//  Copyright © 2021 zengwOS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QLAddFirendCell : UITableViewCell
@property(nonatomic, strong) NSDictionary *data;
@end

NS_ASSUME_NONNULL_END
