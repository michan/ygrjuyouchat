//
//  JY_ChatLogMoveVC.m
//  TFJunYouChat
//
//  Created by p on 2019/6/5.
//  Copyright © 2019年 Reese. All rights reserved.
//

#import "JY_ChatLogMoveVC.h"
#import "JY_ChatLogMoveSelectVC.h"

@interface JY_ChatLogMoveVC ()

@end

@implementation JY_ChatLogMoveVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.isGotoBack = YES;
    self.title = Localized(@"JX_ChatLogMove");
    self.heightHeader = ManMan_SCREEN_TOP;
    self.heightFooter = 0;
    //self.view.frame = CGRectMake(0, 0, ManMan_SCREEN_WIDTH, ManMan_SCREEN_HEIGHT);
    [self createHeadAndFoot];
    
    self.tableBody.backgroundColor = HEXCOLOR(0xffffff);
    UIImageView *imgv = [UIImageView new];
    [self.tableBody addSubview:imgv];
    imgv.image =[UIImage imageNamed:@"迁移图片"];
    [imgv mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(112);
        make.height.mas_equalTo(81);
        make.top.mas_equalTo(self.tableBody).offset(39);
        make.centerX.mas_equalTo(self.tableBody.mas_centerX);
    }];
    
        
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(0, 162, ManMan_SCREEN_WIDTH, 15)];
    title.font = [UIFont systemFontOfSize:14];

    title.textAlignment = NSTextAlignmentCenter;
    title.text = Localized(@"JX_ChatLogMoveToDevice");
    [self.tableBody addSubview:title];
    
    UILabel *subTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(title.frame) + 12, ManMan_SCREEN_WIDTH, 14)];
    subTitle.font = [UIFont systemFontOfSize:13];
    subTitle.textColor = [UIColor lightGrayColor];
    subTitle.textAlignment = NSTextAlignmentCenter;
    subTitle.text = Localized(@"JX_TwoDeviceConnectWIFI");
    [self.tableBody addSubview:subTitle];
    
    UIButton *btn = [UIFactory createCommonButton:Localized(@"JX_MoveChatRecords") target:self action:@selector(onMove)];
    [btn setBackgroundImage:nil forState:UIControlStateHighlighted];
    btn.custom_acceptEventInterval = 1.f;
    [btn.titleLabel setFont:SYSFONT(15)];

    btn.frame = CGRectMake(25,CGRectGetMaxY(subTitle.frame) + 39, ManMan_SCREEN_WIDTH- 50, 44);
    [btn setBackgroundImage:nil forState:UIControlStateNormal];
    btn.layer.masksToBounds = YES;
    btn.layer.cornerRadius = 5.f;
    btn.backgroundColor = HEXCOLOR(0x05D168);
    [self.tableBody addSubview:btn];
    
    
}

- (void)onMove {
    
    JY_ChatLogMoveSelectVC *vc = [[JY_ChatLogMoveSelectVC alloc] init];
    [g_navigation pushViewController:vc animated:YES];
    
    [self actionQuit];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
