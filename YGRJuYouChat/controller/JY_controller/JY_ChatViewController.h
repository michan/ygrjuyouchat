//
//  JY_ChatViewController.h
//
//  Created by Reese on 13-8-11.
//  Copyright (c) 2013年 Reese. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <AVFoundation/AVFoundation.h>
#import "JY_TableViewController.h"
#import "JY_LocationVC.h"


@class JY_Emoji;
@class JY_SelectImageView;
@class JY_VolumeView;
@class JY_RoomObject;
@class JY_BaseChatCell;
@class JY_VideoPlayer;
@interface JY_ChatViewController : JY_TableViewController<UIImagePickerControllerDelegate,UITextViewDelegate,AVAudioPlayerDelegate,UIImagePickerControllerDelegate,AVAudioRecorderDelegate,UINavigationControllerDelegate,LXActionSheetDelegate>
{
    
    NSMutableArray *_pool;
    UITextView *_messageText;
    UIImageView *inputBar;
    UIButton* _recordBtn;
    UIButton* _recordBtnLeft;
    UIImage *_myHeadImage,*_userHeadImage;
    JY_SelectImageView *_moreView;
    UIButton* _btnFace;
    UIButton* _btnClear;
    JY_emojiViewController* _faceView;
    JY_Emoji* _messageConent;

    BOOL recording;
    NSTimer *peakTimer;
    
    AVAudioRecorder *audioRecorder;
    AVAudioPlayer *audioPlayer;
	NSURL *pathURL;
    UIView* talkView;
    NSString* _lastRecordFile;
    NSString* _lastPlayerFile;
    NSTimeInterval _lastPlayerTime;
    long _lastIndex;

    double lowPassResults;
    NSTimeInterval _timeLen;
    int _refreshCount;
    
    JY_VolumeView* _voice;
    NSTimeInterval _disableSay;
    NSString * _audioMeetingNo;
    NSString * _videoMeetingNo;
    NSMutableArray * _orderRedPacketArray ;
}
- (IBAction)sendIt:(id)sender;
- (IBAction)shareMore:(id)sender;
//- (void)refresh;
@property (nonatomic, weak) UILabel *tipColock;
@property (nonatomic,strong) JY_RoomObject* chatRoom;
@property (nonatomic,strong) roomData * room;
@property (nonatomic,strong) JY_UserObject *chatPerson;//必须要赋值
@property (nonatomic, strong) JY_MessageObject *lastMsg;
@property (nonatomic,strong) NSString* roomJid;//相当于RoomJid
@property (nonatomic,strong) NSString* roomId;
@property (nonatomic,strong) JY_BaseChatCell* selCell;
@property (nonatomic,strong) JY_LocationVC * locationVC;
@property (nonatomic, strong) NSMutableArray *array;

@property (nonatomic,strong) NSString* answerContent;


@property (nonatomic, strong) NSMutableArray *arrayReport;

//@property (nonatomic, strong) JY_MessageObject *relayMsg;
@property (nonatomic, strong) NSMutableArray *relayMsgArray;
@property (nonatomic, assign) int scrollLine;

@property (nonatomic, strong) NSMutableArray *courseArray;
@property (nonatomic, copy) NSString *courseId;

@property (nonatomic, strong) NSNumber *groupStatus;

@property (nonatomic, assign) BOOL isCYMSGgroupANDFriendy;
@property (nonatomic, strong) NSMutableArray *userNamesWithGroup;
@property (nonatomic, strong) NSMutableArray *userNmaesWithFriend;

@property (nonatomic, assign) BOOL isGroupMessages;
@property (nonatomic, strong) NSMutableArray *userIds;
@property (nonatomic, strong) NSMutableArray *userNames;

@property (nonatomic, assign) BOOL isHiddenFooter;
@property (nonatomic, strong) NSMutableArray *chatLogArray;

@property (nonatomic, assign) NSInteger rowIndex;
@property (nonatomic, assign) int newMsgCount;

@property (nonatomic, strong) JY_VideoPlayer *player;
@property (nonatomic, strong) UIView *playerView;
@property (nonatomic, assign) BOOL isShare;
@property (nonatomic, copy) NSString *shareSchemes;

@property (nonatomic, strong) NSDictionary *dictGameOverDictg;

@property (nonatomic, assign) BOOL isAudioOrVideo;

-(void)sendRedPacket:(NSDictionary*)redPacketDict withGreet:(NSString *)greet;
//-(void)onPlay;
//-(void)recordPlay:(long)index;
-(void)resend:(JY_MessageObject*)p;
-(void)deleteMsg:(JY_MessageObject*)p;
-(void)showOneMsg:(JY_MessageObject*)msg;
@end
