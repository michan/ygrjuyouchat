//
//  JY_CirclePermissionsVC.h
//  TFJunYouChat
//
//  Created by 1 on 2019/8/27.
//  Copyright © 2019年 Reese. All rights reserved.
//

#import "JY_admobViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface JY_CirclePermissionsVC : JY_admobViewController

@property (nonatomic, strong) JY_UserObject *user;

@end

NS_ASSUME_NONNULL_END
