//
//  JY_RoomMemberVC.h
//  TFJunYouChat
//
//  Created by flyeagleTang on 14-6-10.
//  Copyright (c) 2014年 Reese. All rights reserved.
//

#import "JY_admobViewController.h"
@class roomData;
@class JY_RoomObject;

@protocol ManMan_RoomMemberVCDelegate <NSObject>

- (void) setNickName:(NSString *)nickName;
- (void) needVerify:(JY_MessageObject *)msg;

@end

@interface JY_RoomMemberVC : JY_admobViewController<LXActionSheetDelegate>{
    JY_Label* _desc;
    JY_Label* _userName;
    JY_Label* _roomName;
    UILabel* _memberCount;
    UILabel* _creater;
    UILabel* _size;
    NSMutableArray* _deleteArr;
    NSMutableArray* _images;
    NSMutableArray* _names;
    BOOL _delMode;
    JY_RoomObject *_chatRoom;
    int _h;
    BOOL _isAdmin;
    BOOL _allowEdit;
    UILabel* _note;
    UILabel* _userNum;
    UIView* _heads;
    int _delete;
    int _disable;
    BOOL _disableMode;
    BOOL _unfoldMode;
    JY_UserObject* _user;
    JY_ImageView* _blackBtn;
    int _modifyType;
    NSString* _content;
    NSString* _toUserId;
    NSString* _toUserName;
    UISwitch * _readSwitch;
    UISwitch *_messageFreeSwitch;
    UISwitch *_allNotTalkSwitch;
    UISwitch *_topSwitch;
    UISwitch *_notMsgSwitch;
    UILabel* _roomNum;
}

@property (nonatomic, assign) NSString *roomId;

@property (nonatomic,strong) JY_RoomObject* chatRoom;
@property (nonatomic,strong) roomData* room;
@property (nonatomic,strong) JY_ImageView * iv;
@property (nonatomic, weak) id<ManMan_RoomMemberVCDelegate>delegate;
@property (nonatomic, assign) int rowIndex;
@property (nonatomic,strong) NSDictionary *dictData;
//@property (nonatomic,strong) NSString* userNickname;

@end
