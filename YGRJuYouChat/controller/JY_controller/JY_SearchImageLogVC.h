//
//  JY_SearchImageLogVC.h
//  TFJunYouChat
//
//  Created by p on 2019/4/9.
//  Copyright © 2019年 Reese. All rights reserved.
//

#import "JY_admobViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface JY_SearchImageLogVC : JY_admobViewController

@property (nonatomic, assign) BOOL isImage;
@property (nonatomic, strong) JY_UserObject *user;

@end

NS_ASSUME_NONNULL_END
