//
//  JY_NewLabelVC.m
//  TFJunYouChat
//
//  Created by p on 2018/6/21.
//  Copyright © 2018年 Reese. All rights reserved.
//

#import "JY_NewLabelVC.h"
#import "JY_SelFriendVC.h"
#import "JY_SelectFriendsVC.h"
#import "JY_Cell.h"
#import "BMChineseSort.h"
#import "JY_UserInfoVC.h"
#import "JY_ChatViewController.h"

#define HEIGHT 54

@interface JY_NewLabelVC ()

@property (nonatomic, strong) NSMutableArray *array;

@property (nonatomic, strong) UITextField *labelName;
@property (nonatomic, strong) UILabel *labelUserNum;
@end

@implementation JY_NewLabelVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.heightHeader = ManMan_SCREEN_TOP;
    self.heightFooter = 0;
    self.isShowFooterPull = NO;
    self.isGotoBack   = YES;
    [self createHeadAndFoot];
    
//    self.tableView.backgroundColor = HEXCOLOR(0xf0eff4);
    
    _array = [NSMutableArray array];
    
    UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeSystem];
//    doneBtn.layer.masksToBounds = YES;
//    doneBtn.layer.cornerRadius = 3.f;
    [doneBtn.titleLabel setFont:SYSFONT(15)];
    doneBtn.frame = CGRectMake(ManMan_SCREEN_WIDTH - 51 - 15, ManMan_SCREEN_TOP - 8 - 29, 51, 29);
    [doneBtn setTitle:@"完成" forState:UIControlStateNormal];
    [doneBtn setTitleColor:HEXCOLOR(0x05D168) forState:UIControlStateNormal];
    [doneBtn addTarget:self action:@selector(doneBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self.tableHeader addSubview:doneBtn];
    
    [self createTableHeaderView];
}

- (void)createTableHeaderView {
    UIView *view = [[UIView alloc] init];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, ManMan_SCREEN_WIDTH, 15)];
    label.text = @"标签名字";
    label.textColor = HEXCOLOR(0x909090);
    label.font = SYSFONT(15);
    [view addSubview:label];
    
    UIView *fieldView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(label.frame)+9, ManMan_SCREEN_WIDTH, 50)];
    fieldView.backgroundColor = HEXCOLOR(0xffffff);
    [view addSubview:fieldView];
    self.labelName = [[UITextField alloc] initWithFrame:CGRectMake(12, 0, fieldView.frame.size.width - 24, fieldView.frame.size.height)];
    self.labelName.backgroundColor = [UIColor clearColor];
    self.labelName.font = [UIFont systemFontOfSize:16.0];
    self.labelName.placeholder = @"未设置标签名字";
    if (self.labelObj.groupName.length > 0) {
        self.labelName.text = self.labelObj.groupName;
    }
    [fieldView addSubview:self.labelName];
    
    
    
    UIView *addView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(fieldView.frame) + 10, ManMan_SCREEN_WIDTH, 50)];
    addView.backgroundColor = HEXCOLOR(0xffffff);
    [view addSubview:addView];
    UITapGestureRecognizer *addViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addFriendAction)];
    addView.userInteractionEnabled = YES;
    [addView addGestureRecognizer:addViewTap];

    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(15, 15, 20, 20)];
//    imageView.center = CGPointMake(imageView.center.x, btn.frame.size.height / 2);
    imageView.image = [[UIImage imageNamed:@"person_add_green"] imageWithTintColor:HEXCOLOR(0x05D168)];
    [addView addSubview:imageView];
    label = [[UILabel alloc] initWithFrame:CGRectMake(45, 15, SCREEN_WIDTH - 50, 20)];
    label.textColor =HEXCOLOR(0x909090);// THEMECOLOR;
    label.text = @"添加成员";
    label.font = SYSFONT(15);
    [addView addSubview:label];
//    [view addSubview:btn];
    
//    [addView addSubview:btn];
    
    view.frame = CGRectMake(0, 0, ManMan_SCREEN_WIDTH, CGRectGetMaxY(addView.frame) + 10);
    
    self.tableView.tableHeaderView = view;
    
    NSString *userIdStr = self.labelObj.userIdList;
    NSArray *userIds = [userIdStr componentsSeparatedByString:@","];
    if (userIdStr.length <= 0) {
        userIds = nil;
    }
    [_array removeAllObjects];
    
    for (NSInteger i = 0; i < userIds.count; i ++) {
        JY_UserObject *user = [[JY_UserObject alloc] init];
        user.userId = userIds[i];
//        NSString *userName = [JY_UserObject getUserNameWithUserId:userIds[i]];
//        user.userNickname = userName;
        
        [_array addObject:user];
    }
    self.labelUserNum.text = [NSString stringWithFormat:@"%@(%ld)",@"标签成员",_array.count];
    [self.tableView reloadData];
}

- (void)addFriendAction {
    
    JY_SelectFriendsVC *vc = [[JY_SelectFriendsVC alloc] init];
    vc.type = ManMan_SelectFriendTypeSelFriends;
    vc.delegate = self;
    vc.didSelect = @selector(selectFriendsDelegate:);
    
    NSMutableSet *set = [NSMutableSet set];
    for (NSInteger i = 0; i < self.array.count; i ++) {
        JY_UserObject *user = self.array[i];
        [set addObject:user.userId];
    }
    
    NSMutableArray *friends = [[JY_UserObject sharedInstance] fetchAllUserFromLocal];
    __block NSMutableArray *letterResultArr = [NSMutableArray array];
    //排序 Person对象
    [BMChineseSort sortAndGroup:friends key:@"userNickname" finish:^(bool isSuccess, NSMutableArray *unGroupArr, NSMutableArray *sectionTitleArr, NSMutableArray<NSMutableArray *> *sortedObjArr) {
        if (isSuccess) {
            letterResultArr = unGroupArr;
        }
    }];
//    NSMutableArray *letterResultArr = [BMChineseSort sortObjectArray:friends Key:@"userNickname"];
    NSMutableSet *numSet = [NSMutableSet set];
    for (NSInteger i = 0; i < letterResultArr.count; i ++) {
        NSMutableArray *arr = letterResultArr[i];
        for (NSInteger j = 0; j < arr.count; j ++) {
            JY_UserObject *user = arr[j];
            if ([set containsObject:user.userId]) {
                [numSet addObject:[NSNumber numberWithInteger:(i + 1) * 100000 + j + 1]];
            }
        }

    }
    if (numSet.count > 0) {
        vc.set = numSet;
    }
    vc.existSet = set;
    
    [g_navigation pushViewController:vc animated:YES];
}

- (void)selectFriendsDelegate:(JY_SelectFriendsVC *)vc {
    
    [_array removeAllObjects];
    
    for (NSInteger i = 0; i < vc.userIds.count; i ++) {
        JY_UserObject *user = [[JY_UserObject alloc] init];
        user.userId = vc.userIds[i];
        user.userNickname = vc.userNames[i];
        
        [_array addObject:user];
    }
    self.labelUserNum.text = [NSString stringWithFormat:@"%@(%ld)",@"标签成员",_array.count];
    [self.tableView reloadData];
}

- (void)doneBtnAction:(UIButton *)btn {
    if (self.labelName.text.length <= 0) {
        [g_App showAlert:Localized(@"JX_EnterLabelName")];
        return;
    }
    
    self.labelName.text = [self.labelName.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (self.labelName.text.length <= 0) {
        [JY_MyTools showTipView:@"请输入有效的标签名"];
        return;
    }

//    if (self.array.count <= 0) {
//        [g_App showAlert:Localized(@"JX_AddMember")];
//        return;
//    }
    
    if (self.labelObj.groupId.length > 0) {
        if (![self.labelName.text isEqualToString:self.labelObj.groupName]) {
            [g_server friendGroupUpdate:self.labelObj.groupId groupName:self.labelName.text toView:self];
        }
        
        NSMutableString *userIdListStr = [NSMutableString string];
        for (NSInteger i = 0; i < self.array.count; i ++) {
            JY_UserObject *user = self.array[i];
            if (i == 0) {
                [userIdListStr appendFormat:@"%@", user.userId];
            }else {
                [userIdListStr appendFormat:@",%@", user.userId];
            }
        }
        
        JY_LabelObject *label = [[JY_LabelObject alloc] init];
        label.userId = self.labelObj.userId;
        label.groupId = self.labelObj.groupId;
        label.groupName = self.labelName.text;
        label.userIdList = userIdListStr;
        [label insert];
        [g_server friendGroupUpdateGroupUserList:label.groupId userIdListStr:userIdListStr toView:self];
        
    }else {
        [g_server friendGroupAdd:self.labelName.text toView:self];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _array.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    JY_UserObject *user = _array[indexPath.row];
    
    JY_Cell *cell=nil;
    NSString* cellName = @"JY_Cell";
    cell = [tableView dequeueReusableCellWithIdentifier:cellName];
    
    if(cell==nil){
        
        cell = [[JY_Cell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellName];
        [_table addToPool:cell];
        
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.title = [JY_UserObject getUserNameWithUserId:user.userId];
    cell.index = (int)indexPath.row;
    cell.tag = indexPath.row;
    cell.delegate = self;
    cell.didTouch = @selector(onHeadImage:);
    cell.timeLabel.hidden = YES;
    cell.userId = user.userId;
    [cell.lbTitle setText:cell.title];
    
    cell.headImageView.tag = indexPath.row;
    cell.headImageView.delegate = cell.delegate;
    cell.headImageView.didTouch = cell.didTouch;
    
    cell.dataObj = user;
    cell.isSmall = YES;
    [cell headImageViewImageWithUserId:nil roomId:nil];
    return cell;
}

-(void)onHeadImage:(UIView*)sender{
    NSMutableArray *array;

    array = _array;
    JY_UserObject *user = [array objectAtIndex:sender.tag];
    if([user.userId isEqualToString:FRIEND_CENTER_USERID] || [user.userId isEqualToString:CALL_CENTER_USERID])
        return;
    JY_UserInfoVC* vc = [JY_UserInfoVC alloc];
    vc.userId       = user.userId;
    vc.fromAddType = 6;
    vc = [vc init];
    [g_navigation pushViewController:vc animated:YES];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 59;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    JY_UserObject *userObj = [_array objectAtIndex:indexPath.row];
    
    userObj = [[JY_UserObject sharedInstance] getUserById:userObj.userId];
    
    if ([userObj.status intValue] == -1) {

        [JY_MyTools showTipView:@"此用户已被拉入黑名单"];
        return;
    }
    
    JY_ChatViewController *sendView=[JY_ChatViewController alloc];
    
    sendView.scrollLine = 0;
    sendView.title = userObj.userNickname;

    sendView.chatPerson = userObj;
    sendView = [sendView init];
    //    [g_App.window addSubview:sendView.view];
    [g_navigation pushViewController:sendView animated:YES];
    sendView.view.hidden = NO;
}


-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return YES;
}
- (NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewRowAction *deleteBtn = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:Localized(@"JX_Delete") handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
        
        JY_UserObject *user = _array[indexPath.row];
        [_array removeObject:user];
        
        [_table reloadData];
        
    }];

    
    return @[deleteBtn];
    
}


//服务器返回数据
-(void) didServerResultSucces:(JY_Connection*)aDownload dict:(NSDictionary*)dict array:(NSArray*)array1{
    [_wait stop];
    if ([aDownload.action isEqualToString:act_FriendGroupAdd] || [aDownload.action isEqualToString:act_FriendGroupUpdate]) {
        
        NSMutableString *userIdListStr = [NSMutableString string];
        for (NSInteger i = 0; i < self.array.count; i ++) {
            JY_UserObject *user = self.array[i];
            if (i == 0) {
                [userIdListStr appendFormat:@"%@", user.userId];
            }else {
                [userIdListStr appendFormat:@",%@", user.userId];
            }
        }
        
        
        JY_LabelObject *label = [[JY_LabelObject alloc] init];
        if (dict) {
            label.userId = dict[@"userId"];
            label.groupId = dict[@"groupId"];
            label.groupName = dict[@"groupName"];
        }else {
            label.userId = self.labelObj.userId;
            label.groupId = self.labelObj.groupId;
            label.groupName = self.labelName.text;
        }
        label.userIdList = userIdListStr;
        [label insert];
        
        [g_server friendGroupUpdateGroupUserList:label.groupId userIdListStr:userIdListStr toView:self];
        
    }
    
    if ([aDownload.action isEqualToString:act_FriendGroupUpdateGroupUserList]) {
    
        [g_notify postNotificationName:kLabelVCRefreshNotif object:nil];
        
        [self actionQuit];
    }
}



-(int) didServerResultFailed:(JY_Connection*)aDownload dict:(NSDictionary*)dict{
    [_wait hide];
    
    return show_error;
}

-(int) didServerConnectError:(JY_Connection*)aDownload error:(NSError *)error{//error为空时，代表超时
    [_wait hide];
    return show_error;
}

-(void) didServerConnectStart:(JY_Connection*)aDownload{
    [_wait start];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
