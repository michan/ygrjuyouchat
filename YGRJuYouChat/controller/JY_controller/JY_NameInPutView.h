//
//  JY_NameInPutView.h
//  TFJunYouChat
//
//  Created by TT on 2021/7/3.
//  Copyright © 2021 zengwOS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface JY_NameInPutView : UIViewController
<UITextFieldDelegate>{
    UITextField* _value;
    JY_NameInPutView* _pSelf;
    
}
@property (nonatomic,strong) NSString*  inputTitle;
@property (nonatomic, strong) UIFont *titleFont;
@property (nonatomic, strong) UIColor *titleColor;
@property (nonatomic,strong) NSString*  inputHint;
@property (nonatomic,strong) NSString*  inputBtn;
@property (nonatomic,strong) NSString*  inputText;
@property (nonatomic, weak) NSObject* delegate;
@property (nonatomic, assign) SEL        didTouch;
@end

NS_ASSUME_NONNULL_END
