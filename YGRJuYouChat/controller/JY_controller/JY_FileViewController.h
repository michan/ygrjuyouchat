//
//  JY_FileViewController.h
//  TFJunYouChat
//
//  Created by 1 on 17/7/4.
//  Copyright © 2020 zengwOS. All rights reserved.
//

#import "JY_admobViewController.h"


typedef NS_OPTIONS(NSInteger, JSFileVCType) {
   JSFileVCTypeGroup    = 1 << 0,
};


@interface JY_FileViewController : JY_TableViewController
@property (nonatomic,strong) roomData * room;

@end
