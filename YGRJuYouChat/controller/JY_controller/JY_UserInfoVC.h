//
//  JY_UserInfoVC.h
//  TFJunYouChat
//
//  Created by flyeagleTang on 14-6-10.
//  Copyright (c) 2014年 Reese. All rights reserved.
//

#import "JY_admobViewController.h"
#import "JY_SelectMenuView.h"
#import "JY_GoogleMapVC.h"
#import "JY_ChatViewController.h"


@class DMScaleTransition;

typedef void (^UserInfoVCChangeRemarkName)(NSString *remarkname);
@interface JY_UserInfoVC : JY_admobViewController<LXActionSheetDelegate>{
    UILabel* _name;
    UILabel* _remarkName;
    UILabel* _describe;
    UILabel* _workexp;
    UILabel* _city;
    UILabel* _dip;
    UILabel* _date;
    UILabel* _Fangshi;
    UILabel* _tel;
    UILabel* _lastTime;
    UILabel* _showNum;
    UILabel* _account;
    UILabel* _label;
    UIImageView* _sex;
    JY_Label *_labelLab;
    UILabel* _desLab;

    UISwitch *_messageFreeSwitch;
    UIView *_baseView;
    
    JY_ImageView *_describeImgV;
    JY_ImageView *_lifeImgV;
    JY_ImageView *_birthdayImgV;
    JY_ImageView *_lastTImgV;
    JY_ImageView *_showNImgV;
    JY_ImageView *_circleImgV;
    JY_ImageView *_desImgV;

    double _latitude;
    double _longitude;
    
    JY_ImageView* _head;
//    JY_ImageView* _body;

    int _friendStatus;
    NSString*   _xmppMsgId;
    UIButton* _btn;
    BOOL _deleleMode;
    NSMutableArray * _titleArr;
    DMScaleTransition *_scaleTransition;
    JY_GoogleMapVC *_gooMap;
    
    UIButton* _noTalkBtn;//禁言按钮
    

}

@property (nonatomic,strong) JY_UserObject* user;
@property (nonatomic,strong) UIView * bgBlackAlpha;
@property (nonatomic,strong) JY_SelectMenuView * selectView;
@property (nonatomic, assign) BOOL isJustShow;
@property (nonatomic, assign) BOOL isShowGoinBtn;
//。
@property (nonatomic, assign) BOOL isAdmin;
@property (nonatomic,strong) roomData* room;//当前所在房间聊天信息
@property (nonatomic, strong) memberData *currentData;
@property (nonatomic, copy) NSString *userId;

@property (nonatomic, assign) int fromAddType;

@property (nonatomic, weak) JY_ChatViewController *chatVC;
@property (nonatomic,copy) UILabel * invateName;


@property (nonatomic,strong) NSDictionary *dictData;


@property (nonatomic, copy) UserInfoVCChangeRemarkName changeNameBlock;//修改备注名
/** 不让他看 */
-(void)onCancelSee;
/** 移除黑名单 */
-(void)onDelBlack;
/** 加入黑名单 */
-(void)onAddBlack;
/** 设置备注 */
-(void)onRemark;
/** 删除好友 */
-(void)onDeleteFriend;
@end
