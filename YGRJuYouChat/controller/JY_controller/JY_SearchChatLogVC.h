//
//  JY_SearchChatLogVC.h
//  TFJunYouChat
//
//  Created by p on 2018/6/25.
//  Copyright © 2018年 Reese. All rights reserved.
//

#import "JY_TableViewController.h"

@interface JY_SearchChatLogVC : JY_TableViewController

@property (nonatomic, strong) JY_UserObject *user;
@property (nonatomic, assign) BOOL isGroup;

@end
