//
//  JY_ChatLogQRCodeVC.h
//  TFJunYouChat
//
//  Created by p on 2019/6/5.
//  Copyright © 2019年 Reese. All rights reserved.
//

#import "JY_admobViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface JY_ChatLogQRCodeVC : JY_admobViewController

@property (nonatomic, strong) NSMutableArray *selUserIdArray;

@end

NS_ASSUME_NONNULL_END
