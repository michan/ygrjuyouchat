//
//  JY_NewRoomVC.h
//  TFJunYouChat
//
//  Created by flyeagleTang on 14-6-10.
//  Copyright (c) 2014年 Reese. All rights reserved.
//

#import "JY_admobViewController.h"
@class roomData;
@class JY_RoomObject;

@interface JY_NewRoomVC : JY_admobViewController{
    UITextField* _desc;
    UILabel* _userName;
    UISwitch * _readSwitch;
    UISwitch * _publicSwitch;
    UISwitch * _secretSwitch;
    UILabel* _size;
    JY_RoomObject *_chatRoom;
    roomData* _room;
}

@property (nonatomic,strong) JY_RoomObject* chatRoom;
@property (nonatomic,strong) NSString* userNickname;
@property (nonatomic,strong) UITextField* roomName;
@property (nonatomic, assign) BOOL isAddressBook;
@property (nonatomic, strong) NSMutableArray *addressBookArr;

@end
