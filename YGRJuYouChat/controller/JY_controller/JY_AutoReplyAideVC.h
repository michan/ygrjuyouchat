//
//  JY_AutoReplyAideVC.h
//  TFJunYouChat
//
//  Created by p on 2019/5/14.
//  Copyright © 2019年 Reese. All rights reserved.
//

#import "JY_admobViewController.h"
#import "JY_GroupHeplerModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface JY_AutoReplyAideVC : JY_admobViewController

@property (nonatomic, strong) JY_HelperModel *model;
@property (nonatomic, strong) NSString *roomId;
@property (nonatomic, strong) NSString *roomJid;


@end

NS_ASSUME_NONNULL_END
