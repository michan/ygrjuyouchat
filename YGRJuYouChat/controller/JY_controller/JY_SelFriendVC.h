//
//  JY_SelFriendVC.h
//
//  Created by lifengye on 2020/09/03.
//  Copyright (c) 2014年 Reese. All rights reserved.
//

#import "JY_TableViewController.h"
#import <UIKit/UIKit.h>
@class menuImageView;
@class JY_RoomObject;

typedef NS_OPTIONS(NSInteger, ManMan_SelUserType) {
    ManMan_SelUserTypeGroupAT    = 1,
    ManMan_SelUserTypeSpecifyAdmin,
    ManMan_SelUserTypeSelMembers,
    ManMan_SelUserTypeSelFriends,
    ManMan_SelUserTypeCustomArray,
    ManMan_SelUserTypeDisAble,
    ManMan_SelUserTypeRoomTransfer,
    ManMan_SelUserTypeRoomInvisibleMan,  //设置隐身人
    ManMan_SelUserTypeRoomMonitorPeople, // 设置监控人
    ManMan_SelUserTypeAvoidRobRed //禁止抢红包
};

@interface JY_SelFriendVC: JY_TableViewController{
    NSMutableArray* _array;
    int _refreshCount;
    menuImageView* _tb;
    UIView* _topView;
    int _selMenu;
    
}
@property (nonatomic,strong) JY_RoomObject* chatRoom;
@property (nonatomic,strong) roomData* room;
@property (assign) BOOL isNewRoom;
@property (nonatomic, weak) NSObject* delegate;
@property (nonatomic, assign) SEL		didSelect;
@property (nonatomic,strong) NSMutableSet* set;
@property (nonatomic,strong) NSMutableArray* array;
//@property (nonatomic,strong) memberData* member;
@property (nonatomic,strong) NSSet * existSet;
@property (nonatomic,strong) NSSet * disableSet;
@property (nonatomic,assign) ManMan_SelUserType type;
@property (nonatomic, assign) BOOL isShowMySelf;

@property (nonatomic, assign) BOOL isForRoom;
@property (nonatomic, strong) JY_UserObject *forRoomUser;
@property (nonatomic, strong) NSMutableArray *userIds;
@property (nonatomic, strong) NSMutableArray *userNames;

@property (nonatomic, assign) BOOL isShowAlert;
@property (nonatomic, assign) SEL alertAction;
@end
