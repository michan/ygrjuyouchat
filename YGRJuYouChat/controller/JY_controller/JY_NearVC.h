//
//  JY_NearVC.h
//
//  Created by lifengye on 2020/09/03.
//  Copyright (c) 2014年 Reese. All rights reserved.
//

//#import "JY_TableViewController.h"
#import <UIKit/UIKit.h>

@class searchData;
@class JY_LocMapVC;
@class JY_GooMapVC;
@interface JY_NearVC: JY_admobViewController{
    NSMutableArray* _array;
    int _refreshCount;

    UIView* _topView;
    UIButton* _apply;
    UILabel* _lb;
    //searchData* _search;
    //BOOL _bNearOnly;
}
@property (nonatomic,strong)searchData *search;
@property (nonatomic,assign)BOOL bNearOnly;
@property (nonatomic,assign)int page;
@property (nonatomic,assign)BOOL isSearch;

@property (nonatomic,strong) JY_LocMapVC * mapVC;
@property (nonatomic,strong) JY_GooMapVC * goomapVC;

-(void)onSearch;
-(void)getServerData;
-(void)doSearch:(searchData*)p;
@end
