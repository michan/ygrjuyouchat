//
//  AppDelegate.h
//  TFJunYouChat
//
//  Created by lifengye on 2020/9/15.
//  Copyright © 2020 zengwOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JY_Navigation.h"
#import "JY_DidPushObj.h"
#import "JPUSHService.h"
#import "JY_MsgUtil.h"
#import "JY_MeetingObject.h"
#import "JY_emojiViewController.h"
#import "JY_Server.h"
#import "NumLockViewController.h"

  
@class JY_versionManage;
@class JY_Constant;
@class JY_UserObject; 
@class JY_CommonService;
@class ViewController;
@interface AppDelegate : UIResponder <UIApplicationDelegate,JPUSHRegisterDelegate>
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UIView *subWindow;
@property (nonatomic, strong) ViewController *watherViewC;
@property (nonatomic, strong) NumLockViewController *numLockVC;
@property (nonatomic, strong) NSString *QQ_LOGIN_APPID;
@property (nonatomic,strong)  JY_MeetingObject * jxMeeting;
@property (nonatomic,strong)  JY_Server* jxServer;
@property (nonatomic,strong)  JY_Constant* jxConstant;
@property (nonatomic, strong) JY_versionManage* config;
@property (strong, nonatomic) JY_emojiViewController* faceView;
@property (strong, nonatomic) JY_MainViewController *mainVc;
@property (strong, nonatomic) NSString * isShowRedPacket;

@property (strong, nonatomic) NSString * shortVideo;
@property (strong, nonatomic) NSString * videoMeeting;

@property (strong, nonatomic) NSString * isShowApplyForWithdrawal;
@property (strong, nonatomic) NSNumber * isOpenActivity;
@property (strong, nonatomic) NSString * activityUrl;
@property (strong, nonatomic) NSString * activityName;
@property (assign, nonatomic) double myMoney;
@property (nonatomic, strong) JY_CommonService *commonService;
@property (nonatomic, strong) JY_Navigation *navigation;
@property (nonatomic, assign) BOOL isShowDeviceLock;
@property (nonatomic, strong) JY_DidPushObj *didPushObj;
@property (nonatomic, copy)   NSArray *linkArray;
@property (nonatomic, strong) NSString *imgUrl;
@property (nonatomic, strong) NSArray *customerLinkListArray;
@property (nonatomic, strong) JY_SecurityUtil *securityUtil;
@property (nonatomic, strong) JY_PayServer *payServer;
@property (nonatomic, strong) JY_LoginServer *loginServer;
@property (nonatomic, strong) JY_MsgUtil *msgUtil;
-(void) showAlert: (NSString *) message;
-(UIAlertView *) showAlert: (NSString *) message delegate:(id)delegate;
- (UIAlertView *) showAlert: (NSString *) message delegate:(id)delegate tag:(NSUInteger)tag onlyConfirm:(BOOL)onlyConfirm;
- (void)copyDbWithUserId:(NSString *)userId;
-(void)showMainUI;
-(void)showLoginUI;
- (void)setupLunchADUrl:(NSString *)url link:(NSString *)link;
-(void)endCall;

@property (assign, nonatomic) BOOL is_iPhoneX;
 
@end
