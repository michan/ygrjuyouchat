#import <UIKit/UIKit.h>
@class JY_MoreSelectVC;
@protocol ManMan_MoreSelectVCDelegate <NSObject>
- (void)didSureBtn:(JY_MoreSelectVC *)moreSelectVC indexStr:(NSString *)indexStr;
@end
@interface JY_MoreSelectVC : UIViewController
@property (nonatomic, strong) NSString *indexStr;
@property (weak, nonatomic) id <ManMan_MoreSelectVCDelegate>delegate;
- (instancetype)initWithTitle:(NSString *)title dataArray:(NSArray *)dataArray;
@end
