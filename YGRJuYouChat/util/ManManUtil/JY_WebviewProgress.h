#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN
@interface JY_WebviewProgress : UIView
@property (nonatomic,strong) UIColor  *lineColor;
-(void)startLoadingAnimation;
-(void)endLoadingAnimation;
@end
NS_ASSUME_NONNULL_END
