//
//  JY_URL.h
//  TFJunYouChat
//
//  Created by zoja on 2021/7/19.
//  Copyright © 2021 zengwOS. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface JY_URL : NSObject
+  (NSDictionary *)dictionaryWithUrlString:(NSString *)urlStr;
@end

NS_ASSUME_NONNULL_END
