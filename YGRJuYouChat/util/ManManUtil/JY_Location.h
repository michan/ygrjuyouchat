#import <Foundation/Foundation.h>
@class JY_Location;
@protocol ManMan_LocationDelegate <NSObject>
- (void) location:(JY_Location *)location CountryCode:(NSString *)countryCode CityName:(NSString *)cityName CityId:(NSString *)cityId Address:(NSString *)address Latitude:(double)lat Longitude:(double)lon;
- (void)location:(JY_Location *)location getLocationWithIp:(NSDictionary *)dict;
- (void)location:(JY_Location *)location getLocationError:(NSError *)error;
@end
@interface JY_Location : NSObject
@property (nonatomic, weak) id<ManMan_LocationDelegate> delegate;
- (void) locationStart;
- (void) getLocationWithIp;
@end
