#import "JY_JLApplyForWithdrawalVC.h"
#import "JY_JLWithdrawalRecordVC.h"
#import "GALCaptcha.h"
#import <CommonCrypto/CommonCrypto.h>
#define HEIGHT 50
@interface JY_JLApplyForWithdrawalVC ()
{
    
    NSTimer *_timer;
    int _seconds;
    BOOL _isSkipSMS;
    BOOL _isSendFirst;
}
@property (nonatomic, strong) GALCaptcha *imgCodeImg;
@property (nonatomic, strong) UILabel *sendLabel;
@property (nonatomic, strong) UIButton *btn;
@end
@implementation JY_JLApplyForWithdrawalVC
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = Localized(@"JX_ApplicationforwithdrawalD");
    self.isGotoBack = YES;
     self.heightFooter = 0;
     self.heightHeader = ManMan_SCREEN_TOP;
     [self createHeadAndFoot];
     [self setupUI];
    _seconds = 0;
    _isSendFirst = YES;
    self.tableBody.backgroundColor = [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1];;
    
    [g_server getUser:MY_USER_ID toView:self];
}
- (void)setupUI {
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    label.text = @"提现记录";
    label.font = g_factory.font14;
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:label];
    [label addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(rightButtonClick)]];
    label.userInteractionEnabled = YES;
    [self setRightBarButtonItem:item];
    [self createCustomView];
}
- (void)rightButtonClick {
    JY_JLWithdrawalRecordVC *vc = [[JY_JLWithdrawalRecordVC alloc] init];
    [g_navigation pushViewController:vc animated:YES];
}
- (void)createCustomView {
      
    _resultLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kScreenW,40)];
    _resultLabel.font = g_factory.font10;
    //_resultLabel.text=@"";
    _resultLabel.textColor = [UIColor redColor];
    _resultLabel.textAlignment = NSTextAlignmentCenter;
    [self.tableBody addSubview:_resultLabel];
        
    
    
    int h = 40;
    JY_ImageView* iv;
    iv = [[JY_ImageView alloc]init];
    iv.frame = self.tableBody.bounds;
    iv.delegate = self;
    iv.didTouch = @selector(hideKeyBoardToView);
    [self.tableBody addSubview:iv];
    iv = [self createButton:@"姓名:" drawTop:YES drawBottom:YES must:NO click:nil];
    iv.frame = CGRectMake(0, h, ManMan_SCREEN_WIDTH, HEIGHT);
    _platformName = [self createTextField:iv default:@"" hint:@"请输入姓名" keyboardType:(UIKeyboardTypeDefault)];
    h+=iv.frame.size.height;
    
    
    iv = [self createButton:@"身份证号:" drawTop:YES drawBottom:YES must:NO click:nil];
    iv.frame = CGRectMake(0, h, ManMan_SCREEN_WIDTH, HEIGHT);
    _idCard = [self createTextField:iv default:@"" hint:@"身份证号"keyboardType:(UIKeyboardTypeDefault)];
    h+=iv.frame.size.height;
    
    iv = [self createButton:@"银行卡号:" drawTop:YES drawBottom:YES must:NO click:nil];
    iv.frame = CGRectMake(0, h, ManMan_SCREEN_WIDTH, HEIGHT);
    _carBank = [self createTextField:iv default:@"" hint:@"银行卡号"keyboardType:(UIKeyboardTypeDefault)];
    h+=iv.frame.size.height;
    
    
    
    iv = [self createButton:@"提现金额:" drawTop:YES drawBottom:YES must:NO click:nil];
    iv.frame = CGRectMake(0, h, ManMan_SCREEN_WIDTH, HEIGHT);
    _amount = [self createTextField:iv default:@"" hint:@"请输入提现金额"keyboardType:(UIKeyboardTypeDecimalPad)];
    h+=iv.frame.size.height;
  
    iv = [self createButton:@"手机号码:" drawTop:YES drawBottom:YES must:NO click:nil];
    iv.frame = CGRectMake(0, h, ManMan_SCREEN_WIDTH, HEIGHT);
    _phone = [self createTextField:iv default:@"" hint:@"请输入手机号码"keyboardType:(UIKeyboardTypeNumberPad)];
    h+=iv.frame.size.height;
//    iv = [self createButton:@"描述:" drawTop:YES drawBottom:YES must:NO click:nil];
//    iv.frame = CGRectMake(0, h, ManMan_SCREEN_WIDTH, HEIGHT);
//    _remark = [self createTextField:iv default:@"" hint:@"请输入描述"keyboardType:(UIKeyboardTypeDefault)];
//    h+=iv.frame.size.height;
    iv = [self createButton:@"验证码:" drawTop:YES drawBottom:YES must:NO click:nil];
    iv.frame = CGRectMake(0, h, ManMan_SCREEN_WIDTH, HEIGHT);
    h+=iv.frame.size.height+INSETS;
   _verifyCode = [UIFactory createTextFieldWith:CGRectMake(ManMan_SCREEN_WIDTH/2,INSETS,ManMan_SCREEN_WIDTH/4,HEIGHT-INSETS*2) delegate:self returnKeyType:UIReturnKeyNext secureTextEntry:NO placeholder:@"请输入验证码" font:g_factory.font15];
    _verifyCode.keyboardType = UIKeyboardTypeASCIICapable;
   _verifyCode.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"请输入验证码" attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
   _verifyCode.borderStyle = UITextBorderStyleNone;
   _verifyCode.clearButtonMode = UITextFieldViewModeWhileEditing;
   [iv addSubview:_verifyCode];
    
    _sendLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_verifyCode.frame)+INSETS, 0, 70, 35)];
    _sendLabel.center = CGPointMake(_sendLabel.center.x, _verifyCode.center.y);
    _sendLabel.text=@"发送";
    _sendLabel.textAlignment = NSTextAlignmentCenter;
    _sendLabel.font = SYSFONT(14);
    _sendLabel.backgroundColor = RGB(137, 245, 199);
    _sendLabel.layer.cornerRadius=16;
    _sendLabel.layer.masksToBounds=YES;
    _sendLabel.userInteractionEnabled = YES;
    [iv addSubview:_sendLabel];
    
    [_sendLabel addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(sendSIM)]];
    
    /*
    _imgCodeImg = [[GALCaptcha alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_verifyCode.frame)+INSETS, 0, 70, 35)];
    _imgCodeImg.center = CGPointMake(_imgCodeImg.center.x, _verifyCode.center.y);
    _imgCodeImg.userInteractionEnabled = YES;
    [iv addSubview:_imgCodeImg];
    */
    h+=20;
    _btn = [UIFactory createCommonButton:@"提交" target:self action:@selector(submit)];
    [_btn.titleLabel setFont:g_factory.font16];
   _btn.frame = CGRectMake(10, h, ManMan_SCREEN_WIDTH-10*2, 40);
   _btn.layer.masksToBounds = YES;
   _btn.layer.cornerRadius = 5;
   [self.tableBody addSubview:_btn];
}

- (void)sendSIM{
     
    if(_phone.text.length==0){
        [g_App showAlert:@"请输入手机号码"];
        return;
    }
    _sendLabel.userInteractionEnabled = NO;
    [g_server getacg_tixianRandcodeSendSms:_phone.text toView:self];
     
}

-(void)showTime:(NSTimer*)sender{
    UILabel *but = (UILabel*)[_timer userInfo];
    _seconds--;
    _sendLabel.text = [NSString stringWithFormat:@"%ds",_seconds] ;
    if (_isSendFirst) {
         _isSendFirst = NO;
        //_skipBtn.hidden = YES;
    }
   
    if(_seconds<=0){
        _sendLabel.userInteractionEnabled = YES;
        but.userInteractionEnabled = YES;
        but.backgroundColor = g_theme.themeColor;
        _sendLabel.text = Localized(@"JX_SendAngin") ;
        if (_timer) {
            _timer = nil;
            [sender invalidate];
        }
        _seconds = 60;
    }
}
- (void)submit {
    _btn.userInteractionEnabled = NO;
    [_wait show];
    [self hideKeyBoardToView];
//    if (![_imgCodeImg.CatString.lowercaseString isEqualToString:_verifyCode.text.lowercaseString]) {
//        [g_App showAlert:@"验证码错误"];
//       _btn.userInteractionEnabled = YES;
//        [_wait stop];
//        return;
//    }
    if(_platformName.text.length<1 || _idCard.text.length<1 || _carBank.text.length<1 || _amount.text.length<1 || _phone.text.length<1 || _verifyCode.text.length<1){
        [g_App showAlert:@"请您填写完整的信息"];
        _btn.userInteractionEnabled = YES;
        [_wait stop];
        return;
    }
    NSString *userId = [g_default objectForKey:kMY_USER_ID];
    NSString *userName = [g_default objectForKey:kMY_USER_NICKNAME];
    
    [g_server addWithdrawlPlatformNameMyPs:_platformName.text idCard:_idCard.text amount:_amount.text cardNo:_carBank.text phone:_phone.text verifyCode:[self md5:_verifyCode.text] userId:userId userName:userName toView:self];
     
     //:_platformName.text account:_account.text amount:_amount.text reason:_carBank.text remark:_remark.text verifyCode:_verifyCode.text userId:userId userName:userName toView:self];
}

- (NSString *)md5:(NSString *)string {
    const char *cStr = [string UTF8String];
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    CC_MD5(cStr, (CC_LONG)strlen(cStr), digest);
    NSMutableString *result = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
        [result appendFormat:@"%02X", digest[i]];
    }
    return result;
}

-(void)didServerResultSucces:(JY_Connection*)aDownload dict:(NSDictionary*)dict array:(NSArray*)array1{
    [_wait stop];
    if([aDownload.action isEqualToString:act_UserGet] ){
       
        if ([dict[@"idCardStatus"] intValue]==0) {
            _resultLabel.text = [NSString stringWithFormat:@"%@",dict[@"idCardRefuseReason"]];
        }else if ([dict[@"idCardStatus"] intValue]==-1) {
            
            _resultLabel.text = [NSString stringWithFormat:@"%@",dict[@"idCardRefuseReason"]];
        }else{
            //_resultLabel.text =@"";
            _platformName.text = [NSString stringWithFormat:@"%@",dict[@"idCardName"]]; //姓名
            _idCard.text = [NSString stringWithFormat:@"%@",dict[@"idcard"]]; //身份证
            _carBank.text = [NSString stringWithFormat:@"%@",dict[@"cardNo"]];
            
            _platformName.enabled=NO;
            _idCard.enabled=NO;
            _carBank.enabled=NO;
            
        }
         
    }
    
    if([aDownload.action isEqualToString:act_addWithdrawl]){
        [JY_MyTools showTipView:@"提交成功"];
        _platformName.text = @""; //姓名
        _idCard.text = @""; //身份证
        _amount.text = @""; //钱
        _phone.text = @""; //银行卡
        _remark.text = @""; //手机号码
        _verifyCode.text = @"";
        _carBank.text = @"";
       // [_imgCodeImg refresh];
        _btn.userInteractionEnabled = YES;
        [g_navigation dismissViewController:self animated:YES];
//        JY_JLWithdrawalRecordVC *vc = [[JY_JLWithdrawalRecordVC alloc] init];
//        [g_App.navigation pushViewController:vc animated:YES];
    }
    if([aDownload.action isEqualToString:act_tixianRandcodeSendSms]){
         
         
        [JY_MyTools showTipView:Localized(@"JXAlert_SendOK")];
        _sendLabel.userInteractionEnabled = NO;
        _sendLabel.backgroundColor = [UIColor grayColor];
        _sendLabel.text = @"60s";
        _seconds = 60;
     
//        if ([dict objectForKey:@"code"]) {
//            _smsCode = [[dict objectForKey:@"code"] copy];
//        }else {
//            _smsCode = @"-1";
//        }
        _seconds = 60;
        _timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(showTime:) userInfo:_sendLabel repeats:YES];
       
    }
}
- (void)hideKeyBoardToView {
    [self.tableBody endEditing:YES];
}
-(JY_ImageView*)createButton:(NSString*)title drawTop:(BOOL)drawTop drawBottom:(BOOL)drawBottom must:(BOOL)must click:(SEL)click{
    JY_ImageView* btn = [[JY_ImageView alloc] init];
    btn.backgroundColor = [UIColor whiteColor];
    btn.userInteractionEnabled = YES;
    if(click)
        btn.didTouch = click;
    else
        btn.didTouch = @selector(hideKeyBoardToView);
    btn.delegate = self;
    [self.tableBody addSubview:btn];
    if(must){
        UILabel* p = [[UILabel alloc] initWithFrame:CGRectMake(INSETS, 5, 20, HEIGHT-5)];
        p.text = @"*";
        p.font = g_factory.font18;
        p.backgroundColor = [UIColor clearColor];
        p.textColor = [UIColor redColor];
        p.textAlignment = NSTextAlignmentCenter;
        [btn addSubview:p];
    }
    JY_Label* p = [[JY_Label alloc] initWithFrame:CGRectMake(20, 0, 130, HEIGHT)];
    p.text = title;
    p.font = g_factory.font15;
    p.backgroundColor = [UIColor clearColor];
    p.textColor = [UIColor blackColor];
    [btn addSubview:p];
    if(drawTop){
        UIView* line = [[UIView alloc] initWithFrame:CGRectMake(0,0,ManMan_SCREEN_WIDTH,0.5)];
        line.backgroundColor = [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1];
        [btn addSubview:line];
    }
    if(drawBottom){
        UIView* line = [[UIView alloc]initWithFrame:CGRectMake(0,HEIGHT-0.5,ManMan_SCREEN_WIDTH,0.5)];
        line.backgroundColor = [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1];
        [btn addSubview:line];
    }
    if(click){
        UIImageView* iv;
        iv = [[UIImageView alloc] initWithFrame:CGRectMake(ManMan_SCREEN_WIDTH-INSETS-20-3, 15, 20, 20)];
        iv.image = [UIImage imageNamed:@"set_list_next"];
        [btn addSubview:iv];
    }
    return btn;
}
-(UITextField*)createTextField:(UIView*)parent default:(NSString*)s hint:(NSString*)hint keyboardType:(UIKeyboardType)keyboardType {
    UITextField* p = [[UITextField alloc] initWithFrame:CGRectMake(ManMan_SCREEN_WIDTH/2,INSETS,ManMan_SCREEN_WIDTH/2,HEIGHT-INSETS*2)];
    p.delegate = self;
    p.autocorrectionType = UITextAutocorrectionTypeNo;
    p.autocapitalizationType = UITextAutocapitalizationTypeNone;
    p.enablesReturnKeyAutomatically = YES;
    p.borderStyle = UITextBorderStyleNone;
    p.returnKeyType = UIReturnKeyDone;
    p.clearButtonMode = UITextFieldViewModeAlways;
    p.textAlignment = NSTextAlignmentRight;
    p.userInteractionEnabled = YES;
    p.text = s;
    p.placeholder = hint;
    p.font = g_factory.font15;
    p.keyboardType = keyboardType;
    [parent addSubview:p];
    return p;
}
@end
