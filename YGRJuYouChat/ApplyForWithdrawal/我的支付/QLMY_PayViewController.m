//
//  QLMY_PayViewController.m
//  TFJunYouChat
//
//  Created by Qian on 2021/11/18.
//  Copyright © 2021 zengwOS. All rights reserved.
//

#import "QLMY_PayViewController.h"
#import "JXp1a1y1WithBankMoney_VC.h"
#import "JXWithdrawal_VC.h"
#import "JY_BillingRecordsVC.h"
@interface QLMY_PayViewController ()

@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;
@end

@implementation QLMY_PayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.layer.cornerRadius = 22;
    self.view.layer.masksToBounds = YES;
    [g_notify addObserver:self selector:@selector(doRefresh:) name:kUpdateUserNotifaction object:nil];
    _moneyLabel.text = [NSString stringWithFormat:@"¥%.2f",g_App.myMoney];
    // Do any additional setup after loading the view from its nib.
}
-(void)doRefresh:(NSNotification *)notifacation{
    _moneyLabel.text = [NSString stringWithFormat:@"¥%.2f",g_App.myMoney];
}
- (IBAction)detil:(id)sender {
    JY_BillingRecordsVC * recordVC = [[JY_BillingRecordsVC alloc] init];
    [g_navigation pushViewController:recordVC animated:YES];
}
- (IBAction)back:(id)sender {
    [self actionQuit];
}
- (IBAction)chongzhi:(id)sender {
    JXp1a1y1WithBankMoney_VC * rechargeVC = [[JXp1a1y1WithBankMoney_VC alloc] init];
    [g_navigation pushViewController:rechargeVC animated:YES];
}
- (IBAction)tixian:(id)sender {
    JXWithdrawal_VC * cashWithVC = [[JXWithdrawal_VC alloc] init];
    //JY_CashWithDrawViewController * cashWithVC = [[JY_CashWithDrawViewController alloc] init];
    [g_navigation pushViewController:cashWithVC animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
