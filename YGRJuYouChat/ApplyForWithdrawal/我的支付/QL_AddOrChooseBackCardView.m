//
//  QL_AddOrChooseBackCardView.m
//  TFJunYouChat
//
//  Created by Qian on 2021/12/13.
//  Copyright © 2021 zengwOS. All rights reserved.
//

#import "QL_AddOrChooseBackCardView.h"

@interface QL_AddOrChooseBackCardView ()
@property (weak, nonatomic) IBOutlet UILabel *cardLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgWidth;
@property (weak, nonatomic) IBOutlet UIImageView *rightImg;

@end

@implementation QL_AddOrChooseBackCardView

- (IBAction)addClick:(id)sender {
    if (self.chooseBlock) {
        self.chooseBlock();
    }
}

- (void)setCardNumber:(NSString *)cardNumber{
    _cardNumber = cardNumber;
    
    if ([cardNumber isEqualToString:@"请选择银行卡"] || !cardNumber) {
        _cardLabel.textColor = HEXCOLOR(0xC0C0C0);
        _cardLabel.text = @"请选择银行卡";
        _imgWidth.constant = 30;
        _rightImg.hidden = YES;
    }else{
        _cardLabel.textColor = HEXCOLOR(0x303030);
        _imgWidth.constant = 0;
        _rightImg.hidden = NO;
        
        NSString *subTitle = [cardNumber substringFromIndex:cardNumber.length-4];
        
        _cardLabel.text =[NSString stringWithFormat:@"********%@",subTitle];// cardNumber;
    }
}
+(QL_AddOrChooseBackCardView *)initChooseBackCardView{
    return   [[NSBundle mainBundle] loadNibNamed:@"QL_AddOrChooseBackCardView" owner:nil options:nil].firstObject;

}
@end
