//
//  QL_AddOrChooseBackCardView.h
//  TFJunYouChat
//
//  Created by Qian on 2021/12/13.
//  Copyright © 2021 zengwOS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^ChooseClickBlock)(void);

@interface QL_AddOrChooseBackCardView : UIView

@property(nonatomic,copy)NSString *cardNumber;
@property(nonatomic, copy)ChooseClickBlock chooseBlock;

+(QL_AddOrChooseBackCardView *)initChooseBackCardView;

@end

NS_ASSUME_NONNULL_END
