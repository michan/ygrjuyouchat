//
//  QLAlertView.h
//  TFJunYouChat
//
//  Created by Qian on 2021/11/25.
//  Copyright © 2021 zengwOS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^SubmitClickBlock)(void);

@interface QLAlertNoSheetView : UIView
@property(nonatomic, copy)NSString *titleStr;
@property(nonatomic, copy)NSString *messageStr;

@property(nonatomic, copy)SubmitClickBlock submitBlock;
+(QLAlertNoSheetView *)initAlertView;
@end

NS_ASSUME_NONNULL_END
