//
//  QLAlertView.m
//  TFJunYouChat
//
//  Created by Qian on 2021/11/25.
//  Copyright © 2021 zengwOS. All rights reserved.
//

#import "QLAlertNoSheetView.h"

@interface QLAlertNoSheetView ()
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *message;
@property (weak, nonatomic) IBOutlet UIView *contentV;

@end

@implementation QLAlertNoSheetView
+(QLAlertNoSheetView *)initAlertView{
    return   [[NSBundle mainBundle] loadNibNamed:@"QLAlertNoSheetView" owner:nil options:nil].firstObject;

}
-(void)awakeFromNib{
    [super awakeFromNib];
    _contentV.layer.masksToBounds = YES;
    _contentV.layer.cornerRadius = 8;
    self.backgroundColor = [HEXCOLOR(0x000000) colorWithAlphaComponent:0.6];
  
    
}

- (IBAction)submitClick:(id)sender {
    [self removeFromSuperview];
    if (self.submitBlock) {
        self.submitBlock();
    }
}
- (void)setTitleStr:(NSString *)titleStr{
    _titleStr = titleStr;
    self.title.text = titleStr;
}
- (void)setMessageStr:(NSString *)messageStr{
    _messageStr = messageStr;
    _message.text = messageStr;
}
- (IBAction)cancelClick:(id)sender {
    [self removeFromSuperview];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
