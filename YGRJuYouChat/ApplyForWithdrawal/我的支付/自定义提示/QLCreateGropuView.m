//
//  QLCreateGropuView.m
//  TFJunYouChat
//
//  Created by Qian on 2021/11/26.
//  Copyright © 2021 zengwOS. All rights reserved.
//

#import "QLCreateGropuView.h"

@interface QLCreateGropuView ()
@property (weak, nonatomic) IBOutlet UIView *contentV;
@property (weak, nonatomic) IBOutlet UITextField *groupName;
@property (weak, nonatomic) IBOutlet UITextField *groupDesc;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (weak, nonatomic) IBOutlet UIView *bg1;
@property (weak, nonatomic) IBOutlet UIView *bg2;

@end

@implementation QLCreateGropuView
- (void)awakeFromNib{
    [super awakeFromNib];
    
    ViewRadius(_bg1, 5);
    ViewRadius(_bg2, 5);
    ViewRadius(_submitBtn, 5);
    ViewRadius(_contentV, 5);
    self.backgroundColor = [HEXCOLOR(0x000000) colorWithAlphaComponent:0.6];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap)];
    [self.contentV addGestureRecognizer:tap];
}
-(void)tap{
    [self.groupDesc resignFirstResponder];
    [self.groupName resignFirstResponder];
}
+(QLCreateGropuView *)initCreateGropuView{
    return   [[NSBundle mainBundle] loadNibNamed:@"QLCreateGropuView" owner:nil options:nil].firstObject;
}

- (IBAction)submitDidClick:(id)sender {
    if (self.groupName.text.length == 0) {
        [JY_MyTools showTipView:@"请输入有效的群组名称"];
        return;
    }
    if (self.submitBlock) {
        self.submitBlock(self.groupName.text, self.groupDesc.text);
    }
    [self removeFromSuperview];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
