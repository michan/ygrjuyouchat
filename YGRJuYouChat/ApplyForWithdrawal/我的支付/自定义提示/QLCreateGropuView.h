//
//  QLCreateGropuView.h
//  TFJunYouChat
//
//  Created by Qian on 2021/11/26.
//  Copyright © 2021 zengwOS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^SubmitClickBlock)(NSString *,NSString *);
@interface QLCreateGropuView : UIView
@property(nonatomic, copy)SubmitClickBlock submitBlock;
+(QLCreateGropuView *)initCreateGropuView;
@end

NS_ASSUME_NONNULL_END
