#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN
@interface JY_JLApplyForWithdrawalVC : JY_admobViewController {
    UITextField* _platformName;
    UITextField* _idCard;
    UITextField* _carBank;
    UITextField* _amount;
    UITextField* _phone;
    UITextField* _remark;
    UITextField* _verifyCode;
    UILabel* _resultLabel;
}
@end
NS_ASSUME_NONNULL_END
