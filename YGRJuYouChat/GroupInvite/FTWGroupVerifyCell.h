//
//  FTWGroupVerifyCell.h
//  FTWHandSameFirends
//
//  Created by JayLuo on 2020/10/28.
//  Copyright © 2020 Reese. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FTWGroupVerifyVC.h"
NS_ASSUME_NONNULL_BEGIN

@interface FTWGroupVerifyCell : UITableViewCell
- (void)setCellData:(FTWGroupVerifyVCListModel *)model complete:(void (^)(BOOL confirm, FTWGroupVerifyVCListModel *model)) complete;
@end

NS_ASSUME_NONNULL_END
