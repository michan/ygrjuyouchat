//
//  FTWGroupInviteVC.m
//  FTWHandSameFirends
//
//  Created by JayLuo on 2020/11/11.
//  Copyright © 2020 Reese. All rights reserved.
//

#import "FTWGroupInviteVC.h"
#import "FTWGroupVerifyCell.h"
#import "JY_ChatViewController.h"
#import "JY_XMPP.h"
#import "JY_RoomPool.h"


@interface FTWGroupInviteVC ()
@property (nonatomic, strong) NSDictionary *dataDict;
@property (nonatomic, strong) UIImageView  *headIMG;
@end

@implementation FTWGroupInviteVC

- (id)init
{
    self = [super init];
    if (self) {
        self.heightHeader = ManMan_SCREEN_TOP;
        self.heightFooter = 0;
        self.title = @"邀请详情";
        self.isGotoBack = YES;
        [self createHeadAndFoot];
        [self creatHeadViewUI];
        self.isShowFooterPull = NO;
        self.isShowHeaderPull = NO;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _table.backgroundColor = HEXCOLOR(0xFFFFFF);
    _table.separatorStyle = UITableViewCellSeparatorStyleNone;
    _table.separatorColor = [UIColor clearColor];
    [_table registerNib:[UINib nibWithNibName:@"FTWGroupVerifyCell" bundle:nil] forCellReuseIdentifier:@"FTWGroupVerifyCell"];
    
}

-(int)gethashCode:(NSString *)str {
    // 字符串转hash
    int hash = 0;
    for (int i = 0; i<[str length]; i++) {
        NSString *s = [str substringWithRange:NSMakeRange(i, 1)];
        char *unicode = (char *)[s cStringUsingEncoding:NSUnicodeStringEncoding];
        int charactorUnicode = 0;
        size_t length = strlen(unicode);
        for (int n = 0; n < length; n ++) {
            charactorUnicode += (int)((unicode[n] & 0xff) << (n * sizeof(char) * 8));
        }
        hash = hash * 31 + charactorUnicode;
    }
    return hash;
}

- (void)creatHeadViewUI {
     //背景View
    UIView  *backView = [[UIView alloc]init];
    backView.backgroundColor = HEXCOLOR(0xFFFFFF);
    backView.userInteractionEnabled=YES;
    backView.frame=CGRectMake(0, 0, ManMan_SCREEN_WIDTH, ManMan_SCREEN_HEIGHT);
    _table.tableHeaderView = backView;
    backView.autoresizingMask=UIViewAutoresizingNone;
    
    //头像
    UIImageView  *headIMG = [[UIImageView alloc] init];
    headIMG.layer.cornerRadius = 5;
    headIMG.layer.masksToBounds = YES;
    int hashCode = [self gethashCode:_dict[@"roomJid"]];
    int a = abs(hashCode % 10000);
    int b = abs(hashCode % 20000);
    
    NSString *urlStr = [NSString stringWithFormat:@"%@avatar/o/%d/%d/%@.jpg",g_config.downloadAvatarUrl,a,b,_dict[@"roomJid"]];
    
    [headIMG sd_setImageWithURL:[NSURL URLWithString:urlStr] placeholderImage:[UIImage imageNamed:@"groupImage"]];
    
    [backView addSubview:headIMG];
    _headIMG = headIMG;
    
    [headIMG mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(backView.mas_top).offset(100);
        make.centerX.mas_equalTo(backView.mas_centerX);
        make.width.mas_equalTo(72);
        make.height.mas_equalTo(72);
    }];
    
    UILabel  *groupNameLabel = [[UILabel alloc] init];
    groupNameLabel.text = _dict[@"roomName"];
    groupNameLabel.textColor = HEXCOLOR(0x333333);
    groupNameLabel.font=[UIFont systemFontOfSize:20 weight:UIFontWeightMedium];
    groupNameLabel.textAlignment = NSTextAlignmentCenter;
    groupNameLabel.numberOfLines = 0;
    [backView addSubview:groupNameLabel];
    [groupNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(headIMG.mas_bottom).mas_equalTo(25);
        make.centerX.mas_equalTo(backView.mas_centerX);
        make.left.equalTo(backView.mas_left).offset(47.5);
        make.right.equalTo(backView.mas_right).offset(-47.5);
    }];
    
    UILabel  *tipsLabel = [[UILabel alloc] init];
    if (!_msgCell.isMySend) {
        tipsLabel.text = @"你和群里其他人都不是朋友关系，请注意隐私安全。";
    }else {
        tipsLabel.text = [NSString stringWithFormat:@"%@和群里其他人都不是朋友关系，请注意隐私安全。", _msgCell.toUserName];
    }
    
    tipsLabel.textColor = HEXCOLOR(0x999999);
    tipsLabel.font=[UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
    tipsLabel.textAlignment = NSTextAlignmentCenter;
    tipsLabel.numberOfLines = 0;
    [backView addSubview:tipsLabel];
    [tipsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(groupNameLabel.mas_bottom).mas_equalTo(10);
        make.left.equalTo(backView.mas_left).offset(47.5);
        make.right.equalTo(backView.mas_right).offset(-47.5);
        make.centerX.mas_equalTo(backView.mas_centerX);
    }];
    
    if (!_msgCell.isMySend) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setTitle:@"加入群聊" forState:UIControlStateNormal];
        [button.titleLabel setFont:[UIFont systemFontOfSize:16 weight:UIFontWeightSemibold]];
        button.backgroundColor =HEXCOLOR(0x08C163);
        [button setTitleColor:HEXCOLOR(0xFFFFFF) forState:UIControlStateNormal];
        button.layer.cornerRadius = 5;
        button.layer.masksToBounds=YES;
        [backView addSubview:button];
        
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(tipsLabel.mas_bottom).mas_equalTo(200);
            make.right.mas_equalTo(backView.mas_right).mas_offset(-112);
            make.left.mas_equalTo(backView.mas_left).mas_offset(112);
            make.height.mas_equalTo(40);
        }];
        [button addTarget:self action:@selector(joinRoom) forControlEvents:UIControlEventTouchUpInside];
    }
}


- (void)joinRoom {
    NSDictionary *dict = _dict;
    JY_MessageObject *msgCell = _msgCell;
    if (_type == 1) {
            [g_server roomJoinV2:dict[@"roomId"] fromUserId:msgCell.fromUserId msgId:msgCell.messageId toView:self];
        }else {
            [g_server joinRoomAuditApply:dict[@"roomId"] fromUserId:msgCell.fromUserId toUserId:msgCell.toUserId type:@"10010" msgId:msgCell.messageId toView:self];
            // 发消息给群主或者管理员
            // 单聊
            /*
             {"userIds":"10007849","userNames":"卓凯","roomJid":"4a3e8f135c814e2e83bc2dbcd778c876","isInvite":"0","reason":""}
             */
            JY_MessageObject *msg = [[JY_MessageObject alloc] init];
            msg.fromUserId = msgCell.fromUserId;
            msg.toUserId = [NSString stringWithFormat:@"%@", dict[@"roomCreator"]];
            msg.fromUserName = msgCell.fromUserName;
            msg.toUserName = @"";
            msg.timeSend = [NSDate date];
            msg.type = @(916);
            msg.content = @"";
            NSString *roomJid = dict[@"roomJid"];
            // 0 普通人邀请你。1 主动入群 （扫码， 推荐， 附近）
            NSDictionary *dict = @{
                @"userIds" : MY_USER_ID,
                @"userNames" : MY_USER_NAME,
                @"roomJid" : roomJid,
                @"isInvite" : @"0",
                @"reason" : @"",
            };
            NSError *error = nil;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];

            NSString *jsonStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            msg.objectId = jsonStr;
    //        [msg insert:msg.toUserId];
            [g_xmpp sendMessage:msg roomName:nil];
        }
}

-(void)showChatView{
    [_wait stop];
    NSDictionary * dict = _dataDict;
    
    //老房间:
    JY_RoomObject *chatRoom = [[JY_XMPP sharedInstance].roomPool joinRoom:[dict objectForKey:@"jid"] title:[dict objectForKey:@"name"] lastDate:nil isNew:YES];
    
    roomData * roomdata = [[roomData alloc] init];
    [roomdata getDataFromDict:dict];
    
    JY_ChatViewController *sendView=[JY_ChatViewController alloc];
    
    sendView.title = [dict objectForKey:@"name"];
    sendView.roomJid = [dict objectForKey:@"jid"];
    sendView.roomId = [dict objectForKey:@"id"];
    sendView.chatRoom = chatRoom;
    sendView.room = roomdata;
    
    
    JY_UserObject * userObj = [[JY_UserObject alloc]init];
    userObj.userId = [dict objectForKey:@"jid"];
    userObj.showRead = [dict objectForKey:@"showRead"];
    userObj.userNickname = [dict objectForKey:@"name"];
    userObj.showMember = [dict objectForKey:@"showMember"];
    userObj.allowSendCard = [dict objectForKey:@"allowSendCard"];
    userObj.chatRecordTimeOut = roomdata.chatRecordTimeOut;
    userObj.talkTime = [dict objectForKey:@"talkTime"];
    userObj.allowInviteFriend = [dict objectForKey:@"allowInviteFriend"];
    userObj.allowUploadFile = [dict objectForKey:@"allowUploadFile"];
    userObj.allowConference = [dict objectForKey:@"allowConference"];
    userObj.allowSpeakCourse = [dict objectForKey:@"allowSpeakCourse"];
    userObj.isNeedVerify = [dict objectForKey:@"isNeedVerify"];
    
    sendView.chatPerson = userObj;
    sendView = [sendView init];
    [g_navigation pushViewController:sendView animated:YES];
    
    dict = nil;
}



#pragma mark  -------------------服务器返回数据--------------------
-(void) didServerResultSucces:(JY_Connection*)aDownload dict:(NSDictionary*)dict array:(NSArray*)array1{
    [_wait stop];
    [_header endRefreshing];
    if ([aDownload.action isEqualToString:act_roomJoinV2]) {
        [_wait stop];
        _dataDict = dict;
        [self showChatView];
    }
    
    if ([aDownload.action isEqualToString:act_joinRoomAuditApply]) {
        [_wait stop];
        if (dict) {
            _dataDict = dict;
            [self showChatView];
        }else {
            [g_App showAlert:@"申请成功,等待群主和管理员同意"];
        }
        
    }
}

-(int) didServerResultFailed:(JY_Connection*)aDownload dict:(NSDictionary*)dict{

    [_wait stop];
    [_header endRefreshing];
    if ([aDownload.action isEqualToString:act_joinRoomAuditApply]) {
        [g_App showAlert:dict[@"resultMsg"]];    }
     return show_error;
}

-(int) didServerConnectError:(JY_Connection*)aDownload error:(NSError *)error{//error为空时，代表超时
    [_wait stop];
    [_header endRefreshing];
    return hide_error;
}

-(void) didServerConnectStart:(JY_Connection*)aDownload{
    [_wait start];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FTWGroupVerifyCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FTWGroupVerifyCell" forIndexPath:indexPath];
   
    return  cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70.5;
}

@end
