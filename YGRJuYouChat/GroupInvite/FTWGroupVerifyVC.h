//
//  FTWGroupVerifyVC.h
//  FTWHandSameFirends
//
//  Created by JayLuo on 2020/10/27.
//  Copyright © 2020 Reese. All rights reserved.
//

#import "JY_TableViewController.h"

NS_ASSUME_NONNULL_BEGIN
@interface FTWGroupVerifyVCListModel :NSObject
@property (nonatomic , assign) NSInteger              createTime;
@property (nonatomic , assign) NSInteger              fromUserId;
@property (nonatomic , copy) NSString              * fromUserName;
@property (nonatomic , copy) NSString              * id;
@property (nonatomic , copy) NSString              * roomId;
@property (nonatomic , assign) NSInteger              state;
@property (nonatomic , assign) NSInteger              toUserId;
@property (nonatomic , copy) NSString              * toUserName;
@property (nonatomic , assign) NSInteger              type;
@property (nonatomic , assign) NSInteger              updateTime;

@end


@interface FTWGroupVerifyVC : JY_TableViewController
@property (nonatomic, strong) NSString *roomId;
@end

NS_ASSUME_NONNULL_END
