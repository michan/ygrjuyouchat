//
//  FTWGroupVerifyCell.m
//  FTWHandSameFirends
//
//  Created by JayLuo on 2020/10/28.
//  Copyright © 2020 Reese. All rights reserved.
//

#import "FTWGroupVerifyCell.h"


@interface FTWGroupVerifyCell()
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *msgLabel;
@property (weak, nonatomic) IBOutlet UIButton *ignoreButton;
@property (weak, nonatomic) IBOutlet UIButton *confirmButton;
@property (copy, nonatomic) void (^complete) (BOOL, FTWGroupVerifyVCListModel *);
@property (strong, nonatomic) FTWGroupVerifyVCListModel *model;
@end

@implementation FTWGroupVerifyCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setFrame:(CGRect)frame {
    frame.size.height -= 0.5;
    [super setFrame:frame];
}

- (IBAction)ignoreClick:(UIButton *)sender {
    if (self.complete) {
        self.complete(NO, _model);
    }
}

- (IBAction)confirmClick:(UIButton *)sender {
    if (self.complete) {
        self.complete(YES, _model);
    }
}

- (void)setCellData:(FTWGroupVerifyVCListModel *)model complete:(void (^)(BOOL, FTWGroupVerifyVCListModel * _Nonnull model))complete {
    self.complete = complete;
    self.model = model;
    NSString *contentString = @"";
    // 10001 通过附近的群加入新成员 10002 通过搜索群加入新成员  10003 通过推荐的群加入新成员 10010邀请人入群
    if ([model.fromUserName isEqualToString:model.toUserName]) {
        if (model.type == 10001) {
            contentString = [NSString stringWithFormat:@"%@ 通过附近的群加入群聊",model.fromUserName];
        }else if (model.type == 10002) {
            contentString = [NSString stringWithFormat:@"%@ 通过搜索群加入群聊",model.fromUserName];
        }else if (model.type == 10003) {
            contentString = [NSString stringWithFormat:@"%@ 通过推荐的群加入群聊",model.fromUserName];
        }
    }else {
        if (model.type == 10010) {
            contentString = [NSString stringWithFormat:@"%@ 通过 %@ 的邀请加入群聊",model.toUserName, model.fromUserName];
        }
    }
    _msgLabel.text = contentString;
    _timeLabel.text = [self getDateStringWithTimeStr:[NSString stringWithFormat:@"%ld", model.createTime]];

//    -1 忽略 0 未审核 1 通过 2 拒绝
    if (model.state == -1) {
        _ignoreButton.hidden = YES;
        _confirmButton.hidden = NO;
        [_confirmButton setTitle:@"已忽略" forState:(UIControlStateNormal)];
        _confirmButton.enabled = NO;
    } else if (model.state == 0) {
        _ignoreButton.hidden = NO;
        _confirmButton.hidden = NO;
        [_confirmButton setTitle:@"通过" forState:(UIControlStateNormal)];
        _confirmButton.enabled = YES;
        
    } else if (model.state == 1) {
        _ignoreButton.hidden = YES;
        _confirmButton.hidden = NO;
        [_confirmButton setTitle:@"已通过" forState:(UIControlStateNormal)];
        _confirmButton.enabled = NO;
        
    } else if (model.state == 2) {
        _ignoreButton.hidden = YES;
        _confirmButton.hidden = NO;
        [_confirmButton setTitle:@"已拒绝" forState:(UIControlStateNormal)];
        _confirmButton.enabled = NO;
        
    }
    
    
}

// 时间戳转时间,时间戳为13位是精确到毫秒的，10位精确到秒
- (NSString *)getDateStringWithTimeStr:(NSString *)str {
    NSTimeInterval time=[str doubleValue]/1;//传入的时间戳str如果是精确到毫秒的记得要/1000
    NSDate *detailDate=[NSDate dateWithTimeIntervalSince1970:time];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init]; //实例化一个NSDateFormatter对象
    //设定时间格式,这里可以设置成自己需要的格式
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *currentDateStr = [dateFormatter stringFromDate: detailDate];
    return currentDateStr;
}
@end
