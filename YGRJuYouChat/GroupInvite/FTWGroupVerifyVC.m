//
//  FTWGroupVerifyVC.m
//  FTWHandSameFirends
//
//  Created by JayLuo on 2020/10/27.
//  Copyright © 2020 Reese. All rights reserved.
//

#import "FTWGroupVerifyVC.h"
#import "FTWGroupVerifyCell.h"

@implementation FTWGroupVerifyVCListModel
@end

@interface FTWGroupVerifyVC ()
@property (nonatomic, strong) NSMutableArray <FTWGroupVerifyVCListModel *>*dataArray;
@end

@implementation FTWGroupVerifyVC

- (id)init
{
    self = [super init];
    if (self) {
        self.heightHeader = ManMan_SCREEN_TOP;
        self.heightFooter = 0;
        self.title = @"验证消息";
        self.isGotoBack = YES;
        [self createHeadAndFoot];
        _dataArray = [NSMutableArray array];
        UIButton* btn = [UIFactory createButtonWithTitle:@"清空" titleFont:[UIFont systemFontOfSize:14] titleColor:HEXCOLOR(0x333333) normal:@"清空" highlight:@"清空"];
        [btn addTarget:self action:@selector(clear) forControlEvents:(UIControlEventTouchUpInside)];
        btn.frame = CGRectMake(ManMan_SCREEN_WIDTH -15-50, ManMan_SCREEN_TOP-15-20, 50, 20);
        [self.tableHeader addSubview:btn];
        
        UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(0, ManMan_SCREEN_TOP, ManMan_SCREEN_WIDTH, 50)];
        [self.view addSubview:backView];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.separatorColor = [UIColor clearColor];
    [self.tableView registerNib:[UINib nibWithNibName:@"FTWGroupVerifyCell" bundle:nil] forCellReuseIdentifier:@"FTWGroupVerifyCell"];
    __weak typeof(self) weakSelf = self;
    MJRefreshHeaderView *header = [MJRefreshHeaderView header];
    header.scrollView = self.tableView; // 或者tableView
    header.beginRefreshingBlock = ^(MJRefreshBaseView *refreshView) {
        [weakSelf getData];
    };
    [header beginRefreshing];
    _header = header;
}

- (void)getData {
    [g_server JoinRoomAuditPageSize:30 page:0 roomId:_roomId toView:self];
}

#pragma mark  -------------------服务器返回数据--------------------
-(void) didServerResultSucces:(JY_Connection*)aDownload dict:(NSDictionary*)dict array:(NSArray*)array1{
    [_wait stop];
    [_header endRefreshing];
    if ([aDownload.action isEqualToString:act_JoinRoomAuditPage]) {
        
        _dataArray = [FTWGroupVerifyVCListModel mj_objectArrayWithKeyValuesArray:array1];
        
    }
    if ([aDownload.action isEqualToString:act_JoinRoomAudit]) {
//        [_header beginRefreshing];
        [_dataArray removeAllObjects];
        [self.tableView reloadData];
    }
    [self.tableView reloadData];
}

-(int) didServerResultFailed:(JY_Connection*)aDownload dict:(NSDictionary*)dict{

    [_wait stop];
    [_header endRefreshing];
    
    if ([aDownload.action isEqualToString:act_JoinRoomAudit]) {
        [_header beginRefreshing];
        return show_error;
    }
    
     return hide_error;
}

-(int) didServerConnectError:(JY_Connection*)aDownload error:(NSError *)error{//error为空时，代表超时
    [_wait stop];
    [_header endRefreshing];
    return hide_error;
}

-(void) didServerConnectStart:(JY_Connection*)aDownload{
    [_wait start];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FTWGroupVerifyCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FTWGroupVerifyCell" forIndexPath:indexPath];
    FTWGroupVerifyVCListModel *model = _dataArray[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell setCellData:model complete:^(BOOL confirm, FTWGroupVerifyVCListModel * _Nonnull model) {
         [self verifyByConfirm:confirm dict:model];
    }];
    return  cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70.5;
}


- (void)verifyByConfirm:(BOOL)confirm dict:(FTWGroupVerifyVCListModel *)dict {
    // 确定or忽略
    if (confirm) {
        [g_server JoinRoomAudit:dict.id state:@"1" roomId:dict.roomId toView:self];
    } else {
        [g_server JoinRoomAudit:dict.id state:@"-1" roomId:dict.roomId toView:self];
    }
}

// 清空列表
- (void)clear {
     [g_server JoinRoomAudit:nil state:@"3" roomId:_roomId toView:self];
}



@end
