//
//  FTWGroupInviteVC.h
//  FTWHandSameFirends
//
//  Created by JayLuo on 2020/11/11.
//  Copyright © 2020 Reese. All rights reserved.
//

#import "JY_TableViewController.h"
#import "JY_MessageObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface FTWGroupInviteVC : JY_TableViewController
@property (nonatomic, strong) JY_MessageObject *msgCell;
@property (nonatomic, strong) NSDictionary *dict;
@property (nonatomic, assign) NSInteger type;
@end

NS_ASSUME_NONNULL_END
