//
//  TFJunYou_FAQCenterDetailItemVC.h
//  TFJunYouChat
//
//  Created by JayLuo on 2021/2/8.
//  Copyright © 2021 zengwOS. All rights reserved.
//

#import "JY_admobViewController.h"
@class TFJunYou_FAQCenterDetailModel;

NS_ASSUME_NONNULL_BEGIN

@interface JY_TFJunYou_FAQCenterDetailItemVC : JY_admobViewController
@property(nonatomic, strong) TFJunYou_FAQCenterDetailModel *model;
@end

NS_ASSUME_NONNULL_END
