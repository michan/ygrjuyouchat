//
//  TFJunYou_FreezeNoCodeSendVc.h
//  TFJunYouChat
//
//  Created by os on 2021/2/6.
//  Copyright © 2021 zengwOS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger,JY_FreezeType) {
    FreezeTypeBlocking, // 冻结账号
    FreezeTypeUnblocked, // 解冻账号
    FreezeTypeSafetyLock, //安全锁
};

@interface TFJunYou_FreezeNoCodeSendVc : JY_admobViewController
@property(nonatomic, strong) NSString *IDCard;
@property(nonatomic, strong) NSString *phone;
@property(nonatomic, strong) NSString *name;
@property(nonatomic, assign) JY_FreezeType type;

@end

NS_ASSUME_NONNULL_END
