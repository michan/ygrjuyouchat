//
//  TFJunYou_FreezeNoVc.h
//  TFJunYouChat
//
//  Created by os on 2021/2/6.
//  Copyright © 2021 zengwOS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TFJunYou_openFreezeNoVc : JY_admobViewController
/** 默认是解冻账号   1是解冻资金 2是冻结资金  */
@property(nonatomic,assign) NSInteger type;

@end

NS_ASSUME_NONNULL_END
