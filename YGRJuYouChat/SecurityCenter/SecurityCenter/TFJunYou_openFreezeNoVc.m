//
//  TFJunYou_FreezeNoVc.m
//  TFJunYouChat
//
//  Created by os on 2021/2/6.
//  Copyright © 2021 zengwOS. All rights reserved.
//

#import "TFJunYou_openFreezeNoVc.h"
#import "TFJunYou_FreezeNoCodeVc.h"
#import "QLAlertNoSheetView.h"
#import "JY_MyMoneyViewController.h"

@interface TFJunYou_openFreezeNoVc ()

@end

@implementation TFJunYou_openFreezeNoVc

- (void)viewDidLoad {
    [super viewDidLoad];
    self.heightHeader = ManMan_SCREEN_TOP;
    self.heightFooter = 0;
    self.isGotoBack   = YES;
    
    [self createHeadAndFoot];
    
    self.title =self.type==0?@"解冻聚友号":@"安全锁";
    self.tableBody.backgroundColor = THEMEBACKCOLOR;
    
//    1、为了账户资金余额安全，冻结资金后，您账户内资金余额将被锁定，任何人无法转出。 2、解冻是需要接受短信验证码，确保您绑定的手机号码无误、有效。
    
    UIImageView *iconImg = [[UIImageView alloc]init];
    iconImg.image = [UIImage imageNamed:self.type==0?@"冻结2":@"安全锁"];
    [self.tableBody addSubview:iconImg];
    [iconImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.tableBody.mas_centerX);
        make.top.mas_equalTo(30);
    }];
    
    UILabel *titleLabel = [[UILabel alloc]init];
    titleLabel.text =self.type!=0?@"1、为了账户资金余额安全，冻结资金后，您账户内资金余额将被锁定，任何人无法转出。": @"安全问题解决后，您可以申请解冻聚友账号";
    titleLabel.numberOfLines = 0;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor grayColor];
    titleLabel.font = [UIFont systemFontOfSize:16 weight:UIFontWeightMedium];
    [self.tableBody addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(-15);
        make.centerX.mas_equalTo(self.tableBody.mas_centerX);
        make.top.mas_equalTo(iconImg.mas_bottom).mas_offset(30);
    }];
    
    UILabel *subLabel = [[UILabel alloc]init];
    subLabel.text =self.type!=0?@"2、解冻是需要接受短信验证码，确保您绑定的手机号码无误、有效。": @"检查手机有没有被植入病毒\n不要随便向外人泄漏个人信息";
    subLabel.textAlignment = NSTextAlignmentCenter;
    subLabel.textColor = [UIColor lightGrayColor];
    subLabel.numberOfLines = 0;
    subLabel.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
    [self.tableBody addSubview:subLabel];
    [subLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(60);
        make.right.mas_equalTo(-60);
        make.centerX.mas_equalTo(self.tableBody.mas_centerX);
        make.top.mas_equalTo(titleLabel.mas_bottom).mas_offset(20);
    }];
    
    UIButton *sumitBtn = [[UIButton alloc]init];
    sumitBtn.layer.cornerRadius = 5;
    sumitBtn.layer.masksToBounds = YES;
    sumitBtn.backgroundColor = HEXCOLOR(0x05D168);
    
    NSString *btnTitle =self.type==0?@"开始解冻":self.type==1?@"解冻资金":@"锁定资金";
    [sumitBtn setTitle:btnTitle forState:0];
    [self.tableBody addSubview:sumitBtn];
    [sumitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(20);
        make.right.mas_equalTo(-20);
        make.height.mas_equalTo(44);
        make.top.mas_equalTo(subLabel.mas_bottom).mas_offset(20);
    }];
    
    [sumitBtn addTarget:self action:@selector(sumitBtnClick) forControlEvents:UIControlEventTouchUpInside];
}
 
- (void)sumitBtnClick {
    if (self.type==2) {
        [_wait show];
        QLAlertNoSheetView *sheetV = [QLAlertNoSheetView initAlertView];
        sheetV.frame = g_window.frame;
        sheetV.titleStr = @"提示";
        sheetV.messageStr = @"确定锁定资金？";
        [g_window addSubview:sheetV];
        sheetV.submitBlock = ^{
            [g_server get_act_ApiFreezeOptAutnCode:nil toView:self];
        };
        return;
    }
    if (self.type==1) {
        TFJunYou_FreezeNoCodeSendVc *vc =[TFJunYou_FreezeNoCodeSendVc new];
        vc.type = FreezeTypeSafetyLock;
        [g_navigation pushViewController:vc animated:YES];
        return;
    }
    TFJunYou_FreezeNoCodeVc *vc = [TFJunYou_FreezeNoCodeVc new];
    vc.type = FreezeTypeUnblocked;
    [g_navigation pushViewController:vc animated:YES];
}


-(void) didServerResultSucces:(JY_Connection*)aDownload dict:(NSDictionary*)dict array:(NSArray*)array1{
    [_wait hide];
    if ([aDownload.action containsString:act_ApiFreezeOpt]) {
        [g_App showAlert:dict[@"resultObject"]];
        [g_navigation popToViewController:[JY_MyMoneyViewController class] animated:YES];
    }
}


@end
