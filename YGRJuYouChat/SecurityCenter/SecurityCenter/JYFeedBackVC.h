//
//  JYFeedBackVC.h
//  WonBey
//
//  Created by JayLuo on 2019/4/11.
//  Copyright © 2020 Hunan Liaocheng Technology Co., Ltd.All rights reserved.
//

#import "JY_admobViewController.h"

typedef NS_ENUM(NSUInteger, FeedBackType) {
    FeedBackTypeComplaints, // 投诉
    FeedBackTypeSuggestion, // 意见反馈
};
@interface JYFeedBackVC : JY_admobViewController
@property(nonatomic, assign) FeedBackType type;
/** 投诉群 */
@property(nonatomic, copy)NSString *roomId;
/** 投诉好友 */
@property(nonatomic, copy)NSString *userId;
@end
