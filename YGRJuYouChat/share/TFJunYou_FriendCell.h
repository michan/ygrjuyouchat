//
//  TFJunYou_FriendCell.h
//  share
//
//  Created by 1 on 2019/3/21.
//  Copyright © 2019年 Reese. All rights reserved.
//

#import <UIKit/UIKit.h>


@class TFJunYou_ShareUser;

@interface TFJunYou_FriendCell : UITableViewCell


- (void)setDataWithUser:(TFJunYou_ShareUser *)user;

@end
