//
//  JY_VolumeView.m
//  TFJunYouChat
//
//  Created by flyeagleTang on 14-7-24.
//  Copyright (c) 2014年 Reese. All rights reserved.
//

#import "JY_VolumeView.h"
#import "UIImage-Extensions.h"

@interface JY_VolumeView ()
@property (nonatomic, strong) UIImageView *voiceImgV;

@property (nonatomic, strong) UIImageView *voiceRightImgV;
@property (nonatomic, strong) UILabel *tintLabel;

@property (nonatomic, strong) UIImageView *cancelImgV;


@end

@implementation JY_VolumeView
@synthesize volume;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.layer.cornerRadius = 6;
        self.layer.masksToBounds = YES;
        
        UIView* v = [[UIView alloc]initWithFrame:self.bounds];
        v.backgroundColor = HEXCOLOR(0xFFFFFF);
//        v.alpha = 0.6;
        [self addSubview:v];
        
        //椭圆下方的托架
        _volume = [[JY_ImageView alloc]initWithFrame:CGRectMake(0, 0, 60, 60)];
        _volume.image =[UIImage imageNamed:@"yy_recorder"] ;//[UIImage imageNamed:@"pub_recorder"];
        [self addSubview:_volume];
        _volume.center = v.center;
//        [_volume release];
        
//        //椭圆白色背景
//        JY_ImageView * inputBackground = [[JY_ImageView alloc]initWithFrame:CGRectMake(9, 1, 34, 66)];//20,1,66,132
//        inputBackground.image = [UIImage imageNamed:@"pub_microphone_volumeBg"];
//        inputBackground.layer.cornerRadius = 17;
//        inputBackground.clipsToBounds = YES;
//        [_volume addSubview:inputBackground];
////        [inputBackground release];
//
//        //椭圆红色背景
//        _input = [[JY_ImageView alloc]initWithFrame:CGRectMake(-0.2, 0, 34, 70)];
//        _input.image = [UIImage imageNamed:@"pub_microphone_volume"];
//        [inputBackground addSubview:_input];
        
        
        _cancelImgV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
        _cancelImgV.image =[UIImage imageNamed:@"yy_recorder"];// [UIImage imageNamed:@"voice_cancel"];
        _cancelImgV.hidden = YES;
        _cancelImgV.center = v.center;
        [self addSubview:_cancelImgV];

     

        CGSize size = [@"手指上滑,取消发送" boundingRectWithSize:CGSizeMake(MAXFLOAT, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:15]} context:nil].size;
        
        _tintLabel = [[UILabel alloc]init];//WithFrame:CGRectMake((SCREEN_WIDTH-size.width/2), 25, size.width, 13)];
        _tintLabel.text =@"松手发送";// Localized(@"JXVolumeView_CancelSend");
        _tintLabel.font = SYSFONT(15);
        _tintLabel.textColor =HEXCOLOR(0x303030);// [UIColor whiteColor];
        _tintLabel.numberOfLines = 0;
        _tintLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_tintLabel];
        [_tintLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self).offset(25);
            make.centerX.mas_equalTo(self.mas_centerX);
        }];
        
        
        
        _voiceImgV = [[UIImageView alloc] init];//ithFrame:CGRectMake(CGRectGetMaxX(_volume.frame)+15, 20, _volume.frame.size.width, _volume.frame.size.height)];
        _voiceImgV.image = [UIImage imageNamed:@"vr_1"];
        [self addSubview:_voiceImgV];
        [_voiceImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(_tintLabel.mas_left);
            make.centerY.mas_equalTo(_tintLabel.mas_centerY);
            make.width.mas_equalTo(45);
            make.height.mas_equalTo(15);
        }];
        _voiceRightImgV= [[UIImageView alloc] init];
        _voiceRightImgV.image = [UIImage imageNamed:@"vn_1"];
        [self addSubview:_voiceRightImgV];
        [_voiceRightImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_tintLabel.mas_right).offset(5);
            make.centerY.mas_equalTo(_tintLabel.mas_centerY);
            make.width.mas_equalTo(45);
            make.height.mas_equalTo(15);
        }];
        
    }
    return self;
}

- (void)setIsWillCancel:(BOOL)isWillCancel {
    _isWillCancel = isWillCancel;
    if (_isWillCancel) {
        _cancelImgV.hidden = NO;
        _volume.hidden = YES;
        _voiceImgV.hidden = YES;
        _voiceRightImgV.hidden = YES;
        _tintLabel.text = @"手指上滑,取消发送";
    }else {
        _cancelImgV.hidden = YES;
        _volume.hidden = NO;
        _voiceImgV.hidden = NO;
        _voiceRightImgV.hidden = NO;
        _tintLabel.text =@"松手发送";// Localized(@"JXVolumeView_CancelSend");
    }
}



-(void)setVolume:(double)value{
    volume = value;
    float n = value;
    float m = 1.0-n;
    
    _input.frame  =  CGRectMake(-0.2, 70*m -5 , 34, 70);
    _input.image = [UIImage imageNamed:@"pub_microphone_volume"];
    NSLog(@"---n:%f  m:%f",n,m);
    
    int g = n*10+1 > 7 ? 7 : n*10+1;
    
    _voiceImgV.image = [UIImage imageNamed:[NSString stringWithFormat:@"vr_%d", g]];
    _voiceRightImgV.image = [UIImage imageNamed:[NSString stringWithFormat:@"vn_%d", g]];
}

//截取部分图像,无用
-(UIImage*)getSubImage:(CGRect)rect
{
    CGImageRef subImageRef = CGImageCreateWithImageInRect(_input.image.CGImage, rect);
    CGRect smallBounds = CGRectMake(0, 0, CGImageGetWidth(subImageRef), CGImageGetHeight(subImageRef));
    
    UIGraphicsBeginImageContext(smallBounds.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextDrawImage(context, smallBounds, subImageRef);
    UIImage* smallImage = [UIImage imageWithCGImage:subImageRef];
    UIGraphicsEndImageContext();
    
    return smallImage;
}

-(void)show{
    [g_window addSubview:self];
}

-(void)hide{
    [self removeFromSuperview];
}

@end
