//
//  recordAudioVC.h
//  TFJunYouChat
//
//  Created by flyeagleTang on 14-6-24.
//  Copyright (c) 2014年 Reese. All rights reserved.
//

#import "JY_admobViewController.h"
#import "JY_AudioPlayer.h"
@class MixerHostAudio;
@class mediaOutput;

@interface recordAudioVC : UIViewController{
    MixerHostAudio* _mixRecorder;
    mediaOutput* outputer;
    IBOutlet UISegmentedControl* mFxType;

    BOOL _startOutput;
   
    JY_ImageView* _input;
    JY_ImageView* _volume;
    JY_ImageView* _btnPlay;
    JY_ImageView* _btnRecord;
    JY_ImageView* _btnBack;
    JY_ImageView* _btnDel;
    JY_ImageView* _btnEnter;
    JY_ImageView* _iv;
    UIScrollView* _effectType;
    UILabel* _lb;
    NSTimer* _timer;
    JY_AudioPlayer* _player;
    recordAudioVC* _pSelf;
}
@property(nonatomic,assign) int timeLen;
@property(nonatomic,weak) id delegate;
@property(assign) SEL didRecord;
@property (nonatomic, strong) NSString* outputFileName;

@end
