//
//  JY_VolumeView.h
//  TFJunYouChat
//
//  Created by flyeagleTang on 14-7-24.
//  Copyright (c) 2014年 Reese. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JY_VolumeView : UIView{
    JY_ImageView* _input;
    JY_ImageView* _volume;

}
@property(nonatomic,assign) double volume;

@property (nonatomic, assign) BOOL isWillCancel;

-(void)show;
-(void)hide;
@end
