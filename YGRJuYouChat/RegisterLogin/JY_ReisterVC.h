//
//  JY_ReisterVC.h
//  TFJunYouChat
//
//  Created by TT on 2021/8/18.
//  Copyright © 2021 zengwOS. All rights reserved.
//

#import "JY_admobViewController.h"

NS_ASSUME_NONNULL_BEGIN
/// 类型
typedef enum :NSInteger {
    ReisterPageStyleNone = 0,// 注册
    ReisterPageStyleEditAccout // 修改账号
}ReisterPageStyle;
@interface JY_ReisterVC : JY_admobViewController{
    UITextField* _area;
    UITextField* _phone;
    UITextField* _code;
    UITextField* _pwd;
    UITextField* _inviteCode;
    UIButton* _send;
//    NSString* _smsCode;//验证码后台不再返回，本地不做验证
//    NSString* _imgCodeStr;
    NSString* _areaIDStr;
    NSString* _areaIDNum;
    NSString* _phoneStr;
    int _seconds;
}
@property (nonatomic, assign) BOOL isThirdLogin;
@property (nonatomic, assign) int type;
- (id)initWithReisterPageStyle:(ReisterPageStyle)pageStyle;

@end

NS_ASSUME_NONNULL_END
