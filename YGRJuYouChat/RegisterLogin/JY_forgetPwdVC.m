#import "JY_forgetPwdVC.h"
#import "JY_TelAreaListVC.h"
#import "JY_UserObject.h"
#import "JY_loginVC.h"
#import "MD5Util.h"
#define HEIGHT 50
@interface JY_forgetPwdVC () <UITextFieldDelegate>
{
   UIButton *_areaCodeBtn;
    UIButton *_newAreaCodeBtn;
   NSTimer* timer;
   JY_UserObject *_user;
   UIImageView * _imgCodeImg;
   UITextField *_imgCode;   
   UIButton * _graphicButton;
}
/// 手机号视图
@property (weak, nonatomic) IBOutlet UIView *phoneView;
@property (weak, nonatomic) IBOutlet UITextField *phoneTF;
@property (weak, nonatomic) IBOutlet UIButton *accLoginBtn;
@property (weak, nonatomic) IBOutlet UIButton *forgetpwdBtn;
@property (weak, nonatomic) IBOutlet UIView *forgetLineView;
@property (weak, nonatomic) IBOutlet UIButton *forgetBackBtn;



@property (weak, nonatomic) IBOutlet UIView *codeView;
@property (weak, nonatomic) IBOutlet UILabel *codeToPhoneLabel;
@property (weak, nonatomic) IBOutlet UITextField *p1;
@property (weak, nonatomic) IBOutlet UITextField *p2;
@property (weak, nonatomic) IBOutlet UITextField *p3;
@property (weak, nonatomic) IBOutlet UITextField *p4;
@property (weak, nonatomic) IBOutlet UITextField *p5;
@property (weak, nonatomic) IBOutlet UITextField *p6;
@property (nonatomic, copy) NSArray <UITextField *>*codeTFArray;
@property (nonatomic, assign) NSInteger enterCount;


@property (weak, nonatomic) IBOutlet UIView *enterPwdView;
@property (weak, nonatomic) IBOutlet UITextField *enterPwdTF;


@property (weak, nonatomic) IBOutlet UIView *surePwdView;
@property (weak, nonatomic) IBOutlet UITextField *surePwdTF;

@end
@implementation JY_forgetPwdVC
- (IBAction)surePhoneNextBtnClick:(id)sender {
    if (self.phoneTF.text.length <= 0) {
        [g_App showAlert:@"请输入手机号"];
        return;
    }
    self.codeView.hidden = NO;
    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:@"短信发送至 " attributes:@{NSForegroundColorAttributeName:HEXCOLOR(0x303030)}];
    NSAttributedString *a1 = [[NSAttributedString alloc] initWithString:self.phoneTF.text attributes:@{NSForegroundColorAttributeName:HEXCOLOR(0x05D168)}];
    [attr appendAttributedString:a1];
    self.codeToPhoneLabel.attributedText = attr;
    
    self.codeTFArray = @[_p1,_p2,_p3,_p4,_p5,_p6];
    for (UITextField *tf in self.codeTFArray) {
        tf.delegate = self;
        [tf addTarget:self action:@selector(textFieldDidChanged:) forControlEvents:UIControlEventEditingChanged];
        tf.layer.cornerRadius = 5;
        if (tf.tag == 10) {
            tf.backgroundColor = [UIColor whiteColor];
            self.enterCount = 0;
            [tf becomeFirstResponder];
        }else{
            tf.backgroundColor = HEXCOLOR(0xf0f0f0);
        }
        tf.text = @"";
        tf.tintColor = HEXCOLOR(0x05D168);
    }
    self.codeView.layer.cornerRadius = 5;
    self.codeView.layer.masksToBounds = 5;
    
    [self onSend];
}
-(void)textFieldDidChanged:(UITextField *)textField{
    if ([self.codeTFArray containsObject:textField]) {
        self.enterCount = textField.tag - 10;
        if (textField.text.length > 1) {
           textField.text = [textField.text substringToIndex:1];
        }
        if (textField.text.length == 1 && self.enterCount!=5) {
            textField.backgroundColor = HEXCOLOR(0x05D168);
            
            if (self.codeTFArray[self.enterCount+1].text.length == 1) {
                [textField resignFirstResponder];
            }else{
                [self.codeTFArray[self.enterCount+1] becomeFirstResponder];
            }
            
        }
        if (textField.tag == 15 && textField.text.length == 1) {
            [textField resignFirstResponder];
            textField.backgroundColor = HEXCOLOR(0x05D168);
        }
        
    }
}
- (IBAction)sureCodeNextBtnClick:(id)sender {
    
    if (self.isPayPWD) {
        [self newUpdata:sender];
        return;
    }
    NSString *codeStr = @"";
    for (UITextField *tf in self.codeTFArray) {
        codeStr = [codeStr stringByAppendingString:tf.text];
    }
    if([codeStr length]<6){
        if([codeStr length]<=0){
            [g_App showAlert:@"请输入短信验证码"];
            return;
        }
        [g_App showAlert:Localized(@"inputPhoneVC_MsgCodeNotOK")];
        return;
    }
    self.enterPwdView.hidden = NO;
}
- (IBAction)sureEnterPwdBtnClick:(id)sender {
    if (self.enterPwdTF.text.length < 6 || self.enterPwdTF.text.length > 12) {
        [g_App showAlert:@"请输入6-12位密码"];
        return;
    }
    self.surePwdView.hidden = NO;
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.phoneTF resignFirstResponder];
    [self.enterPwdTF resignFirstResponder];
    [self.surePwdTF resignFirstResponder];
    for (UITextField *tf in self.codeTFArray) {
        [tf resignFirstResponder];
    }
}
- (IBAction)surePwdBtnClick:(id)sender {
    if (![self.surePwdTF.text isEqualToString:self.enterPwdTF.text]) {
        [g_App showAlert:@"确认密码失败"];
        return;
    }
    [self newUpdata:sender];
}

- (id)init
{
   self = [super init];
   if (self) {
   }
   return self;
}
-(void)setNewViewUI{
    if (self.isPayPWD || self.isModify) {
        self.accLoginBtn.hidden = YES;
        self.forgetpwdBtn.hidden = YES;
        self.forgetLineView.hidden = YES;
        self.forgetBackBtn.hidden = NO;
    }
    UIView *l = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 25,35)];
    UIImageView *imgv = [[UIImageView alloc] initWithFrame:CGRectMake(0, 10, 15, 15)];
    [l addSubview:imgv];
    imgv.image = [UIImage imageNamed:@"q_mima_icon"];
    self.enterPwdTF.leftView =l;
    self.enterPwdTF.leftViewMode =UITextFieldViewModeAlways;
    
    UIView *l2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 25,35)];
    UIImageView *imgv2 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 10, 15, 15)];
    [l2 addSubview:imgv2];
    imgv2.image = [UIImage imageNamed:@"q_mima_icon"];
    self.surePwdTF.leftView =l2;
    self.surePwdTF.leftViewMode =UITextFieldViewModeAlways;
    
    self.surePwdView.hidden = YES;
    self.enterPwdView.hidden = YES;
    self.codeView.hidden = YES;
    
    self.phoneTF.delegate = self;
    NSString *areaStr;
    if (![g_default objectForKey:kMY_USER_AREACODE]) {
       areaStr = @"+86";
    } else {
       areaStr = [NSString stringWithFormat:@"+%@",[g_default objectForKey:kMY_USER_AREACODE]];
    }
    _newAreaCodeBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 35)];
    [_newAreaCodeBtn setTitle:areaStr forState:UIControlStateNormal];
    _newAreaCodeBtn.titleLabel.font = SYSFONT(18);
    [_newAreaCodeBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_newAreaCodeBtn setImage:[UIImage imageNamed:@"down_arrow_black"] forState:UIControlStateNormal];
    _newAreaCodeBtn.custom_acceptEventInterval = 1.0f;
    [_newAreaCodeBtn addTarget:self action:@selector(areaCodeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self resetBtnEdgeInsets:_newAreaCodeBtn];
    UIView *phoneLeft = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50,35)];
    [phoneLeft addSubview:_newAreaCodeBtn];
    self.phoneTF.leftView = phoneLeft;
    self.phoneTF.leftViewMode =UITextFieldViewModeAlways;
    self.phoneTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    
}
- (void)viewDidLoad
{
   [super viewDidLoad];
   if (self.isModify) {
      self.title = Localized(@"JX_UpdatePassWord");
   }else{
      self.title = Localized(@"JX_ForgetPassWord");
   }
    [self setNewViewUI];
   _user = [JY_UserObject sharedInstance];
   _seconds = 0;
   self.isGotoBack   = YES;
   self.heightFooter = 0;
   self.heightHeader = ManMan_SCREEN_TOP;
   [self createHeadAndFoot];
    if (self.isPayPWD) {
        self.tableBody.hidden = NO;
        self.tableHeader.hidden = NO;
        self.title = @"";
    }else{
        self.tableBody.hidden = YES;
        self.tableHeader.hidden = YES;
    }
    self.view.backgroundColor = HEXCOLOR(0xffffff);
    self.tableHeader.backgroundColor = HEXCOLOR(0xffffff);
   self.tableBody.backgroundColor = HEXCOLOR(0xffffff);
   UIView *baseView = [[UIView alloc] initWithFrame:CGRectMake(15, 74, ManMan_SCREEN_WIDTH-30, SCREEN_HEIGHT - 74)];
   baseView.backgroundColor = [UIColor whiteColor];
   baseView.layer.cornerRadius = 7.f;
   baseView.layer.masksToBounds = YES;
   [self.tableBody addSubview:baseView];
   CGFloat W = baseView.frame.size.width;
    self.navigationController.navigationBarHidden = YES;
    int n = INSETS;
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, n, SCREEN_WIDTH, 20)];
    [baseView addSubview:label];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = @"忘记密码";
    label.font = g_factory.font20;
    n+=65;
//   int n = INSETS;
   if (!_phone) {
      _phone = [UIFactory createTextFieldWith:CGRectMake(15, n, W-30, HEIGHT) delegate:self returnKeyType:UIReturnKeyNext secureTextEntry:NO placeholder:Localized(@"JX_InputPhone") font:g_factory.font16];
      _phone.clearButtonMode = UITextFieldViewModeWhileEditing;
      _phone.borderStyle = UITextBorderStyleNone;
      [baseView addSubview:_phone];
      [self showLine:_phone];
      UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 67, HEIGHT)];
      _phone.leftView = leftView;
      _phone.leftViewMode = UITextFieldViewModeAlways;
      NSString *areaStr;
      if (![g_default objectForKey:kMY_USER_AREACODE]) {
         areaStr = @"+86";
      } else {
         areaStr = [NSString stringWithFormat:@"+%@",[g_default objectForKey:kMY_USER_AREACODE]];
      }
      _areaCodeBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 41, HEIGHT)];
      [_areaCodeBtn setTitle:areaStr forState:UIControlStateNormal];
      _areaCodeBtn.titleLabel.font = SYSFONT(15);
      [_areaCodeBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
      [_areaCodeBtn setImage:[UIImage imageNamed:@"down_arrow_black"] forState:UIControlStateNormal];
      _areaCodeBtn.custom_acceptEventInterval = 1.0f;
      [_areaCodeBtn addTarget:self action:@selector(areaCodeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
      [self resetBtnEdgeInsets:_areaCodeBtn];
      [leftView addSubview:_areaCodeBtn];
      UIView *verticalLine = [[UIView alloc] initWithFrame:CGRectMake(_areaCodeBtn.frame.size.width+7, HEIGHT/2 - 8, LINE_WH, 16)];
      verticalLine.backgroundColor = THE_LINE_COLOR;
      [leftView addSubview:verticalLine];
   }
   n = n+HEIGHT + 15;
   if (!self.isModify) {
      _imgCode = [UIFactory createTextFieldWith:CGRectMake(15, n, W-25-82 - 15, HEIGHT) delegate:self returnKeyType:UIReturnKeyNext secureTextEntry:NO placeholder:Localized(@"JX_inputImgCode") font:g_factory.font16];
      _imgCode.clearButtonMode = UITextFieldViewModeWhileEditing;
      _imgCode.borderStyle = UITextBorderStyleNone;
      [baseView addSubview:_imgCode];
      [self showLine:_imgCode];
      _imgCode.layer.masksToBounds = YES;
      _imgCode.layer.cornerRadius = 4;
      [self createLeftViewWithImage:[UIImage imageNamed:@"verify"] superView:_imgCode];
       
       UIView *bgv = [[UIView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 82 - 25 - 15, 0, 82, 32)];
       [baseView addSubview:bgv];
       bgv.backgroundColor = HEXCOLOR(0xE2E2E2);
       ViewRadius(bgv, 6);
       bgv.center = CGPointMake(bgv.center.x,_imgCode.center.y);
       
      _imgCodeImg = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMinX(bgv.frame)+6, 0, 48, 17)];
      _imgCodeImg.center = CGPointMake(_imgCodeImg.center.x, _imgCode.center.y);
      _imgCodeImg.userInteractionEnabled = YES;
       _imgCodeImg.backgroundColor = [UIColor clearColor];
      [baseView addSubview:_imgCodeImg];
      _graphicButton = [UIButton buttonWithType:UIButtonTypeCustom];
      _graphicButton.frame = CGRectMake(CGRectGetMaxX(_imgCodeImg.frame)+4, 7, 15, 15);
      _graphicButton.center = CGPointMake(_graphicButton.center.x,_imgCode.center.y);
//      [_graphicButton setBackgroundImage:[UIImage imageNamed:@"menu_refresh_normal"] forState:UIControlStateNormal];
       [_graphicButton setImage:[UIImage imageNamed:@"menu_refresh_normal"] forState:UIControlStateNormal];;
      [_graphicButton addTarget:self action:@selector(refreshGraphicAction:) forControlEvents:UIControlEventTouchUpInside];
      [baseView addSubview:_graphicButton];
       
      
      n = n+HEIGHT + 15;
      _code = [[UITextField alloc] initWithFrame:CGRectMake(15, n, W-110-30, HEIGHT)];
      _code.delegate = self;
      _code.autocorrectionType = UITextAutocorrectionTypeNo;
      _code.autocapitalizationType = UITextAutocapitalizationTypeNone;
      _code.enablesReturnKeyAutomatically = YES;
      _code.font = g_factory.font16;
      _code.returnKeyType = UIReturnKeyDone;
      _code.clearButtonMode = UITextFieldViewModeWhileEditing;
      _code.placeholder = Localized(@"JX_InputMessageCode");
      [baseView addSubview:_code];
      [self showLine:_code];
      [self createLeftViewWithImage:[UIImage imageNamed:@"code"] superView:_code];
      _send = [UIFactory createButtonWithTitle:@"发送"
                                     titleFont:g_factory.font16
                                    titleColor:HEXCOLOR(0x05D168)
                                        normal:nil
                                     highlight:nil ];
      [_send addTarget:self action:@selector(sendSMS) forControlEvents:UIControlEventTouchUpInside];
//       _send.backgroundColor = RGB(89, 202, 194);;
       
      _send.titleLabel.font = SYSFONT(16);
      _send.frame = CGRectMake(W-105-15, n+HEIGHT/2-20, 105, 40);
      _send.layer.cornerRadius = 7.f;
      _send.layer.masksToBounds = YES;
      [baseView addSubview:_send];
      n = n+HEIGHT + 15;
   }
   if (self.isModify) {
      _oldPwd = [[UITextField alloc] initWithFrame:CGRectMake(15,n,W-30,HEIGHT)];
      _oldPwd.delegate = self;
      _oldPwd.autocorrectionType = UITextAutocorrectionTypeNo;
      _oldPwd.autocapitalizationType = UITextAutocapitalizationTypeNone;
      _oldPwd.enablesReturnKeyAutomatically = YES;
      _oldPwd.returnKeyType = UIReturnKeyDone;
      _oldPwd.clearButtonMode = UITextFieldViewModeWhileEditing;
      _oldPwd.placeholder = Localized(@"JX_InputOldPassWord");
      _oldPwd.secureTextEntry = YES;
      _oldPwd.font = g_factory.font16;
      [baseView addSubview:_oldPwd];
      [self showLine:_oldPwd];
      [self createLeftViewWithImage:[UIImage imageNamed:@"password"] superView:_oldPwd];
      n = n+HEIGHT;
   }
   if (!self.isPayPWD) {
      _pwd = [[UITextField alloc] initWithFrame:CGRectMake(15,n,W-30,HEIGHT)];
      _pwd.delegate = self;
      _pwd.autocorrectionType = UITextAutocorrectionTypeNo;
      _pwd.autocapitalizationType = UITextAutocapitalizationTypeNone;
      _pwd.enablesReturnKeyAutomatically = YES;
      _pwd.returnKeyType = UIReturnKeyDone;
      _pwd.clearButtonMode = UITextFieldViewModeWhileEditing;
      _pwd.placeholder = Localized(@"JX_InputNewPassWord");
      _pwd.secureTextEntry = YES;
      _pwd.font = g_factory.font16;
      [baseView addSubview:_pwd];
      [self showLine:_pwd];
      [self createLeftViewWithImage:[UIImage imageNamed:@"password"] superView:_pwd];
      n = n+HEIGHT;
      _repeat = [[UITextField alloc] initWithFrame:CGRectMake(15,n,W-30,HEIGHT)];
      _repeat.delegate = self;
      _repeat.autocorrectionType = UITextAutocorrectionTypeNo;
      _repeat.autocapitalizationType = UITextAutocapitalizationTypeNone;
      _repeat.enablesReturnKeyAutomatically = YES;
      _repeat.returnKeyType = UIReturnKeyDone;
      _repeat.clearButtonMode = UITextFieldViewModeWhileEditing;
      _repeat.placeholder = Localized(@"JX_ConfirmNewPassWord");
      _repeat.secureTextEntry = YES;
      _repeat.font = g_factory.font16;
      [self createLeftViewWithImage:[UIImage imageNamed:@"password"] superView:_repeat];
      [baseView addSubview:_repeat];
      [self showLine:_repeat];
      n = n+HEIGHT;
   }
   n += 30 + 15;
    UIButton* _btn =UIButton.new;// [UIFactory createCommonButton:@"修改密码" target:self action:@selector(onClick:)];
    [_btn setTitle:@"修改密码" forState:UIControlStateNormal];
    [_btn addTarget:self action:@selector(onClick:) forControlEvents:UIControlEventTouchUpInside];
   [_btn.titleLabel setFont:g_factory.font16];
   _btn.layer.masksToBounds = YES;
   _btn.layer.cornerRadius = 5.0f;
    [_btn setBackgroundImage:nil forState:UIControlStateNormal];
    _btn.backgroundColor =  HEXCOLOR(0x05D168);
   _btn.frame = CGRectMake(15, n, W-30, 40);
   [baseView addSubview:_btn];
   CGRect frame = baseView.frame;
   frame.size.height = CGRectGetMaxY(_btn.frame)+30;
   baseView.frame = frame;
   _phone.text = g_myself.phone;
   if (self.isModify) {
      _phone.enabled = NO;
   }else{
      if (_phone.text.length > 0) {
         [self getImgCodeImg];
      }
   }
}
- (void)didReceiveMemoryWarning
{
   [super didReceiveMemoryWarning];
}
-(void)refreshGraphicAction:(UIButton *)button{
   [self getImgCodeImg];
}
-(void)getImgCodeImg{
   if(_phone.text.length > 0){
      NSString *areaCode = [_areaCodeBtn.titleLabel.text stringByReplacingOccurrencesOfString:@"+" withString:@""];
      NSString * codeUrl = [g_server getImgCode:_phone.text areaCode:areaCode];
      NSURLRequest * request = [NSURLRequest requestWithURL:[NSURL URLWithString:codeUrl] cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:10.0];
      [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
         if (!connectionError) {
            UIImage * codeImage = [UIImage imageWithData:data];
            if (codeImage != nil) {
               _imgCodeImg.image = codeImage;
            }else{
               [g_App showAlert:Localized(@"JX_ImageCodeFailed")];
            }
         }else{
            NSLog(@"%@",connectionError);
            [g_App showAlert:connectionError.localizedDescription];
         }
      }];
   }else{
   }
}
#pragma mark------验证
-(void)newUpdata:(UIButton *)btn{
    btn.enabled = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
       btn.enabled = YES;
    });
    NSString *codeStr = @"";
    for (UITextField *tf in self.codeTFArray) {
        codeStr = [codeStr stringByAppendingString:tf.text];
    }
    
        
    if([self.phoneTF.text length]<= 0){
       [g_App showAlert:Localized(@"JX_InputPhone")];
       return;
    }
    if([codeStr length]<6){
       [g_App showAlert:Localized(@"JX_InputMessageCode")];
       return;
    }
    [self.view endEditing:YES];
    NSString *areaCode = [_areaCodeBtn.titleLabel.text stringByReplacingOccurrencesOfString:@"+" withString:@""];
   [_wait start];
   if (self.isPayPWD) {
      long time = (long)[[NSDate date] timeIntervalSince1970];
      time = time *1000 + g_server.timeDifference;
      NSString *salt = [NSString stringWithFormat:@"%ld",time];
      NSString *mac = [self getResetPayPWDMacWithSalt:salt];
      [g_server authkeysResetPayPasswordWithSalt:salt mac:mac toView:self];
   }else {
//      [g_server resetPwd:self.phoneTF.text areaCode:areaCode randcode:codeStr newPwd:self.enterPwdTF.text toView:self];
   }
}
-(void)onClick:(UIButton *)btn{
   btn.enabled = NO;
   dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
      btn.enabled = YES;
   });
   if([_phone.text length]<= 0){
      [g_App showAlert:Localized(@"JX_InputPhone")];
      return;
   }
   if (!self.isModify) {
      if([_code.text length]<4){
         [g_App showAlert:Localized(@"JX_InputMessageCode")];
         return;
      }
   }
   if (self.isModify && [_oldPwd.text length] <= 0){
      [g_App showAlert:Localized(@"JX_InputPassWord")];
      return;
   }
   if (!self.isPayPWD) {
      if([_pwd.text length]<=0){
         [g_App showAlert:Localized(@"JX_InputPassWord")];
         return;
      }
      if ([_pwd.text length] < 6) {
         [g_App showAlert:Localized(@"JX_TurePasswordAlert")];
         return;
      }
      if([_repeat.text length]<=0){
         [g_App showAlert:Localized(@"JX_ConfirmPassWord")];
         return;
      }
      if(![_pwd.text isEqualToString:_repeat.text]){
         [g_App showAlert:Localized(@"JX_PasswordFiled")];
         return;
      }
      if ([_pwd.text isEqualToString:_oldPwd.text]) {
         [g_App showAlert:Localized(@"JX_PasswordOriginal")];
         return;
      }
   }
   [self.view endEditing:YES];
   NSString *areaCode = [_areaCodeBtn.titleLabel.text stringByReplacingOccurrencesOfString:@"+" withString:@""];
   if (self.isModify){
      [_wait start];
#ifdef IS_MsgEncrypt
      [g_server userGetRandomStr:self];
#else
      [g_server updatePwd:_phone.text areaCode:areaCode oldPwd:_oldPwd.text newPwd:_pwd.text checkCode:nil toView:self];
#endif
   }else{
      [_wait start];
      if (self.isPayPWD) {
         long time = (long)[[NSDate date] timeIntervalSince1970];
         time = time *1000 + g_server.timeDifference;
         NSString *salt = [NSString stringWithFormat:@"%ld",time];
         NSString *mac = [self getResetPayPWDMacWithSalt:salt];
         [g_server authkeysResetPayPasswordWithSalt:salt mac:mac toView:self];
      }else {
         [g_server resetPwd:_phone.text areaCode:areaCode randcode:_code.text newPwd:_pwd.text toView:self];
      }
   }
}
- (NSString *)getResetPayPWDMacWithSalt:(NSString *)salt {
   NSMutableString *str = [NSMutableString string];
   [str appendString:APIKEY];
   [str appendString:g_myself.userId];
   [str appendString:g_server.access_token];
   [str appendString:salt];
    
//   NSData *key = [MD5Util getMD5DataWithString:_code.text];
    NSString *codeStr = @"";
    for (UITextField *tf in self.codeTFArray) {
        codeStr = [codeStr stringByAppendingString:tf.text];
    }
    NSData *key = [MD5Util getMD5DataWithString:codeStr];
   NSData *macData = [g_securityUtil getHMACMD5:[str dataUsingEncoding:NSUTF8StringEncoding] key:key];
   NSString *mac = [macData base64EncodedStringWithOptions:0];
   return mac;
}
- (void)sendSMS{
   [_phone resignFirstResponder];
   [_imgCode resignFirstResponder];
   [_code resignFirstResponder];
   _send.enabled = NO;
    if (_isPayPWD) {
       if (_imgCode.text.length < 3) {
          [g_App showAlert:Localized(@"JX_inputImgCode")];
          _send.enabled = YES;
          return;
       }
    }
   [self onSend];
}
- (BOOL)isMobileNumber:(NSString *)number{
   if ([_phone.text length] == 0) {
      UIAlertView* alert = [[UIAlertView alloc] initWithTitle:Localized(@"JX_Tip") message:Localized(@"JX_InputPhone") delegate:nil cancelButtonTitle:Localized(@"JX_Confirm") otherButtonTitles:nil, nil];
      [alert show];
      return NO;
   }
   if ([_areaCodeBtn.titleLabel.text isEqualToString:@"+86"]) {
      NSString *regex = @"^(0|86|17951)?(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}$";
      NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
      BOOL isMatch = [pred evaluateWithObject:number];
      if (!isMatch) {
         [g_App showAlert:Localized(@"inputPhoneVC_InputTurePhone")];
         return NO;
      }
   }
   return YES;
}
-(void)onSend{
   if (!_send.selected) {
      [_wait start];
      NSString *areaCode = [_areaCodeBtn.titleLabel.text stringByReplacingOccurrencesOfString:@"+" withString:@""];
      _user.areaCode = areaCode;
      [g_server sendSMS:[NSString stringWithFormat:@"%@",_phone.text] areaCode:areaCode isRegister:NO imgCode:_imgCode.text toView:self];
       //act_tixianRandcodeSendSms
       [g_server sendSMSOutImage:self.phoneTF.text areaCode:areaCode isRegister:0 toView:self];
   }
}
-(void) didServerResultSucces:(JY_Connection*)aDownload dict:(NSDictionary*)dict array:(NSArray*)array1{
   [_wait stop];
    if([aDownload.action isEqualToString:act_tixianRandcodeSendSms]){
//        _send.enabled = YES;
//        _send.selected = YES;
//        _send.userInteractionEnabled = NO;
//        _send.backgroundColor = [UIColor grayColor];
//  //      _smsCode = [[dict objectForKey:@"code"] copy];
//        [_send setTitle:@"60s" forState:UIControlStateSelected];
//        _seconds = 60;
//        timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(showTime:) userInfo:_send repeats:YES];
    }
   if([aDownload.action isEqualToString:act_SendSMS]){
      _send.enabled = YES;
      _send.selected = YES;
      _send.userInteractionEnabled = NO;
//      _send.backgroundColor = [UIColor grayColor];
//      _smsCode = [[dict objectForKey:@"code"] copy];
      [_send setTitle:@"重新发送60s" forState:UIControlStateSelected];
      _seconds = 60;
      timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(showTime:) userInfo:_send repeats:YES];
   }
   if([aDownload.action isEqualToString:act_PwdUpdate] || [aDownload.action isEqualToString:act_PwdUpdateV1]){
      [g_App showAlert:Localized(@"JX_UpdatePassWordOK")];
//      g_myself.password = [g_server getMD5String:_pwd.text];
//      [g_default setObject:[g_server getMD5String:_pwd.text] forKey:kMY_USER_PASSWORD];
//
       g_myself.password = [g_server getMD5String:self.enterPwdTF.text];
       [g_default setObject:[g_server getMD5String:self.enterPwdTF.text] forKey:kMY_USER_PASSWORD];
      [g_default synchronize];
      [self actionQuit];
      [self relogin];
   }
   if([aDownload.action isEqualToString:act_PwdReset] || [aDownload.action isEqualToString:act_PwdResetV1]){
      [g_App showAlert:Localized(@"JX_UpdatePassWordOK")];
//      g_myself.password = [g_server getMD5String:_pwd.text];
//      [g_default setObject:[g_server getMD5String:_pwd.text] forKey:kMY_USER_PASSWORD];
       
       g_myself.password = [g_server getMD5String:self.enterPwdTF.text];
       [g_default setObject:[g_server getMD5String:self.enterPwdTF.text] forKey:kMY_USER_PASSWORD];
      [g_default synchronize];
      [self actionQuit];
   }
   if ([aDownload.action isEqualToString:act_UserGetRandomStr]) {
//      NSString *checkCode = nil;
#ifdef IS_MsgEncrypt
      NSString *userRandomStr = [dict objectForKey:@"userRandomStr"];
      SecKeyRef privateKey = [g_securityUtil getRSAKeyWithBase64Str:g_msgUtil.rsaPrivateKey isPrivateKey:YES];
      NSData *randomData = [[NSData alloc] initWithBase64EncodedString:userRandomStr options:NSDataBase64DecodingIgnoreUnknownCharacters];
      NSData *codeData = [g_securityUtil decryptMessageRSA:randomData withPrivateKey:privateKey];
      checkCode = [[NSString alloc] initWithData:codeData encoding:NSUTF8StringEncoding];
#endif
//      NSString *areaCode = [_areaCodeBtn.titleLabel.text stringByReplacingOccurrencesOfString:@"+" withString:@""];
       
//      [g_server updatePwd:self.phoneTF.text areaCode:areaCode oldPwd:_oldPwd.text newPwd:_pwd.text checkCode:checkCode toView:self];
   }
   if ([aDownload.action isEqualToString:act_AuthkeysResetPayPassword]) {
      if ([self.delegate respondsToSelector:@selector(forgetPwdSuccess)]) {
         [self actionQuit];
         [self.delegate forgetPwdSuccess];
      }
   }
}
-(int) didServerResultFailed:(JY_Connection*)aDownload dict:(NSDictionary*)dict{
    if ([aDownload.action isEqualToString:act_tixianRandcodeSendSms]) {
        
    }else if([aDownload.action isEqualToString:act_SendSMS]){
      [_send setTitle:Localized(@"JX_SendAngin") forState:UIControlStateNormal];
      _send.enabled = YES;
   }else if ([aDownload.action isEqualToString:act_PwdUpdate] || [aDownload.action isEqualToString:act_PwdUpdateV1]) {
      NSString *error = [dict objectForKey:@"resultMsg"];
      [g_App showAlert:[NSString stringWithFormat:@"%@",error]];
      return hide_error;
   }
   [_wait stop];
   return show_error;
}
-(int) didServerConnectError:(JY_Connection*)aDownload error:(NSError *)error{
   [_wait stop];
   _send.enabled = YES;
   return show_error;
}
-(void) didServerConnectStart:(JY_Connection*)aDownload{
   [_wait stop];
}
-(void)showTime:(NSTimer*)sender{
   UIButton *but = (UIButton*)[timer userInfo];
   _seconds--;
   [but setTitle:[NSString stringWithFormat:@"重新发送%ds",_seconds] forState:UIControlStateSelected];
   if(_seconds<=0){
      but.selected = NO;
      but.userInteractionEnabled = YES;
      but.backgroundColor = g_theme.themeColor;
      [_send setTitle:@"重新发送" forState:UIControlStateNormal];
      if (timer) {
         timer = nil;
         [sender invalidate];
      }
      _seconds = 60;
   }
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
   if (textField == _phone) {
      [self getImgCodeImg];
   }
    if ([self.codeTFArray containsObject:textField]) {
        if (textField.text.length == 0) {
            textField.backgroundColor = HEXCOLOR(0xF0F0F0);
        }else if (textField.text.length == 1){
            textField.backgroundColor = HEXCOLOR(0x05D168);
        }
        textField.layer.shadowColor = [UIColor clearColor].CGColor;
        textField.layer.masksToBounds = YES;
        
    }
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if ([self.codeTFArray containsObject:textField]) {
        textField.text = @"";
        textField.backgroundColor = [UIColor whiteColor];
        textField.layer.masksToBounds = NO;
        textField.layer.shadowColor = HEXCOLOR(0xF0F0F0).CGColor;
        textField.layer.shadowOffset = CGSizeMake(0,2.5);
        textField.layer.shadowOpacity = 1;
        textField.layer.shadowRadius = 7;
    }
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
   if (_phone == textField) {
      return [self validateNumber:string];
   }
    if (self.phoneTF == textField) {
        return [self phoneIsNumber:string];
    }
    if ([self.codeTFArray containsObject:textField]) {
        return [self phoneIsNumber:string];
    }
   return YES;
}

- (BOOL)phoneIsNumber:(NSString*)number {
   NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
   NSString *filtered = [[number componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
   return [number isEqualToString:filtered];
}

- (BOOL)validateNumber:(NSString*)number {
   NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"] invertedSet];
   NSString *filtered = [[number componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
   return [number isEqualToString:filtered];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
   [self.view endEditing:YES];
   return YES;
}
- (void)areaCodeBtnClick:(UIButton *)but{
   [self.view endEditing:YES];
   JY_TelAreaListVC *telAreaListVC = [[JY_TelAreaListVC alloc] init];
   telAreaListVC.telAreaDelegate = self;
   telAreaListVC.didSelect = @selector(didSelectTelArea:);
   [g_navigation pushViewController:telAreaListVC animated:YES];
}
- (void)didSelectTelArea:(NSString *)areaCode{
   [_areaCodeBtn setTitle:[NSString stringWithFormat:@"+%@",areaCode] forState:UIControlStateNormal];
   [self resetBtnEdgeInsets:_areaCodeBtn];
    
    [_newAreaCodeBtn setTitle:[NSString stringWithFormat:@"+%@",areaCode] forState:UIControlStateNormal];
    [self resetBtnEdgeInsets:_newAreaCodeBtn];
}
- (void)resetBtnEdgeInsets:(UIButton *)btn{
   [btn setTitleEdgeInsets:UIEdgeInsetsMake(0, -btn.imageView.frame.size.width-2, 0, btn.imageView.frame.size.width+2)];
   [btn setImageEdgeInsets:UIEdgeInsetsMake(0, btn.titleLabel.frame.size.width+2, 0, -btn.titleLabel.frame.size.width-2)];
}
-(void)relogin{
   [g_default removeObjectForKey:kMY_USER_PASSWORD];
   [g_default removeObjectForKey:kMY_USER_TOKEN];
   [share_defaults removeObjectForKey:kMY_ShareExtensionToken];
   g_server.access_token = nil;
   [g_notify postNotificationName:kSystemLogoutNotifaction object:nil];
   [[JY_XMPP sharedInstance] logout];
   NSLog(@"XMPP ---- forgetPwdVC relogin");
   JY_loginVC* vc = [JY_loginVC alloc];
   vc.isAutoLogin = NO;
   vc.isSwitchUser= NO;
   vc = [vc init];
   [g_mainVC.view removeFromSuperview];
   g_mainVC = nil;
   [self.view removeFromSuperview];
   self.view = nil;
   g_navigation.rootViewController = vc;
   [_wait stop];
#if TAR_IM
#ifdef Meeting_Version
   [g_meeting stopMeeting];
#endif
#endif
}
- (void)createLeftViewWithImage:(UIImage *)image superView:(UITextField *)textField {
   UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 27, HEIGHT)];
   textField.leftView = leftView;
   textField.leftViewMode = UITextFieldViewModeAlways;
   UIImageView *leIgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, HEIGHT/2-10, 20, 20)];
   leIgView.image = image;
   leIgView.contentMode = UIViewContentModeScaleAspectFit;
   [leftView addSubview:leIgView];
}
- (void)showLine:(UIView *)view {
   UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, view.frame.size.height-LINE_WH, view.frame.size.width, LINE_WH)];
   line.backgroundColor = THE_LINE_COLOR;
   [view addSubview:line];
}

- (IBAction)dismissSurePwdView:(id)sender {
    self.surePwdView.hidden = YES;
}

- (IBAction)dismissEnterPwdView:(id)sender {
    self.enterPwdView.hidden = YES;
    self.enterPwdTF.text = @"";
}
- (IBAction)dismissCodeView:(id)sender {
    self.codeView.hidden = YES;
    self.surePwdTF.text = @"";
    
}
- (IBAction)backClick:(id)sender {
    [self actionQuit];
}

- (IBAction)forgetBackBtnClick:(id)sender {
    [self actionQuit];
}

@end
