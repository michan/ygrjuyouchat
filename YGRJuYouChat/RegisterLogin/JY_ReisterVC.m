//
//  JY_ReisterVC.m
//  TFJunYouChat
//
//  Created by TT on 2021/8/18.
//  Copyright © 2021 zengwOS. All rights reserved.
//

#import "JY_ReisterVC.h"
#import "JY_inputPwdVC.h"
#import "JY_TelAreaListVC.h"
#import "JY_UserObject.h"
#import "JY_PSRegisterBaseVC.h"
#import "webpageVC.h"
#import "JY_loginVC.h"
#import "WKWebViewViewController.h"
#import "UIView+LK.h"
#import "JXTermsPrivacyVc.h"
#import "JY_forgetPwdVC.h"
#import "MD5Util.h"
#import "AESUtil.h"
#define HEIGHT 56

@interface JY_ReisterVC ()<UITextFieldDelegate>
{
    NSTimer *_timer;
    UIButton *_areaCodeBtn;
    JY_UserObject *_user;
    UIImageView * _imgCodeImg;
//    UITextField *_imgCode;
    UIButton * _graphicButton;
    UIButton* _skipBtn;
    BOOL _isSkipSMS;
    BOOL _isSendFirst;
    UIImageView * _agreeImgV;
    BOOL _isNext;
    BOOL isRegistered;
}
@property (nonatomic, assign) BOOL isSmsRegister;
@property (nonatomic, assign) BOOL isCheckToSMS;
@property (nonatomic, weak) UIView *backView;
@property (nonatomic, weak) UIView *knowBackView;

@property(nonatomic,strong) UIButton *protocalBtn; // 去登录

@property(nonatomic,strong) UILabel *readLabel; // 去登录
@property(nonatomic,strong) UILabel *yiJiLabel; // 去登录

@property(nonatomic,strong) UIButton *selectProtocalBtn; //yonghuxieyi

@property(nonatomic,strong) UIButton *ProctProtocalBtn; // yingsi zhengce

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *passwordMarginTop;

@property (nonatomic, weak) UIButton *toLoginBtn;
@property (nonatomic,weak) JXTermsPrivacyVc *showImgView;
@property (nonatomic, assign) ReisterPageStyle pageStyle;

// 手机号码视图
@property (weak, nonatomic) IBOutlet UIView *phoneNumberView;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UIButton *surePhoneNumberBtn;


// 密码视图
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *surePasswordBtn;
@property (weak, nonatomic) IBOutlet UIView *passwordView;

//验证码视图
@property (weak, nonatomic) IBOutlet UIButton *sureCodeBtn;
@property (weak, nonatomic) IBOutlet UIView *codeView;
@property (weak, nonatomic) IBOutlet UILabel *codeToPhoneNumberLabel;
@property (weak, nonatomic) IBOutlet UITextField *p1;
@property (weak, nonatomic) IBOutlet UITextField *p2;
@property (weak, nonatomic) IBOutlet UITextField *p3;

@property (weak, nonatomic) IBOutlet UITextField *p4;

@property (weak, nonatomic) IBOutlet UITextField *p5;
@property (weak, nonatomic) IBOutlet UITextField *p6;

@property (nonatomic, copy) NSArray <UITextField *>*codeTFArray;

@property (nonatomic, assign) NSInteger enterCount;

@end

@implementation JY_ReisterVC

- (IBAction)sureCodeBtnClick:(UIButton *)sender {

    if ([sender.currentTitle isEqualToString:@"重新发送"]) {
        if (isRegistered) {
            [g_server getacg_tixianRandcodeSendSms:self.phoneTextField.text toView:self];
        }else{
            [g_server checkPhone:self.phoneTextField.text areaCode:_areaIDStr verifyType:0 toView:self];
        }
        return;
    }
    if (isRegistered) {
        //登录
        [self smsLogin];
        return;
    }
    _isSkipSMS = NO;
    if (_pageStyle == ReisterPageStyleNone) {
        if ([self.passwordTextField.text length] < 6) {
            [g_App showAlert:Localized(@"JX_TurePasswordAlert")];
            return;
        }
        if ([g_config.registerInviteCode intValue] == 1) {
            if ([_inviteCode.text length] <= 0) {
                [g_App showAlert:Localized(@"JX_EnterInvitationCode")];
                return;
            }
        }
    }
    NSString *codeStr = @"";
    for (UITextField *tf in self.codeTFArray) {
        codeStr = [codeStr stringByAppendingString:tf.text];
    }
    if (_isNext == NO) {
        if(codeStr.length < 5){
            if([codeStr length]<=0){
                [g_App showAlert:@"请输入短信验证码"];
                return;
            }
            [g_App showAlert:Localized(@"inputPhoneVC_MsgCodeNotOK")];
            return;
        }
    }
    [g_server checkPhone:self.phoneTextField.text areaCode:_areaIDStr verifyType:0 toView:self];
}
-(void)smsLogin{
    _user = [JY_UserObject sharedInstance];
   _user.telephone = self.phoneTextField.text;
   _user.password  = [g_server getMD5String:self.passwordTextField.text];
   _user.areaCode = _areaIDStr;
    NSString *codeStr = @"";
    for (UITextField *tf in self.codeTFArray) {
        codeStr = [codeStr stringByAppendingString:tf.text];
    }
    _user.verificationCode =codeStr;
    [g_loginServer smsLoginWithUser:_user areaCode:_areaIDStr account:self.phoneTextField.text toView:self];
//    long time = (long)[[NSDate date] timeIntervalSince1970];
//    time = time *1000 + g_server.timeDifference;
//    NSString *salt = [NSString stringWithFormat:@"%ld",time];
//    [g_server userSMSLogin:_areaIDStr account:self.phoneTextField.text salt:salt data:@"" toView:self];

//    self.param = [self getLoginParamWithUser:user];
//    NSString *data = [self getSMSDataStr];
   
}
- (IBAction)surePhoneNumberBtnClick:(id)sender {
    [self.phoneTextField resignFirstResponder];
    
    [self getImgCodeImg];
    _isSkipSMS = NO;
    BOOL isMobile = [self isMobileNumber:self.phoneTextField.text];
    if (!isMobile) {
        [g_App showAlert:@"请检测手机号是否正确"];
        return;
    }
//    [self showSetPassword];
    
    [g_server chenkAccountStatusPhone:self.phoneTextField.text areaCode:_areaIDStr toView:self];
    
    
}
-(void)showSetPassword{
    if (_pageStyle == ReisterPageStyleNone) {
        if ([g_config.registerInviteCode intValue] == 1) {
            if ([_inviteCode.text length] <= 0) {
                [g_App showAlert:Localized(@"JX_EnterInvitationCode")];
                return;
            }
        }
    }
    self.passwordView.hidden = NO;
}

- (IBAction)surePasswordBtnClick:(id)sender {
    [self needPassword:YES];
}
-(void)needPassword:(BOOL)need{
    if (need) {
        if ([self.passwordTextField.text length] < 6) {
            [g_App showAlert:Localized(@"JX_TurePasswordAlert")];
            return;
        }
        [self.passwordTextField resignFirstResponder];
        
        if (_isNext == NO) {
            if([_code.text length]<6){
                if([_code.text length]<=0){
                    [g_App showAlert:@"请输入短信验证码"];
                    return;
                }
                [g_App showAlert:Localized(@"inputPhoneVC_MsgCodeNotOK")];
                return;
            }
        }
        if ([self isMobileNumber:self.phoneTextField.text]) {
           [g_server checkPhone:self.phoneTextField.text areaCode:_areaIDStr verifyType:0 toView:self];
        }
    }else{
        [self showEnterCodeView];
    }
}


- (id)initWithReisterPageStyle:(ReisterPageStyle)pageStyle
{
    self = [super init];
    if (self) {
        
        
        _pageStyle = pageStyle;
        _seconds = 0;
        self.isGotoBack   = YES;
//        if (pageStyle == ReisterPageStyleNone) {
//            self.title = Localized(@"JX_Register");
//        }
        self.heightFooter = 0;
        self.heightHeader = ManMan_SCREEN_TOP;
        //self.view.frame = g_window.bounds;
        [self createHeadAndFoot];
        self.tableBody.backgroundColor = [UIColor whiteColor];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyBoardToView)];
        [self.tableBody addGestureRecognizer:tap];
        _isSendFirst = YES;  // 第一次发送短信
        _isNext = NO;
        self.isSmsRegister = NO;
        [self setNorml];
    }
    return self;
}
-(void)shouqi{
    [self.phoneTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
    for (UITextField *tf in _codeTFArray) {
        [tf resignFirstResponder];
    }
}
- (void)setNorml{
   
    _isNext = YES;
    int n = INSETS;
    int distance = 20; // 左右间距
//    self.passwordMarginTop.constant = 40;
    ViewRadius(self.surePasswordBtn, 5);
    UIView *l = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 25,35)];
    UIImageView *imgv = [[UIImageView alloc] initWithFrame:CGRectMake(0, 10, 15, 15)];
    [l addSubview:imgv];
    imgv.image = [UIImage imageNamed:@"q_mima_icon"];
    self.passwordTextField.leftView =l;
    self.passwordTextField.leftViewMode =UITextFieldViewModeAlways;
    self.passwordView.hidden = YES;
    
    self.passwordTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    [self.passwordTextField addTarget:self action:@selector(textFieldDidChanged:) forControlEvents:UIControlEventEditingChanged];
    
    NSString *areaStr;
    if (![g_default objectForKey:kMY_USER_AREACODE]) {
        areaStr = [NSString stringWithFormat:@"+%@",@"86"];
        _areaIDStr = @"86";
    } else {
       NSMutableArray *telAreaArray = [g_constant.telArea mutableCopy];
        NSString *ID = [g_default objectForKey:kMY_USER_AREACODE];
        
        for (NSDictionary *dict in telAreaArray) {
            if (ID.integerValue == [dict[@"prefix"] integerValue]) {
                _areaIDStr = ID;
                areaStr = [NSString stringWithFormat:@"+%@",ID];
               
            }
        }
    }
    _areaCodeBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 35)];
    //w self_width-distance*2
    _areaCodeBtn.hidden = [g_config.regeditPhoneOrName intValue] == 1;
    [_areaCodeBtn setTitleColor:HEXCOLOR(0x303030) forState:UIControlStateNormal];
    [_areaCodeBtn addTarget:self action:@selector(areaCodeBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [_areaCodeBtn setTitle:areaStr forState:UIControlStateNormal];
    UIView *phoneLeft = [[UIView alloc] initWithFrame:CGRectMake(distance, n, 50,35)];
    
    [phoneLeft addSubview:_areaCodeBtn];
    
    self.phoneTextField.leftView = phoneLeft;
    self.phoneTextField.leftViewMode =UITextFieldViewModeAlways;
    self.phoneTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    [self.phoneTextField addTarget:self action:@selector(textFieldDidChanged:) forControlEvents:UIControlEventEditingChanged];
    ViewRadius(self.surePhoneNumberBtn, 5);
//    self.phoneNumberView.hidden = YES;
    
    
    self.codeView.hidden = YES;
    [self.view bringSubviewToFront:self.phoneNumberView];
    [self.view bringSubviewToFront:self.passwordView];
    [self.view bringSubviewToFront:self.codeView];
    
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(shouqi)];
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(shouqi)];
    UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(shouqi)];
    [self.phoneNumberView addGestureRecognizer:tap1];
    [self.passwordView addGestureRecognizer:tap2];
    [self.codeView addGestureRecognizer:tap3];
    
    
    ViewRadius(self.sureCodeBtn, 5);
    
//    return;
    self.tableHeader.hidden = YES;
    self.tableBody.hidden = YES;
    n += 10;
    
    UILabel *titleLab = [UIFactory createLabelWith:CGRectMake(20,n,ManMan_SCREEN_WIDTH-40 , 30) text:((_pageStyle == ReisterPageStyleNone) ? @"手机号码登录" : @"修改绑定手机号") font:KFontRegular(20) textColor:HEXCOLOR(0x333333) backgroundColor:nil];
    [self.tableBody addSubview:titleLab];
    titleLab.textAlignment = NSTextAlignmentCenter;
    n += (30+30);
    
//
//    UIImageView * kuliaoIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ALOGO_120"]];
//    kuliaoIconView.frame = CGRectMake((ManMan_SCREEN_WIDTH-100)/2, n, 100, 100);
//
//    kuliaoIconView.layer.cornerRadius = 10;
//    kuliaoIconView.layer.masksToBounds = YES;
//    [self.tableBody addSubview:kuliaoIconView];
    
    //手机号
    n += 40+95;
    if (!_phone) {
        NSString *placeHolder;
        if ([g_config.regeditPhoneOrName intValue] == 0) {
            placeHolder = Localized(@"JX_InputPhone");
        }else {
            placeHolder = Localized(@"JX_InputUserAccount");
        }
        _phone = [UIFactory createTextFieldWith:CGRectMake(distance, n, self_width-distance*2, HEIGHT) delegate:self returnKeyType:UIReturnKeyNext secureTextEntry:NO placeholder:placeHolder font:g_factory.font17];
        _phone.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeHolder attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
        _phone.borderStyle = UITextBorderStyleNone;
        if ([g_config.regeditPhoneOrName intValue] == 1) {
            _phone.keyboardType = UIKeyboardTypeDefault;  // 仅支持大小写字母数字
        }else {
            _phone.keyboardType = UIKeyboardTypeNumberPad;  // 限制只能数字输入，使用数字键盘
        }
        _phone.clearButtonMode = UITextFieldViewModeWhileEditing;
        [_phone addTarget:self action:@selector(textFieldDidChanged:) forControlEvents:UIControlEventEditingChanged];
        [self.tableBody addSubview:_phone];
//        UIView *riPhView = [[UIView alloc] initWithFrame:CGRectMake(_phone.frame.size.width-44, 0, item_height + 5 , item_height)];
        
//        CGFloat left_W = [UIFactory getStringWidthWithText:@"手机号" font:g_factory.font16 viewHeight:HEIGHT];
//        UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, left_W + 10, HEIGHT)];
//        _phone.leftView = leftView;
        
//        UILabel *phIgLab = [UIFactory createLabelWith:CGRectMake(0, 0, left_W, HEIGHT) text:@"手机号" font:KFontMedium(17) textColor:HEXCOLOR(0x3333333) backgroundColor:nil];
//        [leftView addSubview:phIgLab];
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT-LINE_WH, _phone.frame.size.width, LINE_WH)];
        line.backgroundColor = THE_LINE_COLOR;
        [_phone addSubview:line];
    }
    n = n+HEIGHT;
    
    if (!_area) {
        NSString *placeHolder;
        NSString *areaStr;
        if (![g_default objectForKey:kMY_USER_AREACODE]) {
            areaStr = [NSString stringWithFormat:@"+%@",@"86"];
            _areaIDStr = @"86";
        } else {
           NSMutableArray *telAreaArray = [g_constant.telArea mutableCopy];
            NSString *ID = [g_default objectForKey:kMY_USER_AREACODE];
            
            for (NSDictionary *dict in telAreaArray) {
                if (ID.integerValue == [dict[@"prefix"] integerValue]) {
                    _areaIDStr = ID;
                    areaStr = [NSString stringWithFormat:@"%@(+%@)",dict[@"country"],ID];
                }
            }
        }
      
//        _area = [UIFactory createTextFieldWith:CGRectMake(distance, n, self_width-distance*2, HEIGHT) delegate:self returnKeyType:UIReturnKeyDefault secureTextEntry:NO placeholder:placeHolder font:g_factory.font16];
//        _area.borderStyle = UITextBorderStyleNone;
//        _area.enabled = NO;
//        _area.text = areaStr;
//        _phone.leftView = _area;
//        _area.textColor = RGB(128, 180, 204);
//        [self.tableBody addSubview:_area];
        
        
//        CGFloat left_W = [UIFactory getStringWidthWithText:@"国家/地区" font:g_factory.font16 viewHeight:HEIGHT];
//        UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, left_W + 10, HEIGHT)];
//        _area.leftView = leftView;
//        _area.leftViewMode = UITextFieldViewModeAlways;
//        UILabel *phIgLab = [UIFactory createLabelWith:CGRectMake(0, 0, left_W, HEIGHT) text:@"国家/地区" font:KFontMedium(16) textColor:HEXCOLOR(0x3333333) backgroundColor:nil];
//        [leftView addSubview:phIgLab];
//        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT-LINE_WH, _area.frame.size.width, LINE_WH)];
//        line.backgroundColor = THE_LINE_COLOR;
//        [_area addSubview:line];
//        _area.userInteractionEnabled = YES;
//        _areaCodeBtn = [[UIButton alloc] initWithFrame:CGRectMake(distance, n, 50, HEIGHT)];
//        //w self_width-distance*2
//        _areaCodeBtn.hidden = [g_config.regeditPhoneOrName intValue] == 1;
//        [_areaCodeBtn setTitleColor:HEXCOLOR(0x303030) forState:UIControlStateNormal];
//        [_areaCodeBtn addTarget:self action:@selector(areaCodeBtnClick) forControlEvents:UIControlEventTouchUpInside];
//        [_areaCodeBtn setTitle:areaStr forState:UIControlStateNormal];
//        
////        [self.tableBody addSubview:_areaCodeBtn];
//        [_areaCodeBtn setTitle:areaStr forState:UIControlStateNormal];
////        [_areaCodeBtn setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
//        
//        _phone.leftView = _areaCodeBtn;
//        _phone.leftViewMode = UITextFieldViewModeAlways;
       
        
        n = n+HEIGHT;
    }
    
   
    
    
    if (_pageStyle == ReisterPageStyleNone) {
        if (!_pwd) {
            //密码
            _pwd = [[UITextField alloc] initWithFrame:CGRectMake(distance, n, ManMan_SCREEN_WIDTH-distance*2, HEIGHT)];
            _pwd.delegate = self;
            _pwd.font = g_factory.font16;
            _pwd.autocorrectionType = UITextAutocorrectionTypeNo;
            _pwd.autocapitalizationType = UITextAutocapitalizationTypeNone;
            _pwd.enablesReturnKeyAutomatically = YES;
            _pwd.returnKeyType = UIReturnKeyDone;
            _pwd.clearButtonMode = UITextFieldViewModeWhileEditing;
            _pwd.attributedPlaceholder = [[NSAttributedString alloc] initWithString:((_pageStyle == ReisterPageStyleNone) ? Localized(@"JX_InputPassWord") : Localized(@"JX_InputMessageCode")) attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
            _pwd.secureTextEntry = YES;
            _pwd.userInteractionEnabled = YES;

            [_pwd addTarget:self action:@selector(textFieldDidChanged:) forControlEvents:UIControlEventEditingChanged];
           [self.tableBody addSubview:_pwd];

            CGFloat left_W = [UIFactory getStringWidthWithText:((_pageStyle == ReisterPageStyleNone) ? @"密码" : @"验证码") font:g_factory.font16 viewHeight:HEIGHT];
            UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,  left_W+10 , HEIGHT)];
            _pwd.leftView = rightView;
            _pwd.leftViewMode = UITextFieldViewModeAlways;
            UILabel *phIgLab = [UIFactory createLabelWith:CGRectMake(0, 0, left_W, HEIGHT) text:((_pageStyle == ReisterPageStyleNone) ? @"密码" : @"验证码") font:KFontMedium(16) textColor:HEXCOLOR(0x3333333) backgroundColor:nil];
            [rightView addSubview:phIgLab];

            UIView *verticalLine = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT-LINE_WH, _pwd.frame.size.width, LINE_WH)];
            verticalLine.backgroundColor = THE_LINE_COLOR;
            [_pwd addSubview:verticalLine];

            n = n+HEIGHT;

        }
        if ([g_config.registerInviteCode intValue] != 0) {
            //邀请码
            _inviteCode = [[UITextField alloc] initWithFrame:CGRectMake(distance, n, ManMan_SCREEN_WIDTH-distance*2, HEIGHT)];
            _inviteCode.delegate = self;
            _inviteCode.font = g_factory.font16;
            _inviteCode.autocorrectionType = UITextAutocorrectionTypeNo;
            _inviteCode.autocapitalizationType = UITextAutocapitalizationTypeNone;
            _inviteCode.enablesReturnKeyAutomatically = YES;
            _inviteCode.returnKeyType = UIReturnKeyDone;
            _inviteCode.clearButtonMode = UITextFieldViewModeWhileEditing;
            _inviteCode.attributedPlaceholder = [[NSAttributedString alloc] initWithString:Localized(@"JX_EnterInvitationCode") attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
            _inviteCode.secureTextEntry = YES;
            _inviteCode.userInteractionEnabled = YES;
            [self.tableBody addSubview:_inviteCode];
            
            UIView *inviteRightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 37, HEIGHT)];
            _inviteCode.leftView = inviteRightView;
            _inviteCode.leftViewMode = UITextFieldViewModeAlways;
            UIImageView *inviteRiIgView = [[UIImageView alloc] initWithFrame:CGRectMake(2, HEIGHT/2-11, 22, 22)];
            inviteRiIgView.image = [UIImage imageNamed:@"password"];
            inviteRiIgView.contentMode = UIViewContentModeScaleAspectFit;
            [inviteRightView addSubview:inviteRiIgView];
            
            UIView  *verticalLine = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(inviteRightView.frame)-4, _inviteCode.frame.size.width, 0.5)];
            verticalLine.backgroundColor = HEXCOLOR(0xD6D6D6);
            [inviteRightView addSubview:verticalLine];
            
            n = n+HEIGHT;
        }
    }else{
       n = [self createCodeRowWithHeight:n];
    }
    

    //新添加的手机验证（注册）
    n = n+HEIGHT+INSETS;
   //
    UIButton * _btn = [UIFactory createButtonWithTitle:(_pageStyle == ReisterPageStyleNone ? @"下一步" : @"确认修改") titleFont:g_factory.font16 titleColor:[UIColor whiteColor] normal:nil highlight:nil];
    
    [_btn addTarget:self action:@selector(checkPhoneNumber) forControlEvents:UIControlEventTouchUpInside];
    _btn.frame = CGRectMake(20, n,ManMan_SCREEN_WIDTH-20*2, 40);
    _btn.backgroundColor = HEXCOLOR(0x05D168);
    ViewRadius(_btn, 5);
    [self.tableBody addSubview:_btn];
    n = n+HEIGHT+INSETS;
    
    if (_pageStyle == ReisterPageStyleEditAccout) {
        UILabel *tipLabel = [[UILabel alloc] init];
        tipLabel.text = @"重要提示：更换手机必须是本人实名手机号，需接受短信验证码。";
        tipLabel.textColor = [UIColor grayColor];
        tipLabel.textAlignment = NSTextAlignmentCenter;
        tipLabel.font = g_factory.font12;
        [self.tableBody addSubview:tipLabel];
        tipLabel.frame = CGRectMake(20, n, ManMan_SCREEN_WIDTH-20*2, 30);
    }
}
-(void)showEnterCodeView{

    //隐藏密码视图
    self.codeTFArray = @[_p1,_p2,_p3,_p4,_p5,_p6];
    for (UITextField *tf in self.codeTFArray) {
        tf.delegate = self;
        [tf addTarget:self action:@selector(textFieldDidChanged:) forControlEvents:UIControlEventEditingChanged];
        tf.layer.cornerRadius = 5;
        if (tf.tag == 10) {
            tf.backgroundColor = [UIColor whiteColor];
            self.enterCount = 0;
            [tf becomeFirstResponder];
        }else{
            tf.backgroundColor = HEXCOLOR(0xf0f0f0);
        }
        tf.text = @"";
        tf.tintColor = HEXCOLOR(0x05D168);
    }
    
    _isNext = NO;
//    self.passwordView.hidden = YES;
    
    //显示验证码视图
    self.codeView.hidden = NO;
    
//    self.codeToPhoneNumberLabel.text = [NSString stringWithFormat:@"短信发送至 %@",self.phoneTextField.text];
    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:@"短信发送至 " attributes:@{NSForegroundColorAttributeName:HEXCOLOR(0x303030)}];
    NSAttributedString *a1 = [[NSAttributedString alloc] initWithString:self.phoneTextField.text attributes:@{NSForegroundColorAttributeName:HEXCOLOR(0x05D168)}];
    [attr appendAttributedString:a1];
    self.codeToPhoneNumberLabel.attributedText = attr;
    
    if([self isMobileNumber:self.phoneTextField.text]){
        self.isCheckToSMS = YES;
        if (isRegistered) {
//            -(void)sendSMS:(NSString*)telephone areaCode:(NSString *)areaCode isRegister:(BOOL)isRegister imgCode:(NSString *)imgCode toView:(id)toView
            [g_server getacg_tixianRandcodeSendSms:self.phoneTextField.text toView:self];
        }else{
            [g_server checkPhone:self.phoneTextField.text areaCode:_areaIDStr verifyType:0 toView:self];
        }
    }
    
    self.sureCodeBtn.userInteractionEnabled = NO;
    self.sureCodeBtn.backgroundColor = [HEXCOLOR(0x05D168) colorWithAlphaComponent:0.5];
    [self.sureCodeBtn setTitle:@"60秒后重新发送" forState:UIControlStateNormal];
    _seconds = 60;
    _timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(showNewTimer:) userInfo:self.sureCodeBtn repeats:YES];
}
- (void)setNextView{
    _seconds = 0;
    _isNext = NO;
    int n = INSETS;
    int distance = 20; // 左右间距
    self.isSmsRegister = NO;
    //icon
    n += 30;
    
    for (UIView *subView in self.tableBody.subviews) {
        subView.hidden = YES;
    }
    
    UILabel *titleLab = [UIFactory createLabelWith:CGRectMake(distance,n,ManMan_SCREEN_WIDTH-distance*2 , 30) text:@"发送短信注册" font:KFontMedium(20) textColor:HEXCOLOR(0x333333) backgroundColor:nil];
    [self.tableBody addSubview:titleLab];
    n += (30+40);
    
    UILabel *phoneLab = [UIFactory createLabelWith:CGRectMake(distance,n,ManMan_SCREEN_WIDTH-distance*2 , 30) text:[NSString stringWithFormat:@"请使用手机号码+%@ %@",_areaIDStr,_phone.text] font:KFontMedium(20) textColor:HEXCOLOR(0x333333) backgroundColor:nil];
    [self.tableBody addSubview:phoneLab];
    n += (30+20);
    
    UILabel *detailLab = [UIFactory createLabelWith:CGRectMake(distance,n,ManMan_SCREEN_WIDTH-distance*2 , 30) text:@"接受短信" font:KFontMedium(20) textColor:HEXCOLOR(0x333333) backgroundColor:nil];
    [self.tableBody addSubview:detailLab];
    
    n = [self createCodeRowWithHeight:n];
    
    UIButton * btn = [UIFactory createButtonWithTitle:Localized(@"REGISTERS") titleFont:g_factory.font16 titleColor:[UIColor whiteColor] normal:nil highlight:nil];
    
    [btn addTarget:self action:@selector(checkPhoneNumber) forControlEvents:UIControlEventTouchUpInside];
    btn.frame = CGRectMake(20, n,ManMan_SCREEN_WIDTH-20*2, 40);
    btn.backgroundColor = RGB(90, 202, 198);
    ViewRadius(btn, 20);
    [self.tableBody addSubview:btn];
    n = n+HEIGHT+INSETS;
    
    
    
}
/// 创建验证码
-(int)createCodeRowWithHeight:(int)n
{
    int distance = 20; // 左右间距
    //手机号
    if (!_code) {
        if (_pageStyle == ReisterPageStyleNone) {
            n += 40+50;
        }
        _code = [[UITextField alloc] initWithFrame:CGRectMake(distance, n, ManMan_SCREEN_WIDTH-75-distance*2, HEIGHT)];
        _code.attributedPlaceholder = [[NSAttributedString alloc] initWithString:((_pageStyle == ReisterPageStyleNone) ?Localized(@"JX_InputMessageCode") :@"请输入原手机验证码" ) attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
        _code.font = g_factory.font16;
        _code.delegate = self;
        _code.autocorrectionType = UITextAutocorrectionTypeNo;
        _code.autocapitalizationType = UITextAutocapitalizationTypeNone;
        _code.enablesReturnKeyAutomatically = YES;
        _code.borderStyle = UITextBorderStyleNone;
        _code.returnKeyType = UIReturnKeyDone;
        _code.clearButtonMode = UITextFieldViewModeWhileEditing;
        
        UIView *codeView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 37, HEIGHT)];
        _code.leftView = codeView;
        _code.leftViewMode = UITextFieldViewModeAlways;
        CGFloat left_W = [UIFactory getStringWidthWithText:@"验证码" font:g_factory.font16 viewHeight:HEIGHT];
        UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, left_W + 10, HEIGHT)];
        _code.leftView = leftView;
        _code.leftViewMode = UITextFieldViewModeAlways;
        UILabel *phIgLab = [UIFactory createLabelWith:CGRectMake(0, 0, left_W, HEIGHT) text:@"验证码" font:KFontMedium(16) textColor:HEXCOLOR(0x3333333) backgroundColor:nil];
        [leftView addSubview:phIgLab];
        
        
        UIView *codeILine = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT-LINE_WH, _code.frame.size.width, LINE_WH)];
        codeILine.backgroundColor = THE_LINE_COLOR;
        [_code addSubview:codeILine];
        
        
        [self.tableBody addSubview:_code];
        
    }
  
    if (!_send) {
        _send = [UIFactory createButtonWithTitle:Localized(@"JX_Send")
                                       titleFont:g_factory.font16
                                      titleColor:[UIColor whiteColor]
                                          normal:nil
                                       highlight:nil ];
        _send.frame = CGRectMake(ManMan_SCREEN_WIDTH-70-distance-11, n+HEIGHT/2-15, 70, 30);
        [_send addTarget:self action:@selector(sendSMS) forControlEvents:UIControlEventTouchUpInside];
        _send.backgroundColor = RGB(89, 202, 194);
        
        _send.layer.masksToBounds = YES;
        _send.layer.cornerRadius = 7.f;
        [self.tableBody addSubview:_send];
        //新添加的手机验证（注册）
        n = n+HEIGHT+INSETS;
    }
    return n;
}

//- (void)setUpView{
//    int n = INSETS;
//    int distance = 40; // 左右间距
//
//    //icon
//    n += 40;
//
//    UIImageView * kuliaoIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ALOGO_120"]];
//    kuliaoIconView.frame = CGRectMake((ManMan_SCREEN_WIDTH-100)/2, n, 100, 100);
//
//    kuliaoIconView.layer.cornerRadius = 10;
//    kuliaoIconView.layer.masksToBounds = YES;
//    [self.tableBody addSubview:kuliaoIconView];
//
//    //手机号
//    n += 30+95;
//    if (!_phone) {
//        NSString *placeHolder;
//        if ([g_config.regeditPhoneOrName intValue] == 0) {
//            placeHolder = Localized(@"JX_InputPhone");
//        }else {
//            placeHolder = Localized(@"JX_InputUserAccount");
//        }
//        _phone = [UIFactory createTextFieldWith:CGRectMake(distance, n, self_width-distance*2, HEIGHT) delegate:self returnKeyType:UIReturnKeyNext secureTextEntry:NO placeholder:placeHolder font:g_factory.font16];
//        _phone.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeHolder attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
//        _phone.borderStyle = UITextBorderStyleNone;
//        if ([g_config.regeditPhoneOrName intValue] == 1) {
//            _phone.keyboardType = UIKeyboardTypeDefault;  // 仅支持大小写字母数字
//        }else {
//            _phone.keyboardType = UIKeyboardTypeNumberPad;  // 限制只能数字输入，使用数字键盘
//        }
//        _phone.clearButtonMode = UITextFieldViewModeWhileEditing;
//        [_phone addTarget:self action:@selector(textFieldDidChanged:) forControlEvents:UIControlEventEditingChanged];
//        [self.tableBody addSubview:_phone];
//
//        UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 37, HEIGHT)];
//        _phone.leftView = leftView;
//        _phone.leftViewMode = UITextFieldViewModeAlways;
//        UIImageView *phIgView = [[UIImageView alloc] initWithFrame:CGRectMake(2, HEIGHT/2-11, 22, 22)];
//        phIgView.image = [UIImage imageNamed:@"account"];
//        phIgView.contentMode = UIViewContentModeScaleAspectFit;
//        [leftView addSubview:phIgView];
//        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT-LINE_WH, _phone.frame.size.width, LINE_WH)];
//        line.backgroundColor = THE_LINE_COLOR;
//        [_phone addSubview:line];
//
//        UIView *riPhView = [[UIView alloc] initWithFrame:CGRectMake(_phone.frame.size.width-44, 0, HEIGHT, HEIGHT)];
//        _phone.rightView = riPhView;
//        _phone.rightViewMode = UITextFieldViewModeAlways;
//        NSString *areaStr;
//        if (![g_default objectForKey:kMY_USER_AREACODE]) {
//            areaStr = @"+86";
//        } else {
//            areaStr = [NSString stringWithFormat:@"+%@",[g_default objectForKey:kMY_USER_AREACODE]];
//        }
//        _areaCodeBtn = [[UIButton alloc] initWithFrame:CGRectMake(5, HEIGHT/2-8, HEIGHT-5, 22)];
//        [_areaCodeBtn setTitle:areaStr forState:UIControlStateNormal];
//        _areaCodeBtn.titleLabel.font = SYSFONT(15);
//        _areaCodeBtn.hidden = [g_config.regeditPhoneOrName intValue] == 1;
//        [_areaCodeBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//            [_areaCodeBtn setImage:[UIImage imageNamed:@"account"] forState:UIControlStateNormal];
//        [_areaCodeBtn setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
//        [_areaCodeBtn addTarget:self action:@selector(areaCodeBtnClick) forControlEvents:UIControlEventTouchUpInside];
//        [self resetBtnEdgeInsets:_areaCodeBtn];
//        [riPhView addSubview:_areaCodeBtn];
//    }
//    n = n+HEIGHT;
//    //密码
//    _pwd = [[UITextField alloc] initWithFrame:CGRectMake(distance, n, ManMan_SCREEN_WIDTH-distance*2, HEIGHT)];
//    _pwd.delegate = self;
//    _pwd.font = g_factory.font16;
//    _pwd.autocorrectionType = UITextAutocorrectionTypeNo;
//    _pwd.autocapitalizationType = UITextAutocapitalizationTypeNone;
//    _pwd.enablesReturnKeyAutomatically = YES;
//    _pwd.returnKeyType = UIReturnKeyDone;
//    _pwd.clearButtonMode = UITextFieldViewModeWhileEditing;
//    _pwd.attributedPlaceholder = [[NSAttributedString alloc] initWithString:Localized(@"JX_InputPassWord") attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
//    _pwd.secureTextEntry = YES;
//    _pwd.userInteractionEnabled = YES;
//
//    [_pwd addTarget:self action:@selector(textFieldDidChanged:) forControlEvents:UIControlEventEditingChanged];
//   [self.tableBody addSubview:_pwd];
//
//    UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 37, HEIGHT)];
//    _pwd.leftView = rightView;
//    _pwd.leftViewMode = UITextFieldViewModeAlways;
//    UIImageView *riIgView = [[UIImageView alloc] initWithFrame:CGRectMake(2, HEIGHT/2-11, 22, 22)];
//    riIgView.image = [UIImage imageNamed:@"password"];
//    riIgView.contentMode = UIViewContentModeScaleAspectFit;
//    [rightView addSubview:riIgView];
//
//    UIView *verticalLine = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT-LINE_WH, _pwd.frame.size.width, LINE_WH)];
//    verticalLine.backgroundColor = THE_LINE_COLOR;
//    [_pwd addSubview:verticalLine];
//
//    n = n+HEIGHT;
//
//    if ([g_config.registerInviteCode intValue] != 0) {
//        //邀请码
//        _inviteCode = [[UITextField alloc] initWithFrame:CGRectMake(distance, n, ManMan_SCREEN_WIDTH-distance*2, HEIGHT)];
//        _inviteCode.delegate = self;
//        _inviteCode.font = g_factory.font16;
//        _inviteCode.autocorrectionType = UITextAutocorrectionTypeNo;
//        _inviteCode.autocapitalizationType = UITextAutocapitalizationTypeNone;
//        _inviteCode.enablesReturnKeyAutomatically = YES;
//        _inviteCode.returnKeyType = UIReturnKeyDone;
//        _inviteCode.clearButtonMode = UITextFieldViewModeWhileEditing;
//        _inviteCode.attributedPlaceholder = [[NSAttributedString alloc] initWithString:Localized(@"JX_EnterInvitationCode") attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
//        _inviteCode.secureTextEntry = YES;
//        _inviteCode.userInteractionEnabled = YES;
//        [self.tableBody addSubview:_inviteCode];
//
//        UIView *inviteRightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 37, HEIGHT)];
//        _inviteCode.leftView = inviteRightView;
//        _inviteCode.leftViewMode = UITextFieldViewModeAlways;
//        UIImageView *inviteRiIgView = [[UIImageView alloc] initWithFrame:CGRectMake(2, HEIGHT/2-11, 22, 22)];
//        inviteRiIgView.image = [UIImage imageNamed:@"password"];
//        inviteRiIgView.contentMode = UIViewContentModeScaleAspectFit;
//        [inviteRightView addSubview:inviteRiIgView];
//
//        verticalLine = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(inviteRightView.frame)-4, _inviteCode.frame.size.width, 0.5)];
//        verticalLine.backgroundColor = HEXCOLOR(0xD6D6D6);
//        [inviteRightView addSubview:verticalLine];
//
//        n = n+HEIGHT;
//    }
//
////        //图片验证码
////        _imgCode = [UIFactory createTextFieldWith:CGRectMake(distance, n, self_width-distance*2-70-INSETS-35-4, HEIGHT) delegate:self returnKeyType:UIReturnKeyNext secureTextEntry:NO placeholder:Localized(@"JX_inputImgCode") font:g_factory.font16];
////        _imgCode.attributedPlaceholder = [[NSAttributedString alloc] initWithString:Localized(@"JX_inputImgCode") attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
////        _imgCode.borderStyle = UITextBorderStyleNone;
////        _imgCode.clearButtonMode = UITextFieldViewModeWhileEditing;
////        [self.tableBody addSubview:_imgCode];
////
////        UIView *imCView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 37, HEIGHT)];
////        _imgCode.leftView = imCView;
////        _imgCode.leftViewMode = UITextFieldViewModeAlways;
////        UIImageView *imCIView = [[UIImageView alloc] initWithFrame:CGRectMake(2, HEIGHT/2-11, 22, 22)];
////        imCIView.image = [UIImage imageNamed:@"verify"];
////        imCIView.contentMode = UIViewContentModeScaleAspectFit;
////        [imCView addSubview:imCIView];
////
////        UIView *imCLine = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT-LINE_WH, _phone.frame.size.width, LINE_WH)];
////        imCLine.backgroundColor = THE_LINE_COLOR;
////        [_imgCode addSubview:imCLine];
////
////        _imgCodeImg = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_imgCode.frame)+INSETS, 0, 70, 35)];
////        _imgCodeImg.center = CGPointMake(_imgCodeImg.center.x, _imgCode.center.y);
////        _imgCodeImg.userInteractionEnabled = YES;
////        [self.tableBody addSubview:_imgCodeImg];
////
////        UIView *imgCodeLine = [[UIView alloc] initWithFrame:CGRectMake(_imgCodeImg.frame.size.width, 3, LINE_WH, _imgCodeImg.frame.size.height-6)];
////        imgCodeLine.backgroundColor = THE_LINE_COLOR;
////        [_imgCodeImg addSubview:imgCodeLine];
////
////        _graphicButton = [UIButton buttonWithType:UIButtonTypeCustom];
////        _graphicButton.frame = CGRectMake(CGRectGetMaxX(_imgCodeImg.frame)+6, 7, 26, 26);
////        _graphicButton.center = CGPointMake(_graphicButton.center.x,_imgCode.center.y);
////        [_graphicButton setBackgroundImage:[UIImage imageNamed:@"refreshGraphic"] forState:UIControlStateNormal];
////        [_graphicButton setBackgroundImage:[UIImage imageNamed:@"refreshGraphic"] forState:UIControlStateHighlighted];
////        [_graphicButton addTarget:self action:@selector(refreshGraphicAction:) forControlEvents:UIControlEventTouchUpInside];
////        [self.tableBody addSubview:_graphicButton];
////        if ([g_config.isOpenSMSCode boolValue] && [g_config.regeditPhoneOrName intValue] != 1) {
////            n = n+HEIGHT;
////        }else {
////            n = n+INSETS;
////        }
//#ifdef IS_TEST_VERSION
//#else
//#endif
//
//    _code = [[UITextField alloc] initWithFrame:CGRectMake(distance, n, ManMan_SCREEN_WIDTH-75-distance*2, HEIGHT)];
//    _code.attributedPlaceholder = [[NSAttributedString alloc] initWithString:Localized(@"JX_InputMessageCode") attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
//    _code.font = g_factory.font16;
//    _code.delegate = self;
//    _code.autocorrectionType = UITextAutocorrectionTypeNo;
//    _code.autocapitalizationType = UITextAutocapitalizationTypeNone;
//    _code.enablesReturnKeyAutomatically = YES;
//    _code.borderStyle = UITextBorderStyleNone;
//    _code.returnKeyType = UIReturnKeyDone;
//    _code.clearButtonMode = UITextFieldViewModeWhileEditing;
//
//    UIView *codeView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 37, HEIGHT)];
//    _code.leftView = codeView;
//    _code.leftViewMode = UITextFieldViewModeAlways;
//    UIImageView *codeIView = [[UIImageView alloc] initWithFrame:CGRectMake(2, HEIGHT/2-11, 22, 22)];
//    codeIView.image = [UIImage imageNamed:@"code"];
//    codeIView.contentMode = UIViewContentModeScaleAspectFit;
//    [codeView addSubview:codeIView];
//
//    UIView *codeILine = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT-LINE_WH, _phone.frame.size.width, LINE_WH)];
//    codeILine.backgroundColor = THE_LINE_COLOR;
//    [_code addSubview:codeILine];
//
//
//    [self.tableBody addSubview:_code];
//
//    _send = [UIFactory createButtonWithTitle:Localized(@"JX_Send")
//                                   titleFont:g_factory.font16
//                                  titleColor:[UIColor whiteColor]
//                                      normal:nil
//                                   highlight:nil ];
//    _send.frame = CGRectMake(ManMan_SCREEN_WIDTH-70-distance-11, n+HEIGHT/2-15, 70, 30);
//    [_send addTarget:self action:@selector(sendSMS) forControlEvents:UIControlEventTouchUpInside];
//    _send.backgroundColor = g_theme.themeColor;
//    _send.layer.masksToBounds = YES;
//    _send.layer.cornerRadius = 7.f;
//    [self.tableBody addSubview:_send];
//
//    //测试版隐藏了短信验证
//    if ([g_config.isOpenSMSCode boolValue] && [g_config.regeditPhoneOrName intValue] != 1) {
//        n = n+HEIGHT+INSETS+INSETS;
//    }else {
//        _send.hidden = YES;
//        _code.hidden = YES;
////            _imgCode.hidden = YES;
//        _imgCodeImg.hidden = YES;
//        _graphicButton.hidden = YES;
//    }
//#ifdef IS_TEST_VERSION
//#else
//#endif
//
//    // 返回登录
//    CGSize size = [Localized(@"JX_HaveAccountLogin") boundingRectWithSize:CGSizeMake(MAXFLOAT, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:g_factory.font16} context:nil].size;
//    UIButton *goLoginBtn = [[UIButton alloc] initWithFrame:CGRectMake(distance, n, size.width+4, size.height)];
//    [goLoginBtn setTitle:Localized(@"JX_HaveAccountLogin") forState:UIControlStateNormal];
//    goLoginBtn.titleLabel.font = g_factory.font16;
//    [goLoginBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//    [goLoginBtn addTarget:self action:@selector(goToLoginVC) forControlEvents:UIControlEventTouchUpInside];
//    [self.tableBody addSubview:goLoginBtn];
//#ifdef IS_Skip_SMS
//        // 跳过当前界面进入下个界面
//        CGSize skipSize = [Localized(@"JX_NotGetSMSCode") boundingRectWithSize:CGSizeMake(MAXFLOAT, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:g_factory.font16} context:nil].size;
//        _skipBtn = [[UIButton alloc] initWithFrame:CGRectMake(ManMan_SCREEN_WIDTH-distance-skipSize.width, n, skipSize.width+4, skipSize.height)];
//        [_skipBtn setTitle:Localized(@"JX_NotGetSMSCode") forState:UIControlStateNormal];
//        _skipBtn.titleLabel.font = g_factory.font16;
//        _skipBtn.hidden = YES;
//        [_skipBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//        [_skipBtn addTarget:self action:@selector(enterNextPage) forControlEvents:UIControlEventTouchUpInside];
//        [self.tableBody addSubview:_skipBtn];
//#else
//
//#endif
//    //弃用手机验证
//    //#ifdef IS_TEST_VERSION
//    //        UIButton* _btn = [UIFactory createCommonButton:@"下一步" target:self action:@selector(onTest)];
//    //#else
//    //        UIButton* _btn = [UIFactory createCommonButton:@"下一步" target:self action:@selector(onClick)];
//    //#endif
//    //新添加的手机验证（注册）
//    n = n+HEIGHT+INSETS;
//
//    UIButton * _btn = [UIFactory createCommonButton:Localized(@"REGISTERS") target:self action:@selector(checkPhoneNumber)];
//    [_btn.titleLabel setFont:g_factory.font16];
//    _btn.frame = CGRectMake(40, n,ManMan_SCREEN_WIDTH-40*2, 40);
//    _btn.layer.masksToBounds = YES;
//    _btn.layer.cornerRadius = 7.f;
//    [self.tableBody addSubview:_btn];
//    n = n+HEIGHT+INSETS;
//
//    UILabel *agreeLab = [[UILabel alloc] init];
//    agreeLab.font = SYSFONT(13);
//    agreeLab.text = @"我已阅读并同意";//Localized(@"JX_ByRegisteringYouAgree");
//    agreeLab.textColor = [UIColor blackColor];
//    agreeLab.userInteractionEnabled = YES;
//    [self.tableBody addSubview:agreeLab];
//    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didAgree)];
//    [agreeLab addGestureRecognizer:tap1];
//
//    UILabel*termsLab = [[UILabel alloc] init];
//    termsLab.text =@"《用户协议》";// Localized(@"《Privacy Policy and Terms of Service》");
//    termsLab.font = SYSFONT(13);
//    termsLab.textColor = [UIColor redColor];
//    termsLab.userInteractionEnabled = YES;
//    [self.tableBody addSubview:termsLab];
//    UITapGestureRecognizer *tapT = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(checkTerms)];
//    [termsLab addGestureRecognizer:tapT];
//    CGSize sizeA = [agreeLab.text sizeWithAttributes:@{NSFontAttributeName:agreeLab.font}];
//    CGSize sizeT = [termsLab.text sizeWithAttributes:@{NSFontAttributeName:termsLab.font}];
//
//    UILabel*toLabel = [[UILabel alloc] init];
//    toLabel.text =@"以及";// Localized(@"《Privacy Policy and Terms of Service》");
//    toLabel.font = SYSFONT(13);
//    toLabel.textColor = [UIColor blackColor];
//    toLabel.userInteractionEnabled = YES;
//    [self.tableBody addSubview:toLabel];
//
//
//    UIImageView *agreeNotImgV = [[UIImageView alloc] initWithFrame:CGRectMake(32, n-6, 25, 25)];
//    agreeNotImgV.image = [UIImage imageNamed:@"registered_not_agree"];
//    agreeNotImgV.userInteractionEnabled = YES;
//    [self.tableBody addSubview:agreeNotImgV];
//    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didAgree)];
//    [agreeNotImgV addGestureRecognizer:tap2];
//    agreeLab.frame = CGRectMake(CGRectGetMaxX(agreeNotImgV.frame), n, sizeA.width, sizeA.height);
//    termsLab.frame = CGRectMake(CGRectGetMaxX(agreeLab.frame), n, sizeT.width, sizeT.height);
//
//    toLabel.frame = CGRectMake(CGRectGetMaxX(termsLab.frame), n, 30, sizeT.height);
//
//    UILabel*yinSLabel = [[UILabel alloc] init];
//    yinSLabel.text =@"《隐私政策》";// Localized(@"《Privacy Policy and Terms of Service》");
//    yinSLabel.font = SYSFONT(13);
//    yinSLabel.textColor = [UIColor redColor];
//    yinSLabel.userInteractionEnabled = YES;
//    [self.tableBody addSubview:yinSLabel];
//    yinSLabel.frame = CGRectMake(CGRectGetMaxX(toLabel.frame), n, sizeA.width, sizeT.height);
//
//    UITapGestureRecognizer *rightBtnLView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(leftIMClick)];
//    [yinSLabel addGestureRecognizer:rightBtnLView];
//
//    _agreeImgV = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMinX(agreeNotImgV.frame), n-10, 25, 30)];
//    _agreeImgV.image = [UIImage imageNamed:@"registered_agree"];
//    _agreeImgV.userInteractionEnabled = YES;
//    [self.tableBody addSubview:_agreeImgV];
//    UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didAgree)];
//    [_agreeImgV addGestureRecognizer:tap3];
//
//    //添加提示
////        UIImageView * careImage = [[UIImageView alloc]initWithFrame:CGRectMake(INSETS, n+HEIGHT+INSETS+3, INSETS, INSETS)];
////        careImage.image = [UIImage imageNamed:@"noread"];
////
////        UILabel * careTitle = [[UILabel alloc]initWithFrame:CGRectMake(INSETS*2+2, n+HEIGHT+INSETS, 100, HEIGHT/3)];
////        careTitle.font = [UIFont fontWithName:@"Verdana" size:13];
////        careTitle.text = Localized(@"inputPhoneVC_BeCareful");
////
////        n = n+HEIGHT+INSETS;
////        UILabel * careFirst = [[UILabel alloc]initWithFrame:CGRectMake(INSETS*2+2, n+HEIGHT/3+INSETS, ManMan_SCREEN_WIDTH-12-INSETS*2, HEIGHT/3)];
////        careFirst.font = [UIFont fontWithName:@"Verdana" size:11];
////        careFirst.text = Localized(@"inputPhoneVC_NotNeedCode");
////
////        n = n+HEIGHT/3+INSETS;
////        UILabel * careSecond = [[UILabel alloc]initWithFrame:CGRectMake(INSETS*2+2, n+HEIGHT/3+INSETS, ManMan_SCREEN_WIDTH-INSETS*2-12, HEIGHT/3+15)];
////        careSecond.font = [UIFont fontWithName:@"Verdana" size:11];
////        careSecond.text = Localized(@"inputPhoneVC_NoReg");
////        careSecond.numberOfLines = 0;
//
//    //测试版隐藏了短信验证
//#ifdef IS_TEST_VERSION
//#else
////        careFirst.hidden = YES;
////        careSecond.hidden = YES;
////        careImage.hidden = YES;
////        careTitle.hidden = YES;
//#endif
////        [self.tableBody addSubview:careImage];
////        [self.tableBody addSubview:careTitle];
////        [self.tableBody addSubview:careFirst];
////        [self.tableBody addSubview:careSecond];
//
//
//}

- (void)didAgree {
    _agreeImgV.hidden = !_agreeImgV.hidden;
}
- (void)rightBtnL {
    self.knowBackView.hidden=YES;

    WKWebViewViewController *vc=[WKWebViewViewController new];
    vc.titleStr = [NSString stringWithFormat:@"http://%@:8092/pages/PrivacyPolicy.html", APIURL];
    [g_navigation pushViewController:vc animated:YES];
}

- (void)leftIMClick{

    self.knowBackView.hidden=YES;
    WKWebViewViewController *vc=[WKWebViewViewController new];
    vc.titleStr = [NSString stringWithFormat:@"http://%@:8092/pages/PrivacyPolicy.html", APIURL];
    [g_navigation pushViewController:vc animated:YES];
    
    
}
- (void)checkTerms {
    
   self.knowBackView.hidden=YES;

   WKWebViewViewController *vc=[WKWebViewViewController new];
    vc.titleStr = [NSString stringWithFormat:@"http://%@:8092/pages/UserPolicy.html", APIURL];
    
   [g_navigation pushViewController:vc animated:YES];

    return;;
    webpageVC * webVC = [webpageVC alloc];
    webVC.url = [self protocolUrl];
    webVC.isSend = NO;
    webVC = [webVC init];
    [g_navigation.navigationView addSubview:webVC.view];
}
-(NSString *)protocolUrl{
    NSString * protocolStr = g_config.privacyPolicyPrefix;
    NSString * lange = g_constant.sysLanguage;
    if (![lange isEqualToString:ZHHANTNAME] && ![lange isEqualToString:NAME]) {
        lange = ENNAME;
    }
    return [NSString stringWithFormat:@"%@%@.html",protocolStr,lange];
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (_phone == textField) {
        return [self validateNumber:string];
    }
    if (self.phoneTextField == textField) {
        return [self phoneIsNumber:string];
    }
    if ([self.codeTFArray containsObject:textField]) {
        return [self phoneIsNumber:string];
    }
    return YES;
}
- (BOOL)phoneIsNumber:(NSString*)number {
   NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
   NSString *filtered = [[number componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
   return [number isEqualToString:filtered];
}
- (BOOL)validateNumber:(NSString*)number {
    if ([g_config.regeditPhoneOrName intValue] == 1) {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"] invertedSet];
        NSString *filtered = [[number componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        return [number isEqualToString:filtered];
    }
    BOOL res = YES;
    NSCharacterSet *tmpSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    int i = 0;
    while (i < number.length) {
        NSString *string = [number substringWithRange:NSMakeRange(i, 1)];
        NSRange range = [string rangeOfCharacterFromSet:tmpSet];
        if (range.length == 0) {
            res = NO;
            break;
        }
        i ++;
    }
    return res;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
- (void)enterNextPage {
    _isSkipSMS = YES;
    BOOL isMobile = [self isMobileNumber:_phone.text];
    if ([_pwd.text length] < 6) {
        [g_App showAlert:Localized(@"JX_TurePasswordAlert")];
        return;
    }
    if (isMobile) {
//        NSString *areaCode = [_areaCodeBtn.titleLabel.text stringByReplacingOccurrencesOfString:@"+" withString:@""];
        [g_server checkPhone:_phone.text areaCode:_areaIDStr verifyType:0 toView:self];
    }
}
- (void)textFieldDidChanged:(UITextField *)textField {
    
    
    if (textField == _phone) {
        if ([g_config.regeditPhoneOrName intValue] == 1) {
            if (_phone.text.length > 11) {
                _phone.text = [_phone.text substringToIndex:11];
            }
        }else {
            if (_phone.text.length > 11) {
                _phone.text = [_phone.text substringToIndex:11];
            }
        }
    }
    if (textField == self.phoneTextField) {
        if ([g_config.regeditPhoneOrName intValue] == 1) {
            if (self.phoneTextField.text.length > 11) {
                self.phoneTextField.text = [self.phoneTextField.text substringToIndex:11];
            }
        }else {
            if (self.phoneTextField.text.length > 11) {
                self.phoneTextField.text = [self.phoneTextField.text substringToIndex:11];
            }
        }
    }
    if (textField == _pwd) {
        if (_pwd.text.length > 17) {
            _pwd.text = [_pwd.text substringToIndex:17];
        }
    }
    if (textField == self.passwordTextField) {
        if (self.passwordTextField.text.length > 17) {
            self.passwordTextField.text = [self.passwordTextField.text substringToIndex:17];
        }
    }
    
    if ([self.codeTFArray containsObject:textField]) {
        self.enterCount = textField.tag - 10;
        if (textField.text.length > 1) {
           textField.text = [textField.text substringToIndex:1];
        }
        if (textField.text.length == 1 && self.enterCount!=5) {
            textField.backgroundColor = HEXCOLOR(0x05D168);
            
            if (self.codeTFArray[self.enterCount+1].text.length == 1) {
                [textField resignFirstResponder];
            }else{
                [self.codeTFArray[self.enterCount+1] becomeFirstResponder];
            }
            
        }
        if (textField.tag == 15 && textField.text.length == 1) {
            [textField resignFirstResponder];
            textField.backgroundColor = HEXCOLOR(0x05D168);
            
            [_timer invalidate];
            [self.sureCodeBtn setTitle:@"下一步" forState:UIControlStateNormal];
            self.sureCodeBtn.userInteractionEnabled = YES;
            self.sureCodeBtn.backgroundColor = HEXCOLOR(0x05D168);
        }
        
    }
//    if ([self.codeTFArray containsObject:textField]) {
//        self.enterCount = textField.tag - 10;
//        if (textField.text.length > 1) {
//           textField.text = [textField.text substringToIndex:1];
//        }
//        if (textField.text.length == 1 && self.enterCount!=5) {
//            textField.backgroundColor = HEXCOLOR(0x05D168);
//            [self.codeTFArray[self.enterCount+1] becomeFirstResponder];
//
//        }
//        if (textField.tag == 15 && textField.text.length == 1) {
//            [textField resignFirstResponder];
//            textField.backgroundColor = HEXCOLOR(0x05D168);
//            [_timer invalidate];
//            [self.sureCodeBtn setTitle:@"下一步" forState:UIControlStateNormal];
//            self.sureCodeBtn.userInteractionEnabled = YES;
//            self.sureCodeBtn.backgroundColor = HEXCOLOR(0x05D168);
//
//        }
//
//    }
}
- (void)goToLoginVC {
    if (self.isThirdLogin) {
        JY_loginVC *login = [JY_loginVC alloc];
        login.isThirdLogin = YES;
        login.isAutoLogin = NO;
        login.isSwitchUser= NO;
        login.type = self.type;
        login = [login init];
        [g_navigation pushViewController:login animated:YES];
    }else {
        [self actionQuit];
    }
}
- (void)checkPhoneNumber{
    
    [_phone resignFirstResponder];
    [_pwd resignFirstResponder];
//    [_imgCode resignFirstResponder];
    [_code resignFirstResponder];
    _isSkipSMS = NO;
    BOOL isMobile = [self isMobileNumber:_phone.text];
    if (_pageStyle == ReisterPageStyleNone) {
        if ([_pwd.text length] < 6) {
            [g_App showAlert:Localized(@"JX_TurePasswordAlert")];
            return;
        }
        if ([g_config.registerInviteCode intValue] == 1) {
            if ([_inviteCode.text length] <= 0) {
                [g_App showAlert:Localized(@"JX_EnterInvitationCode")];
                return;
            }
        }
    }
    if (_isNext == NO) {
        if([_code.text length]<6){
            if([_code.text length]<=0){
                [g_App showAlert:@"请输入短信验证码"];
                return;
            }
            [g_App showAlert:Localized(@"inputPhoneVC_MsgCodeNotOK")];
            return;
        }
    }
    if (_pageStyle == ReisterPageStyleNone) {
        if (isMobile) {
    //        NSString *areaCode = [_areaCodeBtn.titleLabel.text stringByReplacingOccurrencesOfString:@"+" withString:@""];
            [g_server checkPhone:_phone.text areaCode:_areaIDStr verifyType:0 toView:self];
        }
    }else{
        NSString *telephone = [NSString stringWithFormat:@"%@%@",_areaIDStr,_phone.text];
        [g_server updateTelephone:_phone.text telephone:telephone smscode:_code.text toView:self];
    }
}
- (BOOL)isMobileNumber:(NSString *)number{
//    if ([g_config.isOpenSMSCode boolValue] && [g_config.regeditPhoneOrName intValue] != 1) {
//        if ([_phone.text length] == 0) {
//            [g_App showAlert:Localized(@"JX_InputPhone")];
//            return NO;
//        }
//    }
    if ([g_config.isOpenSMSCode boolValue] && [g_config.regeditPhoneOrName intValue] != 1) {
        if ([number length] == 0) {
            [g_App showAlert:Localized(@"JX_InputPhone")];
            return NO;
        }
    }
    
#ifdef IS_TEST_VERSION
#else
#endif
    return YES;
}
-(void)refreshGraphicAction:(UIButton *)button{
    [self getImgCodeImg];
}
-(void)getImgCodeImg{
    if([self isMobileNumber:self.phoneTextField.text]){//[self isMobileNumber:_phone.text]
//        NSString *areaCode = [_areaCodeBtn.titleLabel.text stringByReplacingOccurrencesOfString:@"+" withString:@""];
//        NSString * codeUrl = [g_server getImgCode:_phone.text areaCode:_areaIDStr];
        NSString * codeUrl = [g_server getImgCode:self.phoneTextField.text areaCode:_areaIDStr];
        NSURLRequest * request = [NSURLRequest requestWithURL:[NSURL URLWithString:codeUrl] cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:10.0];
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
            if (!connectionError) {
                UIImage * codeImage = [UIImage imageWithData:data];
                _imgCodeImg.image = codeImage;
            }else{
                NSLog(@"%@",connectionError);
                [g_App showAlert:connectionError.localizedDescription];
            }
        }];
    }else{
    }
}
#pragma mark----验证短信验证码
-(void)onClick{
    if([self.phoneTextField.text length]<=0){
        [g_App showAlert:Localized(@"JX_InputPhone")];
        return;
    }
    if (!_isSkipSMS) {
        NSString *codeStr = @"";
        for (UITextField *tf in self.codeTFArray) {
            codeStr = [codeStr stringByAppendingString:tf.text];
        }
        
        if([codeStr length]<6){
            if([codeStr length]<=0){
                [g_App showAlert:@"请输入短信验证码"];
                return;
            }
            [g_App showAlert:Localized(@"inputPhoneVC_MsgCodeNotOK")];
            return;
        }
        
//        if (![_phoneStr isEqualToString:_phone.text]) {
//            [g_App showAlert:Localized(@"JX_No.Changed,Again")];
//            return;
//        }
        
    }
    [self.view endEditing:YES];
    if (!_isSkipSMS) {
        
        self.isSmsRegister = YES;
        [self setUserInfo];
        

    } else {
        self.isSmsRegister = NO;
        [self setUserInfo];
    }
}


- (void)setUserInfo {
    if (_agreeImgV.isHidden == YES) {
        [g_App showAlert:Localized(@"JX_NotAgreeProtocol")];
        return;
    }
    JY_UserObject* user = [JY_UserObject sharedInstance];
    user.telephone = self.phoneTextField.text;
    user.password  = [g_server getMD5String:_passwordTextField.text];
    user.areaCode = _areaIDStr;
   
    JY_PSRegisterBaseVC* vc = [JY_PSRegisterBaseVC alloc];
    vc.password = self.passwordTextField.text;
    vc.isRegister = YES;
    vc.inviteCodeStr = _inviteCode.text;
    NSString *codeStr = @"";
    for (UITextField *tf in self.codeTFArray) {
        codeStr = [codeStr stringByAppendingString:tf.text];
    }
    vc.smsCode = codeStr;
    vc.resumeId   = nil;
    vc.isSmsRegister = self.isSmsRegister;
    vc.resumeModel     = [[resumeBaseData alloc]init];
    vc.user       = user;
    vc.type = self.type;
    vc = [vc init];
    [g_navigation pushViewController:vc animated:YES];
    [self actionQuit];
}
-(void)onTest{
    if (_agreeImgV.isHidden == YES) {
        [g_App showAlert:Localized(@"JX_NotAgreeProtocol")];
        return;
    }
    JY_UserObject* user = [JY_UserObject sharedInstance];
    user.telephone = self.phoneTextField.text;
    user.password  = [g_server getMD5String:self.passwordTextField.text];
    user.areaCode = _areaIDStr;
    JY_PSRegisterBaseVC* vc = [JY_PSRegisterBaseVC alloc];
    vc.password = self.passwordTextField.text;
    vc.isRegister = YES;
    vc.inviteCodeStr = _inviteCode.text;
    NSString *codeStr = @"";
    for (UITextField *tf in self.codeTFArray) {
        codeStr = [codeStr stringByAppendingString:tf.text];
    }
    vc.smsCode = codeStr;
    vc.resumeId   = nil;
    vc.isSmsRegister = NO;
    vc.resumeModel     = [[resumeBaseData alloc]init];
    vc.user       = user;
    vc.type = self.type;
    vc = [vc init];
    [g_navigation pushViewController:vc animated:YES];
    [self actionQuit];
}
-(void) didServerResultSucces:(JY_Connection*)aDownload dict:(NSDictionary*)dict array:(NSArray*)array1{
    [_wait stop];
  
    if ([aDownload.action isEqualToString:act_UserSMSLogin]) {
//        NSString *codeStr = @"";
//        for (UITextField *tf in self.codeTFArray) {
//            codeStr = [codeStr stringByAppendingString:tf.text];
//        }
//        NSString *dataStr = [dict objectForKey:@"data"];
//        NSData *data = [[NSData alloc] initWithBase64EncodedString:dataStr options:NSDataBase64DecodingIgnoreUnknownCharacters];
//        NSData *jsonData = [AESUtil decryptAESData:data key:[MD5Util getMD5DataWithString:codeStr]];
//        NSString *json = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
//        SBJsonParser * resultParser = [[SBJsonParser alloc] init];
//        NSDictionary *resultObject = [resultParser objectWithString:json];
////        if( [self.toView respondsToSelector:@selector(didServerResultSucces:dict:array:)] )
////            [self.toView didServerResultSucces:aDownload dict:resultObject array:nil];
//        
        [g_server doLoginOK:dict user:_user];
        [g_App showMainUI];
    }
    if([aDownload.action isEqualToString:user_getAccountStatus]){
        NSLog(@"%@",aDownload.responseData);
        NSData *jsonData = [aDownload.responseData dataUsingEncoding:NSUTF8StringEncoding];
        NSError *err;
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&err];
        NSInteger data =[dic[@"data"] intValue];
        if (data == 0) {
            //没注册
            isRegistered = NO;
            [self showSetPassword];
        }else{
            //注册了
            isRegistered = YES;
            [self needPassword:NO];
            
            
        }
    }
    if([aDownload.action isEqualToString:act_tixianRandcodeSendSms]){
        [JY_MyTools showTipView:Localized(@"JXAlert_SendOK")];
        _send.selected = YES;
        _send.userInteractionEnabled = NO;
        _send.backgroundColor = [UIColor grayColor];
        [_send setTitle:@"60s" forState:UIControlStateSelected];
        _phoneStr = _phone.text;
    }
    if ([aDownload.action isEqualToString:act_updateTelephone]) {
        //修改成功
        [JY_MyTools showTipView:Localized(@"JXAlert_UpdateOK")];
        [self actionQuit];
    }
    if([aDownload.action isEqualToString:act_CheckPhone]){
        
        if (_isNext) {
            _isNext = NO;
            if (_pageStyle == ReisterPageStyleNone) {
//                [self setNextView];
                [self showEnterCodeView];
                
            }
            
            return;
        }
        //检测手机号
        if (self.isCheckToSMS) {
            self.isCheckToSMS = NO;
            [self onSend];
            return;
        }
        if ([g_config.isOpenSMSCode boolValue] && [g_config.regeditPhoneOrName intValue] != 1) {
            [self onClick];
        }else {
            [self onTest];
        }
    }
}
-(int) didServerResultFailed:(JY_Connection*)aDownload dict:(NSDictionary*)dict{
    [_wait stop];
    if([aDownload.action isEqualToString:act_SendSMS]){
        [_send setTitle:Localized(@"JX_Send") forState:UIControlStateNormal];
        [g_App showAlert:dict[@"resultMsg"]];
        [self getImgCodeImg];
        return hide_error;
    }
    return show_error;
}
-(int) didServerConnectError:(JY_Connection*)aDownload error:(NSError *)error{
    [_wait stop];
    return show_error;
}
-(void) didServerConnectStart:(JY_Connection*)aDownload{
    [_wait stop];
}
-(void)showNewTimer:(NSTimer *)timer{
    UIButton *but =self.sureCodeBtn;// (UIButton*)[_timer userInfo];
    _seconds--;
    [but setTitle:[NSString stringWithFormat:@"%d秒后重新发送",_seconds] forState:UIControlStateNormal];
    if(_seconds<=0){
        but.selected = NO;
        but.userInteractionEnabled = YES;
        but.backgroundColor = HEXCOLOR(0x05D168);
        [but setTitle:@"重新发送" forState:UIControlStateNormal];
        if (_timer) {
            _timer = nil;
            [timer invalidate];
        }
    }
}
-(void)showTime:(NSTimer*)sender{
    UIButton *but = (UIButton*)[_timer userInfo];
    _seconds--;
    [but setTitle:[NSString stringWithFormat:@"%ds",_seconds] forState:UIControlStateSelected];
    if (_isSendFirst) {
        _isSendFirst = NO;
        _skipBtn.hidden = YES;
    }
    if (_seconds <= 30) {
        _skipBtn.hidden = NO;
    }
    if(_seconds<=0){
        but.selected = NO;
        but.userInteractionEnabled = YES;
        but.backgroundColor = g_theme.themeColor;
        [_send setTitle:Localized(@"JX_SendAngin") forState:UIControlStateNormal];
        if (_timer) {
            _timer = nil;
            [sender invalidate];
        }
        _seconds = 60;
    }
}



- (void)sendSMS{
    [_phone resignFirstResponder];
    [_pwd resignFirstResponder];
//    [_imgCode resignFirstResponder];
    [_code resignFirstResponder];
    if([self isMobileNumber:_phone.text]){
        if (_pageStyle == ReisterPageStyleEditAccout) {
            [self onSend];
        }else{
            self.isCheckToSMS = YES;
            [g_server checkPhone:_phone.text areaCode:_areaIDStr verifyType:0 toView:self];
        }
    }
}
-(void)onSend{
    if (!_send.selected) {
        [_wait start:Localized(@"JX_Testing")];
        _user = [JY_UserObject sharedInstance];
        _user.areaCode = _areaIDStr;
    //不带图形验证
        NSString *phone = [NSString stringWithFormat:@"%@",self.phoneTextField.text];
        if (_pageStyle == ReisterPageStyleEditAccout) {
            phone = g_myself.telephone;
            [g_server sendSMSOutImage:phone areaCode:_areaIDStr isRegister:10 toView:self];
        }else{
            [g_server sendSMSOutImage:phone areaCode:_areaIDStr isRegister:0 toView:self];
        }
        [_send setTitle:Localized(@"JX_Sending") forState:UIControlStateNormal];
    }
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    if ([g_config.isOpenSMSCode boolValue] && [g_config.regeditPhoneOrName intValue] != 1) {
        if (textField == _phone) {
            [self getImgCodeImg];
        }
    }
    if ([g_config.isOpenSMSCode boolValue] && [g_config.regeditPhoneOrName intValue] != 1) {
        if (textField == self.phoneTextField) {
            [self getImgCodeImg];
        }
    }
    if ([self.codeTFArray containsObject:textField]) {
        if (textField.text.length == 0) {
            textField.backgroundColor = HEXCOLOR(0xF0F0F0);
        }else if (textField.text.length == 1){
            textField.backgroundColor = HEXCOLOR(0x05D168);
        }
        textField.layer.shadowColor = [UIColor clearColor].CGColor;
        textField.layer.masksToBounds = YES;
        
    }
    
#ifndef IS_TEST_VERSION
#endif
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if ([self.codeTFArray containsObject:textField]) {
        textField.text = @"";
        textField.backgroundColor = [UIColor whiteColor];
        textField.layer.masksToBounds = NO;
        textField.layer.shadowColor = HEXCOLOR(0xF0F0F0).CGColor;
        textField.layer.shadowOffset = CGSizeMake(0,2.5);
        textField.layer.shadowOpacity = 1;
        textField.layer.shadowRadius = 7;
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}
- (void)areaCodeBtnClick{
    [self.view endEditing:YES];
    JY_TelAreaListVC *telAreaListVC = [[JY_TelAreaListVC alloc] init];
    telAreaListVC.telAreaDelegate = self;
    telAreaListVC.didSelect = @selector(didSelectTelArea:);
    [g_navigation pushViewController:telAreaListVC animated:YES];
}
- (void)didSelectTelArea:(NSString *)areaCode{
    NSMutableArray *telAreaArray = [g_constant.telArea mutableCopy];
    NSString *areaStr;
    _areaIDNum = areaCode;
     for (NSDictionary *dict in telAreaArray) {
         if (areaCode.integerValue == [dict[@"prefix"] integerValue]) {
             areaStr = [NSString stringWithFormat:@"+%@",areaCode];
             _areaIDStr = areaCode;
         }
     }
    _area.text = areaStr;
    [_areaCodeBtn setTitle:areaStr forState:UIControlStateNormal];
}
- (void)resetBtnEdgeInsets:(UIButton *)btn{
    [btn setTitleEdgeInsets:UIEdgeInsetsMake(0, -btn.imageView.frame.size.width-2, 0, btn.imageView.frame.size.width+2)];
    [btn setImageEdgeInsets:UIEdgeInsetsMake(0, btn.titleLabel.frame.size.width+2, 0, -btn.titleLabel.frame.size.width-2)];
}
- (void)hideKeyBoardToView {
    [self.view endEditing:YES];
}




#pragma mark -- 协议条款
-(void)iniframeProtocal{
     
    JXTermsPrivacyVc *showImgView = [JXTermsPrivacyVc XIBJXTermsPrivacyVc];
    showImgView.blockBtn = ^(UIButton *sender) {
      
        if (sender.tag ==1) {
            [self rightBtnL];
        }else if (sender.tag ==2){
            
            [self leftIMClick];
        }else{
            
            [self cancelBtnClick];
        }
        
    };
    [self.view addSubview:showImgView];
    showImgView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    self.showImgView=showImgView;
 
 return;
    
    UIView *backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    backView.userInteractionEnabled=YES;
    backView.backgroundColor=[UIColor colorWithWhite:0.0 alpha:0.5];
    [[UIApplication sharedApplication].keyWindow addSubview:backView];
    self.backView=backView;
    
    
    UIView *leftIM=[[UIView alloc]init];
    leftIM.backgroundColor=[UIColor whiteColor];
    leftIM.userInteractionEnabled=YES;
    [backView addSubview:leftIM];
    leftIM.frame=CGRectMake(20, SCREEN_WIDTH/2, SCREEN_WIDTH-40, SCREEN_WIDTH-40);
    leftIM.layer.cornerRadius=15;
    leftIM.layer.masksToBounds=YES;
    [backView addSubview:leftIM];
    
    UILabel *titleLable=[[UILabel alloc]init];
    titleLable.text=@"隐私政策";
    titleLable.font=[UIFont boldSystemFontOfSize:20];
    titleLable.textAlignment=NSTextAlignmentCenter;
    titleLable.frame=CGRectMake(10, 30, SCREEN_WIDTH-60, 20);
    [leftIM addSubview:titleLable];
    
    UILabel *subtitle=[[UILabel alloc]init];
    subtitle.text=@"        欢迎使用聚友免费聊天，为了更好地保护您的隐私和个人信息安全，根据国家相关法律规定拟定了";
    subtitle.numberOfLines=3;
    subtitle.frame=CGRectMake(10, 64, SCREEN_WIDTH-60,66);
    [leftIM addSubview:subtitle];
     
    
    UIButton *leftBtn=[[UIButton alloc]init];
    [leftBtn setTitle:@"《隐私政策》" forState:UIControlStateNormal];
    [leftBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    leftBtn.layer.cornerRadius=5;
    leftBtn.layer.masksToBounds=YES;
    [leftIM addSubview:leftBtn];
    leftBtn.frame=CGRectMake(10, subtitle.bottom+5, 111, 15);
    [leftBtn addTarget:self action:@selector(leftIMClick) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *heLabel=[[UILabel alloc]init];
    heLabel.text=@"和";
    heLabel.textAlignment=NSTextAlignmentCenter;
    heLabel.frame=CGRectMake(leftBtn.right+5, subtitle.bottom+5, 30,15);
    [leftIM addSubview:heLabel];
    
    UIButton *rightBtn=[[UIButton alloc]init];
    [rightBtn setTitle:@"《用户协议》" forState:UIControlStateNormal];
    [rightBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    rightBtn.layer.cornerRadius=5;
    rightBtn.layer.masksToBounds=YES;
    [leftIM addSubview:rightBtn];
    rightBtn.frame=CGRectMake(heLabel.right+5,subtitle.bottom+5, 111, 15);
    [rightBtn addTarget:self action:@selector(rightBtnL) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *contentLabel=[[UILabel alloc]init];
    contentLabel.text=@"请在使用前仔细阅读并同意";
    contentLabel.frame=CGRectMake(10, leftBtn.bottom+10,SCREEN_WIDTH-60 ,15);
    [leftIM addSubview:contentLabel];
    
    
       UIButton *cancelBtn=[[UIButton alloc]init];
       [cancelBtn setTitle:@"同意协议" forState:UIControlStateNormal];
       [cancelBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
       cancelBtn.backgroundColor=[UIColor redColor];
       cancelBtn.layer.cornerRadius=20;
       cancelBtn.layer.masksToBounds=YES;
       [leftIM addSubview:cancelBtn];
      cancelBtn.frame=CGRectMake(20, contentLabel.bottom+44, leftIM.width-40, 40);
       [cancelBtn addTarget:self action:@selector(cancelBtnClick) forControlEvents:UIControlEventTouchUpInside];
       
       UIButton *sureBtn=[[UIButton alloc]init];
       [sureBtn setTitle:@"不同意" forState:UIControlStateNormal];
       [sureBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
       sureBtn.backgroundColor=[UIColor lightGrayColor];
       sureBtn.layer.cornerRadius=20;
       sureBtn.layer.masksToBounds=YES;
       [leftIM addSubview:sureBtn];
       sureBtn.frame=CGRectMake(20, cancelBtn.bottom+5, leftIM.width-40, 40);;
       [sureBtn addTarget:self action:@selector(sureBtnClickaaa) forControlEvents:UIControlEventTouchUpInside];
}
- (void)sureBtnClick{
    exit(0);
}

//同意协议
- (void)cancelBtnClick{
    [self.backView removeFromSuperview];
    [self.showImgView removeFromSuperview];
}

- (void)sureBtnClickaaa{
    exit(0);
}


-(void)iniframeShow{
    
     
    UIView *backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    backView.userInteractionEnabled=YES;
    backView.backgroundColor=[UIColor colorWithWhite:0.0 alpha:0.5];
    [[UIApplication sharedApplication].keyWindow addSubview:backView];
    self.knowBackView=backView;
    
    
    UIView *leftIM=[[UIView alloc]init];
    leftIM.backgroundColor=[UIColor whiteColor];
    leftIM.userInteractionEnabled=YES;
    [backView addSubview:leftIM];
    leftIM.frame=CGRectMake(40, SCREEN_WIDTH/2, SCREEN_WIDTH-80, SCREEN_WIDTH/1.8+60);
    leftIM.layer.cornerRadius=15;
    leftIM.layer.masksToBounds=YES;
    [backView addSubview:leftIM];
    
    UILabel *titleLable=[[UILabel alloc]init];
    titleLable.text=@"隐私政策";
    titleLable.font=[UIFont boldSystemFontOfSize:20];
    titleLable.textAlignment=NSTextAlignmentCenter;
    titleLable.frame=CGRectMake(10, 30, SCREEN_WIDTH-100, 20);
    [leftIM addSubview:titleLable];
    
    UILabel *subtitle=[[UILabel alloc]init];
    subtitle.text=@"        为了更好地保护您的权益，请阅读并同意下方的";
    subtitle.numberOfLines=3;
    subtitle.frame=CGRectMake(10, 64, SCREEN_WIDTH-100,66);
    [leftIM addSubview:subtitle];
     
    
    UIButton *leftBtn=[[UIButton alloc]init];
    [leftBtn setTitle:@"《隐私政策》" forState:UIControlStateNormal];
    [leftBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    leftBtn.layer.cornerRadius=5;
    leftBtn.layer.masksToBounds=YES;
    [leftIM addSubview:leftBtn];
    leftBtn.frame=CGRectMake(0, subtitle.bottom, 111, 15);
    [leftBtn addTarget:self action:@selector(leftIMClick) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *heLabel=[[UILabel alloc]init];
    heLabel.text=@"和";
    heLabel.textAlignment=NSTextAlignmentCenter;
    heLabel.frame=CGRectMake(leftBtn.right, subtitle.bottom, 30,15);
    [leftIM addSubview:heLabel];
    
    UIButton *rightBtn=[[UIButton alloc]init];
    [rightBtn setTitle:@"《用户协议》" forState:UIControlStateNormal];
    [rightBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    rightBtn.layer.cornerRadius=5;
    rightBtn.layer.masksToBounds=YES;
    [leftIM addSubview:rightBtn];
    rightBtn.frame=CGRectMake(heLabel.right,subtitle.bottom, 111, 15);
    [rightBtn addTarget:self action:@selector(rightBtnL) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *contentLabel=[[UILabel alloc]init];
    contentLabel.text=@"后,再进行登录";
    contentLabel.frame=CGRectMake(8, rightBtn.bottom,SCREEN_WIDTH-60 ,22);
    [leftIM addSubview:contentLabel];
    
    
       UIButton *cancelBtn=[[UIButton alloc]init];
       [cancelBtn setTitle:@"知道了" forState:UIControlStateNormal];
       [cancelBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
       cancelBtn.backgroundColor=[UIColor redColor];
       cancelBtn.layer.cornerRadius=20;
       cancelBtn.layer.masksToBounds=YES;
       [leftIM addSubview:cancelBtn];
      cancelBtn.frame=CGRectMake(20, contentLabel.bottom+44, leftIM.width-40, 40);
       [cancelBtn addTarget:self action:@selector(knowBtn) forControlEvents:UIControlEventTouchUpInside];
      
}
- (void)knowBtn{
    
    [self.showImgView removeFromSuperview];
    [self.knowBackView removeFromSuperview];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.knowBackView.hidden=NO;
    self.showImgView.hidden =NO;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
//隐私协议

- (IBAction)usergreementClick:(id)sender {
    WKWebViewViewController *vc=[WKWebViewViewController new];
    vc.urlStr = @"用户协议";
    vc.titleStr = [NSString stringWithFormat:@"http://%@:8092/UserPolicy.html", APIURL];
    [g_navigation pushViewController:vc animated:YES];
}
- (IBAction)privacyAgreementBtnClikc:(id)sender {
    WKWebViewViewController *vc=[WKWebViewViewController new];
    vc.urlStr = @"隐私协议";
    vc.titleStr = [NSString stringWithFormat:@"http://%@:8092/PrivacyPolicy.html", APIURL];
    [g_navigation pushViewController:vc animated:YES];
}
//http://118.190.105.176:8092/PrivacyPolicy.html

- (IBAction)accountLoginBtnClick:(id)sender {
//    [self.navigationController popViewControllerAnimated:YES];
    [self actionQuit];
}
- (IBAction)retrievePasswordBtnClick:(id)sender {
    JY_forgetPwdVC* vc = [[JY_forgetPwdVC alloc] init];
    vc.isModify = NO;
    [g_navigation pushViewController:vc animated:YES];
}
- (IBAction)codeBackClick:(id)sender {
    self.codeView.hidden = YES;
}

- (IBAction)passwordBackClick:(id)sender {
    self.passwordView.hidden = YES;
    self.passwordTextField.text = @"";
}


@end
