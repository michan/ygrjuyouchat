//
//  JY_RegisterNextVC.m
//  TFJunYouChat
//
//  Created by zoja on 2021/8/18.
//  Copyright © 2021 zengwOS. All rights reserved.
//
#define HEIGHT 56
#import "JY_RegisterNextVC.h"
#import "JY_PSRegisterBaseVC.h"
@interface JY_RegisterNextVC ()<UITextFieldDelegate>
{
    NSTimer *_timer;
    UIButton *_areaCodeBtn;
    JY_UserObject *_user;
    UIImageView * _imgCodeImg;
    //    UITextField *_imgCode;
    UIButton * _graphicButton;
    UIButton* _skipBtn;
    BOOL _isSkipSMS;
    BOOL _isSendFirst;
    UIImageView * _agreeImgV;
}
@property (nonatomic, assign) BOOL isSmsRegister;
@property (nonatomic, assign) BOOL isCheckToSMS;
@property (nonatomic, weak) UIView *backView;
@property (nonatomic, weak) UIView *knowBackView;

@property(nonatomic,strong) UIButton *protocalBtn; // 去登录

@property(nonatomic,strong) UILabel *readLabel; // 去登录
@property(nonatomic,strong) UILabel *yiJiLabel; // 去登录

@property(nonatomic,strong) UIButton *selectProtocalBtn; //yonghuxieyi

@property(nonatomic,strong) UIButton *ProctProtocalBtn; // yingsi zhengce

@property (nonatomic, weak) UIButton *toLoginBtn;
//@property (nonatomic,weak) JXTermsPrivacyVc *showImgView;

@end

@implementation JY_RegisterNextVC
- (instancetype)initWithPhoneStr:(NSString *)phoneStr areaStr:(NSString *)areaStr{
    self = [super init];
    if (self) {
        self.phoneStr = phoneStr;
        self.areaStr = areaStr;
        //        _seconds = 0;
        self.isGotoBack   = YES;
        self.title = Localized(@"JX_Register");
        self.heightFooter = 0;
        self.heightHeader = ManMan_SCREEN_TOP;
        //self.view.frame = g_window.bounds;
        [self createHeadAndFoot];
        self.tableBody.backgroundColor = [UIColor whiteColor];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyBoardToView)];
        [self.tableBody addGestureRecognizer:tap];
        _isSendFirst = YES;  // 第一次发送短信
        int n = INSETS;
        int distance = 20; // 左右间距
        self.isSmsRegister = NO;
        //icon
        n += 30;
        
        UILabel *titleLab = [UIFactory createLabelWith:CGRectMake(distance,n,ManMan_SCREEN_WIDTH-distance*2 , 30) text:@"发送短信注册" font:KFontRegular(28) textColor:HEXCOLOR(0x333333) backgroundColor:nil];
        [self.tableBody addSubview:titleLab];
        n += (30+40);
        
        UILabel *phoneLab = [UIFactory createLabelWith:CGRectMake(distance,n,ManMan_SCREEN_WIDTH-distance*2 , 30) text:[NSString stringWithFormat:@"请使用手机号码+%@ %@",self.areaStr,self.phoneStr] font:KFontRegular(20) textColor:HEXCOLOR(0x333333) backgroundColor:nil];
        [self.tableBody addSubview:phoneLab];
        n += (30+20);
        
        UILabel *detailLab = [UIFactory createLabelWith:CGRectMake(distance,n,ManMan_SCREEN_WIDTH-distance*2 , 30) text:@"接受短信" font:KFontRegular(20) textColor:HEXCOLOR(0x333333) backgroundColor:nil];
        [self.tableBody addSubview:detailLab];
        
        //手机号
        n += 40+50;
        
        
        _code = [[UITextField alloc] initWithFrame:CGRectMake(distance, n, ManMan_SCREEN_WIDTH-75-distance*2, HEIGHT)];
        _code.attributedPlaceholder = [[NSAttributedString alloc] initWithString:Localized(@"JX_InputMessageCode") attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
        _code.font = g_factory.font16;
        _code.delegate = self;
        _code.autocorrectionType = UITextAutocorrectionTypeNo;
        _code.autocapitalizationType = UITextAutocapitalizationTypeNone;
        _code.enablesReturnKeyAutomatically = YES;
        _code.borderStyle = UITextBorderStyleNone;
        _code.returnKeyType = UIReturnKeyDone;
        _code.clearButtonMode = UITextFieldViewModeWhileEditing;
        
        UIView *codeView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 37, HEIGHT)];
        _code.leftView = codeView;
        _code.leftViewMode = UITextFieldViewModeAlways;
        CGFloat left_W = [UIFactory getStringWidthWithText:@"验证码" font:g_factory.font16 viewHeight:HEIGHT];
        UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, left_W + 10, HEIGHT)];
        _code.leftView = leftView;
        _code.leftViewMode = UITextFieldViewModeAlways;
        UILabel *phIgLab = [UIFactory createLabelWith:CGRectMake(0, 0, left_W, HEIGHT) text:@"验证码" font:KFontMedium(16) textColor:HEXCOLOR(0x3333333) backgroundColor:nil];
        [leftView addSubview:phIgLab];
        
        
        UIView *codeILine = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT-LINE_WH, _code.frame.size.width, LINE_WH)];
        codeILine.backgroundColor = THE_LINE_COLOR;
        [_code addSubview:codeILine];
        
        
        [self.tableBody addSubview:_code];
        
        _send = [UIFactory createButtonWithTitle:Localized(@"JX_Send")
                                       titleFont:g_factory.font16
                                      titleColor:[UIColor whiteColor]
                                          normal:nil
                                       highlight:nil ];
        _send.frame = CGRectMake(ManMan_SCREEN_WIDTH-70-distance-11, n+HEIGHT/2-15, 70, 30);
        [_send addTarget:self action:@selector(onSend) forControlEvents:UIControlEventTouchUpInside];
        _send.backgroundColor = g_theme.themeColor;
        _send.layer.masksToBounds = YES;
        _send.layer.cornerRadius = 7.f;
        [self.tableBody addSubview:_send];
        
        
        //新添加的手机验证（注册）
        n = n+HEIGHT+INSETS;
        
        UIButton * btn = [UIFactory createButtonWithTitle:Localized(@"REGISTERS") titleFont:g_factory.font16 titleColor:[UIColor whiteColor] normal:nil highlight:nil];
        
        [btn addTarget:self action:@selector(onSend) forControlEvents:UIControlEventTouchUpInside];
        btn.frame = CGRectMake(20, n,ManMan_SCREEN_WIDTH-20*2, 40);
        btn.backgroundColor = RGB(90, 202, 198);
        ViewRadius(btn, 20);
        [self.tableBody addSubview:btn];
        n = n+HEIGHT+INSETS;
    }
    return self;
}

- (id)init
{
    self = [super init];
    if (self) {
                _seconds = 0;
        self.isGotoBack   = YES;
        self.title = Localized(@"JX_Register");
        self.heightFooter = 0;
        self.heightHeader = ManMan_SCREEN_TOP;
        //self.view.frame = g_window.bounds;
        [self createHeadAndFoot];
        self.tableBody.backgroundColor = [UIColor whiteColor];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyBoardToView)];
        [self.tableBody addGestureRecognizer:tap];
        _isSendFirst = YES;  // 第一次发送短信
        int n = INSETS;
        int distance = 20; // 左右间距
        self.isSmsRegister = NO;
        //icon
        n += 30;
        
        UILabel *titleLab = [UIFactory createLabelWith:CGRectMake(distance,n,ManMan_SCREEN_WIDTH-distance*2 , 30) text:@"发送短信注册" font:KFontMedium(28) textColor:HEXCOLOR(0x333333) backgroundColor:nil];
        [self.tableBody addSubview:titleLab];
        n += (30+40);
        
        UILabel *phoneLab = [UIFactory createLabelWith:CGRectMake(distance,n,ManMan_SCREEN_WIDTH-distance*2 , 30) text:[NSString stringWithFormat:@"请使用手机号码+%@ %@",self.areaStr,self.phoneStr] font:KFontMedium(20) textColor:HEXCOLOR(0x333333) backgroundColor:nil];
        [self.tableBody addSubview:phoneLab];
        n += (30+20);
        
        UILabel *detailLab = [UIFactory createLabelWith:CGRectMake(distance,n,ManMan_SCREEN_WIDTH-distance*2 , 30) text:@"接受短信" font:KFontMedium(28) textColor:HEXCOLOR(0x333333) backgroundColor:nil];
        [self.tableBody addSubview:detailLab];
        
        //手机号
        n += 40+50;
        
        
        _code = [[UITextField alloc] initWithFrame:CGRectMake(distance, n, ManMan_SCREEN_WIDTH-75-distance*2, HEIGHT)];
        _code.attributedPlaceholder = [[NSAttributedString alloc] initWithString:Localized(@"JX_InputMessageCode") attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
        _code.font = g_factory.font16;
        _code.delegate = self;
        _code.autocorrectionType = UITextAutocorrectionTypeNo;
        _code.autocapitalizationType = UITextAutocapitalizationTypeNone;
        _code.enablesReturnKeyAutomatically = YES;
        _code.borderStyle = UITextBorderStyleNone;
        _code.returnKeyType = UIReturnKeyDone;
        _code.clearButtonMode = UITextFieldViewModeWhileEditing;
        
        UIView *codeView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 37, HEIGHT)];
        _code.leftView = codeView;
        _code.leftViewMode = UITextFieldViewModeAlways;
        CGFloat left_W = [UIFactory getStringWidthWithText:@"验证码" font:g_factory.font16 viewHeight:HEIGHT];
        UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, left_W + 10, HEIGHT)];
        _code.leftView = leftView;
        _code.leftViewMode = UITextFieldViewModeAlways;
        UILabel *phIgLab = [UIFactory createLabelWith:CGRectMake(0, 0, left_W, HEIGHT) text:@"验证码" font:KFontMedium(16) textColor:HEXCOLOR(0x3333333) backgroundColor:nil];
        [leftView addSubview:phIgLab];
        
        
        UIView *codeILine = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT-LINE_WH, _code.frame.size.width, LINE_WH)];
        codeILine.backgroundColor = THE_LINE_COLOR;
        [_code addSubview:codeILine];
        
        
        [self.tableBody addSubview:_code];
        
        _send = [UIFactory createButtonWithTitle:Localized(@"JX_Send")
                                       titleFont:g_factory.font16
                                      titleColor:[UIColor whiteColor]
                                          normal:nil
                                       highlight:nil ];
        _send.frame = CGRectMake(ManMan_SCREEN_WIDTH-70-distance-11, n+HEIGHT/2-15, 70, 30);
        [_send addTarget:self action:@selector(sendSMS) forControlEvents:UIControlEventTouchUpInside];
        _send.backgroundColor = g_theme.themeColor;
        _send.layer.masksToBounds = YES;
        _send.layer.cornerRadius = 7.f;
        [self.tableBody addSubview:_send];
        
        
        //新添加的手机验证（注册）
        n = n+HEIGHT+INSETS;
        
        UIButton * btn = [UIFactory createButtonWithTitle:Localized(@"REGISTERS") titleFont:g_factory.font16 titleColor:[UIColor whiteColor] normal:nil highlight:nil];
        
        [btn addTarget:self action:@selector(onTest) forControlEvents:UIControlEventTouchUpInside];
        btn.frame = CGRectMake(20, n,ManMan_SCREEN_WIDTH-20*2, 40);
        btn.backgroundColor = RGB(90, 202, 198);
        ViewRadius(btn, 20);
        [self.tableBody addSubview:btn];
        n = n+HEIGHT+INSETS;
        
        
        
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

#pragma mark----验证短信验证码
-(void)onClick{
    if([_code.text length]<6){
        if([_code.text length]<=0){
            [g_App showAlert:@"请输入短信验证码"];
            return;
        }
        [g_App showAlert:Localized(@"inputPhoneVC_MsgCodeNotOK")];
        return;
    }
    
    
    [self.view endEditing:YES];
    if (!_isSkipSMS) {
        
        self.isSmsRegister = YES;
        [self setUserInfo];

    } else {
        self.isSmsRegister = NO;
        [self setUserInfo];
    }
}
- (void)setUserInfo {
    
}
-(void)onTest{
    if (_agreeImgV.isHidden == YES) {
        [g_App showAlert:Localized(@"JX_NotAgreeProtocol")];
        return;
    }
    JY_UserObject* user = [JY_UserObject sharedInstance];
    user.telephone = _phoneStr;
    user.password  = [g_server getMD5String:self.pwdStr];
    user.areaCode = [_areaCodeBtn.titleLabel.text stringByReplacingOccurrencesOfString:@"+" withString:@""];;
    JY_PSRegisterBaseVC* vc = [JY_PSRegisterBaseVC alloc];
    vc.password = self.pwdStr;
    vc.isRegister = YES;
    vc.inviteCodeStr = @"";
    vc.smsCode = _code.text;
    vc.resumeId   = nil;
    vc.isSmsRegister = NO;
    vc.resumeModel     = [[resumeBaseData alloc]init];
    vc.user       = user;
    vc.type = self.type;
    vc = [vc init];
    [g_navigation pushViewController:vc animated:YES];
    [self actionQuit];
}

-(void)showTime:(NSTimer*)sender{
    UIButton *but = (UIButton*)[_timer userInfo];
    _seconds--;
    [but setTitle:[NSString stringWithFormat:@"%ds",_seconds] forState:UIControlStateSelected];
    if (_isSendFirst) {
        _isSendFirst = NO;
        _skipBtn.hidden = YES;
    }
    if (_seconds <= 30) {
        _skipBtn.hidden = NO;
    }
    if(_seconds<=0){
        but.selected = NO;
        but.userInteractionEnabled = YES;
        but.backgroundColor = g_theme.themeColor;
        [_send setTitle:Localized(@"JX_SendAngin") forState:UIControlStateNormal];
        if (_timer) {
            _timer = nil;
            [sender invalidate];
        }
        _seconds = 60;
    }
}
-(void) didServerResultSucces:(JY_Connection*)aDownload dict:(NSDictionary*)dict array:(NSArray*)array1{
    [_wait stop];
    
    if([aDownload.action isEqualToString:act_tixianRandcodeSendSms]){
        [JY_MyTools showTipView:Localized(@"JXAlert_SendOK")];
        _send.selected = YES;
        _send.userInteractionEnabled = NO;
        _send.backgroundColor = [UIColor grayColor];
//        if ([dict objectForKey:@"code"]) {
//            _smsCode = [[dict objectForKey:@"code"] copy];
//        }else {
//            _smsCode = @"-1";
//        }
        [_send setTitle:@"60s" forState:UIControlStateSelected];
        _seconds = 60;
        _timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(showTime:) userInfo:_send repeats:YES];
     
//        _imgCodeStr = _imgCode.text;
    }
    
    
}

-(int) didServerResultFailed:(JY_Connection*)aDownload dict:(NSDictionary*)dict{
    [_wait stop];
//    if([aDownload.action isEqualToString:act_tixianRandcodeSendSms]){
//        return hide_error;
//    }
    return show_error;
}
-(int) didServerConnectError:(JY_Connection*)aDownload error:(NSError *)error{
    [_wait stop];
    return show_error;
}
-(void) didServerConnectStart:(JY_Connection*)aDownload{
    [_wait stop];
}

- (void)hideKeyBoardToView {
    [self.view endEditing:YES];
}


@end
