#import "JY_admobViewController.h"
typedef NS_ENUM(NSInteger, ManMan_LoginType) {
    ManMan_LoginQQ = 1,          
    ManMan_LoginWX,               
};
@interface JY_loginVC : JY_admobViewController{
    UITextField* _pwd;
    UITextField* _phone;
    UITextField* _code;
    JY_UserObject* _user;
}
@property(assign)BOOL isAutoLogin;
@property(assign)BOOL isSwitchUser;
@property (nonatomic, strong) UIImageView *launchImageView;
@property (nonatomic, strong) UIButton *btn;
@property (nonatomic, strong) JY_Location *location;
@property (nonatomic, assign) BOOL isThirdLogin;
@property (nonatomic, assign) BOOL isSMSLogin;
@property (nonatomic, assign) ManMan_LoginType type;
@property (nonatomic, assign) BOOL iscCodeLogin;

@property (nonatomic, assign) BOOL isLoginOut;//退出登陆
@end
