//
//  ManMa_RegisterViewController.m
//  TFJunYouChat
//
//  Created by zoja on 2021/8/18.
//  Copyright © 2021 zengwOS. All rights reserved.
//

#import "ManMa_RegisterViewController.h"
#import "JY_inputPwdVC.h"
#import "JY_TelAreaListVC.h"
#import "JY_UserObject.h"
#import "JY_PSRegisterBaseVC.h"
#import "webpageVC.h"
#import "JY_loginVC.h"
#import "WKWebViewViewController.h"
#import "UIView+LK.h"
#import "JXTermsPrivacyVc.h"

#import "JY_RegisterNextVC.h"
#define HEIGHT 56
@interface ManMa_RegisterViewController ()
<UITextFieldDelegate>
{
    NSTimer *_timer;
    UIButton *_areaCodeBtn;
    JY_UserObject *_user;
    UIImageView * _imgCodeImg;
//    UITextField *_imgCode;
    UIButton * _graphicButton;
    UIButton* _skipBtn;
    BOOL _isSkipSMS;
    BOOL _isSendFirst;
    UIImageView * _agreeImgV;
}
@property (nonatomic, assign) BOOL isSmsRegister;
@property (nonatomic, assign) BOOL isCheckToSMS;
@property (nonatomic, weak) UIView *backView;
@property (nonatomic, weak) UIView *knowBackView;

@property(nonatomic,strong) UIButton *protocalBtn; // 去登录

@property(nonatomic,strong) UILabel *readLabel; // 去登录
@property(nonatomic,strong) UILabel *yiJiLabel; // 去登录

@property(nonatomic,strong) UIButton *selectProtocalBtn; //yonghuxieyi

@property(nonatomic,strong) UIButton *ProctProtocalBtn; // yingsi zhengce

@property (nonatomic, weak) UIButton *toLoginBtn;
@property (nonatomic,weak) JXTermsPrivacyVc *showImgView;
@end

@implementation ManMa_RegisterViewController


- (id)init
{
    self = [super init];
    if (self) {
        _seconds = 0;
        self.isGotoBack   = YES;
        self.title = Localized(@"JX_Register");
        self.heightFooter = 0;
        self.heightHeader = ManMan_SCREEN_TOP;
        //self.view.frame = g_window.bounds;
        [self createHeadAndFoot];
        self.tableBody.backgroundColor = [UIColor whiteColor];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyBoardToView)];
        [self.tableBody addGestureRecognizer:tap];
        _isSendFirst = YES;  // 第一次发送短信

        self.isSmsRegister = NO;
        //icon
      
        [self setUPView];
        
    }
    return self;
}

- (void)setUPView{
    
    int n = INSETS;
    int distance = 20; // 左右间距
    
    n += 10;
    
    UILabel *titleLab = [UIFactory createLabelWith:CGRectMake(20,n,ManMan_SCREEN_WIDTH-40 , 30) text:@"手机号注册" font:KFontRegular(28) textColor:HEXCOLOR(0x333333) backgroundColor:nil];
    [self.tableBody addSubview:titleLab];
    n += (30+30);
    
    
    UIImageView * kuliaoIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ALOGO_120"]];
    kuliaoIconView.frame = CGRectMake((ManMan_SCREEN_WIDTH-100)/2, n, 100, 100);
    
    kuliaoIconView.layer.cornerRadius = 10;
    kuliaoIconView.layer.masksToBounds = YES;
    [self.tableBody addSubview:kuliaoIconView];
    
    //手机号
    n += 40+95;
    if (!_phone) {
        NSString *placeHolder;
        if ([g_config.regeditPhoneOrName intValue] == 0) {
            placeHolder = Localized(@"JX_InputPhone");
        }else {
            placeHolder = Localized(@"JX_InputUserAccount");
        }
        _phone = [UIFactory createTextFieldWith:CGRectMake(distance, n, self_width-distance*2, HEIGHT) delegate:self returnKeyType:UIReturnKeyNext secureTextEntry:NO placeholder:placeHolder font:g_factory.font16];
        _phone.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeHolder attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
        _phone.borderStyle = UITextBorderStyleNone;
        if ([g_config.regeditPhoneOrName intValue] == 1) {
            _phone.keyboardType = UIKeyboardTypeDefault;  // 仅支持大小写字母数字
        }else {
            _phone.keyboardType = UIKeyboardTypeNumberPad;  // 限制只能数字输入，使用数字键盘
        }
        _phone.clearButtonMode = UITextFieldViewModeWhileEditing;
        [_phone addTarget:self action:@selector(textFieldDidChanged:) forControlEvents:UIControlEventEditingChanged];
        [self.tableBody addSubview:_phone];
        
        CGFloat left_W = [UIFactory getStringWidthWithText:@"手机号" font:g_factory.font16 viewHeight:HEIGHT];
        UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, left_W + 10, HEIGHT)];
        _phone.leftView = leftView;
        _phone.leftViewMode = UITextFieldViewModeAlways;
        UILabel *phIgLab = [UIFactory createLabelWith:CGRectMake(0, 0, left_W, HEIGHT) text:@"手机号" font:KFontMedium(16) textColor:HEXCOLOR(0x3333333) backgroundColor:nil];
//            UIImageView *phIgView = [[UIImageView alloc] initWithFrame:CGRectMake(2, HEIGHT/2-11, 22, 22)];
//            phIgView.image = [UIImage imageNamed:@"account"];
//            phIgView.contentMode = UIViewContentModeScaleAspectFit;
        [leftView addSubview:phIgLab];
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT-LINE_WH, _phone.frame.size.width, LINE_WH)];
        line.backgroundColor = THE_LINE_COLOR;
        [_phone addSubview:line];
    }
    n = n+HEIGHT;
    
    if (!_area) {
        NSString *placeHolder;
        NSString *areaStr;
        if (![g_default objectForKey:kMY_USER_AREACODE]) {
            areaStr = [NSString stringWithFormat:@"%@(+%@)",@"中国",@"86"];
            _areaIDStr = @"86";
        } else {
           NSMutableArray *telAreaArray = [g_constant.telArea mutableCopy];
            NSString *ID = [g_default objectForKey:kMY_USER_AREACODE];
            
            for (NSDictionary *dict in telAreaArray) {
                if (ID.integerValue == [dict[@"prefix"] integerValue]) {
                    _areaIDStr = ID;
                    areaStr = [NSString stringWithFormat:@"%@(+%@)",dict[@"country"],ID];
                   
                }
            }
        }
      
        _area = [UIFactory createTextFieldWith:CGRectMake(distance, n, self_width-distance*2, HEIGHT) delegate:self returnKeyType:UIReturnKeyDefault secureTextEntry:NO placeholder:placeHolder font:g_factory.font16];
        _area.borderStyle = UITextBorderStyleNone;
        _area.enabled = NO;
        _area.text = areaStr;
        
        _area.textColor = RGB(128, 180, 204);
        [self.tableBody addSubview:_area];
        
        CGFloat left_W = [UIFactory getStringWidthWithText:@"国家/地区" font:g_factory.font16 viewHeight:HEIGHT];
        UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, left_W + 10, HEIGHT)];
        _area.leftView = leftView;
        _area.leftViewMode = UITextFieldViewModeAlways;
        UILabel *phIgLab = [UIFactory createLabelWith:CGRectMake(0, 0, left_W, HEIGHT) text:@"国家/地区" font:KFontMedium(16) textColor:HEXCOLOR(0x3333333) backgroundColor:nil];
        [leftView addSubview:phIgLab];
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT-LINE_WH, _area.frame.size.width, LINE_WH)];
        line.backgroundColor = THE_LINE_COLOR;
        [_area addSubview:line];
        _area.userInteractionEnabled = YES;
        _areaCodeBtn = [[UIButton alloc] initWithFrame:_area.frame];
        _areaCodeBtn.hidden = [g_config.regeditPhoneOrName intValue] == 1;
        [_areaCodeBtn addTarget:self action:@selector(areaCodeBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [self.tableBody addSubview:_areaCodeBtn];
        n = n+HEIGHT;
    }
 
    
    
    //密码
    _pwd = [[UITextField alloc] initWithFrame:CGRectMake(distance, n, ManMan_SCREEN_WIDTH-distance*2, HEIGHT)];
    _pwd.delegate = self;
    _pwd.font = g_factory.font16;
    _pwd.autocorrectionType = UITextAutocorrectionTypeNo;
    _pwd.autocapitalizationType = UITextAutocapitalizationTypeNone;
    _pwd.enablesReturnKeyAutomatically = YES;
    _pwd.returnKeyType = UIReturnKeyDone;
    _pwd.clearButtonMode = UITextFieldViewModeWhileEditing;
    _pwd.attributedPlaceholder = [[NSAttributedString alloc] initWithString:Localized(@"JX_InputPassWord") attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
    _pwd.secureTextEntry = YES;
    _pwd.userInteractionEnabled = YES;

    [_pwd addTarget:self action:@selector(textFieldDidChanged:) forControlEvents:UIControlEventEditingChanged];
   [self.tableBody addSubview:_pwd];

    CGFloat left_W = [UIFactory getStringWidthWithText:@"密码" font:g_factory.font16 viewHeight:HEIGHT];
    UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,  left_W+10 , HEIGHT)];
    _pwd.leftView = rightView;
    _pwd.leftViewMode = UITextFieldViewModeAlways;
    UILabel *phIgLab = [UIFactory createLabelWith:CGRectMake(0, 0, left_W, HEIGHT) text:@"密码" font:KFontMedium(16) textColor:HEXCOLOR(0x3333333) backgroundColor:nil];
    [rightView addSubview:phIgLab];
    
    UIView *verticalLine = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT-LINE_WH, _pwd.frame.size.width, LINE_WH)];
    verticalLine.backgroundColor = THE_LINE_COLOR;
    [_pwd addSubview:verticalLine];
    
    n = n+HEIGHT;
    
    if ([g_config.registerInviteCode intValue] != 0) {
        //邀请码
        _inviteCode = [[UITextField alloc] initWithFrame:CGRectMake(distance, n, ManMan_SCREEN_WIDTH-distance*2, HEIGHT)];
        _inviteCode.delegate = self;
        _inviteCode.font = g_factory.font16;
        _inviteCode.autocorrectionType = UITextAutocorrectionTypeNo;
        _inviteCode.autocapitalizationType = UITextAutocapitalizationTypeNone;
        _inviteCode.enablesReturnKeyAutomatically = YES;
        _inviteCode.returnKeyType = UIReturnKeyDone;
        _inviteCode.clearButtonMode = UITextFieldViewModeWhileEditing;
        _inviteCode.attributedPlaceholder = [[NSAttributedString alloc] initWithString:Localized(@"JX_EnterInvitationCode") attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
        _inviteCode.secureTextEntry = YES;
        _inviteCode.userInteractionEnabled = YES;
        [self.tableBody addSubview:_inviteCode];
        
        UIView *inviteRightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 37, HEIGHT)];
        _inviteCode.leftView = inviteRightView;
        _inviteCode.leftViewMode = UITextFieldViewModeAlways;
        UIImageView *inviteRiIgView = [[UIImageView alloc] initWithFrame:CGRectMake(2, HEIGHT/2-11, 22, 22)];
        inviteRiIgView.image = [UIImage imageNamed:@"password"];
        inviteRiIgView.contentMode = UIViewContentModeScaleAspectFit;
        [inviteRightView addSubview:inviteRiIgView];
        
        verticalLine = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(inviteRightView.frame)-4, _inviteCode.frame.size.width, 0.5)];
        verticalLine.backgroundColor = HEXCOLOR(0xD6D6D6);
        [inviteRightView addSubview:verticalLine];
        
        n = n+HEIGHT;
    }

    //新添加的手机验证（注册）
    n = n+HEIGHT+INSETS;
   
    UIButton * _btn = [UIFactory createButtonWithTitle:Localized(@"REGISTERS") titleFont:g_factory.font16 titleColor:[UIColor whiteColor] normal:nil highlight:nil];
    
    [_btn addTarget:self action:@selector(checkPhoneNumber) forControlEvents:UIControlEventTouchUpInside];
    _btn.frame = CGRectMake(20, n,ManMan_SCREEN_WIDTH-20*2, 40);
    _btn.backgroundColor = RGB(90, 202, 198);
    ViewRadius(_btn, 20);
    [self.tableBody addSubview:_btn];
    n = n+HEIGHT+INSETS;
}

- (void)didAgree {
    _agreeImgV.hidden = !_agreeImgV.hidden;
}
- (void)rightBtnL {
    self.knowBackView.hidden=YES;

    WKWebViewViewController *vc=[WKWebViewViewController new];
    vc.titleStr = [NSString stringWithFormat:@"http://%@:8092/pages/PrivacyPolicy.html", APIURL];
    [g_navigation pushViewController:vc animated:YES];
}

- (void)leftIMClick{

    self.knowBackView.hidden=YES;
    WKWebViewViewController *vc=[WKWebViewViewController new];
    vc.titleStr = [NSString stringWithFormat:@"http://%@:8092/pages/PrivacyPolicy.html", APIURL];
    [g_navigation pushViewController:vc animated:YES];
    
    
}


- (void)checkTerms {
    
   self.knowBackView.hidden=YES;

   WKWebViewViewController *vc=[WKWebViewViewController new];
    vc.titleStr = [NSString stringWithFormat:@"http://%@:8092/pages/UserPolicy.html", APIURL];
    
   [g_navigation pushViewController:vc animated:YES];

    return;;
    webpageVC * webVC = [webpageVC alloc];
    webVC.url = [self protocolUrl];
    webVC.isSend = NO;
    webVC = [webVC init];
    [g_navigation.navigationView addSubview:webVC.view];
}
-(NSString *)protocolUrl{
    NSString * protocolStr = g_config.privacyPolicyPrefix;
    NSString * lange = g_constant.sysLanguage;
    if (![lange isEqualToString:ZHHANTNAME] && ![lange isEqualToString:NAME]) {
        lange = ENNAME;
    }
    return [NSString stringWithFormat:@"%@%@.html",protocolStr,lange];
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (_phone == textField) {
        return [self validateNumber:string];
    }
    return YES;
}
- (BOOL)validateNumber:(NSString*)number {
    if ([g_config.regeditPhoneOrName intValue] == 1) {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"] invertedSet];
        NSString *filtered = [[number componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        return [number isEqualToString:filtered];
    }
    BOOL res = YES;
    NSCharacterSet *tmpSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    int i = 0;
    while (i < number.length) {
        NSString *string = [number substringWithRange:NSMakeRange(i, 1)];
        NSRange range = [string rangeOfCharacterFromSet:tmpSet];
        if (range.length == 0) {
            res = NO;
            break;
        }
        i ++;
    }
    return res;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
- (void)enterNextPage {
    _isSkipSMS = YES;
    BOOL isMobile = [self isMobileNumber:_phone.text];
    if ([_pwd.text length] < 6) {
        [g_App showAlert:Localized(@"JX_TurePasswordAlert")];
        return;
    }
    if (isMobile) {
        NSString *areaCode = [_areaCodeBtn.titleLabel.text stringByReplacingOccurrencesOfString:@"+" withString:@""];
        [g_server checkPhone:_phone.text areaCode:areaCode verifyType:0 toView:self];
    }
}
- (void)textFieldDidChanged:(UITextField *)textField {
    if (textField == _phone) {
        if ([g_config.regeditPhoneOrName intValue] == 1) {
            if (_phone.text.length > 11) {
                _phone.text = [_phone.text substringToIndex:11];
            }
        }else {
            if (_phone.text.length > 11) {
                _phone.text = [_phone.text substringToIndex:11];
            }
        }
    }
    if (textField == _pwd) {
        if (_pwd.text.length > 17) {
            _pwd.text = [_pwd.text substringToIndex:17];
        }
    }
}
- (void)goToLoginVC {
    if (self.isThirdLogin) {
        JY_loginVC *login = [JY_loginVC alloc];
        login.isThirdLogin = YES;
        login.isAutoLogin = NO;
        login.isSwitchUser= NO;
        login.type = self.type;
        login = [login init];
        [g_navigation pushViewController:login animated:YES];
    }else {
        [self actionQuit];
    }
}
- (void)checkPhoneNumber{
    _isSkipSMS = NO;
    BOOL isMobile = [self isMobileNumber:_phone.text];
    if ([_pwd.text length] < 6) {
        [g_App showAlert:Localized(@"JX_TurePasswordAlert")];
        return;
    }
    if ([g_config.registerInviteCode intValue] == 1) {
        if ([_inviteCode.text length] <= 0) {
            [g_App showAlert:Localized(@"JX_EnterInvitationCode")];
            return;
        }
    }
    if (isMobile) {
//        NSString *areaCode = [_areaCodeBtn.titleLabel.text stringByReplacingOccurrencesOfString:@"+" withString:@""];
        [g_server checkPhone:_phone.text areaCode:_areaIDStr verifyType:0 toView:self];
    }
}
- (BOOL)isMobileNumber:(NSString *)number{
    if ([g_config.isOpenSMSCode boolValue] && [g_config.regeditPhoneOrName intValue] != 1) {
        if ([_phone.text length] == 0) {
            [g_App showAlert:Localized(@"JX_InputPhone")];
            return NO;
        }
    }
#ifdef IS_TEST_VERSION
#else
#endif
    return YES;
}
-(void)refreshGraphicAction:(UIButton *)button{
    [self getImgCodeImg];
}
-(void)getImgCodeImg{
    if([self isMobileNumber:_phone.text]){
        NSString *areaCode = [_areaCodeBtn.titleLabel.text stringByReplacingOccurrencesOfString:@"+" withString:@""];
        NSString * codeUrl = [g_server getImgCode:_phone.text areaCode:areaCode];
        NSURLRequest * request = [NSURLRequest requestWithURL:[NSURL URLWithString:codeUrl] cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:10.0];
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
            if (!connectionError) {
                UIImage * codeImage = [UIImage imageWithData:data];
                _imgCodeImg.image = codeImage;
            }else{
                NSLog(@"%@",connectionError);
                [g_App showAlert:connectionError.localizedDescription];
            }
        }];
    }else{
    }
}
#pragma mark----验证短信验证码
-(void)onClick{
    JY_RegisterNextVC *vc = [[JY_RegisterNextVC alloc]initWithPhoneStr:_phone.text areaStr:_areaIDStr];
    vc.pwdStr = _pwd.text;
    [g_navigation pushViewController:vc animated:YES];
}
- (void)setUserInfo {
    if (_agreeImgV.isHidden == YES) {
        [g_App showAlert:Localized(@"JX_NotAgreeProtocol")];
        return;
    }
    JY_UserObject* user = [JY_UserObject sharedInstance];
    user.telephone = _phone.text;
    user.password  = [g_server getMD5String:_pwd.text];
    JY_PSRegisterBaseVC* vc = [JY_PSRegisterBaseVC alloc];
    vc.password = _pwd.text;
    vc.isRegister = YES;
    vc.inviteCodeStr = _inviteCode.text;
    vc.smsCode = _code.text;
    vc.resumeId   = nil;
    vc.isSmsRegister = self.isSmsRegister;
    vc.resumeModel     = [[resumeBaseData alloc]init];
    vc.user       = user;
    vc.type = self.type;
    vc = [vc init];
    [g_navigation pushViewController:vc animated:YES];
    [self actionQuit];
}

-(void)onTest{
    if (_agreeImgV.isHidden == YES) {
        [g_App showAlert:Localized(@"JX_NotAgreeProtocol")];
        return;
    }
    JY_UserObject* user = [JY_UserObject sharedInstance];
    user.telephone = _phone.text;
    user.password  = [g_server getMD5String:_pwd.text];
    user.areaCode = [_areaCodeBtn.titleLabel.text stringByReplacingOccurrencesOfString:@"+" withString:@""];;
    JY_PSRegisterBaseVC* vc = [JY_PSRegisterBaseVC alloc];
    vc.password = _pwd.text;
    vc.isRegister = YES;
    vc.inviteCodeStr = _inviteCode.text;
    vc.smsCode = _code.text;
    vc.resumeId   = nil;
    vc.isSmsRegister = NO;
    vc.resumeModel     = [[resumeBaseData alloc]init];
    vc.user       = user;
    vc.type = self.type;
    vc = [vc init];
    [g_navigation pushViewController:vc animated:YES];
    [self actionQuit];
}
-(void) didServerResultSucces:(JY_Connection*)aDownload dict:(NSDictionary*)dict array:(NSArray*)array1{
    [_wait stop];
//    if([aDownload.action isEqualToString:act_SendSMS])
    if([aDownload.action isEqualToString:act_tixianRandcodeSendSms]){
        [JY_MyTools showTipView:Localized(@"JXAlert_SendOK")];
        _send.selected = YES;
        _send.userInteractionEnabled = NO;
        _send.backgroundColor = [UIColor grayColor];
//        if ([dict objectForKey:@"code"]) {
//            _smsCode = [[dict objectForKey:@"code"] copy];
//        }else {
//            _smsCode = @"-1";
//        }
        [_send setTitle:@"60s" forState:UIControlStateSelected];
        _seconds = 60;
        _timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(showTime:) userInfo:_send repeats:YES];
        _phoneStr = _phone.text;
//        _imgCodeStr = _imgCode.text;
    }
    if([aDownload.action isEqualToString:act_CheckPhone]){
        //检测手机号
        [self onClick];
//        if (self.isCheckToSMS) {
//            self.isCheckToSMS = NO;
//            [self onSend];
//            return;
//        }
//        if ([g_config.isOpenSMSCode boolValue] && [g_config.regeditPhoneOrName intValue] != 1) {
//            [self onClick];
//        }
//        else {
//            [self onTest];
//        }
    }
}
-(int) didServerResultFailed:(JY_Connection*)aDownload dict:(NSDictionary*)dict{
    [_wait stop];
    if([aDownload.action isEqualToString:act_SendSMS]){
        [_send setTitle:Localized(@"JX_Send") forState:UIControlStateNormal];
        [g_App showAlert:dict[@"resultMsg"]];
        [self getImgCodeImg];
        return hide_error;
    }
    return show_error;
}
-(int) didServerConnectError:(JY_Connection*)aDownload error:(NSError *)error{
    [_wait stop];
    return show_error;
}
-(void) didServerConnectStart:(JY_Connection*)aDownload{
    [_wait stop];
}
-(void)showTime:(NSTimer*)sender{
    UIButton *but = (UIButton*)[_timer userInfo];
    _seconds--;
    [but setTitle:[NSString stringWithFormat:@"%ds",_seconds] forState:UIControlStateSelected];
    if (_isSendFirst) {
        _isSendFirst = NO;
        _skipBtn.hidden = YES;
    }
    if (_seconds <= 30) {
        _skipBtn.hidden = NO;
    }
    if(_seconds<=0){
        but.selected = NO;
        but.userInteractionEnabled = YES;
        but.backgroundColor = g_theme.themeColor;
        [_send setTitle:Localized(@"JX_SendAngin") forState:UIControlStateNormal];
        if (_timer) {
            _timer = nil;
            [sender invalidate];
        }
        _seconds = 60;
    }
}
- (void)sendSMS{
    [_phone resignFirstResponder];
    [_pwd resignFirstResponder];
//    [_imgCode resignFirstResponder];
    [_code resignFirstResponder];
    if([self isMobileNumber:_phone.text]){
        
        
        self.isCheckToSMS = YES;
        NSString *areaCode = [_areaCodeBtn.titleLabel.text stringByReplacingOccurrencesOfString:@"+" withString:@""];
        [g_server checkPhone:_phone.text areaCode:areaCode verifyType:0 toView:self];
        
        
//        if (_imgCode.text.length < 3) {
//            [g_App showAlert:Localized(@"JX_inputImgCode")];
//        }else{
//            self.isCheckToSMS = YES;
//            NSString *areaCode = [_areaCodeBtn.titleLabel.text stringByReplacingOccurrencesOfString:@"+" withString:@""];
//            [g_server checkPhone:_phone.text areaCode:areaCode verifyType:0 toView:self];
//        }
    }
}
-(void)onSend{
    if (!_send.selected) {
        [_wait start:Localized(@"JX_Testing")];
        NSString *areaCode = [_areaCodeBtn.titleLabel.text stringByReplacingOccurrencesOfString:@"+" withString:@""];
        _user = [JY_UserObject sharedInstance];
        _user.areaCode = areaCode;
//        [g_server sendSMS:[NSString stringWithFormat:@"%@",_phone.text] areaCode:areaCode isRegister:NO imgCode:_imgCode.text toView:self];
    //不带图形验证
        [g_server sendSMSOutImage:[NSString stringWithFormat:@"%@",_phone.text] areaCode:areaCode isRegister:0 toView:self];
        [_send setTitle:Localized(@"JX_Sending") forState:UIControlStateNormal];
    }
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    if ([g_config.isOpenSMSCode boolValue] && [g_config.regeditPhoneOrName intValue] != 1) {
        if (textField == _phone) {
            [self getImgCodeImg];
        }
    }
#ifndef IS_TEST_VERSION
#endif
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}
- (void)areaCodeBtnClick{
    [self.view endEditing:YES];
    JY_TelAreaListVC *telAreaListVC = [[JY_TelAreaListVC alloc] init];
    telAreaListVC.telAreaDelegate = self;
    telAreaListVC.didSelect = @selector(didSelectTelArea:);
    [g_navigation pushViewController:telAreaListVC animated:YES];
}
- (void)didSelectTelArea:(NSString *)areaCode{
    
    NSMutableArray *telAreaArray = [g_constant.telArea mutableCopy];
    NSString *areaStr;
     for (NSDictionary *dict in telAreaArray) {
         if (areaCode.integerValue == [dict[@"prefix"] integerValue]) {
             areaStr = [NSString stringWithFormat:@"%@(+%@)",dict[@"country"],areaCode];
             _areaIDStr = areaCode;
         }
     }
    _area.text = areaStr;

}
- (void)resetBtnEdgeInsets:(UIButton *)btn{
    [btn setTitleEdgeInsets:UIEdgeInsetsMake(0, -btn.imageView.frame.size.width-2, 0, btn.imageView.frame.size.width+2)];
    [btn setImageEdgeInsets:UIEdgeInsetsMake(0, btn.titleLabel.frame.size.width+2, 0, -btn.titleLabel.frame.size.width-2)];
}
- (void)hideKeyBoardToView {
    [self.view endEditing:YES];
}




#pragma mark -- 协议条款
-(void)iniframeProtocal{
     
    JXTermsPrivacyVc *showImgView = [JXTermsPrivacyVc XIBJXTermsPrivacyVc];
    showImgView.blockBtn = ^(UIButton *sender) {
      
        if (sender.tag ==1) {
            [self rightBtnL];
        }else if (sender.tag ==2){
            
            [self leftIMClick];
        }else{
            
            [self cancelBtnClick];
        }
        
    };
    [self.view addSubview:showImgView];
    showImgView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    self.showImgView=showImgView;
 
 return;
    
    UIView *backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    backView.userInteractionEnabled=YES;
    backView.backgroundColor=[UIColor colorWithWhite:0.0 alpha:0.5];
    [[UIApplication sharedApplication].keyWindow addSubview:backView];
    self.backView=backView;
    
    
    UIView *leftIM=[[UIView alloc]init];
    leftIM.backgroundColor=[UIColor whiteColor];
    leftIM.userInteractionEnabled=YES;
    [backView addSubview:leftIM];
    leftIM.frame=CGRectMake(20, SCREEN_WIDTH/2, SCREEN_WIDTH-40, SCREEN_WIDTH-40);
    leftIM.layer.cornerRadius=15;
    leftIM.layer.masksToBounds=YES;
    [backView addSubview:leftIM];
    
    UILabel *titleLable=[[UILabel alloc]init];
    titleLable.text=@"隐私政策";
    titleLable.font=[UIFont boldSystemFontOfSize:20];
    titleLable.textAlignment=NSTextAlignmentCenter;
    titleLable.frame=CGRectMake(10, 30, SCREEN_WIDTH-60, 20);
    [leftIM addSubview:titleLable];
    
    UILabel *subtitle=[[UILabel alloc]init];
    subtitle.text=@"        欢迎使用聚友免费聊天，为了更好地保护您的隐私和个人信息安全，根据国家相关法律规定拟定了";
    subtitle.numberOfLines=3;
    subtitle.frame=CGRectMake(10, 64, SCREEN_WIDTH-60,66);
    [leftIM addSubview:subtitle];
     
    
    UIButton *leftBtn=[[UIButton alloc]init];
    [leftBtn setTitle:@"《隐私政策》" forState:UIControlStateNormal];
    [leftBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    leftBtn.layer.cornerRadius=5;
    leftBtn.layer.masksToBounds=YES;
    [leftIM addSubview:leftBtn];
    leftBtn.frame=CGRectMake(10, subtitle.bottom+5, 111, 15);
    [leftBtn addTarget:self action:@selector(leftIMClick) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *heLabel=[[UILabel alloc]init];
    heLabel.text=@"和";
    heLabel.textAlignment=NSTextAlignmentCenter;
    heLabel.frame=CGRectMake(leftBtn.right+5, subtitle.bottom+5, 30,15);
    [leftIM addSubview:heLabel];
    
    UIButton *rightBtn=[[UIButton alloc]init];
    [rightBtn setTitle:@"《用户协议》" forState:UIControlStateNormal];
    [rightBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    rightBtn.layer.cornerRadius=5;
    rightBtn.layer.masksToBounds=YES;
    [leftIM addSubview:rightBtn];
    rightBtn.frame=CGRectMake(heLabel.right+5,subtitle.bottom+5, 111, 15);
    [rightBtn addTarget:self action:@selector(rightBtnL) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *contentLabel=[[UILabel alloc]init];
    contentLabel.text=@"请在使用前仔细阅读并同意";
    contentLabel.frame=CGRectMake(10, leftBtn.bottom+10,SCREEN_WIDTH-60 ,15);
    [leftIM addSubview:contentLabel];
    
    
       UIButton *cancelBtn=[[UIButton alloc]init];
       [cancelBtn setTitle:@"同意协议" forState:UIControlStateNormal];
       [cancelBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
       cancelBtn.backgroundColor=[UIColor redColor];
       cancelBtn.layer.cornerRadius=20;
       cancelBtn.layer.masksToBounds=YES;
       [leftIM addSubview:cancelBtn];
      cancelBtn.frame=CGRectMake(20, contentLabel.bottom+44, leftIM.width-40, 40);
       [cancelBtn addTarget:self action:@selector(cancelBtnClick) forControlEvents:UIControlEventTouchUpInside];
       
       UIButton *sureBtn=[[UIButton alloc]init];
       [sureBtn setTitle:@"不同意" forState:UIControlStateNormal];
       [sureBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
       sureBtn.backgroundColor=[UIColor lightGrayColor];
       sureBtn.layer.cornerRadius=20;
       sureBtn.layer.masksToBounds=YES;
       [leftIM addSubview:sureBtn];
       sureBtn.frame=CGRectMake(20, cancelBtn.bottom+5, leftIM.width-40, 40);;
       [sureBtn addTarget:self action:@selector(sureBtnClickaaa) forControlEvents:UIControlEventTouchUpInside];
}
- (void)sureBtnClick{
    exit(0);
}

//同意协议
- (void)cancelBtnClick{
    [self.backView removeFromSuperview];
    [self.showImgView removeFromSuperview];
}

- (void)sureBtnClickaaa{
    exit(0);
}


-(void)iniframeShow{
    
     
    UIView *backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    backView.userInteractionEnabled=YES;
    backView.backgroundColor=[UIColor colorWithWhite:0.0 alpha:0.5];
    [[UIApplication sharedApplication].keyWindow addSubview:backView];
    self.knowBackView=backView;
    
    
    UIView *leftIM=[[UIView alloc]init];
    leftIM.backgroundColor=[UIColor whiteColor];
    leftIM.userInteractionEnabled=YES;
    [backView addSubview:leftIM];
    leftIM.frame=CGRectMake(40, SCREEN_WIDTH/2, SCREEN_WIDTH-80, SCREEN_WIDTH/1.8+60);
    leftIM.layer.cornerRadius=15;
    leftIM.layer.masksToBounds=YES;
    [backView addSubview:leftIM];
    
    UILabel *titleLable=[[UILabel alloc]init];
    titleLable.text=@"隐私政策";
    titleLable.font=[UIFont boldSystemFontOfSize:20];
    titleLable.textAlignment=NSTextAlignmentCenter;
    titleLable.frame=CGRectMake(10, 30, SCREEN_WIDTH-100, 20);
    [leftIM addSubview:titleLable];
    
    UILabel *subtitle=[[UILabel alloc]init];
    subtitle.text=@"        为了更好地保护您的权益，请阅读并同意下方的";
    subtitle.numberOfLines=3;
    subtitle.frame=CGRectMake(10, 64, SCREEN_WIDTH-100,66);
    [leftIM addSubview:subtitle];
     
    
    UIButton *leftBtn=[[UIButton alloc]init];
    [leftBtn setTitle:@"《隐私政策》" forState:UIControlStateNormal];
    [leftBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    leftBtn.layer.cornerRadius=5;
    leftBtn.layer.masksToBounds=YES;
    [leftIM addSubview:leftBtn];
    leftBtn.frame=CGRectMake(0, subtitle.bottom, 111, 15);
    [leftBtn addTarget:self action:@selector(leftIMClick) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *heLabel=[[UILabel alloc]init];
    heLabel.text=@"和";
    heLabel.textAlignment=NSTextAlignmentCenter;
    heLabel.frame=CGRectMake(leftBtn.right, subtitle.bottom, 30,15);
    [leftIM addSubview:heLabel];
    
    UIButton *rightBtn=[[UIButton alloc]init];
    [rightBtn setTitle:@"《用户协议》" forState:UIControlStateNormal];
    [rightBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    rightBtn.layer.cornerRadius=5;
    rightBtn.layer.masksToBounds=YES;
    [leftIM addSubview:rightBtn];
    rightBtn.frame=CGRectMake(heLabel.right,subtitle.bottom, 111, 15);
    [rightBtn addTarget:self action:@selector(rightBtnL) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *contentLabel=[[UILabel alloc]init];
    contentLabel.text=@"后,再进行登录";
    contentLabel.frame=CGRectMake(8, rightBtn.bottom,SCREEN_WIDTH-60 ,22);
    [leftIM addSubview:contentLabel];
    
    
       UIButton *cancelBtn=[[UIButton alloc]init];
       [cancelBtn setTitle:@"知道了" forState:UIControlStateNormal];
       [cancelBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
       cancelBtn.backgroundColor=[UIColor redColor];
       cancelBtn.layer.cornerRadius=20;
       cancelBtn.layer.masksToBounds=YES;
       [leftIM addSubview:cancelBtn];
      cancelBtn.frame=CGRectMake(20, contentLabel.bottom+44, leftIM.width-40, 40);
       [cancelBtn addTarget:self action:@selector(knowBtn) forControlEvents:UIControlEventTouchUpInside];
      
}
- (void)knowBtn{
    
    [self.showImgView removeFromSuperview];
    [self.knowBackView removeFromSuperview];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.knowBackView.hidden=NO;
    self.showImgView.hidden =NO;
}


@end
