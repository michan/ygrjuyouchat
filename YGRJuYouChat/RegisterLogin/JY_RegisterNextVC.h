//
//  JY_RegisterNextVC.h
//  TFJunYouChat
//
//  Created by zoja on 2021/8/18.
//  Copyright © 2021 zengwOS. All rights reserved.
//

#import "JY_admobViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface JY_RegisterNextVC : JY_admobViewController
{
    UITextField* _code;
    UIButton* _send;
    int _seconds;
}
@property (nonatomic, strong) NSString *phoneStr;
@property (nonatomic, strong) NSString *areaStr;
@property (nonatomic, strong) NSString *pwdStr;

- (instancetype)initWithPhoneStr:(NSString *)phoneStr areaStr:(NSString *)areaStr;
@property (nonatomic, assign) int type;
@end

NS_ASSUME_NONNULL_END
