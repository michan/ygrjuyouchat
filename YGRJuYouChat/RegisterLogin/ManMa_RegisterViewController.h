//
//  ManMa_RegisterViewController.h
//  TFJunYouChat
//
//  Created by zoja on 2021/8/18.
//  Copyright © 2021 zengwOS. All rights reserved.
//

#import "JY_admobViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ManMa_RegisterViewController : JY_admobViewController{
    UITextField* _area;
    UITextField* _phone;
    UITextField* _code;
    UITextField* _pwd;
    UITextField* _inviteCode;
    UIButton* _send;
//    NSString* _smsCode;//验证码后台不再返回，本地不做验证
    NSString* _areaIDStr;
    NSString* _phoneStr;
    
    int _seconds;
}
@property (nonatomic, assign) BOOL isThirdLogin;
@property (nonatomic, assign) int type;

@end

NS_ASSUME_NONNULL_END
