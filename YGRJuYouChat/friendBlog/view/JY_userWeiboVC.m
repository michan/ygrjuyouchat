#import "JY_userWeiboVC.h"
@interface JY_userWeiboVC ()
@end
@implementation JY_userWeiboVC
- (id)init
{
    self.isNotShowRemind = YES;
    self = [super init];
    if (self) {
            self.title = self.user.userNickname;
    }
    return self;
}
- (void)dealloc {
}
- (void)viewDidLoad
{
    [super viewDidLoad];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
-(void)getServerData{
    [self stopLoading];
    [g_App.jxServer getUserMessage:self.user.userId messageId:[self getLastMessageId:self.datas] toView:self];
}
@end
