#import <UIKit/UIKit.h>
#import "JY_admobViewController.h"
#import "JY_VideoPlayer.h"
#import "JY_AudioPlayer.h"
#import "JY_AudioRecorderViewController.h"
@class JY_TextView;
@class StreamPlayerViewController;
@protocol ManMan_ServerResult;
@interface JY_addMsgVC : JY_admobViewController<ManMan_ServerResult,UITextFieldDelegate,UITextViewDelegate,UIGestureRecognizerDelegate,ManMan_AudioRecorderDelegate,LXActionSheetDelegate>{
    int _nSelMenu;
    UIScrollView* svImages;
    UIScrollView* svAudios;
    UIScrollView* svVideos;
    UIScrollView* svFiles;
    int  _recordCount;
    int  _refreshCount;
    int  _buildHeight;
    NSInteger  _photoIndex;
    UIButton* _sendBtn;
    UITextView*  _remark;
    JY_AudioPlayer* audioPlayer;
    JY_VideoPlayer* videoPlayer;
    NSMutableArray* _array;
    NSMutableArray* _images;
    NSString* tUrl;
    NSString* oUrl;
}
@property(assign)BOOL isChanged;
@property(nonatomic,assign)int  dataType;
@property(nonatomic,retain) NSString* audioFile;
@property(nonatomic,retain) NSString* videoFile;
@property(nonatomic,retain) NSString* fileFile;
@property (nonatomic, weak) NSObject* delegate;
@property (nonatomic, assign) SEL		didSelect;
@property (nonatomic, assign) BOOL isShare;
@property (nonatomic, copy) NSString *shareTitle;
@property (nonatomic, copy) NSString *shareIcon;
@property (nonatomic, copy) NSString *shareUr;
@property (nonatomic, strong) NSString *urlShare; 
@property (nonatomic, assign) BOOL isShortVideo;
@property (nonatomic,assign) int maxImageCount;
@property (nonatomic,copy) void(^block)(void);
-(void)showImages;
-(void)doRefresh;
@end
