#import "ReplyCell.h"
@implementation ReplyCell
 
-(void)prepareForReuse
{
    [super prepareForReuse];
    
   // self.label.match=nil;
}
- (void)awakeFromNib {
    [super awakeFromNib];
}

//- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
//    
//    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
//        HBCoreLabel *label = [[HBCoreLabel alloc]initWithFrame:CGRectMake(10, 4, ManMan_SCREEN_WIDTH, 27)];
//    
//        [self.contentView addSubview:label];
//        _label = label;
//    }
//    return self;
//}

-(void)setMatch:(MatchParser *)match{
    
    _match = match;
    
    _label.text = match.attrString.string;
}
 

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UITouch* touch = [touches anyObject];
    CGPoint point = [touch locationInView:self];
    self.pointIndex = point.x/10;
    [super touchesEnded:touches withEvent:event];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}
@end
