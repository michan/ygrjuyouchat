#import "JY_admobViewController.h"
NS_ASSUME_NONNULL_BEGIN
@class JY_SelectCoverVC;
@protocol ManMan_SelectCoverVCDelegate <NSObject>
- (void)touchAtlasButtonReturnCover:(UIImage *)img;
- (void)selectImage:(UIImage *)img toView:(JY_SelectCoverVC *)view;
@end
@interface JY_SelectCoverVC : JY_admobViewController
@property (nonatomic,weak) id<ManMan_SelectCoverVCDelegate> delegate;
- (instancetype)initWithVideo:(NSString *)video;
@end
NS_ASSUME_NONNULL_END
