#import <UIKit/UIKit.h>
#import "PageLoadFootView.h"
#import "WeiboData.h"
#import "HBCoreLabel.h"
#import "JY_TableViewController.h"
#import "JY_SelectMenuView.h"
@class JY_Server;
@class JY_WeiboReplyData;
@class JY_TextView;
@class JY_WeiboCell;
@class userInfoVC;
@class JY_MenuView;
#define WeiboUpdateNotification  @"WeiboUpdateNotification"
@class JY_WeiboVC;
@protocol weiboVCDelegate <NSObject>
- (void) weiboVC:(JY_WeiboVC *)weiboVC didSelectWithData:(WeiboData *)data;
@end
@interface JY_WeiboVC : JY_TableViewController<HBCoreLabelDelegate,UITextFieldDelegate,UIScrollViewDelegate,UITextViewDelegate>
{
    UITextView* _input;
    UIView* _inputParent;
    void(^_block)(NSString*string);
    NSIndexPath *_deletePath;
    BOOL  animationEnd;
    NSMutableArray* _pool;
    UIView * _bgBlackAlpha;
    JY_SelectMenuView * _selectView;
}
@property(nonatomic,strong) JY_UserObject* user;
@property(nonatomic,strong)NSMutableArray* datas;
@property(nonatomic,strong)WeiboData * selectWeiboData;
@property(nonatomic,strong)JY_WeiboCell* selectManMan_WeiboCell;
@property(nonatomic,strong)JY_WeiboReplyData * replyDataTemp;
@property(nonatomic,strong)WeiboData * deleteWeibo;
@property(nonatomic,assign) int refreshCount;
@property(nonatomic,assign) NSInteger refreshCellIndex;
@property(nonatomic,assign) int deleteReply;
@property (nonatomic, assign) BOOL isDetail;
@property (nonatomic, copy) NSString *detailMsgId;
@property (nonatomic, assign) BOOL isNotShowRemind;
@property (nonatomic, assign) BOOL isCollection;
@property (nonatomic, weak) id<weiboVCDelegate>delegate;
@property (nonatomic, assign) BOOL isSend;
@property (nonatomic, assign) NSInteger videoIndex;
@property(nonatomic,retain) JY_VideoPlayer* videoPlayer;
@property(nonatomic,strong) JY_MenuView *menuView;
@property (nonatomic,retain) UIView * clearBackGround;
-(void)doShowAddComment:(NSString*)s;
-(NSString*)getLastMessageId:(NSArray*)objects;
-(void)delBtnAction:(WeiboData *)cellData;
-(void)btnReplyAction:(UIButton *)sender WithCell:(JY_WeiboCell *)cell;
-(void)fileAction:(WeiboData *)cellData;
-(void)setupTableViewHeight:(CGFloat)height tag:(NSInteger)tag;
-(instancetype)initCollection;
@end
