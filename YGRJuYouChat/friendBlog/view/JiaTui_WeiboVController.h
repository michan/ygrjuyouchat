#import <UIKit/UIKit.h>
#import "PageLoadFootView.h"
#import "WeiboData.h"
#import "HBCoreLabel.h"
#import "JY_TableViewController.h"
#import "JY_SelectMenuView.h" 
@class JiaTui_WeiboCell;

@class JY_Server;
@class JY_WeiboReplyData;
@class JY_TextView;
@class userInfoVC;
@class JY_MenuView;


#define WeiboUpdateNotification  @"WeiboUpdateNotification"
@class JiaTui_WeiboVController;
@protocol weiboVCDelegateJiaTui <NSObject>
- (void) weiboVC:(JiaTui_WeiboVController *)weiboVC didSelectWithData:(WeiboData *)data;
@end
@interface JiaTui_WeiboVController : JY_TableViewController<HBCoreLabelDelegate,UITextFieldDelegate,UIScrollViewDelegate,UITextViewDelegate>
{
    UITextView* _input;
    UIView* _inputParent;
    void(^_block)(NSString*string);
    NSIndexPath *_deletePath;
    BOOL  animationEnd;
    NSMutableArray* _pool;
    UIView * _bgBlackAlpha;
    JY_SelectMenuView * _selectView;
}
@property(nonatomic,strong) JY_UserObject* user;
@property(nonatomic,strong)NSMutableArray* datas;
@property(nonatomic,strong)WeiboData * selectWeiboData;
@property(nonatomic,strong)JiaTui_WeiboCell* selectJiaTui_WeiboCell;
@property(nonatomic,strong)JY_WeiboReplyData * replyDataTemp;
@property(nonatomic,strong)WeiboData * deleteWeibo;
@property(nonatomic,assign) int refreshCount;
@property(nonatomic,assign) NSInteger refreshCellIndex;
@property(nonatomic,assign) int deleteReply;
@property (nonatomic, assign) BOOL isDetail;
@property (nonatomic, copy) NSString *detailMsgId;
@property (nonatomic, assign) BOOL isNotShowRemind;
@property (nonatomic, assign) BOOL isCollection;
@property (nonatomic, weak) id<weiboVCDelegateJiaTui>delegate;
@property (nonatomic, assign) BOOL isSend;
@property (nonatomic, assign) NSInteger videoIndex;
@property(nonatomic,strong) JY_VideoPlayer* videoPlayer;
@property(nonatomic,strong) JY_MenuView *menuView;
@property (nonatomic,retain) UIView * clearBackGround;

@property (nonatomic,assign) int maxImageCount;
@property (nonatomic, assign) BOOL isUpdateImage;

-(void)doShowAddComment:(NSString*)s;
-(NSString*)getLastMessageId:(NSArray*)objects;
-(void)delBtnAction:(WeiboData *)cellData;
-(void)btnReplyAction:(UIButton *)sender WithCell:(JiaTui_WeiboCell *)cell;
-(void)fileAction:(WeiboData *)cellData;
-(void)setupTableViewHeight:(CGFloat)height tag:(NSInteger)tag;
-(instancetype)initCollection;

@property (nonatomic, assign) BOOL dnyamin;

@property (nonatomic, strong) NSMutableArray *remindArray;

@property (nonatomic,weak)UIButton* backButton;
@property (nonatomic,weak)UIButton* addButton;
@property (nonatomic,weak)UIView *navHeadView;
@property (nonatomic,weak)UIButton* backBtnLeft;
@property (nonatomic,weak)UIButton* btnAdd;
@property (nonatomic,strong)UILabel* circleTitle;
@property (nonatomic, assign) BOOL drawScrollView;

@end
