//
//  JXBaseWeb_VC.m
//  shiku_im
//
//  Created by 韩银华 on 2020/7/24.
//  Copyright © 2020 Reese. All rights reserved.
//

#import "JXBaseWeb_VC.h"
#import <WebKit/WebKit.h>

@interface JXBaseWeb_VC ()<WKUIDelegate,WKNavigationDelegate>
//lab
{
    UILabel *tipLab;
}
//view
@property (nonatomic, strong) UIView *bgView;

@end

@implementation JXBaseWeb_VC

- (instancetype)init{
    self = [super init];
    if (self) {
        
        self.isGotoBack = YES;
        self.heightHeader = ManMan_SCREEN_TOP;
        self.heightFooter = 0;
        [self createHeadAndFoot];
        [self.tableBody addSubview:self.bgView];
        [g_notify addObserver:self selector:@selector(showAgreeMent:) name:@"yonghuxieyi" object:nil];
        [g_notify addObserver:self selector:@selector(showUserRegistAction:) name:@"userRegistMent" object:nil];
    
      
        
        
        
    }
    return self;
}
- (UIView *)bgView{
    if (!_bgView) {
        _bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ManMan_SCREEN_WIDTH, ManMan_SCREEN_HEIGHT-ManMan_SCREEN_BOTTOM)];
    }
    return _bgView;
}
- (void)setWKWeb{
    //js脚本 （脚本注入设置网页样式）
    //    NSString *jScript = @"var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);";
    //    //注入
    //    WKUserScript *wkUScript = [[WKUserScript alloc] initWithSource:jScript injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:YES];
    //    WKUserContentController *wkUController = [[WKUserContentController alloc] init];
    //    [wkUController addUserScript:wkUScript];
    //配置对象
    //    WKWebViewConfiguration *wkWebConfig = [[WKWebViewConfiguration alloc] init];
    //    wkWebConfig.userContentController = wkUController;
    //    改变初始化方法 （这里一定要给个初始宽度，要不算的高度不对）
    
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setWKWeb];
    
    
    
    
}

-(void)showAgreeMent:(NSNotification *)notif{
    
     self.title = @"用户协议";
    WKWebView *webView = [[WKWebView alloc] initWithFrame:CGRectMake(12, 0, ManMan_SCREEN_WIDTH-24, ManMan_SCREEN_HEIGHT-ManMan_SCREEN_BOTTOM)];
    [self.bgView addSubview:webView];
    webView.UIDelegate = self;
    webView.navigationDelegate = self;
    NSString *agreementStr = [g_default objectForKey:@"privacyAddBank"];
    [webView loadHTMLString:agreementStr baseURL:nil];
    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_htmlBaseUrl]]];

}
-(void)showUserRegistAction:(NSNotification *)notif{
    
    self.title = @"用户注册协议";
    
    NSString *registContentStr = [g_default objectForKey:@"registAgreement"];
    
    WKWebView *webView = [[WKWebView alloc] initWithFrame:CGRectMake(12, 0, ManMan_SCREEN_WIDTH-24, ManMan_SCREEN_HEIGHT-ManMan_SCREEN_BOTTOM)];
    [self.bgView addSubview:webView];
    webView.UIDelegate = self;
    webView.navigationDelegate = self;
    
    [webView loadHTMLString:registContentStr baseURL:nil];
    
    
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
{
    //修改字体大小 300%
    [ webView evaluateJavaScript:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '180%'" completionHandler:nil];
//
//    //修改字体颜色  #9098b8
//    [ webView evaluateJavaScript:@"document.getElementsByTagName('body')[0].style.webkitTextFillColor= '#0078f0'" completionHandler:nil];
    
}
- (void)dealloc{
    [g_notify removeObserver:self];
}

@end
