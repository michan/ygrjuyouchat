//
//  JXAddBankPhoneCode_VC.m
//  shiku_im
//
//  Created by 韩银华 on 2020/7/22.
//  Copyright © 2020 Reese. All rights reserved.
//

#import "JXAddBankPhoneCode_VC.h"
#import "JXAddBankSuccess_VC.h"
#import "JXBaseWeb_VC.h"
#import "JXBankList_VC.h"
#import "UIButton+ActionBlock.h"
#import "MYTView.h"
#import "BGPopupView.h"
//#import "FWPopupView.h"


//__weak
//#define WeakSelf __weak typeof(self) weak_self = self;

@interface JXAddBankPhoneCode_VC ()
{
    
    UITextField *cardTextF;
    NSTimer *messsageTimer;
    int messageIssssss;//短信倒计时  60s
}
@property (nonatomic, strong) UITextField *nameTextF;

//卡号
//
@property (nonatomic, strong) UITextField *phoneTextF;
//验证码
@property (nonatomic, strong) UITextField *codeTextF;
//发送验证码
@property (nonatomic, strong) UIButton *sendCodeBtn;
//确认充值
@property (nonatomic, strong) UIButton *p1a1y1Btn;
//用户协议
@property (nonatomic, strong) UILabel *agreeLab;
//是否同意
@property (nonatomic, strong) UIButton *agreeBtn;
//选择银行卡按钮
@property (nonatomic, strong) UIButton *bBtn;

//银行列表
@property (nonatomic, strong) MYTView *bankListView;


@property (nonatomic, strong) NSArray *bankAry;

@end

@implementation JXAddBankPhoneCode_VC

- (UIButton *)p1a1y1Btn{
    if (!_p1a1y1Btn) {
        _p1a1y1Btn = [UIButton buttonWithType:UIButtonTypeCustom];
        _p1a1y1Btn.backgroundColor = HEXCOLOR(0x42DD5E);
        _p1a1y1Btn.layer.cornerRadius = 7;
        _p1a1y1Btn.layer.masksToBounds = YES;
        [_p1a1y1Btn setTitle:@"完成" forState:UIControlStateNormal];
        [_p1a1y1Btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _p1a1y1Btn.titleLabel.font = [UIFont boldSystemFontOfSize:17];
        [_p1a1y1Btn addTarget:self action:@selector(p1a1y1BtnokAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _p1a1y1Btn;
}
-(void)p1a1y1BtnokAction{
    
    
    if (cardTextF.text.length ==0) {
        [JY_MyTools showTipView:@"请输入本人银行卡号"];
        return;
    }
    if ([self.isOpenBankStr isEqualToString:@"1"]) {
        
        if (_phoneTextF.text.length == 0) {
            [JY_MyTools showTipView:@"请输入手机号"];
            return;
        }
        
    }
    
    if (_codeTextF.text.length == 0) {
        [JY_MyTools showTipView:@"请输入验证码"];
        return;
    }
    [g_server jxConfirmAddNewBankMsgCode:_codeTextF.text cardId:self.bankAddId otherID:self.bankOrderId toView:self];
    

}
-(void) didServerResultSucces:(JY_Connection*)aDownload dict:(NSDictionary*)dict array:(NSArray*)array1{
    
    if ([aDownload.action isEqual:act_AddBank]) {
        
        self.bankAddId = [dict valueForKey:@"ncountOrderId"];
        
        if (messsageTimer == nil) {
            messsageTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(daojishiCode) userInfo:nil repeats:YES];
        }

        
    }
    else if ([aDownload.action isEqual:act_ConfirmBankList]) {
        NSLog(@"%@", dict);
//        NSArray *bA = dict[@"data"];
        NSMutableArray *mA = [[NSMutableArray alloc] init];
        for (NSDictionary *d in array1) {
            [mA addObject:d[@"bankName"]];
        }
        self.bankAry = mA;
        
    }
    
    else{
        [g_notify postNotificationName:@"loadBnakList" object:nil];
        
        [g_navigation popToViewController:[JXBankList_VC class] animated:YES];
    }
}
-(int) didServerResultFailed:(JY_Connection*)aDownload dict:(NSDictionary*)dict{
//    [_wait stop];

    NSLog(@"123");

    return show_error;
}

-(void)sendCodeBtnAction:(UIButton *)sender{
    
    if (cardTextF.text.length == 0) {
        [JY_MyTools showTipView:@"请输入银行卡号"];
        return;
    }
    if ([self.bBtn.titleLabel.text isEqualToString:@"选择银行"]) {
        
            [JY_MyTools showTipView:@"请选择银行"];
            return;
    }
    
    if (_phoneTextF.text.length == 0) {
         [JY_MyTools showTipView:@"请输入手机号"];
         return;
     }
    
    NSString *timeStr = @"";
      if ([self.cardtype isEqual:@"信用卡"]) {
          timeStr = @"";
      }
      
    self.cardtype = @"1";
    
    [g_server jxAddBankMsgCardNum:cardTextF.text cardTime:timeStr cardId:self.bankOrderId cardCode:@"" cardType:self.cardtype cvvTimes:@"" addBankPhone:_phoneTextF.text toView:self];
    
}
#pragma mark ----------- 倒计时
-(void)daojishiCode{
    
    [_sendCodeBtn setTitle:[NSString stringWithFormat:@"%ds重新发送",messageIssssss] forState:UIControlStateNormal];
    _sendCodeBtn.userInteractionEnabled = NO;
    [_sendCodeBtn setTitleColor:THEMECOLOR forState:UIControlStateNormal];

    if (messageIssssss<=0) {
        [_sendCodeBtn setTitleColor:HEXCOLOR(0x2CB345) forState:0];
        [_sendCodeBtn setTitle:@"重新发送" forState:UIControlStateNormal];
        _sendCodeBtn.userInteractionEnabled = YES;
        [messsageTimer invalidate];
        messsageTimer = nil;
        messageIssssss = 60;
    }
    messageIssssss-=1;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
    self.bankListView.hidden = YES;
}

-(void)click{
    [self.view endEditing:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    @weakify(self);
    [g_server jxConfirmBankListToView:weak_self];
    // Do any additional setup after loading the view.
    // 1029修改
    
    self.isGotoBack = YES;
    self.title = @"";
    self.tableHeader.backgroundColor = HEXCOLOR(0xffffff);
    self.heightHeader = ManMan_SCREEN_TOP;
    self.view.backgroundColor = HEXCOLOR(0xffffff);
    self.heightFooter = 0;
    [self createHeadAndFoot];
    self.tableBody.backgroundColor = HEXCOLOR(0xffffff);
    self.tableBody.scrollEnabled = YES;
     messageIssssss = 60;
    
    
    
    UILabel *titLab = [[UILabel alloc] initWithFrame:CGRectMake(12, 15,ManMan_SCREEN_WIDTH-24 , 20)];
    titLab.text = @"添加银行卡";
    [self.tableBody addSubview:titLab];
    titLab.font = [UIFont systemFontOfSize:18];
    titLab.textAlignment = NSTextAlignmentCenter;
    
    UILabel *tipLabs = [[UILabel alloc] initWithFrame:CGRectMake(12, 15+ 20 + 20, ManMan_SCREEN_WIDTH-24, 15)];
    tipLabs.text = @"请绑定持卡人本人的银行卡";
    tipLabs.textColor = HEXCOLOR(0x333333);
    tipLabs.font = [UIFont systemFontOfSize:13];
    tipLabs.textAlignment = NSTextAlignmentCenter;
    [self.tableBody addSubview:tipLabs];
    
    
    
    UIView *bankBgView = [[UIView alloc] initWithFrame:CGRectMake(12, 50 + 40, ManMan_SCREEN_WIDTH-24, 146 + 50 + 48.5)];
    bankBgView.backgroundColor = HEXCOLOR(0xFFFFFF);
    bankBgView.layer.borderColor = HEXCOLOR(0xDEDFE1).CGColor;
//    bankBgView.layer.borderWidth = 0.5;
//    bankBgView.layer.cornerRadius =7;
    bankBgView.layer.masksToBounds = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(click)];
    [bankBgView addGestureRecognizer:tap];
    
    [self.tableBody addSubview:bankBgView];
    
    
    UILabel *nameLab = [[UILabel alloc] initWithFrame:CGRectMake(12, 0, 60, 48)];
    nameLab.font = [UIFont systemFontOfSize:15];
    NSMutableAttributedString *nameAtt = [[NSMutableAttributedString alloc] initWithString:@"持卡人"];
    nameLab.textColor = HEXCOLOR(0x333333);
    nameLab.attributedText = nameAtt;
    nameLab.textAlignment = NSTextAlignmentLeft;
    [bankBgView addSubview:nameLab];
    
    _nameTextF = [[UITextField alloc] initWithFrame:CGRectMake(100, 0, ManMan_SCREEN_WIDTH-90-24, 48)];
    _nameTextF.placeholder = @"持卡人姓名";
    _nameTextF.textColor = HEXCOLOR(0x333333);
    _nameTextF.font = [UIFont systemFontOfSize:15];
    [_nameTextF becomeFirstResponder];
    [bankBgView addSubview:_nameTextF];
    
    
    UIView *line0 = [[UIView alloc] initWithFrame:CGRectMake(0, 48, ManMan_SCREEN_WIDTH-24, 0.5)];
    line0.backgroundColor = HEXCOLOR(0xF5F7FA);
    [bankBgView addSubview:line0];
    
    
    
    
    UILabel *bankNumLab = [[UILabel alloc] initWithFrame:CGRectMake(12, 48.5, 60, 48)];
    bankNumLab.font = [UIFont systemFontOfSize:15];
    NSMutableAttributedString *numAtt = [[NSMutableAttributedString alloc] initWithString:@"卡号"];
    bankNumLab.textColor = HEXCOLOR(0x333333);
    bankNumLab.attributedText = numAtt;
    bankNumLab.textAlignment = NSTextAlignmentLeft;
    [bankBgView addSubview:bankNumLab];
    
    cardTextF = [[UITextField alloc] initWithFrame:CGRectMake(100, 48.5, ManMan_SCREEN_WIDTH-90-24, 48)];
    cardTextF.placeholder = @"持卡人本人银行卡号";
    cardTextF.textColor = HEXCOLOR(0x333333);
    cardTextF.font = [UIFont systemFontOfSize:15];
    cardTextF.keyboardType = UIKeyboardTypeNumberPad;
    [cardTextF becomeFirstResponder];
    [bankBgView addSubview:cardTextF];
    
    
    UIView *line1 = [[UIView alloc] initWithFrame:CGRectMake(0, 48+48.5, ManMan_SCREEN_WIDTH-24, 0.5)];
    line1.backgroundColor = HEXCOLOR(0xF5F7FA);
    [bankBgView addSubview:line1];
    
    
    
    UILabel *bankTypeLab = [[UILabel alloc] initWithFrame:CGRectMake(12, 48.5+48.5, 60, 48)];
    bankTypeLab.font = [UIFont systemFontOfSize:15];
    NSMutableAttributedString *bankTypeAtt = [[NSMutableAttributedString alloc] initWithString:@"卡类型"];
    bankTypeLab.attributedText = bankTypeAtt;
    bankTypeLab.textColor = HEXCOLOR(0x333333);
    bankTypeLab.textAlignment = NSTextAlignmentLeft;
    [bankBgView addSubview:bankTypeLab];
    
    UILabel *bankTypeSelLab = [[UILabel alloc] initWithFrame:CGRectMake(100, 48.5+48.5, 100, 48)];
    bankTypeSelLab.font = [UIFont systemFontOfSize:15];
    bankTypeSelLab.textColor = HEXCOLOR(0x333333);
    bankTypeSelLab.text = @"借记卡";
    bankTypeSelLab.textAlignment = NSTextAlignmentLeft;
    [bankBgView addSubview:bankTypeSelLab];
    
    UIView *line2 = [[UIView alloc] initWithFrame:CGRectMake(0, 48.5+48+48.5, ManMan_SCREEN_WIDTH-24, 0.5)];
    line2.backgroundColor = HEXCOLOR(0xF5F7FA);
    [bankBgView addSubview:line2];
    
    
    
    UILabel *bankLabel = [[UILabel alloc] initWithFrame:CGRectMake(12, line2.frame.origin.y + 0.5, 80, 48)];
    bankLabel.font = [UIFont systemFontOfSize:15];
    NSMutableAttributedString *bankTypeAtt1 = [[NSMutableAttributedString alloc] initWithString:@"银行"];
    bankLabel.textColor = HEXCOLOR(0x333333);
    bankLabel.attributedText = bankTypeAtt1;
    bankLabel.textAlignment = NSTextAlignmentLeft;
    [bankBgView addSubview:bankLabel];
    
    UIButton *bankBtn = [[UIButton alloc] initWithFrame:CGRectMake(100, bankLabel.frame.origin.y, ManMan_SCREEN_WIDTH - 90, 48)];
    bankBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [bankBtn setTitle:@"选择银行" forState:0];
    bankBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [bankBtn setTitleColor:HEXCOLOR(0x333333) forState:0];
    [bankBgView addSubview:bankBtn];
    self.bBtn = bankBtn;
    
    [bankBtn addTarget:self action:@selector(bankBtnClick) forControlEvents:(UIControlEventTouchUpInside)];
  
    
    UIView *line3 = [[UIView alloc] initWithFrame:CGRectMake(0, bankBtn.frame.origin.y + bankBtn.frame.size.height, ManMan_SCREEN_WIDTH-24, 0.5)];
    line3.backgroundColor = HEXCOLOR(0xF5F7FA);
    [bankBgView addSubview:line3];
    
    
    
    UILabel *titleLab = [[UILabel alloc] initWithFrame:CGRectMake(12, 49+48 + 48+48.5, 100, 48)];
    
    titleLab.font = [UIFont systemFontOfSize:15];
    NSMutableAttributedString *bankPhoneAtt = [[NSMutableAttributedString alloc] initWithString:@"手机号"];
    titleLab.textColor = HEXCOLOR(0x333333);
    titleLab.attributedText = bankPhoneAtt;
    
    titleLab.textAlignment = NSTextAlignmentLeft;
    [bankBgView addSubview:titleLab];
    
    self.phoneTextF= [[UITextField alloc] initWithFrame:CGRectMake(90, 48*2+1 + 48+48.5, ManMan_SCREEN_WIDTH-90-24, 48)];
    
    _phoneTextF.placeholder = @"请输入手机号";
    _phoneTextF.textColor = HEXCOLOR(0x333333);
    _phoneTextF.font = [UIFont systemFontOfSize:15];
    _phoneTextF.keyboardType = UIKeyboardTypeNumberPad;
//    _phoneTextF.userInteractionEnabled = NO;
    [bankBgView addSubview:_phoneTextF];
    
    
    
    
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(12, CGRectGetMaxY(bankBgView.frame) +12, ManMan_SCREEN_WIDTH-24-138, 48)];
    
    bgView.backgroundColor = HEXCOLOR(0xFFFFFF);
    bgView.layer.borderColor = HEXCOLOR(0xDEDFE1).CGColor;
    bgView.layer.masksToBounds = YES;
    [self.tableBody addSubview:bgView];
    
    UILabel *codeLab = [[UILabel alloc] initWithFrame:CGRectMake(12, 0, 100, 48)];
    codeLab.font = [UIFont systemFontOfSize:15];
    NSMutableAttributedString *codeAtt = [[NSMutableAttributedString alloc] initWithString:@"验证码"];
    codeLab.textColor = HEXCOLOR(0x333333);
    codeLab.attributedText = codeAtt;
    codeLab.textAlignment = NSTextAlignmentLeft;
    [bgView addSubview:codeLab];
    
    
    self.codeTextF= [[UITextField alloc] initWithFrame:CGRectMake(90, 0, bgView.frame.size.width, 48)];
    _codeTextF.placeholder = @"请输入短信验证码";
    _codeTextF.textColor = HEXCOLOR(0x333333);
    _codeTextF.font = [UIFont systemFontOfSize:15];
    _codeTextF.keyboardType = UIKeyboardTypeNumberPad;
    [bgView addSubview:_codeTextF];
    

    
    self.sendCodeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _sendCodeBtn.backgroundColor = HEXCOLOR(0xFFFFFF);
     _sendCodeBtn.layer.borderColor = HEXCOLOR(0xDEDFE1).CGColor;
//     _sendCodeBtn.layer.borderWidth = 0.5;
//     _sendCodeBtn.layer.cornerRadius =7;
     _sendCodeBtn.layer.masksToBounds = YES;
    [_sendCodeBtn setTitleColor:THEMECOLOR forState:UIControlStateNormal];
    [_sendCodeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    _sendCodeBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [_sendCodeBtn setFrame:CGRectMake(ManMan_SCREEN_WIDTH-100-12 , CGRectGetMaxY(bankBgView.frame) +12, 100, 48)];
    
    [_sendCodeBtn addTarget:self action:@selector(sendCodeBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.tableBody addSubview:_sendCodeBtn];
    
//    [_sendCodeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.mas_equalTo(-12);
//        make.top.mas_equalTo();
//        make.width.mas_equalTo(114);
//        make.height.mas_equalTo(48);
//    }];
    
    [self.p1a1y1Btn setFrame:CGRectMake(ManMan_SCREEN_WIDTH/2-70, CGRectGetMaxY(bgView.frame)+70, 140, 40)];

    [self.tableBody addSubview:self.p1a1y1Btn];
    [self.tableBody addSubview:self.agreeLab];
    [self.tableBody addSubview:self.agreeBtn];
    self.agreeBtn.selected = YES;

    
    [_agreeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.mas_equalTo(bgView.mas_bottom).offset(26);
    }];
//    @weakify(self);
    [_agreeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(weak_self.agreeLab.mas_left).offset(-8);
        make.centerY.mas_equalTo(weak_self.agreeLab);
        make.width.height.mas_equalTo(25);
    }];
    
    if ([self.isOpenBankStr isEqualToString:@"2"]) {
        _phoneTextF.userInteractionEnabled = NO;
    }else{
        _phoneTextF.userInteractionEnabled = YES;
    }
    _phoneTextF.text = self.cardPhone;

}
- (UIButton *)agreeBtn{
    if (!_agreeBtn) {
        _agreeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_agreeBtn setImage:[UIImage imageNamed:@"selected_fause"] forState:UIControlStateNormal];
        [_agreeBtn setImage:[UIImage imageNamed:@"selected_ture"] forState:UIControlStateSelected];
      
        [_agreeBtn addTarget:self action:@selector(agreeBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _agreeBtn;
}

- (void)bankBtnClick {
    __weak typeof(self)weakSelf = self;
    [self.view endEditing:YES];
    MYTView *listV = [[MYTView alloc] initWithFrame:CGRectMake(0, ManMan_SCREEN_HEIGHT -200, ManMan_SCREEN_WIDTH, 300)];
    listV.blocl = ^(NSString * _Nonnull str) {
        [weakSelf.bBtn setTitle:str forState:0];
    };

    [listV loadUIWithAry:self.bankAry];
    
    FWPopupBaseViewProperty *property = [FWPopupBaseViewProperty manager];
    property.popupAlignment = FWPopupAlignmentBottomCenter;
    property.popupAnimationStyle = FWPopupAnimationStylePosition;
    property.maskViewColor = [UIColor colorWithWhite:0 alpha:0.5];
    property.touchWildToHide = @"1";
    property.popupEdgeInsets = UIEdgeInsetsMake(0, 0, 10, 0);
    property.animationDuration = 0.5;
    property.usingSpringWithDamping = 0.7;
    listV.vProperty = property;
    
    [listV show];
    
}

-(void)agreeBtnAction:(UIButton *)sender{
    sender.selected = !sender.selected;
}
- (UILabel *)agreeLab {
    if (!_agreeLab) {
        _agreeLab = [UILabel new];
        _agreeLab.font = [UIFont systemFontOfSize:13];
        
        NSMutableAttributedString *att = [[NSMutableAttributedString alloc] initWithString:@"同意《用户协议》"];
        [att addAttribute:NSForegroundColorAttributeName value:HEXCOLOR(0x333333) range:NSMakeRange(0, 2)];
        [att addAttribute:NSForegroundColorAttributeName value:HEXCOLOR(0x4091F7) range:NSMakeRange(2, 6)];

        _agreeLab.attributedText = att;
        _agreeLab.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(agreeLabAction)];
        [_agreeLab addGestureRecognizer:tap];
        
        
    }
    return _agreeLab;
}
-(void)agreeLabAction{
    JXBaseWeb_VC *webVC = [JXBaseWeb_VC new];
    webVC.htmlBaseUrl = [NSString stringWithFormat:@"http://%@:8092/UserPolicy.html", APIURL];
    [g_notify postNotificationName:@"yonghuxieyi" object:nil];
    [g_navigation pushViewController:webVC animated:YES];
}

@end
