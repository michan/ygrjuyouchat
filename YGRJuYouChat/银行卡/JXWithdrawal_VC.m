//
//  JXWithdrawal_VC.m
//  shiku_im
//
//  Created by 韩银华 on 2020/7/6.
//  Copyright © 2020 Reese. All rights reserved.
//

#import "JXWithdrawal_VC.h"
#import "JXSelectDrarWayCell.h"
#import "JXDrawNumMoneyCell.h"
#import "JXSelectDrawWay_VC.h"
#import "JXBankList_VC.h"
#import "JXSettingBankPwd_VC.h"
#import "JXp1a1y1WithBankMoney_VC.h"
#import "JXp1a1y1BankListView.h"
#import "JXOpenBank_VC.h"
#import "JXBankPwdView.h"
#import "JXBaseWeb_VC.h"
#import "JXAddBankPhoneCode_VC.h"
#import <RPSDK/RPSDK.h>
#import <RPSDK/RPConfiguration.h>
#import "CertifyVC.h"
#import <AliyunIdentityManager/AliyunIdentityPublicApi.h>

@interface JXWithdrawal_VC ()<JXDrawNumMoneyDelegate,JXBankList_VCDelegate,JXp1a1y1BankListViewDelegate,UIAlertViewDelegate,JXBankPwdViewDelegate>
//tab
{
    NSString *drawMoneyStr;
    JXBankListModel *selectBankModel;
    JXBankPwdView *p1a1y1PwdView;

}
@property (nonatomic, strong) JXp1a1y1BankListView *selectBankView;
@property (nonatomic, strong) NSString *tokenString;// token
@property (nonatomic, assign) BOOL isCertiy;//是否认证
@end

@implementation JXWithdrawal_VC
- (instancetype)init{
    if (self = [super init]) {
        _isCertiy = NO;
        [self setBgUI];
    }
    return self;
}
- (void)viewDidLoad{
    [super viewDidLoad];
    [_wait start];
    [g_server checkCerityTokenToView:self];
    
    
}
- (void)setBgUI{
    
    self.title = @"提现";
    self.heightHeader = ManMan_SCREEN_TOP;
    self.heightFooter = 0;
    self.isGotoBack   = YES;
    [self createHeadAndFoot];
    self.isShowHeaderPull = NO;
    self.isShowFooterPull = NO;
    self.tableViewStyle = UITableViewStyleGrouped;
    self.tableView.backgroundColor = HEXCOLOR(0xffffff);
    self.selectBankView.hidden = YES;
    self.tableView.separatorStyle = 0;

}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    [self actionQuit];
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    JXSelectDrarWayCell *selectCell = [JXSelectDrarWayCell cellWithTableView:tableView];
    
    JXDrawNumMoneyCell *moneyCell = [JXDrawNumMoneyCell cellWithTableView:tableView];
    moneyCell.delegate = self;
    if (indexPath.section == 0) {
        if (selectBankModel) {
            selectCell.selectDrawWayModel = selectBankModel;
        }
        return selectCell;
    }else{

        return moneyCell;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return 48;
    }else{
        return 355;
    }
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headViws = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ManMan_SCREEN_WIDTH, 12)];
    headViws.backgroundColor = HEXCOLOR(0xF2F2F2);
    return headViws;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
   return 12;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return nil;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        
        if (self.selectBankView.isHidden) {
            self.selectBankView.hidden = NO;
            [self.selectBankView show];
            return;
        }else{
            self.selectBankView.hidden = NO;
            [self.selectBankView show];
            
        }
        
        //        JXBankList_VC *vc = [JXBankList_VC new];
//        vc.delegate = self;
//           [g_navigation pushViewController:vc animated:YES];
        
//        JXSelectDrawWay_VC *vc = [JXSelectDrawWay_VC new];
//        [g_navigation pushViewController:vc animated:YES];

    }
}
- (void)selectBankListMsg:(JXBankListModel *)bankModel{
    
    
    selectBankModel = bankModel;
    NSIndexSet *indexSet=[[NSIndexSet alloc]initWithIndex:0];
    [self.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];

    

}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}
- (void)jxAddNewBank{
    self.selectBankView.hidden = YES;
    JXOpenBank_VC *vc = [JXOpenBank_VC new];
    [g_navigation pushViewController:vc animated:YES];
}
-(void)jxSelectBankModel:(JXBankListModel *)bankModel{
    self.selectBankView.hidden = YES;
    selectBankModel = bankModel;
    NSIndexSet *indexSet=[[NSIndexSet alloc]initWithIndex:0];
    [self.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
}

- (void)jxqueckDwarMoney{
    
    if (!selectBankModel) {
        if (self.selectBankView.isHidden) {
            self.selectBankView.hidden = NO;
            [self.selectBankView show];
            [self.view endEditing:YES];
        }else{
            
            //            [self.bankMsgView show];
            
        }
    }

   
//

    if (drawMoneyStr.length == 0) {
        [JY_MyTools showTipView:@"提现金额不能为空"];
        return;
    }
//    if ([drawMoneyStr floatValue] < 50) {
//        [JXMyTools showTipView:@"提现金额最低为50元"];
//        return;
//    }
    if ([drawMoneyStr floatValue] > 20000) {
        [g_server showMsg:@"提现金额最高为20000元"];
        return;;
    }
    if (!selectBankModel) {
        [JY_MyTools showTipView:@"请选择银行卡"];
        return;
    }
    
    [self.view endEditing:YES];
    
    if (_isCertiy == NO) {
        // 未实名认证
        [JY_MyTools showTipView:@"您还未实名认证，请先进行实名认证"];
        [self pushToCertifyVC];
    }else{
    //获取token
        [g_server getFaceTokenToView:self];
    }
    
}
/// 活体
-(void)certifyBtnAction
{
    NSLog(@"去认证");
    
    RPConfiguration *configuration = [RPConfiguration configuration];
  
    configuration.isMutedByDefault = YES;
    @weakify(self)
    NSDictionary *extParams = [[NSMutableDictionary alloc]init];
    [extParams setValue:self forKey:@"currentCtr"];
    [extParams setValue:@"05D168" forKey:ZIM_EXT_PARAMS_KEY_OCR_BOTTOM_BUTTON_COLOR];
    [extParams setValue:@"05D168" forKey:ZIM_EXT_PARAMS_KEY_OCR_BOTTOM_BUTTON_CLICKED_COLOR];
    [extParams setValue:@"05D168" forKey:ZIM_EXT_PARAMS_KEY_OCR_FACE_CIRCLE_COLOR];
//    [extParams setValue:@"true" forKey:ZIM_EXT_PARAMS_KEY_USE_VIDEO_UPLOAD];
    
    [[AliyunIdentityManager sharedInstance] verifyWith:self.tokenString extParams:extParams onCompletion:^(ZIMResponse *response) {
         dispatch_async(dispatch_get_main_queue(), ^{
             
             NSString *title = @"刷脸成功";
             switch (response.code) {
                 case 1000:
                 {
                     [self->_wait start];
                     [g_server saveFaceAuthToView:weak_self];
//                     [g_server saveCerityTokenToView:weak_self];
                 }
                     break;
                 case 1003:
                     title = @"用户退出";
                     break;
                 case 2002:
                     title = @"网络错误";
                     break;
                 case 2006:
                     title = @"刷脸失败";
                     break;
                 default:
                     break;
             }
             [JY_MyTools showTipView:title];
         });
    }];
    
    
    return;
    
    [RPSDK startByNativeWithVerifyToken:self.tokenString viewController:self configuration:configuration progress:^(RPPhase phase) {
        NSLog(@"%ld",phase);
    } completion:^(RPResult * _Nonnull result) {
        // 建议接入方调用实人认证服务端接口DescribeVerifyResult，来获取最终的认证状态，并以此为准进行业务上的判断和处理。
        NSLog(@"实人认证结果：%@", result);
        switch (result.state) {
            case RPStatePass:
            {
                // 认证通过。
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self->_wait start];
                    [g_server saveFaceAuthToView:weak_self];
                });
            }
                break;
            case RPStateFail:
                // 认证不通过。
                [JY_MyTools showTipView:@"认证不通过"];
                break;
            case RPStateNotVerify:
                [JY_MyTools showTipView:@"认证失败"];
                // 未认证。
                // 通常是用户主动退出或者姓名身份证号实名校验不匹配等原因导致。
                // 具体原因可通过result.errorCode来区分（详见文末错误码说明表格）。
                break;
        }
    }];
    
}

/// 弹出输入密码
-(void)createPwdView{
    p1a1y1PwdView = [[JXBankPwdView alloc] initWithFrame:CGRectMake(0, 0, ManMan_SCREEN_WIDTH, ManMan_SCREEN_HEIGHT) pwdType:@"2"];
    p1a1y1PwdView.delegate = self;
    [p1a1y1PwdView show];
}
- (void)jxp1a1y1PwdMSgCode:(NSString *)msgCode{
    //避免重复点击提现
    JXDrawNumMoneyCell *moneyCell = (JXDrawNumMoneyCell *)[_table cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    moneyCell.queckBtn.userInteractionEnabled = NO;
    [_wait start];
    JY_UserObject *user = [[JY_UserObject alloc] init];
    user.payPassword = msgCode;
    [g_server checkp1a1y1PasswordWithUser:user toView:self];
    
}
-(void)jxBankAgreemensts{
    JXBaseWeb_VC *webVC = [JXBaseWeb_VC new];
    webVC.htmlBaseUrl = [NSString stringWithFormat:@"http://%@:8092/pages/UserPolicy.html", APIURL];
    [g_notify postNotificationName:@"yonghuxieyi" object:nil];
    [g_navigation pushViewController:webVC animated:YES];
}
-(void) didServerResultSucces:(JY_Connection*)aDownload dict:(NSDictionary*)dict array:(NSArray*)array1{
    
    [_wait stop];
    if ([aDownload.action isEqualToString:act_GetFaceToken]) {
        // 获取活体token成功
        if (dict != nil) {
            NSString *tokenStr = [dict objectForKey:@"resultObject"];
            if (tokenStr != nil && ![tokenStr isEqualToString:@""]) {
                self.tokenString = tokenStr;
                //活体
                [self certifyBtnAction];
                return;
            }
        }
        [self createPwdView];
        return;
    }
    else if ([aDownload.action isEqualToString:act_CheckCerityToken])
    {
        //是否认证
        NSDictionary *tempDict = [aDownload.responseData mj_JSONObject];
        BOOL isc = [[tempDict objectForKey:@"data"] boolValue];
        self.isCertiy = isc;
        return;
    }
    else if ([aDownload.action isEqualToString:act_SaveFaceAuth])
    {
        //保存活体认证成功
        self.isCertiy = YES;
        [self createPwdView];
        return;
    }
    
    JXDrawNumMoneyCell *moneyCell = (JXDrawNumMoneyCell *)[_table cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    
    if ([aDownload.action isEqual:act_Checkp1a1y1Password]) {
        [_wait start];
        if (p1a1y1PwdView) {
            [p1a1y1PwdView closeBtnAction];
        }
        moneyCell.queckBtn.userInteractionEnabled = NO;
        [g_server jxBornWithdrawalid:selectBankModel.cardId tranAmount:drawMoneyStr toView:self];
    }else{
        moneyCell.queckBtn.userInteractionEnabled = YES;
        [g_App showAlert:@"提现成功，稍后几分钟到账" delegate:self tag:1 onlyConfirm:YES];
        g_App.myMoney = g_App.myMoney - [drawMoneyStr doubleValue];
        [g_notify postNotificationName:kUpdateUserNotifaction object:nil];
    }
    
}
-(int) didServerResultFailed:(JY_Connection*)aDownload dict:(NSDictionary*)dict{
    [_wait stop];
    if ([aDownload.action isEqualToString:act_GetFaceToken]) {
        int resultCode = [[dict objectForKey:@"resultCode"] intValue];
        if (resultCode == 104501) {
            [JY_MyTools showTipView:@"请重新进行实人认证"];
            [self pushToCertifyVC];
            return hide_error;
        }else if(resultCode == 104500){
            //黑名单
            [JY_MyTools showTipView:@"你无权限提现"];
            return hide_error;
        }
    }
    if ([aDownload.action isEqualToString:act_CheckCerityToken]){
        return hide_error;
    }
    JXDrawNumMoneyCell *moneyCell = (JXDrawNumMoneyCell *)[_table cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    moneyCell.queckBtn.userInteractionEnabled = YES;

    return show_error;
}
//跳转到实名认证
-(void)pushToCertifyVC{
    // 未认证去做实名认证
    CertifyVC * webVC = [CertifyVC alloc];
    webVC = [webVC init];
   [g_navigation pushViewController:webVC animated:YES];
}

- (void)jxDrawNumMoney:(NSString *)moneyNum{
    drawMoneyStr = moneyNum;
}
- (JXp1a1y1BankListView *)selectBankView{
    if (!_selectBankView) {
        _selectBankView = [[JXp1a1y1BankListView alloc] initWithFrame:CGRectMake(0, 0, ManMan_SCREEN_WIDTH, ManMan_SCREEN_HEIGHT)];
        _selectBankView.delegate =self;
        
    }
    return _selectBankView;
}


- (void)didVerifyp1a1y1:(NSString *)sender {

    //
    NSLog(@"123");
}
@end
