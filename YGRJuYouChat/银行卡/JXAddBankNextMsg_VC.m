//
//  JXAddBankNextMsg_VC.m
//  shiku_im
//
//  Created by 韩银华 on 2020/7/22.
//  Copyright © 2020 Reese. All rights reserved.
//

#import "JXAddBankNextMsg_VC.h"
#import "JXAddBankPhoneCode_VC.h"
@interface JXAddBankNextMsg_VC ()
{
    UITextField *cardTextF;
}
//确认充值
@property (nonatomic, strong) UIButton *p1a1y1Btn;
@end

@implementation JXAddBankNextMsg_VC

- (id)init
{
    self = [super init];
    if (self) {
        
        self.isGotoBack = YES;
        self.title = @"添加银行卡";
        self.heightHeader = ManMan_SCREEN_TOP;
        self.heightFooter = 0;
        [self createHeadAndFoot];
        self.tableBody.backgroundColor = HEXCOLOR(0xF2F2F2);
        self.tableBody.scrollEnabled = YES;
        
        UILabel *tipLab = [[UILabel alloc] initWithFrame:CGRectMake(20, 20, ManMan_SCREEN_WIDTH-40, 30)];
        tipLab.textColor = HEXCOLOR(0x333333);
        tipLab.font = [UIFont systemFontOfSize:15];
        tipLab.text = @"请绑定持卡人本人银行卡";
        tipLab.textAlignment = NSTextAlignmentCenter;
        [self.tableBody addSubview:tipLab];
        
        
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(12, 70, ManMan_SCREEN_WIDTH-24, 40)];
        
        bgView.backgroundColor = HEXCOLOR(0xFFFFFF);
        bgView.layer.borderColor = HEXCOLOR(0xDEDFE1).CGColor;
        bgView.layer.borderWidth = 0.5;
        bgView.layer.cornerRadius =7;
        bgView.layer.masksToBounds = YES;
        [self.tableBody addSubview:bgView];
        
        UILabel *titleLab = [[UILabel alloc] initWithFrame:CGRectMake(12, 3, 45, 36)];
        
        titleLab.textColor = HEXCOLOR(0x333333);
        titleLab.font = [UIFont systemFontOfSize:15];
        titleLab.text = @"卡号";
        titleLab.textAlignment = NSTextAlignmentLeft;
        [bgView addSubview:titleLab];
        
        cardTextF = [[UITextField alloc] initWithFrame:CGRectMake(80, 0, ManMan_SCREEN_WIDTH-80-24, 40)];
        cardTextF.placeholder = @"持卡人本人银行卡号";
        cardTextF.textColor = HEXCOLOR(0x333333);
        cardTextF.font = [UIFont systemFontOfSize:15];
        cardTextF.keyboardType = UIKeyboardTypeNumberPad;
        [cardTextF becomeFirstResponder];
        [bgView addSubview:cardTextF];
        
        [self.tableBody addSubview:self.p1a1y1Btn];
 
     
        
    }
    return self;
}
- (UIButton *)p1a1y1Btn{
    if (!_p1a1y1Btn) {
        _p1a1y1Btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_p1a1y1Btn setFrame:CGRectMake(12, 150, ManMan_SCREEN_WIDTH-24, 40)];
        _p1a1y1Btn.backgroundColor = HEXCOLOR(0x42DD5E);
        _p1a1y1Btn.layer.cornerRadius = 7;
        _p1a1y1Btn.layer.masksToBounds = YES;
        [_p1a1y1Btn setTitle:@"下一步" forState:UIControlStateNormal];
        [_p1a1y1Btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _p1a1y1Btn.titleLabel.font = [UIFont boldSystemFontOfSize:17];
        [_p1a1y1Btn addTarget:self action:@selector(p1a1y1BtnNextAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _p1a1y1Btn;
}
-(void)p1a1y1BtnNextAction{
    
    if (cardTextF.text.length == 0) {
        [JY_MyTools showTipView:@"请填写持卡人卡号"];
        return;
        
    }
    
    
    NSString *timeStr = @"";
    if ([self.cardtype isEqual:@"信用卡"]) {
        timeStr = @"2020";
    }
    NSString *type;
    if ([self.cardtype isEqual:@"信用卡"]) {
        type = @"2";
    }else{
        type = @"1";
    }
    [_wait start];
    [g_server jxAddBankMsgCardNum:cardTextF.text cardTime:timeStr cardId:[_bankDic valueForKey:@"id"] cardCode:@"" cardType:type cvvTimes:@"" addBankPhone:self.cardPhone toView:self];

   
}
-(void) didServerResultSucces:(JY_Connection*)aDownload dict:(NSDictionary*)dict array:(NSArray*)array1{
    [_wait stop];
    JXAddBankPhoneCode_VC *vc = [JXAddBankPhoneCode_VC new];
    vc.cardName = self.cardName;
    vc.cardtype = self.cardtype;
    vc.cardIdNum = self.cardIdNum;
    vc.cardPhone = self.cardPhone;
    vc.bankcardNums = cardTextF.text;
    vc.bankAddId = [dict valueForKey:@"id"];
    vc.bankOrderId = [dict valueForKey:@"ncountOrderId"];
    vc.bankDict = self.bankDic;
    [g_navigation pushViewController:vc animated:YES];
    
}
-(int) didServerResultFailed:(JY_Connection*)aDownload dict:(NSDictionary*)dict{
    [_wait stop];

    NSLog(@"123");
   
    return show_error;
}




- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


@end
