//
//  JXOpenBank_VC.m
//  shiku_im
//
//  Created by 韩银华 on 2020/7/11.
//  Copyright © 2020 Reese. All rights reserved.
//

#import "JXOpenBank_VC.h"
#import "JXOpenBankMsgView.h"
#import "JXAddBank_VC.h"
#import "JHVerificationCodeView.h"
#import "JXAddBankPhoneCode_VC.h"
@interface JXOpenBank_VC ()<JXOpenBankMsgViewDelegate,ManMan_ServerResult>
{
    NSString *nikeNameStr;
    NSString *idCardNumStr;
    NSString *phoneNumStr;
}
//view
@property (nonatomic, strong) JXOpenBankMsgView *openBankView;

@end

@implementation JXOpenBank_VC
- (id)init
{
    self = [super init];
    if (self) {
        
        self.isGotoBack = YES;
        
        self.heightHeader = ManMan_SCREEN_TOP;
        self.heightFooter = 0;
        self.title = @"验证密码";
        [self createHeadAndFoot];
        self.tableBody.backgroundColor = HEXCOLOR(0xF2F2F2);
        self.tableBody.scrollEnabled = YES;
        
//        [self.tableBody addSubview:self.openBankView];
        [self creatUI];
    }
    return self;
}
-(void)creatUI{
    
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(12, 12, ManMan_SCREEN_WIDTH-24, 200)];
    bgView.backgroundColor = [UIColor whiteColor];
    bgView.layer.borderColor = HEXCOLOR(0xDEDEE0).CGColor;
    bgView.layer.borderWidth = 0.5;
    bgView.layer.cornerRadius = 7;
    bgView.layer.masksToBounds = YES;
    [self.tableBody addSubview:bgView];
    
    UILabel *titleLab = [UILabel new];
    titleLab.textColor = HEXCOLOR(0x333333);
    titleLab.font =[UIFont boldSystemFontOfSize:16];
    titleLab.text = @"添加银行卡";
    [bgView addSubview:titleLab];
    
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.mas_equalTo(20.5);
    }];
    
    
    UIView *line = [UIView new];
    line.backgroundColor = HEXCOLOR(0xF3F3F3);
    [bgView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(53);
        make.height.mas_equalTo(0.5);
    }];
    
    
//    UILabel *otherLab = [UILabel new];
//    otherLab.textColor = HEXCOLOR(0x656565);
//    otherLab.font =[UIFont systemFontOfSize:14];
//    otherLab.text = @"请输入支付码，已验证身份";
//    [bgView addSubview:otherLab];
//
//    [otherLab mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerX.mas_equalTo(0);
//        make.top.mas_equalTo(70);
//    }];
//
//
//    JHVCConfig *config     = [[JHVCConfig alloc] init];
//    config.autoShowKeyboard = YES;
//    config.inputBoxNumber  = 6;
//    config.inputBoxSpacing = 0;
//    config.inputBoxWidth   = (ManMan_SCREEN_WIDTH-24-54)/6;
//    config.inputBoxHeight  = 47;
//    config.tintColor       = HEXCOLOR(0xD1D1D1);
//    config.secureTextEntry = YES;
//    config.inputBoxColor   = HEXCOLOR(0xB7B7B7);
//    config.font            = [UIFont boldSystemFontOfSize:22];
//    config.textColor       = HEXCOLOR(0x333333);
//    config.inputType       = JHVCConfigInputType_Alphabet;
//    config.inputBoxBorderWidth  = 0.5;
//    config.showUnderLine = NO;
//    config.underLineSize = CGSizeMake(33, 2);
//    config.underLineColor = HEXCOLOR(0xD1D1D1);
//    config.underLineHighlightedColor = HEXCOLOR(0xD1D1D1);
//    JHVerificationCodeView *codeView = [[JHVerificationCodeView alloc] initWithFrame:CGRectMake(27, 113, ManMan_SCREEN_WIDTH-24-54, 47) config:config];
//
//    codeView.finishBlock = ^(NSString *code) {
//        JY_UserObject *user = [[JY_UserObject alloc] init];
//        user.payPassword = code;
//        [g_server checkp1a1y1PasswordWithUser:user toView:self];
//    };
//    [bgView addSubview:codeView];
    [g_server jxIsOpenBankMsg:self];
//
}

- (void)jxOpenBnakNext{
    
    if (nikeNameStr.length == 0) {
        [JY_MyTools showTipView:@"持卡人名字不能为空"];
        return;
    }
    if (idCardNumStr.length == 0) {
        [JY_MyTools showTipView:@"身份证不能为空"];
        return;
    }
    
    if (phoneNumStr.length == 0) {
        [JY_MyTools showTipView:@"手机号不能为空"];
        return;
    }
    
    
    
    
}
-(void) didServerResultSucces:(JY_Connection*)aDownload dict:(NSDictionary*)dict array:(NSArray*)array1{
  
    
    if ([aDownload.action isEqualToString:act_Checkp1a1y1Password]) {
        [g_server jxIsOpenBankMsg:self];
    }else{
        
        if ([[dict valueForKey:@"resultObject"] isEqualToString:@"未开户"]) {
            JXAddBank_VC *vc = [JXAddBank_VC new];
            [g_navigation pushViewController:vc animated:YES];
        }else{
            JXAddBankPhoneCode_VC *vc = [JXAddBankPhoneCode_VC new];
            vc.cardName = [dict valueForKey:@"userName"];
            vc.cardIdNum = [dict valueForKey:@"certNo"];
            vc.cardtype = @"1";
            vc.cardPhone = [dict valueForKey:@"mobile"];
            vc.bankOrderId = [dict valueForKey:@"id"];
            vc.isOpenBankStr = @"1";
            [g_navigation pushViewController:vc animated:YES];
        }
    }
}
-(int) didServerResultFailed:(JY_Connection*)aDownload dict:(NSDictionary*)dict{
    //    [_wait stop];
    
    NSLog(@"123");
    
    return show_error;
}

- (void)jxBankPersonName:(NSString *)nikeName{
    nikeNameStr = nikeName;
}
- (void)jxBankIdCardNum:(NSString *)cardNum{
    idCardNumStr = cardNum;
}
- (void)jxBankTelphoneNums:(NSString *)phoneNum{
    phoneNumStr = phoneNum;
}
- (JXOpenBankMsgView *)openBankView{
    if (!_openBankView) {
        _openBankView = [[JXOpenBankMsgView alloc] initWithFrame:CGRectMake(0, 0, ManMan_SCREEN_WIDTH, ManMan_SCREEN_HEIGHT)];
        _openBankView.delegate = self;
    }
    return _openBankView;
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


@end
