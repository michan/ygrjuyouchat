//
//  JXAddBankSuccess_VC.m
//  shiku_im
//
//  Created by 韩银华 on 2020/7/11.
//  Copyright © 2020 Reese. All rights reserved.
//

#import "JXAddBankSuccess_VC.h"
#import "JXBankList_VC.h"
@interface JXAddBankSuccess_VC ()
//背景
@property (nonatomic, strong) UIView *bgView;
//成功图片
@property (nonatomic, strong) UIImageView *successPic;
//按钮
@property (nonatomic, strong) UIButton *okBtns;


@end

@implementation JXAddBankSuccess_VC
- (id)init
{
    self = [super init];
    if (self) {
        
        self.isGotoBack = YES;
//        self.title = @"添加银行卡";
        self.heightHeader = ManMan_SCREEN_TOP;
        self.heightFooter = 0;
        [self createHeadAndFoot];
        self.tableBody.backgroundColor = HEXCOLOR(0xF2F2F2);
        self.tableBody.scrollEnabled = YES;
        [self.tableBody addSubview:self.bgView];
        [self.bgView addSubview:self.successPic];
        [self.bgView addSubview:self.okBtns];
        
        [_bgView setFrame:CGRectMake(0, 0, ManMan_SCREEN_WIDTH, ManMan_SCREEN_HEIGHT)];
    
        
        [_successPic mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(0);
            make.top.mas_equalTo(150);
            make.width.height.mas_equalTo(100);
        }];

 
        [_okBtns mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(0);
            make.bottom.mas_equalTo(ManMan_SCREEN_BOTTOM-80);
            make.width.mas_equalTo(200);
            make.height.mas_equalTo(40);
        }];
        
    }
    return self;
}

- (UIView *)bgView{
    if (!_bgView) {
        _bgView = [UIView new];
        _bgView.backgroundColor = [UIColor whiteColor];
        _bgView.layer.cornerRadius = 7;
        _bgView.layer.masksToBounds = YES;
        _bgView.layer.borderColor = HEXCOLOR(0xDEDFE1).CGColor;
        _bgView.layer.borderWidth = 0.5;
    }
    return _bgView;
}
- (UIImageView *)successPic{
    if (!_successPic) {
        _successPic = [UIImageView new];
        _successPic.image = [UIImage imageNamed:@""];
    }
    return _successPic;
}
- (UIButton *)okBtns{
    if (!_okBtns) {
        _okBtns = [UIButton buttonWithType:UIButtonTypeCustom];
        _okBtns.backgroundColor = HEXCOLOR(0x42DD5E);
        [_okBtns setTitle:@"完成" forState:UIControlStateNormal];
        [_okBtns setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _okBtns.titleLabel.font = [UIFont boldSystemFontOfSize:17];
        _okBtns.layer.cornerRadius = 7;
        _okBtns.layer.masksToBounds = YES;
        [_okBtns addTarget:self action:@selector(cuccessBankAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _okBtns;
}
-(void)cuccessBankAction{
    JXBankList_VC *vc = [JXBankList_VC new];
    [g_navigation pushViewController:vc animated:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
