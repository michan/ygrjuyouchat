//
//  JXBankListModel.m
//  shiku_im
//
//  Created by 韩银华 on 2020/7/12.
//  Copyright © 2020 Reese. All rights reserved.
//

#import "JXBankListModel.h"

@implementation JXBankListModel

- (void)getAllBankList:(NSDictionary *)dict{
    self.cardNo = [dict objectForKey:@"cardNo"];
    self.cardType = [dict objectForKey:@"cardType"];
    self.certNo = [dict objectForKey:@"certNo"];
    self.cardId = [dict objectForKey:@"id"];
    self.merUserId = [dict objectForKey:@"merUserId"];
    self.mobile = [dict objectForKey:@"mobile"];
    self.bankStatus = [dict objectForKey:@"status"];
    self.bankTime = [dict objectForKey:@"time"];
    self.userId = [dict objectForKey:@"userId"];
    self.userName = [dict objectForKey:@"userName"];
    self.userNo = [dict objectForKey:@"userNo"];
    self.bankName = [dict objectForKey:@"bankName"];
}

@end
