//
//  JXBankListModel.h
//  shiku_im
//
//  Created by 韩银华 on 2020/7/12.
//  Copyright © 2020 Reese. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface JXBankListModel : NSObject

//银行卡号
@property (nonatomic, strong) NSString *cardNo;
//银行卡类型1、借记卡  2、信用卡
@property (nonatomic, strong) NSString *cardType;
//身份证号
@property (nonatomic, strong) NSString *certNo;
//ID
@property (nonatomic, strong) NSString *cardId;
//ID
@property (nonatomic, strong) NSString *merUserId;
//手机号
@property (nonatomic, strong) NSString *mobile;
//状态
@property (nonatomic, strong) NSString *bankStatus;
//时间
@property (nonatomic, strong) NSString *bankTime;
//用户ID
@property (nonatomic, strong) NSString *userId;
//用户昵称
@property (nonatomic, strong) NSString *userName;
//
@property (nonatomic, strong) NSString *userNo;

//背景
@property (nonatomic, strong) NSString *bankBgPic;
//银行卡名字
@property (nonatomic, strong) NSString *bankName;

//记录当前选择的状态
@property (nonatomic, strong) NSString *selectBankStr;


- (void)getAllBankList:(NSDictionary *)dict;


@end

NS_ASSUME_NONNULL_END
