//
//  JXBankListCell.h
//  shiku_im
//
//  Created by 韩银华 on 2020/7/7.
//  Copyright © 2020 Reese. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JXBankListModel.h"
NS_ASSUME_NONNULL_BEGIN

@protocol JXBankListCellDelegate <NSObject>

-(void)jxUnbingBank:(JXBankListModel *)bankModel index:(NSInteger)selectIndexs;

@end

@interface JXBankListCell : UITableViewCell

+(instancetype)cellWithTableView:(UITableView *)tableView;

//index
@property (nonatomic, assign) NSInteger selectBankIndex;


//数据源
@property (nonatomic, strong) JXBankListModel *bankModel;

//代理
@property (nonatomic, assign) id<JXBankListCellDelegate>delegate;


@end

NS_ASSUME_NONNULL_END
