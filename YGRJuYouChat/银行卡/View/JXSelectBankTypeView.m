//
//  JXSelectBankTypeView.m
//  shiku_im
//
//  Created by 韩银华 on 2020/7/22.
//  Copyright © 2020 Reese. All rights reserved.
//

#import "JXSelectBankTypeView.h"

@interface JXSelectBankTypeView ()<UIPickerViewDelegate,UIPickerViewDataSource,UIGestureRecognizerDelegate>
//背景
@property (nonatomic, strong) UIView *bgView;
//picker
//pickerview
@property (nonatomic, strong) UIPickerView *pickerViews;
//数据源
@property (nonatomic, strong) NSArray *dataSorce;
@end

@implementation JXSelectBankTypeView
- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.35];
        
        [self initUI];
    }
    return self;
}
-(void)initUI{
    [self addSubview:self.bgView];
    [self.bgView addSubview:self.pickerViews];
    
    [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(0);
        make.height.mas_equalTo(220);
    }];
    
    [_pickerViews mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(20);
        make.left.right.bottom.mas_equalTo(0);
    }];
    
}
- (void)show{
    UIWindow *window = [UIApplication sharedApplication].delegate.window;
    [window addSubview:self];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(disMissSelectTypeView)];
    tap.delegate = self;
    [self addGestureRecognizer:tap];
    
    [self initUI];
    
    
}
#pragma mark - dataSouce
//有几行
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}
//行中有几列
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return self.dataSorce.count;
}
-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 40;
}
//列显示的数据
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger) row forComponent:(NSInteger)component {
    return self.dataSorce[row];
}
#pragma mark - delegate
// 选中某一组的某一行
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    NSString *selFood = self.dataSorce[row];
    
    
    if (self.delegate) {
        [self.delegate jxSelectBankType:selFood];
    }
    
    [self disMissSelectTypeView];
}
- (UIView *)bgView{
    if (!_bgView) {
        _bgView = [UIView new];
        _bgView.backgroundColor = [UIColor whiteColor];
    }
    return _bgView;
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if ([touch.view isDescendantOfView:self.bgView]) {
        return NO;
    }
    return YES;
}
- (UIPickerView *)pickerViews {
    if (!_pickerViews) {
        _pickerViews = [[UIPickerView alloc] initWithFrame:CGRectZero];
        _pickerViews.delegate = self;
        _pickerViews.dataSource = self;
    }
    return _pickerViews;
    
}
- (NSArray *)dataSorce{
    if (!_dataSorce) {
        _dataSorce = @[@"工商银行",
                       @"招商银行",
                       @"建设银行",
                       @"农业银行",
                       @"邮储银行",
                       @"中国银行"];
    }
    return _dataSorce;
}
-(void)disMissSelectTypeView{
    [self removeFromSuperview];
}
@end
