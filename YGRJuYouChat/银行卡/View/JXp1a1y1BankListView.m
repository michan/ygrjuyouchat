
//
//  JXp1a1y1BankListView.m
//  shiku_im
//
//  Created by 韩银华 on 2020/7/16.
//  Copyright © 2020 Reese. All rights reserved.
//

#import "JXp1a1y1BankListView.h"
#import "JXp1a1y1SelectBankCell.h"
@interface JXp1a1y1BankListView ()<UITableViewDelegate,UITableViewDataSource>
//背景
@property (nonatomic, strong) UIView *bgView;
//提示
@property (nonatomic, strong) UILabel *tipLab;
//关闭
@property (nonatomic, strong) UIButton *closeBtn;
//line
@property (nonatomic, strong) UIView *line;
@property (nonatomic, strong) UIView *line1;

@property (nonatomic, strong) UIView *closeBgView;


//银行卡列表
@property (nonatomic, strong) UITableView *bankTab;

//添加银行卡
@property (nonatomic, strong) UIButton *addBankBtn;
//数据源
@property (nonatomic, strong) NSMutableArray *bankArr;



@end

@implementation JXp1a1y1BankListView
- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.35];
        
        [self initUI];
    }
    return self;
}

-(void)requestData{
    if (self.bankArr.count > 0) {
        [self.bankTab reloadData];
    }else{
        [g_server jxBankNewsListtoView:self];
    }

}

-(void) didServerResultSucces:(JY_Connection*)aDownload dict:(NSDictionary*)dict array:(NSArray*)array1{
    NSLog(@"123");
    [self.bankArr removeAllObjects];
 for (int i = 0; i < array1.count; i++) {
     JXBankListModel *model = [[JXBankListModel alloc] init];
     [model getAllBankList:[array1 objectAtIndex:i]];
     [self.bankArr addObject:model];
 }
 
 [self.bankTab reloadData];
    
}
-(int) didServerResultFailed:(JY_Connection*)aDownload dict:(NSDictionary*)dict{
//    [_wait stop];

    NSLog(@"123");

    return show_error;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.bankArr.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    JXp1a1y1SelectBankCell *cell = [JXp1a1y1SelectBankCell cellWithTableView:tableView];
    cell.bankModel = self.bankArr[indexPath.row];
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return nil;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.01;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return nil;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //遍历viewModel的数组，如果点击的行数对应的viewModel相同，将isSelected变为Yes，反之为No
    for (NSInteger i = 0; i<self.bankArr.count; i++) {
        JXBankListModel *itemViewModel = self.bankArr[i];
        if (i!=indexPath.row) {
            itemViewModel.selectBankStr = @"1";
        }else if (i == indexPath.row){
            itemViewModel.selectBankStr = @"2";
        }
    }
    [self.bankTab reloadData];
    
    if (self.delegate) {
        [self.delegate jxSelectBankModel:self.bankArr[indexPath.row]];
    }
    
}


-(void)initUI{
    [self addSubview:self.bgView];
    [self.bgView addSubview:self.tipLab];
    [self.bgView addSubview:self.closeBgView];
    [self.closeBgView addSubview:self.closeBtn];
    [self.bgView addSubview:self.line];
    [self.bgView addSubview:self.bankTab];
    [self.bgView addSubview:self.line1];
    [self.bgView addSubview:self.addBankBtn];
    
    [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(0);
        make.height.mas_equalTo(267);
    }];
    
    [_tipLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12.5);
        make.top.mas_equalTo(14);
    }];
    @weakify(self);
    [_line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(weak_self.tipLab.mas_bottom).offset(14);
        make.height.mas_equalTo(0.5);
    }];
    
    [_closeBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.right.mas_equalTo(0);
        make.width.mas_equalTo(100);
        make.bottom.mas_equalTo(weak_self.line.mas_top);
    }];
    
    [_closeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.centerY.mas_equalTo(weak_self.closeBgView);
        make.width.height.mas_equalTo(15);
    }];
    
    [_line1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(-54.5);
        make.height.mas_equalTo(0.5);
    }];
    
    
    [_addBankBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(0);
        make.height.mas_equalTo(54);
    }];
    
    
    [_bankTab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(weak_self.line).offset(0.1);
        make.bottom.mas_equalTo(-55);
    }];
    
    
    
}
- (UIView *)bgView{
    if (!_bgView) {
        _bgView = [UIView new];
        _bgView.backgroundColor = [UIColor whiteColor];
    }
    return _bgView;
}
- (UILabel *)tipLab{
    if (!_tipLab) {
        _tipLab = [UILabel new];
        _tipLab.textColor = HEXCOLOR(0x333333);
        _tipLab.font = [UIFont boldSystemFontOfSize:17];
        _tipLab.text = @"请选择银行卡";
    }
    return _tipLab;
}
- (UIView *)line{
    if (!_line) {
        _line = [UIView new];
        _line.backgroundColor = HEXCOLOR(0xEEEEEE);
    }
    return _line;
}
- (UIView *)line1{
    if (!_line1) {
        _line1 = [UIView new];
        _line1.backgroundColor = HEXCOLOR(0xEEEEEE);
    }
    return _line1;
}
- (UIView *)closeBgView{
    if (!_closeBgView) {
        _closeBgView = [UIView new];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeBtnAction)];
        [_closeBgView addGestureRecognizer:tap];
    }
    return _closeBgView;
}
- (UIButton *)closeBtn{
    if (!_closeBtn) {
        _closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_closeBtn setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
        [_closeBtn addTarget:self action:@selector(closeBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _closeBtn;
}
- (UIButton *)addBankBtn{
    if (!_addBankBtn) {
        _addBankBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_addBankBtn setTitle:@"+添加银行卡" forState:UIControlStateNormal];
        [_addBankBtn setTitleColor:HEXCOLOR(0x333333) forState:UIControlStateNormal];
        _addBankBtn.titleLabel.font = [UIFont boldSystemFontOfSize:16];
        
        [_addBankBtn addTarget:self action:@selector(addBankBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _addBankBtn;
}
- (UITableView *)bankTab{
    if (!_bankTab) {
        _bankTab = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _bankTab.backgroundColor = [UIColor whiteColor];
        _bankTab.delegate = self;
        _bankTab.dataSource = self;
    }
    return _bankTab;
}
- (NSMutableArray *)bankArr{
    if (!_bankArr) {
        _bankArr = [NSMutableArray array];
    }
    return _bankArr;
}

- (void)show{
    UIWindow *window = [UIApplication sharedApplication].delegate.window;
    [window addSubview:self];
    
    [self initUI];
    
    [self requestData];
}
-(void)closeBtnAction{
    self.hidden = YES;
}
-(void)addBankBtnAction{
    if (self.delegate) {
        [self.delegate jxAddNewBank];
    }
}
@end
