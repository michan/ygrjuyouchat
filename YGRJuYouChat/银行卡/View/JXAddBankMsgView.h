//
//  JXAddBankMsgView.h
//  shiku_im
//
//  Created by 韩银华 on 2020/7/7.
//  Copyright © 2020 Reese. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol JXAddBankMsgViewDelegate <NSObject>

//下一步
-(void)nextAddBank;
//持卡人
-(void)jxBankName:(NSString *)addName;
//卡号
-(void)jxBankCardNum:(NSString *)cardNum;

//手机号
-(void)jxBankSelectCardType:(NSString *)bankCardType;

//手机号
-(void)jxBankPhone:(NSString *)bankPhone;

//用户协议
-(void)jxAggresBankMsg;

-(void)backClick;

@end

@interface JXAddBankMsgView : UIView

//选择了卡
@property (nonatomic, strong) NSString *addBankTypeStr;



@property (nonatomic, assign) id<JXAddBankMsgViewDelegate>delegate;

@end

NS_ASSUME_NONNULL_END
