//
//  JXSelectBankTypeView.h
//  shiku_im
//
//  Created by 韩银华 on 2020/7/22.
//  Copyright © 2020 Reese. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol JXSelectBankTypeViewDelegate <NSObject>

-(void)jxSelectBankType:(NSString *)bankTypes;

@end

@interface JXSelectBankTypeView : UIView

//代理
@property (nonatomic, assign) id <JXSelectBankTypeViewDelegate>delegate;


-(void)show;

@end

NS_ASSUME_NONNULL_END
