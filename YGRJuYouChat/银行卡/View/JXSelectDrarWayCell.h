//
//  JXSelectDrarWayCell.h
//  shiku_im
//
//  Created by 韩银华 on 2020/7/7.
//  Copyright © 2020 Reese. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JXBankListModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface JXSelectDrarWayCell : UITableViewCell

+(instancetype)cellWithTableView:(UITableView *)tableView;

//选择了那种方式
@property (nonatomic, strong) NSString *selectDrawWayStr;
@property (nonatomic, strong) JXBankListModel *selectDrawWayModel;



@end

NS_ASSUME_NONNULL_END
