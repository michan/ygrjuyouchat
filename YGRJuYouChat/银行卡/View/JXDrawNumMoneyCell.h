//
//  JXDrawNumMoneyCell.h
//  shiku_im
//
//  Created by 韩银华 on 2020/7/7.
//  Copyright © 2020 Reese. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol JXDrawNumMoneyDelegate <NSObject>
//输入的金额
-(void)jxDrawNumMoney:(NSString *)moneyNum;
//全部提现金额
-(void)jxDwarAllMoney:(NSString *)allMoney;
//确认提现
-(void)jxqueckDwarMoney;

//协议
-(void)jxBankAgreemensts;

@end

@interface JXDrawNumMoneyCell : UITableViewCell
//确认提现
@property (nonatomic, strong) UIButton *queckBtn;

+(instancetype)cellWithTableView:(UITableView *)tableView;

//代理
@property (nonatomic, assign) id<JXDrawNumMoneyDelegate>delegate;

@end

NS_ASSUME_NONNULL_END
