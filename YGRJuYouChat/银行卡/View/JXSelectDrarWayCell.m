//
//  JXSelectDrarWayCell.m
//  shiku_im
//
//  Created by 韩银华 on 2020/7/7.
//  Copyright © 2020 Reese. All rights reserved.
//

#import "JXSelectDrarWayCell.h"

@interface JXSelectDrarWayCell ()
//背景
@property (nonatomic, strong) UIView *bgView;
//提现方式
@property (nonatomic, strong) UILabel *tipLab;
//更多
@property (nonatomic, strong) UIImageView *morePic;


@end

@implementation JXSelectDrarWayCell

+(instancetype)cellWithTableView:(UITableView *)tableView{
    JXSelectDrarWayCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JXSelectDrarWayCell"];
    if (!cell) {
        cell = [[JXSelectDrarWayCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"JXSelectDrarWayCell"];
    }
    return cell;
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.backgroundColor = HEXCOLOR(0xF2F2F2);

        [self initUI];
    }
    return self;
}
-(void)initUI{
    [self.contentView addSubview:self.bgView];
    [self.bgView addSubview:self.tipLab];
    [self.bgView addSubview:self.morePic];
    [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.height.mas_equalTo(48);
    }];
    
    [_tipLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(0);
        make.left.mas_equalTo(15.5);
    }];
    
    [_morePic mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15.5);
        make.centerY.mas_equalTo(0);
        make.width.mas_equalTo(7.5);
        make.height.mas_equalTo(9.5);
    }];
    
}
- (void)setSelectDrawWayModel:(JXBankListModel *)selectDrawWayModel{
//    _tipLab.text = @"提现到银行卡";
    
    NSString *carNum = selectDrawWayModel.cardNo;
    
    carNum = [carNum stringByReplacingCharactersInRange:NSMakeRange(0, carNum.length-4) withString:@""];
    
    NSString *showBankName;
    if (selectDrawWayModel.bankName.length == 0) {
        showBankName = [NSString stringWithFormat:@"到账银行卡(%@)",carNum];
    }else{
        showBankName = [NSString stringWithFormat:@"到账银行卡        %@(%@)",selectDrawWayModel.bankName,carNum];
    }
    
    _tipLab.text = showBankName;
    
}
- (UIView *)bgView{
    if (!_bgView) {
        _bgView = [UIView new];
        _bgView.backgroundColor = HEXCOLOR(0xf0f0f0);
//        _bgView.layer.borderColor = HEXCOLOR(0x1).CGColor;
//        _bgView.layer.borderWidth = 0.5;
//        _bgView.layer.cornerRadius = 7;
//        _bgView.layer.masksToBounds = YES;
    }
    return _bgView;
}
- (UILabel *)tipLab{
    if (!_tipLab) {
        _tipLab = [UILabel new];
        _tipLab.textColor = HEXCOLOR(0x333333);
        _tipLab.text = @"选择提现方式";
        _tipLab.font = [UIFont systemFontOfSize:15];
    }
    return _tipLab;
}
- (UIImageView *)morePic{
    if (!_morePic) {
        _morePic = [UIImageView new];
        _morePic.image = [UIImage imageNamed:@"morePic"];
    }
    return _morePic;
}

@end
