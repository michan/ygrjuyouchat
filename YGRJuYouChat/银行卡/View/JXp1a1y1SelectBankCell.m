
//
//  JXp1a1y1SelectBankCell.m
//  shiku_im
//
//  Created by 韩银华 on 2020/7/16.
//  Copyright © 2020 Reese. All rights reserved.
//

#import "JXp1a1y1SelectBankCell.h"

@interface JXp1a1y1SelectBankCell ()
//银行卡名字
@property (nonatomic, strong) UILabel *bankNameLab;

//银行卡
@property (nonatomic, strong) UILabel *bankTitleLab;
//选择按钮
@property (nonatomic, strong) UIImageView *selectPic;


@end

@implementation JXp1a1y1SelectBankCell


+(instancetype)cellWithTableView:(UITableView *)tableView{
    JXp1a1y1SelectBankCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JXp1a1y1SelectBankCell"];
    if (!cell) {
        cell = [[JXp1a1y1SelectBankCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"JXp1a1y1SelectBankCell"];
    }
    return cell;
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.backgroundColor = HEXCOLOR(0xFFFFFF);

        [self initUI];
    }
    return self;
}
- (void)setBankModel:(JXBankListModel *)bankModel{
    _bankNameLab.text = bankModel.bankName;
    
    NSString *cardNum = bankModel.cardNo;
    
    
    cardNum = [cardNum stringByReplacingCharactersInRange:NSMakeRange(4, cardNum.length-8) withString:@"********"];
    
    _bankTitleLab.text = cardNum;
    if ([bankModel.selectBankStr isEqual:@"2"]) {
        _selectPic.hidden = NO;
    }else{
        _selectPic.hidden = YES;
    }
    
}

-(void)initUI{
    [self.contentView addSubview:self.bankNameLab];
    [self.contentView addSubview:self.bankTitleLab];
    [self.contentView addSubview:self.selectPic];
    
    [_bankNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.centerY.mas_equalTo(0);
    }];
    @weakify(self);
    
    [_bankTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(weak_self.bankNameLab.mas_right).offset(45);
        make.centerY.mas_equalTo(0);
    }];
    
    [_selectPic mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.centerY.mas_equalTo(0);
        make.width.mas_equalTo(15.5);
        make.height.mas_equalTo(14.5);
    }];
    
}
- (UILabel *)bankNameLab{
    if (!_bankNameLab) {
        _bankNameLab = [UILabel new];
        _bankNameLab.textColor = HEXCOLOR(0x333333);
        _bankNameLab.font = [UIFont systemFontOfSize:15];
    }
    return _bankNameLab;
}
- (UILabel *)bankTitleLab{
    if (!_bankTitleLab) {
        _bankTitleLab = [UILabel new];
        _bankTitleLab.textColor = HEXCOLOR(0x333333);
        _bankTitleLab.font = [UIFont boldSystemFontOfSize:14];
    }
    return _bankTitleLab;
}
- (UIImageView *)selectPic{
    if (!_selectPic) {
        _selectPic = [UIImageView new];
        _selectPic.image = [UIImage imageNamed:@"selectBank"];
    }
    return _selectPic;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
