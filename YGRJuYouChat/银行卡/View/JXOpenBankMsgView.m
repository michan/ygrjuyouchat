//
//  JXOpenBankMsgView.m
//  shiku_im
//
//  Created by 韩银华 on 2020/7/11.
//  Copyright © 2020 Reese. All rights reserved.
//

#import "JXOpenBankMsgView.h"

@interface JXOpenBankMsgView ()<UITextFieldDelegate>
//提示
@property (nonatomic, strong) UILabel *tipLab;

//背景
@property (nonatomic, strong) UIView *bgView;
//持卡人
@property (nonatomic, strong) UILabel *nameLab;
//姓名
@property (nonatomic, strong) UITextField *nameTextF;

//持卡人
@property (nonatomic, strong) UILabel *idCardNumLab;
//姓名
@property (nonatomic, strong) UITextField *idCardNumTextF;

//卡号
@property (nonatomic, strong) UILabel *numLab;
//
@property (nonatomic, strong) UITextField *numTextF;
//拍照
@property (nonatomic, strong) UIButton *camerBtn;
//line
@property (nonatomic, strong) UIView *line;
//line
@property (nonatomic, strong) UIView *line1;


//下一步按钮
@property (nonatomic, strong) UIButton *nextBtn;

@end

@implementation JXOpenBankMsgView

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self initUI];
    }
    return self;
}
-(void)initUI{
    [self addSubview:self.tipLab];
    [self addSubview:self.bgView];
    [self.bgView addSubview:self.line];
    [self.bgView addSubview:self.line1];
    [self.bgView addSubview:self.nameLab];
    [self.bgView addSubview:self.nameTextF];
    [self.bgView addSubview:self.idCardNumLab];
    [self.bgView addSubview:self.idCardNumTextF];
    [self.bgView addSubview:self.numLab];
    [self.bgView addSubview:self.numTextF];
    [self.bgView addSubview:self.camerBtn];
    [self addSubview:self.nextBtn];
    
    
    [_tipLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.mas_equalTo(16);
    }];
    
    [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.top.mas_equalTo(73);
        make.height.mas_equalTo(97+58);
    }];
    

    
    [_nameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(16);
        make.top.mas_equalTo(16.5);
        make.width.mas_equalTo(60);
    }];
    
   
    
    [_nameTextF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(86);
        make.right.mas_equalTo(-35);
        make.centerY.mas_equalTo(_nameLab);
        make.height.mas_equalTo(25);
    }];
    

    [_line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_nameLab.mas_bottom).offset(16.5);
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(0.5);
    }];
    
    [_idCardNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(16);
        make.top.mas_equalTo(_line.mas_bottom).offset(16.5);
        make.width.mas_equalTo(60);
    }];
    
    [_idCardNumTextF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(86);
        make.right.mas_equalTo(-35);
        make.centerY.mas_equalTo(_idCardNumLab);
        make.height.mas_equalTo(25);
    }];
    
    
    [_line1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_idCardNumLab.mas_bottom).offset(16.5);
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(0.5);
    }];
    
    
    
    
    [_numLab mas_makeConstraints:^(MASConstraintMaker *make) {
         make.left.mas_equalTo(16);
         make.top.mas_equalTo(_line1.mas_bottom).offset(16.5);
         make.width.mas_equalTo(60);
     }];
     
     [_numTextF mas_makeConstraints:^(MASConstraintMaker *make) {
         make.left.mas_equalTo(86);
         make.right.mas_equalTo(-35);
         make.centerY.mas_equalTo(_numLab);
         make.height.mas_equalTo(25);
     }];
    
    [_camerBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.centerY.mas_equalTo(_numLab);
        make.width.mas_equalTo(16);
        make.height.mas_equalTo(14);
    }];
    
    
    _camerBtn.hidden = YES;
    
    
    [_nextBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.height.mas_equalTo(40);
        make.top.mas_equalTo(_bgView.mas_bottom).offset(60);
    }];
    
}
- (UILabel *)tipLab{
    if (!_tipLab) {
        _tipLab = [UILabel new];
        _tipLab.textColor = HEXCOLOR(0x333333);
        _tipLab.text = @"添加银行卡开户\n请绑定持卡人本人的银行卡";
        _tipLab.numberOfLines= 0;
        _tipLab.textAlignment = NSTextAlignmentCenter;
        _tipLab.font = [UIFont systemFontOfSize:13];
    }
    return _tipLab;
}
- (UIView *)bgView{
    if (!_bgView) {
        _bgView = [UIView new];
        _bgView.backgroundColor = [UIColor whiteColor];
        _bgView.layer.cornerRadius = 7;
        _bgView.layer.masksToBounds = YES;
        _bgView.layer.borderColor = HEXCOLOR(0xDEDFE1).CGColor;
        _bgView.layer.borderWidth = 0.5;
    }
    return _bgView;
}
- (UIView *)line{
    if (!_line) {
        _line = [UIView new];
        _line.backgroundColor = HEXCOLOR(0xF5F7FA);
    }
    return _line;
}
- (UIView *)line1{
    if (!_line1) {
        _line1 = [UIView new];
        _line1.backgroundColor = HEXCOLOR(0xF5F7FA);
    }
    return _line1;
}
- (UILabel *)nameLab {
    if (!_nameLab) {
        _nameLab = [UILabel new];
        _nameLab.font = [UIFont boldSystemFontOfSize:15];
        
        NSMutableAttributedString *att = [[NSMutableAttributedString alloc] initWithString:@"*持卡人"];
        [att addAttribute:NSForegroundColorAttributeName value:HEXCOLOR(0xF45E43) range:NSMakeRange(0, 1)];
        [att addAttribute:NSForegroundColorAttributeName value:HEXCOLOR(0x333333) range:NSMakeRange(1, 3)];
        _nameLab.attributedText = att;
        
    }
    return _nameLab;
}
- (UILabel *)idCardNumLab {
    if (!_idCardNumLab) {
        _idCardNumLab = [UILabel new];
        _idCardNumLab.font = [UIFont boldSystemFontOfSize:15];
        
        NSMutableAttributedString *att = [[NSMutableAttributedString alloc] initWithString:@"*身份证"];
        [att addAttribute:NSForegroundColorAttributeName value:HEXCOLOR(0xF45E43) range:NSMakeRange(0, 1)];
        [att addAttribute:NSForegroundColorAttributeName value:HEXCOLOR(0x333333) range:NSMakeRange(1, 3)];
        _idCardNumLab.attributedText = att;
        
    }
    return _idCardNumLab;
}
- (UILabel *)numLab {
    if (!_numLab) {
        _numLab = [UILabel new];
        _numLab.font = [UIFont boldSystemFontOfSize:15];
        
        NSMutableAttributedString *att = [[NSMutableAttributedString alloc] initWithString:@"*手机号"];
        [att addAttribute:NSForegroundColorAttributeName value:HEXCOLOR(0xF45E43) range:NSMakeRange(0, 1)];
        [att addAttribute:NSForegroundColorAttributeName value:HEXCOLOR(0x333333) range:NSMakeRange(1, 3)];
        _numLab.attributedText = att;
        
    }
    return _numLab;
}
- (UITextField *)nameTextF{
    if (!_nameTextF) {
        _nameTextF = [UITextField new];
        _nameTextF.placeholder = @"请输入持卡人姓名";
        _nameTextF.font = [UIFont systemFontOfSize:15];
        _nameTextF.delegate =self;
        [_nameTextF becomeFirstResponder];
    }
    return _nameTextF;
}
- (UITextField *)idCardNumTextF{
    if (!_idCardNumTextF) {
        _idCardNumTextF = [UITextField new];
        _idCardNumTextF.placeholder = @"请输入本人身份证";
        _idCardNumTextF.font = [UIFont systemFontOfSize:15];
        _idCardNumTextF.delegate =self;
    }
    return _idCardNumTextF;
}
- (UITextField *)numTextF{
    if (!_numTextF) {
        _numTextF = [UITextField new];
        _numTextF.placeholder = @"银行卡关联的手机号";
        _numTextF.font = [UIFont systemFontOfSize:15];
        _numTextF.delegate =self;
        _numTextF.keyboardType = UIKeyboardTypeNumberPad;
    }
    return _numTextF;
}
- (UIButton *)camerBtn{
    if (!_camerBtn) {
        _camerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_camerBtn setImage:[UIImage imageNamed:@"bankCamer"] forState:UIControlStateNormal];
        _camerBtn.tag = 102;
//        [_camerBtn addTarget:self action:@selector(bankCardNumBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _camerBtn;
}
- (UIButton *)nextBtn{
    if (!_nextBtn) {
        _nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _nextBtn.backgroundColor = HEXCOLOR(0x42DD5E);
        [_nextBtn setTitle:@"下一步" forState:UIControlStateNormal];
        [_nextBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _nextBtn.titleLabel.font = [UIFont boldSystemFontOfSize:17];
        _nextBtn.layer.cornerRadius = 7;
        _nextBtn.layer.masksToBounds = YES;
        [_nextBtn addTarget:self action:@selector(nextBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _nextBtn;
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self endEditing:YES];
}




-(void)nextBtnAction:(UIButton *)sender{
    if (self.delegate) {
        [self.delegate jxOpenBnakNext];
    }
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;

{  //string就是此时输入的那个字符 textField就是此时正在输入的那个输入框 返回YES就是可以改变输入框的值 NO相反
    
    if ([string isEqualToString:@"\n"])  //按会车可以改变
        
    {
        return YES;
    }
    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string]; //得到输入框的内容
    if (textField == self.nameTextF) {
        if (self.delegate) {
            [self.delegate jxBankPersonName:toBeString];
        }
        
    }else if (textField == self.idCardNumTextF){
        
        if (toBeString.length > 19) {
            toBeString = [toBeString substringToIndex:18];
        }
        if (self.delegate) {
            [self.delegate jxBankIdCardNum:toBeString];
        }
    }else{
        if (toBeString.length > 12) {
            toBeString = [toBeString substringToIndex:12];
        }
        if (self.delegate) {
            [self.delegate jxBankTelphoneNums:toBeString];
        }
    }
    
    return YES;
    
}

@end
