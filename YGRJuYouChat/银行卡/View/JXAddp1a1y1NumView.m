//
//  JXAddp1a1y1NumView.m
//  shiku_im
//
//  Created by 韩银华 on 2020/7/8.
//  Copyright © 2020 Reese. All rights reserved.
//

#import "JXAddp1a1y1NumView.h"

@interface JXAddp1a1y1NumView ()<UITextFieldDelegate>
//背景
@property (nonatomic, strong) UIView *bgView;
//持卡人
@property (nonatomic, strong) UILabel *p1a1y1NumLab;
//姓名
@property (nonatomic, strong) UITextField *p1a1y1NumTextF;
//卡号
@property (nonatomic, strong) UILabel *nameLab;
//
@property (nonatomic, strong) UITextField *nameTextF;
//line
@property (nonatomic, strong) UIView *line;
//下一步按钮
@property (nonatomic, strong) UIButton *bingdBtn;

@end

@implementation JXAddp1a1y1NumView


- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self initUI];
    }
    return self;
}
-(void)initUI{
    [self addSubview:self.bgView];
    [self.bgView addSubview:self.line];
    [self.bgView addSubview:self.nameLab];
    [self.bgView addSubview:self.nameTextF];
    [self.bgView addSubview:self.p1a1y1NumLab];
    [self.bgView addSubview:self.p1a1y1NumTextF];
    [self addSubview:self.bingdBtn];
    

    
    [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.top.mas_equalTo(12);
        make.height.mas_equalTo(97);
    }];
    
    [_line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(0);
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(0.5);
    }];
    
    [_p1a1y1NumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(16);
        make.top.mas_equalTo(16.5);
        make.width.mas_equalTo(86);
    }];
    
    [_p1a1y1NumTextF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(123.5);
        make.right.mas_equalTo(-12);
        make.centerY.mas_equalTo(_p1a1y1NumLab);
        make.height.mas_equalTo(25);
    }];
    
    
     [_nameLab mas_makeConstraints:^(MASConstraintMaker *make) {
         make.left.mas_equalTo(16);
         make.top.mas_equalTo(_line.mas_bottom).offset(16.5);
         make.width.mas_equalTo(86);
     }];
     
     [_nameTextF mas_makeConstraints:^(MASConstraintMaker *make) {
         make.left.mas_equalTo(123.5);
         make.right.mas_equalTo(-12);
         make.centerY.mas_equalTo(_nameLab);
         make.height.mas_equalTo(25);
     }];
    
 
    
    [_bingdBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.height.mas_equalTo(40);
        make.top.mas_equalTo(_bgView.mas_bottom).offset(60);
    }];
    
}

- (UIView *)bgView{
    if (!_bgView) {
        _bgView = [UIView new];
        _bgView.backgroundColor = [UIColor whiteColor];
        _bgView.layer.cornerRadius = 7;
        _bgView.layer.masksToBounds = YES;
        _bgView.layer.borderColor = HEXCOLOR(0xDEDFE1).CGColor;
        _bgView.layer.borderWidth = 0.5;
    }
    return _bgView;
}
- (UIView *)line{
    if (!_line) {
        _line = [UIView new];
        _line.backgroundColor = HEXCOLOR(0xF5F7FA);
    }
    return _line;
}
- (UILabel *)p1a1y1NumLab {
    if (!_p1a1y1NumLab) {
        _p1a1y1NumLab = [UILabel new];
        _p1a1y1NumLab.font = [UIFont boldSystemFontOfSize:15];
        
        NSMutableAttributedString *att = [[NSMutableAttributedString alloc] initWithString:@"*支付宝账号"];
        [att addAttribute:NSForegroundColorAttributeName value:HEXCOLOR(0xF45E43) range:NSMakeRange(0, 1)];
        [att addAttribute:NSForegroundColorAttributeName value:HEXCOLOR(0x333333) range:NSMakeRange(1, 5)];
        _p1a1y1NumLab.attributedText = att;
        
    }
    return _p1a1y1NumLab;
}
- (UILabel *)nameLab {
    if (!_nameLab) {
        _nameLab = [UILabel new];
        _nameLab.font = [UIFont boldSystemFontOfSize:15];
        
        NSMutableAttributedString *att = [[NSMutableAttributedString alloc] initWithString:@"*真实姓名"];
        [att addAttribute:NSForegroundColorAttributeName value:HEXCOLOR(0xF45E43) range:NSMakeRange(0, 1)];
        [att addAttribute:NSForegroundColorAttributeName value:HEXCOLOR(0x333333) range:NSMakeRange(1, 4)];
        _nameLab.attributedText = att;
        
    }
    return _nameLab;
}
- (UITextField *)p1a1y1NumTextF{
    if (!_p1a1y1NumTextF) {
        _p1a1y1NumTextF = [UITextField new];
        _p1a1y1NumTextF.placeholder = @"请输入支付宝账号";
        _p1a1y1NumTextF.font = [UIFont systemFontOfSize:15];
        _p1a1y1NumTextF.delegate =self;
    }
    return _p1a1y1NumTextF;
}
- (UITextField *)nameTextF{
    if (!_nameTextF) {
        _nameTextF = [UITextField new];
        _nameTextF.placeholder = @"请输入真实姓名";
        _nameTextF.font = [UIFont systemFontOfSize:15];
        _nameTextF.delegate =self;
    }
    return _nameTextF;
}

- (UIButton *)bingdBtn{
    if (!_bingdBtn) {
        _bingdBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _bingdBtn.backgroundColor = HEXCOLOR(0x42DD5E);
        [_bingdBtn setTitle:@"绑定" forState:UIControlStateNormal];
        [_bingdBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _bingdBtn.titleLabel.font = [UIFont boldSystemFontOfSize:17];
        _bingdBtn.layer.cornerRadius = 7;
        _bingdBtn.layer.masksToBounds = YES;
        [_bingdBtn addTarget:self action:@selector(bingdBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _bingdBtn;
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self endEditing:YES];
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;

{  //string就是此时输入的那个字符 textField就是此时正在输入的那个输入框 返回YES就是可以改变输入框的值 NO相反
    
    if ([string isEqualToString:@"\n"])  //按会车可以改变
        
    {
        return YES;
    }
    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string]; //得到输入框的内容
    if (textField == self.nameTextF) {
        if (self.delegate) {
            [self.delegate jxAlip1a1y1RealName:toBeString];
        }
        
    }else{
        if (self.delegate) {
            [self.delegate jxAlip1a1y1Num:toBeString];
        }
    }
    
    return YES;
    
}

-(void)bingdBtnAction:(UIButton *)sender{
    if (self.delegate) {
        [self.delegate jxaliBangding];
    }
}
@end
