//
//  JXOpenBankMsgView.h
//  shiku_im
//
//  Created by 韩银华 on 2020/7/11.
//  Copyright © 2020 Reese. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol JXOpenBankMsgViewDelegate <NSObject>
//下一步
-(void)jxOpenBnakNext;

//持卡人
-(void)jxBankPersonName:(NSString *)nikeName;
//身份证
-(void)jxBankIdCardNum:(NSString *)cardNum;
//手机号
-(void)jxBankTelphoneNums:(NSString *)phoneNum;


@end

@interface JXOpenBankMsgView : UIView

//代理
@property (nonatomic, assign) id<JXOpenBankMsgViewDelegate>delegate;


@end

NS_ASSUME_NONNULL_END
