
//
//  JXAddBankMsgView.m
//  shiku_im
//
//  Created by 韩银华 on 2020/7/7.
//  Copyright © 2020 Reese. All rights reserved.
//

#import "JXAddBankMsgView.h"

@interface JXAddBankMsgView ()<UITextFieldDelegate>
{
    NSString *selectBankType;
    
}
//提示
@property (nonatomic, strong) UILabel *tipLab;
//标题
@property (nonatomic, strong) UILabel *titleLab;
//背景
@property (nonatomic, strong) UIView *bgView;
//持卡人
@property (nonatomic, strong) UILabel *nameLab;
//持卡人文本框
@property (nonatomic, strong) UITextField *nameTextF;
//卡号
@property (nonatomic, strong) UILabel *cardLab;
//卡号
@property (nonatomic, strong) UITextField *cardTextF;

//卡类型
@property (nonatomic, strong) UILabel *cardTypeLab;
//选项卡
@property (nonatomic, strong) UILabel *cardTypes;

//信用卡验证码
@property (nonatomic, strong) UILabel *phoneLab;

@property (nonatomic, strong) UILabel *codeLab;
@property (nonatomic, strong) UITextField *codeTF;
@property (nonatomic, strong) UIButton *codeBtn;


//
@property (nonatomic, strong) UITextField *phoneTextF;

//line
@property (nonatomic, strong) UIView *line;
//line
@property (nonatomic, strong) UIView *line1;
//line
@property (nonatomic, strong) UIView *line2;
@property (nonatomic, strong) UIView *line3;
//用户协议
@property (nonatomic, strong) UILabel *agreeLab;
//是否同意
@property (nonatomic, strong) UIButton *agreeBtn;

//下一步按钮
@property (nonatomic, strong) UIButton *nextBtn;


//返回按钮
@property (nonatomic, strong) UIButton *backBtn;


@end

@implementation JXAddBankMsgView

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self initUI];
    }
    return self;
}
-(void)initUI{
    self.backgroundColor = HEXCOLOR(0xffffff);
    selectBankType = @"借记卡";
    [self addSubview:self.backBtn];
    [self addSubview:self.titleLab];
    [self addSubview:self.tipLab];
    [self addSubview:self.bgView];
    [self.bgView addSubview:self.line];
    [self.bgView addSubview:self.line1];
    [self.bgView addSubview:self.line2];
//    [self.bgView addSubview:self.line3];
    [self.bgView addSubview:self.nameLab];
    [self.bgView addSubview:self.nameTextF];
    [self.bgView addSubview:self.cardLab];
    [self.bgView addSubview:self.cardTextF];
    [self.bgView addSubview:self.cardTypeLab];
    [self.bgView addSubview:self.cardTypes];
    [self.bgView addSubview:self.phoneLab];
    [self.bgView addSubview:self.phoneTextF];
    
//    [self.bgView addSubview:self.codeLab];
//    [self.bgView addSubview:self.codeTF];
//    [self.bgView addSubview:self.codeBtn];

    [self addSubview:self.agreeLab];
    [self addSubview:self.agreeBtn];
    
    [self addSubview:self.nextBtn];
    
    
    [self.backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.width.height.mas_equalTo(30);
        make.top.mas_equalTo(ManMan_SCREEN_TOP - 40);
    }];
    
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.mas_equalTo(116);
    }];
    [_tipLab mas_makeConstraints:^(MASConstraintMaker *make) {
         make.centerX.mas_equalTo(0);
         make.top.mas_equalTo(self.titleLab.mas_bottom).offset(20);
     }];
//
     [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
         make.left.mas_equalTo(0);
         make.right.mas_equalTo(-0);
         make.top.mas_equalTo(218 );
         make.height.mas_equalTo(222);
     }];
     

     
     [_nameLab mas_makeConstraints:^(MASConstraintMaker *make) {
         make.left.mas_equalTo(16);
         make.top.mas_equalTo(16.5);
         make.width.mas_equalTo(75);
         make.height.mas_equalTo(22);
     }];
     
    
     
     [_nameTextF mas_makeConstraints:^(MASConstraintMaker *make) {
         make.left.mas_equalTo(106);
         make.right.mas_equalTo(-35);
         make.centerY.mas_equalTo(_nameLab);
         make.height.mas_equalTo(25);
     }];
     

     [_line mas_makeConstraints:^(MASConstraintMaker *make) {
         make.top.mas_equalTo(_nameLab.mas_bottom).offset(16.5);
         make.left.right.mas_equalTo(0);
         make.height.mas_equalTo(0.5);
     }];
     
     [_cardLab mas_makeConstraints:^(MASConstraintMaker *make) {
         make.left.mas_equalTo(16);
         make.top.mas_equalTo(_line.mas_bottom).offset(16.5);
         make.width.mas_equalTo(75);
         make.height.mas_equalTo(22);
     }];
     
     [_cardTextF mas_makeConstraints:^(MASConstraintMaker *make) {
         make.left.mas_equalTo(106);
         make.right.mas_equalTo(-35);
         make.centerY.mas_equalTo(_cardLab);
         make.height.mas_equalTo(25);
     }];
     
     
     [_line1 mas_makeConstraints:^(MASConstraintMaker *make) {
         make.top.mas_equalTo(_cardLab.mas_bottom).offset(16.5);
         make.left.right.mas_equalTo(0);
         make.height.mas_equalTo(0.5);
     }];
     
     
     
     
     [_cardTypeLab mas_makeConstraints:^(MASConstraintMaker *make) {
          make.left.mas_equalTo(16);
          make.top.mas_equalTo(_line1.mas_bottom).offset(16.5);
          make.width.mas_equalTo(75);
         make.height.mas_equalTo(22);
      }];
      
      [_cardTypes mas_makeConstraints:^(MASConstraintMaker *make) {
          make.left.mas_equalTo(106);
          make.right.mas_equalTo(-35);
          make.centerY.mas_equalTo(_cardTypeLab);
          make.height.mas_equalTo(25);
      }];

    
    [_line2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_cardTypeLab.mas_bottom).offset(16.5);
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(0.5);
    }];
    
    [_phoneLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(16);
        make.top.mas_equalTo(_line2.mas_bottom).offset(16.5);
        make.width.mas_equalTo(75);
        make.height.mas_equalTo(22);
    }];
    
    [_phoneTextF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(106);
        make.right.mas_equalTo(-35);
        make.centerY.mas_equalTo(_phoneLab);
        make.height.mas_equalTo(25);
    }];
//    [_line3 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(_phoneLab.mas_bottom).offset(16.5);
//        make.left.right.mas_equalTo(0);
//        make.height.mas_equalTo(0.5);
//    }];
//    [self.codeLab mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(16);
//        make.top.mas_equalTo(_line3.mas_bottom).offset(16.5);
//        make.width.mas_equalTo(75);
//        make.height.mas_equalTo(22);
//    }];
//    [self.codeTF mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(106);
//        make.right.mas_equalTo(-35);
//        make.centerY.mas_equalTo(_codeLab);
//        make.height.mas_equalTo(25);
//    }];
//    [self.codeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.mas_equalTo(-15);
//        make.centerY.mas_equalTo(_codeLab);
//        make.width.mas_equalTo(100);
//        make.height.mas_equalTo(25);
//    }];
    
     
    [_agreeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.mas_equalTo(_bgView.mas_bottom).offset(20);
    }];
    @weakify(self);
    [_agreeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(weak_self.agreeLab.mas_left).offset(-8);
        make.centerY.mas_equalTo(weak_self.agreeLab);
        make.width.height.mas_equalTo(25);
    }];
    
    
     
     [_nextBtn mas_makeConstraints:^(MASConstraintMaker *make) {
         make.left.mas_equalTo(112);
         make.right.mas_equalTo(-112);
         make.height.mas_equalTo(40);
         make.top.mas_equalTo(_bgView.mas_bottom).offset(70);
     }];
    
    self.agreeBtn.selected = YES;
    
}
- (UILabel *)titleLab{
    if (!_titleLab) {
        _titleLab = [UILabel new];
        _titleLab.textColor = HEXCOLOR(0x303030);
        _titleLab.text = @"添加银行卡";
        _titleLab.textAlignment = NSTextAlignmentCenter;
        _titleLab.font = [UIFont systemFontOfSize:20];
    }
    return _titleLab;
}
- (UILabel *)tipLab{
    if (!_tipLab) {
        _tipLab = [UILabel new];
        _tipLab.textColor = HEXCOLOR(0x303030);
        _tipLab.text = @"请绑定持卡人本人的银行卡";
        _tipLab.textAlignment = NSTextAlignmentCenter;
        _tipLab.font = [UIFont systemFontOfSize:13];
    }
    return _tipLab;
}
- (UIView *)bgView{
    if (!_bgView) {
        _bgView = [UIView new];
        _bgView.backgroundColor = [UIColor whiteColor];
        _bgView.layer.cornerRadius = 7;
        _bgView.layer.masksToBounds = YES;
        _bgView.layer.borderColor = HEXCOLOR(0xDEDFE1).CGColor;
        _bgView.layer.borderWidth = 0.5;
    }
    return _bgView;
}
- (UIView *)line{
    if (!_line) {
        _line = [UIView new];
        _line.backgroundColor = HEXCOLOR(0xF5F7FA);
    }
    return _line;
}
- (UIView *)line3{
    if (!_line3) {
        _line3 = [UIView new];
        _line3.backgroundColor = HEXCOLOR(0xF5F7FA);
    }
    return _line3;
}
- (UIView *)line1{
    if (!_line1) {
        _line1 = [UIView new];
        _line1.backgroundColor = HEXCOLOR(0xF5F7FA);
    }
    return _line1;
}
- (UIView *)line2{
    if (!_line2) {
        _line2 = [UIView new];
        _line2.backgroundColor = HEXCOLOR(0xF5F7FA);
    }
    return _line2;
}
- (UILabel *)nameLab {
    if (!_nameLab) {
        _nameLab = [UILabel new];
        _nameLab.font = [UIFont boldSystemFontOfSize:15];
        
        NSMutableAttributedString *att = [[NSMutableAttributedString alloc] initWithString:@"持卡人"];
//        [att addAttribute:NSForegroundColorAttributeName value:HEXCOLOR(0xF35D42) range:NSMakeRange(0, 1)];
//        [att addAttribute:NSForegroundColorAttributeName value:HEXCOLOR(0x333333) range:NSMakeRange(0, 2)];
        _nameLab.attributedText = att;
        
    }
    return _nameLab;
}

- (UILabel *)cardLab {
    if (!_cardLab) {
        _cardLab = [UILabel new];
        _cardLab.font = [UIFont boldSystemFontOfSize:15];
        
        NSMutableAttributedString *att = [[NSMutableAttributedString alloc] initWithString:@"卡号"];
//        [att addAttribute:NSForegroundColorAttributeName value:HEXCOLOR(0xF35D42) range:NSMakeRange(0, 1)];
        [att addAttribute:NSForegroundColorAttributeName value:HEXCOLOR(0x333333) range:NSMakeRange(0, 2)];
        _cardLab.attributedText = att;
        
    }
    return _cardLab;
}
- (UILabel *)cardTypeLab {
    if (!_cardTypeLab) {
        _cardTypeLab = [UILabel new];
        _cardTypeLab.font = [UIFont boldSystemFontOfSize:15];
        
        NSMutableAttributedString *att = [[NSMutableAttributedString alloc] initWithString:@"卡类型"];
//        [att addAttribute:NSForegroundColorAttributeName value:HEXCOLOR(0xF35D42) range:NSMakeRange(0, 1)];
        [att addAttribute:NSForegroundColorAttributeName value:HEXCOLOR(0x333333) range:NSMakeRange(0, 2)];
        _cardTypeLab.attributedText = att;
        
    }
    return _cardTypeLab;
}
- (UILabel *)phoneLab {
    if (!_phoneLab) {
        _phoneLab = [UILabel new];
        _phoneLab.font = [UIFont boldSystemFontOfSize:15];
        
        NSMutableAttributedString *att = [[NSMutableAttributedString alloc] initWithString:@"+86"];
//        [att addAttribute:NSForegroundColorAttributeName value:HEXCOLOR(0xF35D42) range:NSMakeRange(0, 1)];
        [att addAttribute:NSForegroundColorAttributeName value:HEXCOLOR(0x333333) range:NSMakeRange(0, 2)];
        _phoneLab.attributedText = att;
        
    }
    return _phoneLab;
}

-(UITextField *)codeTF{
    if (!_codeTF) {
        _codeTF = [UITextField new];
        _codeTF.placeholder = @"请输入验证码";
        _codeTF.font = [UIFont systemFontOfSize:15];
        _codeTF.delegate =self;
        
    }
    return _codeTF;
}
-(UILabel *)codeLab{
    if (!_codeLab) {
        _codeLab = UILabel.new;
        _codeLab.font = [UIFont boldSystemFontOfSize:15];
        _codeLab.textColor = HEXCOLOR(0x333333);
        _codeLab.text = @"验证码";
    }
    return _codeLab;
}
-(void)sendCode{
    
}
- (UIButton *)codeBtn{
    if (!_codeBtn) {
        _codeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_codeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        [_codeBtn setTitleColor:HEXCOLOR(0x05D168) forState:UIControlStateNormal];
        _codeBtn.titleLabel.font = [UIFont boldSystemFontOfSize:12];
        _codeBtn.layer.cornerRadius = 7;
        _codeBtn.layer.masksToBounds = YES;
        [_codeBtn addTarget:self action:@selector(sendCode) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _codeBtn;
}
-(void)backClick{
    if (self.delegate) {
        [self.delegate backClick];
    }
}
- (UIButton *)backBtn{
    if (!_backBtn) {
        _backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_backBtn setImage:[UIImage imageNamed:@"photo_title_back_black"] forState:UIControlStateNormal];
        [_backBtn addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _backBtn;
}
- (UILabel *)agreeLab {
    if (!_agreeLab) {
        _agreeLab = [UILabel new];
        _agreeLab.font = [UIFont systemFontOfSize:13];
        
        NSMutableAttributedString *att = [[NSMutableAttributedString alloc] initWithString:@"同意《用户协议》"];
        [att addAttribute:NSForegroundColorAttributeName value:HEXCOLOR(0x333333) range:NSMakeRange(0, 2)];
        [att addAttribute:NSForegroundColorAttributeName value:HEXCOLOR(0x4091F7) range:NSMakeRange(2, 6)];

        _agreeLab.attributedText = att;
        _agreeLab.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(agreeLabAction)];
        [_agreeLab addGestureRecognizer:tap];
        
        
    }
    return _agreeLab;
}

- (UITextField *)nameTextF{
    if (!_nameTextF) {
        _nameTextF = [UITextField new];
        _nameTextF.placeholder = @"请输入持卡人姓名";
        _nameTextF.font = [UIFont systemFontOfSize:15];
        _nameTextF.delegate =self;
        [_nameTextF becomeFirstResponder];

    }
    return _nameTextF;
}
- (UITextField *)cardTextF{
    if (!_cardTextF) {
        _cardTextF = [UITextField new];
        _cardTextF.placeholder = @"请输入本人银行卡号";
        _cardTextF.font = [UIFont systemFontOfSize:15];
        _cardTextF.delegate =self;
//        _cardTextF.keyboardType =  UIKeyboardTypeNumberPad;


    }
    return _cardTextF;
}
- (UITextField *)phoneTextF{
    if (!_phoneTextF) {
        _phoneTextF = [UITextField new];
        _phoneTextF.placeholder = @"请输入手机号";
        _phoneTextF.font = [UIFont systemFontOfSize:15];
        _phoneTextF.delegate =self;
        _phoneTextF.keyboardType = UIKeyboardTypeNumberPad;

    }
    return _phoneTextF;
}
- (UILabel *)cardTypes{
    if (!_cardTypes) {
        _cardTypes = [UILabel new];
        _cardTypes.userInteractionEnabled = YES;
        _cardTypes.text = @"借记卡";
        _cardTypes.textColor = HEXCOLOR(0x333333);
        _cardTypes.font = [UIFont systemFontOfSize:15];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cardTypesAction)];
        [_cardTypes addGestureRecognizer:tap];
    }
    return _cardTypes;
}
- (UIButton *)nextBtn{
    if (!_nextBtn) {
        _nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _nextBtn.backgroundColor = HEXCOLOR(0x05D168);
        [_nextBtn setTitle:@"完成" forState:UIControlStateNormal];//下一步
        [_nextBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _nextBtn.titleLabel.font = [UIFont boldSystemFontOfSize:17];
        _nextBtn.layer.cornerRadius = 7;
        _nextBtn.layer.masksToBounds = YES;
        [_nextBtn addTarget:self action:@selector(nextBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _nextBtn;
}
- (UIButton *)agreeBtn{
    if (!_agreeBtn) {
        _agreeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_agreeBtn setImage:[UIImage imageNamed:@"selected_fause"] forState:UIControlStateNormal];
        [_agreeBtn setImage:[UIImage imageNamed:@"selected_true"] forState:UIControlStateSelected];
      
        [_agreeBtn addTarget:self action:@selector(agreeBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _agreeBtn;
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self endEditing:YES];
}

-(void)agreeBtnAction:(UIButton *)sender{
    sender.selected = !sender.selected;
}
- (void)setAddBankTypeStr:(NSString *)addBankTypeStr{
    _cardTypes.text = addBankTypeStr;
}

-(void)nextBtnAction:(UIButton *)sender{
    
    if (!self.agreeBtn.isSelected) {
        [JY_MyTools showTipView:@"请同意用户协议"];
        return;
    }
    
    if (self.delegate) {
        [self.delegate nextAddBank];
    }
}
-(void)agreeLabAction
{
    if (self.delegate) {
        [self.delegate jxAggresBankMsg];
    }
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;

{  //string就是此时输入的那个字符 textField就是此时正在输入的那个输入框 返回YES就是可以改变输入框的值 NO相反
    
    if ([string isEqualToString:@"\n"])  //按会车可以改变
        
    {
        return YES;
    }
    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string]; //得到输入框的内容
    if (textField == self.cardTextF) {
        if (self.delegate) {
            [self.delegate jxBankCardNum:toBeString];
        }
    
    }else if(textField == self.nameTextF){

        if (self.delegate) {
            [self.delegate jxBankName:toBeString];
        }
    }else if (textField == self.phoneTextF){
        if (self.delegate) {
            [self.delegate jxBankPhone:toBeString];
        }
    }
    
    return YES;
    
}
-(void)cardTypesAction{
    
    if (self.delegate) {
        [self.delegate jxBankSelectCardType:selectBankType];
    }
    
}
@end
