//
//  JXDrawNumMoneyCell.m
//  shiku_im
//
//  Created by 韩银华 on 2020/7/7.
//  Copyright © 2020 Reese. All rights reserved.
//

#import "JXDrawNumMoneyCell.h"

@interface JXDrawNumMoneyCell ()<UITextFieldDelegate>
//背景
@property (nonatomic, strong) UIView *bgView;
//提现方式
@property (nonatomic, strong) UILabel *tipLab;
//符号
@property (nonatomic, strong) UILabel *moenyLab;
//文本框
@property (nonatomic, strong) UITextField *moneyTextF;
//line
@property (nonatomic, strong) UIView *line;
//零用余额
@property (nonatomic, strong) UILabel *otherMoneyLab;
//全部提现
@property (nonatomic, strong) UIButton *allMoneyBtn;
//手续费说明
@property (nonatomic, strong) UILabel *declareLab;
//用户协议
@property (nonatomic, strong) UILabel *agreeLab;
//是否同意
@property (nonatomic, strong) UIButton *agreeBtn;
@end

@implementation JXDrawNumMoneyCell

+(instancetype)cellWithTableView:(UITableView *)tableView{
    JXDrawNumMoneyCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JXDrawNumMoneyCell"];
    if (!cell) {
        cell = [[JXDrawNumMoneyCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"JXDrawNumMoneyCell"];
    }
    return cell;
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.contentView.backgroundColor = HEXCOLOR(0xFfFfFf);
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self initUI];
    }
    return self;
}
-(void)initUI{
    [self.contentView addSubview:self.bgView];
    [self.bgView addSubview:self.tipLab];
    [self.bgView addSubview:self.moenyLab];
    [self.bgView addSubview:self.moneyTextF];
    [self.bgView addSubview:self.otherMoneyLab];
    [self.bgView addSubview:self.line];
    [self.bgView addSubview:self.allMoneyBtn];
//    [self.contentView addSubview:self.agreeBtn];
//    [self.contentView addSubview:self.agreeLab];
    [self.contentView addSubview:self.queckBtn];
    [self.contentView addSubview:self.declareLab];
    
    [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.top.mas_equalTo(0);
        make.height.mas_equalTo(160);
    }];
    
    [_tipLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(17);
    }];
    
    [_moenyLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(_tipLab.mas_bottom).offset(19.5);
        make.width.mas_equalTo(48);
    }];
    
    [_moneyTextF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.moenyLab.mas_right).offset(2);
        make.right.mas_equalTo(-12);
        make.centerY.mas_equalTo(_moenyLab);
        make.height.mas_equalTo(65);
    }];
    
    
    [_line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(13.5);
        make.top.mas_equalTo(_moenyLab.mas_bottom).offset(14);
        make.height.mas_equalTo(0.5);
    }];
    [_otherMoneyLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(_line.mas_bottom).offset(13);
    }];
    
    
    [_allMoneyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.centerY.mas_equalTo(_otherMoneyLab);
        make.width.mas_equalTo(64);
        make.height.mas_equalTo(15);
    }];
    
    @weakify(self);
    
//    [_agreeLab mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerX.mas_equalTo(0);
//        make.top.mas_equalTo(_bgView.mas_bottom).offset(20);
//    }];
//    
//    [_agreeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.mas_equalTo(weak_self.agreeLab.mas_left).offset(-8);
//        make.centerY.mas_equalTo(weak_self.agreeLab);
//        make.width.height.mas_equalTo(25);
//    }];
    
    
    self.agreeBtn.selected = YES;
    
    
    
    [_queckBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.top.mas_equalTo(_bgView.mas_bottom).offset(64);
        make.height.mas_equalTo(44);
    }];
    
    
    [_declareLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_queckBtn.mas_bottom).offset(18);
        make.left.mas_equalTo(_queckBtn.mas_left);
        make.right.mas_equalTo(_queckBtn.mas_right);
    }];
    
}

- (UIView *)bgView{
    if (!_bgView) {
        _bgView = [UIView new];
        _bgView.backgroundColor = [UIColor whiteColor];
//        _bgView.layer.borderColor = HEXCOLOR(0xDEDFE1).CGColor;
//        _bgView.layer.borderWidth = 0.5;
//        _bgView.layer.cornerRadius = 7;
//        _bgView.layer.masksToBounds = YES;
    }
    return _bgView;
}
- (UILabel *)tipLab{
    if (!_tipLab) {
        _tipLab = [UILabel new];
        _tipLab.textColor = HEXCOLOR(0x151515);
        _tipLab.text = @"提现金额";
        _tipLab.font = [UIFont boldSystemFontOfSize:15];
    }
    return _tipLab;
}
- (UILabel *)moenyLab{
    if (!_moenyLab) {
        _moenyLab = [UILabel new];
        _moenyLab.textColor = HEXCOLOR(0x151515);
        _moenyLab.text = @"￥";
        _moenyLab.font = [UIFont boldSystemFontOfSize:40];
    }
    return _moenyLab;
}
- (UITextField *)moneyTextF{
    if (!_moneyTextF) {
        _moneyTextF = [UITextField new];
        _moneyTextF.placeholder = @"最小提现金额50元";
        _moneyTextF.font = [UIFont boldSystemFontOfSize:30];
        _moneyTextF.delegate = self;
        _moneyTextF.keyboardType = UIKeyboardTypeNumberPad;
    }
    return _moneyTextF;
}
-(UIView *)line{
    if (!_line) {
        _line = [UIView new];
        _line.backgroundColor = HEXCOLOR(0xF4F4F4);
    }
    return _line;
}
- (UILabel *)otherMoneyLab{
    if (!_otherMoneyLab) {
        _otherMoneyLab = [UILabel new];
        _otherMoneyLab.textColor = HEXCOLOR(0x828282);
        NSString *otherMoneyStr = [NSString stringWithFormat:@"零钱余额：￥%.2f",g_App.myMoney];
        _otherMoneyLab.text = otherMoneyStr;
        
        _otherMoneyLab.font = [UIFont systemFontOfSize:13];
    }
    return _otherMoneyLab;
}
- (UIButton *)allMoneyBtn{
    if (!_allMoneyBtn) {
        _allMoneyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_allMoneyBtn setTitle:@"全部提现" forState:UIControlStateNormal];
        [_allMoneyBtn setTitleColor:HEXCOLOR(0x426085) forState:UIControlStateNormal];
        _allMoneyBtn.titleLabel.font = [UIFont systemFontOfSize:13];
        [_allMoneyBtn addTarget:self action:@selector(allMoneyBtnAction) forControlEvents:UIControlEventTouchUpInside];

    }
    return _allMoneyBtn;
}
- (UIButton *)queckBtn{
    if (!_queckBtn) {
        _queckBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _queckBtn.backgroundColor = HEXCOLOR(0x05D168);
        [_queckBtn setTitle:@"确认提现" forState:UIControlStateNormal];
        [_queckBtn setTitleColor:HEXCOLOR(0xFFFFFF) forState:UIControlStateNormal];
        _queckBtn.titleLabel.font = [UIFont boldSystemFontOfSize:17];
        _queckBtn.layer.cornerRadius = 7;
        _queckBtn.layer.masksToBounds = YES;
        [_queckBtn addTarget:self action:@selector(queckBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _queckBtn;
}
- (UILabel *)declareLab{
    if (!_declareLab) {
        _declareLab = [UILabel new];
        _declareLab.textColor = HEXCOLOR(0x05D168);
        _declareLab.text = @"1、提现手续费：单笔1元+0.6%手续费\n2、提现时间：晚23:00-8:00为银行清算时间，银行不进行打款，早8:00统一到账，其他时间正常（若通道达到当日交易上限，顺延至早8:00统一到账）\n3、风控要求：夜间提现将增加扫脸验证，以确保资金安全\n4、其他说明：特殊原因未到账，请联系在线客服解决，一般两小时内处理";
        _declareLab.font = [UIFont systemFontOfSize:11];
        _declareLab.numberOfLines = 0;
    }
    return _declareLab;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
      if ([string isEqualToString:@"\n"])  //按会车可以改变
          
      {
          return YES;
      }
      NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string]; //得到输入框的内容
    
    if ([toBeString floatValue] > g_App.myMoney) {
        return NO;
    }
    
    if (self.delegate) {
        [self.delegate jxDrawNumMoney:toBeString];
    }
    return YES;
}
-(void)queckBtnAction:(UIButton *)sender{
    if (!self.agreeBtn.isSelected) {
        [JY_MyTools showTipView:@"请勾选协议"];
        return;
    }
    /// 一秒钟只需点击一次提现
    sender.userInteractionEnabled = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        sender.userInteractionEnabled = YES;
    });
    if (self.delegate) {
        [self.delegate jxqueckDwarMoney];
    }
}
-(void)allMoneyBtnAction{
    NSString *otherMoneyStr = [NSString stringWithFormat:@"%.2f",g_App.myMoney];
    _moneyTextF.text = otherMoneyStr;
    if (self.delegate) {
        [self.delegate jxDrawNumMoney:otherMoneyStr];
    }
}
-(void)agreeBtnAction:(UIButton *)sender{
    sender.selected = !sender.selected;
}
- (UIButton *)agreeBtn{
    if (!_agreeBtn) {
        _agreeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_agreeBtn setImage:[UIImage imageNamed:@"selected_fause"] forState:UIControlStateNormal];
        [_agreeBtn setImage:[UIImage imageNamed:@"selected_true"] forState:UIControlStateSelected];
      
        [_agreeBtn addTarget:self action:@selector(agreeBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _agreeBtn;
}
- (UILabel *)agreeLab {
    if (!_agreeLab) {
        _agreeLab = [UILabel new];
        _agreeLab.font = [UIFont systemFontOfSize:13];
        
        NSMutableAttributedString *att = [[NSMutableAttributedString alloc] initWithString:@"同意《用户协议》"];
        [att addAttribute:NSForegroundColorAttributeName value:HEXCOLOR(0x333333) range:NSMakeRange(0, 2)];
        [att addAttribute:NSForegroundColorAttributeName value:HEXCOLOR(0x4091F7) range:NSMakeRange(2, 6)];

        _agreeLab.attributedText = att;
        _agreeLab.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(agreeLabAction)];
        [_agreeLab addGestureRecognizer:tap];
        
        
    }
    return _agreeLab;
}
-(void)agreeLabAction{
    if (self.delegate) {
        [self.delegate jxBankAgreemensts];
    }
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.contentView endEditing:YES];
}
@end
