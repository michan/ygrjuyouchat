//
//  JXAddp1a1y1NumView.h
//  shiku_im
//
//  Created by 韩银华 on 2020/7/8.
//  Copyright © 2020 Reese. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN


@protocol JXAddp1a1y1NumViewDelegate <NSObject>

-(void)jxAlip1a1y1Num:(NSString *)p1a1y1Num;
-(void)jxAlip1a1y1RealName:(NSString *)realName;
//绑定
- (void)jxaliBangding;

@end

@interface JXAddp1a1y1NumView : UIView

//搭理
@property (nonatomic, assign) id<JXAddp1a1y1NumViewDelegate>delegate;


@end

NS_ASSUME_NONNULL_END
