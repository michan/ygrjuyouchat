//
//  JXp1a1y1BankListView.h
//  shiku_im
//
//  Created by 韩银华 on 2020/7/16.
//  Copyright © 2020 Reese. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JXBankListModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol JXp1a1y1BankListViewDelegate <NSObject>
//添加银行卡
-(void)jxAddNewBank;
//选择了银行卡
-(void)jxSelectBankModel:(JXBankListModel *)bankModel;

@end

@interface JXp1a1y1BankListView : UIView

//代理
@property (nonatomic, assign) id <JXp1a1y1BankListViewDelegate>delegate;


-(void)show;


@end

NS_ASSUME_NONNULL_END
