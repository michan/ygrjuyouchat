//
//  JXp1a1y1SelectBankCell.h
//  shiku_im
//
//  Created by 韩银华 on 2020/7/16.
//  Copyright © 2020 Reese. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JXBankListModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface JXp1a1y1SelectBankCell : UITableViewCell

//数据源
@property (nonatomic, strong) JXBankListModel *bankModel;


+(instancetype)cellWithTableView:(UITableView *)tableView;


@end

NS_ASSUME_NONNULL_END
