//
//  JXBankListCell.m
//  shiku_im
//
//  Created by 韩银华 on 2020/7/7.
//  Copyright © 2020 Reese. All rights reserved.
//

#import "JXBankListCell.h"

@interface JXBankListCell ()
//背景图片
@property (nonatomic, strong) UIImageView *bgPic;
//logo
@property (nonatomic, strong) UIImageView *logoPic;
//银行卡名字
@property (nonatomic, strong) UILabel *bankNameLab;

//title
@property (nonatomic, strong) UILabel *bankTitleLab;
//类型
@property (nonatomic, strong) UILabel *typeLab;
//卡号
@property (nonatomic, strong) UILabel *bankNumLab;

//
@property (nonatomic, strong) UIView *moreView;

// more
@property (nonatomic, strong) UIButton *deleteBankBtn;

@end

@implementation JXBankListCell

+(instancetype)cellWithTableView:(UITableView *)tableView{
    JXBankListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JXBankListCell"];
    if (!cell) {
        cell = [[JXBankListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"JXBankListCell"];
    }
    return cell;
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.backgroundColor = HEXCOLOR(0xF2F2F2);

        [self initUI];
    }
    return self;
}
-(void)initUI{
    [self.contentView addSubview:self.bgPic];
    [self.bgPic addSubview:self.logoPic];
    [self.bgPic addSubview:self.bankNameLab];
//    [self.bgPic addSubview:self.bankTitleLab];
    [self.bgPic addSubview:self.typeLab];
    [self.bgPic addSubview:self.bankNumLab];
    [self.bgPic addSubview:self.moreView];
    [self.moreView addSubview:self.deleteBankBtn];
    [_bgPic mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.top.bottom.mas_equalTo(0);
    }];
    
    [_logoPic mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(20);
        make.left.mas_equalTo(12);
        make.width.mas_equalTo(48);
        make.height.mas_equalTo(29.5);
    }];
    @weakify(self);
    [_bankNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(79);
        make.top.mas_equalTo(19);
    }];
    
    [_typeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(79);
        make.top.mas_equalTo(weak_self.bankNameLab.mas_bottom).offset(8);
    }];
    
    
    [_bankNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(79);
        make.top.mas_equalTo(weak_self.typeLab.mas_bottom).offset(11);
    }];
    
    [_moreView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.top.mas_equalTo(0);
        make.width.mas_equalTo(65);
        make.height.mas_equalTo(48);
    }];
    
    [_deleteBankBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-13.5);
        make.top.mas_equalTo(25);
        make.width.mas_equalTo(21);
        make.height.mas_equalTo(4.5);
    }];
    
}
- (void)setSelectBankIndex:(NSInteger)selectBankIndex{
    _selectBankIndex = selectBankIndex;
    
}
- (void)setBankModel:(JXBankListModel *)bankModel{
    _bankModel = bankModel;
    
    _bankNameLab.text = bankModel.bankName;
    
    NSString *cardTypeStr;
//    if ([bankModel.cardType isEqual:@"1"]) {
        cardTypeStr = @"借记卡";
//    }
    _typeLab.text = cardTypeStr;
    NSString *cardNums = bankModel.cardNo;
    
    cardNums = [cardNums stringByReplacingCharactersInRange:NSMakeRange(4, cardNums.length -8) withString:@"***********"];
    
    _bankNumLab.text = cardNums;

}

- (void)setBankDict:(NSDictionary *)bankDict{

    
}
- (UIView *)moreView{
    if (!_moreView) {
        _moreView = [UIView new];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(deleteBankBtnAction)];
        [_moreView addGestureRecognizer:tap];
    }
    return _moreView;
}
- (UIButton *)deleteBankBtn{
    if (!_deleteBankBtn) {
        _deleteBankBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_deleteBankBtn setImage:[UIImage imageNamed:@"bankMores"] forState:UIControlStateNormal];
        [_deleteBankBtn addTarget:self action:@selector(deleteBankBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _deleteBankBtn;
}
- (UIImageView *)bgPic{
    if (!_bgPic) {
        _bgPic = [UIImageView new];
        _bgPic.userInteractionEnabled = YES;
        _bgPic.image = [UIImage imageNamed:@"bankBGPics"];
    }
    return _bgPic;
}
- (UIImageView *)logoPic{
    if (!_logoPic) {
        _logoPic = [UIImageView new];
        _logoPic.image = [UIImage imageNamed:@"bankTitlePic"];
    }
    return _logoPic;
}
- (UILabel *)bankNameLab{
    if (!_bankNameLab) {
        _bankNameLab = [UILabel new];
        _bankNameLab.textColor = HEXCOLOR(0xFFFFFF);
        _bankNameLab.font = [UIFont boldSystemFontOfSize:17];
    }
    return _bankNameLab;
}
- (UILabel *)typeLab{
    if (!_typeLab) {
        _typeLab = [UILabel new];
        _typeLab.textColor = HEXCOLOR(0xFFFFFF);
        _typeLab.font = [UIFont systemFontOfSize:14];
    }
    return _typeLab;
}

- (UILabel *)bankNumLab{
    if (!_bankNumLab) {
        _bankNumLab = [UILabel new];
        _bankNumLab.textColor = HEXCOLOR(0xFFFFFF);
        _bankNumLab.font = [UIFont boldSystemFontOfSize:16];
    }
    return _bankNumLab;
}
-(void)deleteBankBtnAction{
    if (self.delegate) {
        [self.delegate jxUnbingBank:_bankModel index:_selectBankIndex];
    }
    
}
@end
