//
//  JXBankPwdView.m
//  shiku_im
//
//  Created by 韩银华 on 2020/7/7.
//  Copyright © 2020 Reese. All rights reserved.
//
#define bg_left  ([pwdTypeStr isEqual:@"1"] ? 50:12)
#import "JXBankPwdView.h"

@interface JXBankPwdView ()
{
    NSString *pwdTypeStr;
    
}
//背景
@property (nonatomic, strong) UIView *bgView;
//title
@property (nonatomic, strong) UILabel *tipLab;
//detail
@property (nonatomic, strong) UILabel *detailLab;
//line
@property (nonatomic, strong) UIView *line;
//pwd
@property (nonatomic, strong) UILabel *pwdLab;
//code
@property (nonatomic, strong) JHVerificationCodeView *codeView;
//关闭
@property (nonatomic, strong) UIButton *closeBtn;



@end

@implementation JXBankPwdView
- (instancetype)initWithFrame:(CGRect)frame pwdType:(NSString *)pwdtype{
    pwdTypeStr = pwdtype;
    if (self = [super initWithFrame:frame]) {
          self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.35];
          [self initUI];
      }
      return self;
}
-(void)initUI{
    [self addSubview:self.bgView];
    [self.bgView addSubview:self.tipLab];
    [self.bgView addSubview:self.detailLab];
    [self.bgView addSubview:self.line];
    [self.bgView addSubview:self.pwdLab];
    [self.bgView addSubview:self.codeView];
    [self.bgView addSubview:self.closeBtn];
    [_tipLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.mas_equalTo(20);
    }];
    
    [_detailLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(_tipLab);
            [make.top.mas_equalTo(_tipLab.mas_bottom)setOffset:25];
    }];
    
  
    [_closeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-12);
        make.top.mas_equalTo(10);
//        make.centerY.mas_equalTo(_tipLab);
        make.width.height.mas_equalTo(15);
    }];
    
    [_line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(_tipLab.mas_bottom).offset(16);
        make.height.mas_equalTo(0.5);
    }];
    
    [_pwdLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.mas_equalTo(_line.mas_bottom).offset(17);
    }];
    
    
    if ([pwdTypeStr isEqual:@"1"]) {
        self.detailLab.hidden = NO;
        _tipLab.text = @"支付验证";
//        @"请输入验证码";
        self.detailLab.text = @"请输入验证码";
        _pwdLab.text = @"";
   
      
    }else{
        _pwdLab.text = @"请输入支付密码";
        self.detailLab.hidden = YES;
    }
    
    
}
- (void)show{
    UIWindow *window = [UIApplication sharedApplication].delegate.window;
    [window addSubview:self];
    
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeBtnAction)];
//    [self addGestureRecognizer:tap];
    
    [self initUI];
}
- (UIView *)bgView{
    if (!_bgView) {
    
        _bgView = [[UIView alloc] initWithFrame:CGRectMake(bg_left, 250, ManMan_SCREEN_WIDTH-bg_left*2, 200)];
        _bgView.backgroundColor = [UIColor whiteColor];
        _bgView.layer.cornerRadius = 7;
        _bgView.layer.masksToBounds = YES;
        _bgView.layer.borderColor = HEXCOLOR(0xDEDFE1).CGColor;
        _bgView.layer.borderWidth = 0.5;
    }
    return _bgView;
    
}
- (UILabel *)tipLab{
    if (!_tipLab) {
        _tipLab = [UILabel new];
        _tipLab.textColor = HEXCOLOR(0x333333);
        _tipLab.font = [UIFont boldSystemFontOfSize:17];
        _tipLab.text = @"请输入支付密码";
    }
    return _tipLab;
}

- (UILabel *)detailLab{
    if (!_detailLab) {
        _detailLab = [UILabel new];
        _detailLab.textColor = HEXCOLOR(0x333333);
        _detailLab.font = [UIFont boldSystemFontOfSize:15];
        _detailLab.text = @"请输入验证码";
    }
    return _detailLab;
}
- (UIView *)line{
    if (!_line) {
        _line = [UIView new];
        _line.backgroundColor = HEXCOLOR(0xF4F4F4);
    }
    return _line;
}
- (UILabel *)pwdLab{
    if (!_pwdLab) {
        _pwdLab = [UILabel new];
        _pwdLab.textColor = HEXCOLOR(0x666666);
        _pwdLab.font = [UIFont systemFontOfSize:14];
        _pwdLab.text = @"请输入验证码";
    }
    return _pwdLab;
}
- (JHVerificationCodeView *)codeView{
    if (!_codeView) {
        JHVCConfig *config     = [[JHVCConfig alloc] init];
        
        if ([pwdTypeStr isEqual:@"1"]) {
            //单个输入框的颜色
            config.inputBoxColor=  [UIColor whiteColor];
            //光标
            config.tintColor       = [UIColor whiteColor];
            config.underLineColor =  [UIColor whiteColor];
            config.underLineHighlightedColor =  [UIColor whiteColor];
            
        }else{
            config.inputBoxColor   = HEXCOLOR(0xB7B7B7);
            config.tintColor       = HEXCOLOR(0xD1D1D1);
            config.underLineColor = HEXCOLOR(0xD1D1D1);
            config.underLineHighlightedColor = HEXCOLOR(0xD1D1D1);
        }
        config.autoShowKeyboard = YES;
        config.inputBoxNumber  = 6;
        config.inputBoxSpacing = 0;
        config.inputBoxWidth   = (ManMan_SCREEN_WIDTH-bg_left*2-54)/6;
        config.inputBoxHeight  = 47;
        config.secureTextEntry = YES;
       
        config.font            = [UIFont systemFontOfSize:17];
        config.textColor       = HEXCOLOR(0x333333);
        config.inputType       = JHVCConfigInputType_Alphabet;
        config.font = [UIFont boldSystemFontOfSize:22];
        config.inputBoxBorderWidth  = 0.5;
        config.showUnderLine = NO;
        config.underLineSize = CGSizeMake(33, 2);
    
        _codeView = [[JHVerificationCodeView alloc] initWithFrame:CGRectMake(27, 113, ManMan_SCREEN_WIDTH-bg_left*2-54, 47) config:config];
        _codeView.backgroundColor = [pwdTypeStr isEqual:@"1"] ?HEXCOLOR(0xD1D1D1) : [UIColor whiteColor];
        ViewRadius(_codeView, 6);
    }
    @weakify(self);
    _codeView.finishBlock = ^(NSString *code) {
        if (weak_self.delegate) {
            [weak_self.delegate jxp1a1y1PwdMSgCode:code];
        }
    };
    return _codeView;
}
- (UIButton *)closeBtn{
    if (!_closeBtn) {
        _closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_closeBtn setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
        [_closeBtn addTarget:self action:@selector(closeBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _closeBtn;
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self endEditing:YES];
}
-(void)closeBtnAction{
    [self removeFromSuperview];
}
@end
