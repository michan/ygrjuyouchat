//
//  JXBankPwdView.h
//  shiku_im
//
//  Created by 韩银华 on 2020/7/7.
//  Copyright © 2020 Reese. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JHVerificationCodeView.h"

NS_ASSUME_NONNULL_BEGIN

@protocol JXBankPwdViewDelegate <NSObject>

-(void)jxp1a1y1PwdMSgCode:(NSString *)msgCode;

@end

@interface JXBankPwdView : UIView

- (instancetype)initWithFrame:(CGRect)frame pwdType:(NSString *)pwdtype;

@property(nonatomic , assign) id<JXBankPwdViewDelegate>delegate;

- (void)show;

-(void)closeBtnAction;

@end

NS_ASSUME_NONNULL_END
