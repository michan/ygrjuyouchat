//
//  JXSelectDrawWay_VC.m
//  shiku_im
//
//  Created by 韩银华 on 2020/7/7.
//  Copyright © 2020 Reese. All rights reserved.
//

#import "JXSelectDrawWay_VC.h"
#import "JXAddBank_VC.h"
#import "JXAddAlp1a1y1Num_VC.h"
#import "JXOpenBank_VC.h"
@interface JXSelectDrawWay_VC ()
@property (nonatomic, strong) UIView *baseView;

@end

@implementation JXSelectDrawWay_VC

- (id)init
{
    self = [super init];
    if (self) {
        
        self.isGotoBack = YES;
        self.title = @"选择提现方式";
        self.heightHeader = ManMan_SCREEN_TOP;
        self.heightFooter = 0;
        //self.view.frame = CGRectMake(0, 0, ManMan_SCREEN_WIDTH, ManMan_SCREEN_HEIGHT);
        [self createHeadAndFoot];
        self.tableBody.backgroundColor = HEXCOLOR(0xF2F2F2);
        self.tableBody.scrollEnabled = YES;
        
//        self.baseView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ManMan_SCREEN_WIDTH, 0)];
//        self.baseView.backgroundColor = [UIColor clearColor];
//        [self.tableBody addSubview:self.baseView];
        
        NSArray *titleArr = @[@"提现到银行卡",@"提现到支付宝"];
        
        CGFloat btnX = 12;
//        CGFloat btnY;
        CGFloat btnW = ManMan_SCREEN_WIDTH - 24;
        CGFloat btnH = 44;
        
        for (int i = 0; i< titleArr.count ; i++) {
            UIButton *selectWayBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            [selectWayBtn setTitle:titleArr[i] forState:UIControlStateNormal];
            selectWayBtn.titleLabel.font = [UIFont systemFontOfSize:17];
            selectWayBtn.layer.cornerRadius = 7;
            selectWayBtn.layer.masksToBounds = YES;
            selectWayBtn.tag = 101+i;
            [selectWayBtn addTarget:self action:@selector(selectWayBtnAction:) forControlEvents:UIControlEventTouchUpInside];
            if (i == 0) {
                [selectWayBtn setTitleColor:HEXCOLOR(0xFFFFFF) forState:UIControlStateNormal];
                selectWayBtn.backgroundColor = HEXCOLOR(0x42DD5E);
                [selectWayBtn setFrame:CGRectMake(btnX, 56, btnW, btnH)];
            }else{
                [selectWayBtn setTitleColor:HEXCOLOR(0x888888) forState:UIControlStateNormal];
                
                selectWayBtn.backgroundColor = HEXCOLOR(0xFFFFFF);
                selectWayBtn.layer.borderColor = HEXCOLOR(0xDFDEE3).CGColor;
                selectWayBtn.layer.borderWidth = 1;
                [selectWayBtn setFrame:CGRectMake(btnX, 56*2, btnW, btnH)];
            }
            [self.tableBody addSubview:selectWayBtn];
        }
        
    }
    return self;
}
-(void)selectWayBtnAction:(UIButton *)sender{
    
    switch (sender.tag -101) {
        case 0:
        {
            //提现到银行卡
            JXOpenBank_VC *vc = [JXOpenBank_VC new];
            [g_navigation pushViewController:vc animated:YES];

        }
            break;
        case 1:{
            //提现到支付宝
            JXAddAlp1a1y1Num_VC *vc = [JXAddAlp1a1y1Num_VC new];
            [g_navigation pushViewController:vc animated:YES];

        }
        default:
            break;
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    NSLog(@"123");
}


@end
