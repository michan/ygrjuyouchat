//
//  JXAddAlp1a1y1Num_VC.m
//  shiku_im
//
//  Created by 韩银华 on 2020/7/8.
//  Copyright © 2020 Reese. All rights reserved.
//

#import "JXAddAlp1a1y1Num_VC.h"
#import "JXAddp1a1y1NumView.h"
@interface JXAddAlp1a1y1Num_VC ()<JXAddp1a1y1NumViewDelegate>
{
    NSString *realp1a1y1NameStr;
    NSString *realp1a1y1NumStr;
}
//view
@property (nonatomic, strong) JXAddp1a1y1NumView *addNumView;

@end

@implementation JXAddAlp1a1y1Num_VC
- (instancetype)init{
    if (self = [super init]) {
        [self setBgUI];
    }
    return self;
}
- (void)setBgUI{
    
    self.isGotoBack = YES;
    self.title = @"添加支付宝账号";
    self.heightHeader = ManMan_SCREEN_TOP;
    self.heightFooter = 0;
    [self createHeadAndFoot];
    self.tableBody.backgroundColor = HEXCOLOR(0xF2F2F2);
    self.tableBody.scrollEnabled = YES;
    [self.tableBody addSubview:self.addNumView];
}
- (void)jxaliBangding{
    
    if (realp1a1y1NumStr.length == 0) {
        [JY_MyTools showTipView:@"支付宝号不能为空"];
        return;
    }
    if (realp1a1y1NameStr.length == 0) {
        [JY_MyTools showTipView:@"姓名不能为空"];
        return;
    }
    
    
    
}
- (void)jxAlip1a1y1RealName:(NSString *)realName{
    realp1a1y1NameStr = realName;
}
- (void)jxAlip1a1y1Num:(NSString *)p1a1y1Num{
    realp1a1y1NumStr = p1a1y1Num;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (JXAddp1a1y1NumView *)addNumView{
    if (!_addNumView) {
        _addNumView = [[JXAddp1a1y1NumView alloc]initWithFrame:CGRectMake(0, 0, ManMan_SCREEN_WIDTH, ManMan_SCREEN_HEIGHT)];
        _addNumView.delegate = self;
    }
    return _addNumView;
}


@end
