//
//  JXBankList_VC.m
//  shiku_im
//
//  Created by 韩银华 on 2020/7/7.
//  Copyright © 2020 Reese. All rights reserved.
//

#import "JXBankList_VC.h"
#import "JXBankListCell.h"
#import "JXSelectDrawWay_VC.h"
#import "JXOpenBank_VC.h"
#import "JXAddBank_VC.h"
#import "JXAddBankPhoneCode_VC.h"

@interface JXBankList_VC ()<JXBankListCellDelegate,UIAlertViewDelegate>
{
    UIButton *addBankBtn;
    JXBankListModel *selectBankModels;
}
//数据源
@property (nonatomic, strong) NSMutableArray *bankData;

@property (nonatomic, assign) NSInteger delateIndex;

@end

@implementation JXBankList_VC
- (instancetype)init{
    if (self = [super init]) {
        [self setBgUI];
    }
    return self;
}
- (void)setBgUI{
    
    [g_notify addObserver:self selector:@selector(addNewBankLoad) name:@"loadBnakList" object:nil];
    
    self.title = @"银行卡";
     self.heightHeader = ManMan_SCREEN_TOP;
     self.heightFooter = 0;
     self.isGotoBack   = YES;
     [self createHeadAndFoot];
     self.isShowHeaderPull = NO;
     self.isShowFooterPull = NO;
     self.tableViewStyle = UITableViewStyleGrouped;
     self.tableView.backgroundColor = HEXCOLOR(0xF2F2F2);
    
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 65)];
    self.tableView.tableFooterView = footerView;
    
    
    UIView *btnView = [[UIView alloc] initWithFrame:CGRectMake(0, 10, SCREEN_WIDTH, 55)];
    [footerView addSubview:btnView];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addBankBtnAction)];
    btnView.backgroundColor = [UIColor whiteColor];
    [btnView addGestureRecognizer:tap];
    
    UIImageView *imgv = [[UIImageView alloc] initWithFrame:CGRectMake(16, 16.5, 22, 22)];
    imgv.image =[UIImage imageNamed:@"icon_tianjia"];
    [btnView addSubview:imgv];
    UILabel *lb = [[UILabel alloc] initWithFrame:CGRectMake(51, 19, SCREEN_WIDTH - 100, 17)];
    lb.font = SYSFONT(15);
    lb.textColor = HEXCOLOR(0x303030);
    lb.text = @"添加银行卡";
    [btnView addSubview:lb];
    
    
    addBankBtn = [[UIButton alloc] initWithFrame:CGRectMake(12, ManMan_SCREEN_TOP - 30, SCREEN_WIDTH-24, 17)];
    addBankBtn.titleLabel.font = [UIFont systemFontOfSize:14.0];
    [addBankBtn setTitle:@"添加银行卡" forState:UIControlStateNormal];
    
    [addBankBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [addBankBtn setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
    [addBankBtn addTarget:self action:@selector(addBankBtnAction) forControlEvents:UIControlEventTouchUpInside];
    addBankBtn.selected = NO;
//    [self.tableHeader addSubview:addBankBtn];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [g_server jxBankNewsListtoView:self];
    });
    
}
-(void)addNewBankLoad{
    [g_server jxBankNewsListtoView:self];

}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == 1) {
        [g_server jxjiebangBankbankId:selectBankModels.cardId toView:self];
    }
    
}
- (void)jxUnbingBank:(JXBankListModel *)bankModel index:(NSInteger)selectIndexs{
    selectBankModels = bankModel;
    self.delateIndex = selectIndexs;
    [g_App showAlert:@"确认取消绑定吗？" delegate:self tag:12345 onlyConfirm:NO];
    
}

-(void) didServerResultSucces:(JY_Connection*)aDownload dict:(NSDictionary*)dict array:(NSArray*)array1{
    
    if ([aDownload.action isEqualToString:act_isOpenBnak]) {
        
        if ([[dict valueForKey:@"resultObject"] isEqualToString:@"未开户"]) {
            JXAddBank_VC *vc = [JXAddBank_VC new];
            [g_navigation pushViewController:vc animated:YES];
        }else{
            JXAddBankPhoneCode_VC *vc = [JXAddBankPhoneCode_VC new];
            vc.cardName = [dict valueForKey:@"userName"];
            vc.cardIdNum = [dict valueForKey:@"certNo"];
            vc.cardtype = @"1";
            vc.cardPhone = [dict valueForKey:@"mobile"];
            vc.bankOrderId = [dict valueForKey:@"id"];
            vc.isOpenBankStr = @"1";
            [g_navigation pushViewController:vc animated:YES];
        }
        
    }
    
    
    if ([aDownload.action isEqualToString:act_jiebangBank]) {
        [self.bankData removeObjectAtIndex:self.delateIndex];
        [self.tableView reloadData];
    }
    
    if ([aDownload.action isEqualToString:act_BankList]) {
        NSArray *arr = @[@"pinganBg",@"zhongxinBg"];
        int x = arc4random() % 2;
        [self.bankData removeAllObjects];
        for (int i = 0; i < array1.count; i++) {
            JXBankListModel *model = [[JXBankListModel alloc] init];
            [model getAllBankList:[array1 objectAtIndex:i]];
            model.bankBgPic = arr[x];
            [self.bankData addObject:model];
        }
         [self.tableView reloadData];
    }

}
-(int) didServerResultFailed:(JY_Connection*)aDownload dict:(NSDictionary*)dict{
    //    [_wait stop];
    return show_error;
}
#pragma mark -- 添加银行卡
-(void)addBankBtnAction{
    [g_server jxIsOpenBankMsg:self];
    
//    JXOpenBank_VC *vc = [JXOpenBank_VC new];
//    [g_navigation pushViewController:vc animated:YES];
}
- (NSMutableArray *)bankData{
    if (!_bankData) {
        _bankData = [NSMutableArray array];
    }
    return _bankData;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return  self.bankData.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    JXBankListCell *cell = [JXBankListCell cellWithTableView:tableView];
    cell.bankModel = self.bankData[indexPath.section];
    cell.selectBankIndex = indexPath.section;
    cell.delegate = self;
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 110;
    
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headViws = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ManMan_SCREEN_WIDTH, 12)];
    headViws.backgroundColor = HEXCOLOR(0xF2F2F2);
    return headViws;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 12;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return nil;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (self.delegate) {
        [self.delegate selectBankListMsg:self.bankData[indexPath.section]];
        [self actionQuit];
        
    }
}

@end
