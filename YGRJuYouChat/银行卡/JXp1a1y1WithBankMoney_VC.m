//
//  JXp1a1y1WithBankMoney_VC.m
//  shiku_im
//
//  Created by 韩银华 on 2020/7/16.
//  Copyright © 2020 Reese. All rights reserved.
//

#import "JXp1a1y1WithBankMoney_VC.h"
#import "JXp1a1y1BankListView.h"
#import "JXBankPwdView.h"
#import "JXOpenBank_VC.h"
#import "QL_AddOrChooseBackCardView.h"
#import "JXAddBankPhoneCode_VC.h"
@interface JXp1a1y1WithBankMoney_VC ()<JXp1a1y1BankListViewDelegate,JXBankPwdViewDelegate>
{
    NSInteger selectp1a1y1Index;
    NSString *selecp1a1y1Money;
    JXBankListModel *selectBankModel;
    JXBankPwdView *pwdView;
    NSString *tradeOrderStr;//交易订单
       NSString *ncountOrderIdStr;//ID新账号
}
//bg
@property (nonatomic, strong) UIView *bgView;
//确认充值
@property (nonatomic, strong) UIButton *p1a1y1Btn;

@property (nonatomic, strong) NSMutableArray *p1a1y1MoneyData;
//view
@property (nonatomic, strong) JXp1a1y1BankListView *bankMsgView;
//银行卡列表数据
@property (nonatomic, strong) NSMutableArray *bankListDataSource;
//手续费说明
@property (nonatomic, strong) UILabel *declareLab;

@property (nonatomic, strong) QL_AddOrChooseBackCardView *backCardView;

@property(nonatomic, copy)NSDictionary *openDict;



@end

@implementation JXp1a1y1WithBankMoney_VC
- (QL_AddOrChooseBackCardView *)backCardView{
    if (!_backCardView) {
        _backCardView =[QL_AddOrChooseBackCardView initChooseBackCardView];
        _backCardView.frame = CGRectMake(0, ManMan_SCREEN_TOP, SCREEN_WIDTH, 100);
        _backCardView.cardNumber = @"请选择银行卡";
        WEAKSELF;
        _backCardView.chooseBlock = ^{
            if (!selectBankModel.cardNo) {
                [weakSelf addBackCard];
                return;
            }
            if (self.bankMsgView.isHidden) {
                self.bankMsgView.hidden = NO;
                [self.bankMsgView show];
                return;
            }
        };
    }
    return _backCardView;
}
-(void)addBackCard{
    self.bankMsgView.hidden = YES;
    JXAddBankPhoneCode_VC *vc = [JXAddBankPhoneCode_VC new];
    vc.cardName = [self.openDict valueForKey:@"userName"];
    vc.cardIdNum = [self.openDict valueForKey:@"certNo"];
    vc.cardtype = @"1";
    vc.cardPhone = [self.openDict valueForKey:@"mobile"];
    vc.bankOrderId = [self.openDict valueForKey:@"id"];
    vc.isOpenBankStr = @"1";
    [g_navigation pushViewController:vc animated:YES];
}
- (id)init
{
    self = [super init];
    if (self) {
        
        self.isGotoBack = YES;
        self.title = @"充值";
        self.heightHeader = ManMan_SCREEN_TOP;
        self.heightFooter = 0;
        [self createHeadAndFoot];
        self.tableBody.backgroundColor = HEXCOLOR(0xF2F2F2);
        self.tableBody.scrollEnabled = YES;
        [self.tableBody addSubview:self.bgView];
        [self creatMoneyTag];
        [self.bgView addSubview:self.p1a1y1Btn];
        [self.bgView addSubview:self.declareLab];
//        [self.tableBody addSubview:self.bankMsgView];
        self.bankMsgView.hidden = YES;
        
        [self.view addSubview:self.backCardView];
        
 
        [_p1a1y1Btn mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.top.mas_equalTo(264 + 40);
            make.left.mas_equalTo(12);
            make.right.mas_equalTo(-12);
            make.height.mas_equalTo(44);
        }];
        [_declareLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_p1a1y1Btn.mas_bottom).offset(18);
            make.left.mas_equalTo(_p1a1y1Btn.mas_left);
            make.right.mas_equalTo(_p1a1y1Btn.mas_right);
        }];
        [g_server jxBankNewsListtoView:self];
        [g_server jxIsOpenBankMsg:self];
        [g_server jxBankNewsListtoView:self];
    }
    return self;
}
- (void)jxAddNewBank{
    [self addBackCard];
    return;
    self.bankMsgView.hidden = YES;
    JXOpenBank_VC *vc = [JXOpenBank_VC new];
    [g_navigation pushViewController:vc animated:YES];
}
-(void)jxSelectBankModel:(JXBankListModel *)bankModel{
    self.bankMsgView.hidden = YES;
    selectBankModel = bankModel;
    self.backCardView.cardNumber = bankModel.cardNo;
    
    if (selecp1a1y1Money) {
    }
}
-(void)pay{
    [_wait start];
    [g_server jxp1a1y1WithBankMoney:self.p1a1y1MoneyData[selectp1a1y1Index] bankId:selectBankModel.cardId toView:self];
}
- (UIView *)bgView{
    if (!_bgView) {
        _bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ManMan_SCREEN_WIDTH, ManMan_SCREEN_HEIGHT)];
        _bgView.backgroundColor = [UIColor whiteColor];
    }
    return _bgView;
}
-(void)creatMoneyTag{
    // 计算位置
    CGFloat leftSpace = 12.0f;  // 左右空隙
    CGFloat topSpace = 12.f; // 上下空隙
    CGFloat margin = 15.0f;  // 两边的间距
    CGFloat currentX = leftSpace; // X
    CGFloat currentY = 0;// Y
    NSInteger countRow = 0; // 第几行数
    CGFloat lastLabelWidth = 0; // 记录上一个宽度
    CGFloat nowWidth = (ManMan_SCREEN_WIDTH - 24-30)/3;
    
    for (int i = 0 ; i < self.p1a1y1MoneyData.count; i++) {
        
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        NSString *p1a1y1Str = [NSString stringWithFormat:@"%@元",[self.p1a1y1MoneyData objectAtIndex:i]];
        
        [btn setTitle:p1a1y1Str forState:UIControlStateNormal];
        [btn setTitleColor:HEXCOLOR(0x05D168) forState:UIControlStateNormal];
        [btn setTitleColor:HEXCOLOR(0xFFFFFF) forState:UIControlStateSelected];

        btn.layer.borderColor = HEXCOLOR(0x05D168).CGColor;
        btn.layer.borderWidth = 1;
        btn.layer.cornerRadius = 5;
        btn.layer.masksToBounds = YES;
        btn.titleLabel.font = [UIFont systemFontOfSize:15];
        btn.titleLabel.textAlignment = NSTextAlignmentCenter;
        btn.tag = 101+i;
        [btn addTarget:self action:@selector(btnTagsAction:) forControlEvents:UIControlEventTouchUpInside];
        
        /** 计算Frame */
        if (i == 0) {
            currentX = 12;
        }else{
            currentX = currentX  + lastLabelWidth+margin;

        }
        
        
        currentY =18+ countRow * 52 + (countRow + 1) * topSpace + 80;
        // 换行
        if (currentX  + nowWidth >= ManMan_SCREEN_WIDTH) {
                countRow++;
                currentY = currentY + 52 + topSpace;
                currentX = margin;
            }
            lastLabelWidth = nowWidth;
            
            [btn setFrame:CGRectMake(currentX, currentY, nowWidth, 52)];
            
            
            [self.bgView addSubview:btn];
   
            
        }
    
    UILabel *tipLab = [UILabel new];
    tipLab.textColor = HEXCOLOR(0x333333);
    tipLab.font = [UIFont systemFontOfSize:13];
    tipLab.text = @"充值略有延迟，请稍后查询";
    [self.bgView addSubview:tipLab];
    
    [tipLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.mas_equalTo(234 + 20  );
    }];
    
    
}
- (UILabel *)declareLab{
    if (!_declareLab) {
        _declareLab = [UILabel new];
        _declareLab.textColor = HEXCOLOR(0x333333);
        _declareLab.text = @"1、充值限额：单笔最高1000元，次数未作限制\n2、充值时间：根据支付通道风控要求，凌晨1:00-5:00不允许充值，其他时间正常\n3、风控要求：夜间充值将需要扫脸验证，以确保资金安全\n4、其他说明：特殊原因未到账，请联系在线客服解决，一般两小时内处理";
        _declareLab.font = [UIFont systemFontOfSize:11];
        _declareLab.numberOfLines = 0;
    }
    return _declareLab;
}
- (NSMutableArray *)p1a1y1MoneyData{
    if (!_p1a1y1MoneyData) {
        _p1a1y1MoneyData = @[@"10.00",@"50.00",@"100.00",@"200.00",@"500.00",@"1000.00"].mutableCopy;
    }
    return _p1a1y1MoneyData;
}
- (UIButton *)p1a1y1Btn{
    if (!_p1a1y1Btn) {
        _p1a1y1Btn = [UIButton buttonWithType:UIButtonTypeCustom];
        _p1a1y1Btn.backgroundColor = HEXCOLOR(0x05D168);
        _p1a1y1Btn.layer.cornerRadius = 5;
        _p1a1y1Btn.layer.masksToBounds = YES;
        [_p1a1y1Btn setTitle:@"确认充值" forState:UIControlStateNormal];
        [_p1a1y1Btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _p1a1y1Btn.titleLabel.font = [UIFont boldSystemFontOfSize:17];
        [_p1a1y1Btn addTarget:self action:@selector(p1a1y1BtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _p1a1y1Btn;
}
#pragma mark Action
-(void)jxp1a1y1PwdMSgCode:(NSString *)msgCode{
    
//    bankMsgCodeStr = msgCode;
    
    if (msgCode.length != 6) {
        [JY_MyTools showTipView:@"验证码不正确"];
        return;
    }
    if (tradeOrderStr.length == 0) {
        [JY_MyTools showTipView:@""];
        return;
    }
    [_wait start];
    [g_server jxp1a1y1WithBankMoneyConfirmmerOrderId:tradeOrderStr ncountOrderId:ncountOrderIdStr msgCode:msgCode toView:self];
    
    
}
-(void)p1a1y1BtnAction{
    
    if (!selecp1a1y1Money) {
        [JY_MyTools showTipView:@"请选择充值金额"];
        return;
    }
    
    if (self.bankListDataSource.count == 0) {
        self.bankMsgView.hidden = YES;
        [self addBackCard];
        
//        JXOpenBank_VC *vc = [JXOpenBank_VC new];
//        [g_navigation pushViewController:vc animated:YES];
        return;
    }else{
        if (!selectBankModel) {
            if (self.bankMsgView.isHidden) {
                self.bankMsgView.hidden = NO;
                [self.bankMsgView show];
                return;
            }else{
                
                
            }
        }
    }
    
    [_wait start];
    [g_server jxp1a1y1WithBankMoney:self.p1a1y1MoneyData[selectp1a1y1Index] bankId:selectBankModel.cardId toView:self];
    
    
}

- (void)didServerResultSucces:(JY_Connection *)aDownload dict:(NSDictionary *)dict array:(NSArray *)array1{
    [_wait stop];

    if ([aDownload.action isEqualToString:act_isOpenBnak]) {
        
        
        self.openDict = dict;
        
        
    }
    
    if ([aDownload.action isEqualToString:act_BankList]) {
        
        selectBankModel = [[JXBankListModel alloc] init];
        [selectBankModel getAllBankList:[array1 objectAtIndex:0]];
        self.backCardView.cardNumber =selectBankModel.cardNo;
    }
    
    if ([aDownload.action isEqualToString:act_BankList]) {
        
        self.bankListDataSource = array1.mutableCopy;
        
    }
    
    if ([aDownload.action isEqualToString:act_p1a1y1BankMoney]) {
        ////银行卡充值
        //支付
        tradeOrderStr = [dict valueForKey:@"merOrderId"];
        ncountOrderIdStr = [dict valueForKey:@"ncountOrderId"];
        pwdView = [[JXBankPwdView alloc] initWithFrame:CGRectMake(0, 0, ManMan_SCREEN_WIDTH, ManMan_SCREEN_HEIGHT) pwdType:@"1"];
        pwdView.delegate = self;
        [pwdView show];
    }
    if ([aDownload.action isEqual:act_p1a1y1ConfirmBank]) {
        //
        if (pwdView) {
            [pwdView closeBtnAction];
        }
        
        NSString *msg = @"提交成功，等待充值结果";
       
        [JY_MyTools showTipView:msg];
        [self actionQuit];
        
    }
    
}
- (int)didServerResultFailed:(JY_Connection *)aDownload dict:(NSDictionary *)dict{
    [_wait stop];
    return show_error;
}
-(void)btnTagsAction:(UIButton *)sender{
    
    selectp1a1y1Index = sender.tag - 101;
    selecp1a1y1Money = @"123";
    for (int i = 0; i< self.p1a1y1MoneyData.count; i++) {
        
        if (sender.tag == 101+i) {
            sender.selected = YES;
            sender.backgroundColor = HEXCOLOR(0x42DD5E);
            sender.layer.borderColor = HEXCOLOR(0xFFFFFF).CGColor;
            sender.layer.borderWidth = 0.5;
            continue;
        }
        
        UIButton *btn = (UIButton *)[self.bgView viewWithTag:i+101];
        btn.selected = NO;
        btn.backgroundColor = HEXCOLOR(0xFFFFFF);
        btn.layer.borderColor = HEXCOLOR(0x42DD5E).CGColor;
        btn.layer.borderWidth = 0.5;
        
    }
    
}
- (JXp1a1y1BankListView *)bankMsgView{
    if (!_bankMsgView) {
        _bankMsgView = [[JXp1a1y1BankListView alloc] initWithFrame:CGRectMake(0, 0, ManMan_SCREEN_WIDTH, ManMan_SCREEN_HEIGHT)];
        _bankMsgView.delegate =self;
        
    }
    return _bankMsgView;
}
- (NSMutableArray *)bankListDataSource{
    if (!_bankListDataSource) {
        _bankListDataSource = [NSMutableArray array];
    }
    return _bankListDataSource;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


@end
