//
//  JXConfirmAddBank_VC.m
//  shiku_im
//
//  Created by 韩银华 on 2020/7/11.
//  Copyright © 2020 Reese. All rights reserved.
//

#import "JXConfirmAddBank_VC.h"
#import "JXAddBankSuccess_VC.h"
@interface JXConfirmAddBank_VC ()
{
    
}
//提示
@property (nonatomic, strong) UILabel *tipLab;
//背景view
@property (nonatomic, strong) UIView *bgView;

//文字
@property (nonatomic, strong) UILabel *codeLab;
//验证码
@property (nonatomic, strong) UITextField *codeTextF;
//完成按钮
@property (nonatomic, strong) UIButton *okBtn;


@end

@implementation JXConfirmAddBank_VC
- (id)init
{
    self = [super init];
    if (self) {
        
        self.isGotoBack = YES;
//        self.title = @"添加银行卡";
        self.heightHeader = ManMan_SCREEN_TOP;
        self.heightFooter = 0;
        [self createHeadAndFoot];
        self.tableBody.backgroundColor = HEXCOLOR(0xF2F2F2);
        self.tableBody.scrollEnabled = YES;
        [self.tableBody addSubview:self.bgView];
        [self.tableBody addSubview:self.tipLab];
        [self.bgView addSubview:self.codeLab];
        [self.bgView addSubview:self.codeTextF];
        [self.tableBody addSubview:self.okBtn];
        
        
        
        [_tipLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(0);
            make.top.mas_equalTo(16);
        }];
        
        [_bgView setFrame:CGRectMake(12, 60, ManMan_SCREEN_WIDTH-24, 48)];
    
        
        [_codeLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(12);
            make.centerY.mas_equalTo(_bgView);
            make.width.mas_equalTo(60);
        }];

        
        [_codeTextF mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(82);
            make.right.mas_equalTo(-12);
            make.centerY.mas_equalTo(_bgView);
            make.height.mas_equalTo(35);
        }];
        
        [_okBtn setFrame:CGRectMake(12, 168, ManMan_SCREEN_WIDTH-24, 40)];
    
        
    }
    return self;
}

-(void) didServerResultSucces:(JY_Connection*)aDownload dict:(NSDictionary*)dict array:(NSArray*)array1{
    NSLog(@"123");
    JXAddBankSuccess_VC *vc = [JXAddBankSuccess_VC new];
    [g_navigation pushViewController:vc animated:YES];
    
}
-(int) didServerResultFailed:(JY_Connection*)aDownload dict:(NSDictionary*)dict{
//    [_wait stop];

    NSLog(@"123");

    return show_error;
}

- (UILabel *)tipLab{
    if (!_tipLab) {
        _tipLab = [UILabel new];
        _tipLab.textColor = HEXCOLOR(0x333333);
        _tipLab.text = @"添加银行卡-绑卡确认\n请绑定持卡人本人的银行卡";
        _tipLab.numberOfLines= 0;
        _tipLab.textAlignment = NSTextAlignmentCenter;
        _tipLab.font = [UIFont systemFontOfSize:13];
    }
    return _tipLab;
}
- (UILabel *)codeLab{
    if (!_codeLab) {
          _codeLab = [UILabel new];
          _codeLab.textColor = HEXCOLOR(0x333333);
          _codeLab.text = @"验证码";
          _codeLab.font = [UIFont systemFontOfSize:15];
      }
      return _codeLab;
}
- (UITextField *)codeTextF{
    if (!_codeTextF) {
        _codeTextF = [UITextField new];
        _codeTextF.placeholder = @"持卡人本人银行卡号";
        _codeTextF.font = [UIFont systemFontOfSize:15];
//        _codeTextF.delegate =self;
        _codeTextF.keyboardType = UIKeyboardTypeNumberPad;
        [_codeTextF becomeFirstResponder];
    }
    return _codeTextF;
}

- (UIView *)bgView{
    if (!_bgView) {
        _bgView = [UIView new];
        _bgView.backgroundColor = [UIColor whiteColor];
        _bgView.layer.cornerRadius = 7;
        _bgView.layer.masksToBounds = YES;
        _bgView.layer.borderColor = HEXCOLOR(0xDEDFE1).CGColor;
        _bgView.layer.borderWidth = 0.5;
    }
    return _bgView;
}
- (UIButton *)okBtn{
    if (!_okBtn) {
        _okBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _okBtn.backgroundColor = HEXCOLOR(0x42DD5E);
        [_okBtn setTitle:@"完成" forState:UIControlStateNormal];
        [_okBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _okBtn.titleLabel.font = [UIFont boldSystemFontOfSize:17];
        _okBtn.layer.cornerRadius = 7;
        _okBtn.layer.masksToBounds = YES;
        [_okBtn addTarget:self action:@selector(confirmBankAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _okBtn;
}
//完成事件
-(void)confirmBankAction{
   
    if (_codeTextF.text.length == 0) {
        [JY_MyTools showTipView:@"验证码不能为空"];
        return;
    }
    [g_server jxConfirmAddNewBankMsgCode:_codeTextF.text cardId:[_confirmBankDict valueForKey:@"ncountOrderId"] otherID:[_confirmBankDict valueForKey:@"id"] toView:self];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


@end
