//
//  JXAddBank_VC.m
//  shiku_im
//
//  Created by 韩银华 on 2020/7/7.
//  Copyright © 2020 Reese. All rights reserved.
//

#import "JXAddBank_VC.h"
#import "JXAddBankMsgView.h"
#import "JXConfirmAddBank_VC.h"
#import "JXSelectBankTypeView.h"
#import "JXAddBankPhoneCode_VC.h"
#import "JXBaseWeb_VC.h"
@interface JXAddBank_VC ()<JXAddBankMsgViewDelegate,JXSelectBankTypeViewDelegate>
{
    NSString *cardNumStr;
    NSString *cardNameStr;
    NSString *cardTypeStr;
    NSString *cardPhoneStr;

    NSString *addBankType;
}


//view
@property (nonatomic, strong) JXAddBankMsgView *bankView;

@end

@implementation JXAddBank_VC

- (id)init
{
    self = [super init];
    if (self) {
        
        self.isGotoBack = YES;
        self.title = @"添加银行卡";
        self.title = @"";
        self.heightHeader = 0;
        self.tableHeader.backgroundColor = HEXCOLOR(0xffffff);
        self.heightFooter = 0;
        [self createHeadAndFoot];
        self.tableBody.backgroundColor = HEXCOLOR(0xffffff);
        self.tableBody.scrollEnabled = YES;
        addBankType = @"借记卡";
        [self.tableBody addSubview:self.bankView];
    }
    return self;
}
//下一步
- (void)nextAddBank{
    
    
    if (cardNameStr.length == 0) {
        [JY_MyTools showTipView:@"请填写持卡人名字"];
        return;
    }
    if (cardNumStr.length == 0) {
        [JY_MyTools showTipView:@"请填写银行卡卡号"];
        return;
    }
    
    if (addBankType.length == 0) {
        [JY_MyTools showTipView:@"请选择卡类型"];
        return;
    }
    
    if (cardPhoneStr.length == 0) {
        [JY_MyTools showTipView:@"请填写持卡人手机号"];
        return;
    }
    [_wait start];
    
    
    [g_server jxOpenBankMsgUserName:cardNameStr idCardNum:cardNumStr mobile:cardPhoneStr toView:self];
    
    
  

}
//用户协议
- (void)jxAggresBankMsg{
    
    JXBaseWeb_VC *webVC = [JXBaseWeb_VC new];
    [g_notify postNotificationName:@"yonghuxieyi" object:nil];
    webVC.htmlBaseUrl = [NSString stringWithFormat:@"http://%@:8092/UserPolicy.html", APIURL];
    [g_navigation pushViewController:webVC animated:YES];
    
    
}
-(void) didServerResultSucces:(JY_Connection*)aDownload dict:(NSDictionary*)dict array:(NSArray*)array1{
    [_wait stop];

    JXAddBankPhoneCode_VC *vc = [JXAddBankPhoneCode_VC new];
    vc.cardName = cardNameStr;
    vc.cardIdNum = cardNumStr;
    vc.cardtype =@"1";
    vc.cardPhone = cardPhoneStr;
    vc.isOpenBankStr = @"2";
    vc.bankOrderId = [dict valueForKey:@"id"];
    [g_navigation pushViewController:vc animated:YES];
    
    
}
-(int) didServerResultFailed:(JY_Connection*)aDownload dict:(NSDictionary*)dict{
    [_wait stop];

    NSLog(@"123");

    return show_error;
}
-(void)jxBankName:(NSString *)addName{
    cardNameStr = addName;
}
- (void)jxBankCardNum:(NSString *)cardNum{
    cardNumStr = cardNum;
}
- (void)jxBankSelectCardType:(NSString *)bankCardType{
    [self.view endEditing:YES];
    JXSelectBankTypeView *typeView = [[JXSelectBankTypeView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    typeView.delegate = self;
    
    [typeView show];
}
- (void)jxBankPhone:(NSString *)bankPhone{
    cardPhoneStr = bankPhone;
}
//选择的事件
- (void)jxSelectBankType:(NSString *)bankTypes{

    if ([bankTypes containsString:@"储蓄卡"]) {
        _bankView.addBankTypeStr = bankTypes;
    }else{
        _bankView.addBankTypeStr = [NSString stringWithFormat:@"%@ 储蓄卡",bankTypes];
    }
    addBankType = bankTypes;
}

- (JXAddBankMsgView *)bankView{
    if (!_bankView) {
        _bankView = [[JXAddBankMsgView alloc] initWithFrame:CGRectMake(0, 0, ManMan_SCREEN_WIDTH, ManMan_SCREEN_HEIGHT)];
        _bankView.delegate = self;
    }
    return _bankView;
}
-(void)backClick{
    [self actionQuit];
}

@end
