//
//  JXAddBankNextMsg_VC.h
//  shiku_im
//
//  Created by 韩银华 on 2020/7/22.
//  Copyright © 2020 Reese. All rights reserved.
//

#import "JY_admobViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface JXAddBankNextMsg_VC : JY_admobViewController
//持卡人
@property (nonatomic, strong) NSString *cardName;
//身份证
@property (nonatomic, strong) NSString *cardIdNum;
//卡类型
@property (nonatomic, strong) NSString *cardtype;
//手机号
@property (nonatomic, strong) NSString *cardPhone;
//银行卡号
@property (nonatomic, strong) NSString *bankCardNum;

//对象
@property (nonatomic, strong) NSDictionary *bankDic;


@end

NS_ASSUME_NONNULL_END
