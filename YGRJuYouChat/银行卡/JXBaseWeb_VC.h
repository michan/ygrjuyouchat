//
//  JXBaseWeb_VC.h
//  shiku_im
//
//  Created by 韩银华 on 2020/7/24.
//  Copyright © 2020 Reese. All rights reserved.
//

#import "JY_admobViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface JXBaseWeb_VC : JY_admobViewController

//url
@property (nonatomic, strong) NSString *htmlBaseUrl;


@end

NS_ASSUME_NONNULL_END
