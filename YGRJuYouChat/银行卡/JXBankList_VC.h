//
//  JXBankList_VC.h
//  shiku_im
//
//  Created by 韩银华 on 2020/7/7.
//  Copyright © 2020 Reese. All rights reserved.
//

#import "JY_TableViewController.h"
#import "JXBankListModel.h"

@protocol JXBankList_VCDelegate <NSObject>

-(void)selectBankListMsg:(JXBankListModel *)bankModel;

@end


NS_ASSUME_NONNULL_BEGIN

@interface JXBankList_VC : JY_TableViewController

@property (nonatomic, assign) id<JXBankList_VCDelegate>delegate;



@end

NS_ASSUME_NONNULL_END
