//
//  JXAddBank_VC.h
//  shiku_im
//
//  Created by 韩银华 on 2020/7/7.
//  Copyright © 2020 Reese. All rights reserved.
//

#import "JY_admobViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface JXAddBank_VC : JY_admobViewController

//对象传过去
@property (nonatomic, strong) NSDictionary *bankDict;


@end

NS_ASSUME_NONNULL_END
