//
//  JXSettingBankPwd_VC.m
//  shiku_im
//
//  Created by 韩银华 on 2020/7/7.
//  Copyright © 2020 Reese. All rights reserved.
//

#import "JXSettingBankPwd_VC.h"
#import "JXBankPwdView.h"
@interface JXSettingBankPwd_VC ()
//view
@property (nonatomic, strong) JXBankPwdView *pwdView;

@end

@implementation JXSettingBankPwd_VC
- (instancetype)init{
    if (self = [super init]) {
        [self setBgUI];
    }
    return self;
}
- (void)setBgUI{
    
    self.isGotoBack = YES;
    self.title = @"设置支付密码";
    self.heightHeader = ManMan_SCREEN_TOP;
    self.heightFooter = 0;
    [self createHeadAndFoot];
    self.tableBody.backgroundColor = HEXCOLOR(0xF2F2F2);
    self.tableBody.scrollEnabled = YES;
    [self.tableBody addSubview:self.pwdView];
}
- (JXBankPwdView *)pwdView{
    if (!_pwdView) {
        _pwdView = [[JXBankPwdView alloc] initWithFrame:CGRectMake(0, 0, ManMan_SCREEN_WIDTH, 200)];
    }
    return _pwdView;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


@end
