//
//  JXConfirmAddBank_VC.h
//  shiku_im
//
//  Created by 韩银华 on 2020/7/11.
//  Copyright © 2020 Reese. All rights reserved.
//

#import "JY_admobViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface JXConfirmAddBank_VC : JY_admobViewController
//数据源
@property (nonatomic, strong) NSDictionary *confirmBankDict;

@end

NS_ASSUME_NONNULL_END
