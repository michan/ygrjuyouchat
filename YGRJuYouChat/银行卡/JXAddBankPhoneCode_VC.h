//
//  JXAddBankPhoneCode_VC.h
//  shiku_im
//
//  Created by 韩银华 on 2020/7/22.
//  Copyright © 2020 Reese. All rights reserved.
//

#import "JY_admobViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface JXAddBankPhoneCode_VC : JY_admobViewController

//持卡人
@property (nonatomic, strong) NSString *cardName;
//身份证
@property (nonatomic, strong) NSString *cardIdNum;
//卡类型
@property (nonatomic, strong) NSString *cardtype;
//手机号
@property (nonatomic, strong) NSString *cardPhone;
//银行卡号
@property (nonatomic, strong) NSString *bankcardNums;
//ncountOrderId
@property (nonatomic, strong) NSString *bankOrderId;

//绑卡返回的ID
@property (nonatomic, strong) NSString *bankAddId;

//dic
@property (nonatomic, strong) NSDictionary *bankDict;
//是否开户了  1:已经开户  2：未卡户
@property (nonatomic, strong) NSString *isOpenBankStr;

@end

NS_ASSUME_NONNULL_END
