//
//  JY_TableView.h
//  TFJunYouChat
//
//  Created by flyeagleTang on 14-5-27.
//  Copyright (c) 2014年 Reese. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ManMan_TableViewDelegate <NSObject>
@optional

- (void)tableView:(UITableView *)tableView touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event;

- (void)tableView:(UITableView *)tableView touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event;

- (void)tableView:(UITableView *)tableView touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event;

- (void)tableView:(UITableView *)tableView touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event;

@end


typedef enum : NSUInteger {
    EmptyTypeNoData,
    EmptyTypeNetWorkError,
} EmptyType;

@interface  JY_TableView : UITableView
{
    UILabel *_tipLabel;
    UIButton *_tipBtn;
@private
//    id _touchDelegate;
    NSMutableArray* _pool;
}

@property (nonatomic,weak) id<ManMan_TableViewDelegate> touchDelegate;
@property (nonatomic,strong) UIImageView *empty;

- (void)gotoLastRow:(BOOL)animated;
- (void)gotoFirstRow:(BOOL)animated;
- (void)gotoRow:(int)n;

- (void)showEmptyImage:(EmptyType)emptyType;
- (void)hideEmptyImage;
- (void)onAfterLoad;

-(void)addToPool:(id)p;
-(void)delFromPool:(id)p;
//-(void)clearPool:(BOOL)delObj;

-(void)reloadRow:(int)n section:(int)section;//刷新一行
-(void)insertRow:(int)n section:(int)section;//增加一行
-(void)deleteRow:(int)n section:(int)section;//删除一行

//我发送的红包 出现的方式增加过度----
- (void)insertRowAnimation:(int)n section:(int)section;
@end
