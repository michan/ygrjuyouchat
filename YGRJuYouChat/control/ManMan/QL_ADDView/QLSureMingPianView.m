//
//  QLSureMingPianView.m
//  TFJunYouChat
//
//  Created by Qian on 2021/11/13.
//  Copyright © 2021 zengwOS. All rights reserved.
//

#import "QLSureMingPianView.h"

@interface QLSureMingPianView ()
@property (weak, nonatomic) IBOutlet UIView *contentV;

@property (weak, nonatomic) IBOutlet UILabel *nickName;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UITextField *tf;

@end

@implementation QLSureMingPianView
-(void)awakeFromNib{
    [super awakeFromNib];
    self.contentV.layer.cornerRadius = 5;
    self.contentV.layer.masksToBounds = YES;
    ViewRadius(self.tf, 3);
}
- (IBAction)cancelClick:(id)sender {
    [self removeFromSuperview];
}
- (void)setNickNameStr:(NSString *)nickNameStr{
    _nickNameStr = nickNameStr;
    self.nickName.text = nickNameStr?nickNameStr:@"";
    self.contentLabel.text = [NSString stringWithFormat:@"[个人名片]:%@", self.nickName.text];
}
- (void)setHeadImgStr:(NSString *)headImgStr{
    _headImgStr = headImgStr;
    
}
- (IBAction)sureBtnClick:(id)sender {
    [self removeFromSuperview];
    if (self.clickBlock) {
        self.clickBlock(self.tf.text);
    }
}
+(QLSureMingPianView *)initSureMintPianView{
    NSString *className = NSStringFromClass([self class]);
    UINib *nib = [UINib nibWithNibName:className bundle:nil];
    return [nib instantiateWithOwner:nil options:nil].firstObject;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
