//
//  QL_IngoSetingCelll.m
//  TFJunYouChat
//
//  Created by Qian on 2021/11/14.
//  Copyright © 2021 zengwOS. All rights reserved.
//

#import "QL_IngoSetingCelll.h"

@interface QL_IngoSetingCelll ()
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UISwitch *changeSwitch;
@property (weak, nonatomic) IBOutlet UIImageView *jiantou;

@end

@implementation QL_IngoSetingCelll

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setDict:(NSDictionary *)dict{
    _dict = dict;
    self.title.text = dict[@"title"];
    self.changeSwitch.hidden = ![dict[@"type"] boolValue];
    self.jiantou.hidden =[dict[@"type"] boolValue];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
