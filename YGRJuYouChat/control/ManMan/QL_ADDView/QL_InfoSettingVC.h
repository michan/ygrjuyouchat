//
//  QL_InfoSettingVC.h
//  TFJunYouChat
//
//  Created by Qian on 2021/11/14.
//  Copyright © 2021 zengwOS. All rights reserved.
//

#import "JY_TableViewController.h"
#import "JY_UserInfoVC.h"
NS_ASSUME_NONNULL_BEGIN

@interface QL_InfoSettingVC : JY_TableViewController

@property(nonatomic, strong)JY_UserInfoVC *supVC;

@property(nonatomic, assign)int friendStatus;
@property (nonatomic,strong) NSNumber* isBeenBlack;//是否被拉黑
@property (nonatomic,strong) NSNumber* notSeeHim;    // 不看他
@property (nonatomic,strong) NSNumber* notLetSeeHim; // 不让他看

@end

NS_ASSUME_NONNULL_END
