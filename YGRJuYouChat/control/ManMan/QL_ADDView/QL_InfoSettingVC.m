//
//  QL_InfoSettingVC.m
//  TFJunYouChat
//
//  Created by Qian on 2021/11/14.
//  Copyright © 2021 zengwOS. All rights reserved.
//

#import "QL_InfoSettingVC.h"
#import "QL_IngoSetingCelll.h"
#import "JYFeedBackVC.h"
#define HEIGHT 56
#define LINE_INSET 8
#define TopHeight 7
#define CellHeight 45
@interface QL_InfoSettingVC ()
@property(nonatomic, copy) NSArray *array;

@property(nonatomic, strong) UISwitch *guanZhuSwitch;
@property(nonatomic, strong) UISwitch *noSeeTa;
@property(nonatomic, strong) UISwitch *noLetTaSee;
@property(nonatomic, strong) UISwitch *blackList;

@end

@implementation QL_InfoSettingVC
//@synthesize user;
- (UISwitch *)guanZhuSwitch{
    
    if (!_guanZhuSwitch) {
        _guanZhuSwitch = [[UISwitch alloc] init];
    }
    return _guanZhuSwitch;
}
- (UISwitch *)noSeeTa{
    if (!_noSeeTa) {
        _noSeeTa = [[UISwitch alloc] init];
        [_noSeeTa addTarget:self action:@selector(onSeeTa:) forControlEvents:UIControlEventValueChanged];
    }
    return _noSeeTa;
}
-(void)onSeeTa:(UISwitch *)swithc{
    if (swithc.on) {
        
    }else{
        [self.supVC onCancelSee];
    }
}
-(UISwitch *)noLetTaSee{
    if (!_noLetTaSee) {
        _noLetTaSee = [[UISwitch alloc] init];
        
    }
    return _noLetTaSee;
}
- (UISwitch *)blackList{
    if (!_blackList) {
        _blackList = [[UISwitch alloc] init];
        [_blackList addTarget:self action:@selector(onBlackList:) forControlEvents:UIControlEventValueChanged];
    }
    return _blackList;;
}
-(void)onBlackList:(UISwitch *)sender{
    if(!sender.on){
        [self.supVC onDelBlack];
    }else{
        [self.supVC onAddBlack];
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.heightHeader = ManMan_SCREEN_TOP;
    self.heightFooter = 0;
    self.title = @"资料设置";
    self.isGotoBack = YES;
    [self createHeadAndFoot];
    self.view.backgroundColor = self.tableView.backgroundColor;
    self.array = @[
        @[@{@"title":@"备注Ta的信息",@"type":@(0)}],
        @[@{@"title":@"把Ta设为特别关注",@"type":@(1)}],
        @[@{@"title":@"向好友推荐Ta",@"type":@(0)}],
        @[@{@"title":@"不看Ta",@"type":@(1)},@{@"title":@"不让Ta看",@"type":@(1)}],
        @[@{@"title":@"投诉",@"type":@(0)},@{@"title":@"加入黑名单",@"type":@(1)}]];
    

    if (self.friendStatus == -1) {
        self.blackList.on = YES;
    }else{
        self.blackList.on = NO;
    }
    if ([self.notSeeHim boolValue]) {
        self.noSeeTa.on = YES;
    }else{
        self.noSeeTa.on = NO;
    }
    if ([self.notLetSeeHim boolValue]) {
        self.noLetTaSee.on = YES;
    }else{
        self.noLetTaSee.on = NO;
    }
    /*
     notSeeHim;    // 不看他
     notLetSeeHim; // 不让他看
     */
    
    NSArray *a = @[@"备注Ta的信息",@"把Ta设为特别关注",@"向好友推荐Ta",@"不看Ta",@"不让Ta看",@"投诉",@"加入黑名单"];
    CGFloat h = ManMan_SCREEN_TOP;
    for (NSInteger i = 0; i < a.count; i++) {
        NSString *title = a[i];
        UIView *view = [[UIView alloc] init];
        view.backgroundColor = [UIColor whiteColor];
        view.frame = CGRectMake(0, h, SCREEN_WIDTH, 50);
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, SCREEN_WIDTH - 100, 20)];
        [view addSubview:label];
        label.text = a[i];
        label.font = [UIFont systemFontOfSize:14];
        label.textColor = HEXCOLOR(0x303030);
        [self.view addSubview:view];
        view.tag = i;
        if ([title isEqualToString:@"备注Ta的信息"] || [title isEqualToString:@"向好友推荐Ta"] || [title isEqualToString:@"投诉"]) {
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewtapAction:)];
            view.userInteractionEnabled = YES;
            [view addGestureRecognizer:tap];
            UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 40, 15, 20, 20)];
            [view addSubview:img];
            img.image = [UIImage imageNamed:@"ql_arrow_right"];
        }
        if ([title isEqualToString:@"把Ta设为特别关注"]) {
            [view addSubview:self.guanZhuSwitch];
            self.guanZhuSwitch.frame = CGRectMake(SCREEN_WIDTH - 70, 10, 60, 30);
        }
        if ([title isEqualToString:@"不看Ta"]) {
            [view addSubview:self.noSeeTa];
            self.noSeeTa.frame = CGRectMake(SCREEN_WIDTH - 70, 10, 60, 30);
        }
        if ([title isEqualToString:@"不让Ta看"]) {
            [view addSubview:self.noLetTaSee];
            self.noLetTaSee.frame = CGRectMake(SCREEN_WIDTH - 70, 10, 60, 30);
        }
        if ([title isEqualToString:@"加入黑名单"]) {
            [view addSubview:self.blackList];
            self.blackList.frame = CGRectMake(SCREEN_WIDTH - 70, 10, 60, 30);
        }
        h+=50;
        if (i == 0 || i == 1 || i == 2 || i== 4) {
            h += 10;
        }
    
    }
    
    UIView *footView = [[UIView alloc] initWithFrame:CGRectMake(0, h, SCREEN_WIDTH, 100)];
    [self.view addSubview:footView];
    footView.backgroundColor = [UIColor clearColor];
    UIButton *deleteBtn = [[UIButton alloc]init];
    [deleteBtn setTitle:@"删除好友" forState:UIControlStateNormal];
    deleteBtn.backgroundColor = [UIColor whiteColor];
    deleteBtn.frame = CGRectMake(0, 10, SCREEN_WIDTH, 50);
    [deleteBtn setTitleColor:HEXCOLOR(0xFF3B30) forState:UIControlStateNormal];
    [footView addSubview:deleteBtn];
    [deleteBtn addTarget:self action:@selector(deleteBtnClick) forControlEvents:UIControlEventTouchUpInside];
    
    self.tableView.hidden = YES;
    self.tableView.tableFooterView = [UIView new];
    [self.tableView registerNib:[UINib nibWithNibName:@"QL_IngoSetingCelll" bundle:nil] forCellReuseIdentifier:@"QL_IngoSetingCelll"];
    // Do any additional setup after loading the view.
}
-(void)viewtapAction:(UITapGestureRecognizer *)tap{
    NSInteger tag = tap.view.tag;
    switch (tag) {
        case 0:
            [self.supVC onRemark];
            break;
        case 5:
            [self gotoFeedBackVC];
            break;;
        default:
            break;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *title =self.array[indexPath.section][indexPath.row][@"title"];
    if ([title isEqualToString:@"备注Ta的信息"]) {
        [self.supVC onRemark];
    }else if ([title isEqualToString:@"向好友推荐Ta"]) {
        
    }else if ([title isEqualToString:@"投诉"]) {
        [self gotoFeedBackVC];
    }
}
-(void)gotoFeedBackVC{
    JYFeedBackVC *vc  = [JYFeedBackVC new];
    vc.userId = self.supVC.user.userId;
    vc.type = FeedBackTypeComplaints;
    [g_navigation pushViewController:vc animated:YES];
}
-(void)deleteBtnClick{
    [self.supVC onDeleteFriend];
}

@end
