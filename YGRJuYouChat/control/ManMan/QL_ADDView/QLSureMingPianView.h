//
//  QLSureMingPianView.h
//  TFJunYouChat
//
//  Created by Qian on 2021/11/13.
//  Copyright © 2021 zengwOS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void (^HeadBtnClickBlock)(NSString *message);
@interface QLSureMingPianView : UIView
@property(nonatomic, copy) HeadBtnClickBlock clickBlock;
@property(nonatomic, copy)NSString *nickNameStr;
@property(nonatomic, copy)NSString *headImgStr;
@property (weak, nonatomic) IBOutlet UIImageView *head;

+(QLSureMingPianView *)initSureMintPianView;
@end

NS_ASSUME_NONNULL_END
