//
//  JY_admobViewController.h
//  TFJunYouChat
//
//  Created by lifengye on 2020/8/15.
//  Copyright © 2020 zengwOS. All rights reserved.
//

#import <UIKit/UIKit.h>
 
@class AppDelegate;
@class JY_ImageView;
@class JY_Label;

@interface JY_admobViewController : UIViewController{
    ATMHud* _wait;
//    JY_admobViewController* _pSelf;
}
@property(nonatomic,retain,setter = setLeftBarButtonItem:)  UIBarButtonItem *leftBarButtonItem;
@property(nonatomic,retain,setter = setRightBarButtonItem:) UIBarButtonItem *rightBarButtonItem;

@property(nonatomic,retain) UIBarButtonItem *leftBackBarButtonItem;

@property(nonatomic,assign) BOOL isGotoBack;
@property(nonatomic,assign) BOOL isFreeOnClose;
@property(nonatomic,strong) UIView *tableHeader;
@property(nonatomic,strong) UIView *tableFooter;
@property(nonatomic,strong) UIScrollView *tableBody;
@property(nonatomic,assign) int heightHeader;
@property(nonatomic,assign) int heightFooter;
@property(nonatomic,strong) UIButton *footerBtnMid;
@property(nonatomic,strong) UIButton *footerBtnLeft;
@property(nonatomic,strong) UIButton *footerBtnRight;
@property(nonatomic,strong) JY_Label  *headerTitle;

-(void)createHeadAndFoot;
-(void)actionQuit;
-(void)onGotoHome;
@end
