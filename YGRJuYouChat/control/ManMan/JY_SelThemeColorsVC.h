//
//  JY_SelThemeColorsVC.h
//  TFJunYouChat
//
//  Created by 1 on 2019/7/31.
//  Copyright © 2019年 Reese. All rights reserved.
//

#import "JY_admobViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface JY_SelThemeColorsVC : JY_admobViewController

@property (nonatomic, strong) NSArray *array;

@property (nonatomic, assign) NSInteger selectIndex;

@end

NS_ASSUME_NONNULL_END
