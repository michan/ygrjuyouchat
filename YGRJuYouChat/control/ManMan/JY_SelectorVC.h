//
//  JY_SelectorVC.h
//  TFJunYouChat
//
//  Created by lifengye on 2020/8/26.
//  Copyright © 2020 zengwOS. All rights reserved.
//

#import "JY_TableViewController.h"
@class JY_SelectorVC;

@protocol ManMan_SelectorVCDelegate <NSObject>

- (void) selector:(JY_SelectorVC*)selector selectorAction:(NSInteger)selectIndex;

@end

@interface JY_SelectorVC : JY_TableViewController

@property (nonatomic, strong) NSArray *array;

@property (nonatomic, assign) NSInteger selectIndex;

@property (nonatomic, weak) id delegate;

@property (nonatomic, assign) SEL didSelected;

@property (nonatomic, weak) id<ManMan_SelectorVCDelegate> selectorDelegate;

@end
