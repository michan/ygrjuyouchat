//
//  JXRedPacketDisplayVC.m
//  XmimTalkClient
//
//  Created by JayLuo on 2020/9/6.
//  Copyright © 2020 Reese. All rights reserved.
//

#import "JXRedPacketDisplayVC.h"
#import "Masonry.h"
#import "JY_RedPacketListVC.h"
#import "YCMenuView.h"
#import "JY_RecordCodeVC.h"
#define TopHeight 40

@interface JXRedPacketDisplayVC ()
@property (nonatomic, strong) JY_ImageView *head;
@property(nonatomic, weak) UILabel *name;
@property(nonatomic, weak) UILabel *money;
@property(nonatomic, weak) UILabel *sendMoney;
@property(nonatomic, weak) UILabel *gongji;
@property(nonatomic, weak) UILabel *sendGongji;
@property(nonatomic, weak) UILabel *send;
@property(nonatomic, weak) UILabel *received;
@property(nonatomic, weak) UILabel *luckyBoy;
@property(nonatomic, weak) UILabel *time;
@property(nonatomic, strong) NSArray *titles;
@property(nonatomic, assign) long selectedTime;
@end

@implementation JXRedPacketDisplayVC

-(instancetype)init{
    if (self = [super init]) {
        self.isGotoBack = YES;
        self.heightHeader = ManMan_SCREEN_TOP;
        self.heightFooter = 0;
       
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createHeadAndFoot];
    [self setBody];
    [self getDataWithTime:3600*24*7];
}


- (void)getDataWithTime:(long)time {
    _selectedTime = time;
    NSDate *date = [NSDate date];
    NSTimeInterval timeSecond = [date timeIntervalSince1970];
    if (time == 0) {
        // 所有
        [g_server redPacketGetRedReceiveListIndex:0 StartTime:0 EndTime:timeSecond toView:self];
        [g_server redPacketGetSendRedPacketListIndex:0 StartTime:0 EndTime:timeSecond toView:self];
    }else {
        NSTimeInterval startTime = timeSecond - time;
        [g_server redPacketGetRedReceiveListIndex:0 StartTime:startTime EndTime:timeSecond toView:self];
        [g_server redPacketGetSendRedPacketListIndex:0 StartTime:startTime EndTime:timeSecond toView:self];
    }
}

- (void)selectTimes {
    __weak typeof(self) weakSelf = self;
    long oneDay = 3600*24;
    UIImage *image = [UIImage imageNamed:@""];
    YCMenuAction *action = [YCMenuAction actionWithTitle:@"近7天" image:image handler:^(YCMenuAction *action) {
        weakSelf.time.text = action.title;
        [weakSelf getDataWithTime:oneDay*7];
    }];
    YCMenuAction *action1 = [YCMenuAction actionWithTitle:@"近30天" image:image handler:^(YCMenuAction *action) {
        weakSelf.time.text = action.title;
        [weakSelf getDataWithTime:oneDay*30];
    }];
    YCMenuAction *action2 = [YCMenuAction actionWithTitle:@"近90天" image:image handler:^(YCMenuAction *action) {
        weakSelf.time.text = action.title;
        [weakSelf getDataWithTime:oneDay*90];
    }];
    YCMenuAction *action3 = [YCMenuAction actionWithTitle:@"所有" image:image handler:^(YCMenuAction *action) {
        weakSelf.time.text = action.title;
        [weakSelf getDataWithTime:0];
    }];

    // 按钮（YCMenuAction）的集合
    self.titles = @[action,action1,action2,action3];

    // 创建YCMenuView(根据关联点或者关联视图)
    YCMenuView *view = [YCMenuView menuWithActions:self.titles width:100 relyonView:_time];
    
    // 自定义设置
    view.menuColor = [UIColor whiteColor];
    view.separatorColor = [UIColor whiteColor];
    view.maxDisplayCount = 5;  // 最大展示数量（其他的需要滚动才能看到）
    view.offset = 0; // 关联点和弹出视图的偏移距离
    view.textColor = [UIColor blackColor];
    view.textFont = [UIFont systemFontOfSize:14];
    view.menuCellHeight = 30;
    view.dismissOnselected = YES;
    view.dismissOnTouchOutside = YES;
    
    // 显示
    [view show];
}

-(void) didServerResultSucces:(JY_Connection*)aDownload dict:(NSDictionary*)dict array:(NSArray*)array1{
    [_wait hide];
    if ([aDownload.action isEqualToString:act_redPacketGetRedReceiveList]) {
        if (dict) {
            NSString *bestSum = dict[@"bestSum"];
            NSString *redReceiveSum = dict[@"redReceiveSum"];
            NSString *amountSum = dict[@"amountSum"];
            
            NSMutableAttributedString *redReceiveSumString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"收到红包  %@个", redReceiveSum] attributes: @{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 14],NSForegroundColorAttributeName: [UIColor blackColor]}];
            
            [redReceiveSumString addAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 14],NSForegroundColorAttributeName: [UIColor orangeColor]} range:NSMakeRange(4, redReceiveSumString.length - 4)];
            
            _received.attributedText = redReceiveSumString;
            
            
            NSMutableAttributedString *bestSumString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"手气最佳  %@次", bestSum] attributes: @{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 14],NSForegroundColorAttributeName: [UIColor blackColor]}];
            
            [bestSumString addAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 14],NSForegroundColorAttributeName: [UIColor orangeColor]} range:NSMakeRange(4, bestSumString.length - 4)];
            
            _luckyBoy.attributedText = bestSumString;
            
            _money.text = [NSString stringWithFormat:@"¥%@", amountSum];
        }
    }
    
    if ([aDownload.action isEqualToString:act_redPacketGetSendRedPacketList]) {
        if (dict) {
            NSString *redPacketSum = dict[@"redPacketSum"];
            NSString *amountSum = dict[@"amountSum"];
            
            NSMutableAttributedString *redReceiveSumString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"发出红包  %@个", redPacketSum] attributes: @{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 14],NSForegroundColorAttributeName: [UIColor blackColor]}];
            
            [redReceiveSumString addAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 14],NSForegroundColorAttributeName: [UIColor orangeColor]} range:NSMakeRange(4, redReceiveSumString.length - 4)];
            
            _send.attributedText = redReceiveSumString;
            
            _sendMoney.text = [NSString stringWithFormat:@"¥%@", amountSum];
            
        }
    }
    
}

-(int) didServerResultFailed:(JY_Connection*)aDownload dict:(NSDictionary*)dict{
    [_wait stop];
    return show_error;
}

-(int) didServerConnectError:(JY_Connection*)aDownload error:(NSError *)error{//error为空时，代表超时
    [_wait stop];
    return show_error;
}

-(void) didServerConnectStart:(JY_Connection*)aDownload{
    [_wait start];
}

- (void)tapReceived {
//    JY_RedPacketListVC *vc = [[JY_RedPacketListVC alloc] init];
//    vc.selectedTime = self.selectedTime;
//    [g_navigation pushViewController:vc animated:YES];
//    [vc.getBtn sendActionsForControlEvents:UIControlEventTouchUpInside];//代码点击
    
    // 跳转
    JY_RecordCodeVC * recordVC = [[JY_RecordCodeVC alloc] init];
    recordVC.type = ManMan_RecordTypeRedPacketReceive;
    recordVC.selectedTime = self.selectedTime;
    [g_navigation pushViewController:recordVC animated:YES];
    
}

- (void)tapSend {
//    JY_RedPacketListVC *vc = [[JY_RedPacketListVC alloc] init];
//    vc.selectedTime = self.selectedTime;
//    [g_navigation pushViewController:vc animated:YES];
//    [vc.sendBtn sendActionsForControlEvents:UIControlEventTouchUpInside];//代码点击
    JY_RecordCodeVC * recordVC = [[JY_RecordCodeVC alloc] init];
    recordVC.type = ManMan_RecordTypeRedPacketSend;
    recordVC.selectedTime = self.selectedTime;
    [g_navigation pushViewController:recordVC animated:YES];
}

- (void)setBody {
    self.title = @"我的红包";
    self.tableBody.backgroundColor = [UIColor whiteColor];
    UIView *cotanierView = [[UIView alloc] initWithFrame:CGRectMake(20, 20, ManMan_SCREEN_WIDTH - 40, 160)];
    cotanierView.layer.cornerRadius = 10;
    cotanierView.layer.masksToBounds = YES;
    cotanierView.backgroundColor = HEXCOLOR(0xEEEAEF);
    [self.tableBody addSubview:cotanierView];
    
    //头像
    _head = [[JY_ImageView alloc] init];
//    _head.image = [UIImage imageNamed:@"all_members"];
    _head.layer.cornerRadius = 10;
    _head.layer.masksToBounds = YES;
    _head.delegate = self;
    [cotanierView addSubview:_head];
    [_head mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(cotanierView.mas_top).offset(10);
        make.left.equalTo(cotanierView.mas_left).offset(10);
        make.width.height.equalTo(@60);
    }];
    
    [g_server getHeadImageSmall:MY_USER_ID userName:MY_USER_NAME imageView:_head];
    
    //名字Label
    UILabel* p = [[UILabel alloc] init];
    p.font = [UIFont fontWithName:@"AvenirNext-DemiBold" size:18];
    p.text = MY_USER_NAME;
    p.textColor = [UIColor blackColor];
    p.preferredMaxLayoutWidth = 150;
    p.numberOfLines = 2;
    p.userInteractionEnabled = YES;
    [cotanierView addSubview:p];
    _name = p;
    [p mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_head.mas_top).offset(5);
        make.left.equalTo(_head.mas_right).offset(10);
    }];
    
    p = [[UILabel alloc] init];
    p.font = [UIFont systemFontOfSize:14];
    p.text = @"发出红包";
    p.textColor = [UIColor blackColor];
    p.userInteractionEnabled = YES;
    [cotanierView addSubview:p];
    _send = p;
    [p mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_name.mas_top);
        make.right.equalTo(cotanierView.mas_right).offset(-10);
    }];
    
    p = [[UILabel alloc] init];
    p.font = [UIFont systemFontOfSize:14];
    p.text = @"收到红包";
    p.textColor = [UIColor blackColor];
    p.userInteractionEnabled = YES;
    [cotanierView addSubview:p];
    _received = p;
    [p mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_send.mas_bottom).offset(5);
        make.right.equalTo(_send.mas_right);
    }];
    
    p = [[UILabel alloc] init];
    p.font = [UIFont systemFontOfSize:14];
    p.text = @"手气最佳";
    p.textColor = [UIColor blackColor];
    p.userInteractionEnabled = YES;
    [cotanierView addSubview:p];
    _luckyBoy = p;
    [p mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_received.mas_bottom).offset(5);
        make.right.equalTo(_received.mas_right);
    }];
    
    p = [[UILabel alloc] init];
    p.font = [UIFont systemFontOfSize:14];
    p.text = @"近7天";
    p.textColor = [UIColor blackColor];
    p.userInteractionEnabled = YES;
    [cotanierView addSubview:p];
    _time = p;
    [p mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_name.mas_bottom).offset(10);
        make.left.equalTo(_name.mas_left);
    }];
    
    UITapGestureRecognizer *tapTime1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectTimes)];
    [p addGestureRecognizer:tapTime1];
    
    
    
    //头像
    JY_ImageView *imageView = [[JY_ImageView alloc] init];
    imageView.image = [UIImage imageNamed:@"downbelow"];
    imageView.layer.cornerRadius = 5;
    imageView.layer.masksToBounds = YES;
    imageView.delegate = self;
    imageView.userInteractionEnabled = YES;
    [cotanierView addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_time.mas_top);
        make.left.equalTo(_time.mas_right);
        make.width.height.equalTo(@15);
    }];
    
    
    UITapGestureRecognizer *tapTime2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectTimes)];
    [imageView addGestureRecognizer:tapTime2];
    
    p = [[UILabel alloc] init];
    p.font = [UIFont systemFontOfSize:16];
    p.text = @"共计收到";
    p.textColor = [UIColor blackColor];
    p.userInteractionEnabled = YES;
    [cotanierView addSubview:p];
     _gongji = p;
    [p mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(cotanierView.mas_bottom).offset(-10);
        make.left.equalTo(_head.mas_left);
    }];
    
    p = [[UILabel alloc] init];
    p.font = [UIFont fontWithName:@"AvenirNext-DemiBold" size:35];
    p.text = @"¥0.00";
    p.textColor = [UIColor blackColor];
    p.preferredMaxLayoutWidth = 150;
    p.numberOfLines = 1;
    p.userInteractionEnabled = YES;
    [cotanierView addSubview:p];
    _money = p;
    
    [p mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(_gongji.mas_centerY);
        make.left.equalTo(_gongji.mas_right).offset(5);
    }];
    
    // ----
    p = [[UILabel alloc] init];
    p.font = [UIFont systemFontOfSize:16];
    p.text = @"共计发出";
    p.textColor = [UIColor blackColor];
    p.userInteractionEnabled = YES;
    [cotanierView addSubview:p];
    _sendGongji = p;
    [p mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(_gongji.mas_top).offset(-20);
        make.left.equalTo(_gongji.mas_left);
    }];
    
    p = [[UILabel alloc] init];
    p.font = [UIFont fontWithName:@"AvenirNext-DemiBold" size:35];
    p.text = @"¥0.00";
    p.textColor = [UIColor blackColor];
    p.preferredMaxLayoutWidth = 150;
    p.numberOfLines = 1;
    p.userInteractionEnabled = YES;
    [cotanierView addSubview:p];
    _sendMoney = p;
    
    [p mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(_sendGongji.mas_centerY);
        make.left.equalTo(_sendGongji.mas_right).offset(5);
    }];
    // ------
    
    
    UIView *cotanierBaseView = [[UIView alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(cotanierView.frame)+20, ManMan_SCREEN_WIDTH - 40, 110)];
    [self.tableBody addSubview:cotanierBaseView];
    UIBezierPath *shadowPath = [UIBezierPath
    bezierPathWithRect:cotanierBaseView.bounds];
    cotanierBaseView.layer.masksToBounds = NO;
    cotanierBaseView.layer.shadowColor = [[UIColor grayColor] colorWithAlphaComponent:0.5].CGColor;
    cotanierBaseView.layer.shadowOffset = CGSizeMake(0.0f, 0.5f);
    cotanierBaseView.layer.shadowOpacity = 0.5f;
    cotanierBaseView.layer.shadowPath = shadowPath.CGPath;
    
    UIView *cotanierView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ManMan_SCREEN_WIDTH - 40, 110)];
    cotanierView2.layer.cornerRadius = 10;
    cotanierView2.layer.masksToBounds = YES;
    cotanierView2.backgroundColor = HEXCOLOR(0xFFFFFF);
    [cotanierBaseView addSubview:cotanierView2];
    
    
    JY_ImageView *imageView2 = [[JY_ImageView alloc] init];
    imageView2.image = [UIImage imageNamed:@"icon_hbjl"];
    [cotanierView2 addSubview:imageView2];
    [imageView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(cotanierView2.mas_centerY);
        make.left.equalTo(cotanierView2.mas_left).offset(10);
        make.width.height.equalTo(@70);
    }];
    
    UIView *cotanierView3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ManMan_SCREEN_WIDTH - 40, 120)];
    [cotanierView2 addSubview:cotanierView3];
    [cotanierView3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(cotanierView2.mas_top);
        make.left.equalTo(imageView2.mas_right).offset(10);
        make.height.equalTo(cotanierView2).multipliedBy(0.5);
        make.right.equalTo(cotanierView2.mas_right);
    }];
    
    
    p = [[UILabel alloc] init];
    p.font = [UIFont systemFontOfSize:16];
    p.text = @"收到的红包";
    p.textColor = [UIColor blackColor];
    p.userInteractionEnabled = YES;
    [cotanierView3 addSubview:p];
    [p mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(cotanierView3.mas_centerY);
        make.left.equalTo(cotanierView3.mas_left).offset(10);
    }];
    
    
    // 箭头
    JY_ImageView *imageView3 = [[JY_ImageView alloc] init];
    imageView3.image = [UIImage imageNamed:@"set_list_next"];
    imageView3.delegate = self;
    [cotanierView3 addSubview:imageView3];
    [imageView3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(cotanierView3.mas_centerY);
        make.right.equalTo(cotanierView3.mas_right).offset(-5);
        make.width.height.equalTo(@20);
    }];

    
    UIView *cotanierView4 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ManMan_SCREEN_WIDTH - 40, 120)];
    [cotanierView2 addSubview:cotanierView4];
    [cotanierView4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(cotanierView3.mas_bottom);
        make.left.right.height.equalTo(cotanierView3);
    }];
    
    p = [[UILabel alloc] init];
    p.font = [UIFont systemFontOfSize:16];
    p.text = @"发出的红包";
    p.textColor = [UIColor blackColor];
    p.userInteractionEnabled = YES;
    [cotanierView4 addSubview:p];
    [p mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(cotanierView4.mas_centerY);
        make.left.equalTo(cotanierView4.mas_left).offset(10);
    }];
    
    
    // 箭头
    JY_ImageView *imageView4 = [[JY_ImageView alloc] init];
    imageView4.image = [UIImage imageNamed:@"set_list_next"];
    imageView4.delegate = self;
    [cotanierView4 addSubview:imageView4];
    [imageView4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(cotanierView4.mas_centerY);
        make.right.equalTo(cotanierView4.mas_right).offset(-5);
        make.width.height.equalTo(@20);
    }];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapReceived)];
    [cotanierView3 addGestureRecognizer:tap];
    
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapSend)];
    [cotanierView4 addGestureRecognizer:tap1];
}

- (void)dealloc {
    NSLog(@"%s", __FUNCTION__);
}

@end
