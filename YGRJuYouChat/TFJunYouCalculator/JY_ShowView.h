//
//  JY_ShowView.h
//  JTManyChildrenSongs
//
//  Created by os on 2020/9/19.
//  Copyright © 2020 Reese. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JY_CalculatorView.h"
#import "JY_Stack.h"

NS_ASSUME_NONNULL_BEGIN

@interface JY_ShowView : UIView

@property(nonatomic, strong) UITextView *textView;
@property(nonatomic, strong) JY_CalculatorView *JY_CalculatorView;
@property(nonatomic, strong) JY_Stack *stack;
@property(nonatomic, strong) NSMutableString *textString;
@end

NS_ASSUME_NONNULL_END
