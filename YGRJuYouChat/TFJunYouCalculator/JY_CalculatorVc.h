//
//  ViewController.h
//  TFJunYouChat
//
//  Created by lifengye on 2018/9/27.
//  Copyright © 2020 zengwOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JY_CalculatorView.h"
#import "JY_Stack.h"

@interface JY_CalculatorVc : UIViewController

@property(nonatomic, strong) UITextView *textView;
@property(nonatomic, strong) JY_CalculatorView *JY_CalculatorView;
@property(nonatomic, strong) JY_Stack *stack;
@property(nonatomic, strong) NSMutableString *textString;

@end

