#import <UIKit/UIKit.h>
#import "ImageSelectorViewController.h"
#import "JY_VideoPlayer.h"

@class JY_CaptureMedia;
@class JY_Label;
@class JY_ImageView;

@interface recordVideoViewController : UIViewController <ImageSelectorViewDelegate>{
    JY_CaptureMedia* _capture;

    UIView* preview;
    UIImageView *_timeBGView;
    UILabel *_timeLabel;
    JY_ImageView* _flash;
    JY_ImageView* _flashOn;
    JY_ImageView* _flashOff;
    JY_ImageView* _cammer;
    UIButton* _recrod;
    JY_ImageView* _close;
    JY_ImageView* _save;
//    UIImageView *_noticeView;
    UILabel *_noticeLabel;
    UILabel * _recordLabel;
    UIView *_bottomView;
    recordVideoViewController* _pSelf;
}

//- (IBAction)doFileConvert;

@property(nonatomic,assign) BOOL isReciprocal;//是否倒计时,为该参赋值一定也要给mixTime赋值
@property(nonatomic,assign) int maxTime;
@property(nonatomic,assign) int minTime;
@property(nonatomic,assign) BOOL isShowSaveImage;//是否显示选择保存截图界面
@property(nonatomic,assign) int timeLen;
@property(nonatomic,weak) id delegate;
@property(assign) SEL didRecord;
@property (nonatomic,strong) NSString* outputFileName;//返回的video
@property (nonatomic,strong) NSString* outputImage;//返回的截图
@property (nonatomic,strong) JY_CaptureMedia* recorder;


@property (nonatomic, strong) UIView *playerView;
@property (nonatomic, strong) JY_VideoPlayer *player;

@end

