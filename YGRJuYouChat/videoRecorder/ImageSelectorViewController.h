//
//  ImageSelectorViewController.h
//  TFJunYouChat
//
//  Created by 1 on 17/1/19.
//  Copyright © 2020 zengwOS. All rights reserved.
//

#import "JY_admobViewController.h"

@protocol ImageSelectorViewDelegate <NSObject>

-(void)imageSelectorDidiSelectImage:(NSString *)imagePath;

@end

@interface ImageSelectorViewController : JY_admobViewController


@property (nonatomic,strong) NSArray * imageFileNameArray;
@property (nonatomic, weak) id<ImageSelectorViewDelegate> imgDelegete;

@end
