//
//  JY_BillingRecordModel.h
//  TFJunYouChat
//
//  Created by JayLuo on 2021/3/20.
//  Copyright © 2021 zengwOS. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TotalVo :NSObject
@property (nonatomic , assign) double              expendMoney;
@property (nonatomic , assign) double              expendSum;
@property (nonatomic , assign) double              receiveSum;
@property (nonatomic , assign) double              receiveMoney;

@end


@interface Id :NSObject
@property (nonatomic , assign) NSInteger              date;
@property (nonatomic , assign) NSInteger              machineIdentifier;
@property (nonatomic , assign) NSInteger              counter;
@property (nonatomic , assign) NSInteger              processIdentifier;
@property (nonatomic , assign) NSInteger              time;
@property (nonatomic , assign) NSInteger              timeSecond;
@property (nonatomic , assign) NSInteger              timestamp;

@end


@interface RedPacketId :NSObject
@property (nonatomic , assign) NSInteger              date;
@property (nonatomic , assign) NSInteger              machineIdentifier;
@property (nonatomic , assign) NSInteger              counter;
@property (nonatomic , assign) NSInteger              processIdentifier;
@property (nonatomic , assign) NSInteger              time;
@property (nonatomic , assign) NSInteger              timeSecond;
@property (nonatomic , assign) NSInteger              timestamp;

@end


@interface JY_DataItem :NSObject
@property (nonatomic , assign) NSInteger              transferStatus;
@property (nonatomic , copy) NSString              * tradeNo;
@property (nonatomic , assign) NSInteger              changeType;
@property (nonatomic , assign) NSInteger              currentBalance;
@property (nonatomic , assign) NSInteger              manualPay_status;
@property (nonatomic , assign) NSInteger              type;
@property (nonatomic , assign) NSInteger              toUserId;
@property (nonatomic , assign) NSInteger              userId;
@property (nonatomic , assign) NSInteger              payType;
@property (nonatomic , assign) double              money;
@property (nonatomic , assign) NSInteger              operationAmount;
@property (nonatomic , strong) Id              * id;
@property (nonatomic , assign) NSInteger              time;
@property (nonatomic , strong) RedPacketId              * redPacketId;
@property (nonatomic , copy) NSString              * desc;
@property (nonatomic , assign) NSInteger              status;
@property (nonatomic , copy) NSString              * toUserName;
@property (nonatomic , copy) NSString              * extendId;
@property (nonatomic , assign) NSInteger              isGet;

@end


@interface JY_BillingRecordModel :NSObject
@property (nonatomic , strong) TotalVo              * totalVo;
@property (nonatomic , assign) double              total;
@property (nonatomic , strong) NSArray <JY_DataItem *>              * data;
@property (nonatomic , assign) NSInteger              count;

@end

NS_ASSUME_NONNULL_END
