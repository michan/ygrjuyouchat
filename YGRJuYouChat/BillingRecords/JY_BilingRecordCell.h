//
//  JY_BilingRecordCell.h
//  TFJunYouChat
//
//  Created by JayLuo on 2021/3/20.
//  Copyright © 2021 zengwOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JY_BillingRecordModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface JY_BilingRecordCell : UITableViewCell
@property(nonatomic, strong) JY_DataItem *model;
@end

NS_ASSUME_NONNULL_END
