//
//  JY_BillingRecordsVC.m
//  TFJunYouChat
//
//  Created by JayLuo on 2021/3/19.
//  Copyright © 2021 zengwOS. All rights reserved.
//

#import "JY_BillingRecordsVC.h"
#import "UIButton+HQCustomIcon.h"
#import "JY_SelectMenuView.h"
#import "YCMenuView.h"
#import "JY_BilingRecordCell.h"
#import "JY_BillingRecordModel.h"
#import "JY_redPacketDetailVC.h"
#import "JY_TransferDeatilVC.h"

@interface JY_BillingRecordsVC ()<ManMan_SelectMenuViewDelegate>
@property (nonatomic, strong) UIView * headView;
@property (nonatomic, strong) UIButton * listButton;
@property (nonatomic, strong) UIButton * selectDaysButton;
@property (nonatomic, strong) UILabel *selectDaysLabel;
@property(nonatomic, strong) UILabel *spendingLabel;
@property(nonatomic, strong) UILabel *receivedLabel;
@property(nonatomic, strong) UILabel *spendingNumberLabel;
@property(nonatomic, strong) UILabel *receivedNumberLabel;
@property(nonatomic, strong) NSArray *titles;
@property(nonatomic, assign) long selectedTime;
@property(nonatomic, assign) int type;
@property(nonatomic, strong) JY_BillingRecordModel *model;
@end

@implementation JY_BillingRecordsVC

- (instancetype)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"账单记录";
    self.heightHeader = ManMan_SCREEN_TOP;
    self.heightFooter = 0;
    self.isGotoBack = YES;
    [self createHeadAndFoot];
    [self.tableHeader addSubview:self.listButton];
    long oneDay = 3600*24;
    _selectedTime = oneDay * 7;
    _type = 0;
    UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0, -60, ManMan_SCREEN_WIDTH, 60)];
    headView.backgroundColor = HEXCOLOR(0xf0f0f0);
//    _table.tableHeaderView = headView;
    _headView = headView;
    
    UILabel *spendingLabel = [[UILabel alloc] init];
    spendingLabel.text = @"共支出--元";
    spendingLabel.font = [UIFont systemFontOfSize:14];
    spendingLabel.textColor = [UIColor lightGrayColor];
    [headView addSubview:spendingLabel];
    _spendingLabel = spendingLabel;
    [spendingLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(headView).offset(10);
    }];
    
    UILabel *receivedLabel = [[UILabel alloc] init];
    receivedLabel.text = @"共收到--元";
    receivedLabel.font = [UIFont systemFontOfSize:14];
    receivedLabel.textColor = [UIColor lightGrayColor];
    [headView addSubview:receivedLabel];
    _receivedLabel = receivedLabel;
    [receivedLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(spendingLabel.mas_right).offset(20);
        make.centerY.equalTo(spendingLabel.mas_centerY);
    }];
    
    UILabel *spendingNumberLabel = [[UILabel alloc] init];
    spendingNumberLabel.text = @"共支出：--笔";
    spendingNumberLabel.font = [UIFont systemFontOfSize:14];
    spendingNumberLabel.textColor = [UIColor lightGrayColor];
    [headView addSubview:spendingNumberLabel];
    _spendingNumberLabel = spendingNumberLabel;
    [spendingNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(spendingLabel.mas_left);
        make.top.equalTo(spendingLabel.mas_bottom).offset(5);
    }];
    
    UILabel *receivedNumberLabel = [[UILabel alloc] init];
    receivedNumberLabel.text = @"共收到：--笔";
    receivedNumberLabel.font = [UIFont systemFontOfSize:14];
    receivedNumberLabel.textColor = [UIColor lightGrayColor];
    [headView addSubview:receivedNumberLabel];
    _receivedNumberLabel = receivedNumberLabel;
    [receivedNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(receivedLabel.mas_left);
        make.centerY.equalTo(spendingNumberLabel.mas_centerY);
    }];
    
    
//    [headView addSubview:self.selectDaysButton];
//    [self.selectDaysButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.equalTo(headView.mas_right).offset(-10);
//        make.centerY.equalTo(headView.mas_centerY);
//    }];
    
    
    UILabel *selectDaysLabel = [[UILabel alloc] init];
    selectDaysLabel.font = [UIFont systemFontOfSize:14];
    selectDaysLabel.text = @"最近7天";
    selectDaysLabel.textColor = [UIColor blackColor];
    selectDaysLabel.userInteractionEnabled = YES;
    [headView addSubview:selectDaysLabel];
    _selectDaysLabel = selectDaysLabel;
    [selectDaysLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(headView.mas_right).offset(-20);
        make.centerY.equalTo(headView.mas_centerY);
    }];
    
    UITapGestureRecognizer *tapTime1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectTimes)];
    [selectDaysLabel addGestureRecognizer:tapTime1];
    
    
    
    //头像
    JY_ImageView *imageView = [[JY_ImageView alloc] init];
    imageView.image = [UIImage imageNamed:@"icon_jiantou"];
    imageView.layer.cornerRadius = 5;
    imageView.layer.masksToBounds = YES;
    imageView.delegate = self;
    imageView.userInteractionEnabled = YES;
    [headView addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(selectDaysLabel.mas_top).offset(5);
        make.left.equalTo(selectDaysLabel.mas_right);
        make.width.equalTo(@16);
        make.height.equalTo(@10);
    }];
    
    
    UITapGestureRecognizer *tapTime2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectTimes)];
    [imageView addGestureRecognizer:tapTime2];

    //悬停view
    [_table addSubview:headView];
    _table.contentInset = UIEdgeInsetsMake(60, 0, 0, 0);
    [_table registerNib:[UINib nibWithNibName:@"JY_BilingRecordCell" bundle:nil] forCellReuseIdentifier:@"JY_BilingRecordCell"];
    [self scrollToPageUp];
}

- (UIButton *)listButton {
    if(!_listButton) {
        _listButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _listButton.frame = CGRectMake(ManMan_SCREEN_WIDTH - 51 - 15, ManMan_SCREEN_TOP - 8 - 29, 51, 29);
        _listButton.titleLabel.font = [UIFont systemFontOfSize:15];
        [_listButton setTitle:@"全部" forState:UIControlStateNormal];
        [_listButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_listButton setImage:THESIMPLESTYLE ? [UIImage imageNamed:@"icon_xiala"] : [UIImage imageNamed:@"icon_xiala"] forState:UIControlStateNormal];
        _listButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [_listButton addTarget:self action:@selector(listButtonAction) forControlEvents:UIControlEventTouchUpInside];
        [_listButton setIconInRightWithSpacing:5];
    }
    return _listButton;
}

- (UIButton *)selectDaysButton {
    if(!_selectDaysButton) {
        _selectDaysButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _selectDaysButton.frame = CGRectMake(0, 0,  100, 20);
        _selectDaysButton.titleLabel.font = [UIFont systemFontOfSize:14];
        [_selectDaysButton setTitle:@"最近7天" forState:UIControlStateNormal];
        [_selectDaysButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_selectDaysButton setImage:THESIMPLESTYLE ? [UIImage imageNamed:@"icon_xiala"] : [UIImage imageNamed:@"icon_xiala"] forState:UIControlStateNormal];
        _selectDaysButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [_selectDaysButton addTarget:self action:@selector(selectTimes) forControlEvents:UIControlEventTouchUpInside];
        [_selectDaysButton setIconInRightWithSpacing:5];
    }
    return _selectDaysButton;
}

#pragma mark Action
- (void)listButtonAction {
    NSMutableArray *titles = [NSMutableArray arrayWithArray:@[@"全部", @"红包",@"转账",@"退款",@"充值",@"提现"]];
    JY_SelectMenuView *menuView = [[JY_SelectMenuView alloc] initWithTitle:titles image:nil cellHeight:44];
//    menuView.sels = sels;
    menuView.delegate = self;
    [g_App.window addSubview:menuView];
}


- (void)didMenuView:(JY_SelectMenuView *)MenuView WithIndex:(NSInteger)index {
    [self getDataType:index];
}


- (void)getDataType:(NSInteger)type {
    // type 类型 0所有 1红包 2转账 3退款 4充值 5提现
    NSLog(@"type ----- %zd", type);
    NSString *title = @"";
    switch (type) {
        case 0:
            title = @"全部";
            break;
        case 1:
            title = @"红包";
            break;
        case 2:
            title = @"转账";
            break;
        case 3:
            title = @"退款";
            break;
        case 4:
            title = @"充值";
            break;
        case 5:
            title = @"提现";
            break;
        default:
            title = @"全部";
            break;
    }
    [_listButton setTitle:title forState:UIControlStateNormal];
    _type = (int)type;
    [self getDataWithTime:_selectedTime];
}

- (void)selectTimes {
    __weak typeof(self) weakSelf = self;
    long oneDay = 3600*24;
    UIImage *image = [UIImage imageNamed:@""];
    YCMenuAction *action = [YCMenuAction actionWithTitle:@"最近7天" image:image handler:^(YCMenuAction *action) {
        weakSelf.selectDaysLabel.text = action.title;
        [weakSelf getDataWithTime:oneDay*7];
    }];
    YCMenuAction *action1 = [YCMenuAction actionWithTitle:@"最近30天" image:image handler:^(YCMenuAction *action) {
        weakSelf.selectDaysLabel.text = action.title;
        [weakSelf getDataWithTime:oneDay*30];
    }];
    YCMenuAction *action2 = [YCMenuAction actionWithTitle:@"最近90天" image:image handler:^(YCMenuAction *action) {
        weakSelf.selectDaysLabel.text = action.title;
        [weakSelf getDataWithTime:oneDay*90];
    }];
    YCMenuAction *action3 = [YCMenuAction actionWithTitle:@"所有" image:image handler:^(YCMenuAction *action) {
        weakSelf.selectDaysLabel.text = action.title;
        [weakSelf getDataWithTime:0];
    }];

    // 按钮（YCMenuAction）的集合
    self.titles = @[action,action1,action2,action3];

    // 创建YCMenuView(根据关联点或者关联视图)
    YCMenuView *view = [YCMenuView menuWithActions:self.titles width:100 relyonView:_selectDaysLabel];
    
    // 自定义设置
    view.menuColor = [UIColor whiteColor];
    view.separatorColor = [UIColor whiteColor];
    view.maxDisplayCount = 5;  // 最大展示数量（其他的需要滚动才能看到）
    view.offset = 0; // 关联点和弹出视图的偏移距离
    view.textColor = [UIColor blackColor];
    view.textFont = [UIFont systemFontOfSize:14];
    view.menuCellHeight = 30;
    view.dismissOnselected = YES;
    view.dismissOnTouchOutside = YES;
    
    // 显示
    [view show];
}


- (void)getDataWithTime:(long)time {
    _selectedTime = time;
    NSDate *date = [NSDate date];
    NSTimeInterval timeSecond = [date timeIntervalSince1970];
    if (time == 0) {
        // 所有
        [g_server getNewConsumeRecord:0 pageSize:10000 type:_type startTime:0 toView:self];
    }else {
        NSTimeInterval startTime = timeSecond - time;
        [g_server getNewConsumeRecord:0 pageSize:10000 type:_type startTime:startTime toView:self];
    }
}



- (void)didServerResultSucces:(JY_Connection*)aDownload dict:(NSDictionary*)dict array:(NSArray*)array1 {
    [_wait hide];
    [_header endRefreshing];
    [_footer endRefreshing];
    if ([aDownload.action isEqualToString:act_consumeRecord]) {
        [JY_BillingRecordModel mj_setupObjectClassInArray:^NSDictionary *{
            return @{
                       @"data" : @"DataItem",
                   };
        }];
        _model = [JY_BillingRecordModel mj_objectWithKeyValues:dict];
        // type 类型 0所有 1红包 2转账 3退款 4充值 5提现
        if (_type == 0 || _type == 1 || _type == 2) {
            double expendMoney = _model.totalVo.expendMoney;
            double expendSum = _model.totalVo.expendSum;
            double receiveMoney = _model.totalVo.receiveMoney;
            double receiveSum = _model.totalVo.receiveSum;
            _spendingLabel.text = [NSString stringWithFormat:@"共支出%.2f元", expendMoney];
            _spendingNumberLabel.text = [NSString stringWithFormat:@"共支出：%.f笔", expendSum];
            _receivedLabel.text = [NSString stringWithFormat:@"共收到%.2f元", receiveMoney];
            _receivedNumberLabel.text = [NSString stringWithFormat:@"共收到：%.f笔", receiveSum];
            [_spendingLabel mas_updateConstraints:^(MASConstraintMaker *make) {
                make.left.top.equalTo(_headView).offset(10);
            }];
        } else if (_type == 3 || _type == 4 || _type == 5) {
            double expendMoney = _model.totalVo.receiveMoney;
            NSString *typeString = @"";
            switch (_type) {
                case 3:
                    typeString = @"退款";
                    break;
                case 4:
                    typeString = @"充值";
                    break;
                case 5:
                    typeString = @"提现";
                    break;
                    
                default:
                    break;
            }
            _spendingLabel.text = [NSString stringWithFormat:@"共%@%.2f元", typeString, expendMoney];
            
//            [_spendingLabel mas_updateConstraints:^(MASConstraintMaker *make) {
//                make.left.equalTo(_headView.mas_left).offset(10);
////                make.centerY.equalTo(_headView.mas_centerY);
//            }];
            _spendingNumberLabel.text = @"";
            _receivedLabel.text = @"";
            _receivedNumberLabel.text = @"";
        }

        [_table reloadData];
        
    }
    
    if ([aDownload.action isEqualToString:act_getRedPacket]) {
        
        NSString *roomJid = [(NSDictionary *)[dict objectForKey:@"packet"] objectForKey:@"roomJid"];
        BOOL isGroup = NO;
        if (roomJid == nil || roomJid.length <= 0) {
            isGroup = NO;
        }else {
            isGroup = YES;
        }
        
        JY_redPacketDetailVC * redPacketDetailVC = [[JY_redPacketDetailVC alloc]init];
        redPacketDetailVC.dataDict = [[NSDictionary alloc]initWithDictionary:dict];
        redPacketDetailVC.isGroup = isGroup;
        [g_navigation pushViewController:redPacketDetailVC animated:YES];
    }
    

    
}

- (int)didServerResultFailed:(JY_Connection*)aDownload dict:(NSDictionary*)dict {
    [_wait hide];
    [_header endRefreshing];
    [_footer endRefreshing];
    if ([aDownload.action isEqualToString:act_getRedPacket]) {
        
        NSString *roomJid = [(NSDictionary *)[dict objectForKey:@"packet"] objectForKey:@"roomJid"];
        BOOL isGroup = NO;
        if (roomJid == nil || roomJid.length <= 0) {
            isGroup = NO;
        }else {
            isGroup = YES;
        }
        
        JY_redPacketDetailVC * redPacketDetailVC = [[JY_redPacketDetailVC alloc]init];
        redPacketDetailVC.dataDict = [[NSDictionary alloc]initWithDictionary:dict];
        redPacketDetailVC.isGroup = isGroup;
        [g_navigation pushViewController:redPacketDetailVC animated:YES];
    }
    return hide_error;
}

- (int)didServerConnectError:(JY_Connection*)aDownload error:(NSError *)error {//error为空时，代表超时
    [_wait hide];
    [_header endRefreshing];
    [_footer endRefreshing];
    return show_error;
}

- (void)didServerConnectStart:(JY_Connection*)aDownload {
    [_wait start];
}





- (void)scrollToPageUp {
    [self getDataWithTime:_selectedTime];
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat offsetY = scrollView.contentOffset.y;
    self.headView.frame = CGRectMake(0, offsetY, self.headView.bounds.size.width, self.headView.bounds.size.height);
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _model.data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    JY_BilingRecordCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JY_BilingRecordCell" forIndexPath:indexPath];
    cell.model = _model.data[indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    JY_DataItem *modelItem = _model.data[indexPath.row];
    if (modelItem.type == 4 || modelItem.type == 5) {
        // 红包详情
        if (modelItem.extendId != nil) {
            [g_server getRedPacket:modelItem.extendId toView:self];
        }else {
//            [g_App showAlert:@"获取红包信息失败！"];
            [g_App showAlert:@"红包已删除！"];
        }
        
    }
    
    if (modelItem.type == 7 || modelItem.type == 8) {
        if (modelItem.extendId != nil) {
            // 转账详情
            JY_MessageObject *msg = [JY_MessageObject new];
            msg.objectId = modelItem.extendId;
            JY_TransferDeatilVC *detailVC = [JY_TransferDeatilVC alloc];
            detailVC.msg = msg;
            detailVC = [detailVC init];
            [g_navigation pushViewController:detailVC animated:YES];
        } else {
//            [g_App showAlert:@"获取转账信息失败！"];
            [g_App showAlert:@"转账已删除！"];
            
        }
    }
}

@end
