//
//  JY_BilingRecordCell.m
//  TFJunYouChat
//
//  Created by JayLuo on 2021/3/20.
//  Copyright © 2021 zengwOS. All rights reserved.
//

#import "JY_BilingRecordCell.h"

@interface JY_BilingRecordCell()
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *leftTopLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightTopLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;

@end

@implementation JY_BilingRecordCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.avatarImageView.layer.cornerRadius = self.avatarImageView.frame.size.width / 2;
    self.avatarImageView.layer.masksToBounds = YES;
}

- (void)setModel:(JY_DataItem *)model {
    _model = model;
    if (model.type == 1) {
        
    }
    NSString *symbolStr = @"";
    NSInteger type = model.type;
    if (type == 2 || type == 4 || type == 7 || type == 10 || type == 12 || type == 14|| type == 16) {
        symbolStr = @"-";
        _rightTopLabel.textColor = [UIColor blackColor];
    }else {
        symbolStr = @"+";
        _rightTopLabel.textColor = [UIColor orangeColor];
    }
    
    // 发出红包
    if (type == 4) {
        //。
        _statusLabel.hidden = NO;
        _statusLabel.text = model.isGet == 1 ? @"已领完" : @"未领完";
        _statusLabel.textColor = model.isGet == 1 ? [UIColor blackColor] : [UIColor redColor];
    }else if (type == 7) {
        _statusLabel.hidden = NO;
        _statusLabel.text = model.isGet == 1 ? @"已领取" : @"未领取";
        _statusLabel.textColor = model.isGet == 1 ? [UIColor blackColor] : [UIColor redColor];
    }else {
        _statusLabel.hidden = YES;
    }
    
    _rightTopLabel.text = [NSString stringWithFormat:@"%@%.2f", symbolStr, model.money];
    _timeLabel.text = [self getDateStringWithTimeStr:[NSString stringWithFormat:@"%ld", model.time]];
    
    // 转账和红包描述
    if (model.type == 5) {
        _leftTopLabel.text = [NSString stringWithFormat:@"收到%@的红包", model.toUserName];
    } else if (model.type == 7) {
        _leftTopLabel.text = [NSString stringWithFormat:@"发给%@的转账", model.toUserName];
    } else if (model.type == 8) {
        _leftTopLabel.text = [NSString stringWithFormat:@"收到%@的转账", model.toUserName];
    } else {
        _leftTopLabel.text = [NSString stringWithFormat:@"%@", model.desc];
    }
    
    // 图片
    NSString *avatarImgString = @"";
    switch (model.type) {
        case 1:
        case 3:
            avatarImgString = @"icon_chongzhi";
            break;
        case 2:
        case 16:
            avatarImgString = @"icon_tixian";
            break;
        case 4:
        case 5:
            avatarImgString = @"icon_hongbao_r";
            break;
        case 6:
        case 9:
        case 20:
            avatarImgString = @"icon_tuikuan";
            break;
        case 7:
            avatarImgString = @"7"; // 转账头像
            break;
        case 8:
            avatarImgString = @"8"; // 接受转账头像
            break;
        case 10:
        case 11:
        case 12:
        case 13:
            avatarImgString = @"icon_tuikuan";
            break;
            
        default:
            avatarImgString = @"icon_tuikuan";
            break;
        }
    
    if ([avatarImgString isEqualToString:@"7"]) { // toUserId
        [g_server getHeadImageSmall:[NSString stringWithFormat:@"%ld", model.toUserId] userName:@"" imageView:_avatarImageView];
    } else if ([avatarImgString isEqualToString:@"8"]) { // userId
        [g_server getHeadImageSmall:[NSString stringWithFormat:@"%ld", model.toUserId] userName:@"" imageView:_avatarImageView];
    } else {
        _avatarImageView.image = [UIImage imageNamed:avatarImgString];
    }
    
    
}

// 时间戳转时间,时间戳为13位是精确到毫秒的，10位精确到秒
- (NSString *)getDateStringWithTimeStr:(NSString *)str {
    NSTimeInterval time=[str doubleValue]/1;//传入的时间戳str如果是精确到毫秒的记得要/1000
    NSDate *detailDate=[NSDate dateWithTimeIntervalSince1970:time];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init]; //实例化一个NSDateFormatter对象
    //设定时间格式,这里可以设置成自己需要的格式
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSString *currentDateStr = [dateFormatter stringFromDate: detailDate];
    return currentDateStr;
}

@end
