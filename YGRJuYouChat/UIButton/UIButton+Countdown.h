//
//  UIButton+Countdown.h
//  JMSmartAPP
//
//  Created by 冉彬 on 2019/1/9.
//  Copyright © 2019 Binge. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIButton (Countdown)


/**
 倒计时

 @param time 时间
 */
-(void)showCountdownTime:(NSUInteger)time;


/**
 倒计时
 
 @param time 时间
 @param bgcolor 背景色
 @param titleColor title颜色
 */
-(void)showCountdownTime:(NSUInteger)time bgcolor:(UIColor *)bgcolor titleColor:(UIColor *)titleColor;
@end

NS_ASSUME_NONNULL_END
