//
//  UIButton+Countdown.m
//  JMSmartAPP
//
//  Created by 冉彬 on 2019/1/9.
//  Copyright © 2019 Binge. All rights reserved.
//

#import "UIButton+Countdown.h"

@implementation UIButton (Countdown)


/**
 倒计时
 
 @param time 时间
 */
-(void)showCountdownTime:(NSUInteger)time
{
    [self showCountdownTime:time bgcolor:[UIColor lightGrayColor] titleColor:[UIColor whiteColor]];
}


/**
 倒计时
 
 @param time 时间
 @param bgcolor 背景色
 @param titleColor title颜色
 */
-(void)showCountdownTime:(NSUInteger)time bgcolor:(UIColor *)bgcolor titleColor:(UIColor *)titleColor
{
    UIColor *bColor = self.backgroundColor;
    UIColor *tColor = self.titleLabel.textColor;
    NSString *title = self.titleLabel.text;
    __block int timeout = (int)time;
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0);
    dispatch_source_set_event_handler(_timer, ^{
        if(timeout<=1){
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                [self setTitle:title forState:UIControlStateNormal];
                self.enabled = YES;
                [self setTitleColor:tColor forState:UIControlStateNormal];
                self.backgroundColor = bColor;
            });
        }else{
            int seconds = timeout-1;
            NSString *strTime = [NSString stringWithFormat:@"%.2d", seconds];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.enabled = NO;
                NSString *timeString = [NSString stringWithFormat:@"%@ s",strTime];
                self.titleLabel.text = timeString;
                [self setTitle: timeString forState:UIControlStateDisabled];
                [self setTitleColor:titleColor forState:UIControlStateDisabled];
                self.backgroundColor = bgcolor;
            });
            timeout--;
        }
    });
    dispatch_resume(_timer);
}

@end
