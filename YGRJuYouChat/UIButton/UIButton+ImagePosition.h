//
//  UIButton+ImagePosition.h
//  jmsg
//
//  Created by xxf on 16/9/21.
//  Copyright © 2016年 jmsg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (ImagePosition)

/**
 *  按钮文字图片垂直居中
 *
 *  @param spacing spacing
 */
- (void)centerImageAndTitle:(float)spacing;
- (void)centerImageAndTitle;


/**
 *  按钮文字在左，图片在右
 *
 *  @param spacing spacing
 */
- (void)imageRightAndTitleLeft:(float)spacing;
- (void)imageRightAndTitleLeft;

@end
