//
//  UIButton+ActionBlock.m
//  BGBaseProject
//
//  Created by 冉彬 on 2018/11/9.
//  Copyright © 2018 Binge. All rights reserved.
//

#import "UIButton+ActionBlock.h"
#import <objc/runtime.h>

@implementation UIButton (ActionBlock)

static BGActionBlock _clickActionBlock;

#pragma mark - Get,Set
- (void)setClickActionBlock:(BGActionBlock)clickActionBlock
{
    objc_setAssociatedObject(self, &_clickActionBlock, clickActionBlock, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(BGActionBlock)getClickActionBlock
{
    return (BGActionBlock)objc_getAssociatedObject(self, &_clickActionBlock);
}



#pragma mark - 实现
/**
 添加点击事件block
 
 @param callBack 回调
 */
- (void)addClickActionBlock:(BGActionBlock)callBack
{
    [self addActionBlock:callBack forControlEvents:(UIControlEventTouchUpInside)];
}


/**
 添加事件block
 
 @param callBack 回调
 @param controlEvents 事件类型
 */
- (void)addActionBlock:(BGActionBlock)callBack forControlEvents:(UIControlEvents)controlEvents
{
    [self setClickActionBlock:callBack];
    [self addTarget:self action:@selector(buttonAction:) forControlEvents:controlEvents];
}


- (void)buttonAction:(UIButton *)btn
{
    self.getClickActionBlock(btn);
}


/**
 按钮点击变灰

 @param time 时间，秒
 */
- (void)showCountdownTime:(NSUInteger)time
{
    __block int timeout = (int)time;
    __block NSString *titleStr = self.titleLabel.text;
    __block UIColor *bgColor = self.backgroundColor;
    __block UIColor *titleColor = self.titleLabel.textColor;
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0);
    dispatch_source_set_event_handler(_timer, ^{
        if(timeout<=0){
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                [self setTitle:titleStr forState:UIControlStateNormal];
                self.enabled = YES;
                [self setTitleColor:titleColor forState:UIControlStateNormal];
                self.backgroundColor = bgColor;
            });
        }else{
            int seconds = timeout-1;
            NSString *strTime = [NSString stringWithFormat:@"%.2d", seconds];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.enabled = NO;
                NSString *timeString = [NSString stringWithFormat:@"%@秒",strTime];
                self.titleLabel.text = timeString;
                [self setTitle: timeString forState:UIControlStateDisabled];
                [self setTitleColor:[UIColor whiteColor] forState:UIControlStateDisabled];
                self.backgroundColor = [UIColor lightGrayColor];
            });
            timeout--;
        }
    });
    dispatch_resume(_timer);
}







@end
