//
//  UIButton+ActionBlock.h
//  BGBaseProject
//
//  Created by 冉彬 on 2018/11/9.
//  Copyright © 2018 Binge. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^BGActionBlock)(UIButton *button);

@interface UIButton (ActionBlock)


/**
 添加点击事件block
 
 @param callBack 回调
 */
- (void)addClickActionBlock:(BGActionBlock)callBack;


/**
 添加事件block

 @param callBack 回调
 @param controlEvents 事件类型
 */
- (void)addActionBlock:(BGActionBlock)callBack forControlEvents:(UIControlEvents)controlEvents;


/**
 按钮点击变灰
 
 @param time 时间，秒
 */
- (void)showCountdownTime:(NSUInteger)time;








@end

NS_ASSUME_NONNULL_END
