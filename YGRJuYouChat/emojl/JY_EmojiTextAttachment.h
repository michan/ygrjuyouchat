#import <UIKit/UIKit.h>
@interface JY_EmojiTextAttachment : NSTextAttachment
@property(strong, nonatomic) NSString *emojiTag;
@end
