#import <UIKit/UIKit.h>
#import "JY_FaceViewController.h"
#import "JY_FavoritesVC.h"
#import "JY_EmojiPackgeVC.h"
@class menuImageView;
@class JY_gifViewController;
@interface JY_emojiViewController : UIView{
    menuImageView* _tb;
    JY_FaceViewController* _faceView;
    JY_gifViewController* _gifView;
}
@property (nonatomic, weak) id delegate;
@property (nonatomic, strong) JY_FaceViewController* faceView;
@property (nonatomic, strong) JY_FavoritesVC *JY_FavoritesVC;
@property (nonatomic, strong) NSArray *emojiDataArray;
-(void)selectType:(int)n;
@end
