@protocol ManMan_FavoritesVCDelegate <NSObject>
- (void) selectFavoritWithString:(NSString *) str;
- (void) deleteFavoritWithString:(NSString *) str;
@end
#import <UIKit/UIKit.h>
@interface JY_FavoritesVC : UIViewController
@property (nonatomic, weak) id<ManMan_FavoritesVCDelegate>delegate;
@end
