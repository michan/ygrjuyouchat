#import "JY_Label.h"
#import <UIKit/UIKit.h>
@interface JY_Emoji : JY_Label {
    NSMutableArray *data;
    int _top;
    int _size;
}
@property(nonatomic,assign)int faceHeight;
@property(nonatomic,assign)int faceWidth;
@property(nonatomic,assign)int maxWidth;
@property(nonatomic,assign)int offset;
@property(nonatomic, strong) NSMutableArray* matches;
@property(nonatomic, strong) NSSet* lastTouches;
@property(nonatomic,strong)  NSString * textCopy;
@property(nonatomic,assign)  BOOL contentEmoji;
@property(nonatomic, strong) NSString *atUserIdS;
@property(nonatomic,assign)  BOOL contentAt;
@property (nonatomic, assign) BOOL isShowNumber;
@property (nonatomic, copy) NSString *textStr;
-(void) setText:(NSString *)text;
@end
