//
//  JY_ManyAboutVc.m
//  TFJunYouChat
//
//  Created by os on 2021/2/8.
//  Copyright © 2021 zengwOS. All rights reserved.
//

#import "JY_ManyAboutVc.h"

@interface JY_ManyAboutVc ()

@end

@implementation JY_ManyAboutVc

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = Localized(@"JXAboutVC_AboutUS");
    self.heightFooter = 0;
    self.isGotoBack=YES;
    self.heightHeader = ManMan_SCREEN_TOP;
    [self createHeadAndFoot];
    self.tableBody.backgroundColor = HEXCOLOR(0xF2F2F2);
    
    [self prepareForCer];
}
 
- (void)prepareForCer{
         
    
    UIImageView *iconImg = [[UIImageView alloc] initWithFrame:CGRectMake((ManMan_SCREEN_WIDTH-120)/2,ManMan_SCREEN_TOP,120 ,120)];
    iconImg.image = [UIImage imageNamed:@"ALOGO_120"];
    [self.tableBody addSubview:iconImg];
    
    
    UILabel *name = [[UILabel alloc] initWithFrame:CGRectMake(30,CGRectGetMaxY(iconImg.frame)+10,ManMan_SCREEN_WIDTH-60,20)];
    name.font = [UIFont systemFontOfSize:14.0];
    name.textColor = RGB(51, 51, 51);
    name.textAlignment=NSTextAlignmentCenter;
    name.text = @"聚友版本V1.8.0";
    [self.tableBody addSubview:name];
    
    
    
    
}

@end
