#import "JY_admobViewController.h"
#import "DMScaleTransition.h"
#import "JY_ImageScrollVC.h"
#import "JY_ActionSheetVC.h"
#import "JY_SelectMenuView.h"
#import "JY_addMsgVC.h"
#import "JY_BlogRemindVC.h"
@protocol ManMan_ServerResult;
@interface JY_PSMyViewController : JY_admobViewController<ManMan_ServerResult,UIImagePickerControllerDelegate,UINavigationControllerDelegate,ManMan_ActionSheetVCDelegate,ManMan_SelectMenuViewDelegate>{
    JY_ImageView* _head;
    UIImage* _image;
    UILabel* _userName;
    UILabel* _userDesc;
    UIImageView *_qrImgV;
    UILabel* _friendLabel;
    UILabel* _groupLabel;
    BOOL _isSelected;
    JY_ImageView *_topImageVeiw;
    UIView *_setBaseView;
}
@property (nonatomic,weak)UILabel *userXinLabel;
@property (nonatomic, strong) DMScaleTransition *scaleTransition;
@property (nonatomic,assign) BOOL isRefresh;
@property (nonatomic,strong) UILabel * moneyLabel;
@property (nonatomic, assign) BOOL isAudioMeeting;
@property (nonatomic, assign) BOOL isGetUser;
-(void)refreshUserDetail;
@end
