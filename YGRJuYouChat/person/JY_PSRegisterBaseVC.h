#import "JY_admobViewController.h"
@interface JY_PSRegisterBaseVC : JY_admobViewController{
    UITextField* _pwd;
    UITextField* _repeat;
    UITextField* _name;
    UILabel* _workexp;
    UILabel* _city;
    UILabel* _dip;
    UISegmentedControl* _sex;
    UITextField* _birthday;
    JY_DatePicker* _date;
    UIImage* _image;
    JY_ImageView* _head;
}
@property (nonatomic,strong) NSString* resumeId;
@property (nonatomic,strong) NSString* password;
@property (nonatomic,assign) BOOL isRegister;
@property (nonatomic,strong) resumeBaseData* resumeModel;
@property (nonatomic,strong) JY_UserObject* user;
@property (nonatomic,assign) BOOL isSmsRegister;
@property (nonatomic,copy) NSString *inviteCodeStr; 
@property (nonatomic,copy) NSString *smsCode; 
@property (nonatomic, assign) int type;
@end
