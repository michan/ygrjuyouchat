#import "JY_admobViewController.h"
NS_ASSUME_NONNULL_BEGIN
@class JY_SetShikuNumVC;
@protocol ManMan_SetShikuNumVCDelegate <NSObject>
-(void)setShikuNum:(JY_SetShikuNumVC *)setShikuNumVC updateSuccessWithAccount:(NSString *)account;
@end
@interface JY_SetShikuNumVC : JY_admobViewController
@property (nonatomic, strong) JY_UserObject *user;
@property (nonatomic, weak) id<ManMan_SetShikuNumVCDelegate> delegate;
@end
NS_ASSUME_NONNULL_END
