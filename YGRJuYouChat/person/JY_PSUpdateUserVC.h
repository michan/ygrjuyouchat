#import "JY_admobViewController.h"
@interface JY_PSUpdateUserVC : JY_admobViewController{
    UITextField* _pwd;
    UITextField* _repeat;
    UITextField* _name;
    UILabel* _workexp;
    UILabel* _city;
    UILabel* _dip;
    UISegmentedControl* _sex;
    UITextField* _birthday;
    JY_DatePicker* _date;
    UIImage* _image;
    JY_ImageView* _head;
    UILabel *_inviteCode;
}
@property (nonatomic,strong) JY_UserObject* user;
@property (nonatomic,assign) BOOL isRegister;
@property (nonatomic, strong) UIImage *headImage;
@end
