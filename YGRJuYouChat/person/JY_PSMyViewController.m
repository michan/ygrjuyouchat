#import "JY_PSMyViewController.h"
#import "JY_ImageView.h"
#import "JY_Label.h"
#import "AppDelegate.h"
#import "JY_Server.h"
#import "JY_Connection.h"
#import "UIFactory.h"
#import "JY_TableView.h"
#import "JY_FriendViewController.h"
#import "ImageResize.h"
#import "JY_userWeiboVC.h"
#import "JY_myMediaVC.h"
#import "webpageVC.h"
#import "JY_loginVC.h"
#import "JY_NewFriendViewController.h"
#import "JY_PSRegisterBaseVC.h"
#import "photosViewController.h"
#import "JY_SettingVC.h"
#import "JY_PSUpdateUserVC.h"
#import "OrganizTreeViewController.h"
#import "JY_CourseListVC.h"
#import "JY_MyMoneyViewController.h"
#import "JY_NearVC.h"
#import "JY_SelFriendVC.h"
#import "JY_SelectFriendsVC.h"
#import "JiaTui_WeiboVController.h"
#ifdef Meeting_Version 
#endif
#ifdef Live_Version
#import "ManMan_LiveViewController.h"
#endif
#import "JY_FriendViewController.h"
#import "JY_GroupViewController.h"
#import "UIImage+Color.h"
#import "JY_ScanQRViewController.h"
#import "JY_JLApplyForWithdrawalVC.h"
#import "JY_RealCerVc.h"
#import "QRImage.h"
#import "JY_ManyAboutVc.h"
#import "CertifyVC.h"


#import "JY_TFJunYou_SecuritySettingVC.h"
#import "JY_AboutVC.h"
#import "JY_ChatViewController.h"
#import "TFJunYou_securityCenterVC.h"

#define HEIGHT 50
#define MY_INSET  0
#define TOP_ADD_HEIGHT  400  
@implementation JY_PSMyViewController
- (id)init
{
    self = [super init];
    if (self) {
        self.isRefresh = NO;
        self.title = @"我";
        self.heightHeader = 0;
        self.heightFooter = ManMan_SCREEN_BOTTOM;
        
        [self createHeadAndFoot];
        int h=-20;
        int w=ManMan_SCREEN_WIDTH;
        float marginHei = 10;
        self.tableBody.backgroundColor = RGB(249, 249, 249);;
        JY_ImageView* iv;
        iv = [self createHeadButtonclick:@selector(onResume)];
        _topImageVeiw = iv;
        iv.frame = CGRectMake(0, THE_DEVICE_HAVE_HEAD ? -TOP_ADD_HEIGHT- 44 : -TOP_ADD_HEIGHT-20, w, 173+ManMan_SCREEN_TOP+TOP_ADD_HEIGHT);
        h = CGRectGetMaxY(iv.frame);
        _setBaseView = [[UIView alloc] initWithFrame:CGRectMake(0, h-20, ManMan_SCREEN_WIDTH,ManMan_SCREEN_HEIGHT - h+100)];
        _setBaseView.backgroundColor = RGB(249, 249, 249);
        [self.tableBody addSubview:_setBaseView];
        //[self setPartRoundWithView:_setBaseView corners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadius:20];
        h = 10;
        
        
        if ([g_config.enablePayModule boolValue]) {
            iv = [self createButton:@"我的钱包" drawTop:NO drawBottom:NO icon:THESIMPLESTYLE ? @"icon_qianbao" : @"icon_qianbao" click:@selector(onRecharge)];
            iv.frame = CGRectMake(MY_INSET,h, w-MY_INSET*2, HEIGHT);

            h+=iv.frame.size.height;
            h += 10;
        }
        /*  */
        
        iv = [self createButton:@"我的动态" drawTop:NO drawBottom:YES icon:THESIMPLESTYLE ? @"icon_dongtai" : @"icon_dongtai" click:@selector(onMyBlog)];
        iv.frame = CGRectMake(MY_INSET,h, w-MY_INSET*2, HEIGHT);
        h+=iv.frame.size.height;
        iv = [self createButton:@"我的收藏" drawTop:NO drawBottom:NO icon:THESIMPLESTYLE ? @"icon_shoucang_my" : @"icon_shoucang_my" click:@selector(onMyFavorite)];
        iv.frame = CGRectMake(MY_INSET,h, w-MY_INSET*2, HEIGHT);
        h+=iv.frame.size.height;
       
        //= 0 开 =1 关
        /*  */
        if ([g_App.isShowApplyForWithdrawal intValue] == 1) {
             iv = [self createButton:Localized(@"JX_ApplicationforwithdrawalD") drawTop:NO drawBottom:YES icon:THESIMPLESTYLE ? @"icon_tixian" : @"icon_tixian" click:@selector(onApplyForWithdrawal)];
             iv.frame = CGRectMake(MY_INSET,h, w-MY_INSET*2, HEIGHT);
             h+=iv.frame.size.height;
       
        }
      
        
//        iv = [self createButton:@"实名认证" drawTop:NO drawBottom:NO icon:THESIMPLESTYLE ? @"icon_dongtai" : @"icon_dongtai" click:@selector(cerActivity)];
//        iv.frame = CGRectMake(MY_INSET,h, w-MY_INSET*2, HEIGHT);
//        h+=iv.frame.size.height;
       
        
        BOOL isShowLine = NO;
#ifdef IS_SHOW_MENU
#else
#ifdef Meeting_Version
       isShowLine = YES;
        /*
        h += 10;
        if ([g_App.videoMeeting intValue] == 1) {
            h+=15;
            UIView *line1 = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(iv.frame), ManMan_SCREEN_WIDTH, marginHei)];
            line1.backgroundColor = HEXCOLOR(0xF2F2F2);
            [_setBaseView addSubview:line1];
            iv = [self createButton:Localized(@"JXSettingVC_VideoMeeting") drawTop:NO drawBottom:YES icon:THESIMPLESTYLE ? @"videomeeting_simple" : @"videomeeting" click:@selector(onMeeting)];
         
            iv.frame = CGRectMake(MY_INSET,h, w-MY_INSET*2, HEIGHT);
            h+=iv.frame.size.height;

        }
        
       */
        isShowLine = NO;
#else
        isShowLine = YES;
#endif
#ifdef Live_Version
//        h+=15;
        
        if ([g_App.isShowRedPacket intValue] == 1 ) {
            if (!isShowLine) {
                UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(iv.frame), ManMan_SCREEN_WIDTH, marginHei)];
               // line.backgroundColor = HEXCOLOR(0xF2F2F2);
                [_setBaseView addSubview:line];
            }
            UIView *line1 = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(iv.frame), ManMan_SCREEN_WIDTH, marginHei)];
            //line1.backgroundColor = HEXCOLOR(0xF2F2F2);
            [_setBaseView addSubview:line1];
            iv = [self createButton:Localized(@"JX_LiveDemonstration") drawTop:isShowLine drawBottom:NO icon:THESIMPLESTYLE ? @"videoshow_simple" : @"videoshow" click:@selector(onLive)];
            
            iv.frame = CGRectMake(MY_INSET,h, w-MY_INSET*2, HEIGHT);
            h+=iv.frame.size.height + marginHei;
        }
      
        isShowLine = YES;
#else
        isShowLine = NO;
#endif
#endif
        
        h+=10;
        UIView *line1 = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(iv.frame), ManMan_SCREEN_WIDTH, marginHei)];
        //line1.backgroundColor = HEXCOLOR(0xF2F2F2);
        [_setBaseView addSubview:line1];
        
        
        iv = [self createButton:@"分享" drawTop:NO drawBottom:NO icon:THESIMPLESTYLE ? @"icon_fenxiang" : @"icon_fenxiang" click:@selector(realNameCer)];
        
        iv.frame = CGRectMake(MY_INSET,h, w-MY_INSET*2, HEIGHT);
            h+=iv.frame.size.height;
 
        h+=10;
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(iv.frame), ManMan_SCREEN_WIDTH, marginHei)];
        //line.backgroundColor = HEXCOLOR(0xF2F2F2);
        [_setBaseView addSubview:line];
        
        
        
        iv = [self createButton:@"账号与安全" drawTop:NO drawBottom:YES icon:THESIMPLESTYLE ? @"icon_zhanghaoyuanquan" : @"icon_zhanghaoyuanquan" click:@selector(accountSecurity)];
        iv.frame = CGRectMake(MY_INSET,h, w-MY_INSET*2, HEIGHT);
        h+=iv.frame.size.height;
        
        iv = [self createButton:@"设置" drawTop:NO drawBottom:NO icon:THESIMPLESTYLE ? @"icon_shezhi" : @"icon_shezhi" click:@selector(onSetting)];
        iv.frame = CGRectMake(MY_INSET,h, w-MY_INSET*2, HEIGHT);
        CGRect frame = _setBaseView.frame;
        frame.size.height = CGRectGetMaxY(iv.frame);
        _setBaseView.frame = frame;
        if ((h + HEIGHT + 20) > self.tableBody.frame.size.height) {
            self.tableBody.contentSize = CGSizeMake(self_width, CGRectGetMaxY(_setBaseView.frame) + 40);
        }
        self.view.backgroundColor = RGB(249, 249, 249);
        self.tableBody.backgroundColor = RGB(249, 249, 249);
        self.tableBody.contentSize = CGSizeMake(self_width, CGRectGetMaxY(_setBaseView.frame)+20);
        [g_notify addObserver:self selector:@selector(doRefresh:) name:kUpdateUserNotifaction object:nil];
        [g_notify addObserver:self selector:@selector(updateUserInfo:) name:kXMPPMessageUpadteUserInfoNotification object:nil];
        [g_notify addObserver:self selector:@selector(doRefresh:) name:kOfflineOperationUpdateUserSet object:nil];
        [g_server getUserMoenyToView:self];
    }
    return self;
}
- (void)updateUserInfo:(NSNotification *)noti {
    [g_server getUser:g_server.myself.userId toView:self];
}
-(void)dealloc{
    NSLog(@"JY_PSMyViewController.dealloc");
    [g_notify removeObserver:self name:kUpdateUserNotifaction object:nil];
    [g_notify removeObserver:self name:kXMPPMessageUpadteUserInfoNotification object:nil];
    [g_notify removeObserver:self name:kOfflineOperationUpdateUserSet object:nil];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
- (void)setStatusBarBackgroundColor:(UIColor *)color {
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = color;
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [g_server getUser:g_myself.userId toView:self];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    if (_friendLabel) {
         NSArray *friends = [[JY_UserObject sharedInstance] fetchAllUserFromLocal];
           _friendLabel.text = [NSString stringWithFormat:@"%ld",friends.count];
    }
    if (_groupLabel) {
        NSArray *groups = [[JY_UserObject sharedInstance] fetchAllRoomsFromLocal];
        _groupLabel.text = [NSString stringWithFormat:@"%ld",groups.count];
    }
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}
- (void)viewDidAppear:(BOOL)animated
{
    if (self.isRefresh) {
        self.isRefresh = NO;
    }else{
        [super viewDidAppear:animated];
        [self doRefresh:nil];
    }
}
- (void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
}
-(void)doRefresh:(NSNotification *)notifacation{
    _head.image = nil;
    [g_server getHeadImageSmall:g_server.myself.userId userName:g_server.myself.userNickname imageView:_head];
    _userName.text = g_server.myself.userNickname;
    _userDesc.text = [NSString stringWithFormat:@"%@:%@",@"聚友号", g_server.myself.account];;
}
- (void)viewDidUnload
{
    [super viewDidUnload];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(void) didServerResultSucces:(JY_Connection*)aDownload dict:(NSDictionary*)dict array:(NSArray*)array1{
        [_wait hide];
    if( [aDownload.action isEqualToString:act_resumeList] ){
    }
    if( [aDownload.action isEqualToString:act_UserGet] ){
        JY_UserObject* user = [[JY_UserObject alloc]init];
        [user getDataFromDict:dict];
        g_server.myself.userNickname = user.userNickname;
        g_server.myself.account = user.account;
        _userDesc.text = [NSString stringWithFormat:@"%@:%@",@"聚友号", g_server.myself.account];
        NSRange range = [user.telephone rangeOfString:@"86"];
        if (range.location != NSNotFound) {
            g_server.myself.telephone = [user.telephone substringFromIndex:range.location + range.length];
        }
        if (self.isGetUser) {
            self.isGetUser = NO;
            JY_PSUpdateUserVC* vc = [JY_PSUpdateUserVC alloc];
            vc.headImage = [_head.image copy];
            vc.user = user;
            vc = [vc init];
            [g_navigation pushViewController:vc animated:YES];
            return;
        }
        _userName.text = user.userNickname;
        [g_server delHeadImage:g_server.myself.userId];
        [g_server getHeadImageSmall:g_server.myself.userId userName:g_server.myself.userNickname imageView:_head];
    }
    if ([aDownload.action isEqualToString:act_getUserMoeny]) {
        g_App.myMoney = [dict[@"balance"] doubleValue];
        _moneyLabel.text = [NSString stringWithFormat:@"%.2f%@",g_App.myMoney,Localized(@"JX_ChinaMoney")];
    }
}
-(int) didServerResultFailed:(JY_Connection*)aDownload dict:(NSDictionary*)dict{
    [_wait hide];
    if( [aDownload.action isEqualToString:act_UserGet] ){
        if (!self.isGetUser) {
            JY_PSUpdateUserVC* vc = [JY_PSUpdateUserVC alloc];
            vc.headImage = [_head.image copy];
            vc.user = g_server.myself;
            vc = [vc init];
            [g_navigation pushViewController:vc animated:YES];
        }
    }
    return hide_error;
}
-(int) didServerConnectError:(JY_Connection*)aDownload error:(NSError *)error{
    [_wait hide];
    if( [aDownload.action isEqualToString:act_UserGet] ){
        if (!self.isGetUser) {
            JY_PSUpdateUserVC* vc = [JY_PSUpdateUserVC alloc];
            vc.headImage = [_head.image copy];
            vc.user = g_server.myself;
            vc = [vc init];
            [g_navigation pushViewController:vc animated:YES];
        }
    }
    return hide_error;
}
-(void) didServerConnectStart:(JY_Connection*)aDownload{
}
-(void)actionClear{
    [_wait start:Localized(@"PSMyViewController_Clearing") delay:100];
}
#ifdef Live_Version
- (void)onLive {
    ManMan_LiveViewController *vc = [[ManMan_LiveViewController alloc] init];
    [g_navigation pushViewController:vc animated:YES];
}
#endif
#ifdef Meeting_Version
- (void)onMeeting {
    if(g_xmpp.isLogined != 1){
        [g_xmpp showXmppOfflineAlert];
        return;
    }
    NSString *str1;
    NSString *str2;
    str1 = Localized(@"JXSettingVC_VideoMeeting");
    str2 = Localized(@"JX_Meeting");
    JY_ActionSheetVC *actionVC = [[JY_ActionSheetVC alloc] initWithImages:@[@"meeting_tel",@"meeting_video"] names:@[str2,str1]];
    actionVC.delegate = self;
    [self presentViewController:actionVC animated:NO completion:nil];
}
- (void)actionSheet:(JY_ActionSheetVC *)actionSheet didButtonWithIndex:(NSInteger)index {
    if (index == 0) {
        [self onGroupAudioMeeting:nil];
    }else if(index == 1){
        [self onGroupVideoMeeting:nil];
    }
}
-(void)onGroupAudioMeeting:(JY_MessageObject*)msg{
    self.isAudioMeeting = YES;
    [self onInvite];
}
-(void)onGroupVideoMeeting:(JY_MessageObject*)msg{
    self.isAudioMeeting = NO;
    [self onInvite];
}

- (void)accountSecurity {
    JY_TFJunYou_SecuritySettingVC *vc = [[JY_TFJunYou_SecuritySettingVC alloc] init];
    [g_navigation pushViewController:vc animated:YES];
}

- (void)team {
    
    // 客服
    JY_ChatViewController *sendView = [JY_ChatViewController alloc];

    
    JY_UserObject *user = [[JY_UserObject alloc] init];
    
    user = [user getUserById:COSUMER_SERVICE_CENTER_USERID];

    sendView.chatPerson = user;
    sendView.chatPerson.userId = COSUMER_SERVICE_CENTER_USERID;
    sendView.chatPerson.userNickname = @"在线客服";
    sendView.title = @"在线客服";

    sendView = [sendView init];

    [g_navigation pushViewController:sendView animated:YES];
}


- (void)onAbout {
    JY_AboutVC *vc = [[JY_AboutVC alloc] init];
//    [g_window addSubview:vc.view];
    [g_navigation pushViewController:vc animated:YES];
}

-(void)realNameCer{
      
    NSMutableString * qrStr = [NSMutableString stringWithFormat:@"%@?action=",g_config.website];
    [qrStr appendString:@"user"];
    UIImageView *imageView = [[UIImageView alloc] init];
    [g_server getHeadImageLarge:g_myself.userId userName:g_myself.userNickname imageView:imageView];
     
    UIImage * qrImage = [QRImage qrImageForString:qrStr imageSize:300 logoImage:imageView.image logoImageSize:70];
      
    NSArray * activityItems = @[qrImage];
    UIActivityViewController * activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    UIActivityViewControllerCompletionWithItemsHandler myBlock = ^(UIActivityType __nullable activityType, BOOL completed, NSArray * __nullable returnedItems, NSError * __nullable activityError){
        if (completed) {
            //[ToastUtils showHud:@"分享成功"];//此tost为自己封装的所以这句不用复制
        }else{
            // [ToastUtils showHud:@"分享失败，请重试"];//此tost为自己封装的所以这句不用复制
        }
        [activityVC dismissViewControllerAnimated:YES completion:nil];
    };
    activityVC.completionWithItemsHandler = myBlock;
    activityVC.excludedActivityTypes = @[UIActivityTypePostToFacebook,    UIActivityTypePostToTwitter,  UIActivityTypePostToWeibo,  UIActivityTypeMessage,  UIActivityTypeMail,  UIActivityTypePrint,  UIActivityTypeCopyToPasteboard, UIActivityTypeSaveToCameraRoll,  UIActivityTypePostToTencentWeibo,  UIActivityTypeAirDrop, UIActivityTypeOpenInIBooks];
    [self presentViewController:activityVC animated:YES completion:nil];
  
  
    
    
    
    
    return;
    JY_RealCerVc *cervc=[[JY_RealCerVc alloc]init];
    
    [g_navigation pushViewController:cervc animated:YES];
}
-(void)onInvite{
    NSMutableSet* p = [[NSMutableSet alloc]init];
    JY_SelectFriendsVC* vc = [JY_SelectFriendsVC alloc];
    vc.isNewRoom = NO;
    vc.isShowMySelf = NO;
    vc.type = ManMan_SelUserTypeSelFriends;
    vc.existSet = p;
    vc.delegate = self;
    vc.didSelect = @selector(meetingAddMember:);
    vc = [vc init];
    [g_navigation pushViewController:vc animated:YES];
}
-(void)meetingAddMember:(JY_SelectFriendsVC*)vc{
    int type;
    if (self.isAudioMeeting) {
        type = kWCMessageTypeAudioMeetingInvite;
    }else {
        type = kWCMessageTypeVideoMeetingInvite;
    }
    for(NSNumber* n in vc.set){
        JY_UserObject *user;
        if (vc.seekTextField.text.length > 0) {
            user = vc.searchArray[[n intValue] % 100000-1];
        }else{
            user = [[vc.letterResultArr objectAtIndex:[n intValue] / 100000-1] objectAtIndex:[n intValue] % 100000-1];
        }
        NSString* s = [NSString stringWithFormat:@"%@",user.userId];
        [g_meeting sendMeetingInvite:s toUserName:user.userNickname roomJid:MY_USER_ID callId:nil type:type];
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (g_meeting.isMeeting) {
            return;
        }
        
    });
}
#endif
- (void)onApplyForWithdrawal {
    JY_JLApplyForWithdrawalVC *vc = [[JY_JLApplyForWithdrawalVC alloc] init];
    [g_App.navigation pushViewController:vc animated:YES];
}


//实名认证
-(void)cerActivity{
    
    CertifyVC * webVC = [CertifyVC alloc];
    webVC = [webVC init];
   [g_navigation pushViewController:webVC animated:YES];
 
}


//微博 JY_userWeiboVC
-(void)onMyBlog{
    
    
    JiaTui_WeiboVController* vc = [JiaTui_WeiboVController alloc];
    vc.user = g_myself;
    vc.isGotoBack = YES;
    vc = [vc init];
    [g_navigation pushViewController:vc animated:YES];
}


-(void)onNear{
    JY_NearVC * nearVc = [[JY_NearVC alloc] init];
    [g_navigation pushViewController:nearVc animated:YES];
}
-(void)onFriend{
    JY_FriendViewController* vc = [[JY_FriendViewController alloc]init];
    [g_navigation pushViewController:vc animated:YES];
}
-(void)onResume{
    self.isGetUser = YES;
    [g_server getUser:MY_USER_ID toView:self];
}
-(void)onSpace{
}
-(void)onVideo{
    JY_myMediaVC* vc = [[JY_myMediaVC alloc] init];
    [g_navigation pushViewController:vc animated:YES];
}
-(void)onMyFavorite{
    JY_WeiboVC * collection = [[JY_WeiboVC alloc] initCollection];
    [g_navigation pushViewController:collection animated:YES];
}
- (void)onCourse {
    JY_CourseListVC *vc = [[JY_CourseListVC alloc] init];
    [g_navigation pushViewController:vc animated:YES];
}
-(void)onRecharge{
    JY_MyMoneyViewController * moneyVC = [[JY_MyMoneyViewController alloc] init];
    [g_navigation pushViewController:moneyVC animated:YES];
}
-(void)onOrganiz{
    OrganizTreeViewController * organizVC = [[OrganizTreeViewController alloc] init];
    [g_navigation pushViewController:organizVC animated:YES];
}
-(void)onMyLove{
}
-(void)onMoney{
}
-(void)onSetting{
    JY_SettingVC* vc = [[JY_SettingVC alloc]init];
    [g_navigation pushViewController:vc animated:YES];
}
- (void)onAdd {
    JY_SelectMenuView *menuView = [[JY_SelectMenuView alloc] initWithTitle:@[Localized(@"JX_SendImage"),Localized(@"JX_SendVoice"),Localized(@"JX_SendVideo"),Localized(@"JX_SendFile")]image:@[@"menu_add_msg",@"menu_add_voice",@"menu_add_video",@"menu_add_file",@"menu_add_reply"]cellHeight:45];
    menuView.delegate = self;
    [g_App.window addSubview:menuView];
}
- (void)didMenuView:(JY_SelectMenuView *)MenuView WithIndex:(NSInteger)index {
    switch (index) {
        case 0:
        case 1:
        case 2:
        case 3:{
            JY_addMsgVC* vc = [[JY_addMsgVC alloc] init];
            vc.dataType = (int)index + 2;
            [g_navigation pushViewController:vc animated:YES];
        }
            break;
        default:
            break;
    }
}
-(JY_ImageView*)createHeadButtonclick:(SEL)click{
    JY_ImageView* btn = [[JY_ImageView alloc] init];
    btn.backgroundColor = RGB(249, 249, 249);
    btn.userInteractionEnabled = YES;
    btn.didTouch = click;
    btn.delegate = self;
    btn.layer.cornerRadius = 5;
    btn.layer.masksToBounds = YES;
    [self.tableBody addSubview:btn];
    UIView *baseView = [[UIView alloc] initWithFrame:CGRectMake(0, TOP_ADD_HEIGHT, ManMan_SCREEN_WIDTH, 173+ManMan_SCREEN_TOP)];
    baseView.backgroundColor = [UIColor whiteColor];
    [btn addSubview:baseView];
    UIButton *addBtn = [[UIButton alloc] initWithFrame:CGRectMake(ManMan_SCREEN_WIDTH-15*2-18, THE_DEVICE_HAVE_HEAD ? 59-15 : 35-15, 18+15*2, 18+15*2)];
    //[addBtn setImage:[UIImage imageNamed:@"my_add_white"] forState:UIControlStateNormal];
    //[addBtn addTarget:self action:@selector(onAdd) forControlEvents:UIControlEventTouchUpInside];
    addBtn.userInteractionEnabled=NO;
    [baseView addSubview:addBtn];
    _head = [[JY_ImageView alloc]initWithFrame:CGRectMake(20, ManMan_SCREEN_TOP + 32+32, 57, 57)];
    _head.layer.cornerRadius = 10;
    _head.layer.masksToBounds = YES;
    _head.layer.borderWidth = 3.f;
    _head.layer.borderColor = [UIColor whiteColor].CGColor;
    _head.didTouch = @selector(onResume);
    _head.delegate = self;
    [baseView addSubview:_head];
    UILabel* p = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_head.frame)+10, CGRectGetMinY(_head.frame)+5, 150, 22)];
    p.font = SYSFONT(20);
    p.text = MY_USER_NAME;
    p.textColor = RGB(17, 17, 17);
    p.backgroundColor = [UIColor clearColor];
    [baseView addSubview:p];
    
    _userName = p;
    p = [[UILabel alloc]init];//WithFrame:CGRectMake(CGRectGetMinX(p.frame), CGRectGetMaxY(p.frame)+8, 180, 14)];
    p.font = SYSFONT(14);
    p.text = g_server.myself.account;
    p.textColor =HEXCOLOR(0x606060);
    p.backgroundColor = [UIColor clearColor];
    [baseView addSubview:p];
    [p mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_userName.mas_bottom).offset(8);
        make.left.mas_equalTo(_userName);
    }];
    _userDesc = p;
    UIImageView* qrImgV = [[UIImageView alloc] init];//WithFrame:CGRectMake(ManMan_SCREEN_WIDTH-20-35, CGRectGetMinY(p.frame), 13, 13)];
    qrImgV.image = [UIImage imageNamed:@"icon_erweima"];
    [baseView addSubview:qrImgV];
    _qrImgV = qrImgV;
    [qrImgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.width.mas_equalTo(13);
        make.centerY.mas_equalTo(_userDesc.mas_centerY);
        make.left.mas_equalTo(_userDesc.mas_right).offset(5);
    }];
    
    UIImageView* iv = [[UIImageView alloc] init];//WithFrame:CGRectMake(ManMan_SCREEN_WIDTH-15-20, CGRectGetMinY(qrImgV.frame), 6, 12)];
    iv.image = [UIImage imageNamed:@"new_icon_>"];
    [baseView addSubview:iv];
    [iv mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(qrImgV.mas_centerY);
        make.width.mas_equalTo(6);
        make.height.mas_equalTo(12);
        make.right.mas_equalTo(baseView).offset(-29);
    }];
    
    return btn;
}
- (void)setPartRoundWithView:(UIView *)view corners:(UIRectCorner)corners cornerRadius:(float)cornerRadius {
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    shapeLayer.path = [UIBezierPath bezierPathWithRoundedRect:view.bounds byRoundingCorners:corners cornerRadii:CGSizeMake(cornerRadius, cornerRadius)].CGPath;
    view.layer.mask = shapeLayer;
}
- (void)onColleagues:(UITapGestureRecognizer *)tap {
    if (_isSelected)
        return;
    _isSelected = YES;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self->_isSelected = NO;
    });
    switch (tap.view.tag) {
        case 0:{
            JY_FriendViewController *friendVC = [JY_FriendViewController alloc];
            friendVC.isMyGoIn = YES;
            friendVC = [friendVC  init];
            [g_navigation pushViewController:friendVC animated:YES];
        }
            break;
        case 1:{
            JY_GroupViewController *groupVC = [[JY_GroupViewController alloc] init];
            [g_navigation pushViewController:groupVC animated:YES];
        }
            break;
        default:
            break;
    }
}
- (UIButton *)createViewWithFrame:(CGRect)frame title:(NSString *)title icon:(NSString *)icon index:(CGFloat)index showLine:(BOOL)isShow{
    UIButton *view = [[UIButton alloc] init];
    [view setBackgroundImage:[UIImage createImageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
    [view setBackgroundImage:[UIImage createImageWithColor:HEXCOLOR(0xF6F5FA)] forState:UIControlStateHighlighted];
    view.frame = frame;
    view.tag = index;
    [self.tableBody addSubview:view];
    int imgH = 40.5;
    UIImageView *imgV = [[UIImageView alloc] init];
    imgV.frame = CGRectMake((view.frame.size.width-imgH)/2, (view.frame.size.height-imgH-15-3)/2, imgH, imgH);
    imgV.image = [UIImage imageNamed:icon];
    [view addSubview:imgV];
    UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(0, CGRectGetMaxY(imgV.frame)+3, view.frame.size.width, 15);
    label.text = title;
    label.textAlignment = NSTextAlignmentCenter;
    label.font = SYSFONT(15);
    label.textColor = HEXCOLOR(0x323232);
    [view addSubview:label];
    if (index == 0) {
        _friendLabel = label;
    }else {
        _groupLabel = label;
    }
    if (isShow) {
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(view.frame.size.width-LINE_WH, (view.frame.size.height-24)/2, LINE_WH, 24)];
        line.backgroundColor = THE_LINE_COLOR;
       // [view addSubview:line];
    }
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onColleagues:)];
    [view addGestureRecognizer:tap];
    return view;
}
-(JY_ImageView*)createButton:(NSString*)title drawTop:(BOOL)drawTop drawBottom:(BOOL)drawBottom icon:(NSString*)icon click:(SEL)click{
    JY_ImageView* btn = [[JY_ImageView alloc] init];
    btn.backgroundColor = [UIColor whiteColor];
    btn.userInteractionEnabled = YES;
    btn.didTouch = click;
    btn.delegate = self;
//    btn.layer.cornerRadius =5;
//    btn.layer.masksToBounds =YES;
    [_setBaseView addSubview:btn];
     
  
    JY_Label* p = [[JY_Label alloc] initWithFrame:CGRectMake(54, 0, self_width-35-20-35, HEIGHT)];
    p.text = title;
    p.font = g_factory.font16;
    p.backgroundColor = [UIColor clearColor];
    p.textColor = HEXCOLOR(0x323232);
    [btn addSubview:p];
    if(icon){
        UIImageView* iv = [[UIImageView alloc] initWithFrame:CGRectMake(10, 13.5, 23, 23)];
        iv.image = [UIImage imageNamed:icon];
        [btn addSubview:iv];
    }
    if(drawTop){
        UIView* line = [[UIView alloc] initWithFrame:CGRectMake(53,0,ManMan_SCREEN_WIDTH-53,LINE_WH)];
        //line.backgroundColor = THE_LINE_COLOR;
        [btn addSubview:line];
    }
    if(drawBottom){
        UIView* line = [[UIView alloc] initWithFrame:CGRectMake(53,HEIGHT-0.3,ManMan_SCREEN_WIDTH-53,LINE_WH)];
        line.backgroundColor = THE_LINE_COLOR;
        line.alpha = 0.5;
        [btn addSubview:line];
    }
    if(click){
        UIImageView* iv;
        iv = [[UIImageView alloc] initWithFrame:CGRectMake(ManMan_SCREEN_WIDTH-10-27, (HEIGHT-13)/2, 7, 13)];
        iv.image = [UIImage imageNamed:@"new_icon_>"];
        [btn addSubview:iv];
    }
    return btn;
}
-(void)onHeadImage{
    [g_server delHeadImage:g_myself.userId];
    JY_ImageScrollVC * imageVC = [[JY_ImageScrollVC alloc]init];
    imageVC.imageSize = CGSizeMake(ManMan_SCREEN_WIDTH, ManMan_SCREEN_WIDTH);
    imageVC.iv = [[JY_ImageView alloc] initWithFrame:CGRectMake(0, 0, ManMan_SCREEN_WIDTH, ManMan_SCREEN_WIDTH)];
    imageVC.iv.center = imageVC.view.center;
    [g_server getHeadImageLarge:g_myself.userId userName:g_myself.userNickname imageView:imageVC.iv];
    [self addTransition:imageVC];
    [self presentViewController:imageVC animated:YES completion:^{
        self.isRefresh = YES;
    }];
}
- (void)setupView:(UIView *)view colors:(NSArray *)colors {
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = CGRectMake(0, 0, ManMan_SCREEN_WIDTH, 266+TOP_ADD_HEIGHT-86);  
    gradientLayer.colors = colors;  
    gradientLayer.startPoint = CGPointMake(0, 0);
    gradientLayer.endPoint = CGPointMake(1, 0);
    [view.layer addSublayer:gradientLayer];
}
- (void) addTransition:(JY_ImageScrollVC *) siv
{
    self.scaleTransition = [[DMScaleTransition alloc]init];
    [siv setTransitioningDelegate:self.scaleTransition];
}
@end
